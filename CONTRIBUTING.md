**How to contribute**

You must have Eclipse installed, at version Oxygen or higher, on a computer with a minimum of 1.6 GB of RAM. The current download site is located [here](http://www.eclipse.org/downloads/eclipse-packages/).
 

You must also have Xtext installed as an Eclipse plugin. The following section walks you through the install, with the assumption that you already have Eclipse up and running.

*Setting up Xtext for Eclipse*

1.  In the Eclipse menu bar, select "Help" -> "Install new software...", then click the "Add" button in the top right. A new menu called "Add repository" will pop up.
2.  In the "Name" field you can write whatever you like - we suggest something descriptive like "Xtext releases update site" - while in the "Location" field you should enter the http://download.eclipse.org/modeling/tmf/xtext/updates/composite/releases/ link. Then press the "OK" button. A list of possible downloads will appear. Please note that this step and the next may take a while; if you're waiting for Eclipse to respond, look for a small green progress bar in the bottom right-hand corner of the program to see how far along you are in the process.
3.   One of those downloads is called "Xtext". Press the '>' character next to its checkbox, check the subitem called "Xtext Complete SDK" and press the "Next" button, then "Accept License", and finally "Finish". Please note that if asked, you need to confirm that you trust the certificate.

(Optional) If you wish to familiarize yourself with Xtext, we suggest starting with the "5 minute tutorial" found at https://www.eclipse.org/Xtext/documentation/101_five_minutes.html

Now you need to test Xtext is correctly set up for Eclipse. To do so, please follow the steps listed in the "15 minute tutorial" at https://www.eclipse.org/Xtext/documentation/102_domainmodelwalkthrough.html up to 
the "Run the Generated Eclipse Plug-in" section. 

Once you've completed the "Run as" -> "Eclipse Application", you may be asked to select a launch configuration; if so, choose "Eclipse Application". However, please note that this configuration may not enable the correct plug-ins for the second Eclipse instance you just ran. To ensure that it's enabled, do the following:
1.  Choose "Run As" -> "Run configurations..."
2.  Select your "Eclipse Application" configuration and click the "Plug-ins" tab
3.  From the "Launch with" drop-down menu, choose the "plug-ins selected below only" option.
4.  Click the "Select all" button on the right, then *deselect* from your Workspace plug-in tree (not your Target Platform tree) all plugins that are not from <pre>org.example.domainmodel.*</pre>
