package de.ugoe.cs.swe.T3Q;

import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.validation.FeatureBasedDiagnostic;

import com.google.common.collect.Lists;

public class TTCN3Output {
	private final Resource resource;
	private final List<FeatureBasedDiagnostic> output;
	
	public TTCN3Output(final Resource resource) {
		this.resource = resource;
		this.output  = Lists.newArrayList();
	}

	public Resource getResource() {
		return resource;
	}

	public List<FeatureBasedDiagnostic> getOutput() {
		return output;
	}
}
