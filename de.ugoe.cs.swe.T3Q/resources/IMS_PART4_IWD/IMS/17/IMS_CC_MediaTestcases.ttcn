/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-02-24 17:02:28 +0100 (Wed, 24 Feb 2016) $
// $Rev: 15347 $
/******************************************************************************/
module IMS_CC_MediaTestcases {

  import from LibSip_SIPTypesAndValues all;
  import from LibSip_SDPTypes all;
  import from IP_ASP_TypeDefs all;
  import from IMS_CommonDefs all;
  import from IMS_Component all;
  import from IMS_ASP_TypeDefs all;
  import from IMS_ASP_Templates all;
  import from IMS_CommonParameters all;
  import from IMS_SIP_Templates all;
  import from IMS_MessageBody_Templates all;
  import from IMS_SDP_Templates all;
  import from IMS_SDP_Messages all;
  import from IMS_SDP_MessagesVideo all;
  import from IMS_CC_CommonFunctions all;
  import from IMS_Procedures_CallControl all;
  import from IMS_Procedures_Video all;
  import from IMS_PTC_CoordMsg all;
  import from IMS_CC_IPCAN_Coordination all;
  import from UpperTesterFunctions  all;
  import from IMS_CommonFunctions all;
  import from IMS_Procedures_Common all;
  
  /****************************************************************************/

  function f_IMS_MOCall_ReleaseVideo_ReceiveInvite(charstring p_FmtAudio,
                                                   charstring p_FmtVideo) runs on IMS_PTC return IMS_InviteRequestWithSdp_Type
  {
    var IMS_DATA_REQ v_IMS_INVITE_REQ;
    var SDP_Message v_SDP_Message;
    var boolean v_A11 := pc_IMS_Video_FeatureTag;
    var boolean v_A12 := false;   // as we have video but not voice

    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Audio := {
      cr_SDP_Attribute_rtpmap(p_FmtAudio, cr_RTPMAP_AMR_8000),
      cr_SDP_Attribute_fmtp(p_FmtAudio)
    };
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Video := {
      cr_SDP_Attribute_rtpmap(p_FmtVideo, cr_RTPMAP_H264_90000),
      cr_SDP_Attribute_fmtp(p_FmtVideo)
    };
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Audio := cr_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_optional);
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Video := cr_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_optional, c_mandatory);

    var template (present) SDP_Message v_SDP_MessageTemplate := f_IMS_BuildSDP_AudioVideo_RX(cr_SDP_Time_Any,
                                                                                             cr_SDP_Media_Audio,
                                                                                             v_SDP_MediaAttributes_Audio,
                                                                                             v_SDP_PrecondionAttributes_Audio,
                                                                                             cr_SDP_Media(c_video, 0, c_rtpAvp, cr_SDP_FmtList_AtLeastOne(p_FmtVideo)),  /* port number == 0 => release video stream */
                                                                                             v_SDP_MediaAttributes_Video,
                                                                                             v_SDP_PrecondionAttributes_Video);

    v_IMS_INVITE_REQ := f_IMS_INVITE_ReceiveRequest(A_2_1_A5, tsc_OptionTagList_precondition, -, -, -, -, v_A11, v_A12);
    v_SDP_Message := f_IMS_SIP_SdpMessageBody_DecodeMatchAndCheckSDP(v_IMS_INVITE_REQ.Request.Invite.messageBody, v_SDP_MessageTemplate);

    return f_IMS_InviteRequestWithSdpMO(v_IMS_INVITE_REQ, v_SDP_Message);
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      REFERENCE TS 34.229-1 clause 17.1
   */
  function f_TC_17_1_IMS1() runs on IMS_PTC
  {
    var IMS_InviteRequestWithSdp_Type v_InviteRequestWithSdp;
    var INVITE_Request v_InviteRequest;
    var template (value) IMS_RoutingInfo_Type v_RoutingInfo_DL;
    var MessageHeader v_MessageHeader_Response183;
    var template (value) MessageHeader v_MessageHeader_Response;
    var template (present) MessageHeader v_MessageHeader_Request;
    var SDP_Message v_SDP_LatestOffer;
    var charstring v_FmtAudio;
    var charstring v_FmtVideo;
    var template (present) SipUrl v_CalleeContactUri;
    var charstring v_CallIdentification;

    f_IMS_CC_Preamble(IPCAN_SpeechCall, IMS_REGISTERED);
    
    // setup MO speech call
    v_CallIdentification := f_IMS_CC_StartCall(IPCAN_MO_SpeechCall);
    f_IMS_CC_MO_SpeechCall_Signalling();

    f_IMS_TestBody_Set(true);

    // @siclog "Step 1" siclog@
    f_UT_AddVideoToIMSCall(MMI, v_CallIdentification);

    // @siclog "Step 2" siclog@
    v_InviteRequestWithSdp := f_IMS_MOCallSetup_Video_Step2(AddVideo);

    // @siclog "Step 3 - 4" siclog@
    v_MessageHeader_Response183 := f_IMS_MOCallSetup_Video_Step3_4(AddVideo, v_InviteRequestWithSdp);
    
    f_IMS_IPCAN_StartProcedure(IPCAN, IPCAN_MO_AddVideo);

    // @siclog "Step 5 - 8" siclog@
    v_SDP_LatestOffer := f_IMS_MOCallSetup_Video_Steps5_8(AddVideo, v_InviteRequestWithSdp, v_MessageHeader_Response183, -, PreconditionSupported);  /* @sic R5-160794: PreconditionSupported sic@ */

    // @siclog "Step 9 - 10" siclog@
    f_IMS_MOCallSetup_AnnexC21C25_Step12_13(v_InviteRequestWithSdp);

    f_IMS_IPCAN_WaitForTrigger(IPCAN);    // Wait for trigger from IPCAN to ensure that radio resources have really been established

    // @siclog "Step 10a" siclog@
    f_UT_IMS_RemoveVideoMedia(MMI, v_CallIdentification);

    // @siclog "Step 11" siclog@
    v_FmtAudio := f_SDP_MediaDescr_GetAMR_8000_1(v_InviteRequestWithSdp.SdpOffer.media_list[0]);
    v_FmtVideo := f_SDP_MediaDescr_GetH264_90000(v_InviteRequestWithSdp.SdpOffer.media_list[1]);
    v_InviteRequestWithSdp := f_IMS_MOCall_ReleaseVideo_ReceiveInvite(v_FmtAudio, v_FmtVideo);
    v_InviteRequest := v_InviteRequestWithSdp.Invite;
    v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(v_InviteRequestWithSdp.RoutingInfo);

    // @siclog "Step 12" siclog@
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine100, f_IMS_InviteResponse_100_MessageHeaderTX(v_InviteRequest))));

    // @siclog "Step 12a" siclog@
    f_IMS_IPCAN_StartProcedure(IPCAN, IPCAN_ReleaseVideo);

    // @siclog "Step 13" siclog@
    v_MessageHeader_Response := f_IMS_OtherResponse_200_MessageHeaderTX(v_InviteRequest.msgHeader);
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine200, v_MessageHeader_Response)));
    
    // @siclog "Step 14" siclog@
    v_MessageHeader_Request := f_IMS_AckRequest_MessageHeaderRX(v_InviteRequest, c_statusLine200);
    v_CalleeContactUri := f_SIP_BuildSipUri_RX(px_IMS_CalleeContactUri);
    IMS_Server.receive(car_IMS_Ack_Request(cr_ACK_Request(v_CalleeContactUri, v_MessageHeader_Request)));
    
    f_IMS_IPCAN_WaitForTrigger(IPCAN);    // Wait for trigger from IPCAN to ensure that radio resources have been released

    // @siclog "Step 15 - 16" siclog@
    f_IMS_CallReleaseMO(v_InviteRequest);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_MO_SpeechCall);
  }
  
  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 17.2
   */
  function f_TC_17_2_IMS1() runs on IMS_PTC
  {
    var IMS_InviteRequestWithSdp_Type v_InviteRequestWithSdp;
    var IMS_ResponseWithSdp_Type v_ResponseWithSdp;
    var IMS_Response_Type v_SessionInProgress183;
    var template (value) INVITE_Request v_InviteRequest;
    var template (value)   SDP_Message v_Tx_SDP;
    var template (present) SDP_Message v_Rx_SDP;
    var template (value) MessageHeader v_MessageHeader_Invite;
    var template (value) MessageHeader v_MessageHeader_Update;
    var template (value) MessageHeader v_MessageHeader_Ack;
    var template (present) MessageHeader v_MessageHeader_Response;
    var template (omit) SDP_attribute v_SDP_Attribute;
    var SipUrl v_ContactUrl;
    var charstring v_FmtAudio := tsc_SDP_FmtAudio;
    var charstring v_FmtVideo := tsc_SDP_FmtVideo;
    var InternetProtocol_Type v_Protocol := f_IMS_PTC_ImsInfo_GetTransportProtocol();
    var IMS_DATA_RSP v_IMS_DATA_RSP;
    var IMS_PreconditionIndication_Type v_PreconditionIndication := PreconditionSupported;
    
    // preamble MT speech call setup
    f_IMS_CC_Preamble(IPCAN_SpeechCall, IMS_REGISTERED);
    f_IMS_CC_StartCall(IPCAN_MT_SpeechCall);

    // Steps 1 to 4 as defined in Annex C.11
    v_InviteRequestWithSdp := f_IMS_MTCallSetup_AnnexC11_INVITE();
    v_ResponseWithSdp := f_IMS_MTCallSetup_AnnexC11_Steps1_4(v_InviteRequestWithSdp);
    v_SessionInProgress183 := v_ResponseWithSdp.Response;
    v_ContactUrl := f_MessageHeader_GetContactSipUrl(v_SessionInProgress183.msgHeader);
    // Establish Dedicated EPS bearer
    // See TS. 36.508 EUTRA/EPS signalling for IMS MT speech call Table 4.5A.7.3-1 Step 9-12
    f_IMS_CC_TriggerDedicatedBearerActivation(IPCAN_MT_SpeechCall);  // trigger IPCAN to continue with activation of secondary PDP context or dedicated EPS bearer
    // Steps 5 to 13 as defined in Annex C.11
    f_IMS_MTCallSetup_AnnexC11_Steps5_13(v_InviteRequestWithSdp, v_ResponseWithSdp);

    // MT Call is now established

    f_IMS_TestBody_Set(true);
    
    // @siclog "Step 1" siclog@
    // Step 1 SS sends re-INVITE with second SDP offer to add video.
    v_InviteRequestWithSdp := f_IMS_MTCallSetup_Video_SendInvite(AddVideo, Invite, v_FmtAudio, v_FmtVideo, v_ContactUrl);
    v_InviteRequest := v_InviteRequestWithSdp.Invite;
    
    // @siclog "Step 2: Void" siclog@

    // @siclog "Step 3" siclog@
    v_ResponseWithSdp := f_IMS_MTCallSetup_Video_Receive183(AddVideo, v_InviteRequestWithSdp, v_FmtAudio, v_FmtVideo);
    v_SessionInProgress183 := v_ResponseWithSdp.Response;

    f_IMS_IPCAN_StartProcedure(IPCAN, IPCAN_MT_AddVideo);
    f_IMS_IPCAN_WaitForTrigger(IPCAN);

    // @siclog "Step 4 - 5" siclog@
    f_IMS_MTCallSetup_SendPRACK_ReceiveOK(v_InviteRequest, v_SessionInProgress183.msgHeader);

    // @siclog "Step 6" siclog@
    v_SDP_Attribute := f_SDP_AttributeList_GetAttribute(v_ResponseWithSdp.SdpMessage.media_list[0].attributes, cr_SDP_Attribute_curr_qos(c_local, ?));
    v_Tx_SDP := f_IMS_BuildSDP_MTCallAudioVideo_TX(AddVideo, Update, v_FmtAudio, v_FmtVideo, valueof(v_SDP_Attribute.curr.direction));
    v_MessageHeader_Update := f_IMS_UpdateRequest_MessageHeaderTX(v_InviteRequest, v_PreconditionIndication);                                   /* @sic R5-160795: v_PreconditionIndication sic@ */
    IMS_Client.send(cas_IMS_Update_Request(cs_IMS_RoutingInfo(v_Protocol), cs_UPDATE_Request(v_ContactUrl, v_MessageHeader_Update, cs_MessageBody_SDP(v_Tx_SDP))));
   
    // @siclog "Step 7" siclog@
    v_Rx_SDP := f_IMS_BuildSDP_MTCallAudioVideo_RX(AddVideo, Response200, v_FmtAudio, v_FmtVideo);
    v_MessageHeader_Response := f_IMS_OtherResponse_200_MessageHeaderRX(v_MessageHeader_Update, cr_ContentTypeSDP, v_PreconditionIndication);   /* @sic R5-160795: v_PreconditionIndication sic@ */
    v_IMS_DATA_RSP := f_IMS_ReceiveResponse(v_MessageHeader_Update, v_Protocol, cr_Response(c_statusLine200, v_MessageHeader_Response, cr_MessageBody_SDP));
    f_IMS_SIP_SdpMessageBody_DecodeMatchAndCheckSDP(v_IMS_DATA_RSP.Response.messageBody, v_Rx_SDP);
     
    // @siclog "Step 7A" siclog@
    // Step 7A Make UE accept the speech and video offer.
    f_UT_AnswerCall(MMI);

    // @siclog "Step 8 - 9" siclog@
    // Step 8 The UE responds to the re-INVITE with a 200 OK final response.
    // Step 9 The SS acknowledges the receipt of 200 OK for the re-INVITE.
    f_IMS_MTCallSetup_Common_Steps12_13(v_InviteRequestWithSdp);

    // @siclog "Step 10" siclog@
    // Step 10 SS sends a re-INVITE with a SDP offer indicating that the video component is removed from the call
    v_InviteRequestWithSdp := f_IMS_MTCallSetup_Video_SendInvite(AddVideo, InviteRemoveVideo, v_FmtAudio, v_FmtVideo, v_ContactUrl);

    // @siclog "Step 11" siclog@
    v_MessageHeader_Invite := v_InviteRequestWithSdp.Invite.msgHeader;
    v_Rx_SDP := v_InviteRequestWithSdp.SdpOffer;      /* @sic R5-160795 sic@ !!!! PROSE ISSUE: the INVITE to remove video is sent by the SS => the SDP answer is sent by the UE and cannot be copied from the offer !!!! */
    v_Rx_SDP.origin := cr_SDP_Origin;                 /* @sic R5-160795 sic@ !!!! PROSE ISSUE: o-line is specified as being the SS but the SDP is sent by the UE !!!! */
    v_Rx_SDP.connection := cr_SDP_Connection;         /* @sic R5-160795 sic@ !!!! PROSE ISSUE: c-line does not make sense for UL SDP !!!! */

    v_MessageHeader_Response := f_IMS_OtherResponse_200_MessageHeaderRX(v_MessageHeader_Invite, cr_ContentTypeSDP, v_PreconditionIndication);   /* @sic R5-160795: cr_ContentTypeSDP, v_PreconditionIndication sic@ */
    v_IMS_DATA_RSP := f_IMS_ReceiveResponse(v_MessageHeader_Invite, v_InviteRequestWithSdp.RoutingInfo.Protocol, cr_Response(c_statusLine200, v_MessageHeader_Response, cr_MessageBody_SDP)); /* @sic R5-160795: cr_MessageBody_SDP sic@ */
    f_IMS_SIP_SdpMessageBody_DecodeMatchAndCheckSDP(v_IMS_DATA_RSP.Response.messageBody, v_Rx_SDP);   /* @sic R5-160795 sic@ */

    // @siclog "Step 11a" siclog@
    f_IMS_IPCAN_StartProcedure(IPCAN, IPCAN_ReleaseVideo);
    
    // @siclog "Step 12" siclog@
    v_MessageHeader_Ack := f_IMS_AckRequest_MessageHeaderTX(v_InviteRequestWithSdp.Invite);
    IMS_Client.send(cas_IMS_Ack_Request(cs_IMS_RoutingInfo(v_InviteRequestWithSdp.RoutingInfo.Protocol), cs_ACK_Request(v_ContactUrl, v_MessageHeader_Ack)));

    f_IMS_IPCAN_WaitForTrigger(IPCAN);    // Wait for trigger from IPCAN to ensure that radio resources have been released

    // Step 13 void // @sic R5-134646 sic@
    // Step 14 and 15. SS sends BYE to release the call; UE sends 200 OK for the BYE request and ends the call
    f_IMS_CallReleaseMT(v_InviteRequest, v_ContactUrl);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_MT_SpeechCall);
  }
  
}
