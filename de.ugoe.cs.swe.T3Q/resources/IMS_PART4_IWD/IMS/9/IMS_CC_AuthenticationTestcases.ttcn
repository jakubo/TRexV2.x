/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-11-06 11:15:57 +0100 (Fri, 06 Nov 2015) $
// $Rev: 14687 $
/******************************************************************************/

module IMS_CC_AuthenticationTestcases {

  import from LibSip_Common all;
  import from LibSip_SIPTypesAndValues all;
  import from IP_ASP_TypeDefs all;

  import from IMS_Component all;
  import from IMS_ASP_TypeDefs all;
  import from IMS_ASP_Templates all;
  import from IMS_CommonDefs all;
  import from IMS_SIP_Templates all;
  import from NAS_AuthenticationCommon all;
  import from IMS_CommonFunctions all;
  import from IMS_CommonTemplates all;
  import from IMS_Procedures_Registration all;

  import from IMS_PTC_CoordMsg all;
  import from IMS_CC_CommonFunctions all;


  type record of SecurityClientParams_Type SecurityClientParamsList_Type; /* @status    APPROVED (IMS) */

  //----------------------------------------------------------------------------

  template (present) Authorization cr_Authorization_9_1 :=
  { /* @status    APPROVED (IMS) */
    fieldName := AUTHORIZATION_E,
    body := {
      {
        digestResponse := {
          cr_GenericParam_Common("response", cr_GenValue_None),
          *
        }
      }
    }
  };
  
  template (present) Authorization cr_Authorization_9_2(charstring p_Nonce,
                                                        charstring p_Opaque) :=
  { /* @status    APPROVED (IMS) */
    fieldName := AUTHORIZATION_E,
    body := {
      {
        digestResponse := {
          cr_GenericParam("nonce",    p_Nonce),
          cr_GenericParam("opaque",   p_Opaque),
          cr_GenericParam("response", ?),
          cr_GenericParam("auts",     ?),
          *
        }
      }
    }
  };
  
  //----------------------------------------------------------------------------
  /*
   * @desc      generate an invalid NONCE
   * @param     p_AuthenticationError
   * @return    charstring
   * @status    APPROVED (IMS)
   */
  function fl_GenerateInvalidNonce(AuthenticationError_Type p_AuthenticationError) return charstring
  {
    var Common_AuthenticationParams_Type v_AuthenticationParams;

    v_AuthenticationParams := f_IMS_AuthenticationInit(tsc_IMS_AuthRAND, p_AuthenticationError);
    return f_Bitstring2Base64(v_AuthenticationParams.RandValue & v_AuthenticationParams.AUTN);
  }

  /*
   * @desc      check that spi-c, spi-s and port-c are different than in previous REGISTER requests
   * @param     p_ListOfPreviousSecurityClientParams
   * @param     p_RegisterRequest
   * @return    SecurityClientParamsList_Type
   * @status    APPROVED (IMS)
   */
  function fl_CheckSecurityClientParams(SecurityClientParamsList_Type p_ListOfPreviousSecurityClientParams,
                                        REGISTER_Request p_RegisterRequest) runs on IMS_PTC return SecurityClientParamsList_Type
  {
    var SecurityClientParams_Type v_CurrentSecurityClientParams := f_IMS_Register_GetSecurityClientParams(p_RegisterRequest);
    var SecurityClientParamsList_Type v_ListOfPreviousSecurityClientParams := p_ListOfPreviousSecurityClientParams;
    var integer v_NoOfPreviousSecurityClientParams := lengthof(v_ListOfPreviousSecurityClientParams);
    var SecurityClientParams_Type v_SecurityClientParams;
    var integer i;

    for (i:=0; i < v_NoOfPreviousSecurityClientParams; i:=i+1) {
      v_SecurityClientParams := v_ListOfPreviousSecurityClientParams[i];
      if (v_SecurityClientParams.SPI_us == v_CurrentSecurityClientParams.SPI_us or
          v_SecurityClientParams.SPI_uc == v_CurrentSecurityClientParams.SPI_uc or
          v_SecurityClientParams.Port_uc == v_CurrentSecurityClientParams.Port_uc) {
        f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "spi-c, spi-s and port-c shall be different than in previous REGISTER request");
      }
    }
    v_ListOfPreviousSecurityClientParams[v_NoOfPreviousSecurityClientParams] := v_CurrentSecurityClientParams;
    return v_ListOfPreviousSecurityClientParams;
  }

  /*
   * @desc      receive REGISTER with a specific Authorization (given by p_Authorization)
   * @param     p_CallId
   * @param     p_Authorization
   * @return    IMS_DATA_REQ
   * @status    APPROVED (IMS)
   */
  function f_IMS_REGISTER_RequestWithSpecificAuth(charstring p_CallId,
                                                  template (present) Authorization p_Authorization) runs on IMS_PTC return IMS_DATA_REQ
  {
    var IMS_DATA_REQ v_IMS_DATA_REQ;
    var MessageHeader v_MessageHeader;
    var InternetProtocol_Type v_Protocol;
    var template (present) MessageHeader v_MessageHeader_Template;
    var integer v_CseqValue := f_IMS_PTC_ImsInfo_CseqIncr(register);
    var IP_AddrInfo_Type v_UE_Address := f_IMS_PTC_UE_Address_Get();
    var template (present) SipUrl v_SipUrl := cr_SipUri_HostPort(f_IMS_PTC_ImsInfo_GetHomeDomainName());
    var boolean v_CheckAuthorization;
    var boolean v_Match := false;

    IMS_Server.receive(car_IMS_Register_Request(cr_REGISTER_Request(v_SipUrl))) -> value v_IMS_DATA_REQ;   // receive any REGISTER
    v_MessageHeader := v_IMS_DATA_REQ.Request.Register.msgHeader;
    v_Protocol := v_IMS_DATA_REQ.RoutingInfo.Protocol;
    
    v_MessageHeader_Template := f_IMS_RegisterRequest_MessageHeaderRX_Initial(v_Protocol);         // @sic R5s130369 sic@
    // test case specific message contents:
    v_MessageHeader_Template.cSeq        := cr_CseqDef(v_CseqValue, "REGISTER");
    v_MessageHeader_Template.callId      := cr_CallId(p_CallId);
    v_MessageHeader_Template.securityVerify := omit;
    v_MessageHeader_Template.authorization  := p_Authorization;

    v_Match := match(v_MessageHeader, v_MessageHeader_Template);
    if (not v_Match) {
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "invalid REGISTER Request");
    }
    else {
      v_CheckAuthorization := false;                                           // Authorization needs to be checked separately
      f_IMS_A_1_1_MessageHeader_CommonChecks(v_MessageHeader, v_Protocol, -, v_UE_Address, -, v_CheckAuthorization);
      f_IMS_A_1_1_MessageHeader_CheckPublicUserIdentity(v_MessageHeader);      // check 'To' and 'From' field (needed for initial registration only)
    }
    return v_IMS_DATA_REQ;
  }

  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 9.1
   * @status    APPROVED (IMS)
   */
  function f_TC_9_1_IMS1() runs on IMS_PTC
  { /* @sic R5-153463: step 6 (403 forbidden) replaced by step 6..12 (completition of registration procedure) sic@ */
    var charstring v_InvalidNonce := fl_GenerateInvalidNonce(macError);    // Nonce with invalid MAC
    var IMS_DATA_REQ v_IMS_DATA_REQ;
    var template (value) IMS_RoutingInfo_Type v_RoutingInfo_DL;
    var REGISTER_Request v_RegisterReq;
    var charstring v_CallId;
    var integer i;
    var SecurityClientParamsList_Type v_SecurityClientParamsList;
    var template CommaParam_List v_DigestResponseContainingAuts := {cr_GenericParam("auts", ?), *};

    f_IMS_CC_Preamble(IPCAN_SignallingOnly, IMS_NULL, IMS_Security);
    f_IMS_CC_StartSignalling(IPCAN_InitialRegistration);

    f_IMS_TestBody_Set(true);

    // Step 1
    // @siclog "Step 1" siclog@
    v_IMS_DATA_REQ := f_IMS_REGISTER_InitialRequest();
    v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(v_IMS_DATA_REQ.RoutingInfo);
    v_RegisterReq := v_IMS_DATA_REQ.Request.Register;
    v_CallId := v_RegisterReq.msgHeader.callId.callid;

    v_SecurityClientParamsList[0] := f_IMS_Register_GetSecurityClientParams(v_RegisterReq);  /* @sic R5-144754 sic@ */

    for (i:=0; i<2; i:=i+1) {

      f_IMS_Register_SecurityInit(v_RegisterReq, -, false);   // @sic R5s120947, R5s150873 sic@

      // @siclog "Step 2/4" siclog@
      IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine401, f_IMS_RegisterResponse_401_MessageHeaderTX(v_RegisterReq, v_InvalidNonce))));
      
      // Step 3/5
      // @siclog "Step 3/5" siclog@
      v_IMS_DATA_REQ := f_IMS_REGISTER_RequestWithSpecificAuth(v_CallId, cr_Authorization_9_1);
      v_RegisterReq := v_IMS_DATA_REQ.Request.Register;
      v_SecurityClientParamsList := fl_CheckSecurityClientParams(v_SecurityClientParamsList, v_RegisterReq);  /* @sic R5-144754 sic@ */

      // Check that digestResponse does not contain (non-empty) "auts" parameter
      if (match(v_RegisterReq.msgHeader.authorization.body[0].digestResponse, v_DigestResponseContainingAuts)) {
        f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "");
      } else {
        f_IMS_PreliminaryPass(__FILE__, __LINE__, "");
      }
    }
 
    // @siclog "Step 6" siclog@
    f_IMS_Register_SecurityInit(v_RegisterReq);
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine401, f_IMS_RegisterResponse_401_MessageHeaderTX(v_RegisterReq))));
    
    // @siclog "Step 7" siclog@
    v_IMS_DATA_REQ := f_IMS_REGISTER_SubsequentRequest();
    v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(v_IMS_DATA_REQ.RoutingInfo);   /* security has changed to "protected" now */
    v_RegisterReq := v_IMS_DATA_REQ.Request.Register;
    
    // @siclog "Step 8" siclog@
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine200, f_IMS_RegisterResponse_200_MessageHeaderTX(v_RegisterReq))));
    
    // @siclog "Step 9 - 12" siclog@
    f_IMS_Register_SubscribeNotify(v_RegisterReq);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_InitialRegistration);
  }

  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 9.2
   * @status    APPROVED (IMS)
   */
  function f_TC_9_2_IMS1() runs on IMS_PTC
  {
    var charstring v_InvalidNonce := fl_GenerateInvalidNonce(sqnFailure);
    var IMS_DATA_REQ v_IMS_DATA_REQ;
    var SecurityClientParamsList_Type v_SecurityClientParamsList;
    var template (value) IMS_RoutingInfo_Type v_RoutingInfo_DL;
    var REGISTER_Request v_RegisterReq;
    var charstring v_CallId;
    
    f_IMS_CC_Preamble(IPCAN_SignallingOnly, IMS_NULL, IMS_Security);
    f_IMS_CC_StartSignalling(IPCAN_InitialRegistration);
    
    f_IMS_TestBody_Set(true);
    
    // Step 1
    // @siclog "Step 1" siclog@
    v_IMS_DATA_REQ := f_IMS_REGISTER_InitialRequest();
    v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(v_IMS_DATA_REQ.RoutingInfo);
    v_RegisterReq := v_IMS_DATA_REQ.Request.Register;
    v_CallId := v_RegisterReq.msgHeader.callId.callid;
    v_SecurityClientParamsList[0] := f_IMS_Register_GetSecurityClientParams(v_RegisterReq);  /* @sic R5-144754 sic@ */
    
    // Step 2
    // @siclog "Step 2" siclog@
    f_IMS_Register_SecurityInit(v_RegisterReq, -, false);   /* @sic R5s130264 Change 2.1, R5s150873 MCC160 implementation sic@ */
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine401, f_IMS_RegisterResponse_401_MessageHeaderTX(v_RegisterReq, v_InvalidNonce))));
    
    // Step 3
    // @siclog "Step 3" siclog@
    v_IMS_DATA_REQ := f_IMS_REGISTER_RequestWithSpecificAuth(v_CallId, cr_Authorization_9_2(v_InvalidNonce, tsc_IMS_Opaque));
    v_RegisterReq := v_IMS_DATA_REQ.Request.Register;
    v_SecurityClientParamsList := fl_CheckSecurityClientParams(v_SecurityClientParamsList, v_RegisterReq);  /* @sic R5-144754 sic@ */
    
    // Step 4
    // @siclog "Step 4" siclog@
    f_IMS_Register_SecurityInit(v_RegisterReq);       /* @sic R5s130573: IPSec layer needs to be updated acc. to new Security client header sent by the UE sic@ */
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine401, f_IMS_RegisterResponse_401_MessageHeaderTX(v_RegisterReq))));
    
    // Step 5
    // @siclog "Step 5" siclog@
    v_IMS_DATA_REQ := f_IMS_REGISTER_SubsequentRequest();
    v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(v_IMS_DATA_REQ.RoutingInfo);   /* @sic R5s130264 Change 2.2: security has changed to "protected" now sic@ */
    v_RegisterReq := v_IMS_DATA_REQ.Request.Register;
    
    // Step 6
    // @siclog "Step 6" siclog@
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine200, f_IMS_RegisterResponse_200_MessageHeaderTX(v_RegisterReq))));
    
    f_IMS_Register_SubscribeNotify(v_RegisterReq);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_InitialRegistration);
  }
}
