/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-04 21:03:50 +0100 (Fri, 04 Mar 2016) $
// $Rev: 15513 $
/******************************************************************************/

module HTTP_CommonFunctions {

  import from CommonDefs all;
  import from IP_ASP_TypeDefs all;
  import from HTTP_ASP_TypeDefs all;
  import from HTTP_CommonTemplates all;

  //============================================================================
  // Configuration
  //----------------------------------------------------------------------------

  template (value) HttpServerInfo_Type cs_HttpServerInfo(IP_AddrInfo_Type p_ServerAddr,
                                                         PortNumber_Type p_PortNumber,
                                                         IP_DrbInfo_Type p_DrbInfo,
                                                         template (omit) TLSInfo_Type p_TlsInfo := omit) :=
  { /* @status    APPROVED (IMS) */
    serverAddr := p_ServerAddr,
    serverPort := p_PortNumber,
    drbInfo := p_DrbInfo,
    tlsInfo := p_TlsInfo
  };

  template (value) HTTP_CTRL_REQ cs_HttpCtlReq(template (value) HttpServerList_Type p_HttpServerList) :=
  { /* @status    APPROVED (IMS) */
    httpServerList := p_HttpServerList
  };
  
  template (present) HTTP_CTRL_CNF cr_HttpCtlCnf := { errorInfo := omit }; /* @status    APPROVED (IMS) */


  //----------------------------------------------------------------------------
  /*
   * @desc      Configure the http server
   * @param     p_HttpCtrlPort
   * @param     p_HttpServerAddrAndPort_List
   * @param     p_BsfServerAddrAndPort_List
   * @param     p_DrbRoutingInfo
   * @param     p_TLSInfoForHttpServer (default value: omit)
   * @status    APPROVED (IMS)
   */
  function f_HTTP_Configure(HTTP_CTRL_PORT p_HttpCtrlPort,
                            IP_SocketList_Type p_HttpServerAddrAndPort_List,
                            IP_SocketList_Type p_BsfServerAddrAndPort_List,
                            IP_DrbInfo_Type p_DrbRoutingInfo,
                            template (omit) TLSInfo_Type p_TLSInfoForHttpServer := omit)
  { /* @sic R5-151795: sic@
     * - HTTP implementation does not distinguish servers for XCAP or BSF but there can be one or more than one HTTP servers:
     *   responses sent by the HTTP implementation are sent back using the same connection as the corresponding request */
    /* @sic R5-160765: GBA for ProSE - p_TLSInfoForHttpServer sic@ */
    var template (value) HttpServerList_Type v_HttpServerList;
    var integer i;
    var integer k := 0;

    for (i := 0; i < lengthof(p_HttpServerAddrAndPort_List); i := i + 1) {   // @sic R5s150148 sic@
      v_HttpServerList[k] := cs_HttpServerInfo(p_HttpServerAddrAndPort_List[i].IpAddr,
                                               p_HttpServerAddrAndPort_List[i].Port,
                                               p_DrbRoutingInfo,
                                               p_TLSInfoForHttpServer);
      k := k + 1;
    }
    for (i := 0; i < lengthof(p_BsfServerAddrAndPort_List); i := i + 1) {   // @sic R5s150148 sic@
      v_HttpServerList[k] := cs_HttpServerInfo(p_BsfServerAddrAndPort_List[i].IpAddr,
                                               p_BsfServerAddrAndPort_List[i].Port,
                                               p_DrbRoutingInfo);
      k := k + 1;
    }
    p_HttpCtrlPort.send(cs_HttpCtlReq(v_HttpServerList));
    p_HttpCtrlPort.receive(cr_HttpCtlCnf)
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Receive Http Request from UE
   * @param     p_Port
   * @param     p_HttpRequest
   * @param     p_Watchdog
   * @param     p_InactivityDuration (default value: 9999.0)
   * @return    template (omit) HTTP_DATA_IND
   * @status    APPROVED (IMS)
   */
  function f_HTTP_ReceiveRequest(HTTP_DATA_PORT p_Port,
                                 template (present) HttpRequest_Type p_HttpRequest,
                                 timer p_Watchdog,
                                 float p_InactivityDuration := 9999.0) return template (omit) HTTP_DATA_IND
  {
    var HTTP_DATA_IND v_DataInd;
    var template (omit) HTTP_DATA_IND v_HTTP_DATA_IND := omit;
    var boolean v_TimerIsRunning := p_Watchdog.running;
    timer t_InactivityTimer;

    t_InactivityTimer.start(p_InactivityDuration);
      
    alt {
      [] p_Port.receive(cr_HttpDataInd(?, p_HttpRequest)) -> value v_DataInd
        {
          v_HTTP_DATA_IND := v_DataInd;
        }
      [] p_Port.receive(cr_HttpDataInd(?, ?))
        {
          f_SetVerdictInconc(__FILE__, __LINE__, "unexpected HTTP request"); /* @sic R5s150945 change 10 sic@ */
        }
      [] t_InactivityTimer.timeout {}
      [v_TimerIsRunning] p_Watchdog.timeout {}
    }
    t_InactivityTimer.stop;
    return v_HTTP_DATA_IND;
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      Send Http Response to UE
   * @param     p_Port
   * @param     p_HttpRoutingInfo
   * @param     p_HttpResponse
   * @status    APPROVED (IMS)
   */
  function f_HTTP_SendResponse(HTTP_DATA_PORT p_Port,
                               template (value) IP_Connection_Type p_HttpRoutingInfo,
                               template (value) HttpResponse_Type p_HttpResponse)
  {
    p_Port.send(cs_HttpDataReq(p_HttpRoutingInfo, p_HttpResponse));
  }
}
