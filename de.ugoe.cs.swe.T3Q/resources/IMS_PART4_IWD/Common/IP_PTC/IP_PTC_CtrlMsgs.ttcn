/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:03:08 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15515 $
/******************************************************************************/


module IP_PTC_CtrlMsgs {
  /* definition of coordination messages to be used by EUTRA_PTC/UTRAN_PTC/GERAN_PTC do configure
     routing table for the IP data */

  import from CommonDefs all;
  import from IP_ASP_TypeDefs all;
  import from IP_PTC_Templates all;

  //============================================================================

  type record IP_Start_Type {           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    PDN_Index_Type      PdnId,
    IP_DrbInfo_Type     DrbInfo
  };

  type union IP_Stop_Type {             /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    Null_Type           All,
    PDN_Index_Type      PdnId
  };

  type record IP_ChangeDrbInfo_Type {   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    PDN_Index_Type      PdnId,
    IP_DrbInfo_Type     DrbInfo
  };

  type record IP_SendRouterAdvertisement_Type {   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    PDN_Index_Type      PdnId
  };

  type union IP_ExplicitRouting_Type {   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IP_RoutingInfo_Type Add,
    IP_Connection_Type  Remove
  };

  //============================================================================

  type union IP_ConfigReq_Type {        /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IP_Start_Type                       StartIP,
    IP_Stop_Type                        StopIP,           /* @sic R5s131000: Null_Type replaced by IP_Stop_Type sic@ */
    Null_Type                           GeranCnf,         /* e.g. to confirm GeranTrigger */
    IP_ChangeDrbInfo_Type               ChangeDrbInfo,    /* @sic R5s150648 Change 5.1 sic@ */
    EUTRA_CellId_Type                   ChangeEutraCell,  /* @sic R5s150648 Change 5.1 sic@ */
    IP_SendRouterAdvertisement_Type     SendRouterAdvertisement,
    IP_ExplicitRouting_Type             ExplicitRouting,  /* @sic R5s130681 change 3: handling of RTP data; R5s160131: Start/Stop sic@ */
    Null_Type                           ChangeToRawmode   /* @sic R5s150074 - Additional change sic@ */
  };

  type record IP_GeranTrigger_Type {
    integer CellId
  };

  type union IP_Indication_Type {       /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN)
                                           @sic R5-133612: trigger to GERAN sic@ */
    Null_Type                   Cnf,
    IP_DrbInfo_Type             GeranTrigger
  };


  //============================================================================

  type port IP_RAT_CTRL_PORT message {         /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    out IP_ConfigReq_Type;
    in  IP_Indication_Type;
  };

  type port IP_PTC_CTRL_PORT message {         /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
    in  IP_ConfigReq_Type;
    out IP_Indication_Type;
  };

  //****************************************************************************
  // EUTRA/UTRAN/GERAN side
  //----------------------------------------------------------------------------
  // Commands:

  template (value) IP_ConfigReq_Type cs_IP_StartReq(PDN_Index_Type p_PdnId,
                                                    template (value) IP_DrbInfo_Type p_DrbInfo) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
    StartIP := {
      PdnId := p_PdnId,
      DrbInfo := p_DrbInfo
    }
  };

  template (value) IP_ConfigReq_Type cs_IP_ChangeToRawmode := {ChangeToRawmode := true};      /* @status    APPROVED (LTE, LTE_A_R10_R11, LTE_IRAT) */

  template (value) IP_Stop_Type cs_IP_StopAll := {All := true};                               /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
  template (value) IP_Stop_Type cs_IP_StopPDN(PDN_Index_Type p_PdnId) := {PdnId := p_PdnId};  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  template (value) IP_ConfigReq_Type cs_IP_StopReq(template (value) IP_Stop_Type p_IP_Stop) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
    /* @sic R5s131000: new parameter p_IP_Stop sic@ */
    StopIP := p_IP_Stop
  };

  template (value) IP_ConfigReq_Type cs_IP_GeranCnf :=
  { /* @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT) */
    GeranCnf := true
  };

  template (value) IP_ConfigReq_Type cs_IP_ChangeEutraCellReq(EUTRA_CellId_Type p_EutraCellId) :=
  { /* @sic R5-125676: new parameter p_DrbId sic@ */
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    ChangeEutraCell := p_EutraCellId  /* @sic R5s150648 Change 5.2 sic@ */
  };

  template (value) IP_ConfigReq_Type cs_IP_ChangeDrbInfoReq(PDN_Index_Type p_PdnId,
                                                            template (value) IP_DrbInfo_Type p_DrbInfo) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    ChangeDrbInfo := {
      PdnId := p_PdnId,
      DrbInfo := p_DrbInfo
    }
  };

  template (value) IP_ConfigReq_Type cs_IP_SendRouterAdvertisementReq(PDN_Index_Type p_PdnId) :=
  {
    SendRouterAdvertisement := {
      PdnId := p_PdnId
    }
  };

  template (value) IP_ConfigReq_Type cs_IP_ConfigureDiscardDataLoopbackDataReq(InternetProtocol_Type p_Protocol,
                                                                               PortNumber_Type  p_MediaPort,
                                                                               template (omit) IP_AddrInfo_Type p_IpAddrRemoteUE,
                                                                               template (value) IP_DrbInfo_Type p_DrbInfo,
                                                                               IP_DataMode_Type p_DataMode) :=
  { /* NOTE: p_IpAddrRemoteUE the IP address of the simulated remote UE (i.e. a 'local' (SS) address in terms of the routing table) */
    /* @sic R5-153746: p_DataMode (discard, loopbackRTP, loopbackRTCP) sic@ */
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
    ExplicitRouting := {
      Add := {  /* @sic R5s160131 sic@ */
        IpInfo := cs_IP_ConnectionId(p_Protocol, cs_IP_Socket(p_IpAddrRemoteUE, p_MediaPort)),
        DRB := p_DrbInfo,
        DataMode := p_DataMode  // @sic R5-150356, R5w150008; R5-153746 sic@
      }
    }
  };
                                                               
  template (value) IP_ConfigReq_Type cs_IP_StopDiscardDataLoopbackDataReq(InternetProtocol_Type p_Protocol,
                                                                          PortNumber_Type  p_MediaPort,
                                                                          template (omit) IP_AddrInfo_Type p_IpAddrRemoteUE) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
    ExplicitRouting := {
      Remove := cs_IP_ConnectionId(p_Protocol, cs_IP_Socket(p_IpAddrRemoteUE, p_MediaPort))
    }
  };

  template (present) IP_Indication_Type cr_IP_CommonCnf := {Cnf := true}; /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
  template (present) IP_Indication_Type cr_IP_GeranTrigger :=
  { /* @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT) */
    GeranTrigger := {
      Geran := ?
    }
  };

  //****************************************************************************
  // IP PTC side
  //----------------------------------------------------------------------------

  template (present) IP_ConfigReq_Type cr_IP_CtrlAnyReq := ?;   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
  template (value) IP_Indication_Type cs_IP_CommonCnf := {Cnf := true};   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */

  template (value) IP_Indication_Type cs_IP_GeranTrigger(template (value) IP_DrbInfo_Type p_DrbInfo) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
    GeranTrigger := p_DrbInfo
  };

  //****************************************************************************
  // templates for DRB info
  //----------------------------------------------------------------------------

  template (value) IP_DrbInfo_Type cs_DrbInfo_EUTRA(EUTRA_CellId_Type p_CellId,
                                                    IP_DrbId_Type p_DrbId) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    /* @sic R5w110204: change of IP_EUTRA_DrbInfo_Type sic@ */
    Eutra := {
      CellId := p_CellId,
      DrbId := p_DrbId
    }
  };

  template (value) IP_DrbInfo_Type cs_DrbInfo_UTRAN(UTRAN_CellId_Type p_CellId,
                                                    IP_DrbId_Type p_DrbId) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT, POS, UTRAN) */
    Utran := {
      CellId := f_UtranCellId2Int(p_CellId),
      DrbId := p_DrbId
    }
  };

  template (value) IP_DrbInfo_Type cs_DrbInfo_GERAN(integer p_CellId,
                                                    IP_DrbId_Type p_DrbId) :=
  { /* @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT) */
    Geran := {
      CellId := p_CellId,
      DrbId := p_DrbId
    }
  };

  //****************************************************************************
  // Functions to be called from the test cases
  //----------------------------------------------------------------------------
  /*
   * @desc      generic function to configure IP
   * @param     p_Port
   * @param     p_IP_ConfigReq
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function fl_IP_Config(IP_RAT_CTRL_PORT p_Port,
                        template (value) IP_ConfigReq_Type p_IP_ConfigReq)
  {
    p_Port.send(p_IP_ConfigReq);
    p_Port.receive(cr_IP_CommonCnf);
  }
  
  /*
   * @desc      RAT independent function to start DHCP and ICMPv6 server for given PDN
   * @param     p_Port
   * @param     p_PdnIndex         .. index of the PDN
   * @param     p_DrbInfo          .. DRB on which user plane signalling happens for this PDN
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_IP_Handling_Start(IP_RAT_CTRL_PORT p_Port,
                               PDN_Index_Type p_PdnIndex,
                               template (value) IP_DrbInfo_Type p_DrbInfo)
  { /* @sic R5-113734 change 8: replacing former f_DHCPv4_Start, f_ICMPv6_Start sic@ */
    fl_IP_Config(p_Port, cs_IP_StartReq(p_PdnIndex, p_DrbInfo));
  }

  /*
   * @desc      RAT independent function to stop all DHCP and ICMPv6 servers
   * @param     p_Port
   * @param     p_IP_Stop          (default value: cs_IP_StopAll)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_IP_Handling_Stop(IP_RAT_CTRL_PORT p_Port,
                              template (value) IP_Stop_Type p_IP_Stop := cs_IP_StopAll)
  { /* @sic R5-113734 change 8: replacing former f_DHCPv4_Stop, f_ICMPv6_Stop sic@ */
    /* @sic R5s131000: new parameter p_IP_Stop sic@ */
    fl_IP_Config(p_Port, cs_IP_StopReq(p_IP_Stop));
  }

  /*
   * @desc      to remove all entries in the routing table
   *            NOTE: this has no impact on any existing sockets
   * @param     p_Port
   * @status    APPROVED (LTE, LTE_A_R10_R11, LTE_IRAT)
   */
  function f_IP_Routing_ChangeToRawmode(IP_RAT_CTRL_PORT p_Port)
  {
    fl_IP_Config(p_Port, cs_IP_ChangeToRawmode);
  }

  /*
   * @desc      Function to change the EUTRA cell for all entries of the routing table
   * @param     p_Port
   * @param     p_EutraCellId      .. new cell
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IP_ChangeEutraCell(IP_RAT_CTRL_PORT p_Port,
                                EUTRA_CellId_Type p_EutraCellId)
  { /* @sic R5-125676: new parameter p_DrbId sic@ */
    /* @sic R5s150648 Change 5.3: p_DrbId removed sic@ */
    fl_IP_Config(p_Port, cs_IP_ChangeEutraCellReq(p_EutraCellId));
  }

  /*
   * @desc      change DRB mapping for given PDN
   * @param     p_Port
   * @param     p_DrbInfo
   * @param     p_PdnIndex          (default value: PDN_1)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IP_ChangeDrbMapping(IP_RAT_CTRL_PORT p_Port,
                                 template (value) IP_DrbInfo_Type p_DrbInfo,
                                 PDN_Index_Type p_PdnIndex := PDN_1)
  {
    fl_IP_Config(p_Port, cs_IP_ChangeDrbInfoReq(p_PdnIndex, p_DrbInfo));
  }

  /*
   * @desc      EUTRA: change DRB mapping for given PDN
   * @param     p_Port
   * @param     p_EutraCellId
   * @param     p_DrbId
   * @param     p_PdnIndex          (default value: PDN_1)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IP_ChangeDrbMappingEUTRA(IP_RAT_CTRL_PORT p_Port,
                                      EUTRA_CellId_Type p_EutraCellId,
                                      IP_DrbId_Type p_DrbId,
                                      PDN_Index_Type p_PdnIndex := PDN_1)
  { /* @sic R5s150648 Change 5.3 sic@ */
    f_IP_ChangeDrbMapping(p_Port, cs_DrbInfo_EUTRA(p_EutraCellId, p_DrbId), p_PdnIndex);
  }

  /*
   * @desc      Function to change the UTRAN cell for all entries of the routing table
   * @param     p_Port
   * @param     p_UtranCellId
   * @param     p_DrbId
   * @status    APPROVED (LTE_A_IRAT, LTE_IRAT)
   */
  function f_IP_ChangeDrbMappingUTRAN(IP_RAT_CTRL_PORT p_Port,
                                      UTRAN_CellId_Type p_UtranCellId,
                                      IP_DrbId_Type p_DrbId)
  { /* @sic R5s150648 Change 5.3 sic@ */
    f_IP_ChangeDrbMapping(p_Port, cs_DrbInfo_UTRAN(p_UtranCellId, p_DrbId));
  }

  /*
   * @desc      Function to change the GERAN cell for all entries of the routing table
   * @param     p_Port
   * @param     p_GeranCellId
   * @param     p_DrbId
   */
  function f_IP_ChangeDrbMappingGERAN(IP_RAT_CTRL_PORT p_Port,
                                      integer p_GeranCellId,
                                      IP_DrbId_Type p_DrbId)
  {
    f_IP_ChangeDrbMapping(p_Port, cs_DrbInfo_GERAN(p_GeranCellId, p_DrbId));
  }

  /*
   * @desc      Function to trigger the IP PTC to sent out an (unsolicited) router advertisement
   * @param     p_Port
   * @param     PDN_Index_Type     .. index of the PDN
   * @status
   */
  function f_IP_RouterAdvertisement(IP_RAT_CTRL_PORT p_Port,
                                    PDN_Index_Type p_PdnIndex)
  {
    fl_IP_Config(p_Port, cs_IP_SendRouterAdvertisementReq(p_PdnIndex));
  }

  /*
   * @desc      start loopback of RTP/RTCP packets
   * @param     p_Port
   * @param     p_MediaPort
   * @param     p_IpAddrRemoteUE
   * @param     p_DrbInfo
   * @param     p_DataMode
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IP_ConfigureDiscardDataLoopbackData_EUTRA(IP_RAT_CTRL_PORT p_Port,
                                                       PortNumber_Type  p_MediaPort,
                                                       template (omit) IP_AddrInfo_Type p_IpAddrRemoteUE,
                                                       template (value) IP_DrbInfo_Type p_DrbInfo,
                                                       IP_DataMode_Type p_DataMode)
  { /* @sic R5-153746: p_DataMode (discard, loopbackRTP, loopbackRTCP) sic@ */
    /* @sic R5s160131: p_DrbInfo, use of fl_IP_Config sic@ */
    fl_IP_Config(p_Port, cs_IP_ConfigureDiscardDataLoopbackDataReq(udp, p_MediaPort, p_IpAddrRemoteUE, p_DrbInfo, p_DataMode));
  }

  /*
   * @desc      stop loopback of RTP/RTCP packets
   * @param     p_Port
   * @param     p_MediaPort
   * @param     p_IpAddrRemoteUE
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IP_StopDiscardDataLoopbackData_EUTRA(IP_RAT_CTRL_PORT p_Port,
                                                  PortNumber_Type  p_MediaPort,
                                                  template (omit) IP_AddrInfo_Type p_IpAddrRemoteUE)
  {
    fl_IP_Config(p_Port, cs_IP_StopDiscardDataLoopbackDataReq(udp, p_MediaPort, p_IpAddrRemoteUE));
  }

}
