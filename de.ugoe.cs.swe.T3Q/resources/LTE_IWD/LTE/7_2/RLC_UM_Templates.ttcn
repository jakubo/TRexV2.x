/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2013-07-01 02:17:54 +0200 (Mon, 01 Jul 2013) $
// $Rev: 9064 $
/******************************************************************************/

module RLC_UM_Templates
{
  import from EUTRA_ASP_TypeDefs all;
  import from EUTRA_ASP_DrbDefs all;
  import from EUTRA_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from EUTRA_DRB_Templates all;
  import from RLC_Common all;
  import from L2_CommonTemplates all;
  import from EUTRA_AspCommon_Templates all;
  
  /* Declarations */
  
  group RLCUMTemplates {
  
  template (value) DL_UM_RLC cds_RX_UM_RLC_DRB_SN10_tReord80ms modifies cs_508_DRB_DL_UM_RLC :=
  { /* @status    (APPROVED) */
    t_Reordering := ms80
  };
  
  template (value) DL_UM_RLC cds_RX_UM_RLC_DRB_SN10_tReord100ms modifies cs_508_DRB_DL_UM_RLC :=
  { /* @status    APPROVED (LTE) */
    t_Reordering := ms100
  };
  
  template (value) DL_UM_RLC cds_RX_UM_RLC_DRB_SN10_tReord200ms modifies cs_508_DRB_DL_UM_RLC :=
  { /* @status    APPROVED (LTE) */
    t_Reordering := ms200
  };

  //=============================================================================
  
  template (value) RLC_UMD_PDU_Type cs_UMD_PDU_NoLIs_SN5(integer p_SN,
                                                         RLC_FramingInfo_Type p_FramingInfo,
                                                         RLC_DataFieldList_Type p_Data) :=
  { /* @status    APPROVED (LTE) */
    ShortSN := {
      Header := {
        FixPart := {
          FramingInfo :=  p_FramingInfo,
          Extension := tsc_E_Data,
          SequenceNumber := int2bit (p_SN, tsc_UM_SN_Size_5)
        },
        FlexPart := omit
      },
      Data := p_Data
    }
  };
  
  template (value) RLC_UMD_PDU_Type cs_UMD_PDUoneSDU_SN5(integer p_SN,
                                                         RLC_DataFieldList_Type p_RLC_SduData) :=
  /* serves to transmit one SDU in one UMD PDU */
  /* @status    APPROVED (LTE) */
    cs_UMD_PDU_NoLIs_SN5(p_SN, tsc_FI_FullSDU, p_RLC_SduData);
  
  //=============================================================================
  //PDU Constraint
  
  template (present) RLC_UMD_PDU_Type cr_UMD_PDUoneSDU_SN10(integer p_SN,
                                                            RLC_DataFieldList_Type p_RLC_SduData) :=
  /* serves to receive one SDU in one UMD PDU */
  /* @status    APPROVED (LTE) */
    cr_UMD_PDU_SN10 (p_SN, tsc_FI_FullSDU, p_RLC_SduData);
  
  template (present) RLC_UMD_PDU_Type cr_UMD_PDU_LIs_SN10 (integer p_SN,
                                                           template RLC_FramingInfo_Type p_FramingInfo,
                                                           template RLC_PDU_Header_FlexPart_Type p_Header_FlexPart,
                                                           template RLC_DataFieldList_Type p_Data) :=
  { /* @status    APPROVED (LTE) */
    LongSN := {
    Header := {
      FixPart := {
        Reserved := tsc_B3_Reserved,
        FramingInfo :=  p_FramingInfo,
        Extension := tsc_E_BitAndLI,
        SequenceNumber := int2bit (p_SN, tsc_UM_SN_Size_10)
      },
      FlexPart := p_Header_FlexPart
    },
    Data := p_Data
    }
  };
  
  template (present) RLC_UMD_PDU_Type cr_UMD_PDU_SN5(integer p_SN,
                                                     template RLC_FramingInfo_Type p_FI,
                                                     template RLC_DataFieldList_Type p_RLC_SduData) :=
  { /* @status    APPROVED (LTE) */
    ShortSN := {
      Header := {
        FixPart := {
          FramingInfo :=  p_FI,
          Extension := tsc_E_Data,
          SequenceNumber := int2bit (p_SN, tsc_UM_SN_Size_5)
        },
        FlexPart := omit
      },
      Data := p_RLC_SduData
    }
  };
  
  template (present) RLC_UMD_PDU_Type cr_UMD_PDUoneSDU_SN5(integer p_SN,
                                                           RLC_DataFieldList_Type p_RLC_SduData) :=
  /* @status    APPROVED (LTE) */
    cr_UMD_PDU_SN5(p_SN, tsc_FI_FullSDU, p_RLC_SduData);
  
  template (present) RLC_UMD_PDU_Type cr_UMD_PDU_LIs_SN5 (integer p_SN,
                                                          template RLC_FramingInfo_Type p_FramingInfo,
                                                          template RLC_PDU_Header_FlexPart_Type p_Header_FlexPart,
                                                          template RLC_DataFieldList_Type p_Data) :=
  { /* @status    APPROVED (LTE) */
    ShortSN := {
      Header := {
        FixPart := {
          FramingInfo :=  p_FramingInfo,
          Extension := tsc_E_BitAndLI,
          SequenceNumber := int2bit (p_SN, tsc_UM_SN_Size_5)
        },
        FlexPart := p_Header_FlexPart
      },
      Data := p_Data
    }
  };
  
  template (value) RLC_UMD_PDU_Type cs_UMD_PDU_LIs_SN10 (integer p_SN,
                                                         RLC_FramingInfo_Type p_FramingInfo,
                                                         template (omit) RLC_PDU_Header_FlexPart_Type p_Header_FlexPart,
                                                         RLC_DataFieldList_Type p_Data) :=
  { /* serves to transmit one UMD PDU with Length Indicators */
    /* @status    APPROVED (LTE) */
    LongSN := {
      Header := {
        FixPart := {
          Reserved := tsc_B3_Reserved,
          FramingInfo :=  p_FramingInfo,
          Extension := tsc_E_BitAndLI,
          SequenceNumber := int2bit (p_SN, tsc_UM_SN_Size_10)
        },
        FlexPart := p_Header_FlexPart
      },
      Data := p_Data
    }
  };
  
  template (value) RLC_UMD_PDU_Type cs_UMD_PDU_LIs_SN5 (integer p_SN,
                                                        RLC_FramingInfo_Type p_FramingInfo,
                                                        template (omit) RLC_PDU_Header_FlexPart_Type p_Header_FlexPart,
                                                        RLC_DataFieldList_Type p_Data) :=
  { /* @status    APPROVED (LTE) */
    ShortSN := {
      Header := {
        FixPart := {
          FramingInfo :=  p_FramingInfo,
          Extension := tsc_E_BitAndLI,
          SequenceNumber := int2bit (p_SN, tsc_UM_SN_Size_5)
        },
        FlexPart := p_Header_FlexPart
      },
      Data := p_Data
    }
  };
  
  template (value) RLC_Configuration_Type cs_RLC_Configuration_UM_SN5 :=
  { // @sic R5s100076 sic@
    /* @status    APPROVED (LTE) */
    Rb := {
      UM := {
        Tx := cs_UL_UM_RLC_r8(cds_TX_UM_RLC_DRB_SN5),
        Rx := cs_DL_UM_RLC_r8(cds_RX_UM_RLC_DRB_SN5)
      }
    },
    TestMode := {
      None := true
    }
  };
  
  } // end of group RLCUMTemplates
  
} // module
