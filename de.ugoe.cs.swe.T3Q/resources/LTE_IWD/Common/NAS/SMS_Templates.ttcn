/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-09-07 11:40:07 +0200 (Mon, 07 Sep 2015) $
// $Rev: 14197 $
/******************************************************************************/

module SMS_Templates {
  
  import from CommonDefs all;
  import from SMS_TypeDefs all;
  
  template (value) TransactionIdentifier_Type cs_MT_TI0_FromSS :=
  { /* @status    APPROVED (LTE) */
    tI_Flag := '0'B,
    tI_Value := '000'B
  };
  
  template (present) TransactionIdentifier_Type cr_MT_TI0_FromUE :=
  { /* @status    APPROVED (LTE) */
    tI_Flag := '1'B,
    tI_Value := '000'B
  };
  
  template (present) TransactionIdentifier_Type cr_MO_AnyTI_FromUE :=
  { /* @status    APPROVED (LTE) */
    tI_Flag := '0'B,
    tI_Value := '???'B
  };
  
  template (present) TypeOfNumberingPlan cr_TypeOfNumberingPlanAny :=
  { /* @status    APPROVED (IMS, LTE) */
    extBit := ?,
    typeOfNumber := ?,
    numberingPlanId := ?
  };
  
  template (value) TypeOfNumberingPlan cs_TypeOfNumberingPlan :=
  { /* @status    APPROVED (IMS, LTE) */
    extBit := '1'B,
    typeOfNumber := '001'B, // international number
    numberingPlanId := '0001'B // ISDN/telephony numbering plan (Rec. E.164/E.163)
  };
  
  template (present) TypeOfNumberingPlan cr_TypeOfNumberingPlan :=
  { /* @status    APPROVED (IMS, LTE) */
    extBit := ?,
    typeOfNumber := ?,
    numberingPlanId := ?
  };
  
  /* End Non-SMS Type Constraints */
  
  /* SM-CP Type Constraints */
  /*   template (omit) CP_UserData_Type cs_CP_UserData (RP_PDU_Type p_RP_PDU, */
  /*                                                    integer p_Length) := */
  /*   { */
  /*     spare := omit, */
  /*     iei := omit, */
  /*     iel := int2oct (p_Length,1), // size of p_RP_PDU in octets, provided by the caller */
  /*     rP_PDU := p_RP_PDU */
  /*   }; */
  
  
  template (omit) CP_UserData_Type cs_CP_UserData_DELIVER (octetstring p_Digits):=
  { /* @status    APPROVED (LTE) */
    spare := omit,
    iei := omit,
    iel := int2oct ((162 + lengthof (p_Digits)), 1), // 162 = 153 + 1 + 5 + 3=size of cs_RP_OriginatorAddress_dl
    rP_PDU := {RP_DATA_dl := cs_RP_DATA_dl_DELIVER (p_Digits)}
  };
  
  template (omit) CP_UserData_Type cs_CP_UserData_SUBMIT_REPORT(O1_Type p_MsgRef) :=
  {
    // @sic R5s100604 sic@
    spare := omit,
    iei := omit,
    iel := '0D'O, // the length of RPDU is 13
    rP_PDU := {RP_ACK := cs_RP_ACK_SUBMIT_REPORT(p_MsgRef)}
  };
  
  template (omit) CP_UserData_Type cs_CP_UserData_RP_ACK_dl (O1_Type p_MsgRef):=
  { /* @status    APPROVED (LTE) */
    // @sic R5s100604 sic@ @sic R5s110206 sic@
    spare := omit,
    iei := omit,
    iel := '02'O, // the length of RPDU is 2 as RP UserData are omitted now
    rP_PDU := {
      RP_ACK := cs_RP_ACK_dl(p_MsgRef)
    }
  };
  
  
/*   template (present) CP_UserData_Type cr_CP_UserData (RP_PDU_Type p_RP_PDU) := */
/*   { */
/*     spare := omit, */
/*     iei := omit, */
/*     iel := ?, */
/*     rP_PDU := p_RP_PDU */
/*   }; */

  
  template (present) CP_UserData_Type cr_CP_UserData_SUBMIT(template (present) RP_DATA_ul_Type p_RP_DATA_ul := cr_RP_DATA_ul_SUBMIT) :=
  { /* @status    APPROVED (LTE) */
    spare := omit,
    iei := omit,
    iel := ?,
    rP_PDU := {
      RP_DATA_ul := p_RP_DATA_ul
    }
  };
  
  template (present) CP_UserData_Type cr_CP_UserData_DELIVER_REPORT :=
  {
    spare := omit,
    iei := omit,
    iel := ?,
    rP_PDU := {
      RP_ACK := cr_RP_ACK_DELIVER_REPORT
    }
  };
  
  template (present) CP_UserData_Type cr_CP_UserData_RP_ACK_ul :=
  { /* @status    APPROVED (LTE) */
    spare := omit,
    iei := omit,
    iel := ?,
    rP_PDU := {
      RP_ACK := cr_RP_ACK_ul
    }
  };
  /* End SM-CP Type Constraints */

  /* SM-CP PDU Constraints */
  
  template (value) CP_DATA_Type cs_CP_DATA (template (value) TransactionIdentifier_Type p_TransactionIdentifier,
                                            template (omit) CP_UserData_Type p_CP_UserData) :=
  { /* @status    APPROVED (LTE) */
    transactionIdentifier := p_TransactionIdentifier,
    protocolDiscriminator := tsc_PD_SMS,
    messageType := tsc_MT_CP_DATA,
    cP_UserData := p_CP_UserData
  };
  
  template (present) CP_DATA_Type cr_CP_DATA (template TransactionIdentifier_Type p_TransactionIdentifier,
                                              template CP_UserData_Type p_CP_UserData) :=
  { /* @status    APPROVED (LTE) */
    transactionIdentifier := p_TransactionIdentifier,
    protocolDiscriminator := tsc_PD_SMS,
    messageType := tsc_MT_CP_DATA,
    cP_UserData := p_CP_UserData
  };
  
  template (value) CP_ACK_Type cs_CP_ACK(template (value) TransactionIdentifier_Type p_TransactionIdentifier) :=
  { /* @status    APPROVED (LTE) */
    transactionIdentifier := p_TransactionIdentifier,
    protocolDiscriminator := tsc_PD_SMS,
    messageType := tsc_MT_CP_ACK
  };
  
  template (present) CP_ACK_Type cr_CP_ACK(template TransactionIdentifier_Type p_TransactionIdentifier) :=
  { /* @status    APPROVED (LTE) */
    transactionIdentifier := p_TransactionIdentifier,
    protocolDiscriminator := tsc_PD_SMS,
    messageType := tsc_MT_CP_ACK
  };
  
  template (value) CP_ERROR_Type cs_CP_ERROR(TransactionIdentifier_Type p_TransactionIdentifier,
                                             CP_Cause_Type p_CP_Cause) :=
  {
    transactionIdentifier := p_TransactionIdentifier,
    protocolDiscriminator := tsc_PD_SMS,
    messageType := tsc_MT_CP_ERROR,
    cP_Cause := p_CP_Cause
  };
  
  template (present) CP_ERROR_Type cr_CP_ERROR(template (value) TransactionIdentifier_Type p_TransactionIdentifier,
                                               CP_Cause_Type p_CP_Cause) :=
  {
    transactionIdentifier := p_TransactionIdentifier,
    protocolDiscriminator := tsc_PD_SMS,
    messageType := tsc_MT_CP_ERROR,
    cP_Cause := p_CP_Cause
  };
  
  template (present) CP_PDU_Type cr_CP_DATA_PDU(template TransactionIdentifier_Type p_TransactionIdentifier,
                                                template CP_UserData_Type p_CP_UserData) :=
  { /* @status    APPROVED (LTE) */
    CP_DATA := cr_CP_DATA (p_TransactionIdentifier, p_CP_UserData)
  };
  
  template (value) CP_PDU_Type cs_CP_DATA_PDU (template (value) TransactionIdentifier_Type p_TransactionIdentifier,
                                               template (value) CP_UserData_Type p_CP_UserData) :=
  { /* @status    APPROVED (LTE) */
    CP_DATA := cs_CP_DATA (p_TransactionIdentifier, p_CP_UserData)
  };
  
  template (present) CP_PDU_Type cr_CP_ACK_PDU(template TransactionIdentifier_Type p_TransactionIdentifier) :=
  { /* @status    APPROVED (LTE) */
    CP_ACK := cr_CP_ACK (p_TransactionIdentifier)
  };
  
  template (value) CP_PDU_Type cs_CP_ACK_PDU(template (value) TransactionIdentifier_Type p_TransactionIdentifier) :=
  { /* @status    APPROVED (LTE) */
    CP_ACK := cs_CP_ACK (p_TransactionIdentifier)
  };
  
  /* End SM-CP PDU Constraints */
  
  /* SM-RP Type Constraints */
  
  template (omit) RP_OriginatorAddress_dl cs_RP_OriginatorAddress_dl :=
  { /* @status    APPROVED (IMS, LTE) */
    spare := omit,
    iei := omit,
    iel := '04'O, // 4 semi-octets   @sic R5s100586 sic@
    typeOfNumberingPlan := cs_TypeOfNumberingPlan,
    digits := '001122'O
  };
  
  template (present) RP_OriginatorAddress_ul cr_RP_OriginatorAddress_ul :=
  { /* @status    APPROVED (IMS, LTE) */
    spare := omit,
    iei := omit,
    iel := '00'O
  };
  
  template (present) RP_DestinationAddress_ul cr_RP_DestinationAddress_ul(template (present) octetstring p_Digits := ?) :=
  { /* @status    APPROVED (IMS, LTE) */
    spare := omit,
    iei := omit,
    iel := ?,
    typeOfNumberingPlan := cr_TypeOfNumberingPlan,
    digits := p_Digits
  };

  template (omit) RP_DestinationAddress_dl cs_RP_DestinationAddress_dl :=
  { /* @status    APPROVED (IMS, LTE) */
    spare := omit,
    iei := omit,
    iel := '00'O
  };

/*   template (omit) RP_UserData cs_RP_UserData (TP_PDU_Type p_TP_PDU) := */
/*   { */
/*     spare := omit, */
/*     iei := omit, */
/*     iel := '05'O,  // to be corrected  !!! ??? */
/*     tP_PDU := p_TP_PDU */
/*   }; */

  template (omit) RP_UserData cs_RP_UserData_DELIVER (octetstring p_Digits) :=
  { /* @status    APPROVED (IMS, LTE) */
    spare := omit,
    iei := omit,
    iel := int2oct ((153 + lengthof (p_Digits)), 1),
    tP_PDU := {SMS_DELIVER := cs_SMS_DELIVER (p_Digits)}
  };
  
  template (omit) RP_UserData cs_RP_UserData_SUBMIT_REPORT :=
  { /* @status    APPROVED (IMS) */
    spare := tsc_Sparebit,
    iei := tsc_IEI_RP_UserData,
    iel := '0A'O, // the TPDU data length is 10 octets
    tP_PDU := {SMS_SUBMIT_REPORT := cs_SMS_SUBMIT_REPORT}
  };

/*   template RP_UserData cr_RP_UserData (TP_PDU_Type p_TP_PDU) := */
/*   { */
/*     spare := omit, */
/*     iei := omit, */
/*     iel := ?, */
/*     tP_PDU := p_TP_PDU */
/*   }; */

  template (present) RP_UserData cr_RP_UserData_SUBMIT(template (present) SMS_SUBMIT_Type p_SMS_SUBMIT := cr_SMS_SUBMIT_VPF_REF) :=
  { /* @status    APPROVED (IMS, LTE) */
    /* NOTE: cr_SMS_SUBMIT_VPF_REF fits with AT command !! */
    spare := omit,
    iei := omit,
    iel := ?,
    tP_PDU := {
      SMS_SUBMIT := p_SMS_SUBMIT
    }
  };
  
  template (present) RP_UserData cr_RP_UserData_DELIVER_REPORT :=
  {
    spare := tsc_Sparebit,
    iei := tsc_IEI_RP_UserData,
    iel := ?,
    tP_PDU := {
      SMS_DELIVER_REPORT := cr_SMS_DELIVER_REPORT
    }
  };
  
  /* End SM-RP Type Constraints */
  
  /* SM-RP PDU Constraints */
  
  template (value) RP_DATA_dl_Type cs_RP_DATA_dl_DELIVER (octetstring p_Digits) :=
  { /* @status    APPROVED (IMS, LTE) */
    spare5 := '00000'B,
    rP_MessageTypeIndicator := tsc_MT_RP_DATA_dl,
    rP_MessageReference := int2oct(0, 1),
    rP_OriginatorAddress := cs_RP_OriginatorAddress_dl,
    rP_DestinationAddress := cs_RP_DestinationAddress_dl,
    rP_UserData := cs_RP_UserData_DELIVER(p_Digits)
  };
  
  template (present) RP_DATA_ul_Type cr_RP_DATA_ul_SUBMIT(template (present) RP_UserData p_RP_UserData := cr_RP_UserData_SUBMIT,
                                                          template (present) octetstring p_Digits := ?) :=
  { /* @status    APPROVED (IMS, LTE) */
    spare5 := '00000'B,
    rP_MessageTypeIndicator := tsc_MT_RP_DATA_ul,
    rP_MessageReference := ?,
    rP_OriginatorAddress := cr_RP_OriginatorAddress_ul,
    rP_DestinationAddress := cr_RP_DestinationAddress_ul(p_Digits),
    rP_UserData := p_RP_UserData
  };
  
  template (value) RP_ACK_Type cs_RP_ACK_SUBMIT_REPORT(O1_Type p_MsgRef) :=
  { /* @status    APPROVED (IMS) */
    spare5 := '00000'B,
    rP_MessageTypeIndicator := tsc_MT_RP_ACK_dl,
    rP_MessageReference := p_MsgRef,   // @sic R5s100604 sic@
    rP_UserData := cs_RP_UserData_SUBMIT_REPORT
  };
  
  template (present) RP_ACK_Type  cr_RP_ACK_DELIVER_REPORT :=
  {
    spare5 := '00000'B,
    rP_MessageTypeIndicator := tsc_MT_RP_ACK_ul,
    rP_MessageReference := ?,
    rP_UserData := cr_RP_UserData_DELIVER_REPORT
  };
  
  template (value) RP_ACK_Type cs_RP_ACK_dl(O1_Type p_MsgRef) :=
  { /* @status    APPROVED (LTE) */
    spare5 := '00000'B,
    rP_MessageTypeIndicator := tsc_MT_RP_ACK_dl,
    rP_MessageReference := p_MsgRef,   // @sic R5s100604 sic@
    rP_UserData := omit
  };
  
  template (present) RP_ACK_Type  cr_RP_ACK_ul :=
  { /* @status    APPROVED (IMS, LTE) */
    spare5 := '00000'B,
    rP_MessageTypeIndicator := tsc_MT_RP_ACK_ul,
    rP_MessageReference := ?,
    rP_UserData := *
  };
  
  /* End SM-RP PDU Constraints */
  
  /* SM-TP Type Constraints */
  template (present) TP_ProtocolIdentifier_Type cr_TP_ProtocolIdentifier :=
  { /* @status    APPROVED (IMS, LTE) */
    // @sic R5s100586 sic@
    pidType := '00'B,
    interworking := '0'B,
    pidValue := '00000'B  // @sic R5s100773 sic@
  };
  
  template (value) TP_ProtocolIdentifier_Type cs_TP_ProtocolIdentifier :=
  { /* @status    APPROVED (IMS, LTE) */
    // @sic R5s100586 sic@
    pidType := '00'B,
    interworking := '0'B,
    pidValue := '00000'B  // @sic R5s100773 sic@
  };
  
  template (value) TP_DataCodingScheme_Type cs_TP_DataCodingScheme :=
  { /* @status    APPROVED (IMS, LTE) */
    codingGroup := '0000'B,
    codeValue := '0000'B
  };
  
  template (present) TP_Address_Type cr_TP_AddressAny :=
  { /* @status    APPROVED (IMS, LTE) */
    iel := ?,
    typeOfNumberingPlan := cr_TypeOfNumberingPlanAny,
    digits := *
  };
  
  template (value) TP_Address_Type cs_TP_Address (octetstring p_Digits) :=
  { /* @status    APPROVED (IMS, LTE) */
    iel := int2oct(2 * lengthof(p_Digits), 1),
    // length is number of useful semi-octets
    // as p_digits is declared as octetstring the number must be even
    typeOfNumberingPlan := cs_TypeOfNumberingPlan,
    digits := p_Digits
  };
  
  template (value) TP_ParameterIndicator_Type cs_TP_ParameterIndicator :=
  { /* @status    APPROVED (IMS) */
    extBit1 := '0'B,
    spare4 := '0000'B,
    tP_UDL := '0'B,
    tP_DCS := '0'B,
    tP_PID := '1'B
  };
  
  /* End SM-TP Type Constraints */
  
  /* SM-TP PDU Constraints */
  
  template (value) SMS_DELIVER_Type cs_SMS_DELIVER (octetstring p_Digits) :=
  { /* @status    APPROVED (IMS, LTE) */
    tP_ReplyPath := '0'B,
    tP_UserDataHeaderIndicator := '0'B,
    tP_StatusReportIndication := '0'B,
    spare2 := '00'B,
    tP_MoreMessagesToSend := '1'B,
    tP_MessageTypeIndicator := tsc_MT_SMS_DELIVER,
    tP_OriginatingAddress := cs_TP_Address (p_Digits),
    tP_ProtocolIdentifier := cs_TP_ProtocolIdentifier,
    tP_DataCodingScheme_Type := cs_TP_DataCodingScheme,
    tP_ServiceCentreTimeStamp := f_BCD_TimestampWithTimezone(),
    tP_UserDataLength := int2oct(160,1),
    tP_UserData := f_CharPacking_IA5toGsm7Bit(tsc_Fox, SMS_Packing)
  };
  
  template (present) SMS_DELIVER_REPORT_Type cr_SMS_DELIVER_REPORT :=
  {
    spare1 := '0'B,
    tP_UserDataHeaderIndicator  := '0'B,
    spare4 := '0000'B,
    tP_MessageTypeIndicator := tsc_MT_SMS_DELIVER_REPORT,
    tP_FailureCause := omit,
    tP_ParameterIndicator := ?,
    tP_ProtocolIdentifier := cr_TP_ProtocolIdentifier ifpresent, // @sic R5s100773 sic@
    tP_DataCodingScheme_Type := *,
    tP_UserDataLength := *,
    tP_UserData := *
  };

  template (present) SMS_SUBMIT_Type cr_SMS_SUBMIT(template (present) B2_Type p_ValidityPeriodFormat := '??'B,
                                                   template TP_ValidityPeriod_Type p_ValidityPeriod := *,
                                                   template (present) TP_UserDataLength_Type p_UserDataLength := ?) :=
  { /* @status    APPROVED (IMS, LTE) */
    tP_ReplyPath := '?'B,
    tP_UserDataHeaderIndicator := '?'B,
    tP_StatusReportRequest := '?'B,
    tP_ValidityPeriodFormat := p_ValidityPeriodFormat,
    tP_RejectDuplicates := '?'B,
    tP_MessageTypeIndicator := tsc_MT_SMS_SUBMIT,
    tP_MessageReference := ?,
    tP_DestinationAddress := cr_TP_AddressAny,
    tP_ProtocolIdentifier := cr_TP_ProtocolIdentifier,
    tP_DataCodingScheme_Type := ?,
    tP_ValidityPeriod  := p_ValidityPeriod,
    tP_UserDataLength := p_UserDataLength, // @sic R5s120530 sic@
    tP_UserData := ? // any data will do
  };
  
  template (present) SMS_SUBMIT_Type cr_SMS_SUBMIT_VPF_REF := cr_SMS_SUBMIT('10'B, {TP_ValidityPeriodRelative := int2oct(167,1)}, int2oct(160,1));  /* @status    APPROVED (IMS, LTE) */
  template (present) SMS_SUBMIT_Type cr_SMS_SUBMIT_AnyVP := cr_SMS_SUBMIT(-, *);          /* @status    APPROVED (LTE)
                                                                                             @sic R5s141400 sic@ @sic R5s150777 sic@ */

  template (value) SMS_SUBMIT_REPORT_Type cs_SMS_SUBMIT_REPORT :=
  { /* @status    APPROVED (IMS) */
    spare1 := '0'B,
    tP_UserDataHeaderIndicator := '0'B,
    spare4 := '0000'B,
    tP_MessageTypeIndicator := tsc_MT_SMS_SUBMIT_REPORT,
    tP_FailureCause := omit,
    tP_ParameterIndicator := cs_TP_ParameterIndicator,
    tP_ServiceCentreTimeStamp := f_BCD_TimestampWithTimezone(),
    tP_ProtocolIdentifier := cs_TP_ProtocolIdentifier,
    tP_DataCodingScheme_Type := omit,
    tP_UserDataLength := omit,
    tP_UserData := omit
  };
  
  /* End SM-TP PDU Constraints */

}
