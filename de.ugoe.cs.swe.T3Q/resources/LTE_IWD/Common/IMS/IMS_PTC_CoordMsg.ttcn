/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-08 19:30:40 +0100 (Tue, 08 Mar 2016) $
// $Rev: 15624 $
/******************************************************************************/

module IMS_PTC_CoordMsg {
  import from CommonDefs all;
  import from NAS_CommonTypeDefs all;

  type enumerated IMS_IpcanPortType_Type { IPCAN_E, OtherIPCAN_E }; /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ)
                                                                       @sic R5s160133: sic@
                                                                       In general coordination messages are exchanged with EUTRA being connected to the IPCAN port of the IMS_PTC;
                                                                       furthermore in IRAT scenarios UTRAN is connected to the OtherIPCAN port;
                                                                       NOTE 1: this is applicable for LTE test case but not e.g. for IMS_UTRAN test cases where UTRAN is connected to the IPCAN port of the IMS_PTC
                                                                       => in case of LTE IRAT scenarios when the IMS registration happens on UTRAN the IPCAN_QUERY needs to be sent to the OtherIPCAN port instead of the IPCAN port
                                                                       NOTE 2: Acc. to the TTCN-3 core language (clause 5.4.1.4) port parameters are inout parameters and therefore connnot be initialised
                                                                       => IMS_IpcanPortType_Type is used to distinguish which coordination port shall be used during IMS registration */

  type enumerated DelayForUserPlaneSignalling_Type { noDelay, waitForIMS, dontWaitForIMS };   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  //============================================================================
  // Commands/Responses used at the IMS-IPCAN interface for IMS test cases acc. to 34.229

  // parameters

  type enumerated IMS_TestProcedure_Type {      /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IPCAN_InitialRegistration,                  /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5.2.3 without RRC Connection Release at the end of the procedure
                                                 * NOTE: As working assumption the UE does IMS REGISTRATION automatically after RRC/NAS registration */
    IPCAN_EmergencyCall_NormalService,          /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.4.3 */
    IPCAN_EmergencyCall_LimitedService,         /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.5.3 */
    IPCAN_MO_SpeechCall,                        /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.6.3 */
    IPCAN_MT_SpeechCall,                        /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.7.3 */
    IPCAN_MO_VideoCall,                         /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.8.3 */
    IPCAN_MT_VideoCall,                         /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.9.3 */
    IPCAN_MO_AddVideo,                          /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.11.3 */
    IPCAN_MT_AddVideo,                          /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.12.3 */
    IPCAN_ReleaseVideo,                         /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.15.3 */
    IPCAN_MO_IMS_Signalling,                    /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5.3.3 with m = n = 0; used e.g. for MT SMS test case 18.2 */
    IPCAN_MT_IMS_Signalling,                    /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5.3.3 Steps 3 to 9 with m = n = 0; used e.g. for MT SMS test case 18.1 */
    IPCAN_XCAP_Signalling,                      /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.14 */
    IPCAN_MO_Speech_EmergencyCall               /* EUTRA/EPS signalling acc. to 36.508 cl. 4.5A.4.3 and 4.5A.6.3
                                                   !!!! TO BE REMOVED !!!! */
  };
  
  type enumerated IMS_TestConfiguration_Type {  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IPCAN_SignallingOnly,                       /* EUTRA: default DRB is used only */
    IPCAN_SpeechCall,                           /* EUTRA: one dedicated UM bearer; for normal speech calls and emergency call for limited services */
    IPCAN_VideoCall,                            /* EUTRA: two dedicated UM bearers */
    IPCAN_EmergencyCall,                        /* EUTRA: second default bearer (AM) and one dedicated UM bearer */
    IPCAN_SpeechAndEmergencyCall,               /* EUTRA: default bearer + dedicated bearer for normal speech call and another default bearer + dedicated bearer for emergency call */
    IPCAN_XCAP                                  /* EUTRA: second default bearer (AM) for second PDN used for XCAP signalling */
  };

  type enumerated IMS_CellConfiguration_Type {  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN)
                                                   !!!! TO BE REMOVED !!!! */
    SIB2_Normal,                                /* to change cell configuration back to normal configuration */
    SIB2_TC_12_18,                              /* SIB2 configuration acc. to test case 12.18 */
    SIB2_TC_12_18b,                             /* SIB2 configuration acc. to test case 12.18b */
    SIB2_TC_12_19,                              /* SIB2 configuration acc. to test case 12.19 */
    SIB2_TC_12_19b,                             /* SIB2 configuration acc. to test case 12.19b */
    SIB2_TC_12_20,                              /* SIB2 configuration acc. to test case 12.20 */
    SIB2_TC_12_20a,                             /* SIB2 configuration acc. to test case 12.20a */
    IPCAN_UpdateUELocationInformation           /* EUTRA: set UELocationInformation acc. to 36.509 */
 };

  type record IPCAN_INFO_Type {                 /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    /* @sic R5s120907 R5s130133 additional changes sic@ */
    /* @sic R5s130739 MCC160 implementation: new field UE_Release sic@ */
    IPCAN_RAN_Type RanType  optional,
    integer        UE_Release  optional,
    integer        AuthResLength  optional
  };
  
  type enumerated IMS_IPCAN_CommandName_Type {  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IPCAN_INIT,                                 /* trigger the IPCAN_PTC to create a cell and do further appropriate initialisation;
                                                 * which RAN technology to be use is decided by the IPCAN_PTC based on PIXITs;
                                                 * as test procedure shall be specified which procedure is used during the test body to know which DRBs need to be pre-configured;
                                                 * IPCAN returns response indicating the RAN type */
    IPCAN_CONFIG,                               /* trigger the IPCAN_PTC to apply test case specific change of the cell configuration as e.g. SIB2 for cell barring
                                                   !!!! TO BE REMOVED !!!! */
    IPCAN_STARTPROCEDURE,                       /* trigger the IPCAN to expect (MO) or page (MT) the UE to establish an RRC connection;
                                                 * depending on the connection type triggers may need to be sent from IPCAN to IMS or from IMS to IPCAN to synchronise establishment
                                                 * of dedicated DRBs (EUTRA) or secondary PDP contexts (UTRAN) */
    IPCAN_ENDPROCEDURE,                         /* trigger RRC connection release by the IPCAN_PTC;
                                                 * as a SIP message may be sent out just before end of the procedure in general a delay of 2s shall be added before sending IPCAN_ENDPROCEDURE to IPCAN;
                                                 * for UTRAN it is up to IPCAN and SS implementation to cope with possible/necessary release of (secondary) PDP context;
                                                 * a trigger shall be sent from IPCAN to IMS to indicate when RRC connection is released */
    IPCAN_RELEASE,                              /* Detach UE and release cell (postamble);
                                                 * a trigger is shall be sent from IPCAN to IMS to indicate when IPCAN is released */
    IPCAN_QUERY                                 /* query information from the IPCAN PTC
                                                 * @sic R5s130133 additional changes sic@ */
    //IPCAN_PROVIDELOCATIONINFORMATION            /* trigger IPCAN to provide UELocationInformation to the UE acc. to 36.509 */
  };

  type record IMS_IPCAN_Command_Type {                                       /* Messages IMS_PTC -> IPCAN
                                                                                @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IMS_IPCAN_CommandName_Type          Name,
    IMS_TestConfiguration_Type          TestConfiguration  optional,
    IMS_TestProcedure_Type              TestProcedure  optional,
    IMS_CellConfiguration_Type          CellConfiguration optional           /* used for IPCAN_CONFIG to allow test case specific initialisation of the EUTRA cell info
                                                                                !!!! TO BE REMOVED !!!! */
  };

  type enumerated IMS_IPCAN_ResponseName_Type {  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IPCAN_INIT,                                  /* response for INIT command: carries the RAN type as used by the IPCAN PTC;
                                                    the RAN type depends on PIXIT settings:
                                                    part 4 model:   px_RANTech
                                                    part 3 model:   EUTRA_FDD or EUTRA_TDD depending on px_ePrimaryFrequencyBand (px_ePrimaryFrequencyBand < 33 => FDD) */
    IPCAN_QUERY                                  /* @sic R5s120907 R5s130133 additional changes sic@ */
  };

  type record IPCAN_IMS_Response_Type {          /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    IMS_IPCAN_ResponseName_Type         Name,
    IPCAN_INFO_Type                     IpcanInfo optional      /* @sic R5s120907 R5s130133 additional changes sic@ */
  };

  //============================================================================

  type enumerated TriggerResult_Type {NORMAL, ABORT};   /* @status    APPROVED (IMS) */

  type union IMS_IPCAN_Coordination_MSG {               /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    Null_Type                           TriggerEvent,           // any trigger of confirmation
    Null_Type                           AbortEvent,             // sent instead of TriggerEvent if procedure shall be aborted
    Null_Type                           Reset,                  // IMS <- IPCAN: to reset IMS after UE has been switched off and gets switched on again
    IMS_IPCAN_Command_Type              IMS_IPCAN_Command,      // IMS -> IPCAN: command to be done at IPCAN
    IPCAN_IMS_Response_Type             IPCAN_IMS_Response,     // IMS <- IPCAN: response for previous command
    NAS_ProtocolConfigOptions_Type      ProtocolConfigOptions,  // IMS <-> IPCAN: PCOs to be used in NAS signalling
    charstring                          IPCAN_IMS_Data          // IMS <- IPCAN: to send data from IPCAN to IMS @sic R5-151607 sic@
  };

  type port IMS_IPCAN_CO_ORD_PORT message
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, SSNITZ, UTRAN) */
    out IMS_IPCAN_Coordination_MSG;
    in  IMS_IPCAN_Coordination_MSG;
  };
  

  type union IMS_IMS_Coordination_MSG {                 /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Null_Type                           TriggerEvent
  };

  type port IMS_IMS_CO_ORD_PORT message {               /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    out IMS_IMS_Coordination_MSG;
    in  IMS_IMS_Coordination_MSG;
  };
  
  //----------------------------------------------------------------------------
  
  template (value) IMS_IPCAN_Coordination_MSG cms_IMS_IPCAN_Trigger :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    TriggerEvent := true
  };
  
  template (present) IMS_IPCAN_Coordination_MSG cmr_IMS_IPCAN_Trigger :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    TriggerEvent := true
  };
  
  template (value) IMS_IPCAN_Coordination_MSG cms_IMS_IPCAN_Abort :=
  { /* @status    APPROVED (IMS) */
    AbortEvent := true
  };
  
  template (present) IMS_IPCAN_Coordination_MSG cmr_IMS_IPCAN_Abort :=
  { /* @status    APPROVED (IMS) */
    AbortEvent := true
  };
  
  template (value) IMS_IPCAN_Coordination_MSG cms_IMS_IPCAN_Command(IMS_IPCAN_CommandName_Type p_CommandName,
                                                                    template (omit) IMS_TestConfiguration_Type p_TestConfiguration := omit,
                                                                    template (omit) IMS_TestProcedure_Type p_TestProcedure := omit,
                                                                    template (omit) IMS_CellConfiguration_Type p_CellConfiguration := omit) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    IMS_IPCAN_Command := {
      Name := p_CommandName,
      TestConfiguration := p_TestConfiguration,
      TestProcedure := p_TestProcedure,
      CellConfiguration := p_CellConfiguration
    }
  };
  
  template (present) IMS_IPCAN_Coordination_MSG cmr_IMS_IPCAN_Command(template (present) IMS_IPCAN_CommandName_Type p_CommandName := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    IMS_IPCAN_Command := {
      Name := p_CommandName,
      TestConfiguration := *,
      TestProcedure := *,
      CellConfiguration := *
    }
  };
  
  template (value) IPCAN_INFO_Type cs_IPCAN_INFO(template (omit) IPCAN_RAN_Type p_RanType,
                                                 template (omit) integer p_UE_Release,
                                                 template (omit) integer p_AuthResLength) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    RanType := p_RanType,
    UE_Release := p_UE_Release,
    AuthResLength := p_AuthResLength
  };
  
  template (value) IMS_IPCAN_Coordination_MSG cms_IPCAN_IMS_Response(IMS_IPCAN_ResponseName_Type p_ResponseName,
                                                                     template (omit) IPCAN_INFO_Type p_IpcanInfo := omit) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    /* @sic R5s120907 R5s130133 additional changes sic@ */
    IPCAN_IMS_Response := {
      Name := p_ResponseName,
      IpcanInfo := p_IpcanInfo
    }
  };
  
  template (value) IMS_IPCAN_Coordination_MSG cms_IPCAN_IMS_QueryResponse(template (value) IPCAN_INFO_Type p_IpcanInfo) := cms_IPCAN_IMS_Response(IPCAN_QUERY, p_IpcanInfo);  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
                                                                                                                                                                                 @sic R5s130133 additional changes sic@ */
  
  template (present) IMS_IPCAN_Coordination_MSG cmr_IPCAN_IMS_Response :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    IPCAN_IMS_Response := ?
  };

  template (value) IMS_IPCAN_Coordination_MSG cms_IPCAN_IMS_Reset :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Reset := true
  };
  
  template (present) IMS_IPCAN_Coordination_MSG cmr_IPCAN_IMS_Reset :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Reset := true
  };
  
  template (value) IMS_IPCAN_Coordination_MSG cms_IPCAN_IMS_Data (charstring p_Data):=
  { /* @status    APPROVED (LTE_A_IRAT) */
    /* @sic R5-151607 sic@ */
    IPCAN_IMS_Data := p_Data
  };
  
  template (present) IMS_IPCAN_Coordination_MSG cmr_IPCAN_IMS_Data (template (present) charstring p_Data := ?):=
  { /* @status    APPROVED (LTE_A_IRAT) */
    /* @sic R5-151607 sic@ */
    IPCAN_IMS_Data := p_Data
  };
  
  //----------------------------------------------------------------------------
  
  template (value) IMS_IMS_Coordination_MSG cms_IMS_IMS_Trigger :=
  { /* @status    APPROVED (IMS) */
    TriggerEvent := true
  };
  
  template (present) IMS_IMS_Coordination_MSG cmr_IMS_IMS_Trigger :=
  { /* @status    APPROVED (IMS) */
    TriggerEvent := true
  };
  
  //----------------------------------------------------------------------------
  /*
   * @desc      query IPCAN specific information from IPCAN PTC
   * @param     p_ImsCoorPort
   * @return    IPCAN_INFO_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_IPCAN_Query(IMS_IPCAN_CO_ORD_PORT p_ImsCoorPort) return IPCAN_INFO_Type
  {
    var IMS_IPCAN_Coordination_MSG v_IMS_IPCAN_CoordMsg;
    p_ImsCoorPort.send(cms_IMS_IPCAN_Command(IPCAN_QUERY));
    p_ImsCoorPort.receive(cmr_IPCAN_IMS_Response) -> value v_IMS_IPCAN_CoordMsg;
    return v_IMS_IPCAN_CoordMsg.IPCAN_IMS_Response.IpcanInfo;
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      to immediately detect programming errors (shall normally not happen)
   * @param     p_Port
   * @status    APPROVED (IMS)
   */
  altstep a_IMS_IPCAN_Default(IMS_IPCAN_CO_ORD_PORT p_Port)
  {
    [] p_Port.receive {
      FatalError(__FILE__, __LINE__, "unexpected coordination message");
    }
  }
  
  /*
   * @desc      Wait for a synchonisation message from IMS/IPCAN PTC
   * @param     p_Port
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_IPCAN_WaitForTrigger(IMS_IPCAN_CO_ORD_PORT p_Port)
  { /* @sic R5s130495 change 3: a_IMS_IPCAN_Default removed as at least at EUTRA queries (IPCAN_QUERY) need to be handled by EUTRA's default behaviour sic@ */
    alt {
      [] p_Port.receive(cmr_IMS_IPCAN_Trigger) {}
      //[] a_IMS_IPCAN_Default(p_Port) {}
    }
  }

  /*
   * @desc      Wait for a synchonisation message from IMS/IPCAN PTC
   * @param     p_Port
   * @return    TriggerResult_Type
   * @status    APPROVED (IMS)
   */
  function f_IMS_IPCAN_WaitForTriggerOrAbort(IMS_IPCAN_CO_ORD_PORT p_Port) return TriggerResult_Type
  { /* @sic R5s130495 change 3: a_IMS_IPCAN_Default removed as at least at EUTRA queries (IPCAN_QUERY) need to be handled by EUTRA's default behaviour sic@ */
    var TriggerResult_Type v_Result := ABORT;
    alt {
      [] p_Port.receive(cmr_IMS_IPCAN_Trigger) {v_Result := NORMAL}
      [] p_Port.receive(cmr_IMS_IPCAN_Abort) {}
      //[] a_IMS_IPCAN_Default(p_Port) {}
    }
    return v_Result;
  }

  /*
   * @desc      Wait for a synchonisation message from IMS/IPCAN PTC
   * @param     p_Port
   * @return    charstring
   * @status    APPROVED (LTE_A_IRAT)
   */
  function f_IMS_IPCAN_WaitForData(IMS_IPCAN_CO_ORD_PORT p_Port) return charstring
  { /* @sic R5-151607 sic@ */
    var IMS_IPCAN_Coordination_MSG v_IMS_IPCAN_CoordMsg;
    p_Port.receive(cmr_IPCAN_IMS_Data) -> value v_IMS_IPCAN_CoordMsg; // @sic R5s150704 sic@
    return v_IMS_IPCAN_CoordMsg.IPCAN_IMS_Data;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      send message to IMS/IPCAN PTC
   * @param     p_Port
   * @param     p_Msg               (default value: cms_IMS_IPCAN_Trigger)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_IPCAN_SendCoOrdMsg(IMS_IPCAN_CO_ORD_PORT p_Port,
                                    template (value) IMS_IPCAN_Coordination_MSG p_Msg := cms_IMS_IPCAN_Trigger)
  {
    p_Port.send(p_Msg);
  }

/*   function f_IMS_IPCAN_CheckForTrigger(IMS_IPCAN_CO_ORD_PORT p_Port) return boolean */
/*   { */
/*     timer t_Timer := 0.0; */
/*     t_Timer.start; */
    
/*     alt { */
/*       [] p_Port.receive(cmr_IMS_IPCAN_Trigger) { return true; } */
/*       [] a_IMS_IPCAN_Default(p_Port) {} */
/*       [] t_Timer.timeout {} */
/*     } */
/*     return false; */
/*   } */

  //----------------------------------------------------------------------------
  /*
   * @desc      send coordination message to the other IMS PTC
   * @param     p_Port
   * @param     p_Msg               (default value: cms_IMS_IMS_Trigger)
   * @status    APPROVED (IMS)
   */
  function f_IMS_IMS_SendCoOrdMsg(IMS_IMS_CO_ORD_PORT p_Port,
                                  template (value) IMS_IMS_Coordination_MSG p_Msg := cms_IMS_IMS_Trigger)
  {
    p_Port.send(p_Msg);
  }

  /*
   * @desc      wait for coordination message from the other IMS PTC
   * @param     p_Port
   * @status    APPROVED (IMS)
   */
  function f_IMS_IMS_WaitForTrigger(IMS_IMS_CO_ORD_PORT p_Port)
  {
    p_Port.receive(cmr_IMS_IMS_Trigger);
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Common function for EUTRA and UTRAN to allow user plane signalling during initial registration
   * @param     p_Port
   * @param     p_DelayForUserPlaneSignalling
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IPCAN_DelayForUserPlaneSignalling(IMS_IPCAN_CO_ORD_PORT p_Port,
                                               DelayForUserPlaneSignalling_Type p_DelayForUserPlaneSignalling)
  {
    var boolean v_WaitForFirstTrigger := true;
    timer t_WaitForUPlaneSignalling;
    
    select (p_DelayForUserPlaneSignalling) {
      case (noDelay) {
        /* do nothing */
      }
      case (dontWaitForIMS) {
        t_WaitForUPlaneSignalling.start(1.2);
        alt {
          [] t_WaitForUPlaneSignalling.timeout {
            // just continue
          }
          [] p_Port.receive(cmr_IMS_IPCAN_Trigger) {
            f_SetVerdictInconc(__FILE__, __LINE__, "IMS Registration even though loopback mode is activated");  /* @sic R5-153971 sic@ */
          }
        }
      }
      case (waitForIMS) {
        t_WaitForUPlaneSignalling.start(10.0);
        alt {
          [] t_WaitForUPlaneSignalling.timeout {
            // just continue
          }
          [] p_Port.receive(cmr_IMS_IPCAN_Trigger) {
            if (v_WaitForFirstTrigger) {                // @sic R5s120727 change 1 sic@
              t_WaitForUPlaneSignalling.stop;
              v_WaitForFirstTrigger := false;
              repeat;      // receive second trigger sent by IMS after registration has finished
            }
          }
        }
      }
    }
  }
}
