/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-11-28 01:19:40 +0100 (Sat, 28 Nov 2015) $
// $Rev: 14774 $
/******************************************************************************/

module IP_ASP_Common {
  import from IP_ASP_TypeDefs all;
  import from CommonIP all;

  //****************************************************************************
  // Auxiliary functions
  //----------------------------------------------------------------------------

  function f_IP_AddrInfo_AddrStr(IP_AddrInfo_Type p_AddrInfo) return charstring
  {
    var charstring v_AddrStr := "";
    if (ischosen(p_AddrInfo.V4)) { v_AddrStr := p_AddrInfo.V4.Addr; }
    if (ischosen(p_AddrInfo.V6)) { v_AddrStr := p_AddrInfo.V6.Addr; }
    return v_AddrStr;
  }

  function f_IP_AddrInfo_TypeStr(IP_AddrInfo_Type p_AddrInfo) return charstring
  { /* @sic R5s130333 change 29: IPv4 -> IP6, IPv6 -> IP4 sic@ */
    var charstring v_TypeStr := "";
    if (ischosen(p_AddrInfo.V4)) { v_TypeStr := "IP4"; }
    if (ischosen(p_AddrInfo.V6)) { v_TypeStr := "IP6"; }
    return v_TypeStr;
  }

  /*
   * @desc      match IP address sent by the UE in IMS message against given address
   * @param     p_AddrInfo
   * @param     p_ReceivedAddrString
   * @return    boolean
   * @status    APPROVED (IMS, LTE, LTE_IRAT)
   */
  function f_IMS_IpAddrMatch(IP_AddrInfo_Type p_AddrInfo,
                             charstring p_ReceivedAddrString) return boolean
  { /* @sic R5w130106 - square brackets to be handled by codec sic@ */

    if (ischosen(p_AddrInfo.V4)) {
      return match(p_ReceivedAddrString, p_AddrInfo.V4.Addr);
    }
    if (ischosen(p_AddrInfo.V6)) {
      return (f_Convert_IPv6Addr2OctString(p_ReceivedAddrString) == f_Convert_IPv6Addr2OctString(p_AddrInfo.V6.Addr));
    }
    return false;
  }
}
