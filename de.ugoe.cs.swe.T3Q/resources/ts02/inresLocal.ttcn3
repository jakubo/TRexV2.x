module inresLocal {
    import from inresDefinitions all;

    import from inresUserDefinitions {
        const somePayload;
    }

    modulepar {
        integer transmissions := 100;
        float maxRetransmissionTime := 20.0;
        float maxExecutionTime := 2000.0;
        SequenceNumber initialSequenceNumber := one;
    } 

    testcase bulkDataTransfer(integer iterations) runs on InresSystemType {
        var integer counter;
        var SequenceNumber expectedSequenceNumber := initialSequenceNumber;
        var MDATind receivedPDU;
        timer T;
        var default failOrInconcDefault := activate(failOrInconc(T));
        map(self:ISAP, system:ISAP);
        map(self:MSAP, system:MSAP);
        Preamble();
        for (var integer i := 1; i <= iterations; i := i + 1) {
            ISAP.send(IDATreq:{somePayload});
            counter := 1;
            T.start(maxRetransmissionTime);
            alt {
                [] MSAP.receive(DataTransfer(somePayload, expectedSequenceNumber)) {
                    T.stop;
                    MSAP.send(DataAcknowledgement(expectedSequenceNumber));
                    expectedSequenceNumber := toggle(expectedSequenceNumber);
                }
                [counter <= 4] MSAP.receive(DataTransfer(somePayload, ?)) ->  value receivedPDU {
                    MSAP.send(DataAcknowledgement(receivedPDU.mData.seqNo));
                    counter := counter + 1;
                    repeat;
                }
                [counter > 4] MSAP.receive(DataTransfer(somePayload, ?)) {
                    setverdict(fail);
                    stop;
                }
            }
        }
        setverdict(pass);
        Postamble();
        deactivate(failOrInconcDefault);
    }

    altstep failOrInconc(timer T) runs on InresSystemType {
        [] ISAP.receive(IDISind:{

        }) {
            setverdict(inconc);
            stop;
        }
        [] ISAP.receive {
            setverdict(fail);
            stop;
        }
        [] MSAP.receive {
            setverdict(fail);
            stop;
        }
        [] T.timeout {
            setverdict(fail);
            stop;
        }
    }

    function Preamble() runs on InresSystemType {
        timer T;
        var default failOrInconcDefault := activate(failOrInconc(T));
        ISAP.send(ICONreq:{

        });
        T.start(maxRetransmissionTime);
        alt {
            [] MSAP.receive(ConnectionRequest) {
                MSAP.send(ConnectionConfirmation);
                repeat;
            }
            [] ISAP.receive(ICONconf:{

            }) {
                T.stop;
            }
        }
        deactivate(failOrInconcDefault);
    }

    function Postamble() runs on InresSystemType {
        MSAP.send(DisconnectionRequest);
    }

    testcase connectionEstablishment() runs on InresSystemType { /* ... */
    }

    testcase connectionRelease() runs on InresSystemType { /* ... */
    }

    control {
        var verdicttype myVerdict;
        log("Starting test execution...");
        myVerdict := execute(connectionEstablishment(), maxExecutionTime);
        
        if (myVerdict == pass) {
            myVerdict := execute(connectionRelease(), maxExecutionTime);
        }
        
        if (myVerdict == pass) {
            myVerdict := execute(bulkDataTransfer(transmissions), maxExecutionTime);
        }
        log("Test execution terminated");
    }
}// End of module inresLocal
