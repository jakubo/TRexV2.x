/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2017-01-11 11:46:58 +0100 (Wed, 11 Jan 2017) $
// $Rev: 17719 $
/******************************************************************************/

module NBIOT_L2_Common {
  import from NBIOT_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from CommonDefs all;
  import from EUTRA_NB_CommonDefs all;
  import from EUTRA_NB_ASP_L2DataDefs all;
  import from NBIOT_ASP_TypeDefs all;
  import from NBIOT_ASP_SrbDefs all;
  import from NBIOT_ASP_L2DataDefs all;
  import from NBIOT_AspCommon_Templates all;
  import from NBIOT_CommonDefs all;
  import from NBIOT_Component all;
  import from NBIOT_SRB_Templates all;
  import from NBIOT_CellCfg_Templates all;
  import from NBIOT_CellInfo all;
  import from NBIOT_ConfigurationSteps all;
  import from NBIOT_CommonProcedures all;
  import from NBIOT_LoopBack all;
  import from NasEmu_L2Test_NBIOT all;
  import from EPS_NAS_Constants all;
  import from EPS_NAS_TypeDefs all;
  import from EPS_NAS_Templates all;
  import from EPS_NAS_MsgContainers all;
  import from CIOT_NASTemplates all;

  //----------------------------------------------------------------------------

  template (value) NB_L2_DATA_REQ cas_NB_L2_DATA_REQ(template (value) NB_ReqAspCommonPart_Type p_NB_ReqAspCommonPart,
                                                     template (value) L2Data_Request_Type p_L2Data_Request) :=
  {
    Common := p_NB_ReqAspCommonPart,
    L2Data := p_L2Data_Request
  };

  template (present) NB_L2_DATA_IND car_NB_L2_DATA_IND(template (present) NB_IndAspCommonPart_Type p_NB_IndAspCommonPart,
                                                       template (present) L2Data_Indication_Type p_L2Data_Indication) :=
  {
    Common := p_NB_IndAspCommonPart,
    L2Data := p_L2Data_Indication
  };

  //============================================================================
  // Common function and templates to retrieve encoded RRC/NAS message from the NAS emulator
  //----------------------------------------------------------------------------
  
  template (value) NB_ReqAspCommonPart_Type cs_NB_ReqAspCommonPart_L2TEST :=
  { /* ASP common part for ASPs sent to the NAS emulator to be routed back to the L2DATA port */
    CellId := nbiot_Cell_NonSpecific,
    RoutingInfo := cs_NB_RoutingInfo_L2Test,
    TimingInfo := cs_TimingInfo_None,
    ControlInfo := {
      CnfFlag := false,
      FollowOnFlag := false
    }
  };

  template (value) NB_SRB_COMMON_REQ cas_NB_L2TEST_NasPdu_REQ(template (value) NAS_MSG_Request_Type p_NasMsg) :=
  { /* ASP to send NAS message (within RRC DLInformationTransfer) to NAS emulator to get is back encoded at NASEMU_L2TEST */
    Common := cs_NB_ReqAspCommonPart_L2TEST,
    Signalling := {
      Rrc := omit,
      Nas := { p_NasMsg }
    }
  };

  template (present) NASEMU_NBIOT_NB_L2SDU_MSG cr_NB_L2SDU_MSG_DCCH :=
  {
    Dcch := ?
  };

  function f_NBIOT_L2_CPMode_EncodeL3Message(template (value) NAS_MSG_Request_Type p_NasRequest) runs on NBIOT_PTC return octetstring
  {
    var NASEMU_NBIOT_NB_L2SDU_MSG v_NB_L2SDU_MSG_DCCH;

    SRB.send(cas_NB_L2TEST_NasPdu_REQ(p_NasRequest));
    NASEMU_L2TEST.receive(cr_NB_L2SDU_MSG_DCCH) -> value v_NB_L2SDU_MSG_DCCH;
    
    return bit2oct(v_NB_L2SDU_MSG_DCCH.Dcch);
  }

  //----------------------------------------------------------------------------

  function f_NBIOT_L2_CPMode_DL_NAS_Message(NBIOT_IDLEUPDATED_STATE_Type p_TestLoopMode,
                                            template (value) RLC_SDU_Type p_RlcSdu,
                                            EPS_BearerIdentity p_EPSBearerId := tsc_EpsDefaultBearerId) return template (value) NAS_MSG_Request_Type
  {
    var ProcedureTransactionIdentifier v_PTI := tsc_PTI_Unassigned;
    var template (value) NAS_DL_Message_Type v_NasMsg;

    select (p_TestLoopMode) {
      case (STATE2ANB_TESTLOOP_ModeG) {
        v_NasMsg := cs_ESM_DATA_TRANSPORT(p_EPSBearerId, v_PTI, cs_UserDataContainer(p_RlcSdu));
      }
      case else {
        FatalError(__FILE__, __LINE__, "TestLoopMode not implemented yet");                                // !!!! FFS - STATE2ANB_TESTLOOP_ModeH to be implemented !!!!
      }
    }
    return cs_NAS_Request(tsc_SHT_IntegrityProtected_Ciphered, v_NasMsg);
  }

  //============================================================================
  // Common functions and templates to get/set RLC Counters VRR and VTS for L2 CP mode test cases
  //----------------------------------------------------------------------------

  template (value) NB_RLC_CountsReq_Type cs_RLC_CountsGet :=
  {
    Get := true
  };

  template (present) NB_SYSTEM_CTRL_CNF car_RlcCounts_CNF(NBIOT_CellId_Type p_CellId,
                                                          template (present) NB_RLC_CountsCnf_Type p_RLC_CountsCnf) :=
  {
    Common := cr_NB_CnfAspCommonPart_CellCfg(p_CellId),
    Confirm := {
      RlcCounts := p_RLC_CountsCnf
    }
  };

  template (value) NB_RLC_CountsReq_Type cs_RLC_CountsSet(integer p_AM_VTS,
                                                          integer p_AM_VRR) :=
  {
    Set := {
      AM_VTS := p_AM_VTS,
      AM_VRR := p_AM_VRR
    }
  };

  template (value) NB_SYSTEM_CTRL_REQ cas_RlcCounts_REQ(NBIOT_CellId_Type p_CellId,
                                                        template (value) NB_RLC_CountsReq_Type p_CountsReq,
                                                        template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now) :=
  {
    Common  := cs_NB_ReqAspCommonPart_CellCfg(p_CellId, p_TimingInfo),
    Request := {
      RlcCounts := p_CountsReq
    }
  };
  
  /*
   * @desc      Common functon to enquire RLC sequence numbers
   * @param     p_CellId
   * @return    NB_RLC_CountsInfoList_Type
   * @status
   */
  function f_NBIOT_SS_RlcCounts_Get(NBIOT_CellId_Type p_CellId) runs on NBIOT_PTC return NB_RLC_CountsInfoList_Type
  {
    var NB_SYSTEM_CTRL_CNF  v_ReceivedAsp;
    
    SYS.send   (cas_RlcCounts_REQ (p_CellId, cs_RLC_CountsGet));
    SYS.receive(car_RlcCounts_CNF(p_CellId, {Get := ?})) -> value v_ReceivedAsp;

    return v_ReceivedAsp.Confirm.RlcCounts.Get;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      set RLC counts at the given cell
   * @param     p_CellId
   * @param     p_RlcCountsInfoList
   * @param     p_TimingInfo         (default value: cs_TimingInfo_Now)
   * @status
   */
  function f_NBIOT_SS_RlcCounts_Set(NBIOT_CellId_Type p_CellId,
                                    integer p_AM_VTS,
                                    integer p_AM_VRR,
                                    template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now) runs on NBIOT_PTC
  {
    var boolean v_CnfFlag := f_TimingInfo_IsNow(p_TimingInfo);
    SYS.send (cas_RlcCounts_REQ(p_CellId, cs_RLC_CountsSet(p_AM_VTS, p_AM_VRR), p_TimingInfo));
    if (v_CnfFlag) {
      SYS.receive(car_RlcCounts_CNF(p_CellId, {Set := true}));
    }
  }

  //============================================================================
  // Common function to configure the SS to/from transparent mode for MAC and RLC tests
  //----------------------------------------------------------------------------
  
  type enumerated NBIOT_L2_TestConfig_Type { NormalMode, TransparentMode_RLC_Only, TransparentMode_RLC_MAC };

  function f_NBIOT_L2_CPMode_SrbTestMode(NBIOT_CellId_Type p_CellId,
                                         NBIOT_L2_TestConfig_Type p_TestConfig) runs on NBIOT_PTC
  {
    var template (value) NB_RadioBearer_Type v_NB_RadioBearer;
    var template (value) NB_RadioBearerList_Type v_SrbList;
    var IndicationAndControlMode_Type v_L2TestMode;

    select (p_TestConfig) {
      case (NormalMode) {
        v_L2TestMode := disable;
        v_NB_RadioBearer := cs_NB_SRB1bis_ConfigCommon(v_L2TestMode);
      }
      case (TransparentMode_RLC_Only) {
        v_L2TestMode := enable;         // SS to route UL messages to the L2DATA port
        v_NB_RadioBearer := cs_NB_SRB1bis_ConfigCommon(v_L2TestMode, cs_NB_RLC_RbConfig_TM);
      }
      case (TransparentMode_RLC_MAC) {
        v_L2TestMode := enable;         // SS to route UL messages to the L2DATA port
        v_NB_RadioBearer := cs_NB_SRB1bis_ConfigCommon(v_L2TestMode, cs_NB_RLC_RbConfig_TM, cs_NB_MAC_TestMode_Transparent);
      }
      case else {
        FatalError(__FILE__, __LINE__, "unknown test mode");
      }
    }
    v_SrbList := { v_NB_RadioBearer };
    f_NBIOT_SS_CommonRadioBearerConfig(p_CellId, v_SrbList);
  }

  //----------------------------------------------------------------------------

  function f_NBIOT_L2_Enter_State2B_NB(out NB_RLC_CountsInfoList_Type p_RLC_CountsInfoList,
                                       NBIOT_CellId_Type p_CellId,
                                       NBIOT_L2_TestConfig_Type p_TestConfig,
                                       NBIOT_IDLEUPDATED_STATE_Type p_TestLoopMode,
                                       integer p_TestLoopModeRepetitions := 1) runs on NBIOT_PTC
  {
    //Bring UE to initial state
    f_NBIOT_Preamble(p_CellId, CONTROL_PLANE, p_TestLoopMode);
    
    // test case specific pre-configuration
    f_NBIOT_CloseUE_TestLoopMode(p_CellId,                              // Bring UE to state 2B-NB
                                 tsc_SRB1bis,
                                 f_NBIOT_UE_TestLoopMode_Type(p_TestLoopMode),
                                 tsc_TestLoopModeGH_UplinkMode_RLC,
                                 p_TestLoopModeRepetitions);
    f_NBIOT_StopULGrantTransmission(p_CellId);                          // stop UL grants
    p_RLC_CountsInfoList := f_NBIOT_SS_RlcCounts_Get(p_CellId);         // get RLC sequence numbers
    f_NBIOT_L2_CPMode_SrbTestMode(p_CellId, p_TestConfig);              // enable testmode
  }

  //----------------------------------------------------------------------------

  function f_NBIOT_L2_Preamble_State2B_NB(out NB_RLC_CountsInfoList_Type p_RLC_CountsInfoList,
                                          NBIOT_CellId_Type p_CellId,
                                          NBIOT_L2_TestConfig_Type p_TestConfig,
                                          NBIOT_IDLEUPDATED_STATE_Type p_TestLoopMode,
                                          template (omit) DL_CCCH_Message_NB p_RrcConnSetup := omit,
                                          integer p_TestLoopModeRepetitions := 1,
                                          AbsoluteCellPower_Type p_MaxReferencePower := tsc_ServingCellRS_EPRE) runs on NBIOT_PTC
  { /* common preamble for L2 test cases; to bring UE into state 2B-NB: Test loop mode closed */
    
    f_NBIOT_Init(c1);
    
    if (p_MaxReferencePower != tsc_ServingCellRS_EPRE) {
      f_NBIOT_CellInfo_InitMaxReferencePower(p_CellId, p_MaxReferencePower);
    }

    //Create and configure all cells
    f_NBIOT_CellConfig_Def(p_CellId);

    if (isvalue(p_RrcConnSetup)) {            // configure RRC Connection Setup being used during RACH procedure
      f_NBIOT_SS_ConfigRRCConnectionSetup(p_CellId, valueof(p_RrcConnSetup));
    }

    f_NBIOT_L2_Enter_State2B_NB(p_RLC_CountsInfoList, p_CellId, p_TestConfig, p_TestLoopMode, p_TestLoopModeRepetitions);
  }

  //----------------------------------------------------------------------------

  function f_NBIOT_L2_Leave_State2B_NB(NBIOT_CellId_Type p_CellId,
                                       integer p_AM_VTS,
                                       integer p_AM_VRR) runs on NBIOT_PTC
  {
    f_NBIOT_L2_CPMode_SrbTestMode(p_CellId, NormalMode);
    f_NBIOT_SS_RlcCounts_Set(p_CellId, p_AM_VTS, p_AM_VRR);

    f_NBIOT_ULGrantTransmission(p_CellId, cs_NB_UL_GrantScheduling_Start);
    f_NBIOT_OpenUE_TestLoopMode(p_CellId, tsc_SRB1bis);
  }

  //----------------------------------------------------------------------------

  function f_NBIOT_L2_Postamble_State2B_NB(NBIOT_CellId_Type p_CellId,
                                           integer p_AM_VTS,
                                           integer p_AM_VRR) runs on NBIOT_PTC
  {
    f_NBIOT_L2_Leave_State2B_NB(p_CellId, p_AM_VTS, p_AM_VRR);
    f_NBIOT_Postamble(p_CellId, CONTROL_PLANE, N3_TESTMODE);
  }
}
