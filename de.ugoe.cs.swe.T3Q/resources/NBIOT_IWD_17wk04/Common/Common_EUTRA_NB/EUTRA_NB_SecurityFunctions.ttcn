/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2016-12-28 11:10:44 +0100 (Wed, 28 Dec 2016) $
// $Rev: 17692 $
/******************************************************************************/
module EUTRA_NB_SecurityFunctions {
  import from CommonDefs all;
  import from EUTRA_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from EUTRA_NB_CommonDefs all;
  import from NAS_CommonTypeDefs all;
  import from NAS_AuthenticationCommon all;
  import from NasEmu_CtrlAspTypes all;
  import from EPS_NAS_TypeDefs  all;
 

  //============================================================================
  // constants and types

  const B3_Type tsc_EPS_Integerity_Snow3G := '001'B;            /* @status    APPROVED (LTE, NBIOT) */
  const B3_Type tsc_EPS_Integerity_AES := '010'B;               /* @status    APPROVED (LTE, NBIOT) */
  const B3_Type tsc_EPS_Encryption_Snow3G := '001'B;            /* @status    APPROVED (LTE, NBIOT) */
  const B3_Type tsc_EPS_Encryption_AES := '010'B;               /* @status    APPROVED (LTE, NBIOT) */
  const B3_Type tsc_EPS_Integerity_ZUC := '011'B;               /* @status    APPROVED (LTE_A_R10_R11, NBIOT) */
  const B3_Type tsc_EPS_Encryption_ZUC := '011'B;               /* @status    APPROVED (LTE_A_R10_R11) */
  
  const integer tsc_NAS_Enc_Alg := 1;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
  const integer tsc_NAS_Int_Alg := 2;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
  const integer tsc_RRC_Enc_Alg := 3;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
  const integer tsc_RRC_Int_Alg := 4;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
  const integer tsc_UP_Enc_Alg  := 5;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */

  type integer AlgTypeDistg_Type(tsc_NAS_Enc_Alg,
                                 tsc_NAS_Int_Alg,
                                 tsc_RRC_Enc_Alg,
                                 tsc_RRC_Int_Alg,
                                 tsc_UP_Enc_Alg);      /* As per Table A.8-1 of 33.401;  @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
  
  type record VarShortMAC_Input_Type {   /* 36.331 cl. 7.1 */
    /* @status    APPROVED (LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_IRAT) */
    CellIdentity      cellIdentity,       // Bit string (28)
    PhysCellId        physCellId,         // Integer (0..503)
    C_RNTI            c_RNTI              // Bit string (16)
  };
  

  const B32_Type tsc_NonceMME := oct2bit ('11223344'O);                /* @sic R5-102227, R5-123655 Can be changed to pixit sic@
                                                                          @status    APPROVED (LTE_A_IRAT, LTE_IRAT) */
  
  //============================================================================
  // TEMPLATES
  //----------------------------------------------------------------------------

  template (value) NAS_SecurityInfo_Type cs_NAS_SecurityInfo(B3_Type p_Algo,
                                                             B128_Key_Type p_Key) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    Algorithm         := p_Algo,
    K_NAS             := p_Key
  };
  
  template (value) UE_SecurityCapability cs_NAS_SecurityCapabilityInit :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    // @sic R5s100135 sic@
    iel    := '02'O,
    eeaCap := '00'O,
    eiaCap := '00'O,
    ueaCap := omit,
    uiaCap := omit,
    geaCap := omit
  };

  //============================================================================
  // Group of S funcions defined in Annex A of 33.401
  //----------------------------------------------------------------------------
  /*
   * @desc      KASME derivation function (S10); As per annex A.2 of 33.401
   * @param     p_AuthParams
   * @param     p_KDF
   * @param     p_Ks
   * @param     p_PLMN
   * @return    B256_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_Authentication_S10(Common_AuthenticationParams_Type p_AuthParams,
                                         KDF_Type       p_KDF,
                                         B256_Type      p_Ks,
                                         NAS_PlmnId     p_PLMN) return B256_Type
  {
    const octetstring const_S10_FC := '10'O;
    var octetstring v_S;

    // Generation of String
    v_S := const_S10_FC;
    //FC = 0x10
    v_S :=  (v_S & p_PLMN);
    //P0 = serving network ID
    v_S :=  (v_S & '0003'O) ;
    //L0 = length of serving network ID (i.e. 0x00 0x03)
    v_S :=  ( v_S & ( substr (( bit2oct ( p_AuthParams.AUTN )) , 0,6 ))); // @sic R5s160711 sic@
    //P1 = SQN XOR AK
    // to have MSB 6 bytes which is SQN xor AK and truncated as SQN xor AK is first 6 bytes of AUTN
    v_S :=  ( v_S & '0006'O );
    //L1 = length of SQN XOR AK (i.e. 0x00 0x06)
    return fx_KeyDerivationFunction( p_KDF, p_Ks, v_S ); // @sic R5s160711 sic@
  };

  //--------------------------------------------------------------------------
  /*
   * @desc     Converts ASN.1 CipheringAlgorithm_Type to bit string representation
   * @param     p_Alg
   * @return    B3_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_ConvertCiphAlg2Bitstring(CipheringAlgorithm_r12 p_Alg) return B3_Type
  { /* @sic BASELINE MOVING 2015: CipheringAlgorithm_Type -> CipheringAlgorithm_r12 sic@ */
    var B3_Type v_B3 := '111'B;
    select (p_Alg) {
      case (eea0) {
        v_B3 := '000'B;
      }
      case (eea1) {
        v_B3 := '001'B;
      }
      case (eea2) {
        v_B3 := '010'B;
      }
    case (eea3_v1130) {
        v_B3 := '011'B;
      }
      case else {
        FatalError (__FILE__, __LINE__, "Non defined AS Ciphering algorithm Selected");
      }
    }
    return v_B3;
  }
  
  //--------------------------------------------------------------------------
  /*
   * @desc      Converts ASN.1 IntegrityProtAlgorithm_Type to bit string representation
   * @param     p_Alg
   * @return    B3_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_ConvertIntAlg2Bitstring(IntegrityProtAlgorithm_Type p_Alg) return B3_Type
  {
    var B3_Type v_B3 := '111'B;
    select (p_Alg) {
      case (eia1) {
        v_B3 := '001'B;
      }
      case (eia2) {
        v_B3 := '010'B;
      }
      case (eia3_v1130) {
        v_B3 := '011'B;
      }
      case else {
        FatalError (__FILE__, __LINE__, "Non defined AS Integrity algorithm Selected");
      }
    }
    return v_B3;
  }
 
  //--------------------------------------------------------------------------
  /*
   * @desc      KeNB derivation function used at ECM-IDLE to ECM CONNECTED transition,
   *            ECM-IDLE mode mobility, transition away from EMM-DEREGISTERED to EMM-REGISTERED/ECM-CONNECTED
   *            and key change on the-fly (S11)
   *            As per annex A.3 of 33.401
   * @param     p_KDF
   * @param     p_KASME
   * @param     p_UL_NAS_Count
   * @return    B256_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_Authentication_S11(KDF_Type p_KDF,
                                         B256_Type p_KASME,
                                         O4_Type p_UL_NAS_Count) return B256_Type
  {
    const octetstring const_S11_FC :='11'O;
    var octetstring v_S;
    // Generation of String
    v_S := const_S11_FC;
    //FC = 0x11
    v_S :=   ( v_S & p_UL_NAS_Count);
    //P0 = Uplink NAS COUNT
    v_S :=  ( v_S & '0004'O );
    //L0 = length of uplink NAS COUNT (i.e. 0x00 0x04)
    return fx_KeyDerivationFunction ( p_KDF, p_KASME, v_S ); // @sic R5s160711 sic@
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      NH* derivation function (S12)
   *            As per annex A.4 of 33.401
   *            The SYNC-input parameter shall be the newly derived KeNB for the initial NH derivation,
   *            and the previous NH for all subsequent derivations
   * @param     p_KDF
   * @param     p_KASME
   * @param     p_Synch
   * @return    B256_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_Authentication_S12(KDF_Type p_KDF,
                                         B256_Type p_KASME,
                                         B256_Type p_Synch) return B256_Type
  {
    const octetstring const_S12_FC :='12'O;
    var octetstring v_S;
    // Generation of String
    v_S := const_S12_FC;
    //  FC = 0x12
    v_S :=   ( v_S & bit2oct ( p_Synch ) );
    // P0 = SYNC-input
    v_S :=   ( v_S & '0020'O );
    //L0 = length of SYNC-input (i.e. 0x00 0x20)

    return fx_KeyDerivationFunction ( p_KDF, p_KASME, v_S ); // @sic R5s160711 sic@
  };

  //--------------------------------------------------------------------------
  /*
   * @desc      KeNB* derivation function (S13)
   *            As per annex A.5 of 33.401
   * @param     p_KDF
   * @param     p_KENB
   * @param     p_NH
   * @param     p_PhyCellId
   * @param     p_EARFCN_DL
   * @param     p_FromNH            (default value: false)
   * @return    B256_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_Authentication_S13(KDF_Type p_KDF,
                                         B256_Type p_KENB,
                                         B256_Type p_NH,
                                         PhysCellId p_PhyCellId,
                                         ARFCN_ValueEUTRA_r9 p_EARFCN_DL,
                                         boolean p_FromNH := false) // False for Deriving Kenb* from Kenb, else from NH
    return B256_Type //@sic R5s100496 sic@
  { // @sic R5s100138 sic@
    //@sic R5-165892 Band extension p_EARFCN_DL -> ARFCN_ValueEUTRA_r9 sic@
    const octetstring const_S13_FC :='13'O; //@sic R5s100178 sic@
    var octetstring v_S;
    var B256_Type v_SourceKey;
    // Generation of String
    v_S := const_S13_FC;
    //FC = 0x13
    v_S :=   ( v_S & int2oct ( p_PhyCellId , 2 ) );
    //P0 = PCI (target physical cell id)
    v_S :=   ( v_S & '0002'O );
    //L0 = length of PCI (i.e. 0x00 0x02)
    v_S :=   ( v_S & int2oct ( p_EARFCN_DL , 2 ) );
    //P1 = EARFCN-DL (target physical cell downlink frequency)
    if ( p_EARFCN_DL < maxEARFCN) {  //@sic R5-165892 Band extension sic@
        v_S :=   ( v_S & '0002'O );
    } else if ((p_EARFCN_DL > maxEARFCN_Plus1) and (p_EARFCN_DL < maxEARFCN2)) {
        v_S :=   ( v_S & '0003'O );
    }
    //L1 length of EARFCN-DL (i.e. L1 = 0x00 0x02 if EARFCN-DL is between 0 and 65535, and L1 = 0x00 0x03 if EARFCN-DL is between 65536 and 262143)
    if ( p_FromNH == false) //  @sic R5s100496 sic@ Kenb* derived from Kenb
      {
        v_SourceKey := p_KENB; // @sic R5s160711 sic@
      }
    else // Kenb* derived from Fresh Derived NH
      {
        v_SourceKey := p_NH; // @sic R5s160711 sic@
      };
    return fx_KeyDerivationFunction ( p_KDF, v_SourceKey, v_S );//@sic R5s100496, R5s160711 sic@
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      Algorithm key derivation functions (S15)
   *            As per annex A.7 of 33.401
   * @param     p_AlgTypeDistg
   * @param     p_Alg
   * @param     p_Key
   * @param     p_KDF_Type
   * @return    B128_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_EUTRA_NB_Authentication_S15(AlgTypeDistg_Type p_AlgTypeDistg,
                                         B3_Type p_Alg,
                                         B256_Type p_Key,
                                         KDF_Type p_KDF_Type) return B128_Type
  {
    const octetstring const_S15_FC :='15'O;
    var octetstring v_S;
    // Generation of String
    v_S := const_S15_FC;
    //FC = 0x15
    v_S :=   ( v_S & int2oct ( p_AlgTypeDistg, 1  ) );
    //P0 = algorithm type distinguisher
    v_S :=   ( v_S & '0001'O );
    //L0 = length of algorithm type distinguisher (i.e. 0x00 0x01))
    v_S :=   ( v_S & bit2oct ( p_Alg ) );
    //P1 = algorithm identity
    v_S :=   ( v_S & '0001'O );
    //L1 = length of algorithm identity (i.e. 0x00 0x01)
    
    return substr(fx_KeyDerivationFunction(p_KDF_Type, p_Key, v_S), 128, 128);
    // returns LSB 128 bits[truncated] of the key generated
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      KASME to CK, IK derivation (S16)
   *            As per annex A.8 of 33.401
   * @param     p_KDF
   * @param     p_KASME
   * @param     p_DL_NAS_Count
   * @return    B256_Type
   * @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT)
   */
  function f_EUTRA_Authentication_S16(KDF_Type p_KDF,
                                      B256_Type p_KASME,
                                      O4_Type p_DL_NAS_Count) return B256_Type
  {
    const octetstring const_S16_FC :='16'O;
    var octetstring v_S;
    
    // Generation of String
    v_S := const_S16_FC;
    //FC = 0x16
    v_S :=   ( v_S & p_DL_NAS_Count );
    //P0 = NAS downlink COUNT value
    v_S :=   ( v_S & '0004'O );
    //L0 = length of NAS downlink COUNT value (i.e. 0x00 0x04)
    
    return fx_KeyDerivationFunction ( p_KDF, p_KASME, v_S ); // @sic R5s160711 sic@
    // The result is 256 bit  CK ll IK each of 128 bits
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      NAS token derivation for inter-RAT mobility (S17)
   *            As per annex A.9 of 33.401
   * @param     p_KDF
   * @param     p_KASME
   * @param     p_UL_NAS_Count
   * @return    B256_Type
   * @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT)
   */
  function f_EUTRA_Authentication_S17(KDF_Type p_KDF,
                                      B256_Type p_KASME,
                                      O4_Type p_UL_NAS_Count) return B256_Type
  {
    const octetstring const_S17_FC :='17'O;
    var octetstring v_S;
    // Generation of String
    v_S := const_S17_FC;
    //FC = 0x17
    v_S := (v_S & p_UL_NAS_Count);
    //P0 = uplink  NAS COUNT
    v_S := (v_S & '0004'O);
    //L0 = length of downlink NAS COUNT (i.e. 0x00 0x04)
    
    return fx_KeyDerivationFunction(p_KDF, p_KASME, v_S); // @sic R5s160711 sic@
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      KASME from CK, IK derivation during handover (S18)
   *            As per annex A.10 of 33.401
   * @param     p_KDF
   * @param     p_NonceMME
   * @param     p_Auth_Params
   * @return    B256_Type
   * @status    APPROVED (LTE_A_IRAT, LTE_IRAT)
   */
  function f_EUTRA_Authentication_S18(KDF_Type p_KDF,
                                      B32_Type p_NonceMME,
                                      Common_AuthenticationParams_Type p_Auth_Params) return B256_Type
  {
    const octetstring const_S18_FC :='18'O;
    var octetstring v_S;
    
    // Generation of String
    v_S := const_S18_FC;                                 //FC = 0x18
    v_S := (v_S & bit2oct(p_NonceMME));      //P0 = NONCEMME  @sic R5s160711 sic@
    v_S := (v_S & '0004'O);                              //L0  = length of NONCEMME (i.e. 0x00 0x04)
    
    return fx_KeyDerivationFunction(p_KDF, (p_Auth_Params.CK & p_Auth_Params.IK), v_S); //The input key shall be the concatenation of CK || IK. @sic R5s160711 sic@
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      KASME from CK, IK derivation during idle mode mobility (S19)
   *            As per annex A.11 of 33.401
   * @param     p_KDF
   * @param     p_NonceUE
   * @param     p_NonceMME
   * @param     p_Auth_Params
   * @return    B256_Type
   * @status    APPROVED (LTE_A_IRAT, LTE_IRAT)
   */
  function f_EUTRA_NB_Authentication_S19(KDF_Type p_KDF,
                                         B32_Type p_NonceUE,
                                         B32_Type p_NonceMME,
                                         Common_AuthenticationParams_Type p_Auth_Params) return B256_Type
  {
    const octetstring const_S19_FC :='19'O;
    var octetstring v_S;

    // Generation of String
    v_S := const_S19_FC;
    //FC = 0x14
    v_S :=   ( v_S & bit2oct ( p_NonceUE  ) ); // @sic R5s160711 sic@
    //P0 = NONCEUE
    v_S :=   ( v_S & '0004'O );
    //L0  = length of NONCEUE (i.e. 0x00 0x04)
    v_S :=   ( v_S & bit2oct ( p_NonceMME  ) ); // @sic R5s160711 sic@
    //P1 = NONCEMME
    v_S :=   ( v_S & '0004'O );
    //L1  = length of NONCEMME (i.e. 0x00 0x04)

    return fx_KeyDerivationFunction(p_KDF, (p_Auth_Params.CK & p_Auth_Params.IK), v_S); // @sic R5s160711 sic@
    //The input key shall be the concatenation of CK || IK.
  };

  //--------------------------------------------------------------------------
  /*
   * @desc      KASME to CKSRVCC, IKSRVCC derivation (S1A)
   *            As per annex A.12 of 33.401
   * @param     p_KDF
   * @param     p_KASME
   * @param     p_DL_NAS_Count
   * @return    B256_Type
   * @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT)
   */
  function f_EUTRA_Authentication_S1A(KDF_Type p_KDF,
                                      B256_Type p_KASME,
                                      O4_Type p_DL_NAS_Count) return B256_Type
  {
    const octetstring const_S1A_FC :='1A'O;
    var octetstring v_S;
    
    // Generation of String
    v_S := const_S1A_FC;
    //FC = 0x1A
    v_S :=   ( v_S &  p_DL_NAS_Count );
    //P0 = NAS downlink COUNT value
    v_S :=   ( v_S & '0004'O );
    //L0 = length of NAS downlink COUNT value (i.e. 0x00 0x04)
    
    return fx_KeyDerivationFunction ( p_KDF, p_KASME, v_S ); // @sic R5s160711 sic@
    // The result is 256 bit  CKSRVCC ll IKSRVCC  each of 128 bits
  };
  
  //--------------------------------------------------------------------------
  /*
   * @desc      KASME to CK', IK' derivation at idle mobility(S1B)
   *            As per annex A.13 of 33.401
   * @param     p_KDF
   * @param     p_KASME
   * @param     p_UL_NAS_Count
   * @return    B256_Type
   * @status    APPROVED (IMS_IRAT, LTE_A_IRAT, LTE_IRAT)
   */
  function f_EUTRA_Authentication_S1B(KDF_Type p_KDF,
                                      B256_Type p_KASME,
                                      O4_Type p_UL_NAS_Count) return B256_Type
  {
    const octetstring const_S1B_FC :='1B'O;
    var octetstring v_S;
    
    // Generation of String
    v_S := const_S1B_FC;
    //FC = 0x1B
    v_S :=   ( v_S & p_UL_NAS_Count );
    //P0 = NAS uplink COUNT value
    v_S :=   ( v_S & '0004'O );
    //L0 = length of NAS uplink COUNT value (i.e. 0x00 0x04)
    
    return fx_KeyDerivationFunction( p_KDF, p_KASME, v_S ); // @sic R5s160711 sic@
    // The result is 256 bit  CK ll IK each of 128 bits
  };
  
}
