/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2017-01-25 18:56:42 +0100 (Wed, 25 Jan 2017) $
// $Rev: 17829 $
/******************************************************************************/

module NBIOT_Timing {
  import from CommonDefs all;
  import from NBIOT_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from EUTRA_NB_Timing all;
  import from NBIOT_CommonDefs all;
  import from EUTRA_NB_CommonDefs all;
  import from NBIOT_ASP_TypeDefs all;
  import from NBIOT_Component all;
  import from NBIOT_AspCommon_Templates all;

  type record NBIOT_SearchSpaceParameters_Type {  /* @status    APPROVED (NBIOT) */
    UInt_Type Rmax,
    UInt_Type G,
    UInt_Type Alpha
  };

  const NBIOT_SearchSpaceParameters_Type tsc_NBIOT_SearchSpaceParameters_Def := { 16, 4, 0 };  /* @status    APPROVED (NBIOT) */

  //****************************************************************************
  // Handling Timers
  //----------------------------------------------------------------------------
  /*
   * @desc      calculate timer tolerance acc. to 36.508 cl. 6.7
   * @param     p_ProtocolTimer    .. kind of timer
   * @param     p_Timer
   * @return    float .. timer value
   * @status    APPROVED (NBIOT)
   */
  function fl_NBIOT_TimerTolerance(ProtocolTimer_Type p_ProtocolTimer,
                                   float p_Timer) return float
  { /* calculate timer tolerance acc. to 36.508 cl. 6.7 */
    var float v_Tolerance;
    var float v_Rtt_x_5;
    var integer v_RTT;

    v_Tolerance := p_Timer * 0.1;

    select (p_ProtocolTimer) {
      case (rrcTimer) {            /* @sic R5-106675 change 2 - timer tolerance for RRC timers sic@ */
        v_Tolerance := f_Max(v_Tolerance, 0.140);
      }
      case (l2Timer, nasTimer) {
        v_RTT := 8;
        v_Rtt_x_5 := int2float(v_RTT) * 0.001 * 5.0;  /* RTT * TTI * 5 */
        v_Tolerance := f_Max(v_Tolerance, v_Rtt_x_5);
      }
      case else {  /* non-protocol timers; see 36.523-3 clause 7.8.4 */
        v_Tolerance := 0.0;   /* no tolerance */
      }
    }
    return v_Tolerance;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      to calculate lower bound of timer tolerance acc. to 36.508 cl. 6.7
   * @param     p_ProtocolTimer
   * @param     p_Timer
   * @return    float
   * @status    APPROVED (NBIOT)
   */
  function f_NBIOT_SetTimerToleranceMin(ProtocolTimer_Type p_ProtocolTimer,
                                        float p_Timer) return float
  {
    var float v_Tolerance := fl_NBIOT_TimerTolerance(p_ProtocolTimer, p_Timer);
    if (p_Timer < v_Tolerance) {
      return 0.0;
    }
    return p_Timer - v_Tolerance;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      calculate upper bound of timer tolerance acc. to 36.508 cl. 6.7
   * @param     p_ProtocolTimer    .. kind of timer
   * @param     p_Timer
   * @return    float .. timer value
   * @status    APPROVED (NBIOT)
   */
  function f_NBIOT_SetTimerToleranceMax(ProtocolTimer_Type p_ProtocolTimer,
                                        float p_Timer)  return float
  {
    var float v_Tolerance := fl_NBIOT_TimerTolerance(p_ProtocolTimer, p_Timer);
    return p_Timer + v_Tolerance;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      check whether given duration is valid considering expected duration and given tolerance
   * @param     p_CellId
   * @param     p_ProtocolTimer    .. kind of timer
   * @param     p_Duration         .. messured duration (in milliseconds)
   * @param     p_ExpectedDuration .. duration as specified by the standard (in milliseconds)
   * @return    boolean
   */
  function f_NBIOT_SubFrameTimingCheckDuration(ProtocolTimer_Type p_ProtocolTimer,
                                               integer p_Duration,
                                               integer p_ExpectedDuration) return boolean
  {
   
    var float v_ToleranceFloat := fl_NBIOT_TimerTolerance(p_ProtocolTimer, int2float(p_ExpectedDuration)/1000.0);
    var integer v_ToleranceInt := float2int(v_ToleranceFloat * 1000.0);
    var integer v_Min := p_ExpectedDuration - v_ToleranceInt;
    var integer v_Max := p_ExpectedDuration + v_ToleranceInt;

    return ((v_Min <= p_Duration) and (p_Duration <= v_Max));
  }

  /*
   * @desc      return PDCCH period
   * @param     p_SearchSpaceParameters (default value: tsc_NBIOT_SearchSpaceParameters_Def)
   * @return    UInt_Type
   * @status    APPROVED (NBIOT)
   */
  function f_NBIOT_SearchSpace_GetPdcchPeriod(NBIOT_SearchSpaceParameters_Type p_SearchSpaceParameters := tsc_NBIOT_SearchSpaceParameters_Def) return UInt_Type
  {
    return (p_SearchSpaceParameters.Rmax * p_SearchSpaceParameters.G);
  }

  //============================================================================
  // System Timing
  //----------------------------------------------------------------------------
  /*
   * @desc      Return the current SFN and subframe
   * @param     p_CellId
   * @return    SubFrameTiming_Type
   * @status    APPROVED (NBIOT)
   */
  function f_NBIOT_GetCurrentTiming(NBIOT_CellId_Type p_CellId) runs on NBIOT_PTC return SubFrameTiming_Type
  {
    var NB_SYSTEM_CTRL_CNF v_ReceivedCnf;
    var SubFrameTiming_Type v_Timing;

    SYS.send(cas_NB_EnquireTiming(p_CellId));
    SYS.receive(car_NB_EnquireTiming(p_CellId)) -> value v_ReceivedCnf;
    v_Timing := v_ReceivedCnf.Common.TimingInfo.SubFrame;

    return v_Timing;
  }
  
  /*
   * @desc      get the send occasion: start of the next UE specific search space which starts at least p_MilliSeconds in advance from now on
   * @param     p_CellId
   * @param     p_MilliSeconds            (default value: 100)  ...  minimum time (in ms) before the next send occasion
   * @param     p_SearchSpaceParameters   (default value: tsc_NBIOT_SearchSpaceParameters_Def)
   * @return    SubFrameTiming_Type
   * @status    APPROVED (NBIOT)
   */
  function f_NBIOT_GetNextSearchSpace(NBIOT_CellId_Type p_CellId,
                                      UInt_Type p_MilliSeconds := 100,
                                      NBIOT_SearchSpaceParameters_Type p_SearchSpaceParameters := tsc_NBIOT_SearchSpaceParameters_Def) runs on NBIOT_PTC return SubFrameTiming_Type
  {
    var SubFrameTiming_Type v_Timing;
    var SubFrameTiming_Type v_NextSearchSpace;
    var UInt_Type v_CycleLength := f_NBIOT_SearchSpace_GetPdcchPeriod(p_SearchSpaceParameters);
    var UInt_Type v_CycleOffset := p_SearchSpaceParameters.Alpha;

    if (p_MilliSeconds < 100) {
      FatalError(__FILE__, __LINE__, "next send occasion shall be at least 100ms in advance");
    }
    
    v_Timing := f_NBIOT_GetCurrentTiming(p_CellId);
    v_Timing := f_EUTRA_NB_TimingInfoAdd(v_Timing, p_MilliSeconds);
    v_NextSearchSpace := f_EUTRA_NB_CalculateCycleStartTime(v_Timing, v_CycleLength, v_CycleOffset);

    return v_NextSearchSpace;
  }

  function f_NBIOT_IncrementSearchSpace(SubFrameTiming_Type p_CurrentSearchSpace,
                                        float p_Offset := 1.0,
                                        NBIOT_SearchSpaceParameters_Type p_SearchSpaceParameters := tsc_NBIOT_SearchSpaceParameters_Def) return SubFrameTiming_Type
  { /* p_Offset as float to have possibility of half cycles */
    var UInt_Type v_CycleLength := f_NBIOT_SearchSpace_GetPdcchPeriod(p_SearchSpaceParameters);
    var UInt_Type v_MilliSeconds := float2int(int2float(v_CycleLength) * p_Offset);
    return f_EUTRA_NB_TimingInfoAdd(p_CurrentSearchSpace, v_MilliSeconds);
  }

  function f_NBIOT_TimeStamp_CheckSearchSpace(SubFrameTiming_Type p_CurrentSearchSpace,
                                              SubFrameTiming_Type p_TimeStampOfReceivedMsg,
                                              NBIOT_SearchSpaceParameters_Type p_SearchSpaceParameters := tsc_NBIOT_SearchSpaceParameters_Def) return boolean
  { /* return true, if the given time stamp is within the current search space */
    var UInt_Type v_CycleLength := f_NBIOT_SearchSpace_GetPdcchPeriod(p_SearchSpaceParameters);
    var UInt_Type v_CycleOffset := p_SearchSpaceParameters.Alpha;
    var SubFrameTiming_Type v_NextSearchSpace := f_EUTRA_NB_CalculateCycleStartTime(p_TimeStampOfReceivedMsg, v_CycleLength, v_CycleOffset);
    
    return (v_NextSearchSpace == f_NBIOT_IncrementSearchSpace(p_CurrentSearchSpace, 1.0, p_SearchSpaceParameters));
  }

}
