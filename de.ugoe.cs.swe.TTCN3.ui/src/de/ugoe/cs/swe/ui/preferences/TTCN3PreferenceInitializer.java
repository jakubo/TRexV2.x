package de.ugoe.cs.swe.ui.preferences;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile;
import de.ugoe.cs.swe.common.ConfigTools;
import de.ugoe.cs.swe.common.ConfigurationProfile;
import de.ugoe.cs.swe.ui.internal.TTCN3Activator;

/**
 * The Class GFPreferenceInitializer.
 */
public class TTCN3PreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * Logger
	 */
	private static final Logger log = Logger.getLogger(TTCN3PreferenceInitializer.class);
	
	
    public static String[] getFieldsOfType(Class<?> target, Class<?> searchType) {

        Field[] fields  = target.getDeclaredFields();
        
        List<String> results = new LinkedList<String>();
        for(Field f:fields){
//        	System.out.println(f.getName() + " -> "+f.getType());
            if(f.getType().equals(searchType)){
                results.add(f.getName());
            }
        }
        return results.toArray(new String[results.size()]);
    }

    public static String[] getBooleans(){
        return getFieldsOfType(QualityCheckProfile.class, boolean.class); 
    }
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		System.out.println("init");
		IPreferenceStore store = TTCN3Activator.getInstance().getPreferenceStore();
		
		// Set defaults from environment variables
		try {
			QualityCheckProfile activeProfile = (QualityCheckProfile) ConfigTools.getInstance().getSelectedProfile();
			
			for (String s : getBooleans()) {
				System.out.println(s);
			}
			for (String s : getBooleans()) {
				Method method = activeProfile.getClass().getDeclaredMethod("is"+(""+s.charAt(0)).toUpperCase()+s.substring(1));
				boolean v = (boolean) method.invoke(activeProfile);
				store.setDefault(s, "is"+(""+s.charAt(0)).toUpperCase()+s.substring(1));
				//store.setDefault("checkNoUninitialisedVariables", activeProfile.isCheckNoUninitialisedVariables());
			}
		} catch (Exception e) {
		}
				
		store.setDefault("log-level", "DEBUG");
		
		// Listener for changing logging  level as needed
		store.addPropertyChangeListener(new IPropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent event) {
				if (event.getProperty().equals("log-level")) {
					Level level = Level.toLevel(event.getNewValue().toString(), Level.INFO);
				}
			}
		});
	}

}