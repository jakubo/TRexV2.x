package de.ugoe.cs.swe.ui.preferences;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage;
import org.eclipse.xtext.ui.editor.preferences.fields.CheckBoxGroupFieldEditor;

import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile;
import de.ugoe.cs.swe.common.ConfigTools;
import de.ugoe.cs.swe.ui.internal.TTCN3Activator;

/**
 * Root preference page
 */
public class TTCN3RootPreferencePage extends LanguageRootPreferencePage {
	
	/**
	 * The Constant PAGE_DESCRIPTION.
	 */
	private static final String PAGE_DESCRIPTION = "Settings for the guideline checks:";
	
	/* (non-Javadoc)
	 * @see org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		Composite parent = getFieldEditorParent();
		FontData fontData = parent.getFont().getFontData()[0];
		Font fontItalic = new Font(parent.getDisplay(), new FontData(fontData.getName(), fontData.getHeight(), SWT.ITALIC));
		
		
		final IPreferenceStore store = TTCN3Activator.getInstance().getPreferenceStore();
		final QualityCheckProfile activeProfile = (QualityCheckProfile) ConfigTools.getInstance().getSelectedProfile();
		String[] booleans = TTCN3PreferenceInitializer.getBooleans();
		
		//Store profile settings (only needed once)
//		for (String s : booleans) {
//			try {
//				Method method = activeProfile.getClass().getDeclaredMethod("is"+(""+s.charAt(0)).toUpperCase()+s.substring(1));
//				boolean v = (boolean) method.invoke(activeProfile);
//				store.setValue(s, "is"+(""+s.charAt(0)).toUpperCase()+s.substring(1));
//			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			//store.setDefault("checkNoUninitialisedVariables", activeProfile.isCheckNoUninitialisedVariables());
//		}

		//Update profile from stored settings
		for (String s : booleans) {
			try {
				boolean v = store.getBoolean(s);
				Method method = activeProfile.getClass().getDeclaredMethod("set"+(""+s.charAt(0)).toUpperCase()+s.substring(1), Boolean.TYPE);
				method.invoke(activeProfile, v);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//store.setDefault("checkNoUninitialisedVariables", activeProfile.isCheckNoUninitialisedVariables());
		}

		
		String[][] settings = new String[booleans.length][2];
		int i = 0;
		//Create settings from profile
		for (String s : booleans) {
			try {
				Method method = activeProfile.getClass().getDeclaredMethod("is"+(""+s.charAt(0)).toUpperCase()+s.substring(1));
				boolean v = (boolean) method.invoke(activeProfile);
				//v = store.getBoolean(s); //from store
				settings[i] = new String[]{s,v+""};
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			i++;
		}

		addField(new CheckBoxGroupFieldEditor("checks", "Checks:", 2, settings, parent, false) {
			
			@Override
			protected boolean isSelected(String fieldName, String valueToSet) {
//				System.out.println(fieldName +" -> " + valueToSet);
				boolean v = false;
				try {
					//Fill settings from profile
					Method method = activeProfile.getClass().getDeclaredMethod("is"+(""+fieldName.charAt(0)).toUpperCase()+fieldName.substring(1));
					v = (boolean) method.invoke(activeProfile);
					//v = store.getBoolean(fieldName); //from store
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
				return v;
			}
			
			@Override
			protected String calculateResult(String[][] settings) {
				for (String[] s : settings) {
//					if (!s[1].equals(s[2])) {
						try {
							//Update settings in profile and in store
							Method method = activeProfile.getClass().getDeclaredMethod("set"+(""+s[0].charAt(0)).toUpperCase()+s[0].substring(1), Boolean.TYPE);
							method.invoke(activeProfile, Boolean.parseBoolean(s[2]));
							store.setValue(s[0],Boolean.parseBoolean(s[2])); //from store
						} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							e.printStackTrace();
						}
//					}
				}
				return "true";
			}
		});
		

		
		String[][] entryNamesAndValues = new String[][]{
			// Refer: http://logging.apache.org/log4j/1.2/manual.html
			{"All", "DEBUG"},
			{"Info", "INFO"},
			{"Warnings", "WARN"},
			{"None", "OFF"},
		};
		addField(new ComboFieldEditor("log-level", "Log Le&vel:", entryNamesAndValues, parent));
		new Label(parent, SWT.NULL); // skip cell
		Label label_LogLevel = new Label(parent, SWT.NULL);
		label_LogLevel.setFont(fontItalic);
		label_LogLevel.setText("Only affects console output, not Eclipse's internal log. ");

//		addField(new BooleanFieldEditor(GFPreferences.BUILD_DEPENDENTS, "&Build dependents of changed files", parent));
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.xtext.ui.editor.preferences.AbstractPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(TTCN3Activator.getInstance().getPreferenceStore());
		setDescription(PAGE_DESCRIPTION);
	}
	
}