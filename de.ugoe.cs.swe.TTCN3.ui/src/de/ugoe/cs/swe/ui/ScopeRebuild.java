package de.ugoe.cs.swe.ui;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.builder.BuilderParticipant;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.builder.IXtextBuilderParticipant;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.resource.IContainer;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescription.Delta;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider;

import com.google.inject.Inject;

import de.ugoe.cs.swe.common.CommonHelper;
import de.ugoe.cs.swe.common.PreAnalyzer;
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider;
import de.ugoe.cs.swe.tTCN3.TTCN3Module;
import de.ugoe.cs.swe.validation.TTCN3StatisticsProvider;

public class ScopeRebuild extends BuilderParticipant {
	@Inject
	private ResourceDescriptionsProvider resourceDescriptionsProvider;

	@Inject
	private IContainer.Manager containerManager;

	@Inject(optional = true)
	private IGenerator2 generator;

	protected ThreadLocal<Boolean> buildSemaphor = new ThreadLocal<Boolean>();

	@Override
	public void build(IBuildContext context, IProgressMonitor monitor) throws CoreException {
		buildSemaphor.set(false);
		super.build(context, monitor);
	}

	@Override
	protected void handleChangedContents(IResourceDescription.Delta delta,
			IXtextBuilderParticipant.IBuildContext context, EclipseResourceFileSystemAccess2 fileSystemAccess)
			throws org.eclipse.core.runtime.CoreException {
		super.handleChangedContents(delta, context, fileSystemAccess);
		if (!buildSemaphor.get()) {
			//invokeRebuild(delta, context, fileSystemAccess);
		}
	}

	private void invokeRebuild(Delta delta, IBuildContext context, IFileSystemAccess fileSystemAccess) {
		buildSemaphor.set(true);
		//TODO: make this context specific
		//NOTE: probably won't work without better integration
		System.out.println("Rebuilding..");
		Resource resource = context.getResourceSet().getResource(delta.getUri(), true);
//		if (shouldGenerate(resource, context)) {
//			IResourceDescriptions index = resourceDescriptionsProvider.createResourceDescriptions();
//			IResourceDescription resDesc = index.getResourceDescription(resource.getURI());
//			List<IContainer> visibleContainers = containerManager.getVisibleContainers(resDesc, index);
//			for (IContainer c : visibleContainers) {
//				for (IResourceDescription rd : c.getResourceDescriptions()) {
//					context.getResourceSet().getResource(rd.getURI(), true);
//				}
//			}
//
//			//generator.doGenerate(context.getResourceSet(), fileSystemAccess);
//		}
		
		
		TTCN3GlobalScopeProvider.RESOURCES.clear();
		TTCN3GlobalScopeProvider.NAMED_MODULES.clear();
		TTCN3GlobalScopeProvider.EXPORTED_OBJECTS.clear();
		TTCN3GlobalScopeProvider.IMPORTS.clear();
		TTCN3GlobalScopeProvider.IMPORTED_FROM.clear();
		TTCN3GlobalScopeProvider.IMPORTED_RESOURCES.clear();
		TTCN3GlobalScopeProvider.STATIC_SCOPE.clear();
		System.out.println("Reset Master : "+resource.getURI());

		for (IResourceDescription d : resourceDescriptionsProvider.getResourceDescriptions(context.getResourceSet()).getAllResourceDescriptions()) {
			Resource r = resource.getResourceSet().getResource(d.getURI(), true);
			System.out.println("Reset: "+d.getURI());
			TTCN3Module module = CommonHelper.getModule(r);
			if (!TTCN3GlobalScopeProvider.NAMED_MODULES.containsKey(module.getName())) {
				TTCN3GlobalScopeProvider.RESOURCES.add(r);
				TTCN3GlobalScopeProvider.NAMED_MODULES.put(module.getName(), module);
			}
		}

		for (IResourceDescription d : resourceDescriptionsProvider.getResourceDescriptions(context.getResourceSet()).getAllResourceDescriptions()) {
			Resource r = resource.getResourceSet().getResource(d.getURI(), true);
			TTCN3GlobalScopeProvider.initExportedObjects(r);
			System.out.println("InitExports: "+r.getURI());
		}
		
		for (int i = 0; i < TTCN3GlobalScopeProvider.RESOURCES.size(); i++) {
			PreAnalyzer p = new PreAnalyzer(TTCN3GlobalScopeProvider.RESOURCES.get(i));
			try {
				p.call();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			
		TTCN3StatisticsProvider.getInstance().setPreAnalyzingCompleted(true);

	}
}