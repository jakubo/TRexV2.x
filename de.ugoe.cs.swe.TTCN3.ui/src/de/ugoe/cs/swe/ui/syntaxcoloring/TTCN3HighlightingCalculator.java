package de.ugoe.cs.swe.ui.syntaxcoloring;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.AbstractRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator;
import org.eclipse.xtext.nodemodel.ILeafNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;

public class TTCN3HighlightingCalculator implements ISemanticHighlightingCalculator {


	@Override
	public void provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor,
			CancelIndicator cancelIndicator) {
    	if( resource == null ) {
            return;
        }
        INode root = resource.getParseResult().getRootNode();
        for(INode node : root.getAsTreeIterable()) {   
            EObject grammarElement = node.getGrammarElement(); 
            if(grammarElement instanceof RuleCall) {
                RuleCall rc = (RuleCall)grammarElement;
                AbstractRule r = rc.getRule();
                if (r instanceof TerminalRule) {
                	// It tests whether the node represents a name of an element (class, method, parameter).
//                	EObject c = grammarElement.eContainer();
//                	if(r.getName().equals("ID") && c instanceof Assignment && ((Assignment)c).getFeature().equals("name")) {
//	                	INode p = node.getParent();
//	                	if (p != null && p.getGrammarElement() instanceof RuleCall) {
//	                		rc = (RuleCall)p.getGrammarElement();
//	                		AbstractRule r = rc.getRule();
//	                		
//	                		// It tests whether the parent node represents a method.                        
//	                		if(r.getName().equals("Method")) {
//	                			acceptor.addPosition(node.getOffset(), node.getLength(), ExampleHighlightingConfiguration.METHOD_ID);
//	                		}
//	                	}
//	                }
                	if (r.getName().endsWith("KEYWORD") 
                			|| r.getName().equals("MODULE")
                			|| r.getName().equals("ADDRESSVALUE")) {
                		highlightNode( node, TTCN3HighlightingConfiguration.KEYWORD_LAYOUT, acceptor);
                	}
                }
			   
            }
        }

        Iterator<EObject> iter = EcoreUtil.getAllContents(resource, true);
        while(iter.hasNext()) {
            EObject current = iter.next();
//            System.out.println(current.eClass().getName());
//            if (current instanceof GateInstance) {
//                INode name = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getGateInstance().getEStructuralFeature("name"));
//                highlightNode( name, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getGateInstance_Type());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof GateType) {
//                INode name = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getGateType().getEStructuralFeature("name"));
//                highlightNode( name, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//                List<INode> nodes = getAllFeatureNodes( current, tdlPackage.eINSTANCE.getGateType_DataType());
//                for (INode node : nodes) {
//                	highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//                }
//            } else if (current instanceof GateReference) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getGateReference_Component());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//                INode gate = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getGateReference_Gate());
//                highlightNode( gate, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof TestConfiguration) {
//                INode name = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTestConfiguration().getEStructuralFeature("name"));
//                highlightNode( name, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTestDescription_TestConfiguration());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof ComponentInstance) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getComponentInstance().getEStructuralFeature("name"));
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
////                INode gate = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getComponentInstance_Gate());
////                highlightNode( gate, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//                INode type = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getComponentInstance_Type());
//                highlightNode( type, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof ComponentType) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getComponentType().getEStructuralFeature("name"));
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof DataInstance) {
//                INode name = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getDataInstance().getEStructuralFeature("name"));
//                highlightNode( name, TTCN3HighlightingConfiguration.DATA_LAYOUT, acceptor);
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getDataInstance_DataType());
//                highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//            } else if (current instanceof DataType) {
//            	INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getDataType().getEStructuralFeature("name"));
//              	highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//            } else if (current instanceof Parameter) {
//            	INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getParameter_DataType());
//              	highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//            } else if (current instanceof Function) {
//            	INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getFunction_ReturnType());
//              	highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//            } else if (current instanceof Function) {
//            	INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getFunction_ReturnType());
//              	highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//            } else if (current instanceof DataInstanceUse) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getDataInstanceUse_DataInstance());
//                highlightNode( node, TTCN3HighlightingConfiguration.DATA_LAYOUT, acceptor);
//            } else if (current instanceof AnyValue) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getAnyValue_DataType());
//                highlightNode( node, TTCN3HighlightingConfiguration.DATATYPE_LAYOUT, acceptor);
//            } else if (current instanceof VerdictAssignment) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getVerdictAssignment_Verdict());
//                highlightNode( node, TTCN3HighlightingConfiguration.BEHAVIOUR_LAYOUT, acceptor);
//            } else if (current instanceof Assertion) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getAssertion_Otherwise());
//                highlightNode( node, TTCN3HighlightingConfiguration.BEHAVIOUR_LAYOUT, acceptor);
//            } else if (current instanceof Interaction) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getInteraction_SourceGate());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof Target) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTarget_TargetGate());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof Quiescence) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getQuiescence_GateReference());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//                INode component = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTimeOperation_ComponentInstance());
//                highlightNode( component, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof Wait) {
//                INode component = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTimeOperation_ComponentInstance());
//                highlightNode( component, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof VariableUse) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getVariableUse_ComponentInstance());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof ActionBehaviour) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getActionBehaviour_ComponentInstance());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof TimerOperation) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTimerOperation_ComponentInstance());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            } else if (current instanceof TestDescription) {
//                INode node = getFirstFeatureNode( current, tdlPackage.eINSTANCE.getTestDescription_TestConfiguration());
//                highlightNode( node, TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT, acceptor);
//            }

        }
    }

    private void highlightNode( INode node, String id, IHighlightedPositionAcceptor acceptor ) {
        if( node == null )
            return;
        if( node instanceof ILeafNode ) {
            acceptor.addPosition( node.getOffset(), node.getLength(), id );
        } else {
            for( ILeafNode leaf : node.getLeafNodes() ) {
                if( !leaf.isHidden() ) {
                    acceptor.addPosition( leaf.getOffset(), leaf.getLength(), id );
                }
            }
        }
    }

    public INode getFirstFeatureNode( EObject semantic, EStructuralFeature feature ) {
        if( feature == null )
            return NodeModelUtils.findActualNodeFor( semantic );
        List<INode> nodes = NodeModelUtils.findNodesForFeature( semantic, feature );
        if( !nodes.isEmpty() )
            return nodes.get( 0 );
        return null;
    }    
    public List<INode> getAllFeatureNodes( EObject semantic, EStructuralFeature feature ) {
        List<INode> nodes = NodeModelUtils.findNodesForFeature( semantic, feature );
        if( !nodes.isEmpty() )
            return nodes;
        return null;
    }

}