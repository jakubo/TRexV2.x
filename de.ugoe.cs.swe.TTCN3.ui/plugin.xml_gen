<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.0"?>

<plugin>

    <extension
            point="org.eclipse.ui.editors">
        <editor
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="true"
            extensions="ttcn3,ttcn,3mp"
            id="de.ugoe.cs.swe.TTCN3"
            name="TTCN3 Editor">
        </editor>
    </extension>
    <extension
        point="org.eclipse.ui.handlers">
        <handler
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
            commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
            <activeWhen>
                <reference
                    definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
        <handler
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
            commandId="de.ugoe.cs.swe.TTCN3.validate">
         <activeWhen>
            <reference
                    definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened">
            </reference>
         </activeWhen>
      	</handler>
      	<!-- copy qualified name -->
        <handler
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName">
            <activeWhen>
				<reference definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened" />
            </activeWhen>
        </handler>
        <handler
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName">
            <activeWhen>
            	<and>
            		<reference definitionId="de.ugoe.cs.swe.TTCN3.XtextEditor.opened" />
	                <iterate>
						<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
					</iterate>
				</and>
            </activeWhen>
        </handler>
    </extension>
    <extension point="org.eclipse.core.expressions.definitions">
        <definition id="de.ugoe.cs.swe.TTCN3.Editor.opened">
            <and>
                <reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="de.ugoe.cs.swe.TTCN3" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
        <definition id="de.ugoe.cs.swe.TTCN3.XtextEditor.opened">
            <and>
                <reference definitionId="isXtextEditorActive"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="de.ugoe.cs.swe.TTCN3" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
    </extension>
    <extension
            point="org.eclipse.ui.preferencePages">
        <page
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="de.ugoe.cs.swe.TTCN3"
            name="TTCN3">
            <keywordReference id="de.ugoe.cs.swe.ui.keyword_TTCN3"/>
        </page>
        <page
            category="de.ugoe.cs.swe.TTCN3"
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
            id="de.ugoe.cs.swe.TTCN3.coloring"
            name="Syntax Coloring">
            <keywordReference id="de.ugoe.cs.swe.ui.keyword_TTCN3"/>
        </page>
        <page
            category="de.ugoe.cs.swe.TTCN3"
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
            id="de.ugoe.cs.swe.TTCN3.templates"
            name="Templates">
            <keywordReference id="de.ugoe.cs.swe.ui.keyword_TTCN3"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="de.ugoe.cs.swe.TTCN3"
            name="TTCN3">
            <keywordReference id="de.ugoe.cs.swe.ui.keyword_TTCN3"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension
        point="org.eclipse.ui.keywords">
        <keyword
            id="de.ugoe.cs.swe.ui.keyword_TTCN3"
            label="TTCN3"/>
    </extension>
    <extension
         point="org.eclipse.ui.commands">
      <command
            description="Trigger expensive validation"
            id="de.ugoe.cs.swe.TTCN3.validate"
            name="Validate">
      </command>
      <!-- copy qualified name -->
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
    </extension>
    <extension point="org.eclipse.ui.menus">
        <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
             <command
                 commandId="de.ugoe.cs.swe.TTCN3.validate"
                 style="push"
                 tooltip="Trigger expensive validation">
            <visibleWhen checkEnabled="false">
                <reference
                    definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened">
                </reference>
            </visibleWhen>
         </command>  
         </menuContribution>
         <!-- copy qualified name -->
         <menuContribution locationURI="popup:#TextEditorContext?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName" 
         		style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="menu:edit?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            	style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="popup:org.eclipse.xtext.ui.outline?after=additions">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName" 
				style="push" tooltip="Copy Qualified Name">
         		<visibleWhen checkEnabled="false">
	            	<and>
	            		<reference definitionId="de.ugoe.cs.swe.TTCN3.XtextEditor.opened" />
						<iterate>
							<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
						</iterate>
					</and>
				</visibleWhen>
			</command>
         </menuContribution>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
                	<reference definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened">
                	</reference>
            	</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
	    <handler
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
            commandId="org.eclipse.xtext.ui.editor.FindReferences">
            <activeWhen>
                <reference
                    definitionId="de.ugoe.cs.swe.TTCN3.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
    </extension>   

<!-- adding resource factories -->

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="ttcn3">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="ttcn3">
        </resourceServiceProvider>
    </extension>

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="ttcn">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="ttcn">
        </resourceServiceProvider>
    </extension>

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="3mp">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="de.ugoe.cs.swe.ui.TTCN3ExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="3mp">
        </resourceServiceProvider>
    </extension>


	<!-- marker definitions for de.ugoe.cs.swe.TTCN3 -->
	<extension
	        id="ttcn3.check.fast"
	        name="TTCN3 Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.fast"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="ttcn3.check.normal"
	        name="TTCN3 Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.normal"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="ttcn3.check.expensive"
	        name="TTCN3 Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.expensive"/>
	    <persistent value="true"/>
	</extension>

</plugin>
