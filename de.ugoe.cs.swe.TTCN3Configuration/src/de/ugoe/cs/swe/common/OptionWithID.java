package de.ugoe.cs.swe.common;

import org.apache.commons.cli.Option;

public class OptionWithID extends Option {

	private static final long serialVersionUID = -2643329397818768355L;
	private int id;

	public OptionWithID(String opt, String longOpt, boolean hasArg, String description) throws IllegalArgumentException {
		super(opt, longOpt, hasArg, description);
	}

	public OptionWithID(String longOpt, boolean hasArg, String description) throws IllegalArgumentException {
		super(null, longOpt, hasArg, description);
	}

	public OptionWithID(int id, String longOpt, boolean hasArg, String description) throws IllegalArgumentException {
		super(null, longOpt, hasArg, description);
		this.setId(id);
	}

	public OptionWithID(int id, String longOpt, String description) throws IllegalArgumentException {
		super(null, description);
		this.setLongOpt(longOpt);
		this.setId(id);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}
}
