package de.ugoe.cs.swe.common;

public class T3QOptionsHandler extends OptionsHandler {

	public T3QOptionsHandler() {
		super();
		//this.initializeCustomOptions();
	}

	private void initializeCustomOptions() {
		OptionWithID optionWithID;
		optionWithID = new OptionWithID(21, "format", "Apply code formatting\n");
		getOptions().addOption(optionWithID);
	}
}
