package de.ugoe.cs.swe.common.logging;

import java.util.HashMap;

import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass;


//TODO: currently not in use
public class LoggingStatistics {
	private HashMap<MessageClass, Integer> statsMap = new HashMap<MessageClass, Integer>();
	
	public LoggingStatistics(){
		for (MessageClass c : MessageClass.values()){
			this.statsMap.put(c, 0);
		}
	}
	
	public void addOccurence(MessageClass c){
		this.statsMap.put(c, this.statsMap.get(c) + 1);
	}
	
	public int getOccurences(MessageClass c){
		return this.statsMap.get(c);
	}
}
