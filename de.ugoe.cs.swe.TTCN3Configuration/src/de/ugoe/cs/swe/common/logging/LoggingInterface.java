package de.ugoe.cs.swe.common.logging;

import java.io.File;

import org.eclipse.emf.common.util.URI;

public class LoggingInterface {

	public enum LogLevel {
		// ORDER MATTERS
		ERROR, WARNING, INFORMATION, FIXME, DEBUG;
	}

	public enum MessageClass {
		// ORDER MATTERS
		UNIVERSAL("Universal", 0), 
		GENERAL("General", 1), 
		NAMING("Naming Conventions", 2), 
		DOCUMENTATION("Code Documentation", 3), 
		LOGGING("Log Statements", 4),
		STRUCTURE("Structure of Data", 5), 
		STYLE("Code Style", 6),
		MODULARIZATION("Test Suite Modularization", 7);

		private final String description;
		private final int id;
		private int occurenceCount = 0;

		MessageClass(String description, int id) {
			this.description = description;
			this.id = id;
		}

		public String getDescription() {
			return description;
		}

		public int getId() {
			return id;
		}

		public void addOccurence() {
			this.occurenceCount++;
		}

		public int getOccurenceCount() {
			return occurenceCount;
		}
	}

	private String logSourceName = "";
	private LogLevel maximumLogLevel = LogLevel.INFORMATION;
	private LogLevel defaultLogLevel = LogLevel.INFORMATION;
	private LoggingConfiguration configuration = new LoggingConfiguration();

	public LoggingInterface(LoggingConfiguration logConfiguration) {
		this.setConfiguration(logConfiguration);
	}

	public LoggingInterface(String logSourceName, LoggingConfiguration logConfiguration) {
		this(logConfiguration);
		this.setLogSourceName(logSourceName);
	}

	public LoggingInterface(String logSourceName, LoggingConfiguration logConfiguration, LogLevel maximumLogLevel) {
		this(logSourceName, logConfiguration);
		this.setMaximumLogLevel(maximumLogLevel);
	}

	private LogMessage createLogMessage(URI uri, LogLevel l, int startLine, int endLine, MessageClass messageClass, String message, String details) {
		LogMessage logMessage = new LogMessage();
		logMessage.setPrefix(this.getConfiguration().getLogOutputPrefix());

		if (uri != null && this.getConfiguration().isShowFilename() && this.getConfiguration().isShowFullPath()) {
			logMessage.setFilename(uri.devicePath().replaceFirst("///", ""));
		} else if (uri != null && this.getConfiguration().isShowFilename()) {
			String path = uri.devicePath();
			File file = new File(path);
			logMessage.setFilename(file.getName());
		}
		logMessage.setStartLine(startLine);
		logMessage.setEndLine(endLine);
		logMessage.setLogLevel(l);

		messageClass.addOccurence();

		if (this.getConfiguration().isShowMessageClass()) {
			logMessage.setMessageClass(messageClass);
		} else {
			logMessage.setMessageClass(null);
		}

		logMessage.setMessage(message);

		if (this.getConfiguration().isShowDetails() && details != null) {
			logMessage.setDetails(details);
		} else {
			logMessage.setDetails(null);
		}

		return logMessage;
	}

	/*
	 * private String composeLogMessage(LogLevel l, int startLine, int endLine,
	 * MessageClass messageClass, String message, String details){ String
	 * composedLogMessage = "";
	 * 
	 * composedLogMessage += this.getConfiguration().getLogOutputPrefix();
	 * 
	 * if (this.getConfiguration().isShowFullPath()) { composedLogMessage +=
	 * this.getLogSourceName() + ": "; } else { if
	 * (this.getConfiguration().isShowFilename()) { composedLogMessage +=
	 * MiscTools.getShortFilename(this .getLogSourceName()) + ": "; } }
	 * 
	 * if (startLine >= endLine) { composedLogMessage += startLine + ": "; }
	 * else { composedLogMessage += startLine + "-" + endLine + ": "; }
	 * 
	 * composedLogMessage += l + ": ";
	 * 
	 * if (this.getConfiguration().isShowMessageClass()) { composedLogMessage +=
	 * messageClass.getDescription() + ": "; }
	 * 
	 * composedLogMessage += message;
	 * 
	 * if (this.getConfiguration().isShowDetails() && details != null) {
	 * composedLogMessage += "(" + details + ")"; }
	 * 
	 * return composedLogMessage; }
	 */

	public void logMessage(URI uri, LogLevel l, int startLine, int endLine, MessageClass messageClass, String message, String details) {
		if (showLogLevel(l)) {
			LogMessage composedLogMessage = createLogMessage(uri, l, startLine, endLine, messageClass, message, details);
			outputLog(composedLogMessage);
		}
	}

	public void logMessage(URI uri, LogLevel l, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(l)) {
			LogMessage composedLogMessage = createLogMessage(uri, l, startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}
	}

	public void logMessage(URI uri, int startLine, int endLine, MessageClass messageClass, String message, String details) {
		if (showLogLevel(this.getDefaultLogLevel())) {
			LogMessage composedLogMessage = createLogMessage(uri, this.getDefaultLogLevel(), startLine, endLine, messageClass, message, details);
			outputLog(composedLogMessage);
		}
	}

	public void logMessage(URI uri, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(this.getDefaultLogLevel())) {
			LogMessage composedLogMessage = createLogMessage(uri, this.getDefaultLogLevel(), startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}
	}

	// private void outputLog(String composedLogMessage){
	// System.out.println(composedLogMessage);
	// }

	private void outputLog(LogMessage composedLogMessage) {
		System.out.println(composedLogMessage.toString());
	}

	public void logWarning(URI uri, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(LogLevel.WARNING)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.WARNING, startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}
	}

	public void logWarning(URI uri, int startLine, int endLine, MessageClass messageClass, String message, String details) {
		if (showLogLevel(LogLevel.WARNING)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.WARNING, startLine, endLine, messageClass, message, details);
			outputLog(composedLogMessage);
		}

	}

	public void logDebug(URI uri, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(LogLevel.DEBUG)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.DEBUG, startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}

	}

	public void logDebug(URI uri, int startLine, int endLine, MessageClass messageClass, String message, String details) {
		if (showLogLevel(LogLevel.DEBUG)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.DEBUG, startLine, endLine, messageClass, message, details);
			outputLog(composedLogMessage);
		}

	}

	public void logFix(URI uri, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(LogLevel.FIXME)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.FIXME, startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}

	}

	public void logFix(URI uri, int startLine, int endLine, MessageClass messageClass, String message, String details) {
		if (showLogLevel(LogLevel.FIXME)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.FIXME, startLine, endLine, messageClass, message, details);
			outputLog(composedLogMessage);
		}

	}

	public void logError(URI uri, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(LogLevel.ERROR)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.ERROR, startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}
	}

	public void logInformation(URI uri, int startLine, int endLine, MessageClass messageClass, String message) {
		if (showLogLevel(LogLevel.INFORMATION)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.INFORMATION, startLine, endLine, messageClass, message, null);
			outputLog(composedLogMessage);
		}
	}

	public void logInformation(URI uri, int startLine, int endLine, MessageClass messageClass, String message, String detail) {
		if (showLogLevel(LogLevel.INFORMATION)) {
			LogMessage composedLogMessage = createLogMessage(uri, LogLevel.INFORMATION, startLine, endLine, messageClass, message, detail);
			outputLog(composedLogMessage);
		}
	}

	public String getLogSourceName() {
		return logSourceName;
	}

	public void setMaximumLogLevel(LogLevel maximumLogLevel) {
		this.maximumLogLevel = maximumLogLevel;
	}

	public LogLevel getMaximumLogLevel() {
		return maximumLogLevel;
	}

	public boolean showLogLevel(LogLevel logLevel) {
		boolean show = false;
		if (logLevel.ordinal() <= this.getMaximumLogLevel().ordinal()) {
			show = true;
		}
		return show;
	}

	public void setConfiguration(LoggingConfiguration configuration) {
		this.configuration = configuration;
	}

	public LoggingConfiguration getConfiguration() {
		return configuration;
	}

	public void setDefaultLogLevel(LogLevel defaultLogLevel) {
		this.defaultLogLevel = defaultLogLevel;
	}

	public LogLevel getDefaultLogLevel() {
		return defaultLogLevel;
	}

	public void setLogSourceName(String logSourceName) {
		this.logSourceName = logSourceName;
	}

}
