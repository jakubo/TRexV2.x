package de.ugoe.cs.swe.common;

import org.apache.commons.cli.Options;

public class OptionsHandler {

	private Options options = new Options();

	public OptionsHandler() {
		this.initialize();
	}

	private void initialize() {

		OptionWithID optionWithID;

		optionWithID = new OptionWithID(9, "generate-config", "Generate a new default configuration file at the specified file location");
		optionWithID.setArgs(1);
		optionWithID.setArgName("FILE NAME");
		getOptions().addOption(optionWithID);

		optionWithID = new OptionWithID(10, "config", "Configuration file location\n");
		optionWithID.setArgs(1);
		optionWithID.setArgName("FILE NAME");
		// optionWithID.setRequired(true);
		getOptions().addOption(optionWithID);

		optionWithID = new OptionWithID(20, "profile", "Configuration profile\n");
		optionWithID.setArgs(1);
		optionWithID.setArgName("PROFILE NAME");
		getOptions().addOption(optionWithID);

		optionWithID = new OptionWithID(30, "verbosity", "Verbosity level (currently supports ERROR, WARNING and INFORMATION values)\n");
		optionWithID.setArgs(1);
		optionWithID.setArgName("LOG LEVEL");
		getOptions().addOption(optionWithID);

		optionWithID = new OptionWithID(40, "output-path",
				"Destination path for the output (if applicable, otherwise ignored). Overrides the profile setting.\n");
		optionWithID.setArgs(1);
		optionWithID.setArgName("PATH");
		getOptions().addOption(optionWithID);

		optionWithID = new OptionWithID(100, "help", "Show this usage information screen\n");
		getOptions().addOption(optionWithID);

		optionWithID = new OptionWithID(200, "local-dependencies", "Generate local dependencies\n");
		getOptions().addOption(optionWithID);
	}

	public void setOptions(Options options) {
		this.options = options;
	}

	public Options getOptions() {
		return options;
	}

}