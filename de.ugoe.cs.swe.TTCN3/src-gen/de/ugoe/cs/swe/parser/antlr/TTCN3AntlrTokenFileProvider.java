/*
 * generated by Xtext
 */
package de.ugoe.cs.swe.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class TTCN3AntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("de/ugoe/cs/swe/parser/antlr/internal/InternalTTCN3.tokens");
	}
}
