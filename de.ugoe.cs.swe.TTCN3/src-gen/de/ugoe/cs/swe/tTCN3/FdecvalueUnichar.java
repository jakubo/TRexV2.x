/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fdecvalue Unichar</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE1 <em>E1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE2 <em>E2</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE3 <em>E3</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFdecvalueUnichar()
 * @model
 * @generated
 */
public interface FdecvalueUnichar extends PreDefFunction
{
  /**
   * Returns the value of the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E1</em>' containment reference.
   * @see #setE1(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFdecvalueUnichar_E1()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE1 <em>E1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E1</em>' containment reference.
   * @see #getE1()
   * @generated
   */
  void setE1(SingleExpression value);

  /**
   * Returns the value of the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E2</em>' containment reference.
   * @see #setE2(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFdecvalueUnichar_E2()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE2();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE2 <em>E2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E2</em>' containment reference.
   * @see #getE2()
   * @generated
   */
  void setE2(SingleExpression value);

  /**
   * Returns the value of the '<em><b>E3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E3</em>' containment reference.
   * @see #setE3(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFdecvalueUnichar_E3()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE3();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE3 <em>E3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E3</em>' containment reference.
   * @see #getE3()
   * @generated
   */
  void setE3(SingleExpression value);

} // FdecvalueUnichar
