/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Statement Or Def List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getLocalDef <em>Local Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getLocalInst <em>Local Inst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getWithstm <em>Withstm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getControl <em>Control</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDefList()
 * @model
 * @generated
 */
public interface ControlStatementOrDefList extends EObject
{
  /**
   * Returns the value of the '<em><b>Local Def</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local Def</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local Def</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDefList_LocalDef()
   * @model containment="true"
   * @generated
   */
  EList<FunctionLocalDef> getLocalDef();

  /**
   * Returns the value of the '<em><b>Local Inst</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionLocalInst}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local Inst</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local Inst</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDefList_LocalInst()
   * @model containment="true"
   * @generated
   */
  EList<FunctionLocalInst> getLocalInst();

  /**
   * Returns the value of the '<em><b>Withstm</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.WithStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Withstm</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Withstm</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDefList_Withstm()
   * @model containment="true"
   * @generated
   */
  EList<WithStatement> getWithstm();

  /**
   * Returns the value of the '<em><b>Control</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ControlStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Control</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Control</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDefList_Control()
   * @model containment="true"
   * @generated
   */
  EList<ControlStatement> getControl();

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDefList_Sc()
   * @model unique="false"
   * @generated
   */
  EList<String> getSc();

} // ControlStatementOrDefList
