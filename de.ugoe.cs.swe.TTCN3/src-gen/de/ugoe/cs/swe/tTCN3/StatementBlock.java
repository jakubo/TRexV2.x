/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StatementBlock#getDef <em>Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StatementBlock#getStat <em>Stat</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStatementBlock()
 * @model
 * @generated
 */
public interface StatementBlock extends EObject
{
  /**
   * Returns the value of the '<em><b>Def</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionDefList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Def</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Def</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStatementBlock_Def()
   * @model containment="true"
   * @generated
   */
  EList<FunctionDefList> getDef();

  /**
   * Returns the value of the '<em><b>Stat</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionStatementList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stat</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stat</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStatementBlock_Stat()
   * @model containment="true"
   * @generated
   */
  EList<FunctionStatementList> getStat();

} // StatementBlock
