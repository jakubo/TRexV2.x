/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getSimple <em>Simple</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getField <em>Field</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getExtra <em>Extra</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateBody()
 * @model
 * @generated
 */
public interface TemplateBody extends EObject
{
  /**
   * Returns the value of the '<em><b>Simple</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Simple</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Simple</em>' containment reference.
   * @see #setSimple(SimpleSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateBody_Simple()
   * @model containment="true"
   * @generated
   */
  SimpleSpec getSimple();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getSimple <em>Simple</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Simple</em>' containment reference.
   * @see #getSimple()
   * @generated
   */
  void setSimple(SimpleSpec value);

  /**
   * Returns the value of the '<em><b>Field</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' containment reference.
   * @see #setField(FieldSpecList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateBody_Field()
   * @model containment="true"
   * @generated
   */
  FieldSpecList getField();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getField <em>Field</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field</em>' containment reference.
   * @see #getField()
   * @generated
   */
  void setField(FieldSpecList value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayValueOrAttrib)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateBody_Array()
   * @model containment="true"
   * @generated
   */
  ArrayValueOrAttrib getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayValueOrAttrib value);

  /**
   * Returns the value of the '<em><b>Extra</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extra</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extra</em>' containment reference.
   * @see #setExtra(ExtraMatchingAttributes)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateBody_Extra()
   * @model containment="true"
   * @generated
   */
  ExtraMatchingAttributes getExtra();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getExtra <em>Extra</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Extra</em>' containment reference.
   * @see #getExtra()
   * @generated
   */
  void setExtra(ExtraMatchingAttributes value);

} // TemplateBody
