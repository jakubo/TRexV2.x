/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Construct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getForStm <em>For Stm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getWhileStm <em>While Stm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getDoStm <em>Do Stm</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getLoopConstruct()
 * @model
 * @generated
 */
public interface LoopConstruct extends EObject
{
  /**
   * Returns the value of the '<em><b>For Stm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>For Stm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>For Stm</em>' containment reference.
   * @see #setForStm(ForStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getLoopConstruct_ForStm()
   * @model containment="true"
   * @generated
   */
  ForStatement getForStm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getForStm <em>For Stm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>For Stm</em>' containment reference.
   * @see #getForStm()
   * @generated
   */
  void setForStm(ForStatement value);

  /**
   * Returns the value of the '<em><b>While Stm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>While Stm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>While Stm</em>' containment reference.
   * @see #setWhileStm(WhileStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getLoopConstruct_WhileStm()
   * @model containment="true"
   * @generated
   */
  WhileStatement getWhileStm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getWhileStm <em>While Stm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>While Stm</em>' containment reference.
   * @see #getWhileStm()
   * @generated
   */
  void setWhileStm(WhileStatement value);

  /**
   * Returns the value of the '<em><b>Do Stm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Do Stm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Do Stm</em>' containment reference.
   * @see #setDoStm(DoWhileStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getLoopConstruct_DoStm()
   * @model containment="true"
   * @generated
   */
  DoWhileStatement getDoStm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getDoStm <em>Do Stm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Do Stm</em>' containment reference.
   * @see #getDoStm()
   * @generated
   */
  void setDoStm(DoWhileStatement value);

} // LoopConstruct
