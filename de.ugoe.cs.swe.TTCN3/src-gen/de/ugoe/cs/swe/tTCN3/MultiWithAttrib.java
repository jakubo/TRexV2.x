/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi With Attrib</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MultiWithAttrib#getSingle <em>Single</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMultiWithAttrib()
 * @model
 * @generated
 */
public interface MultiWithAttrib extends EObject
{
  /**
   * Returns the value of the '<em><b>Single</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.SingleWithAttrib}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Single</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Single</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMultiWithAttrib_Single()
   * @model containment="true"
   * @generated
   */
  EList<SingleWithAttrib> getSingle();

} // MultiWithAttrib
