/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activate Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ActivateOp#getAltstep <em>Altstep</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getActivateOp()
 * @model
 * @generated
 */
public interface ActivateOp extends EObject
{
  /**
   * Returns the value of the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Altstep</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Altstep</em>' containment reference.
   * @see #setAltstep(AltstepInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getActivateOp_Altstep()
   * @model containment="true"
   * @generated
   */
  AltstepInstance getAltstep();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ActivateOp#getAltstep <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Altstep</em>' containment reference.
   * @see #getAltstep()
   * @generated
   */
  void setAltstep(AltstepInstance value);

} // ActivateOp
