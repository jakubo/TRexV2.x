/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Config Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigSpec#getRunsOn <em>Runs On</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigSpec#getSystemSpec <em>System Spec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigSpec()
 * @model
 * @generated
 */
public interface ConfigSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Runs On</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Runs On</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Runs On</em>' containment reference.
   * @see #setRunsOn(RunsOnSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigSpec_RunsOn()
   * @model containment="true"
   * @generated
   */
  RunsOnSpec getRunsOn();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigSpec#getRunsOn <em>Runs On</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Runs On</em>' containment reference.
   * @see #getRunsOn()
   * @generated
   */
  void setRunsOn(RunsOnSpec value);

  /**
   * Returns the value of the '<em><b>System Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System Spec</em>' containment reference.
   * @see #setSystemSpec(SystemSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigSpec_SystemSpec()
   * @model containment="true"
   * @generated
   */
  SystemSpec getSystemSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigSpec#getSystemSpec <em>System Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System Spec</em>' containment reference.
   * @see #getSystemSpec()
   * @generated
   */
  void setSystemSpec(SystemSpec value);

} // ConfigSpec
