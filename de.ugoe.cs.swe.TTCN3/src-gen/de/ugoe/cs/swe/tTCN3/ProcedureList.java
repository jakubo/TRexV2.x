/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ProcedureList#getAllOrSigList <em>All Or Sig List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcedureList()
 * @model
 * @generated
 */
public interface ProcedureList extends EObject
{
  /**
   * Returns the value of the '<em><b>All Or Sig List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All Or Sig List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All Or Sig List</em>' containment reference.
   * @see #setAllOrSigList(AllOrSignatureList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcedureList_AllOrSigList()
   * @model containment="true"
   * @generated
   */
  AllOrSignatureList getAllOrSigList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ProcedureList#getAllOrSigList <em>All Or Sig List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All Or Sig List</em>' containment reference.
   * @see #getAllOrSigList()
   * @generated
   */
  void setAllOrSigList(AllOrSignatureList value);

} // ProcedureList
