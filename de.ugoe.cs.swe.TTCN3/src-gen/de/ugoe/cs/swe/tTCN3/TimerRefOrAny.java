/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Ref Or Any</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAny()
 * @model
 * @generated
 */
public interface TimerRefOrAny extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(TimerVarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAny_Ref()
   * @model
   * @generated
   */
  TimerVarInstance getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(TimerVarInstance value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' attribute.
   * @see #setTimer(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAny_Timer()
   * @model
   * @generated
   */
  String getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getTimer <em>Timer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' attribute.
   * @see #getTimer()
   * @generated
   */
  void setTimer(String value);

  /**
   * Returns the value of the '<em><b>From</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>From</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>From</em>' attribute.
   * @see #setFrom(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAny_From()
   * @model
   * @generated
   */
  String getFrom();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getFrom <em>From</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>From</em>' attribute.
   * @see #getFrom()
   * @generated
   */
  void setFrom(String value);

} // TimerRefOrAny
