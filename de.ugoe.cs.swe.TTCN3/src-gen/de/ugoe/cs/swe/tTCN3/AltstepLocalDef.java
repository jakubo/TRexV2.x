/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Altstep Local Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getVariable <em>Variable</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getConst <em>Const</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDef()
 * @model
 * @generated
 */
public interface AltstepLocalDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' containment reference.
   * @see #setVariable(VarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDef_Variable()
   * @model containment="true"
   * @generated
   */
  VarInstance getVariable();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getVariable <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' containment reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(VarInstance value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(TimerInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDef_Timer()
   * @model containment="true"
   * @generated
   */
  TimerInstance getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerInstance value);

  /**
   * Returns the value of the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const</em>' containment reference.
   * @see #setConst(ConstDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDef_Const()
   * @model containment="true"
   * @generated
   */
  ConstDef getConst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getConst <em>Const</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const</em>' containment reference.
   * @see #getConst()
   * @generated
   */
  void setConst(ConstDef value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(TemplateDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDef_Template()
   * @model containment="true"
   * @generated
   */
  TemplateDef getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(TemplateDef value);

} // AltstepLocalDef
