/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equal Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getEqualExpression()
 * @model
 * @generated
 */
public interface EqualExpression extends SingleExpression
{
} // EqualExpression
