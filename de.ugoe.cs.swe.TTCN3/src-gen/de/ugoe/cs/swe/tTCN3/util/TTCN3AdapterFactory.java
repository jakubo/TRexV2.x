/**
 */
package de.ugoe.cs.swe.tTCN3.util;

import de.ugoe.cs.swe.tTCN3.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package
 * @generated
 */
public class TTCN3AdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static TTCN3Package modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3AdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = TTCN3Package.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TTCN3Switch<Adapter> modelSwitch =
    new TTCN3Switch<Adapter>()
    {
      @Override
      public Adapter caseTTCN3File(TTCN3File object)
      {
        return createTTCN3FileAdapter();
      }
      @Override
      public Adapter caseConstDef(ConstDef object)
      {
        return createConstDefAdapter();
      }
      @Override
      public Adapter caseType(Type object)
      {
        return createTypeAdapter();
      }
      @Override
      public Adapter caseTypeReferenceTailType(TypeReferenceTailType object)
      {
        return createTypeReferenceTailTypeAdapter();
      }
      @Override
      public Adapter caseTypeReferenceTail(TypeReferenceTail object)
      {
        return createTypeReferenceTailAdapter();
      }
      @Override
      public Adapter caseTypeReference(TypeReference object)
      {
        return createTypeReferenceAdapter();
      }
      @Override
      public Adapter caseSpecTypeElement(SpecTypeElement object)
      {
        return createSpecTypeElementAdapter();
      }
      @Override
      public Adapter caseArrayDef(ArrayDef object)
      {
        return createArrayDefAdapter();
      }
      @Override
      public Adapter caseTTCN3Module(TTCN3Module object)
      {
        return createTTCN3ModuleAdapter();
      }
      @Override
      public Adapter caseLanguageSpec(LanguageSpec object)
      {
        return createLanguageSpecAdapter();
      }
      @Override
      public Adapter caseModuleParDef(ModuleParDef object)
      {
        return createModuleParDefAdapter();
      }
      @Override
      public Adapter caseModulePar(ModulePar object)
      {
        return createModuleParAdapter();
      }
      @Override
      public Adapter caseModuleParList(ModuleParList object)
      {
        return createModuleParListAdapter();
      }
      @Override
      public Adapter caseModuleParameter(ModuleParameter object)
      {
        return createModuleParameterAdapter();
      }
      @Override
      public Adapter caseMultitypedModuleParList(MultitypedModuleParList object)
      {
        return createMultitypedModuleParListAdapter();
      }
      @Override
      public Adapter caseModuleDefinitionsList(ModuleDefinitionsList object)
      {
        return createModuleDefinitionsListAdapter();
      }
      @Override
      public Adapter caseModuleDefinition(ModuleDefinition object)
      {
        return createModuleDefinitionAdapter();
      }
      @Override
      public Adapter caseFriendModuleDef(FriendModuleDef object)
      {
        return createFriendModuleDefAdapter();
      }
      @Override
      public Adapter caseGroupDef(GroupDef object)
      {
        return createGroupDefAdapter();
      }
      @Override
      public Adapter caseExtFunctionDef(ExtFunctionDef object)
      {
        return createExtFunctionDefAdapter();
      }
      @Override
      public Adapter caseExtConstDef(ExtConstDef object)
      {
        return createExtConstDefAdapter();
      }
      @Override
      public Adapter caseIdentifierObjectList(IdentifierObjectList object)
      {
        return createIdentifierObjectListAdapter();
      }
      @Override
      public Adapter caseNamedObject(NamedObject object)
      {
        return createNamedObjectAdapter();
      }
      @Override
      public Adapter caseImportDef(ImportDef object)
      {
        return createImportDefAdapter();
      }
      @Override
      public Adapter caseAllWithExcepts(AllWithExcepts object)
      {
        return createAllWithExceptsAdapter();
      }
      @Override
      public Adapter caseExceptsDef(ExceptsDef object)
      {
        return createExceptsDefAdapter();
      }
      @Override
      public Adapter caseExceptSpec(ExceptSpec object)
      {
        return createExceptSpecAdapter();
      }
      @Override
      public Adapter caseExceptElement(ExceptElement object)
      {
        return createExceptElementAdapter();
      }
      @Override
      public Adapter caseIdentifierListOrAll(IdentifierListOrAll object)
      {
        return createIdentifierListOrAllAdapter();
      }
      @Override
      public Adapter caseExceptGroupSpec(ExceptGroupSpec object)
      {
        return createExceptGroupSpecAdapter();
      }
      @Override
      public Adapter caseExceptTypeDefSpec(ExceptTypeDefSpec object)
      {
        return createExceptTypeDefSpecAdapter();
      }
      @Override
      public Adapter caseExceptTemplateSpec(ExceptTemplateSpec object)
      {
        return createExceptTemplateSpecAdapter();
      }
      @Override
      public Adapter caseExceptConstSpec(ExceptConstSpec object)
      {
        return createExceptConstSpecAdapter();
      }
      @Override
      public Adapter caseExceptTestcaseSpec(ExceptTestcaseSpec object)
      {
        return createExceptTestcaseSpecAdapter();
      }
      @Override
      public Adapter caseExceptAltstepSpec(ExceptAltstepSpec object)
      {
        return createExceptAltstepSpecAdapter();
      }
      @Override
      public Adapter caseExceptFunctionSpec(ExceptFunctionSpec object)
      {
        return createExceptFunctionSpecAdapter();
      }
      @Override
      public Adapter caseExceptSignatureSpec(ExceptSignatureSpec object)
      {
        return createExceptSignatureSpecAdapter();
      }
      @Override
      public Adapter caseExceptModuleParSpec(ExceptModuleParSpec object)
      {
        return createExceptModuleParSpecAdapter();
      }
      @Override
      public Adapter caseImportSpec(ImportSpec object)
      {
        return createImportSpecAdapter();
      }
      @Override
      public Adapter caseImportElement(ImportElement object)
      {
        return createImportElementAdapter();
      }
      @Override
      public Adapter caseImportGroupSpec(ImportGroupSpec object)
      {
        return createImportGroupSpecAdapter();
      }
      @Override
      public Adapter caseIdentifierListOrAllWithExcept(IdentifierListOrAllWithExcept object)
      {
        return createIdentifierListOrAllWithExceptAdapter();
      }
      @Override
      public Adapter caseAllWithExcept(AllWithExcept object)
      {
        return createAllWithExceptAdapter();
      }
      @Override
      public Adapter caseImportTypeDefSpec(ImportTypeDefSpec object)
      {
        return createImportTypeDefSpecAdapter();
      }
      @Override
      public Adapter caseImportTemplateSpec(ImportTemplateSpec object)
      {
        return createImportTemplateSpecAdapter();
      }
      @Override
      public Adapter caseImportConstSpec(ImportConstSpec object)
      {
        return createImportConstSpecAdapter();
      }
      @Override
      public Adapter caseImportAltstepSpec(ImportAltstepSpec object)
      {
        return createImportAltstepSpecAdapter();
      }
      @Override
      public Adapter caseImportTestcaseSpec(ImportTestcaseSpec object)
      {
        return createImportTestcaseSpecAdapter();
      }
      @Override
      public Adapter caseImportFunctionSpec(ImportFunctionSpec object)
      {
        return createImportFunctionSpecAdapter();
      }
      @Override
      public Adapter caseImportSignatureSpec(ImportSignatureSpec object)
      {
        return createImportSignatureSpecAdapter();
      }
      @Override
      public Adapter caseImportModuleParSpec(ImportModuleParSpec object)
      {
        return createImportModuleParSpecAdapter();
      }
      @Override
      public Adapter caseGroupRefListWithExcept(GroupRefListWithExcept object)
      {
        return createGroupRefListWithExceptAdapter();
      }
      @Override
      public Adapter caseQualifiedIdentifierWithExcept(QualifiedIdentifierWithExcept object)
      {
        return createQualifiedIdentifierWithExceptAdapter();
      }
      @Override
      public Adapter caseAllGroupsWithExcept(AllGroupsWithExcept object)
      {
        return createAllGroupsWithExceptAdapter();
      }
      @Override
      public Adapter caseAltstepDef(AltstepDef object)
      {
        return createAltstepDefAdapter();
      }
      @Override
      public Adapter caseAltstepLocalDefList(AltstepLocalDefList object)
      {
        return createAltstepLocalDefListAdapter();
      }
      @Override
      public Adapter caseAltstepLocalDef(AltstepLocalDef object)
      {
        return createAltstepLocalDefAdapter();
      }
      @Override
      public Adapter caseTestcaseDef(TestcaseDef object)
      {
        return createTestcaseDefAdapter();
      }
      @Override
      public Adapter caseTemplateOrValueFormalParList(TemplateOrValueFormalParList object)
      {
        return createTemplateOrValueFormalParListAdapter();
      }
      @Override
      public Adapter caseTemplateOrValueFormalPar(TemplateOrValueFormalPar object)
      {
        return createTemplateOrValueFormalParAdapter();
      }
      @Override
      public Adapter caseConfigSpec(ConfigSpec object)
      {
        return createConfigSpecAdapter();
      }
      @Override
      public Adapter caseSystemSpec(SystemSpec object)
      {
        return createSystemSpecAdapter();
      }
      @Override
      public Adapter caseSignatureDef(SignatureDef object)
      {
        return createSignatureDefAdapter();
      }
      @Override
      public Adapter caseExceptionSpec(ExceptionSpec object)
      {
        return createExceptionSpecAdapter();
      }
      @Override
      public Adapter caseSignatureFormalParList(SignatureFormalParList object)
      {
        return createSignatureFormalParListAdapter();
      }
      @Override
      public Adapter caseModuleControlPart(ModuleControlPart object)
      {
        return createModuleControlPartAdapter();
      }
      @Override
      public Adapter caseModuleControlBody(ModuleControlBody object)
      {
        return createModuleControlBodyAdapter();
      }
      @Override
      public Adapter caseControlStatementOrDefList(ControlStatementOrDefList object)
      {
        return createControlStatementOrDefListAdapter();
      }
      @Override
      public Adapter caseControlStatementOrDef(ControlStatementOrDef object)
      {
        return createControlStatementOrDefAdapter();
      }
      @Override
      public Adapter caseControlStatement(ControlStatement object)
      {
        return createControlStatementAdapter();
      }
      @Override
      public Adapter caseSUTStatements(SUTStatements object)
      {
        return createSUTStatementsAdapter();
      }
      @Override
      public Adapter caseActionText(ActionText object)
      {
        return createActionTextAdapter();
      }
      @Override
      public Adapter caseBehaviourStatements(BehaviourStatements object)
      {
        return createBehaviourStatementsAdapter();
      }
      @Override
      public Adapter caseActivateOp(ActivateOp object)
      {
        return createActivateOpAdapter();
      }
      @Override
      public Adapter caseDeactivateStatement(DeactivateStatement object)
      {
        return createDeactivateStatementAdapter();
      }
      @Override
      public Adapter caseLabelStatement(LabelStatement object)
      {
        return createLabelStatementAdapter();
      }
      @Override
      public Adapter caseGotoStatement(GotoStatement object)
      {
        return createGotoStatementAdapter();
      }
      @Override
      public Adapter caseReturnStatement(ReturnStatement object)
      {
        return createReturnStatementAdapter();
      }
      @Override
      public Adapter caseInterleavedConstruct(InterleavedConstruct object)
      {
        return createInterleavedConstructAdapter();
      }
      @Override
      public Adapter caseInterleavedGuardList(InterleavedGuardList object)
      {
        return createInterleavedGuardListAdapter();
      }
      @Override
      public Adapter caseInterleavedGuardElement(InterleavedGuardElement object)
      {
        return createInterleavedGuardElementAdapter();
      }
      @Override
      public Adapter caseInterleavedGuard(InterleavedGuard object)
      {
        return createInterleavedGuardAdapter();
      }
      @Override
      public Adapter caseAltConstruct(AltConstruct object)
      {
        return createAltConstructAdapter();
      }
      @Override
      public Adapter caseAltGuardList(AltGuardList object)
      {
        return createAltGuardListAdapter();
      }
      @Override
      public Adapter caseElseStatement(ElseStatement object)
      {
        return createElseStatementAdapter();
      }
      @Override
      public Adapter caseGuardStatement(GuardStatement object)
      {
        return createGuardStatementAdapter();
      }
      @Override
      public Adapter caseGuardOp(GuardOp object)
      {
        return createGuardOpAdapter();
      }
      @Override
      public Adapter caseDoneStatement(DoneStatement object)
      {
        return createDoneStatementAdapter();
      }
      @Override
      public Adapter caseKilledStatement(KilledStatement object)
      {
        return createKilledStatementAdapter();
      }
      @Override
      public Adapter caseComponentOrAny(ComponentOrAny object)
      {
        return createComponentOrAnyAdapter();
      }
      @Override
      public Adapter caseIndexAssignment(IndexAssignment object)
      {
        return createIndexAssignmentAdapter();
      }
      @Override
      public Adapter caseIndexSpec(IndexSpec object)
      {
        return createIndexSpecAdapter();
      }
      @Override
      public Adapter caseGetReplyStatement(GetReplyStatement object)
      {
        return createGetReplyStatementAdapter();
      }
      @Override
      public Adapter caseCheckStatement(CheckStatement object)
      {
        return createCheckStatementAdapter();
      }
      @Override
      public Adapter casePortCheckOp(PortCheckOp object)
      {
        return createPortCheckOpAdapter();
      }
      @Override
      public Adapter caseCheckParameter(CheckParameter object)
      {
        return createCheckParameterAdapter();
      }
      @Override
      public Adapter caseRedirectPresent(RedirectPresent object)
      {
        return createRedirectPresentAdapter();
      }
      @Override
      public Adapter caseFromClausePresent(FromClausePresent object)
      {
        return createFromClausePresentAdapter();
      }
      @Override
      public Adapter caseCheckPortOpsPresent(CheckPortOpsPresent object)
      {
        return createCheckPortOpsPresentAdapter();
      }
      @Override
      public Adapter casePortGetReplyOp(PortGetReplyOp object)
      {
        return createPortGetReplyOpAdapter();
      }
      @Override
      public Adapter casePortRedirectWithValueAndParam(PortRedirectWithValueAndParam object)
      {
        return createPortRedirectWithValueAndParamAdapter();
      }
      @Override
      public Adapter caseRedirectWithValueAndParamSpec(RedirectWithValueAndParamSpec object)
      {
        return createRedirectWithValueAndParamSpecAdapter();
      }
      @Override
      public Adapter caseValueMatchSpec(ValueMatchSpec object)
      {
        return createValueMatchSpecAdapter();
      }
      @Override
      public Adapter caseCatchStatement(CatchStatement object)
      {
        return createCatchStatementAdapter();
      }
      @Override
      public Adapter casePortCatchOp(PortCatchOp object)
      {
        return createPortCatchOpAdapter();
      }
      @Override
      public Adapter caseCatchOpParameter(CatchOpParameter object)
      {
        return createCatchOpParameterAdapter();
      }
      @Override
      public Adapter caseGetCallStatement(GetCallStatement object)
      {
        return createGetCallStatementAdapter();
      }
      @Override
      public Adapter casePortGetCallOp(PortGetCallOp object)
      {
        return createPortGetCallOpAdapter();
      }
      @Override
      public Adapter casePortRedirectWithParam(PortRedirectWithParam object)
      {
        return createPortRedirectWithParamAdapter();
      }
      @Override
      public Adapter caseRedirectWithParamSpec(RedirectWithParamSpec object)
      {
        return createRedirectWithParamSpecAdapter();
      }
      @Override
      public Adapter caseParamSpec(ParamSpec object)
      {
        return createParamSpecAdapter();
      }
      @Override
      public Adapter caseParamAssignmentList(ParamAssignmentList object)
      {
        return createParamAssignmentListAdapter();
      }
      @Override
      public Adapter caseAssignmentList(AssignmentList object)
      {
        return createAssignmentListAdapter();
      }
      @Override
      public Adapter caseVariableAssignment(VariableAssignment object)
      {
        return createVariableAssignmentAdapter();
      }
      @Override
      public Adapter caseVariableList(VariableList object)
      {
        return createVariableListAdapter();
      }
      @Override
      public Adapter caseVariableEntry(VariableEntry object)
      {
        return createVariableEntryAdapter();
      }
      @Override
      public Adapter caseTriggerStatement(TriggerStatement object)
      {
        return createTriggerStatementAdapter();
      }
      @Override
      public Adapter casePortTriggerOp(PortTriggerOp object)
      {
        return createPortTriggerOpAdapter();
      }
      @Override
      public Adapter caseReceiveStatement(ReceiveStatement object)
      {
        return createReceiveStatementAdapter();
      }
      @Override
      public Adapter casePortOrAny(PortOrAny object)
      {
        return createPortOrAnyAdapter();
      }
      @Override
      public Adapter casePortReceiveOp(PortReceiveOp object)
      {
        return createPortReceiveOpAdapter();
      }
      @Override
      public Adapter caseFromClause(FromClause object)
      {
        return createFromClauseAdapter();
      }
      @Override
      public Adapter caseAddressRefList(AddressRefList object)
      {
        return createAddressRefListAdapter();
      }
      @Override
      public Adapter casePortRedirect(PortRedirect object)
      {
        return createPortRedirectAdapter();
      }
      @Override
      public Adapter caseSenderSpec(SenderSpec object)
      {
        return createSenderSpecAdapter();
      }
      @Override
      public Adapter caseValueSpec(ValueSpec object)
      {
        return createValueSpecAdapter();
      }
      @Override
      public Adapter caseSingleValueSpec(SingleValueSpec object)
      {
        return createSingleValueSpecAdapter();
      }
      @Override
      public Adapter caseAltGuardChar(AltGuardChar object)
      {
        return createAltGuardCharAdapter();
      }
      @Override
      public Adapter caseAltstepInstance(AltstepInstance object)
      {
        return createAltstepInstanceAdapter();
      }
      @Override
      public Adapter caseFunctionInstance(FunctionInstance object)
      {
        return createFunctionInstanceAdapter();
      }
      @Override
      public Adapter caseFunctionRef(FunctionRef object)
      {
        return createFunctionRefAdapter();
      }
      @Override
      public Adapter caseFunctionActualParList(FunctionActualParList object)
      {
        return createFunctionActualParListAdapter();
      }
      @Override
      public Adapter caseFunctionActualParAssignment(FunctionActualParAssignment object)
      {
        return createFunctionActualParAssignmentAdapter();
      }
      @Override
      public Adapter caseComponentRefAssignment(ComponentRefAssignment object)
      {
        return createComponentRefAssignmentAdapter();
      }
      @Override
      public Adapter caseFormalPortAndValuePar(FormalPortAndValuePar object)
      {
        return createFormalPortAndValueParAdapter();
      }
      @Override
      public Adapter casePortRefAssignment(PortRefAssignment object)
      {
        return createPortRefAssignmentAdapter();
      }
      @Override
      public Adapter caseTimerRefAssignment(TimerRefAssignment object)
      {
        return createTimerRefAssignmentAdapter();
      }
      @Override
      public Adapter caseFunctionActualPar(FunctionActualPar object)
      {
        return createFunctionActualParAdapter();
      }
      @Override
      public Adapter caseComponentRef(ComponentRef object)
      {
        return createComponentRefAdapter();
      }
      @Override
      public Adapter caseComponentOrDefaultReference(ComponentOrDefaultReference object)
      {
        return createComponentOrDefaultReferenceAdapter();
      }
      @Override
      public Adapter caseTestcaseInstance(TestcaseInstance object)
      {
        return createTestcaseInstanceAdapter();
      }
      @Override
      public Adapter caseTestcaseActualParList(TestcaseActualParList object)
      {
        return createTestcaseActualParListAdapter();
      }
      @Override
      public Adapter caseTimerStatements(TimerStatements object)
      {
        return createTimerStatementsAdapter();
      }
      @Override
      public Adapter caseTimeoutStatement(TimeoutStatement object)
      {
        return createTimeoutStatementAdapter();
      }
      @Override
      public Adapter caseStartTimerStatement(StartTimerStatement object)
      {
        return createStartTimerStatementAdapter();
      }
      @Override
      public Adapter caseStopTimerStatement(StopTimerStatement object)
      {
        return createStopTimerStatementAdapter();
      }
      @Override
      public Adapter caseTimerRefOrAny(TimerRefOrAny object)
      {
        return createTimerRefOrAnyAdapter();
      }
      @Override
      public Adapter caseTimerRefOrAll(TimerRefOrAll object)
      {
        return createTimerRefOrAllAdapter();
      }
      @Override
      public Adapter caseBasicStatements(BasicStatements object)
      {
        return createBasicStatementsAdapter();
      }
      @Override
      public Adapter caseStatementBlock(StatementBlock object)
      {
        return createStatementBlockAdapter();
      }
      @Override
      public Adapter caseFunctionStatementList(FunctionStatementList object)
      {
        return createFunctionStatementListAdapter();
      }
      @Override
      public Adapter caseFunctionStatement(FunctionStatement object)
      {
        return createFunctionStatementAdapter();
      }
      @Override
      public Adapter caseTestcaseOperation(TestcaseOperation object)
      {
        return createTestcaseOperationAdapter();
      }
      @Override
      public Adapter caseSetLocalVerdict(SetLocalVerdict object)
      {
        return createSetLocalVerdictAdapter();
      }
      @Override
      public Adapter caseConfigurationStatements(ConfigurationStatements object)
      {
        return createConfigurationStatementsAdapter();
      }
      @Override
      public Adapter caseKillTCStatement(KillTCStatement object)
      {
        return createKillTCStatementAdapter();
      }
      @Override
      public Adapter caseStopTCStatement(StopTCStatement object)
      {
        return createStopTCStatementAdapter();
      }
      @Override
      public Adapter caseComponentReferenceOrLiteral(ComponentReferenceOrLiteral object)
      {
        return createComponentReferenceOrLiteralAdapter();
      }
      @Override
      public Adapter caseStartTCStatement(StartTCStatement object)
      {
        return createStartTCStatementAdapter();
      }
      @Override
      public Adapter caseUnmapStatement(UnmapStatement object)
      {
        return createUnmapStatementAdapter();
      }
      @Override
      public Adapter caseDisconnectStatement(DisconnectStatement object)
      {
        return createDisconnectStatementAdapter();
      }
      @Override
      public Adapter caseAllConnectionsSpec(AllConnectionsSpec object)
      {
        return createAllConnectionsSpecAdapter();
      }
      @Override
      public Adapter caseAllPortsSpec(AllPortsSpec object)
      {
        return createAllPortsSpecAdapter();
      }
      @Override
      public Adapter caseMapStatement(MapStatement object)
      {
        return createMapStatementAdapter();
      }
      @Override
      public Adapter caseParamClause(ParamClause object)
      {
        return createParamClauseAdapter();
      }
      @Override
      public Adapter caseConnectStatement(ConnectStatement object)
      {
        return createConnectStatementAdapter();
      }
      @Override
      public Adapter caseSingleConnectionSpec(SingleConnectionSpec object)
      {
        return createSingleConnectionSpecAdapter();
      }
      @Override
      public Adapter casePortRef(PortRef object)
      {
        return createPortRefAdapter();
      }
      @Override
      public Adapter caseCommunicationStatements(CommunicationStatements object)
      {
        return createCommunicationStatementsAdapter();
      }
      @Override
      public Adapter caseCheckStateStatement(CheckStateStatement object)
      {
        return createCheckStateStatementAdapter();
      }
      @Override
      public Adapter casePortOrAllAny(PortOrAllAny object)
      {
        return createPortOrAllAnyAdapter();
      }
      @Override
      public Adapter caseHaltStatement(HaltStatement object)
      {
        return createHaltStatementAdapter();
      }
      @Override
      public Adapter caseStartStatement(StartStatement object)
      {
        return createStartStatementAdapter();
      }
      @Override
      public Adapter caseStopStatement(StopStatement object)
      {
        return createStopStatementAdapter();
      }
      @Override
      public Adapter caseClearStatement(ClearStatement object)
      {
        return createClearStatementAdapter();
      }
      @Override
      public Adapter casePortOrAll(PortOrAll object)
      {
        return createPortOrAllAdapter();
      }
      @Override
      public Adapter caseRaiseStatement(RaiseStatement object)
      {
        return createRaiseStatementAdapter();
      }
      @Override
      public Adapter casePortRaiseOp(PortRaiseOp object)
      {
        return createPortRaiseOpAdapter();
      }
      @Override
      public Adapter caseReplyStatement(ReplyStatement object)
      {
        return createReplyStatementAdapter();
      }
      @Override
      public Adapter casePortReplyOp(PortReplyOp object)
      {
        return createPortReplyOpAdapter();
      }
      @Override
      public Adapter caseReplyValue(ReplyValue object)
      {
        return createReplyValueAdapter();
      }
      @Override
      public Adapter caseCallStatement(CallStatement object)
      {
        return createCallStatementAdapter();
      }
      @Override
      public Adapter casePortCallOp(PortCallOp object)
      {
        return createPortCallOpAdapter();
      }
      @Override
      public Adapter caseCallParameters(CallParameters object)
      {
        return createCallParametersAdapter();
      }
      @Override
      public Adapter caseCallTimerValue(CallTimerValue object)
      {
        return createCallTimerValueAdapter();
      }
      @Override
      public Adapter casePortCallBody(PortCallBody object)
      {
        return createPortCallBodyAdapter();
      }
      @Override
      public Adapter caseCallBodyStatementList(CallBodyStatementList object)
      {
        return createCallBodyStatementListAdapter();
      }
      @Override
      public Adapter caseCallBodyStatement(CallBodyStatement object)
      {
        return createCallBodyStatementAdapter();
      }
      @Override
      public Adapter caseCallBodyGuard(CallBodyGuard object)
      {
        return createCallBodyGuardAdapter();
      }
      @Override
      public Adapter caseCallBodyOps(CallBodyOps object)
      {
        return createCallBodyOpsAdapter();
      }
      @Override
      public Adapter caseSendStatement(SendStatement object)
      {
        return createSendStatementAdapter();
      }
      @Override
      public Adapter casePortSendOp(PortSendOp object)
      {
        return createPortSendOpAdapter();
      }
      @Override
      public Adapter caseToClause(ToClause object)
      {
        return createToClauseAdapter();
      }
      @Override
      public Adapter caseFunctionDefList(FunctionDefList object)
      {
        return createFunctionDefListAdapter();
      }
      @Override
      public Adapter caseFunctionLocalDef(FunctionLocalDef object)
      {
        return createFunctionLocalDefAdapter();
      }
      @Override
      public Adapter caseFunctionLocalInst(FunctionLocalInst object)
      {
        return createFunctionLocalInstAdapter();
      }
      @Override
      public Adapter caseTimerInstance(TimerInstance object)
      {
        return createTimerInstanceAdapter();
      }
      @Override
      public Adapter caseVarInstance(VarInstance object)
      {
        return createVarInstanceAdapter();
      }
      @Override
      public Adapter caseVarList(VarList object)
      {
        return createVarListAdapter();
      }
      @Override
      public Adapter caseModuleOrGroup(ModuleOrGroup object)
      {
        return createModuleOrGroupAdapter();
      }
      @Override
      public Adapter caseReferencedType(ReferencedType object)
      {
        return createReferencedTypeAdapter();
      }
      @Override
      public Adapter caseTypeDef(TypeDef object)
      {
        return createTypeDefAdapter();
      }
      @Override
      public Adapter caseTypeDefBody(TypeDefBody object)
      {
        return createTypeDefBodyAdapter();
      }
      @Override
      public Adapter caseSubTypeDef(SubTypeDef object)
      {
        return createSubTypeDefAdapter();
      }
      @Override
      public Adapter caseSubTypeDefNamed(SubTypeDefNamed object)
      {
        return createSubTypeDefNamedAdapter();
      }
      @Override
      public Adapter caseStructuredTypeDef(StructuredTypeDef object)
      {
        return createStructuredTypeDefAdapter();
      }
      @Override
      public Adapter caseRecordDef(RecordDef object)
      {
        return createRecordDefAdapter();
      }
      @Override
      public Adapter caseRecordDefNamed(RecordDefNamed object)
      {
        return createRecordDefNamedAdapter();
      }
      @Override
      public Adapter caseRecordOfDef(RecordOfDef object)
      {
        return createRecordOfDefAdapter();
      }
      @Override
      public Adapter caseRecordOfDefNamed(RecordOfDefNamed object)
      {
        return createRecordOfDefNamedAdapter();
      }
      @Override
      public Adapter caseStructDefBody(StructDefBody object)
      {
        return createStructDefBodyAdapter();
      }
      @Override
      public Adapter caseStructFieldDef(StructFieldDef object)
      {
        return createStructFieldDefAdapter();
      }
      @Override
      public Adapter caseSetOfDef(SetOfDef object)
      {
        return createSetOfDefAdapter();
      }
      @Override
      public Adapter caseSetOfDefNamed(SetOfDefNamed object)
      {
        return createSetOfDefNamedAdapter();
      }
      @Override
      public Adapter casePortDef(PortDef object)
      {
        return createPortDefAdapter();
      }
      @Override
      public Adapter casePortDefBody(PortDefBody object)
      {
        return createPortDefBodyAdapter();
      }
      @Override
      public Adapter casePortDefAttribs(PortDefAttribs object)
      {
        return createPortDefAttribsAdapter();
      }
      @Override
      public Adapter caseMixedAttribs(MixedAttribs object)
      {
        return createMixedAttribsAdapter();
      }
      @Override
      public Adapter caseMixedList(MixedList object)
      {
        return createMixedListAdapter();
      }
      @Override
      public Adapter caseProcOrTypeList(ProcOrTypeList object)
      {
        return createProcOrTypeListAdapter();
      }
      @Override
      public Adapter caseProcOrType(ProcOrType object)
      {
        return createProcOrTypeAdapter();
      }
      @Override
      public Adapter caseMessageAttribs(MessageAttribs object)
      {
        return createMessageAttribsAdapter();
      }
      @Override
      public Adapter caseConfigParamDef(ConfigParamDef object)
      {
        return createConfigParamDefAdapter();
      }
      @Override
      public Adapter caseMapParamDef(MapParamDef object)
      {
        return createMapParamDefAdapter();
      }
      @Override
      public Adapter caseUnmapParamDef(UnmapParamDef object)
      {
        return createUnmapParamDefAdapter();
      }
      @Override
      public Adapter caseAddressDecl(AddressDecl object)
      {
        return createAddressDeclAdapter();
      }
      @Override
      public Adapter caseProcedureAttribs(ProcedureAttribs object)
      {
        return createProcedureAttribsAdapter();
      }
      @Override
      public Adapter caseComponentDef(ComponentDef object)
      {
        return createComponentDefAdapter();
      }
      @Override
      public Adapter caseComponentDefList(ComponentDefList object)
      {
        return createComponentDefListAdapter();
      }
      @Override
      public Adapter casePortInstance(PortInstance object)
      {
        return createPortInstanceAdapter();
      }
      @Override
      public Adapter casePortElement(PortElement object)
      {
        return createPortElementAdapter();
      }
      @Override
      public Adapter caseComponentElementDef(ComponentElementDef object)
      {
        return createComponentElementDefAdapter();
      }
      @Override
      public Adapter caseProcedureList(ProcedureList object)
      {
        return createProcedureListAdapter();
      }
      @Override
      public Adapter caseAllOrSignatureList(AllOrSignatureList object)
      {
        return createAllOrSignatureListAdapter();
      }
      @Override
      public Adapter caseSignatureList(SignatureList object)
      {
        return createSignatureListAdapter();
      }
      @Override
      public Adapter caseEnumDef(EnumDef object)
      {
        return createEnumDefAdapter();
      }
      @Override
      public Adapter caseEnumDefNamed(EnumDefNamed object)
      {
        return createEnumDefNamedAdapter();
      }
      @Override
      public Adapter caseNestedTypeDef(NestedTypeDef object)
      {
        return createNestedTypeDefAdapter();
      }
      @Override
      public Adapter caseNestedRecordDef(NestedRecordDef object)
      {
        return createNestedRecordDefAdapter();
      }
      @Override
      public Adapter caseNestedUnionDef(NestedUnionDef object)
      {
        return createNestedUnionDefAdapter();
      }
      @Override
      public Adapter caseNestedSetDef(NestedSetDef object)
      {
        return createNestedSetDefAdapter();
      }
      @Override
      public Adapter caseNestedRecordOfDef(NestedRecordOfDef object)
      {
        return createNestedRecordOfDefAdapter();
      }
      @Override
      public Adapter caseNestedSetOfDef(NestedSetOfDef object)
      {
        return createNestedSetOfDefAdapter();
      }
      @Override
      public Adapter caseNestedEnumDef(NestedEnumDef object)
      {
        return createNestedEnumDefAdapter();
      }
      @Override
      public Adapter caseMessageList(MessageList object)
      {
        return createMessageListAdapter();
      }
      @Override
      public Adapter caseAllOrTypeList(AllOrTypeList object)
      {
        return createAllOrTypeListAdapter();
      }
      @Override
      public Adapter caseTypeList(TypeList object)
      {
        return createTypeListAdapter();
      }
      @Override
      public Adapter caseUnionDef(UnionDef object)
      {
        return createUnionDefAdapter();
      }
      @Override
      public Adapter caseUnionDefNamed(UnionDefNamed object)
      {
        return createUnionDefNamedAdapter();
      }
      @Override
      public Adapter caseUnionDefBody(UnionDefBody object)
      {
        return createUnionDefBodyAdapter();
      }
      @Override
      public Adapter caseEnumerationList(EnumerationList object)
      {
        return createEnumerationListAdapter();
      }
      @Override
      public Adapter caseEnumeration(Enumeration object)
      {
        return createEnumerationAdapter();
      }
      @Override
      public Adapter caseUnionFieldDef(UnionFieldDef object)
      {
        return createUnionFieldDefAdapter();
      }
      @Override
      public Adapter caseSetDef(SetDef object)
      {
        return createSetDefAdapter();
      }
      @Override
      public Adapter caseSetDefNamed(SetDefNamed object)
      {
        return createSetDefNamedAdapter();
      }
      @Override
      public Adapter caseSubTypeSpec(SubTypeSpec object)
      {
        return createSubTypeSpecAdapter();
      }
      @Override
      public Adapter caseAllowedValuesSpec(AllowedValuesSpec object)
      {
        return createAllowedValuesSpecAdapter();
      }
      @Override
      public Adapter caseTemplateOrRange(TemplateOrRange object)
      {
        return createTemplateOrRangeAdapter();
      }
      @Override
      public Adapter caseRangeDef(RangeDef object)
      {
        return createRangeDefAdapter();
      }
      @Override
      public Adapter caseBound(Bound object)
      {
        return createBoundAdapter();
      }
      @Override
      public Adapter caseStringLength(StringLength object)
      {
        return createStringLengthAdapter();
      }
      @Override
      public Adapter caseCharStringMatch(CharStringMatch object)
      {
        return createCharStringMatchAdapter();
      }
      @Override
      public Adapter casePatternParticle(PatternParticle object)
      {
        return createPatternParticleAdapter();
      }
      @Override
      public Adapter caseTemplateDef(TemplateDef object)
      {
        return createTemplateDefAdapter();
      }
      @Override
      public Adapter caseDerivedDef(DerivedDef object)
      {
        return createDerivedDefAdapter();
      }
      @Override
      public Adapter caseBaseTemplate(BaseTemplate object)
      {
        return createBaseTemplateAdapter();
      }
      @Override
      public Adapter caseTempVarList(TempVarList object)
      {
        return createTempVarListAdapter();
      }
      @Override
      public Adapter caseTimerVarInstance(TimerVarInstance object)
      {
        return createTimerVarInstanceAdapter();
      }
      @Override
      public Adapter caseSingleVarInstance(SingleVarInstance object)
      {
        return createSingleVarInstanceAdapter();
      }
      @Override
      public Adapter caseSingleTempVarInstance(SingleTempVarInstance object)
      {
        return createSingleTempVarInstanceAdapter();
      }
      @Override
      public Adapter caseInLineTemplate(InLineTemplate object)
      {
        return createInLineTemplateAdapter();
      }
      @Override
      public Adapter caseDerivedRefWithParList(DerivedRefWithParList object)
      {
        return createDerivedRefWithParListAdapter();
      }
      @Override
      public Adapter caseTemplateBody(TemplateBody object)
      {
        return createTemplateBodyAdapter();
      }
      @Override
      public Adapter caseExtraMatchingAttributes(ExtraMatchingAttributes object)
      {
        return createExtraMatchingAttributesAdapter();
      }
      @Override
      public Adapter caseFieldSpecList(FieldSpecList object)
      {
        return createFieldSpecListAdapter();
      }
      @Override
      public Adapter caseFieldSpec(FieldSpec object)
      {
        return createFieldSpecAdapter();
      }
      @Override
      public Adapter caseSimpleSpec(SimpleSpec object)
      {
        return createSimpleSpecAdapter();
      }
      @Override
      public Adapter caseSimpleTemplateSpec(SimpleTemplateSpec object)
      {
        return createSimpleTemplateSpecAdapter();
      }
      @Override
      public Adapter caseSingleTemplateExpression(SingleTemplateExpression object)
      {
        return createSingleTemplateExpressionAdapter();
      }
      @Override
      public Adapter caseTemplateRefWithParList(TemplateRefWithParList object)
      {
        return createTemplateRefWithParListAdapter();
      }
      @Override
      public Adapter caseTemplateActualParList(TemplateActualParList object)
      {
        return createTemplateActualParListAdapter();
      }
      @Override
      public Adapter caseMatchingSymbol(MatchingSymbol object)
      {
        return createMatchingSymbolAdapter();
      }
      @Override
      public Adapter caseSubsetMatch(SubsetMatch object)
      {
        return createSubsetMatchAdapter();
      }
      @Override
      public Adapter caseSupersetMatch(SupersetMatch object)
      {
        return createSupersetMatchAdapter();
      }
      @Override
      public Adapter caseRange(Range object)
      {
        return createRangeAdapter();
      }
      @Override
      public Adapter caseWildcardLengthMatch(WildcardLengthMatch object)
      {
        return createWildcardLengthMatchAdapter();
      }
      @Override
      public Adapter caseComplement(Complement object)
      {
        return createComplementAdapter();
      }
      @Override
      public Adapter caseListOfTemplates(ListOfTemplates object)
      {
        return createListOfTemplatesAdapter();
      }
      @Override
      public Adapter caseTemplateOps(TemplateOps object)
      {
        return createTemplateOpsAdapter();
      }
      @Override
      public Adapter caseMatchOp(MatchOp object)
      {
        return createMatchOpAdapter();
      }
      @Override
      public Adapter caseValueofOp(ValueofOp object)
      {
        return createValueofOpAdapter();
      }
      @Override
      public Adapter caseConfigurationOps(ConfigurationOps object)
      {
        return createConfigurationOpsAdapter();
      }
      @Override
      public Adapter caseCreateOp(CreateOp object)
      {
        return createCreateOpAdapter();
      }
      @Override
      public Adapter caseRunningOp(RunningOp object)
      {
        return createRunningOpAdapter();
      }
      @Override
      public Adapter caseOpCall(OpCall object)
      {
        return createOpCallAdapter();
      }
      @Override
      public Adapter caseAliveOp(AliveOp object)
      {
        return createAliveOpAdapter();
      }
      @Override
      public Adapter caseTemplateListItem(TemplateListItem object)
      {
        return createTemplateListItemAdapter();
      }
      @Override
      public Adapter caseAllElementsFrom(AllElementsFrom object)
      {
        return createAllElementsFromAdapter();
      }
      @Override
      public Adapter caseSignature(Signature object)
      {
        return createSignatureAdapter();
      }
      @Override
      public Adapter caseTemplateInstanceAssignment(TemplateInstanceAssignment object)
      {
        return createTemplateInstanceAssignmentAdapter();
      }
      @Override
      public Adapter caseTemplateInstanceActualPar(TemplateInstanceActualPar object)
      {
        return createTemplateInstanceActualParAdapter();
      }
      @Override
      public Adapter caseTemplateRestriction(TemplateRestriction object)
      {
        return createTemplateRestrictionAdapter();
      }
      @Override
      public Adapter caseRestrictedTemplate(RestrictedTemplate object)
      {
        return createRestrictedTemplateAdapter();
      }
      @Override
      public Adapter caseLogStatement(LogStatement object)
      {
        return createLogStatementAdapter();
      }
      @Override
      public Adapter caseLogItem(LogItem object)
      {
        return createLogItemAdapter();
      }
      @Override
      public Adapter caseExpression(Expression object)
      {
        return createExpressionAdapter();
      }
      @Override
      public Adapter caseWithStatement(WithStatement object)
      {
        return createWithStatementAdapter();
      }
      @Override
      public Adapter caseWithAttribList(WithAttribList object)
      {
        return createWithAttribListAdapter();
      }
      @Override
      public Adapter caseMultiWithAttrib(MultiWithAttrib object)
      {
        return createMultiWithAttribAdapter();
      }
      @Override
      public Adapter caseSingleWithAttrib(SingleWithAttrib object)
      {
        return createSingleWithAttribAdapter();
      }
      @Override
      public Adapter caseAttribQualifier(AttribQualifier object)
      {
        return createAttribQualifierAdapter();
      }
      @Override
      public Adapter caseIdentifierList(IdentifierList object)
      {
        return createIdentifierListAdapter();
      }
      @Override
      public Adapter caseQualifiedIdentifierList(QualifiedIdentifierList object)
      {
        return createQualifiedIdentifierListAdapter();
      }
      @Override
      public Adapter caseSingleConstDef(SingleConstDef object)
      {
        return createSingleConstDefAdapter();
      }
      @Override
      public Adapter caseCompoundExpression(CompoundExpression object)
      {
        return createCompoundExpressionAdapter();
      }
      @Override
      public Adapter caseArrayExpression(ArrayExpression object)
      {
        return createArrayExpressionAdapter();
      }
      @Override
      public Adapter caseFieldExpressionList(FieldExpressionList object)
      {
        return createFieldExpressionListAdapter();
      }
      @Override
      public Adapter caseArrayElementExpressionList(ArrayElementExpressionList object)
      {
        return createArrayElementExpressionListAdapter();
      }
      @Override
      public Adapter caseFieldExpressionSpec(FieldExpressionSpec object)
      {
        return createFieldExpressionSpecAdapter();
      }
      @Override
      public Adapter caseNotUsedOrExpression(NotUsedOrExpression object)
      {
        return createNotUsedOrExpressionAdapter();
      }
      @Override
      public Adapter caseConstantExpression(ConstantExpression object)
      {
        return createConstantExpressionAdapter();
      }
      @Override
      public Adapter caseCompoundConstExpression(CompoundConstExpression object)
      {
        return createCompoundConstExpressionAdapter();
      }
      @Override
      public Adapter caseFieldConstExpressionList(FieldConstExpressionList object)
      {
        return createFieldConstExpressionListAdapter();
      }
      @Override
      public Adapter caseFieldConstExpressionSpec(FieldConstExpressionSpec object)
      {
        return createFieldConstExpressionSpecAdapter();
      }
      @Override
      public Adapter caseArrayConstExpression(ArrayConstExpression object)
      {
        return createArrayConstExpressionAdapter();
      }
      @Override
      public Adapter caseArrayElementConstExpressionList(ArrayElementConstExpressionList object)
      {
        return createArrayElementConstExpressionListAdapter();
      }
      @Override
      public Adapter caseConstList(ConstList object)
      {
        return createConstListAdapter();
      }
      @Override
      public Adapter caseValue(Value object)
      {
        return createValueAdapter();
      }
      @Override
      public Adapter caseReferencedValue(ReferencedValue object)
      {
        return createReferencedValueAdapter();
      }
      @Override
      public Adapter caseRefValueHead(RefValueHead object)
      {
        return createRefValueHeadAdapter();
      }
      @Override
      public Adapter caseRefValueElement(RefValueElement object)
      {
        return createRefValueElementAdapter();
      }
      @Override
      public Adapter caseHead(Head object)
      {
        return createHeadAdapter();
      }
      @Override
      public Adapter caseRefValueTail(RefValueTail object)
      {
        return createRefValueTailAdapter();
      }
      @Override
      public Adapter caseSpecElement(SpecElement object)
      {
        return createSpecElementAdapter();
      }
      @Override
      public Adapter caseExtendedFieldReference(ExtendedFieldReference object)
      {
        return createExtendedFieldReferenceAdapter();
      }
      @Override
      public Adapter caseRefValue(RefValue object)
      {
        return createRefValueAdapter();
      }
      @Override
      public Adapter casePredefinedValue(PredefinedValue object)
      {
        return createPredefinedValueAdapter();
      }
      @Override
      public Adapter caseBooleanExpression(BooleanExpression object)
      {
        return createBooleanExpressionAdapter();
      }
      @Override
      public Adapter caseSingleExpression(SingleExpression object)
      {
        return createSingleExpressionAdapter();
      }
      @Override
      public Adapter caseDefOrFieldRefList(DefOrFieldRefList object)
      {
        return createDefOrFieldRefListAdapter();
      }
      @Override
      public Adapter caseDefOrFieldRef(DefOrFieldRef object)
      {
        return createDefOrFieldRefAdapter();
      }
      @Override
      public Adapter caseAllRef(AllRef object)
      {
        return createAllRefAdapter();
      }
      @Override
      public Adapter caseGroupRefList(GroupRefList object)
      {
        return createGroupRefListAdapter();
      }
      @Override
      public Adapter caseTTCN3Reference(TTCN3Reference object)
      {
        return createTTCN3ReferenceAdapter();
      }
      @Override
      public Adapter caseTTCN3ReferenceList(TTCN3ReferenceList object)
      {
        return createTTCN3ReferenceListAdapter();
      }
      @Override
      public Adapter caseFieldReference(FieldReference object)
      {
        return createFieldReferenceAdapter();
      }
      @Override
      public Adapter caseArrayOrBitRef(ArrayOrBitRef object)
      {
        return createArrayOrBitRefAdapter();
      }
      @Override
      public Adapter caseFieldOrBitNumber(FieldOrBitNumber object)
      {
        return createFieldOrBitNumberAdapter();
      }
      @Override
      public Adapter caseArrayValueOrAttrib(ArrayValueOrAttrib object)
      {
        return createArrayValueOrAttribAdapter();
      }
      @Override
      public Adapter caseArrayElementSpecList(ArrayElementSpecList object)
      {
        return createArrayElementSpecListAdapter();
      }
      @Override
      public Adapter caseArrayElementSpec(ArrayElementSpec object)
      {
        return createArrayElementSpecAdapter();
      }
      @Override
      public Adapter caseTimerOps(TimerOps object)
      {
        return createTimerOpsAdapter();
      }
      @Override
      public Adapter caseRunningTimerOp(RunningTimerOp object)
      {
        return createRunningTimerOpAdapter();
      }
      @Override
      public Adapter caseReadTimerOp(ReadTimerOp object)
      {
        return createReadTimerOpAdapter();
      }
      @Override
      public Adapter casePermutationMatch(PermutationMatch object)
      {
        return createPermutationMatchAdapter();
      }
      @Override
      public Adapter caseLoopConstruct(LoopConstruct object)
      {
        return createLoopConstructAdapter();
      }
      @Override
      public Adapter caseForStatement(ForStatement object)
      {
        return createForStatementAdapter();
      }
      @Override
      public Adapter caseWhileStatement(WhileStatement object)
      {
        return createWhileStatementAdapter();
      }
      @Override
      public Adapter caseDoWhileStatement(DoWhileStatement object)
      {
        return createDoWhileStatementAdapter();
      }
      @Override
      public Adapter caseConditionalConstruct(ConditionalConstruct object)
      {
        return createConditionalConstructAdapter();
      }
      @Override
      public Adapter caseElseIfClause(ElseIfClause object)
      {
        return createElseIfClauseAdapter();
      }
      @Override
      public Adapter caseElseClause(ElseClause object)
      {
        return createElseClauseAdapter();
      }
      @Override
      public Adapter caseInitial(Initial object)
      {
        return createInitialAdapter();
      }
      @Override
      public Adapter caseSelectCaseConstruct(SelectCaseConstruct object)
      {
        return createSelectCaseConstructAdapter();
      }
      @Override
      public Adapter caseSelectCaseBody(SelectCaseBody object)
      {
        return createSelectCaseBodyAdapter();
      }
      @Override
      public Adapter caseSelectCase(SelectCase object)
      {
        return createSelectCaseAdapter();
      }
      @Override
      public Adapter caseFunctionDef(FunctionDef object)
      {
        return createFunctionDefAdapter();
      }
      @Override
      public Adapter caseMtcSpec(MtcSpec object)
      {
        return createMtcSpecAdapter();
      }
      @Override
      public Adapter caseFunctionFormalParList(FunctionFormalParList object)
      {
        return createFunctionFormalParListAdapter();
      }
      @Override
      public Adapter caseFunctionFormalPar(FunctionFormalPar object)
      {
        return createFunctionFormalParAdapter();
      }
      @Override
      public Adapter caseFormalValuePar(FormalValuePar object)
      {
        return createFormalValueParAdapter();
      }
      @Override
      public Adapter caseFormalTimerPar(FormalTimerPar object)
      {
        return createFormalTimerParAdapter();
      }
      @Override
      public Adapter caseFormalPortPar(FormalPortPar object)
      {
        return createFormalPortParAdapter();
      }
      @Override
      public Adapter caseFormalTemplatePar(FormalTemplatePar object)
      {
        return createFormalTemplateParAdapter();
      }
      @Override
      public Adapter caseRunsOnSpec(RunsOnSpec object)
      {
        return createRunsOnSpecAdapter();
      }
      @Override
      public Adapter caseReturnType(ReturnType object)
      {
        return createReturnTypeAdapter();
      }
      @Override
      public Adapter caseAssignment(Assignment object)
      {
        return createAssignmentAdapter();
      }
      @Override
      public Adapter caseVariableRef(VariableRef object)
      {
        return createVariableRefAdapter();
      }
      @Override
      public Adapter casePreDefFunction(PreDefFunction object)
      {
        return createPreDefFunctionAdapter();
      }
      @Override
      public Adapter caseFint2char(Fint2char object)
      {
        return createFint2charAdapter();
      }
      @Override
      public Adapter caseFint2unichar(Fint2unichar object)
      {
        return createFint2unicharAdapter();
      }
      @Override
      public Adapter caseFint2bit(Fint2bit object)
      {
        return createFint2bitAdapter();
      }
      @Override
      public Adapter caseFint2enum(Fint2enum object)
      {
        return createFint2enumAdapter();
      }
      @Override
      public Adapter caseFint2hex(Fint2hex object)
      {
        return createFint2hexAdapter();
      }
      @Override
      public Adapter caseFint2oct(Fint2oct object)
      {
        return createFint2octAdapter();
      }
      @Override
      public Adapter caseFint2str(Fint2str object)
      {
        return createFint2strAdapter();
      }
      @Override
      public Adapter caseFint2float(Fint2float object)
      {
        return createFint2floatAdapter();
      }
      @Override
      public Adapter caseFfloat2int(Ffloat2int object)
      {
        return createFfloat2intAdapter();
      }
      @Override
      public Adapter caseFchar2int(Fchar2int object)
      {
        return createFchar2intAdapter();
      }
      @Override
      public Adapter caseFchar2oct(Fchar2oct object)
      {
        return createFchar2octAdapter();
      }
      @Override
      public Adapter caseFunichar2int(Funichar2int object)
      {
        return createFunichar2intAdapter();
      }
      @Override
      public Adapter caseFunichar2oct(Funichar2oct object)
      {
        return createFunichar2octAdapter();
      }
      @Override
      public Adapter caseFbit2int(Fbit2int object)
      {
        return createFbit2intAdapter();
      }
      @Override
      public Adapter caseFbit2hex(Fbit2hex object)
      {
        return createFbit2hexAdapter();
      }
      @Override
      public Adapter caseFbit2oct(Fbit2oct object)
      {
        return createFbit2octAdapter();
      }
      @Override
      public Adapter caseFbit2str(Fbit2str object)
      {
        return createFbit2strAdapter();
      }
      @Override
      public Adapter caseFhex2int(Fhex2int object)
      {
        return createFhex2intAdapter();
      }
      @Override
      public Adapter caseFhex2bit(Fhex2bit object)
      {
        return createFhex2bitAdapter();
      }
      @Override
      public Adapter caseFhex2oct(Fhex2oct object)
      {
        return createFhex2octAdapter();
      }
      @Override
      public Adapter caseFhex2str(Fhex2str object)
      {
        return createFhex2strAdapter();
      }
      @Override
      public Adapter caseFoct2int(Foct2int object)
      {
        return createFoct2intAdapter();
      }
      @Override
      public Adapter caseFoct2bit(Foct2bit object)
      {
        return createFoct2bitAdapter();
      }
      @Override
      public Adapter caseFoct2hex(Foct2hex object)
      {
        return createFoct2hexAdapter();
      }
      @Override
      public Adapter caseFoct2str(Foct2str object)
      {
        return createFoct2strAdapter();
      }
      @Override
      public Adapter caseFoct2char(Foct2char object)
      {
        return createFoct2charAdapter();
      }
      @Override
      public Adapter caseFoct2unichar(Foct2unichar object)
      {
        return createFoct2unicharAdapter();
      }
      @Override
      public Adapter caseFstr2int(Fstr2int object)
      {
        return createFstr2intAdapter();
      }
      @Override
      public Adapter caseFstr2hex(Fstr2hex object)
      {
        return createFstr2hexAdapter();
      }
      @Override
      public Adapter caseFstr2oct(Fstr2oct object)
      {
        return createFstr2octAdapter();
      }
      @Override
      public Adapter caseFstr2float(Fstr2float object)
      {
        return createFstr2floatAdapter();
      }
      @Override
      public Adapter caseFenum2int(Fenum2int object)
      {
        return createFenum2intAdapter();
      }
      @Override
      public Adapter caseFlengthof(Flengthof object)
      {
        return createFlengthofAdapter();
      }
      @Override
      public Adapter caseFsizeof(Fsizeof object)
      {
        return createFsizeofAdapter();
      }
      @Override
      public Adapter caseFispresent(Fispresent object)
      {
        return createFispresentAdapter();
      }
      @Override
      public Adapter caseFischosen(Fischosen object)
      {
        return createFischosenAdapter();
      }
      @Override
      public Adapter caseFisvalue(Fisvalue object)
      {
        return createFisvalueAdapter();
      }
      @Override
      public Adapter caseFisbound(Fisbound object)
      {
        return createFisboundAdapter();
      }
      @Override
      public Adapter caseFregexp(Fregexp object)
      {
        return createFregexpAdapter();
      }
      @Override
      public Adapter caseFsubstr(Fsubstr object)
      {
        return createFsubstrAdapter();
      }
      @Override
      public Adapter caseFreplace(Freplace object)
      {
        return createFreplaceAdapter();
      }
      @Override
      public Adapter caseFencvalue(Fencvalue object)
      {
        return createFencvalueAdapter();
      }
      @Override
      public Adapter caseFdecvalue(Fdecvalue object)
      {
        return createFdecvalueAdapter();
      }
      @Override
      public Adapter caseFencvalueUnichar(FencvalueUnichar object)
      {
        return createFencvalueUnicharAdapter();
      }
      @Override
      public Adapter caseFdecvalueUnichar(FdecvalueUnichar object)
      {
        return createFdecvalueUnicharAdapter();
      }
      @Override
      public Adapter caseFrnd(Frnd object)
      {
        return createFrndAdapter();
      }
      @Override
      public Adapter caseFtestcasename(Ftestcasename object)
      {
        return createFtestcasenameAdapter();
      }
      @Override
      public Adapter caseXorExpression(XorExpression object)
      {
        return createXorExpressionAdapter();
      }
      @Override
      public Adapter caseAndExpression(AndExpression object)
      {
        return createAndExpressionAdapter();
      }
      @Override
      public Adapter caseEqualExpression(EqualExpression object)
      {
        return createEqualExpressionAdapter();
      }
      @Override
      public Adapter caseRelExpression(RelExpression object)
      {
        return createRelExpressionAdapter();
      }
      @Override
      public Adapter caseShiftExpression(ShiftExpression object)
      {
        return createShiftExpressionAdapter();
      }
      @Override
      public Adapter caseBitOrExpression(BitOrExpression object)
      {
        return createBitOrExpressionAdapter();
      }
      @Override
      public Adapter caseBitXorExpression(BitXorExpression object)
      {
        return createBitXorExpressionAdapter();
      }
      @Override
      public Adapter caseBitAndExpression(BitAndExpression object)
      {
        return createBitAndExpressionAdapter();
      }
      @Override
      public Adapter caseAddExpression(AddExpression object)
      {
        return createAddExpressionAdapter();
      }
      @Override
      public Adapter caseMulExpression(MulExpression object)
      {
        return createMulExpressionAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TTCN3File <em>File</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3File
   * @generated
   */
  public Adapter createTTCN3FileAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConstDef <em>Const Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConstDef
   * @generated
   */
  public Adapter createConstDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Type
   * @generated
   */
  public Adapter createTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTailType <em>Type Reference Tail Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TypeReferenceTailType
   * @generated
   */
  public Adapter createTypeReferenceTailTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail <em>Type Reference Tail</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TypeReferenceTail
   * @generated
   */
  public Adapter createTypeReferenceTailAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TypeReference <em>Type Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TypeReference
   * @generated
   */
  public Adapter createTypeReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SpecTypeElement <em>Spec Type Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SpecTypeElement
   * @generated
   */
  public Adapter createSpecTypeElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayDef <em>Array Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayDef
   * @generated
   */
  public Adapter createArrayDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module <em>Module</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module
   * @generated
   */
  public Adapter createTTCN3ModuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.LanguageSpec <em>Language Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.LanguageSpec
   * @generated
   */
  public Adapter createLanguageSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleParDef <em>Module Par Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParDef
   * @generated
   */
  public Adapter createModuleParDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModulePar <em>Module Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModulePar
   * @generated
   */
  public Adapter createModuleParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleParList <em>Module Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParList
   * @generated
   */
  public Adapter createModuleParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleParameter <em>Module Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParameter
   * @generated
   */
  public Adapter createModuleParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MultitypedModuleParList <em>Multityped Module Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MultitypedModuleParList
   * @generated
   */
  public Adapter createMultitypedModuleParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList <em>Module Definitions List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList
   * @generated
   */
  public Adapter createModuleDefinitionsListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition <em>Module Definition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition
   * @generated
   */
  public Adapter createModuleDefinitionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FriendModuleDef <em>Friend Module Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FriendModuleDef
   * @generated
   */
  public Adapter createFriendModuleDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GroupDef <em>Group Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GroupDef
   * @generated
   */
  public Adapter createGroupDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef <em>Ext Function Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExtFunctionDef
   * @generated
   */
  public Adapter createExtFunctionDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExtConstDef <em>Ext Const Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExtConstDef
   * @generated
   */
  public Adapter createExtConstDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.IdentifierObjectList <em>Identifier Object List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierObjectList
   * @generated
   */
  public Adapter createIdentifierObjectListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NamedObject <em>Named Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NamedObject
   * @generated
   */
  public Adapter createNamedObjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportDef <em>Import Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportDef
   * @generated
   */
  public Adapter createImportDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllWithExcepts <em>All With Excepts</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllWithExcepts
   * @generated
   */
  public Adapter createAllWithExceptsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptsDef <em>Excepts Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptsDef
   * @generated
   */
  public Adapter createExceptsDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptSpec <em>Except Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptSpec
   * @generated
   */
  public Adapter createExceptSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptElement <em>Except Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement
   * @generated
   */
  public Adapter createExceptElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAll <em>Identifier List Or All</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAll
   * @generated
   */
  public Adapter createIdentifierListOrAllAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec <em>Except Group Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptGroupSpec
   * @generated
   */
  public Adapter createExceptGroupSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec <em>Except Type Def Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec
   * @generated
   */
  public Adapter createExceptTypeDefSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec <em>Except Template Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec
   * @generated
   */
  public Adapter createExceptTemplateSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptConstSpec <em>Except Const Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptConstSpec
   * @generated
   */
  public Adapter createExceptConstSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec <em>Except Testcase Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec
   * @generated
   */
  public Adapter createExceptTestcaseSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec <em>Except Altstep Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec
   * @generated
   */
  public Adapter createExceptAltstepSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec <em>Except Function Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec
   * @generated
   */
  public Adapter createExceptFunctionSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec <em>Except Signature Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec
   * @generated
   */
  public Adapter createExceptSignatureSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec <em>Except Module Par Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec
   * @generated
   */
  public Adapter createExceptModuleParSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportSpec <em>Import Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportSpec
   * @generated
   */
  public Adapter createImportSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportElement <em>Import Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement
   * @generated
   */
  public Adapter createImportElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec <em>Import Group Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportGroupSpec
   * @generated
   */
  public Adapter createImportGroupSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept <em>Identifier List Or All With Except</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept
   * @generated
   */
  public Adapter createIdentifierListOrAllWithExceptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllWithExcept <em>All With Except</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllWithExcept
   * @generated
   */
  public Adapter createAllWithExceptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec <em>Import Type Def Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec
   * @generated
   */
  public Adapter createImportTypeDefSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportTemplateSpec <em>Import Template Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportTemplateSpec
   * @generated
   */
  public Adapter createImportTemplateSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportConstSpec <em>Import Const Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportConstSpec
   * @generated
   */
  public Adapter createImportConstSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportAltstepSpec <em>Import Altstep Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportAltstepSpec
   * @generated
   */
  public Adapter createImportAltstepSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec <em>Import Testcase Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec
   * @generated
   */
  public Adapter createImportTestcaseSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportFunctionSpec <em>Import Function Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportFunctionSpec
   * @generated
   */
  public Adapter createImportFunctionSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportSignatureSpec <em>Import Signature Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportSignatureSpec
   * @generated
   */
  public Adapter createImportSignatureSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ImportModuleParSpec <em>Import Module Par Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ImportModuleParSpec
   * @generated
   */
  public Adapter createImportModuleParSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept <em>Group Ref List With Except</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept
   * @generated
   */
  public Adapter createGroupRefListWithExceptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept <em>Qualified Identifier With Except</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept
   * @generated
   */
  public Adapter createQualifiedIdentifierWithExceptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept <em>All Groups With Except</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept
   * @generated
   */
  public Adapter createAllGroupsWithExceptAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltstepDef <em>Altstep Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef
   * @generated
   */
  public Adapter createAltstepDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList <em>Altstep Local Def List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDefList
   * @generated
   */
  public Adapter createAltstepLocalDefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef <em>Altstep Local Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDef
   * @generated
   */
  public Adapter createAltstepLocalDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef <em>Testcase Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseDef
   * @generated
   */
  public Adapter createTestcaseDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList <em>Template Or Value Formal Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList
   * @generated
   */
  public Adapter createTemplateOrValueFormalParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar <em>Template Or Value Formal Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar
   * @generated
   */
  public Adapter createTemplateOrValueFormalParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConfigSpec <em>Config Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConfigSpec
   * @generated
   */
  public Adapter createConfigSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SystemSpec <em>System Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SystemSpec
   * @generated
   */
  public Adapter createSystemSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SignatureDef <em>Signature Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SignatureDef
   * @generated
   */
  public Adapter createSignatureDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExceptionSpec <em>Exception Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExceptionSpec
   * @generated
   */
  public Adapter createExceptionSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SignatureFormalParList <em>Signature Formal Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SignatureFormalParList
   * @generated
   */
  public Adapter createSignatureFormalParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart <em>Module Control Part</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlPart
   * @generated
   */
  public Adapter createModuleControlPartAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleControlBody <em>Module Control Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlBody
   * @generated
   */
  public Adapter createModuleControlBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList <em>Control Statement Or Def List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList
   * @generated
   */
  public Adapter createControlStatementOrDefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef <em>Control Statement Or Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDef
   * @generated
   */
  public Adapter createControlStatementOrDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ControlStatement <em>Control Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatement
   * @generated
   */
  public Adapter createControlStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SUTStatements <em>SUT Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SUTStatements
   * @generated
   */
  public Adapter createSUTStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ActionText <em>Action Text</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ActionText
   * @generated
   */
  public Adapter createActionTextAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements <em>Behaviour Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements
   * @generated
   */
  public Adapter createBehaviourStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ActivateOp <em>Activate Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ActivateOp
   * @generated
   */
  public Adapter createActivateOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DeactivateStatement <em>Deactivate Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DeactivateStatement
   * @generated
   */
  public Adapter createDeactivateStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.LabelStatement <em>Label Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.LabelStatement
   * @generated
   */
  public Adapter createLabelStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GotoStatement <em>Goto Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GotoStatement
   * @generated
   */
  public Adapter createGotoStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReturnStatement <em>Return Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReturnStatement
   * @generated
   */
  public Adapter createReturnStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.InterleavedConstruct <em>Interleaved Construct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedConstruct
   * @generated
   */
  public Adapter createInterleavedConstructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardList <em>Interleaved Guard List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardList
   * @generated
   */
  public Adapter createInterleavedGuardListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement <em>Interleaved Guard Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardElement
   * @generated
   */
  public Adapter createInterleavedGuardElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuard <em>Interleaved Guard</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuard
   * @generated
   */
  public Adapter createInterleavedGuardAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltConstruct <em>Alt Construct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltConstruct
   * @generated
   */
  public Adapter createAltConstructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltGuardList <em>Alt Guard List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardList
   * @generated
   */
  public Adapter createAltGuardListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ElseStatement <em>Else Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ElseStatement
   * @generated
   */
  public Adapter createElseStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GuardStatement <em>Guard Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GuardStatement
   * @generated
   */
  public Adapter createGuardStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GuardOp <em>Guard Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp
   * @generated
   */
  public Adapter createGuardOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DoneStatement <em>Done Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DoneStatement
   * @generated
   */
  public Adapter createDoneStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.KilledStatement <em>Killed Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.KilledStatement
   * @generated
   */
  public Adapter createKilledStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny <em>Component Or Any</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrAny
   * @generated
   */
  public Adapter createComponentOrAnyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.IndexAssignment <em>Index Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.IndexAssignment
   * @generated
   */
  public Adapter createIndexAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.IndexSpec <em>Index Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.IndexSpec
   * @generated
   */
  public Adapter createIndexSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GetReplyStatement <em>Get Reply Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GetReplyStatement
   * @generated
   */
  public Adapter createGetReplyStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CheckStatement <em>Check Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CheckStatement
   * @generated
   */
  public Adapter createCheckStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortCheckOp <em>Port Check Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortCheckOp
   * @generated
   */
  public Adapter createPortCheckOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CheckParameter <em>Check Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CheckParameter
   * @generated
   */
  public Adapter createCheckParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RedirectPresent <em>Redirect Present</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RedirectPresent
   * @generated
   */
  public Adapter createRedirectPresentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FromClausePresent <em>From Clause Present</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FromClausePresent
   * @generated
   */
  public Adapter createFromClausePresentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent <em>Check Port Ops Present</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent
   * @generated
   */
  public Adapter createCheckPortOpsPresentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp <em>Port Get Reply Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortGetReplyOp
   * @generated
   */
  public Adapter createPortGetReplyOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithValueAndParam <em>Port Redirect With Value And Param</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirectWithValueAndParam
   * @generated
   */
  public Adapter createPortRedirectWithValueAndParamAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec <em>Redirect With Value And Param Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec
   * @generated
   */
  public Adapter createRedirectWithValueAndParamSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ValueMatchSpec <em>Value Match Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ValueMatchSpec
   * @generated
   */
  public Adapter createValueMatchSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CatchStatement <em>Catch Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CatchStatement
   * @generated
   */
  public Adapter createCatchStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortCatchOp <em>Port Catch Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortCatchOp
   * @generated
   */
  public Adapter createPortCatchOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CatchOpParameter <em>Catch Op Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CatchOpParameter
   * @generated
   */
  public Adapter createCatchOpParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GetCallStatement <em>Get Call Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GetCallStatement
   * @generated
   */
  public Adapter createGetCallStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortGetCallOp <em>Port Get Call Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortGetCallOp
   * @generated
   */
  public Adapter createPortGetCallOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithParam <em>Port Redirect With Param</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirectWithParam
   * @generated
   */
  public Adapter createPortRedirectWithParamAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec <em>Redirect With Param Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec
   * @generated
   */
  public Adapter createRedirectWithParamSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ParamSpec <em>Param Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ParamSpec
   * @generated
   */
  public Adapter createParamSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ParamAssignmentList <em>Param Assignment List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ParamAssignmentList
   * @generated
   */
  public Adapter createParamAssignmentListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AssignmentList <em>Assignment List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AssignmentList
   * @generated
   */
  public Adapter createAssignmentListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.VariableAssignment <em>Variable Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.VariableAssignment
   * @generated
   */
  public Adapter createVariableAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.VariableList <em>Variable List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.VariableList
   * @generated
   */
  public Adapter createVariableListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.VariableEntry <em>Variable Entry</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.VariableEntry
   * @generated
   */
  public Adapter createVariableEntryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TriggerStatement <em>Trigger Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TriggerStatement
   * @generated
   */
  public Adapter createTriggerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortTriggerOp <em>Port Trigger Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortTriggerOp
   * @generated
   */
  public Adapter createPortTriggerOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement <em>Receive Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReceiveStatement
   * @generated
   */
  public Adapter createReceiveStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortOrAny <em>Port Or Any</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny
   * @generated
   */
  public Adapter createPortOrAnyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortReceiveOp <em>Port Receive Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortReceiveOp
   * @generated
   */
  public Adapter createPortReceiveOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FromClause <em>From Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FromClause
   * @generated
   */
  public Adapter createFromClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AddressRefList <em>Address Ref List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AddressRefList
   * @generated
   */
  public Adapter createAddressRefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortRedirect <em>Port Redirect</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirect
   * @generated
   */
  public Adapter createPortRedirectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SenderSpec <em>Sender Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SenderSpec
   * @generated
   */
  public Adapter createSenderSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ValueSpec <em>Value Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ValueSpec
   * @generated
   */
  public Adapter createValueSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec <em>Single Value Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleValueSpec
   * @generated
   */
  public Adapter createSingleValueSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltGuardChar <em>Alt Guard Char</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardChar
   * @generated
   */
  public Adapter createAltGuardCharAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AltstepInstance <em>Altstep Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AltstepInstance
   * @generated
   */
  public Adapter createAltstepInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionInstance <em>Function Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionInstance
   * @generated
   */
  public Adapter createFunctionInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionRef <em>Function Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionRef
   * @generated
   */
  public Adapter createFunctionRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParList <em>Function Actual Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParList
   * @generated
   */
  public Adapter createFunctionActualParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment <em>Function Actual Par Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment
   * @generated
   */
  public Adapter createFunctionActualParAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentRefAssignment <em>Component Ref Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRefAssignment
   * @generated
   */
  public Adapter createComponentRefAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar <em>Formal Port And Value Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar
   * @generated
   */
  public Adapter createFormalPortAndValueParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment <em>Port Ref Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortRefAssignment
   * @generated
   */
  public Adapter createPortRefAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment <em>Timer Ref Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefAssignment
   * @generated
   */
  public Adapter createTimerRefAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar <em>Function Actual Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualPar
   * @generated
   */
  public Adapter createFunctionActualParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentRef <em>Component Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRef
   * @generated
   */
  public Adapter createComponentRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference <em>Component Or Default Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference
   * @generated
   */
  public Adapter createComponentOrDefaultReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance <em>Testcase Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseInstance
   * @generated
   */
  public Adapter createTestcaseInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TestcaseActualParList <em>Testcase Actual Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseActualParList
   * @generated
   */
  public Adapter createTestcaseActualParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerStatements <em>Timer Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerStatements
   * @generated
   */
  public Adapter createTimerStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimeoutStatement <em>Timeout Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimeoutStatement
   * @generated
   */
  public Adapter createTimeoutStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement <em>Start Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StartTimerStatement
   * @generated
   */
  public Adapter createStartTimerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StopTimerStatement <em>Stop Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StopTimerStatement
   * @generated
   */
  public Adapter createStopTimerStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny <em>Timer Ref Or Any</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAny
   * @generated
   */
  public Adapter createTimerRefOrAnyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll <em>Timer Ref Or All</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAll
   * @generated
   */
  public Adapter createTimerRefOrAllAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BasicStatements <em>Basic Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements
   * @generated
   */
  public Adapter createBasicStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StatementBlock <em>Statement Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StatementBlock
   * @generated
   */
  public Adapter createStatementBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionStatementList <em>Function Statement List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatementList
   * @generated
   */
  public Adapter createFunctionStatementListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement <em>Function Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement
   * @generated
   */
  public Adapter createFunctionStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TestcaseOperation <em>Testcase Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseOperation
   * @generated
   */
  public Adapter createTestcaseOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict <em>Set Local Verdict</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SetLocalVerdict
   * @generated
   */
  public Adapter createSetLocalVerdictAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements <em>Configuration Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements
   * @generated
   */
  public Adapter createConfigurationStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.KillTCStatement <em>Kill TC Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.KillTCStatement
   * @generated
   */
  public Adapter createKillTCStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StopTCStatement <em>Stop TC Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StopTCStatement
   * @generated
   */
  public Adapter createStopTCStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral <em>Component Reference Or Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral
   * @generated
   */
  public Adapter createComponentReferenceOrLiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StartTCStatement <em>Start TC Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StartTCStatement
   * @generated
   */
  public Adapter createStartTCStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.UnmapStatement <em>Unmap Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.UnmapStatement
   * @generated
   */
  public Adapter createUnmapStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DisconnectStatement <em>Disconnect Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DisconnectStatement
   * @generated
   */
  public Adapter createDisconnectStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllConnectionsSpec <em>All Connections Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllConnectionsSpec
   * @generated
   */
  public Adapter createAllConnectionsSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllPortsSpec <em>All Ports Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllPortsSpec
   * @generated
   */
  public Adapter createAllPortsSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MapStatement <em>Map Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MapStatement
   * @generated
   */
  public Adapter createMapStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ParamClause <em>Param Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ParamClause
   * @generated
   */
  public Adapter createParamClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConnectStatement <em>Connect Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConnectStatement
   * @generated
   */
  public Adapter createConnectStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec <em>Single Connection Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleConnectionSpec
   * @generated
   */
  public Adapter createSingleConnectionSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortRef <em>Port Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortRef
   * @generated
   */
  public Adapter createPortRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements <em>Communication Statements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements
   * @generated
   */
  public Adapter createCommunicationStatementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CheckStateStatement <em>Check State Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CheckStateStatement
   * @generated
   */
  public Adapter createCheckStateStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortOrAllAny <em>Port Or All Any</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAllAny
   * @generated
   */
  public Adapter createPortOrAllAnyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.HaltStatement <em>Halt Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.HaltStatement
   * @generated
   */
  public Adapter createHaltStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StartStatement <em>Start Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StartStatement
   * @generated
   */
  public Adapter createStartStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StopStatement <em>Stop Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StopStatement
   * @generated
   */
  public Adapter createStopStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ClearStatement <em>Clear Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ClearStatement
   * @generated
   */
  public Adapter createClearStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortOrAll <em>Port Or All</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAll
   * @generated
   */
  public Adapter createPortOrAllAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement <em>Raise Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RaiseStatement
   * @generated
   */
  public Adapter createRaiseStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortRaiseOp <em>Port Raise Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortRaiseOp
   * @generated
   */
  public Adapter createPortRaiseOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReplyStatement <em>Reply Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReplyStatement
   * @generated
   */
  public Adapter createReplyStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortReplyOp <em>Port Reply Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortReplyOp
   * @generated
   */
  public Adapter createPortReplyOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReplyValue <em>Reply Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReplyValue
   * @generated
   */
  public Adapter createReplyValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallStatement <em>Call Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallStatement
   * @generated
   */
  public Adapter createCallStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortCallOp <em>Port Call Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortCallOp
   * @generated
   */
  public Adapter createPortCallOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallParameters <em>Call Parameters</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallParameters
   * @generated
   */
  public Adapter createCallParametersAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallTimerValue <em>Call Timer Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallTimerValue
   * @generated
   */
  public Adapter createCallTimerValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortCallBody <em>Port Call Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortCallBody
   * @generated
   */
  public Adapter createPortCallBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatementList <em>Call Body Statement List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatementList
   * @generated
   */
  public Adapter createCallBodyStatementListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatement <em>Call Body Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatement
   * @generated
   */
  public Adapter createCallBodyStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard <em>Call Body Guard</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyGuard
   * @generated
   */
  public Adapter createCallBodyGuardAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CallBodyOps <em>Call Body Ops</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyOps
   * @generated
   */
  public Adapter createCallBodyOpsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SendStatement <em>Send Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SendStatement
   * @generated
   */
  public Adapter createSendStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortSendOp <em>Port Send Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortSendOp
   * @generated
   */
  public Adapter createPortSendOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ToClause <em>To Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ToClause
   * @generated
   */
  public Adapter createToClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList <em>Function Def List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDefList
   * @generated
   */
  public Adapter createFunctionDefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef <em>Function Local Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalDef
   * @generated
   */
  public Adapter createFunctionLocalDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalInst <em>Function Local Inst</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalInst
   * @generated
   */
  public Adapter createFunctionLocalInstAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerInstance <em>Timer Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerInstance
   * @generated
   */
  public Adapter createTimerInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.VarInstance <em>Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance
   * @generated
   */
  public Adapter createVarInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.VarList <em>Var List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.VarList
   * @generated
   */
  public Adapter createVarListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ModuleOrGroup <em>Module Or Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ModuleOrGroup
   * @generated
   */
  public Adapter createModuleOrGroupAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReferencedType <em>Referenced Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReferencedType
   * @generated
   */
  public Adapter createReferencedTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TypeDef <em>Type Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TypeDef
   * @generated
   */
  public Adapter createTypeDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TypeDefBody <em>Type Def Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TypeDefBody
   * @generated
   */
  public Adapter createTypeDefBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SubTypeDef <em>Sub Type Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDef
   * @generated
   */
  public Adapter createSubTypeDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SubTypeDefNamed <em>Sub Type Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
   * @generated
   */
  public Adapter createSubTypeDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef <em>Structured Type Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef
   * @generated
   */
  public Adapter createStructuredTypeDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RecordDef <em>Record Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RecordDef
   * @generated
   */
  public Adapter createRecordDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RecordDefNamed <em>Record Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RecordDefNamed
   * @generated
   */
  public Adapter createRecordDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef <em>Record Of Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDef
   * @generated
   */
  public Adapter createRecordOfDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RecordOfDefNamed <em>Record Of Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
   * @generated
   */
  public Adapter createRecordOfDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StructDefBody <em>Struct Def Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StructDefBody
   * @generated
   */
  public Adapter createStructDefBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef <em>Struct Field Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef
   * @generated
   */
  public Adapter createStructFieldDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SetOfDef <em>Set Of Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDef
   * @generated
   */
  public Adapter createSetOfDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SetOfDefNamed <em>Set Of Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDefNamed
   * @generated
   */
  public Adapter createSetOfDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortDef <em>Port Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortDef
   * @generated
   */
  public Adapter createPortDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortDefBody <em>Port Def Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortDefBody
   * @generated
   */
  public Adapter createPortDefBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs <em>Port Def Attribs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortDefAttribs
   * @generated
   */
  public Adapter createPortDefAttribsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MixedAttribs <em>Mixed Attribs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MixedAttribs
   * @generated
   */
  public Adapter createMixedAttribsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MixedList <em>Mixed List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MixedList
   * @generated
   */
  public Adapter createMixedListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList <em>Proc Or Type List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrTypeList
   * @generated
   */
  public Adapter createProcOrTypeListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ProcOrType <em>Proc Or Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrType
   * @generated
   */
  public Adapter createProcOrTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MessageAttribs <em>Message Attribs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MessageAttribs
   * @generated
   */
  public Adapter createMessageAttribsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef <em>Config Param Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConfigParamDef
   * @generated
   */
  public Adapter createConfigParamDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MapParamDef <em>Map Param Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MapParamDef
   * @generated
   */
  public Adapter createMapParamDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.UnmapParamDef <em>Unmap Param Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.UnmapParamDef
   * @generated
   */
  public Adapter createUnmapParamDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AddressDecl <em>Address Decl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AddressDecl
   * @generated
   */
  public Adapter createAddressDeclAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs <em>Procedure Attribs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureAttribs
   * @generated
   */
  public Adapter createProcedureAttribsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentDef <em>Component Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDef
   * @generated
   */
  public Adapter createComponentDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList <em>Component Def List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDefList
   * @generated
   */
  public Adapter createComponentDefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortInstance <em>Port Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortInstance
   * @generated
   */
  public Adapter createPortInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PortElement <em>Port Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PortElement
   * @generated
   */
  public Adapter createPortElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef <em>Component Element Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef
   * @generated
   */
  public Adapter createComponentElementDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ProcedureList <em>Procedure List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureList
   * @generated
   */
  public Adapter createProcedureListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList <em>All Or Signature List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllOrSignatureList
   * @generated
   */
  public Adapter createAllOrSignatureListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SignatureList <em>Signature List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SignatureList
   * @generated
   */
  public Adapter createSignatureListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.EnumDef <em>Enum Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.EnumDef
   * @generated
   */
  public Adapter createEnumDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.EnumDefNamed <em>Enum Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.EnumDefNamed
   * @generated
   */
  public Adapter createEnumDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedTypeDef <em>Nested Type Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedTypeDef
   * @generated
   */
  public Adapter createNestedTypeDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedRecordDef <em>Nested Record Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordDef
   * @generated
   */
  public Adapter createNestedRecordDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedUnionDef <em>Nested Union Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedUnionDef
   * @generated
   */
  public Adapter createNestedUnionDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedSetDef <em>Nested Set Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetDef
   * @generated
   */
  public Adapter createNestedSetDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedRecordOfDef <em>Nested Record Of Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordOfDef
   * @generated
   */
  public Adapter createNestedRecordOfDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef <em>Nested Set Of Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetOfDef
   * @generated
   */
  public Adapter createNestedSetOfDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NestedEnumDef <em>Nested Enum Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NestedEnumDef
   * @generated
   */
  public Adapter createNestedEnumDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MessageList <em>Message List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MessageList
   * @generated
   */
  public Adapter createMessageListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList <em>All Or Type List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllOrTypeList
   * @generated
   */
  public Adapter createAllOrTypeListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TypeList <em>Type List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TypeList
   * @generated
   */
  public Adapter createTypeListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.UnionDef <em>Union Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.UnionDef
   * @generated
   */
  public Adapter createUnionDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.UnionDefNamed <em>Union Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.UnionDefNamed
   * @generated
   */
  public Adapter createUnionDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.UnionDefBody <em>Union Def Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.UnionDefBody
   * @generated
   */
  public Adapter createUnionDefBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.EnumerationList <em>Enumeration List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.EnumerationList
   * @generated
   */
  public Adapter createEnumerationListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Enumeration <em>Enumeration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Enumeration
   * @generated
   */
  public Adapter createEnumerationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.UnionFieldDef <em>Union Field Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.UnionFieldDef
   * @generated
   */
  public Adapter createUnionFieldDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SetDef <em>Set Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SetDef
   * @generated
   */
  public Adapter createSetDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SetDefNamed <em>Set Def Named</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SetDefNamed
   * @generated
   */
  public Adapter createSetDefNamedAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SubTypeSpec <em>Sub Type Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeSpec
   * @generated
   */
  public Adapter createSubTypeSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllowedValuesSpec <em>Allowed Values Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllowedValuesSpec
   * @generated
   */
  public Adapter createAllowedValuesSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateOrRange <em>Template Or Range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrRange
   * @generated
   */
  public Adapter createTemplateOrRangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RangeDef <em>Range Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RangeDef
   * @generated
   */
  public Adapter createRangeDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Bound <em>Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Bound
   * @generated
   */
  public Adapter createBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.StringLength <em>String Length</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.StringLength
   * @generated
   */
  public Adapter createStringLengthAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CharStringMatch <em>Char String Match</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CharStringMatch
   * @generated
   */
  public Adapter createCharStringMatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PatternParticle <em>Pattern Particle</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PatternParticle
   * @generated
   */
  public Adapter createPatternParticleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateDef <em>Template Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef
   * @generated
   */
  public Adapter createTemplateDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DerivedDef <em>Derived Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DerivedDef
   * @generated
   */
  public Adapter createDerivedDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BaseTemplate <em>Base Template</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BaseTemplate
   * @generated
   */
  public Adapter createBaseTemplateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TempVarList <em>Temp Var List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TempVarList
   * @generated
   */
  public Adapter createTempVarListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerVarInstance <em>Timer Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerVarInstance
   * @generated
   */
  public Adapter createTimerVarInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleVarInstance <em>Single Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleVarInstance
   * @generated
   */
  public Adapter createSingleVarInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance <em>Single Temp Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
   * @generated
   */
  public Adapter createSingleTempVarInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate <em>In Line Template</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.InLineTemplate
   * @generated
   */
  public Adapter createInLineTemplateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DerivedRefWithParList <em>Derived Ref With Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DerivedRefWithParList
   * @generated
   */
  public Adapter createDerivedRefWithParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateBody <em>Template Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateBody
   * @generated
   */
  public Adapter createTemplateBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExtraMatchingAttributes <em>Extra Matching Attributes</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExtraMatchingAttributes
   * @generated
   */
  public Adapter createExtraMatchingAttributesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldSpecList <em>Field Spec List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpecList
   * @generated
   */
  public Adapter createFieldSpecListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldSpec <em>Field Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpec
   * @generated
   */
  public Adapter createFieldSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SimpleSpec <em>Simple Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SimpleSpec
   * @generated
   */
  public Adapter createSimpleSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec <em>Simple Template Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec
   * @generated
   */
  public Adapter createSimpleTemplateSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression <em>Single Template Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleTemplateExpression
   * @generated
   */
  public Adapter createSingleTemplateExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateRefWithParList <em>Template Ref With Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRefWithParList
   * @generated
   */
  public Adapter createTemplateRefWithParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateActualParList <em>Template Actual Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateActualParList
   * @generated
   */
  public Adapter createTemplateActualParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol <em>Matching Symbol</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol
   * @generated
   */
  public Adapter createMatchingSymbolAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SubsetMatch <em>Subset Match</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SubsetMatch
   * @generated
   */
  public Adapter createSubsetMatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SupersetMatch <em>Superset Match</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SupersetMatch
   * @generated
   */
  public Adapter createSupersetMatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Range <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Range
   * @generated
   */
  public Adapter createRangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.WildcardLengthMatch <em>Wildcard Length Match</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.WildcardLengthMatch
   * @generated
   */
  public Adapter createWildcardLengthMatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Complement <em>Complement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Complement
   * @generated
   */
  public Adapter createComplementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ListOfTemplates <em>List Of Templates</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ListOfTemplates
   * @generated
   */
  public Adapter createListOfTemplatesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateOps <em>Template Ops</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOps
   * @generated
   */
  public Adapter createTemplateOpsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MatchOp <em>Match Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MatchOp
   * @generated
   */
  public Adapter createMatchOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ValueofOp <em>Valueof Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ValueofOp
   * @generated
   */
  public Adapter createValueofOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps <em>Configuration Ops</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationOps
   * @generated
   */
  public Adapter createConfigurationOpsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CreateOp <em>Create Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CreateOp
   * @generated
   */
  public Adapter createCreateOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RunningOp <em>Running Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RunningOp
   * @generated
   */
  public Adapter createRunningOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.OpCall <em>Op Call</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.OpCall
   * @generated
   */
  public Adapter createOpCallAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AliveOp <em>Alive Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AliveOp
   * @generated
   */
  public Adapter createAliveOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateListItem <em>Template List Item</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateListItem
   * @generated
   */
  public Adapter createTemplateListItemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllElementsFrom <em>All Elements From</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllElementsFrom
   * @generated
   */
  public Adapter createAllElementsFromAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Signature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Signature
   * @generated
   */
  public Adapter createSignatureAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment <em>Template Instance Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment
   * @generated
   */
  public Adapter createTemplateInstanceAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar <em>Template Instance Actual Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar
   * @generated
   */
  public Adapter createTemplateInstanceActualParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction <em>Template Restriction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRestriction
   * @generated
   */
  public Adapter createTemplateRestrictionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RestrictedTemplate <em>Restricted Template</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RestrictedTemplate
   * @generated
   */
  public Adapter createRestrictedTemplateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.LogStatement <em>Log Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.LogStatement
   * @generated
   */
  public Adapter createLogStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.LogItem <em>Log Item</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.LogItem
   * @generated
   */
  public Adapter createLogItemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Expression
   * @generated
   */
  public Adapter createExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.WithStatement <em>With Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.WithStatement
   * @generated
   */
  public Adapter createWithStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.WithAttribList <em>With Attrib List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.WithAttribList
   * @generated
   */
  public Adapter createWithAttribListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MultiWithAttrib <em>Multi With Attrib</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MultiWithAttrib
   * @generated
   */
  public Adapter createMultiWithAttribAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleWithAttrib <em>Single With Attrib</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleWithAttrib
   * @generated
   */
  public Adapter createSingleWithAttribAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AttribQualifier <em>Attrib Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AttribQualifier
   * @generated
   */
  public Adapter createAttribQualifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.IdentifierList <em>Identifier List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierList
   * @generated
   */
  public Adapter createIdentifierListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList <em>Qualified Identifier List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList
   * @generated
   */
  public Adapter createQualifiedIdentifierListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleConstDef <em>Single Const Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleConstDef
   * @generated
   */
  public Adapter createSingleConstDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CompoundExpression <em>Compound Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CompoundExpression
   * @generated
   */
  public Adapter createCompoundExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayExpression <em>Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayExpression
   * @generated
   */
  public Adapter createArrayExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionList <em>Field Expression List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionList
   * @generated
   */
  public Adapter createFieldExpressionListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementExpressionList <em>Array Element Expression List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementExpressionList
   * @generated
   */
  public Adapter createArrayElementExpressionListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec <em>Field Expression Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionSpec
   * @generated
   */
  public Adapter createFieldExpressionSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.NotUsedOrExpression <em>Not Used Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.NotUsedOrExpression
   * @generated
   */
  public Adapter createNotUsedOrExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConstantExpression <em>Constant Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConstantExpression
   * @generated
   */
  public Adapter createConstantExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.CompoundConstExpression <em>Compound Const Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.CompoundConstExpression
   * @generated
   */
  public Adapter createCompoundConstExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionList <em>Field Const Expression List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionList
   * @generated
   */
  public Adapter createFieldConstExpressionListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec <em>Field Const Expression Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec
   * @generated
   */
  public Adapter createFieldConstExpressionSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayConstExpression <em>Array Const Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayConstExpression
   * @generated
   */
  public Adapter createArrayConstExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementConstExpressionList <em>Array Element Const Expression List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementConstExpressionList
   * @generated
   */
  public Adapter createArrayElementConstExpressionListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConstList <em>Const List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConstList
   * @generated
   */
  public Adapter createConstListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Value <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Value
   * @generated
   */
  public Adapter createValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReferencedValue <em>Referenced Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReferencedValue
   * @generated
   */
  public Adapter createReferencedValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RefValueHead <em>Ref Value Head</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RefValueHead
   * @generated
   */
  public Adapter createRefValueHeadAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RefValueElement <em>Ref Value Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RefValueElement
   * @generated
   */
  public Adapter createRefValueElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Head <em>Head</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Head
   * @generated
   */
  public Adapter createHeadAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RefValueTail <em>Ref Value Tail</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RefValueTail
   * @generated
   */
  public Adapter createRefValueTailAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SpecElement <em>Spec Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SpecElement
   * @generated
   */
  public Adapter createSpecElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference <em>Extended Field Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ExtendedFieldReference
   * @generated
   */
  public Adapter createExtendedFieldReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RefValue <em>Ref Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RefValue
   * @generated
   */
  public Adapter createRefValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue <em>Predefined Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue
   * @generated
   */
  public Adapter createPredefinedValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BooleanExpression <em>Boolean Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BooleanExpression
   * @generated
   */
  public Adapter createBooleanExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SingleExpression <em>Single Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SingleExpression
   * @generated
   */
  public Adapter createSingleExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRefList <em>Def Or Field Ref List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRefList
   * @generated
   */
  public Adapter createDefOrFieldRefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef <em>Def Or Field Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRef
   * @generated
   */
  public Adapter createDefOrFieldRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AllRef <em>All Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AllRef
   * @generated
   */
  public Adapter createAllRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.GroupRefList <em>Group Ref List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.GroupRefList
   * @generated
   */
  public Adapter createGroupRefListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TTCN3Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Reference
   * @generated
   */
  public Adapter createTTCN3ReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList <em>Reference List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList
   * @generated
   */
  public Adapter createTTCN3ReferenceListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldReference <em>Field Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldReference
   * @generated
   */
  public Adapter createFieldReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayOrBitRef <em>Array Or Bit Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayOrBitRef
   * @generated
   */
  public Adapter createArrayOrBitRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FieldOrBitNumber <em>Field Or Bit Number</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FieldOrBitNumber
   * @generated
   */
  public Adapter createFieldOrBitNumberAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib <em>Array Value Or Attrib</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib
   * @generated
   */
  public Adapter createArrayValueOrAttribAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpecList <em>Array Element Spec List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpecList
   * @generated
   */
  public Adapter createArrayElementSpecListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec <em>Array Element Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpec
   * @generated
   */
  public Adapter createArrayElementSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.TimerOps <em>Timer Ops</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.TimerOps
   * @generated
   */
  public Adapter createTimerOpsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp <em>Running Timer Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RunningTimerOp
   * @generated
   */
  public Adapter createRunningTimerOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReadTimerOp <em>Read Timer Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReadTimerOp
   * @generated
   */
  public Adapter createReadTimerOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PermutationMatch <em>Permutation Match</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PermutationMatch
   * @generated
   */
  public Adapter createPermutationMatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct <em>Loop Construct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.LoopConstruct
   * @generated
   */
  public Adapter createLoopConstructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ForStatement <em>For Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ForStatement
   * @generated
   */
  public Adapter createForStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.WhileStatement <em>While Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.WhileStatement
   * @generated
   */
  public Adapter createWhileStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.DoWhileStatement <em>Do While Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.DoWhileStatement
   * @generated
   */
  public Adapter createDoWhileStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct <em>Conditional Construct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ConditionalConstruct
   * @generated
   */
  public Adapter createConditionalConstructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause <em>Else If Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ElseIfClause
   * @generated
   */
  public Adapter createElseIfClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ElseClause <em>Else Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ElseClause
   * @generated
   */
  public Adapter createElseClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Initial <em>Initial</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Initial
   * @generated
   */
  public Adapter createInitialAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct <em>Select Case Construct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseConstruct
   * @generated
   */
  public Adapter createSelectCaseConstructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SelectCaseBody <em>Select Case Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseBody
   * @generated
   */
  public Adapter createSelectCaseBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.SelectCase <em>Select Case</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.SelectCase
   * @generated
   */
  public Adapter createSelectCaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionDef <em>Function Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef
   * @generated
   */
  public Adapter createFunctionDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MtcSpec <em>Mtc Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MtcSpec
   * @generated
   */
  public Adapter createMtcSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalParList <em>Function Formal Par List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalParList
   * @generated
   */
  public Adapter createFunctionFormalParListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar <em>Function Formal Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalPar
   * @generated
   */
  public Adapter createFunctionFormalParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar <em>Formal Value Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FormalValuePar
   * @generated
   */
  public Adapter createFormalValueParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FormalTimerPar <em>Formal Timer Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FormalTimerPar
   * @generated
   */
  public Adapter createFormalTimerParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FormalPortPar <em>Formal Port Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FormalPortPar
   * @generated
   */
  public Adapter createFormalPortParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar <em>Formal Template Par</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar
   * @generated
   */
  public Adapter createFormalTemplateParAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RunsOnSpec <em>Runs On Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RunsOnSpec
   * @generated
   */
  public Adapter createRunsOnSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ReturnType <em>Return Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ReturnType
   * @generated
   */
  public Adapter createReturnTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Assignment <em>Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Assignment
   * @generated
   */
  public Adapter createAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.VariableRef <em>Variable Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.VariableRef
   * @generated
   */
  public Adapter createVariableRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.PreDefFunction <em>Pre Def Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.PreDefFunction
   * @generated
   */
  public Adapter createPreDefFunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2char <em>Fint2char</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2char
   * @generated
   */
  public Adapter createFint2charAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2unichar <em>Fint2unichar</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2unichar
   * @generated
   */
  public Adapter createFint2unicharAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2bit <em>Fint2bit</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2bit
   * @generated
   */
  public Adapter createFint2bitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2enum <em>Fint2enum</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2enum
   * @generated
   */
  public Adapter createFint2enumAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2hex <em>Fint2hex</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2hex
   * @generated
   */
  public Adapter createFint2hexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2oct <em>Fint2oct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2oct
   * @generated
   */
  public Adapter createFint2octAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2str <em>Fint2str</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2str
   * @generated
   */
  public Adapter createFint2strAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fint2float <em>Fint2float</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fint2float
   * @generated
   */
  public Adapter createFint2floatAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Ffloat2int <em>Ffloat2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Ffloat2int
   * @generated
   */
  public Adapter createFfloat2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fchar2int <em>Fchar2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fchar2int
   * @generated
   */
  public Adapter createFchar2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fchar2oct <em>Fchar2oct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fchar2oct
   * @generated
   */
  public Adapter createFchar2octAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Funichar2int <em>Funichar2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Funichar2int
   * @generated
   */
  public Adapter createFunichar2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Funichar2oct <em>Funichar2oct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Funichar2oct
   * @generated
   */
  public Adapter createFunichar2octAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fbit2int <em>Fbit2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2int
   * @generated
   */
  public Adapter createFbit2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fbit2hex <em>Fbit2hex</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2hex
   * @generated
   */
  public Adapter createFbit2hexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fbit2oct <em>Fbit2oct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2oct
   * @generated
   */
  public Adapter createFbit2octAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fbit2str <em>Fbit2str</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2str
   * @generated
   */
  public Adapter createFbit2strAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fhex2int <em>Fhex2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2int
   * @generated
   */
  public Adapter createFhex2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fhex2bit <em>Fhex2bit</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2bit
   * @generated
   */
  public Adapter createFhex2bitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fhex2oct <em>Fhex2oct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2oct
   * @generated
   */
  public Adapter createFhex2octAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fhex2str <em>Fhex2str</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2str
   * @generated
   */
  public Adapter createFhex2strAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Foct2int <em>Foct2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Foct2int
   * @generated
   */
  public Adapter createFoct2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Foct2bit <em>Foct2bit</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Foct2bit
   * @generated
   */
  public Adapter createFoct2bitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Foct2hex <em>Foct2hex</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Foct2hex
   * @generated
   */
  public Adapter createFoct2hexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Foct2str <em>Foct2str</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Foct2str
   * @generated
   */
  public Adapter createFoct2strAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Foct2char <em>Foct2char</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Foct2char
   * @generated
   */
  public Adapter createFoct2charAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Foct2unichar <em>Foct2unichar</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Foct2unichar
   * @generated
   */
  public Adapter createFoct2unicharAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fstr2int <em>Fstr2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2int
   * @generated
   */
  public Adapter createFstr2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fstr2hex <em>Fstr2hex</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2hex
   * @generated
   */
  public Adapter createFstr2hexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fstr2oct <em>Fstr2oct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2oct
   * @generated
   */
  public Adapter createFstr2octAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fstr2float <em>Fstr2float</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2float
   * @generated
   */
  public Adapter createFstr2floatAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fenum2int <em>Fenum2int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fenum2int
   * @generated
   */
  public Adapter createFenum2intAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Flengthof <em>Flengthof</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Flengthof
   * @generated
   */
  public Adapter createFlengthofAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fsizeof <em>Fsizeof</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fsizeof
   * @generated
   */
  public Adapter createFsizeofAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fispresent <em>Fispresent</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fispresent
   * @generated
   */
  public Adapter createFispresentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fischosen <em>Fischosen</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fischosen
   * @generated
   */
  public Adapter createFischosenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fisvalue <em>Fisvalue</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fisvalue
   * @generated
   */
  public Adapter createFisvalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fisbound <em>Fisbound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fisbound
   * @generated
   */
  public Adapter createFisboundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fregexp <em>Fregexp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fregexp
   * @generated
   */
  public Adapter createFregexpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fsubstr <em>Fsubstr</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fsubstr
   * @generated
   */
  public Adapter createFsubstrAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Freplace <em>Freplace</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Freplace
   * @generated
   */
  public Adapter createFreplaceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fencvalue <em>Fencvalue</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fencvalue
   * @generated
   */
  public Adapter createFencvalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Fdecvalue <em>Fdecvalue</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Fdecvalue
   * @generated
   */
  public Adapter createFdecvalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FencvalueUnichar <em>Fencvalue Unichar</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FencvalueUnichar
   * @generated
   */
  public Adapter createFencvalueUnicharAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar <em>Fdecvalue Unichar</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.FdecvalueUnichar
   * @generated
   */
  public Adapter createFdecvalueUnicharAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Frnd <em>Frnd</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Frnd
   * @generated
   */
  public Adapter createFrndAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.Ftestcasename <em>Ftestcasename</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.Ftestcasename
   * @generated
   */
  public Adapter createFtestcasenameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.XorExpression <em>Xor Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.XorExpression
   * @generated
   */
  public Adapter createXorExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AndExpression
   * @generated
   */
  public Adapter createAndExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.EqualExpression <em>Equal Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.EqualExpression
   * @generated
   */
  public Adapter createEqualExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.RelExpression <em>Rel Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.RelExpression
   * @generated
   */
  public Adapter createRelExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.ShiftExpression <em>Shift Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.ShiftExpression
   * @generated
   */
  public Adapter createShiftExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BitOrExpression <em>Bit Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BitOrExpression
   * @generated
   */
  public Adapter createBitOrExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BitXorExpression <em>Bit Xor Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BitXorExpression
   * @generated
   */
  public Adapter createBitXorExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.BitAndExpression <em>Bit And Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.BitAndExpression
   * @generated
   */
  public Adapter createBitAndExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.AddExpression <em>Add Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.AddExpression
   * @generated
   */
  public Adapter createAddExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.ugoe.cs.swe.tTCN3.MulExpression <em>Mul Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.ugoe.cs.swe.tTCN3.MulExpression
   * @generated
   */
  public Adapter createMulExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //TTCN3AdapterFactory
