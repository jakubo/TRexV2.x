/**
 */
package de.ugoe.cs.swe.tTCN3.util;

import de.ugoe.cs.swe.tTCN3.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package
 * @generated
 */
public class TTCN3Switch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static TTCN3Package modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Switch()
  {
    if (modelPackage == null)
    {
      modelPackage = TTCN3Package.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case TTCN3Package.TTCN3_FILE:
      {
        TTCN3File ttcn3File = (TTCN3File)theEObject;
        T result = caseTTCN3File(ttcn3File);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONST_DEF:
      {
        ConstDef constDef = (ConstDef)theEObject;
        T result = caseConstDef(constDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE:
      {
        Type type = (Type)theEObject;
        T result = caseType(type);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE_REFERENCE_TAIL_TYPE:
      {
        TypeReferenceTailType typeReferenceTailType = (TypeReferenceTailType)theEObject;
        T result = caseTypeReferenceTailType(typeReferenceTailType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE_REFERENCE_TAIL:
      {
        TypeReferenceTail typeReferenceTail = (TypeReferenceTail)theEObject;
        T result = caseTypeReferenceTail(typeReferenceTail);
        if (result == null) result = caseSpecTypeElement(typeReferenceTail);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE_REFERENCE:
      {
        TypeReference typeReference = (TypeReference)theEObject;
        T result = caseTypeReference(typeReference);
        if (result == null) result = caseSpecTypeElement(typeReference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SPEC_TYPE_ELEMENT:
      {
        SpecTypeElement specTypeElement = (SpecTypeElement)theEObject;
        T result = caseSpecTypeElement(specTypeElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_DEF:
      {
        ArrayDef arrayDef = (ArrayDef)theEObject;
        T result = caseArrayDef(arrayDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TTCN3_MODULE:
      {
        TTCN3Module ttcn3Module = (TTCN3Module)theEObject;
        T result = caseTTCN3Module(ttcn3Module);
        if (result == null) result = caseModuleOrGroup(ttcn3Module);
        if (result == null) result = caseReferencedType(ttcn3Module);
        if (result == null) result = caseRefValueHead(ttcn3Module);
        if (result == null) result = caseTypeReferenceTailType(ttcn3Module);
        if (result == null) result = caseTTCN3Reference(ttcn3Module);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.LANGUAGE_SPEC:
      {
        LanguageSpec languageSpec = (LanguageSpec)theEObject;
        T result = caseLanguageSpec(languageSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_PAR_DEF:
      {
        ModuleParDef moduleParDef = (ModuleParDef)theEObject;
        T result = caseModuleParDef(moduleParDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_PAR:
      {
        ModulePar modulePar = (ModulePar)theEObject;
        T result = caseModulePar(modulePar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_PAR_LIST:
      {
        ModuleParList moduleParList = (ModuleParList)theEObject;
        T result = caseModuleParList(moduleParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_PARAMETER:
      {
        ModuleParameter moduleParameter = (ModuleParameter)theEObject;
        T result = caseModuleParameter(moduleParameter);
        if (result == null) result = caseRefValue(moduleParameter);
        if (result == null) result = caseRefValueHead(moduleParameter);
        if (result == null) result = caseRefValueElement(moduleParameter);
        if (result == null) result = caseTTCN3Reference(moduleParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MULTITYPED_MODULE_PAR_LIST:
      {
        MultitypedModuleParList multitypedModuleParList = (MultitypedModuleParList)theEObject;
        T result = caseMultitypedModuleParList(multitypedModuleParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_DEFINITIONS_LIST:
      {
        ModuleDefinitionsList moduleDefinitionsList = (ModuleDefinitionsList)theEObject;
        T result = caseModuleDefinitionsList(moduleDefinitionsList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_DEFINITION:
      {
        ModuleDefinition moduleDefinition = (ModuleDefinition)theEObject;
        T result = caseModuleDefinition(moduleDefinition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FRIEND_MODULE_DEF:
      {
        FriendModuleDef friendModuleDef = (FriendModuleDef)theEObject;
        T result = caseFriendModuleDef(friendModuleDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GROUP_DEF:
      {
        GroupDef groupDef = (GroupDef)theEObject;
        T result = caseGroupDef(groupDef);
        if (result == null) result = caseModuleOrGroup(groupDef);
        if (result == null) result = caseRefValueElement(groupDef);
        if (result == null) result = caseReferencedType(groupDef);
        if (result == null) result = caseRefValueHead(groupDef);
        if (result == null) result = caseTypeReferenceTailType(groupDef);
        if (result == null) result = caseTTCN3Reference(groupDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXT_FUNCTION_DEF:
      {
        ExtFunctionDef extFunctionDef = (ExtFunctionDef)theEObject;
        T result = caseExtFunctionDef(extFunctionDef);
        if (result == null) result = caseFunctionRef(extFunctionDef);
        if (result == null) result = caseRefValue(extFunctionDef);
        if (result == null) result = caseRefValueHead(extFunctionDef);
        if (result == null) result = caseRefValueElement(extFunctionDef);
        if (result == null) result = caseTTCN3Reference(extFunctionDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXT_CONST_DEF:
      {
        ExtConstDef extConstDef = (ExtConstDef)theEObject;
        T result = caseExtConstDef(extConstDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IDENTIFIER_OBJECT_LIST:
      {
        IdentifierObjectList identifierObjectList = (IdentifierObjectList)theEObject;
        T result = caseIdentifierObjectList(identifierObjectList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NAMED_OBJECT:
      {
        NamedObject namedObject = (NamedObject)theEObject;
        T result = caseNamedObject(namedObject);
        if (result == null) result = caseRefValue(namedObject);
        if (result == null) result = caseRefValueHead(namedObject);
        if (result == null) result = caseRefValueElement(namedObject);
        if (result == null) result = caseTTCN3Reference(namedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_DEF:
      {
        ImportDef importDef = (ImportDef)theEObject;
        T result = caseImportDef(importDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_WITH_EXCEPTS:
      {
        AllWithExcepts allWithExcepts = (AllWithExcepts)theEObject;
        T result = caseAllWithExcepts(allWithExcepts);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPTS_DEF:
      {
        ExceptsDef exceptsDef = (ExceptsDef)theEObject;
        T result = caseExceptsDef(exceptsDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_SPEC:
      {
        ExceptSpec exceptSpec = (ExceptSpec)theEObject;
        T result = caseExceptSpec(exceptSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_ELEMENT:
      {
        ExceptElement exceptElement = (ExceptElement)theEObject;
        T result = caseExceptElement(exceptElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL:
      {
        IdentifierListOrAll identifierListOrAll = (IdentifierListOrAll)theEObject;
        T result = caseIdentifierListOrAll(identifierListOrAll);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_GROUP_SPEC:
      {
        ExceptGroupSpec exceptGroupSpec = (ExceptGroupSpec)theEObject;
        T result = caseExceptGroupSpec(exceptGroupSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_TYPE_DEF_SPEC:
      {
        ExceptTypeDefSpec exceptTypeDefSpec = (ExceptTypeDefSpec)theEObject;
        T result = caseExceptTypeDefSpec(exceptTypeDefSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_TEMPLATE_SPEC:
      {
        ExceptTemplateSpec exceptTemplateSpec = (ExceptTemplateSpec)theEObject;
        T result = caseExceptTemplateSpec(exceptTemplateSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_CONST_SPEC:
      {
        ExceptConstSpec exceptConstSpec = (ExceptConstSpec)theEObject;
        T result = caseExceptConstSpec(exceptConstSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_TESTCASE_SPEC:
      {
        ExceptTestcaseSpec exceptTestcaseSpec = (ExceptTestcaseSpec)theEObject;
        T result = caseExceptTestcaseSpec(exceptTestcaseSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_ALTSTEP_SPEC:
      {
        ExceptAltstepSpec exceptAltstepSpec = (ExceptAltstepSpec)theEObject;
        T result = caseExceptAltstepSpec(exceptAltstepSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_FUNCTION_SPEC:
      {
        ExceptFunctionSpec exceptFunctionSpec = (ExceptFunctionSpec)theEObject;
        T result = caseExceptFunctionSpec(exceptFunctionSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_SIGNATURE_SPEC:
      {
        ExceptSignatureSpec exceptSignatureSpec = (ExceptSignatureSpec)theEObject;
        T result = caseExceptSignatureSpec(exceptSignatureSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPT_MODULE_PAR_SPEC:
      {
        ExceptModuleParSpec exceptModuleParSpec = (ExceptModuleParSpec)theEObject;
        T result = caseExceptModuleParSpec(exceptModuleParSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_SPEC:
      {
        ImportSpec importSpec = (ImportSpec)theEObject;
        T result = caseImportSpec(importSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_ELEMENT:
      {
        ImportElement importElement = (ImportElement)theEObject;
        T result = caseImportElement(importElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_GROUP_SPEC:
      {
        ImportGroupSpec importGroupSpec = (ImportGroupSpec)theEObject;
        T result = caseImportGroupSpec(importGroupSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT:
      {
        IdentifierListOrAllWithExcept identifierListOrAllWithExcept = (IdentifierListOrAllWithExcept)theEObject;
        T result = caseIdentifierListOrAllWithExcept(identifierListOrAllWithExcept);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_WITH_EXCEPT:
      {
        AllWithExcept allWithExcept = (AllWithExcept)theEObject;
        T result = caseAllWithExcept(allWithExcept);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_TYPE_DEF_SPEC:
      {
        ImportTypeDefSpec importTypeDefSpec = (ImportTypeDefSpec)theEObject;
        T result = caseImportTypeDefSpec(importTypeDefSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_TEMPLATE_SPEC:
      {
        ImportTemplateSpec importTemplateSpec = (ImportTemplateSpec)theEObject;
        T result = caseImportTemplateSpec(importTemplateSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_CONST_SPEC:
      {
        ImportConstSpec importConstSpec = (ImportConstSpec)theEObject;
        T result = caseImportConstSpec(importConstSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_ALTSTEP_SPEC:
      {
        ImportAltstepSpec importAltstepSpec = (ImportAltstepSpec)theEObject;
        T result = caseImportAltstepSpec(importAltstepSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_TESTCASE_SPEC:
      {
        ImportTestcaseSpec importTestcaseSpec = (ImportTestcaseSpec)theEObject;
        T result = caseImportTestcaseSpec(importTestcaseSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_FUNCTION_SPEC:
      {
        ImportFunctionSpec importFunctionSpec = (ImportFunctionSpec)theEObject;
        T result = caseImportFunctionSpec(importFunctionSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_SIGNATURE_SPEC:
      {
        ImportSignatureSpec importSignatureSpec = (ImportSignatureSpec)theEObject;
        T result = caseImportSignatureSpec(importSignatureSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IMPORT_MODULE_PAR_SPEC:
      {
        ImportModuleParSpec importModuleParSpec = (ImportModuleParSpec)theEObject;
        T result = caseImportModuleParSpec(importModuleParSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GROUP_REF_LIST_WITH_EXCEPT:
      {
        GroupRefListWithExcept groupRefListWithExcept = (GroupRefListWithExcept)theEObject;
        T result = caseGroupRefListWithExcept(groupRefListWithExcept);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.QUALIFIED_IDENTIFIER_WITH_EXCEPT:
      {
        QualifiedIdentifierWithExcept qualifiedIdentifierWithExcept = (QualifiedIdentifierWithExcept)theEObject;
        T result = caseQualifiedIdentifierWithExcept(qualifiedIdentifierWithExcept);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_GROUPS_WITH_EXCEPT:
      {
        AllGroupsWithExcept allGroupsWithExcept = (AllGroupsWithExcept)theEObject;
        T result = caseAllGroupsWithExcept(allGroupsWithExcept);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALTSTEP_DEF:
      {
        AltstepDef altstepDef = (AltstepDef)theEObject;
        T result = caseAltstepDef(altstepDef);
        if (result == null) result = caseFunctionRef(altstepDef);
        if (result == null) result = caseRefValue(altstepDef);
        if (result == null) result = caseRefValueHead(altstepDef);
        if (result == null) result = caseRefValueElement(altstepDef);
        if (result == null) result = caseTTCN3Reference(altstepDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST:
      {
        AltstepLocalDefList altstepLocalDefList = (AltstepLocalDefList)theEObject;
        T result = caseAltstepLocalDefList(altstepLocalDefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALTSTEP_LOCAL_DEF:
      {
        AltstepLocalDef altstepLocalDef = (AltstepLocalDef)theEObject;
        T result = caseAltstepLocalDef(altstepLocalDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TESTCASE_DEF:
      {
        TestcaseDef testcaseDef = (TestcaseDef)theEObject;
        T result = caseTestcaseDef(testcaseDef);
        if (result == null) result = caseTTCN3Reference(testcaseDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_OR_VALUE_FORMAL_PAR_LIST:
      {
        TemplateOrValueFormalParList templateOrValueFormalParList = (TemplateOrValueFormalParList)theEObject;
        T result = caseTemplateOrValueFormalParList(templateOrValueFormalParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_OR_VALUE_FORMAL_PAR:
      {
        TemplateOrValueFormalPar templateOrValueFormalPar = (TemplateOrValueFormalPar)theEObject;
        T result = caseTemplateOrValueFormalPar(templateOrValueFormalPar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONFIG_SPEC:
      {
        ConfigSpec configSpec = (ConfigSpec)theEObject;
        T result = caseConfigSpec(configSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SYSTEM_SPEC:
      {
        SystemSpec systemSpec = (SystemSpec)theEObject;
        T result = caseSystemSpec(systemSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SIGNATURE_DEF:
      {
        SignatureDef signatureDef = (SignatureDef)theEObject;
        T result = caseSignatureDef(signatureDef);
        if (result == null) result = caseReferencedType(signatureDef);
        if (result == null) result = caseTypeReferenceTailType(signatureDef);
        if (result == null) result = caseTTCN3Reference(signatureDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXCEPTION_SPEC:
      {
        ExceptionSpec exceptionSpec = (ExceptionSpec)theEObject;
        T result = caseExceptionSpec(exceptionSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SIGNATURE_FORMAL_PAR_LIST:
      {
        SignatureFormalParList signatureFormalParList = (SignatureFormalParList)theEObject;
        T result = caseSignatureFormalParList(signatureFormalParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_CONTROL_PART:
      {
        ModuleControlPart moduleControlPart = (ModuleControlPart)theEObject;
        T result = caseModuleControlPart(moduleControlPart);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_CONTROL_BODY:
      {
        ModuleControlBody moduleControlBody = (ModuleControlBody)theEObject;
        T result = caseModuleControlBody(moduleControlBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST:
      {
        ControlStatementOrDefList controlStatementOrDefList = (ControlStatementOrDefList)theEObject;
        T result = caseControlStatementOrDefList(controlStatementOrDefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF:
      {
        ControlStatementOrDef controlStatementOrDef = (ControlStatementOrDef)theEObject;
        T result = caseControlStatementOrDef(controlStatementOrDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONTROL_STATEMENT:
      {
        ControlStatement controlStatement = (ControlStatement)theEObject;
        T result = caseControlStatement(controlStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SUT_STATEMENTS:
      {
        SUTStatements sutStatements = (SUTStatements)theEObject;
        T result = caseSUTStatements(sutStatements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ACTION_TEXT:
      {
        ActionText actionText = (ActionText)theEObject;
        T result = caseActionText(actionText);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BEHAVIOUR_STATEMENTS:
      {
        BehaviourStatements behaviourStatements = (BehaviourStatements)theEObject;
        T result = caseBehaviourStatements(behaviourStatements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ACTIVATE_OP:
      {
        ActivateOp activateOp = (ActivateOp)theEObject;
        T result = caseActivateOp(activateOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DEACTIVATE_STATEMENT:
      {
        DeactivateStatement deactivateStatement = (DeactivateStatement)theEObject;
        T result = caseDeactivateStatement(deactivateStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.LABEL_STATEMENT:
      {
        LabelStatement labelStatement = (LabelStatement)theEObject;
        T result = caseLabelStatement(labelStatement);
        if (result == null) result = caseTTCN3Reference(labelStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GOTO_STATEMENT:
      {
        GotoStatement gotoStatement = (GotoStatement)theEObject;
        T result = caseGotoStatement(gotoStatement);
        if (result == null) result = caseTTCN3Reference(gotoStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RETURN_STATEMENT:
      {
        ReturnStatement returnStatement = (ReturnStatement)theEObject;
        T result = caseReturnStatement(returnStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INTERLEAVED_CONSTRUCT:
      {
        InterleavedConstruct interleavedConstruct = (InterleavedConstruct)theEObject;
        T result = caseInterleavedConstruct(interleavedConstruct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INTERLEAVED_GUARD_LIST:
      {
        InterleavedGuardList interleavedGuardList = (InterleavedGuardList)theEObject;
        T result = caseInterleavedGuardList(interleavedGuardList);
        if (result == null) result = caseInterleavedConstruct(interleavedGuardList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INTERLEAVED_GUARD_ELEMENT:
      {
        InterleavedGuardElement interleavedGuardElement = (InterleavedGuardElement)theEObject;
        T result = caseInterleavedGuardElement(interleavedGuardElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INTERLEAVED_GUARD:
      {
        InterleavedGuard interleavedGuard = (InterleavedGuard)theEObject;
        T result = caseInterleavedGuard(interleavedGuard);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALT_CONSTRUCT:
      {
        AltConstruct altConstruct = (AltConstruct)theEObject;
        T result = caseAltConstruct(altConstruct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALT_GUARD_LIST:
      {
        AltGuardList altGuardList = (AltGuardList)theEObject;
        T result = caseAltGuardList(altGuardList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ELSE_STATEMENT:
      {
        ElseStatement elseStatement = (ElseStatement)theEObject;
        T result = caseElseStatement(elseStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GUARD_STATEMENT:
      {
        GuardStatement guardStatement = (GuardStatement)theEObject;
        T result = caseGuardStatement(guardStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GUARD_OP:
      {
        GuardOp guardOp = (GuardOp)theEObject;
        T result = caseGuardOp(guardOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DONE_STATEMENT:
      {
        DoneStatement doneStatement = (DoneStatement)theEObject;
        T result = caseDoneStatement(doneStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.KILLED_STATEMENT:
      {
        KilledStatement killedStatement = (KilledStatement)theEObject;
        T result = caseKilledStatement(killedStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_OR_ANY:
      {
        ComponentOrAny componentOrAny = (ComponentOrAny)theEObject;
        T result = caseComponentOrAny(componentOrAny);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INDEX_ASSIGNMENT:
      {
        IndexAssignment indexAssignment = (IndexAssignment)theEObject;
        T result = caseIndexAssignment(indexAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INDEX_SPEC:
      {
        IndexSpec indexSpec = (IndexSpec)theEObject;
        T result = caseIndexSpec(indexSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GET_REPLY_STATEMENT:
      {
        GetReplyStatement getReplyStatement = (GetReplyStatement)theEObject;
        T result = caseGetReplyStatement(getReplyStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CHECK_STATEMENT:
      {
        CheckStatement checkStatement = (CheckStatement)theEObject;
        T result = caseCheckStatement(checkStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_CHECK_OP:
      {
        PortCheckOp portCheckOp = (PortCheckOp)theEObject;
        T result = casePortCheckOp(portCheckOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CHECK_PARAMETER:
      {
        CheckParameter checkParameter = (CheckParameter)theEObject;
        T result = caseCheckParameter(checkParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REDIRECT_PRESENT:
      {
        RedirectPresent redirectPresent = (RedirectPresent)theEObject;
        T result = caseRedirectPresent(redirectPresent);
        if (result == null) result = caseCheckParameter(redirectPresent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FROM_CLAUSE_PRESENT:
      {
        FromClausePresent fromClausePresent = (FromClausePresent)theEObject;
        T result = caseFromClausePresent(fromClausePresent);
        if (result == null) result = caseCheckParameter(fromClausePresent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CHECK_PORT_OPS_PRESENT:
      {
        CheckPortOpsPresent checkPortOpsPresent = (CheckPortOpsPresent)theEObject;
        T result = caseCheckPortOpsPresent(checkPortOpsPresent);
        if (result == null) result = caseCheckParameter(checkPortOpsPresent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_GET_REPLY_OP:
      {
        PortGetReplyOp portGetReplyOp = (PortGetReplyOp)theEObject;
        T result = casePortGetReplyOp(portGetReplyOp);
        if (result == null) result = caseCheckPortOpsPresent(portGetReplyOp);
        if (result == null) result = caseCheckParameter(portGetReplyOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_REDIRECT_WITH_VALUE_AND_PARAM:
      {
        PortRedirectWithValueAndParam portRedirectWithValueAndParam = (PortRedirectWithValueAndParam)theEObject;
        T result = casePortRedirectWithValueAndParam(portRedirectWithValueAndParam);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC:
      {
        RedirectWithValueAndParamSpec redirectWithValueAndParamSpec = (RedirectWithValueAndParamSpec)theEObject;
        T result = caseRedirectWithValueAndParamSpec(redirectWithValueAndParamSpec);
        if (result == null) result = casePortRedirectWithValueAndParam(redirectWithValueAndParamSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VALUE_MATCH_SPEC:
      {
        ValueMatchSpec valueMatchSpec = (ValueMatchSpec)theEObject;
        T result = caseValueMatchSpec(valueMatchSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CATCH_STATEMENT:
      {
        CatchStatement catchStatement = (CatchStatement)theEObject;
        T result = caseCatchStatement(catchStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_CATCH_OP:
      {
        PortCatchOp portCatchOp = (PortCatchOp)theEObject;
        T result = casePortCatchOp(portCatchOp);
        if (result == null) result = caseCheckPortOpsPresent(portCatchOp);
        if (result == null) result = caseCheckParameter(portCatchOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CATCH_OP_PARAMETER:
      {
        CatchOpParameter catchOpParameter = (CatchOpParameter)theEObject;
        T result = caseCatchOpParameter(catchOpParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GET_CALL_STATEMENT:
      {
        GetCallStatement getCallStatement = (GetCallStatement)theEObject;
        T result = caseGetCallStatement(getCallStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_GET_CALL_OP:
      {
        PortGetCallOp portGetCallOp = (PortGetCallOp)theEObject;
        T result = casePortGetCallOp(portGetCallOp);
        if (result == null) result = caseCheckPortOpsPresent(portGetCallOp);
        if (result == null) result = caseCheckParameter(portGetCallOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM:
      {
        PortRedirectWithParam portRedirectWithParam = (PortRedirectWithParam)theEObject;
        T result = casePortRedirectWithParam(portRedirectWithParam);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC:
      {
        RedirectWithParamSpec redirectWithParamSpec = (RedirectWithParamSpec)theEObject;
        T result = caseRedirectWithParamSpec(redirectWithParamSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PARAM_SPEC:
      {
        ParamSpec paramSpec = (ParamSpec)theEObject;
        T result = caseParamSpec(paramSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PARAM_ASSIGNMENT_LIST:
      {
        ParamAssignmentList paramAssignmentList = (ParamAssignmentList)theEObject;
        T result = caseParamAssignmentList(paramAssignmentList);
        if (result == null) result = caseParamSpec(paramAssignmentList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ASSIGNMENT_LIST:
      {
        AssignmentList assignmentList = (AssignmentList)theEObject;
        T result = caseAssignmentList(assignmentList);
        if (result == null) result = caseParamAssignmentList(assignmentList);
        if (result == null) result = caseParamSpec(assignmentList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VARIABLE_ASSIGNMENT:
      {
        VariableAssignment variableAssignment = (VariableAssignment)theEObject;
        T result = caseVariableAssignment(variableAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VARIABLE_LIST:
      {
        VariableList variableList = (VariableList)theEObject;
        T result = caseVariableList(variableList);
        if (result == null) result = caseParamAssignmentList(variableList);
        if (result == null) result = caseParamSpec(variableList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VARIABLE_ENTRY:
      {
        VariableEntry variableEntry = (VariableEntry)theEObject;
        T result = caseVariableEntry(variableEntry);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TRIGGER_STATEMENT:
      {
        TriggerStatement triggerStatement = (TriggerStatement)theEObject;
        T result = caseTriggerStatement(triggerStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_TRIGGER_OP:
      {
        PortTriggerOp portTriggerOp = (PortTriggerOp)theEObject;
        T result = casePortTriggerOp(portTriggerOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RECEIVE_STATEMENT:
      {
        ReceiveStatement receiveStatement = (ReceiveStatement)theEObject;
        T result = caseReceiveStatement(receiveStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_OR_ANY:
      {
        PortOrAny portOrAny = (PortOrAny)theEObject;
        T result = casePortOrAny(portOrAny);
        if (result == null) result = caseGetReplyStatement(portOrAny);
        if (result == null) result = caseCheckStatement(portOrAny);
        if (result == null) result = caseCatchStatement(portOrAny);
        if (result == null) result = caseGetCallStatement(portOrAny);
        if (result == null) result = caseTriggerStatement(portOrAny);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_RECEIVE_OP:
      {
        PortReceiveOp portReceiveOp = (PortReceiveOp)theEObject;
        T result = casePortReceiveOp(portReceiveOp);
        if (result == null) result = caseCheckPortOpsPresent(portReceiveOp);
        if (result == null) result = caseCheckParameter(portReceiveOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FROM_CLAUSE:
      {
        FromClause fromClause = (FromClause)theEObject;
        T result = caseFromClause(fromClause);
        if (result == null) result = caseFromClausePresent(fromClause);
        if (result == null) result = caseCheckParameter(fromClause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ADDRESS_REF_LIST:
      {
        AddressRefList addressRefList = (AddressRefList)theEObject;
        T result = caseAddressRefList(addressRefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_REDIRECT:
      {
        PortRedirect portRedirect = (PortRedirect)theEObject;
        T result = casePortRedirect(portRedirect);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SENDER_SPEC:
      {
        SenderSpec senderSpec = (SenderSpec)theEObject;
        T result = caseSenderSpec(senderSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VALUE_SPEC:
      {
        ValueSpec valueSpec = (ValueSpec)theEObject;
        T result = caseValueSpec(valueSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_VALUE_SPEC:
      {
        SingleValueSpec singleValueSpec = (SingleValueSpec)theEObject;
        T result = caseSingleValueSpec(singleValueSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALT_GUARD_CHAR:
      {
        AltGuardChar altGuardChar = (AltGuardChar)theEObject;
        T result = caseAltGuardChar(altGuardChar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALTSTEP_INSTANCE:
      {
        AltstepInstance altstepInstance = (AltstepInstance)theEObject;
        T result = caseAltstepInstance(altstepInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_INSTANCE:
      {
        FunctionInstance functionInstance = (FunctionInstance)theEObject;
        T result = caseFunctionInstance(functionInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_REF:
      {
        FunctionRef functionRef = (FunctionRef)theEObject;
        T result = caseFunctionRef(functionRef);
        if (result == null) result = caseRefValue(functionRef);
        if (result == null) result = caseRefValueHead(functionRef);
        if (result == null) result = caseRefValueElement(functionRef);
        if (result == null) result = caseTTCN3Reference(functionRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST:
      {
        FunctionActualParList functionActualParList = (FunctionActualParList)theEObject;
        T result = caseFunctionActualParList(functionActualParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT:
      {
        FunctionActualParAssignment functionActualParAssignment = (FunctionActualParAssignment)theEObject;
        T result = caseFunctionActualParAssignment(functionActualParAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_REF_ASSIGNMENT:
      {
        ComponentRefAssignment componentRefAssignment = (ComponentRefAssignment)theEObject;
        T result = caseComponentRefAssignment(componentRefAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FORMAL_PORT_AND_VALUE_PAR:
      {
        FormalPortAndValuePar formalPortAndValuePar = (FormalPortAndValuePar)theEObject;
        T result = caseFormalPortAndValuePar(formalPortAndValuePar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_REF_ASSIGNMENT:
      {
        PortRefAssignment portRefAssignment = (PortRefAssignment)theEObject;
        T result = casePortRefAssignment(portRefAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_REF_ASSIGNMENT:
      {
        TimerRefAssignment timerRefAssignment = (TimerRefAssignment)theEObject;
        T result = caseTimerRefAssignment(timerRefAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_ACTUAL_PAR:
      {
        FunctionActualPar functionActualPar = (FunctionActualPar)theEObject;
        T result = caseFunctionActualPar(functionActualPar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_REF:
      {
        ComponentRef componentRef = (ComponentRef)theEObject;
        T result = caseComponentRef(componentRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_OR_DEFAULT_REFERENCE:
      {
        ComponentOrDefaultReference componentOrDefaultReference = (ComponentOrDefaultReference)theEObject;
        T result = caseComponentOrDefaultReference(componentOrDefaultReference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TESTCASE_INSTANCE:
      {
        TestcaseInstance testcaseInstance = (TestcaseInstance)theEObject;
        T result = caseTestcaseInstance(testcaseInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST:
      {
        TestcaseActualParList testcaseActualParList = (TestcaseActualParList)theEObject;
        T result = caseTestcaseActualParList(testcaseActualParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_STATEMENTS:
      {
        TimerStatements timerStatements = (TimerStatements)theEObject;
        T result = caseTimerStatements(timerStatements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMEOUT_STATEMENT:
      {
        TimeoutStatement timeoutStatement = (TimeoutStatement)theEObject;
        T result = caseTimeoutStatement(timeoutStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.START_TIMER_STATEMENT:
      {
        StartTimerStatement startTimerStatement = (StartTimerStatement)theEObject;
        T result = caseStartTimerStatement(startTimerStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STOP_TIMER_STATEMENT:
      {
        StopTimerStatement stopTimerStatement = (StopTimerStatement)theEObject;
        T result = caseStopTimerStatement(stopTimerStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_REF_OR_ANY:
      {
        TimerRefOrAny timerRefOrAny = (TimerRefOrAny)theEObject;
        T result = caseTimerRefOrAny(timerRefOrAny);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_REF_OR_ALL:
      {
        TimerRefOrAll timerRefOrAll = (TimerRefOrAll)theEObject;
        T result = caseTimerRefOrAll(timerRefOrAll);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BASIC_STATEMENTS:
      {
        BasicStatements basicStatements = (BasicStatements)theEObject;
        T result = caseBasicStatements(basicStatements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STATEMENT_BLOCK:
      {
        StatementBlock statementBlock = (StatementBlock)theEObject;
        T result = caseStatementBlock(statementBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_STATEMENT_LIST:
      {
        FunctionStatementList functionStatementList = (FunctionStatementList)theEObject;
        T result = caseFunctionStatementList(functionStatementList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_STATEMENT:
      {
        FunctionStatement functionStatement = (FunctionStatement)theEObject;
        T result = caseFunctionStatement(functionStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TESTCASE_OPERATION:
      {
        TestcaseOperation testcaseOperation = (TestcaseOperation)theEObject;
        T result = caseTestcaseOperation(testcaseOperation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SET_LOCAL_VERDICT:
      {
        SetLocalVerdict setLocalVerdict = (SetLocalVerdict)theEObject;
        T result = caseSetLocalVerdict(setLocalVerdict);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONFIGURATION_STATEMENTS:
      {
        ConfigurationStatements configurationStatements = (ConfigurationStatements)theEObject;
        T result = caseConfigurationStatements(configurationStatements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.KILL_TC_STATEMENT:
      {
        KillTCStatement killTCStatement = (KillTCStatement)theEObject;
        T result = caseKillTCStatement(killTCStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STOP_TC_STATEMENT:
      {
        StopTCStatement stopTCStatement = (StopTCStatement)theEObject;
        T result = caseStopTCStatement(stopTCStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_REFERENCE_OR_LITERAL:
      {
        ComponentReferenceOrLiteral componentReferenceOrLiteral = (ComponentReferenceOrLiteral)theEObject;
        T result = caseComponentReferenceOrLiteral(componentReferenceOrLiteral);
        if (result == null) result = caseKillTCStatement(componentReferenceOrLiteral);
        if (result == null) result = caseStopTCStatement(componentReferenceOrLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.START_TC_STATEMENT:
      {
        StartTCStatement startTCStatement = (StartTCStatement)theEObject;
        T result = caseStartTCStatement(startTCStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.UNMAP_STATEMENT:
      {
        UnmapStatement unmapStatement = (UnmapStatement)theEObject;
        T result = caseUnmapStatement(unmapStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DISCONNECT_STATEMENT:
      {
        DisconnectStatement disconnectStatement = (DisconnectStatement)theEObject;
        T result = caseDisconnectStatement(disconnectStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_CONNECTIONS_SPEC:
      {
        AllConnectionsSpec allConnectionsSpec = (AllConnectionsSpec)theEObject;
        T result = caseAllConnectionsSpec(allConnectionsSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_PORTS_SPEC:
      {
        AllPortsSpec allPortsSpec = (AllPortsSpec)theEObject;
        T result = caseAllPortsSpec(allPortsSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MAP_STATEMENT:
      {
        MapStatement mapStatement = (MapStatement)theEObject;
        T result = caseMapStatement(mapStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PARAM_CLAUSE:
      {
        ParamClause paramClause = (ParamClause)theEObject;
        T result = caseParamClause(paramClause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONNECT_STATEMENT:
      {
        ConnectStatement connectStatement = (ConnectStatement)theEObject;
        T result = caseConnectStatement(connectStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_CONNECTION_SPEC:
      {
        SingleConnectionSpec singleConnectionSpec = (SingleConnectionSpec)theEObject;
        T result = caseSingleConnectionSpec(singleConnectionSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_REF:
      {
        PortRef portRef = (PortRef)theEObject;
        T result = casePortRef(portRef);
        if (result == null) result = caseAllConnectionsSpec(portRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMMUNICATION_STATEMENTS:
      {
        CommunicationStatements communicationStatements = (CommunicationStatements)theEObject;
        T result = caseCommunicationStatements(communicationStatements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CHECK_STATE_STATEMENT:
      {
        CheckStateStatement checkStateStatement = (CheckStateStatement)theEObject;
        T result = caseCheckStateStatement(checkStateStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_OR_ALL_ANY:
      {
        PortOrAllAny portOrAllAny = (PortOrAllAny)theEObject;
        T result = casePortOrAllAny(portOrAllAny);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.HALT_STATEMENT:
      {
        HaltStatement haltStatement = (HaltStatement)theEObject;
        T result = caseHaltStatement(haltStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.START_STATEMENT:
      {
        StartStatement startStatement = (StartStatement)theEObject;
        T result = caseStartStatement(startStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STOP_STATEMENT:
      {
        StopStatement stopStatement = (StopStatement)theEObject;
        T result = caseStopStatement(stopStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CLEAR_STATEMENT:
      {
        ClearStatement clearStatement = (ClearStatement)theEObject;
        T result = caseClearStatement(clearStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_OR_ALL:
      {
        PortOrAll portOrAll = (PortOrAll)theEObject;
        T result = casePortOrAll(portOrAll);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RAISE_STATEMENT:
      {
        RaiseStatement raiseStatement = (RaiseStatement)theEObject;
        T result = caseRaiseStatement(raiseStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_RAISE_OP:
      {
        PortRaiseOp portRaiseOp = (PortRaiseOp)theEObject;
        T result = casePortRaiseOp(portRaiseOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REPLY_STATEMENT:
      {
        ReplyStatement replyStatement = (ReplyStatement)theEObject;
        T result = caseReplyStatement(replyStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_REPLY_OP:
      {
        PortReplyOp portReplyOp = (PortReplyOp)theEObject;
        T result = casePortReplyOp(portReplyOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REPLY_VALUE:
      {
        ReplyValue replyValue = (ReplyValue)theEObject;
        T result = caseReplyValue(replyValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_STATEMENT:
      {
        CallStatement callStatement = (CallStatement)theEObject;
        T result = caseCallStatement(callStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_CALL_OP:
      {
        PortCallOp portCallOp = (PortCallOp)theEObject;
        T result = casePortCallOp(portCallOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_PARAMETERS:
      {
        CallParameters callParameters = (CallParameters)theEObject;
        T result = caseCallParameters(callParameters);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_TIMER_VALUE:
      {
        CallTimerValue callTimerValue = (CallTimerValue)theEObject;
        T result = caseCallTimerValue(callTimerValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_CALL_BODY:
      {
        PortCallBody portCallBody = (PortCallBody)theEObject;
        T result = casePortCallBody(portCallBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_BODY_STATEMENT_LIST:
      {
        CallBodyStatementList callBodyStatementList = (CallBodyStatementList)theEObject;
        T result = caseCallBodyStatementList(callBodyStatementList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_BODY_STATEMENT:
      {
        CallBodyStatement callBodyStatement = (CallBodyStatement)theEObject;
        T result = caseCallBodyStatement(callBodyStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_BODY_GUARD:
      {
        CallBodyGuard callBodyGuard = (CallBodyGuard)theEObject;
        T result = caseCallBodyGuard(callBodyGuard);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CALL_BODY_OPS:
      {
        CallBodyOps callBodyOps = (CallBodyOps)theEObject;
        T result = caseCallBodyOps(callBodyOps);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SEND_STATEMENT:
      {
        SendStatement sendStatement = (SendStatement)theEObject;
        T result = caseSendStatement(sendStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_SEND_OP:
      {
        PortSendOp portSendOp = (PortSendOp)theEObject;
        T result = casePortSendOp(portSendOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TO_CLAUSE:
      {
        ToClause toClause = (ToClause)theEObject;
        T result = caseToClause(toClause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_DEF_LIST:
      {
        FunctionDefList functionDefList = (FunctionDefList)theEObject;
        T result = caseFunctionDefList(functionDefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_LOCAL_DEF:
      {
        FunctionLocalDef functionLocalDef = (FunctionLocalDef)theEObject;
        T result = caseFunctionLocalDef(functionLocalDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_LOCAL_INST:
      {
        FunctionLocalInst functionLocalInst = (FunctionLocalInst)theEObject;
        T result = caseFunctionLocalInst(functionLocalInst);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_INSTANCE:
      {
        TimerInstance timerInstance = (TimerInstance)theEObject;
        T result = caseTimerInstance(timerInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VAR_INSTANCE:
      {
        VarInstance varInstance = (VarInstance)theEObject;
        T result = caseVarInstance(varInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VAR_LIST:
      {
        VarList varList = (VarList)theEObject;
        T result = caseVarList(varList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MODULE_OR_GROUP:
      {
        ModuleOrGroup moduleOrGroup = (ModuleOrGroup)theEObject;
        T result = caseModuleOrGroup(moduleOrGroup);
        if (result == null) result = caseReferencedType(moduleOrGroup);
        if (result == null) result = caseRefValueHead(moduleOrGroup);
        if (result == null) result = caseTypeReferenceTailType(moduleOrGroup);
        if (result == null) result = caseTTCN3Reference(moduleOrGroup);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REFERENCED_TYPE:
      {
        ReferencedType referencedType = (ReferencedType)theEObject;
        T result = caseReferencedType(referencedType);
        if (result == null) result = caseTypeReferenceTailType(referencedType);
        if (result == null) result = caseTTCN3Reference(referencedType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE_DEF:
      {
        TypeDef typeDef = (TypeDef)theEObject;
        T result = caseTypeDef(typeDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE_DEF_BODY:
      {
        TypeDefBody typeDefBody = (TypeDefBody)theEObject;
        T result = caseTypeDefBody(typeDefBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SUB_TYPE_DEF:
      {
        SubTypeDef subTypeDef = (SubTypeDef)theEObject;
        T result = caseSubTypeDef(subTypeDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SUB_TYPE_DEF_NAMED:
      {
        SubTypeDefNamed subTypeDefNamed = (SubTypeDefNamed)theEObject;
        T result = caseSubTypeDefNamed(subTypeDefNamed);
        if (result == null) result = caseReferencedType(subTypeDefNamed);
        if (result == null) result = caseSubTypeDef(subTypeDefNamed);
        if (result == null) result = caseFieldReference(subTypeDefNamed);
        if (result == null) result = caseTypeReferenceTailType(subTypeDefNamed);
        if (result == null) result = caseRefValue(subTypeDefNamed);
        if (result == null) result = caseTTCN3Reference(subTypeDefNamed);
        if (result == null) result = caseRefValueElement(subTypeDefNamed);
        if (result == null) result = caseRefValueHead(subTypeDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STRUCTURED_TYPE_DEF:
      {
        StructuredTypeDef structuredTypeDef = (StructuredTypeDef)theEObject;
        T result = caseStructuredTypeDef(structuredTypeDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RECORD_DEF:
      {
        RecordDef recordDef = (RecordDef)theEObject;
        T result = caseRecordDef(recordDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RECORD_DEF_NAMED:
      {
        RecordDefNamed recordDefNamed = (RecordDefNamed)theEObject;
        T result = caseRecordDefNamed(recordDefNamed);
        if (result == null) result = caseReferencedType(recordDefNamed);
        if (result == null) result = caseRecordDef(recordDefNamed);
        if (result == null) result = caseTypeReferenceTailType(recordDefNamed);
        if (result == null) result = caseTTCN3Reference(recordDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RECORD_OF_DEF:
      {
        RecordOfDef recordOfDef = (RecordOfDef)theEObject;
        T result = caseRecordOfDef(recordOfDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RECORD_OF_DEF_NAMED:
      {
        RecordOfDefNamed recordOfDefNamed = (RecordOfDefNamed)theEObject;
        T result = caseRecordOfDefNamed(recordOfDefNamed);
        if (result == null) result = caseReferencedType(recordOfDefNamed);
        if (result == null) result = caseRecordOfDef(recordOfDefNamed);
        if (result == null) result = caseTypeReferenceTailType(recordOfDefNamed);
        if (result == null) result = caseTTCN3Reference(recordOfDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STRUCT_DEF_BODY:
      {
        StructDefBody structDefBody = (StructDefBody)theEObject;
        T result = caseStructDefBody(structDefBody);
        if (result == null) result = caseRecordDef(structDefBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STRUCT_FIELD_DEF:
      {
        StructFieldDef structFieldDef = (StructFieldDef)theEObject;
        T result = caseStructFieldDef(structFieldDef);
        if (result == null) result = caseFieldReference(structFieldDef);
        if (result == null) result = caseTypeReferenceTailType(structFieldDef);
        if (result == null) result = caseRefValue(structFieldDef);
        if (result == null) result = caseRefValueElement(structFieldDef);
        if (result == null) result = caseRefValueHead(structFieldDef);
        if (result == null) result = caseTTCN3Reference(structFieldDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SET_OF_DEF:
      {
        SetOfDef setOfDef = (SetOfDef)theEObject;
        T result = caseSetOfDef(setOfDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SET_OF_DEF_NAMED:
      {
        SetOfDefNamed setOfDefNamed = (SetOfDefNamed)theEObject;
        T result = caseSetOfDefNamed(setOfDefNamed);
        if (result == null) result = caseReferencedType(setOfDefNamed);
        if (result == null) result = caseSetOfDef(setOfDefNamed);
        if (result == null) result = caseTypeReferenceTailType(setOfDefNamed);
        if (result == null) result = caseTTCN3Reference(setOfDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_DEF:
      {
        PortDef portDef = (PortDef)theEObject;
        T result = casePortDef(portDef);
        if (result == null) result = caseReferencedType(portDef);
        if (result == null) result = caseTypeReferenceTailType(portDef);
        if (result == null) result = caseTTCN3Reference(portDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_DEF_BODY:
      {
        PortDefBody portDefBody = (PortDefBody)theEObject;
        T result = casePortDefBody(portDefBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_DEF_ATTRIBS:
      {
        PortDefAttribs portDefAttribs = (PortDefAttribs)theEObject;
        T result = casePortDefAttribs(portDefAttribs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MIXED_ATTRIBS:
      {
        MixedAttribs mixedAttribs = (MixedAttribs)theEObject;
        T result = caseMixedAttribs(mixedAttribs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MIXED_LIST:
      {
        MixedList mixedList = (MixedList)theEObject;
        T result = caseMixedList(mixedList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PROC_OR_TYPE_LIST:
      {
        ProcOrTypeList procOrTypeList = (ProcOrTypeList)theEObject;
        T result = caseProcOrTypeList(procOrTypeList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PROC_OR_TYPE:
      {
        ProcOrType procOrType = (ProcOrType)theEObject;
        T result = caseProcOrType(procOrType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MESSAGE_ATTRIBS:
      {
        MessageAttribs messageAttribs = (MessageAttribs)theEObject;
        T result = caseMessageAttribs(messageAttribs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONFIG_PARAM_DEF:
      {
        ConfigParamDef configParamDef = (ConfigParamDef)theEObject;
        T result = caseConfigParamDef(configParamDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MAP_PARAM_DEF:
      {
        MapParamDef mapParamDef = (MapParamDef)theEObject;
        T result = caseMapParamDef(mapParamDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.UNMAP_PARAM_DEF:
      {
        UnmapParamDef unmapParamDef = (UnmapParamDef)theEObject;
        T result = caseUnmapParamDef(unmapParamDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ADDRESS_DECL:
      {
        AddressDecl addressDecl = (AddressDecl)theEObject;
        T result = caseAddressDecl(addressDecl);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PROCEDURE_ATTRIBS:
      {
        ProcedureAttribs procedureAttribs = (ProcedureAttribs)theEObject;
        T result = caseProcedureAttribs(procedureAttribs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_DEF:
      {
        ComponentDef componentDef = (ComponentDef)theEObject;
        T result = caseComponentDef(componentDef);
        if (result == null) result = caseReferencedType(componentDef);
        if (result == null) result = caseTimerVarInstance(componentDef);
        if (result == null) result = caseTypeReferenceTailType(componentDef);
        if (result == null) result = caseRefValue(componentDef);
        if (result == null) result = caseTTCN3Reference(componentDef);
        if (result == null) result = caseRefValueHead(componentDef);
        if (result == null) result = caseRefValueElement(componentDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_DEF_LIST:
      {
        ComponentDefList componentDefList = (ComponentDefList)theEObject;
        T result = caseComponentDefList(componentDefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_INSTANCE:
      {
        PortInstance portInstance = (PortInstance)theEObject;
        T result = casePortInstance(portInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PORT_ELEMENT:
      {
        PortElement portElement = (PortElement)theEObject;
        T result = casePortElement(portElement);
        if (result == null) result = caseFormalPortAndValuePar(portElement);
        if (result == null) result = caseRefValue(portElement);
        if (result == null) result = caseRefValueHead(portElement);
        if (result == null) result = caseRefValueElement(portElement);
        if (result == null) result = caseTTCN3Reference(portElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPONENT_ELEMENT_DEF:
      {
        ComponentElementDef componentElementDef = (ComponentElementDef)theEObject;
        T result = caseComponentElementDef(componentElementDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PROCEDURE_LIST:
      {
        ProcedureList procedureList = (ProcedureList)theEObject;
        T result = caseProcedureList(procedureList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_OR_SIGNATURE_LIST:
      {
        AllOrSignatureList allOrSignatureList = (AllOrSignatureList)theEObject;
        T result = caseAllOrSignatureList(allOrSignatureList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SIGNATURE_LIST:
      {
        SignatureList signatureList = (SignatureList)theEObject;
        T result = caseSignatureList(signatureList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ENUM_DEF:
      {
        EnumDef enumDef = (EnumDef)theEObject;
        T result = caseEnumDef(enumDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ENUM_DEF_NAMED:
      {
        EnumDefNamed enumDefNamed = (EnumDefNamed)theEObject;
        T result = caseEnumDefNamed(enumDefNamed);
        if (result == null) result = caseReferencedType(enumDefNamed);
        if (result == null) result = caseEnumDef(enumDefNamed);
        if (result == null) result = caseTypeReferenceTailType(enumDefNamed);
        if (result == null) result = caseTTCN3Reference(enumDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_TYPE_DEF:
      {
        NestedTypeDef nestedTypeDef = (NestedTypeDef)theEObject;
        T result = caseNestedTypeDef(nestedTypeDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_RECORD_DEF:
      {
        NestedRecordDef nestedRecordDef = (NestedRecordDef)theEObject;
        T result = caseNestedRecordDef(nestedRecordDef);
        if (result == null) result = caseNestedTypeDef(nestedRecordDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_UNION_DEF:
      {
        NestedUnionDef nestedUnionDef = (NestedUnionDef)theEObject;
        T result = caseNestedUnionDef(nestedUnionDef);
        if (result == null) result = caseNestedTypeDef(nestedUnionDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_SET_DEF:
      {
        NestedSetDef nestedSetDef = (NestedSetDef)theEObject;
        T result = caseNestedSetDef(nestedSetDef);
        if (result == null) result = caseNestedTypeDef(nestedSetDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_RECORD_OF_DEF:
      {
        NestedRecordOfDef nestedRecordOfDef = (NestedRecordOfDef)theEObject;
        T result = caseNestedRecordOfDef(nestedRecordOfDef);
        if (result == null) result = caseNestedTypeDef(nestedRecordOfDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_SET_OF_DEF:
      {
        NestedSetOfDef nestedSetOfDef = (NestedSetOfDef)theEObject;
        T result = caseNestedSetOfDef(nestedSetOfDef);
        if (result == null) result = caseNestedTypeDef(nestedSetOfDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NESTED_ENUM_DEF:
      {
        NestedEnumDef nestedEnumDef = (NestedEnumDef)theEObject;
        T result = caseNestedEnumDef(nestedEnumDef);
        if (result == null) result = caseNestedTypeDef(nestedEnumDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MESSAGE_LIST:
      {
        MessageList messageList = (MessageList)theEObject;
        T result = caseMessageList(messageList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_OR_TYPE_LIST:
      {
        AllOrTypeList allOrTypeList = (AllOrTypeList)theEObject;
        T result = caseAllOrTypeList(allOrTypeList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TYPE_LIST:
      {
        TypeList typeList = (TypeList)theEObject;
        T result = caseTypeList(typeList);
        if (result == null) result = caseExceptionSpec(typeList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.UNION_DEF:
      {
        UnionDef unionDef = (UnionDef)theEObject;
        T result = caseUnionDef(unionDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.UNION_DEF_NAMED:
      {
        UnionDefNamed unionDefNamed = (UnionDefNamed)theEObject;
        T result = caseUnionDefNamed(unionDefNamed);
        if (result == null) result = caseReferencedType(unionDefNamed);
        if (result == null) result = caseUnionDef(unionDefNamed);
        if (result == null) result = caseTypeReferenceTailType(unionDefNamed);
        if (result == null) result = caseTTCN3Reference(unionDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.UNION_DEF_BODY:
      {
        UnionDefBody unionDefBody = (UnionDefBody)theEObject;
        T result = caseUnionDefBody(unionDefBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ENUMERATION_LIST:
      {
        EnumerationList enumerationList = (EnumerationList)theEObject;
        T result = caseEnumerationList(enumerationList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ENUMERATION:
      {
        Enumeration enumeration = (Enumeration)theEObject;
        T result = caseEnumeration(enumeration);
        if (result == null) result = caseFieldReference(enumeration);
        if (result == null) result = caseTypeReferenceTailType(enumeration);
        if (result == null) result = caseRefValue(enumeration);
        if (result == null) result = caseRefValueElement(enumeration);
        if (result == null) result = caseRefValueHead(enumeration);
        if (result == null) result = caseTTCN3Reference(enumeration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.UNION_FIELD_DEF:
      {
        UnionFieldDef unionFieldDef = (UnionFieldDef)theEObject;
        T result = caseUnionFieldDef(unionFieldDef);
        if (result == null) result = caseFieldReference(unionFieldDef);
        if (result == null) result = caseTypeReferenceTailType(unionFieldDef);
        if (result == null) result = caseRefValue(unionFieldDef);
        if (result == null) result = caseRefValueElement(unionFieldDef);
        if (result == null) result = caseRefValueHead(unionFieldDef);
        if (result == null) result = caseTTCN3Reference(unionFieldDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SET_DEF:
      {
        SetDef setDef = (SetDef)theEObject;
        T result = caseSetDef(setDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SET_DEF_NAMED:
      {
        SetDefNamed setDefNamed = (SetDefNamed)theEObject;
        T result = caseSetDefNamed(setDefNamed);
        if (result == null) result = caseReferencedType(setDefNamed);
        if (result == null) result = caseSetDef(setDefNamed);
        if (result == null) result = caseTypeReferenceTailType(setDefNamed);
        if (result == null) result = caseTTCN3Reference(setDefNamed);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SUB_TYPE_SPEC:
      {
        SubTypeSpec subTypeSpec = (SubTypeSpec)theEObject;
        T result = caseSubTypeSpec(subTypeSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALLOWED_VALUES_SPEC:
      {
        AllowedValuesSpec allowedValuesSpec = (AllowedValuesSpec)theEObject;
        T result = caseAllowedValuesSpec(allowedValuesSpec);
        if (result == null) result = caseSubTypeSpec(allowedValuesSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_OR_RANGE:
      {
        TemplateOrRange templateOrRange = (TemplateOrRange)theEObject;
        T result = caseTemplateOrRange(templateOrRange);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RANGE_DEF:
      {
        RangeDef rangeDef = (RangeDef)theEObject;
        T result = caseRangeDef(rangeDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BOUND:
      {
        Bound bound = (Bound)theEObject;
        T result = caseBound(bound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.STRING_LENGTH:
      {
        StringLength stringLength = (StringLength)theEObject;
        T result = caseStringLength(stringLength);
        if (result == null) result = caseExtraMatchingAttributes(stringLength);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CHAR_STRING_MATCH:
      {
        CharStringMatch charStringMatch = (CharStringMatch)theEObject;
        T result = caseCharStringMatch(charStringMatch);
        if (result == null) result = caseAllowedValuesSpec(charStringMatch);
        if (result == null) result = caseSubTypeSpec(charStringMatch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PATTERN_PARTICLE:
      {
        PatternParticle patternParticle = (PatternParticle)theEObject;
        T result = casePatternParticle(patternParticle);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_DEF:
      {
        TemplateDef templateDef = (TemplateDef)theEObject;
        T result = caseTemplateDef(templateDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DERIVED_DEF:
      {
        DerivedDef derivedDef = (DerivedDef)theEObject;
        T result = caseDerivedDef(derivedDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BASE_TEMPLATE:
      {
        BaseTemplate baseTemplate = (BaseTemplate)theEObject;
        T result = caseBaseTemplate(baseTemplate);
        if (result == null) result = caseFunctionRef(baseTemplate);
        if (result == null) result = caseRefValue(baseTemplate);
        if (result == null) result = caseRefValueHead(baseTemplate);
        if (result == null) result = caseRefValueElement(baseTemplate);
        if (result == null) result = caseTTCN3Reference(baseTemplate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMP_VAR_LIST:
      {
        TempVarList tempVarList = (TempVarList)theEObject;
        T result = caseTempVarList(tempVarList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_VAR_INSTANCE:
      {
        TimerVarInstance timerVarInstance = (TimerVarInstance)theEObject;
        T result = caseTimerVarInstance(timerVarInstance);
        if (result == null) result = caseRefValue(timerVarInstance);
        if (result == null) result = caseRefValueHead(timerVarInstance);
        if (result == null) result = caseRefValueElement(timerVarInstance);
        if (result == null) result = caseTTCN3Reference(timerVarInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_VAR_INSTANCE:
      {
        SingleVarInstance singleVarInstance = (SingleVarInstance)theEObject;
        T result = caseSingleVarInstance(singleVarInstance);
        if (result == null) result = caseTimerVarInstance(singleVarInstance);
        if (result == null) result = caseRefValue(singleVarInstance);
        if (result == null) result = caseRefValueHead(singleVarInstance);
        if (result == null) result = caseRefValueElement(singleVarInstance);
        if (result == null) result = caseTTCN3Reference(singleVarInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE:
      {
        SingleTempVarInstance singleTempVarInstance = (SingleTempVarInstance)theEObject;
        T result = caseSingleTempVarInstance(singleTempVarInstance);
        if (result == null) result = caseRefValue(singleTempVarInstance);
        if (result == null) result = caseRefValueHead(singleTempVarInstance);
        if (result == null) result = caseRefValueElement(singleTempVarInstance);
        if (result == null) result = caseTTCN3Reference(singleTempVarInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IN_LINE_TEMPLATE:
      {
        InLineTemplate inLineTemplate = (InLineTemplate)theEObject;
        T result = caseInLineTemplate(inLineTemplate);
        if (result == null) result = caseTemplateInstanceActualPar(inLineTemplate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DERIVED_REF_WITH_PAR_LIST:
      {
        DerivedRefWithParList derivedRefWithParList = (DerivedRefWithParList)theEObject;
        T result = caseDerivedRefWithParList(derivedRefWithParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_BODY:
      {
        TemplateBody templateBody = (TemplateBody)theEObject;
        T result = caseTemplateBody(templateBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXTRA_MATCHING_ATTRIBUTES:
      {
        ExtraMatchingAttributes extraMatchingAttributes = (ExtraMatchingAttributes)theEObject;
        T result = caseExtraMatchingAttributes(extraMatchingAttributes);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_SPEC_LIST:
      {
        FieldSpecList fieldSpecList = (FieldSpecList)theEObject;
        T result = caseFieldSpecList(fieldSpecList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_SPEC:
      {
        FieldSpec fieldSpec = (FieldSpec)theEObject;
        T result = caseFieldSpec(fieldSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SIMPLE_SPEC:
      {
        SimpleSpec simpleSpec = (SimpleSpec)theEObject;
        T result = caseSimpleSpec(simpleSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SIMPLE_TEMPLATE_SPEC:
      {
        SimpleTemplateSpec simpleTemplateSpec = (SimpleTemplateSpec)theEObject;
        T result = caseSimpleTemplateSpec(simpleTemplateSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION:
      {
        SingleTemplateExpression singleTemplateExpression = (SingleTemplateExpression)theEObject;
        T result = caseSingleTemplateExpression(singleTemplateExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_REF_WITH_PAR_LIST:
      {
        TemplateRefWithParList templateRefWithParList = (TemplateRefWithParList)theEObject;
        T result = caseTemplateRefWithParList(templateRefWithParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST:
      {
        TemplateActualParList templateActualParList = (TemplateActualParList)theEObject;
        T result = caseTemplateActualParList(templateActualParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MATCHING_SYMBOL:
      {
        MatchingSymbol matchingSymbol = (MatchingSymbol)theEObject;
        T result = caseMatchingSymbol(matchingSymbol);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SUBSET_MATCH:
      {
        SubsetMatch subsetMatch = (SubsetMatch)theEObject;
        T result = caseSubsetMatch(subsetMatch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SUPERSET_MATCH:
      {
        SupersetMatch supersetMatch = (SupersetMatch)theEObject;
        T result = caseSupersetMatch(supersetMatch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RANGE:
      {
        Range range = (Range)theEObject;
        T result = caseRange(range);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.WILDCARD_LENGTH_MATCH:
      {
        WildcardLengthMatch wildcardLengthMatch = (WildcardLengthMatch)theEObject;
        T result = caseWildcardLengthMatch(wildcardLengthMatch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPLEMENT:
      {
        Complement complement = (Complement)theEObject;
        T result = caseComplement(complement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.LIST_OF_TEMPLATES:
      {
        ListOfTemplates listOfTemplates = (ListOfTemplates)theEObject;
        T result = caseListOfTemplates(listOfTemplates);
        if (result == null) result = caseSubsetMatch(listOfTemplates);
        if (result == null) result = caseSupersetMatch(listOfTemplates);
        if (result == null) result = caseComplement(listOfTemplates);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_OPS:
      {
        TemplateOps templateOps = (TemplateOps)theEObject;
        T result = caseTemplateOps(templateOps);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MATCH_OP:
      {
        MatchOp matchOp = (MatchOp)theEObject;
        T result = caseMatchOp(matchOp);
        if (result == null) result = caseTemplateOps(matchOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VALUEOF_OP:
      {
        ValueofOp valueofOp = (ValueofOp)theEObject;
        T result = caseValueofOp(valueofOp);
        if (result == null) result = caseTemplateOps(valueofOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONFIGURATION_OPS:
      {
        ConfigurationOps configurationOps = (ConfigurationOps)theEObject;
        T result = caseConfigurationOps(configurationOps);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CREATE_OP:
      {
        CreateOp createOp = (CreateOp)theEObject;
        T result = caseCreateOp(createOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RUNNING_OP:
      {
        RunningOp runningOp = (RunningOp)theEObject;
        T result = caseRunningOp(runningOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.OP_CALL:
      {
        OpCall opCall = (OpCall)theEObject;
        T result = caseOpCall(opCall);
        if (result == null) result = caseSingleExpression(opCall);
        if (result == null) result = caseWildcardLengthMatch(opCall);
        if (result == null) result = caseExpression(opCall);
        if (result == null) result = caseConstantExpression(opCall);
        if (result == null) result = caseBooleanExpression(opCall);
        if (result == null) result = caseFieldOrBitNumber(opCall);
        if (result == null) result = caseNotUsedOrExpression(opCall);
        if (result == null) result = caseArrayOrBitRef(opCall);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALIVE_OP:
      {
        AliveOp aliveOp = (AliveOp)theEObject;
        T result = caseAliveOp(aliveOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_LIST_ITEM:
      {
        TemplateListItem templateListItem = (TemplateListItem)theEObject;
        T result = caseTemplateListItem(templateListItem);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_ELEMENTS_FROM:
      {
        AllElementsFrom allElementsFrom = (AllElementsFrom)theEObject;
        T result = caseAllElementsFrom(allElementsFrom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SIGNATURE:
      {
        Signature signature = (Signature)theEObject;
        T result = caseSignature(signature);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_INSTANCE_ASSIGNMENT:
      {
        TemplateInstanceAssignment templateInstanceAssignment = (TemplateInstanceAssignment)theEObject;
        T result = caseTemplateInstanceAssignment(templateInstanceAssignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_INSTANCE_ACTUAL_PAR:
      {
        TemplateInstanceActualPar templateInstanceActualPar = (TemplateInstanceActualPar)theEObject;
        T result = caseTemplateInstanceActualPar(templateInstanceActualPar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TEMPLATE_RESTRICTION:
      {
        TemplateRestriction templateRestriction = (TemplateRestriction)theEObject;
        T result = caseTemplateRestriction(templateRestriction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RESTRICTED_TEMPLATE:
      {
        RestrictedTemplate restrictedTemplate = (RestrictedTemplate)theEObject;
        T result = caseRestrictedTemplate(restrictedTemplate);
        if (result == null) result = caseReturnType(restrictedTemplate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.LOG_STATEMENT:
      {
        LogStatement logStatement = (LogStatement)theEObject;
        T result = caseLogStatement(logStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.LOG_ITEM:
      {
        LogItem logItem = (LogItem)theEObject;
        T result = caseLogItem(logItem);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXPRESSION:
      {
        Expression expression = (Expression)theEObject;
        T result = caseExpression(expression);
        if (result == null) result = caseNotUsedOrExpression(expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.WITH_STATEMENT:
      {
        WithStatement withStatement = (WithStatement)theEObject;
        T result = caseWithStatement(withStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.WITH_ATTRIB_LIST:
      {
        WithAttribList withAttribList = (WithAttribList)theEObject;
        T result = caseWithAttribList(withAttribList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MULTI_WITH_ATTRIB:
      {
        MultiWithAttrib multiWithAttrib = (MultiWithAttrib)theEObject;
        T result = caseMultiWithAttrib(multiWithAttrib);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_WITH_ATTRIB:
      {
        SingleWithAttrib singleWithAttrib = (SingleWithAttrib)theEObject;
        T result = caseSingleWithAttrib(singleWithAttrib);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ATTRIB_QUALIFIER:
      {
        AttribQualifier attribQualifier = (AttribQualifier)theEObject;
        T result = caseAttribQualifier(attribQualifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.IDENTIFIER_LIST:
      {
        IdentifierList identifierList = (IdentifierList)theEObject;
        T result = caseIdentifierList(identifierList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.QUALIFIED_IDENTIFIER_LIST:
      {
        QualifiedIdentifierList qualifiedIdentifierList = (QualifiedIdentifierList)theEObject;
        T result = caseQualifiedIdentifierList(qualifiedIdentifierList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_CONST_DEF:
      {
        SingleConstDef singleConstDef = (SingleConstDef)theEObject;
        T result = caseSingleConstDef(singleConstDef);
        if (result == null) result = caseFieldReference(singleConstDef);
        if (result == null) result = caseTypeReferenceTailType(singleConstDef);
        if (result == null) result = caseRefValue(singleConstDef);
        if (result == null) result = caseRefValueElement(singleConstDef);
        if (result == null) result = caseRefValueHead(singleConstDef);
        if (result == null) result = caseTTCN3Reference(singleConstDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPOUND_EXPRESSION:
      {
        CompoundExpression compoundExpression = (CompoundExpression)theEObject;
        T result = caseCompoundExpression(compoundExpression);
        if (result == null) result = caseSingleExpression(compoundExpression);
        if (result == null) result = caseExpression(compoundExpression);
        if (result == null) result = caseNotUsedOrExpression(compoundExpression);
        if (result == null) result = caseWildcardLengthMatch(compoundExpression);
        if (result == null) result = caseConstantExpression(compoundExpression);
        if (result == null) result = caseBooleanExpression(compoundExpression);
        if (result == null) result = caseFieldOrBitNumber(compoundExpression);
        if (result == null) result = caseArrayOrBitRef(compoundExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_EXPRESSION:
      {
        ArrayExpression arrayExpression = (ArrayExpression)theEObject;
        T result = caseArrayExpression(arrayExpression);
        if (result == null) result = caseCompoundExpression(arrayExpression);
        if (result == null) result = caseSingleExpression(arrayExpression);
        if (result == null) result = caseExpression(arrayExpression);
        if (result == null) result = caseNotUsedOrExpression(arrayExpression);
        if (result == null) result = caseWildcardLengthMatch(arrayExpression);
        if (result == null) result = caseConstantExpression(arrayExpression);
        if (result == null) result = caseBooleanExpression(arrayExpression);
        if (result == null) result = caseFieldOrBitNumber(arrayExpression);
        if (result == null) result = caseArrayOrBitRef(arrayExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_EXPRESSION_LIST:
      {
        FieldExpressionList fieldExpressionList = (FieldExpressionList)theEObject;
        T result = caseFieldExpressionList(fieldExpressionList);
        if (result == null) result = caseCompoundExpression(fieldExpressionList);
        if (result == null) result = caseSingleExpression(fieldExpressionList);
        if (result == null) result = caseExpression(fieldExpressionList);
        if (result == null) result = caseNotUsedOrExpression(fieldExpressionList);
        if (result == null) result = caseWildcardLengthMatch(fieldExpressionList);
        if (result == null) result = caseConstantExpression(fieldExpressionList);
        if (result == null) result = caseBooleanExpression(fieldExpressionList);
        if (result == null) result = caseFieldOrBitNumber(fieldExpressionList);
        if (result == null) result = caseArrayOrBitRef(fieldExpressionList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_ELEMENT_EXPRESSION_LIST:
      {
        ArrayElementExpressionList arrayElementExpressionList = (ArrayElementExpressionList)theEObject;
        T result = caseArrayElementExpressionList(arrayElementExpressionList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_EXPRESSION_SPEC:
      {
        FieldExpressionSpec fieldExpressionSpec = (FieldExpressionSpec)theEObject;
        T result = caseFieldExpressionSpec(fieldExpressionSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.NOT_USED_OR_EXPRESSION:
      {
        NotUsedOrExpression notUsedOrExpression = (NotUsedOrExpression)theEObject;
        T result = caseNotUsedOrExpression(notUsedOrExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONSTANT_EXPRESSION:
      {
        ConstantExpression constantExpression = (ConstantExpression)theEObject;
        T result = caseConstantExpression(constantExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.COMPOUND_CONST_EXPRESSION:
      {
        CompoundConstExpression compoundConstExpression = (CompoundConstExpression)theEObject;
        T result = caseCompoundConstExpression(compoundConstExpression);
        if (result == null) result = caseConstantExpression(compoundConstExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_CONST_EXPRESSION_LIST:
      {
        FieldConstExpressionList fieldConstExpressionList = (FieldConstExpressionList)theEObject;
        T result = caseFieldConstExpressionList(fieldConstExpressionList);
        if (result == null) result = caseCompoundConstExpression(fieldConstExpressionList);
        if (result == null) result = caseConstantExpression(fieldConstExpressionList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_CONST_EXPRESSION_SPEC:
      {
        FieldConstExpressionSpec fieldConstExpressionSpec = (FieldConstExpressionSpec)theEObject;
        T result = caseFieldConstExpressionSpec(fieldConstExpressionSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_CONST_EXPRESSION:
      {
        ArrayConstExpression arrayConstExpression = (ArrayConstExpression)theEObject;
        T result = caseArrayConstExpression(arrayConstExpression);
        if (result == null) result = caseCompoundConstExpression(arrayConstExpression);
        if (result == null) result = caseConstantExpression(arrayConstExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_ELEMENT_CONST_EXPRESSION_LIST:
      {
        ArrayElementConstExpressionList arrayElementConstExpressionList = (ArrayElementConstExpressionList)theEObject;
        T result = caseArrayElementConstExpressionList(arrayElementConstExpressionList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONST_LIST:
      {
        ConstList constList = (ConstList)theEObject;
        T result = caseConstList(constList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VALUE:
      {
        Value value = (Value)theEObject;
        T result = caseValue(value);
        if (result == null) result = caseSingleExpression(value);
        if (result == null) result = caseWildcardLengthMatch(value);
        if (result == null) result = caseExpression(value);
        if (result == null) result = caseConstantExpression(value);
        if (result == null) result = caseBooleanExpression(value);
        if (result == null) result = caseFieldOrBitNumber(value);
        if (result == null) result = caseNotUsedOrExpression(value);
        if (result == null) result = caseArrayOrBitRef(value);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REFERENCED_VALUE:
      {
        ReferencedValue referencedValue = (ReferencedValue)theEObject;
        T result = caseReferencedValue(referencedValue);
        if (result == null) result = casePatternParticle(referencedValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REF_VALUE_HEAD:
      {
        RefValueHead refValueHead = (RefValueHead)theEObject;
        T result = caseRefValueHead(refValueHead);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REF_VALUE_ELEMENT:
      {
        RefValueElement refValueElement = (RefValueElement)theEObject;
        T result = caseRefValueElement(refValueElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.HEAD:
      {
        Head head = (Head)theEObject;
        T result = caseHead(head);
        if (result == null) result = caseSpecElement(head);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REF_VALUE_TAIL:
      {
        RefValueTail refValueTail = (RefValueTail)theEObject;
        T result = caseRefValueTail(refValueTail);
        if (result == null) result = caseSpecElement(refValueTail);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SPEC_ELEMENT:
      {
        SpecElement specElement = (SpecElement)theEObject;
        T result = caseSpecElement(specElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EXTENDED_FIELD_REFERENCE:
      {
        ExtendedFieldReference extendedFieldReference = (ExtendedFieldReference)theEObject;
        T result = caseExtendedFieldReference(extendedFieldReference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REF_VALUE:
      {
        RefValue refValue = (RefValue)theEObject;
        T result = caseRefValue(refValue);
        if (result == null) result = caseRefValueHead(refValue);
        if (result == null) result = caseRefValueElement(refValue);
        if (result == null) result = caseTTCN3Reference(refValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PREDEFINED_VALUE:
      {
        PredefinedValue predefinedValue = (PredefinedValue)theEObject;
        T result = casePredefinedValue(predefinedValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BOOLEAN_EXPRESSION:
      {
        BooleanExpression booleanExpression = (BooleanExpression)theEObject;
        T result = caseBooleanExpression(booleanExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SINGLE_EXPRESSION:
      {
        SingleExpression singleExpression = (SingleExpression)theEObject;
        T result = caseSingleExpression(singleExpression);
        if (result == null) result = caseWildcardLengthMatch(singleExpression);
        if (result == null) result = caseExpression(singleExpression);
        if (result == null) result = caseConstantExpression(singleExpression);
        if (result == null) result = caseBooleanExpression(singleExpression);
        if (result == null) result = caseFieldOrBitNumber(singleExpression);
        if (result == null) result = caseNotUsedOrExpression(singleExpression);
        if (result == null) result = caseArrayOrBitRef(singleExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DEF_OR_FIELD_REF_LIST:
      {
        DefOrFieldRefList defOrFieldRefList = (DefOrFieldRefList)theEObject;
        T result = caseDefOrFieldRefList(defOrFieldRefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DEF_OR_FIELD_REF:
      {
        DefOrFieldRef defOrFieldRef = (DefOrFieldRef)theEObject;
        T result = caseDefOrFieldRef(defOrFieldRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ALL_REF:
      {
        AllRef allRef = (AllRef)theEObject;
        T result = caseAllRef(allRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.GROUP_REF_LIST:
      {
        GroupRefList groupRefList = (GroupRefList)theEObject;
        T result = caseGroupRefList(groupRefList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TTCN3_REFERENCE:
      {
        TTCN3Reference ttcn3Reference = (TTCN3Reference)theEObject;
        T result = caseTTCN3Reference(ttcn3Reference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TTCN3_REFERENCE_LIST:
      {
        TTCN3ReferenceList ttcn3ReferenceList = (TTCN3ReferenceList)theEObject;
        T result = caseTTCN3ReferenceList(ttcn3ReferenceList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_REFERENCE:
      {
        FieldReference fieldReference = (FieldReference)theEObject;
        T result = caseFieldReference(fieldReference);
        if (result == null) result = caseTypeReferenceTailType(fieldReference);
        if (result == null) result = caseRefValue(fieldReference);
        if (result == null) result = caseRefValueElement(fieldReference);
        if (result == null) result = caseRefValueHead(fieldReference);
        if (result == null) result = caseTTCN3Reference(fieldReference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_OR_BIT_REF:
      {
        ArrayOrBitRef arrayOrBitRef = (ArrayOrBitRef)theEObject;
        T result = caseArrayOrBitRef(arrayOrBitRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FIELD_OR_BIT_NUMBER:
      {
        FieldOrBitNumber fieldOrBitNumber = (FieldOrBitNumber)theEObject;
        T result = caseFieldOrBitNumber(fieldOrBitNumber);
        if (result == null) result = caseArrayOrBitRef(fieldOrBitNumber);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_VALUE_OR_ATTRIB:
      {
        ArrayValueOrAttrib arrayValueOrAttrib = (ArrayValueOrAttrib)theEObject;
        T result = caseArrayValueOrAttrib(arrayValueOrAttrib);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_ELEMENT_SPEC_LIST:
      {
        ArrayElementSpecList arrayElementSpecList = (ArrayElementSpecList)theEObject;
        T result = caseArrayElementSpecList(arrayElementSpecList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ARRAY_ELEMENT_SPEC:
      {
        ArrayElementSpec arrayElementSpec = (ArrayElementSpec)theEObject;
        T result = caseArrayElementSpec(arrayElementSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.TIMER_OPS:
      {
        TimerOps timerOps = (TimerOps)theEObject;
        T result = caseTimerOps(timerOps);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RUNNING_TIMER_OP:
      {
        RunningTimerOp runningTimerOp = (RunningTimerOp)theEObject;
        T result = caseRunningTimerOp(runningTimerOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.READ_TIMER_OP:
      {
        ReadTimerOp readTimerOp = (ReadTimerOp)theEObject;
        T result = caseReadTimerOp(readTimerOp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PERMUTATION_MATCH:
      {
        PermutationMatch permutationMatch = (PermutationMatch)theEObject;
        T result = casePermutationMatch(permutationMatch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.LOOP_CONSTRUCT:
      {
        LoopConstruct loopConstruct = (LoopConstruct)theEObject;
        T result = caseLoopConstruct(loopConstruct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOR_STATEMENT:
      {
        ForStatement forStatement = (ForStatement)theEObject;
        T result = caseForStatement(forStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.WHILE_STATEMENT:
      {
        WhileStatement whileStatement = (WhileStatement)theEObject;
        T result = caseWhileStatement(whileStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.DO_WHILE_STATEMENT:
      {
        DoWhileStatement doWhileStatement = (DoWhileStatement)theEObject;
        T result = caseDoWhileStatement(doWhileStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.CONDITIONAL_CONSTRUCT:
      {
        ConditionalConstruct conditionalConstruct = (ConditionalConstruct)theEObject;
        T result = caseConditionalConstruct(conditionalConstruct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ELSE_IF_CLAUSE:
      {
        ElseIfClause elseIfClause = (ElseIfClause)theEObject;
        T result = caseElseIfClause(elseIfClause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ELSE_CLAUSE:
      {
        ElseClause elseClause = (ElseClause)theEObject;
        T result = caseElseClause(elseClause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.INITIAL:
      {
        Initial initial = (Initial)theEObject;
        T result = caseInitial(initial);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SELECT_CASE_CONSTRUCT:
      {
        SelectCaseConstruct selectCaseConstruct = (SelectCaseConstruct)theEObject;
        T result = caseSelectCaseConstruct(selectCaseConstruct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SELECT_CASE_BODY:
      {
        SelectCaseBody selectCaseBody = (SelectCaseBody)theEObject;
        T result = caseSelectCaseBody(selectCaseBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SELECT_CASE:
      {
        SelectCase selectCase = (SelectCase)theEObject;
        T result = caseSelectCase(selectCase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_DEF:
      {
        FunctionDef functionDef = (FunctionDef)theEObject;
        T result = caseFunctionDef(functionDef);
        if (result == null) result = caseFunctionRef(functionDef);
        if (result == null) result = caseRefValue(functionDef);
        if (result == null) result = caseRefValueHead(functionDef);
        if (result == null) result = caseRefValueElement(functionDef);
        if (result == null) result = caseTTCN3Reference(functionDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MTC_SPEC:
      {
        MtcSpec mtcSpec = (MtcSpec)theEObject;
        T result = caseMtcSpec(mtcSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_FORMAL_PAR_LIST:
      {
        FunctionFormalParList functionFormalParList = (FunctionFormalParList)theEObject;
        T result = caseFunctionFormalParList(functionFormalParList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNCTION_FORMAL_PAR:
      {
        FunctionFormalPar functionFormalPar = (FunctionFormalPar)theEObject;
        T result = caseFunctionFormalPar(functionFormalPar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FORMAL_VALUE_PAR:
      {
        FormalValuePar formalValuePar = (FormalValuePar)theEObject;
        T result = caseFormalValuePar(formalValuePar);
        if (result == null) result = caseFormalPortAndValuePar(formalValuePar);
        if (result == null) result = caseFieldReference(formalValuePar);
        if (result == null) result = caseTypeReferenceTailType(formalValuePar);
        if (result == null) result = caseRefValue(formalValuePar);
        if (result == null) result = caseRefValueElement(formalValuePar);
        if (result == null) result = caseRefValueHead(formalValuePar);
        if (result == null) result = caseTTCN3Reference(formalValuePar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FORMAL_TIMER_PAR:
      {
        FormalTimerPar formalTimerPar = (FormalTimerPar)theEObject;
        T result = caseFormalTimerPar(formalTimerPar);
        if (result == null) result = caseTimerVarInstance(formalTimerPar);
        if (result == null) result = caseRefValue(formalTimerPar);
        if (result == null) result = caseRefValueHead(formalTimerPar);
        if (result == null) result = caseRefValueElement(formalTimerPar);
        if (result == null) result = caseTTCN3Reference(formalTimerPar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FORMAL_PORT_PAR:
      {
        FormalPortPar formalPortPar = (FormalPortPar)theEObject;
        T result = caseFormalPortPar(formalPortPar);
        if (result == null) result = caseFormalPortAndValuePar(formalPortPar);
        if (result == null) result = caseRefValue(formalPortPar);
        if (result == null) result = caseRefValueHead(formalPortPar);
        if (result == null) result = caseRefValueElement(formalPortPar);
        if (result == null) result = caseTTCN3Reference(formalPortPar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FORMAL_TEMPLATE_PAR:
      {
        FormalTemplatePar formalTemplatePar = (FormalTemplatePar)theEObject;
        T result = caseFormalTemplatePar(formalTemplatePar);
        if (result == null) result = caseRefValue(formalTemplatePar);
        if (result == null) result = caseRefValueHead(formalTemplatePar);
        if (result == null) result = caseRefValueElement(formalTemplatePar);
        if (result == null) result = caseTTCN3Reference(formalTemplatePar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RUNS_ON_SPEC:
      {
        RunsOnSpec runsOnSpec = (RunsOnSpec)theEObject;
        T result = caseRunsOnSpec(runsOnSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.RETURN_TYPE:
      {
        ReturnType returnType = (ReturnType)theEObject;
        T result = caseReturnType(returnType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ASSIGNMENT:
      {
        Assignment assignment = (Assignment)theEObject;
        T result = caseAssignment(assignment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.VARIABLE_REF:
      {
        VariableRef variableRef = (VariableRef)theEObject;
        T result = caseVariableRef(variableRef);
        if (result == null) result = caseVariableEntry(variableRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.PRE_DEF_FUNCTION:
      {
        PreDefFunction preDefFunction = (PreDefFunction)theEObject;
        T result = casePreDefFunction(preDefFunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2CHAR:
      {
        Fint2char fint2char = (Fint2char)theEObject;
        T result = caseFint2char(fint2char);
        if (result == null) result = casePreDefFunction(fint2char);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2UNICHAR:
      {
        Fint2unichar fint2unichar = (Fint2unichar)theEObject;
        T result = caseFint2unichar(fint2unichar);
        if (result == null) result = casePreDefFunction(fint2unichar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2BIT:
      {
        Fint2bit fint2bit = (Fint2bit)theEObject;
        T result = caseFint2bit(fint2bit);
        if (result == null) result = casePreDefFunction(fint2bit);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2ENUM:
      {
        Fint2enum fint2enum = (Fint2enum)theEObject;
        T result = caseFint2enum(fint2enum);
        if (result == null) result = casePreDefFunction(fint2enum);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2HEX:
      {
        Fint2hex fint2hex = (Fint2hex)theEObject;
        T result = caseFint2hex(fint2hex);
        if (result == null) result = casePreDefFunction(fint2hex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2OCT:
      {
        Fint2oct fint2oct = (Fint2oct)theEObject;
        T result = caseFint2oct(fint2oct);
        if (result == null) result = casePreDefFunction(fint2oct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2STR:
      {
        Fint2str fint2str = (Fint2str)theEObject;
        T result = caseFint2str(fint2str);
        if (result == null) result = casePreDefFunction(fint2str);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FINT2FLOAT:
      {
        Fint2float fint2float = (Fint2float)theEObject;
        T result = caseFint2float(fint2float);
        if (result == null) result = casePreDefFunction(fint2float);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FFLOAT2INT:
      {
        Ffloat2int ffloat2int = (Ffloat2int)theEObject;
        T result = caseFfloat2int(ffloat2int);
        if (result == null) result = casePreDefFunction(ffloat2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FCHAR2INT:
      {
        Fchar2int fchar2int = (Fchar2int)theEObject;
        T result = caseFchar2int(fchar2int);
        if (result == null) result = casePreDefFunction(fchar2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FCHAR2OCT:
      {
        Fchar2oct fchar2oct = (Fchar2oct)theEObject;
        T result = caseFchar2oct(fchar2oct);
        if (result == null) result = casePreDefFunction(fchar2oct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNICHAR2INT:
      {
        Funichar2int funichar2int = (Funichar2int)theEObject;
        T result = caseFunichar2int(funichar2int);
        if (result == null) result = casePreDefFunction(funichar2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FUNICHAR2OCT:
      {
        Funichar2oct funichar2oct = (Funichar2oct)theEObject;
        T result = caseFunichar2oct(funichar2oct);
        if (result == null) result = casePreDefFunction(funichar2oct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FBIT2INT:
      {
        Fbit2int fbit2int = (Fbit2int)theEObject;
        T result = caseFbit2int(fbit2int);
        if (result == null) result = casePreDefFunction(fbit2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FBIT2HEX:
      {
        Fbit2hex fbit2hex = (Fbit2hex)theEObject;
        T result = caseFbit2hex(fbit2hex);
        if (result == null) result = casePreDefFunction(fbit2hex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FBIT2OCT:
      {
        Fbit2oct fbit2oct = (Fbit2oct)theEObject;
        T result = caseFbit2oct(fbit2oct);
        if (result == null) result = casePreDefFunction(fbit2oct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FBIT2STR:
      {
        Fbit2str fbit2str = (Fbit2str)theEObject;
        T result = caseFbit2str(fbit2str);
        if (result == null) result = casePreDefFunction(fbit2str);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FHEX2INT:
      {
        Fhex2int fhex2int = (Fhex2int)theEObject;
        T result = caseFhex2int(fhex2int);
        if (result == null) result = casePreDefFunction(fhex2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FHEX2BIT:
      {
        Fhex2bit fhex2bit = (Fhex2bit)theEObject;
        T result = caseFhex2bit(fhex2bit);
        if (result == null) result = casePreDefFunction(fhex2bit);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FHEX2OCT:
      {
        Fhex2oct fhex2oct = (Fhex2oct)theEObject;
        T result = caseFhex2oct(fhex2oct);
        if (result == null) result = casePreDefFunction(fhex2oct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FHEX2STR:
      {
        Fhex2str fhex2str = (Fhex2str)theEObject;
        T result = caseFhex2str(fhex2str);
        if (result == null) result = casePreDefFunction(fhex2str);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOCT2INT:
      {
        Foct2int foct2int = (Foct2int)theEObject;
        T result = caseFoct2int(foct2int);
        if (result == null) result = casePreDefFunction(foct2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOCT2BIT:
      {
        Foct2bit foct2bit = (Foct2bit)theEObject;
        T result = caseFoct2bit(foct2bit);
        if (result == null) result = casePreDefFunction(foct2bit);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOCT2HEX:
      {
        Foct2hex foct2hex = (Foct2hex)theEObject;
        T result = caseFoct2hex(foct2hex);
        if (result == null) result = casePreDefFunction(foct2hex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOCT2STR:
      {
        Foct2str foct2str = (Foct2str)theEObject;
        T result = caseFoct2str(foct2str);
        if (result == null) result = casePreDefFunction(foct2str);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOCT2CHAR:
      {
        Foct2char foct2char = (Foct2char)theEObject;
        T result = caseFoct2char(foct2char);
        if (result == null) result = casePreDefFunction(foct2char);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FOCT2UNICHAR:
      {
        Foct2unichar foct2unichar = (Foct2unichar)theEObject;
        T result = caseFoct2unichar(foct2unichar);
        if (result == null) result = casePreDefFunction(foct2unichar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FSTR2INT:
      {
        Fstr2int fstr2int = (Fstr2int)theEObject;
        T result = caseFstr2int(fstr2int);
        if (result == null) result = casePreDefFunction(fstr2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FSTR2HEX:
      {
        Fstr2hex fstr2hex = (Fstr2hex)theEObject;
        T result = caseFstr2hex(fstr2hex);
        if (result == null) result = casePreDefFunction(fstr2hex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FSTR2OCT:
      {
        Fstr2oct fstr2oct = (Fstr2oct)theEObject;
        T result = caseFstr2oct(fstr2oct);
        if (result == null) result = casePreDefFunction(fstr2oct);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FSTR2FLOAT:
      {
        Fstr2float fstr2float = (Fstr2float)theEObject;
        T result = caseFstr2float(fstr2float);
        if (result == null) result = casePreDefFunction(fstr2float);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FENUM2INT:
      {
        Fenum2int fenum2int = (Fenum2int)theEObject;
        T result = caseFenum2int(fenum2int);
        if (result == null) result = casePreDefFunction(fenum2int);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FLENGTHOF:
      {
        Flengthof flengthof = (Flengthof)theEObject;
        T result = caseFlengthof(flengthof);
        if (result == null) result = casePreDefFunction(flengthof);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FSIZEOF:
      {
        Fsizeof fsizeof = (Fsizeof)theEObject;
        T result = caseFsizeof(fsizeof);
        if (result == null) result = casePreDefFunction(fsizeof);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FISPRESENT:
      {
        Fispresent fispresent = (Fispresent)theEObject;
        T result = caseFispresent(fispresent);
        if (result == null) result = casePreDefFunction(fispresent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FISCHOSEN:
      {
        Fischosen fischosen = (Fischosen)theEObject;
        T result = caseFischosen(fischosen);
        if (result == null) result = casePreDefFunction(fischosen);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FISVALUE:
      {
        Fisvalue fisvalue = (Fisvalue)theEObject;
        T result = caseFisvalue(fisvalue);
        if (result == null) result = casePreDefFunction(fisvalue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FISBOUND:
      {
        Fisbound fisbound = (Fisbound)theEObject;
        T result = caseFisbound(fisbound);
        if (result == null) result = casePreDefFunction(fisbound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FREGEXP:
      {
        Fregexp fregexp = (Fregexp)theEObject;
        T result = caseFregexp(fregexp);
        if (result == null) result = casePreDefFunction(fregexp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FSUBSTR:
      {
        Fsubstr fsubstr = (Fsubstr)theEObject;
        T result = caseFsubstr(fsubstr);
        if (result == null) result = casePreDefFunction(fsubstr);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FREPLACE:
      {
        Freplace freplace = (Freplace)theEObject;
        T result = caseFreplace(freplace);
        if (result == null) result = casePreDefFunction(freplace);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FENCVALUE:
      {
        Fencvalue fencvalue = (Fencvalue)theEObject;
        T result = caseFencvalue(fencvalue);
        if (result == null) result = casePreDefFunction(fencvalue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FDECVALUE:
      {
        Fdecvalue fdecvalue = (Fdecvalue)theEObject;
        T result = caseFdecvalue(fdecvalue);
        if (result == null) result = casePreDefFunction(fdecvalue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FENCVALUE_UNICHAR:
      {
        FencvalueUnichar fencvalueUnichar = (FencvalueUnichar)theEObject;
        T result = caseFencvalueUnichar(fencvalueUnichar);
        if (result == null) result = casePreDefFunction(fencvalueUnichar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FDECVALUE_UNICHAR:
      {
        FdecvalueUnichar fdecvalueUnichar = (FdecvalueUnichar)theEObject;
        T result = caseFdecvalueUnichar(fdecvalueUnichar);
        if (result == null) result = casePreDefFunction(fdecvalueUnichar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FRND:
      {
        Frnd frnd = (Frnd)theEObject;
        T result = caseFrnd(frnd);
        if (result == null) result = casePreDefFunction(frnd);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.FTESTCASENAME:
      {
        Ftestcasename ftestcasename = (Ftestcasename)theEObject;
        T result = caseFtestcasename(ftestcasename);
        if (result == null) result = casePreDefFunction(ftestcasename);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.XOR_EXPRESSION:
      {
        XorExpression xorExpression = (XorExpression)theEObject;
        T result = caseXorExpression(xorExpression);
        if (result == null) result = caseSingleExpression(xorExpression);
        if (result == null) result = caseWildcardLengthMatch(xorExpression);
        if (result == null) result = caseExpression(xorExpression);
        if (result == null) result = caseConstantExpression(xorExpression);
        if (result == null) result = caseBooleanExpression(xorExpression);
        if (result == null) result = caseFieldOrBitNumber(xorExpression);
        if (result == null) result = caseNotUsedOrExpression(xorExpression);
        if (result == null) result = caseArrayOrBitRef(xorExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.AND_EXPRESSION:
      {
        AndExpression andExpression = (AndExpression)theEObject;
        T result = caseAndExpression(andExpression);
        if (result == null) result = caseSingleExpression(andExpression);
        if (result == null) result = caseWildcardLengthMatch(andExpression);
        if (result == null) result = caseExpression(andExpression);
        if (result == null) result = caseConstantExpression(andExpression);
        if (result == null) result = caseBooleanExpression(andExpression);
        if (result == null) result = caseFieldOrBitNumber(andExpression);
        if (result == null) result = caseNotUsedOrExpression(andExpression);
        if (result == null) result = caseArrayOrBitRef(andExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.EQUAL_EXPRESSION:
      {
        EqualExpression equalExpression = (EqualExpression)theEObject;
        T result = caseEqualExpression(equalExpression);
        if (result == null) result = caseSingleExpression(equalExpression);
        if (result == null) result = caseWildcardLengthMatch(equalExpression);
        if (result == null) result = caseExpression(equalExpression);
        if (result == null) result = caseConstantExpression(equalExpression);
        if (result == null) result = caseBooleanExpression(equalExpression);
        if (result == null) result = caseFieldOrBitNumber(equalExpression);
        if (result == null) result = caseNotUsedOrExpression(equalExpression);
        if (result == null) result = caseArrayOrBitRef(equalExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.REL_EXPRESSION:
      {
        RelExpression relExpression = (RelExpression)theEObject;
        T result = caseRelExpression(relExpression);
        if (result == null) result = caseSingleExpression(relExpression);
        if (result == null) result = caseWildcardLengthMatch(relExpression);
        if (result == null) result = caseExpression(relExpression);
        if (result == null) result = caseConstantExpression(relExpression);
        if (result == null) result = caseBooleanExpression(relExpression);
        if (result == null) result = caseFieldOrBitNumber(relExpression);
        if (result == null) result = caseNotUsedOrExpression(relExpression);
        if (result == null) result = caseArrayOrBitRef(relExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.SHIFT_EXPRESSION:
      {
        ShiftExpression shiftExpression = (ShiftExpression)theEObject;
        T result = caseShiftExpression(shiftExpression);
        if (result == null) result = caseSingleExpression(shiftExpression);
        if (result == null) result = caseWildcardLengthMatch(shiftExpression);
        if (result == null) result = caseExpression(shiftExpression);
        if (result == null) result = caseConstantExpression(shiftExpression);
        if (result == null) result = caseBooleanExpression(shiftExpression);
        if (result == null) result = caseFieldOrBitNumber(shiftExpression);
        if (result == null) result = caseNotUsedOrExpression(shiftExpression);
        if (result == null) result = caseArrayOrBitRef(shiftExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BIT_OR_EXPRESSION:
      {
        BitOrExpression bitOrExpression = (BitOrExpression)theEObject;
        T result = caseBitOrExpression(bitOrExpression);
        if (result == null) result = caseSingleExpression(bitOrExpression);
        if (result == null) result = caseWildcardLengthMatch(bitOrExpression);
        if (result == null) result = caseExpression(bitOrExpression);
        if (result == null) result = caseConstantExpression(bitOrExpression);
        if (result == null) result = caseBooleanExpression(bitOrExpression);
        if (result == null) result = caseFieldOrBitNumber(bitOrExpression);
        if (result == null) result = caseNotUsedOrExpression(bitOrExpression);
        if (result == null) result = caseArrayOrBitRef(bitOrExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BIT_XOR_EXPRESSION:
      {
        BitXorExpression bitXorExpression = (BitXorExpression)theEObject;
        T result = caseBitXorExpression(bitXorExpression);
        if (result == null) result = caseSingleExpression(bitXorExpression);
        if (result == null) result = caseWildcardLengthMatch(bitXorExpression);
        if (result == null) result = caseExpression(bitXorExpression);
        if (result == null) result = caseConstantExpression(bitXorExpression);
        if (result == null) result = caseBooleanExpression(bitXorExpression);
        if (result == null) result = caseFieldOrBitNumber(bitXorExpression);
        if (result == null) result = caseNotUsedOrExpression(bitXorExpression);
        if (result == null) result = caseArrayOrBitRef(bitXorExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.BIT_AND_EXPRESSION:
      {
        BitAndExpression bitAndExpression = (BitAndExpression)theEObject;
        T result = caseBitAndExpression(bitAndExpression);
        if (result == null) result = caseSingleExpression(bitAndExpression);
        if (result == null) result = caseWildcardLengthMatch(bitAndExpression);
        if (result == null) result = caseExpression(bitAndExpression);
        if (result == null) result = caseConstantExpression(bitAndExpression);
        if (result == null) result = caseBooleanExpression(bitAndExpression);
        if (result == null) result = caseFieldOrBitNumber(bitAndExpression);
        if (result == null) result = caseNotUsedOrExpression(bitAndExpression);
        if (result == null) result = caseArrayOrBitRef(bitAndExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.ADD_EXPRESSION:
      {
        AddExpression addExpression = (AddExpression)theEObject;
        T result = caseAddExpression(addExpression);
        if (result == null) result = caseSingleExpression(addExpression);
        if (result == null) result = caseWildcardLengthMatch(addExpression);
        if (result == null) result = caseExpression(addExpression);
        if (result == null) result = caseConstantExpression(addExpression);
        if (result == null) result = caseBooleanExpression(addExpression);
        if (result == null) result = caseFieldOrBitNumber(addExpression);
        if (result == null) result = caseNotUsedOrExpression(addExpression);
        if (result == null) result = caseArrayOrBitRef(addExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case TTCN3Package.MUL_EXPRESSION:
      {
        MulExpression mulExpression = (MulExpression)theEObject;
        T result = caseMulExpression(mulExpression);
        if (result == null) result = caseSingleExpression(mulExpression);
        if (result == null) result = caseWildcardLengthMatch(mulExpression);
        if (result == null) result = caseExpression(mulExpression);
        if (result == null) result = caseConstantExpression(mulExpression);
        if (result == null) result = caseBooleanExpression(mulExpression);
        if (result == null) result = caseFieldOrBitNumber(mulExpression);
        if (result == null) result = caseNotUsedOrExpression(mulExpression);
        if (result == null) result = caseArrayOrBitRef(mulExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>File</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>File</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTTCN3File(TTCN3File object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Const Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Const Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstDef(ConstDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseType(Type object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Reference Tail Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Reference Tail Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeReferenceTailType(TypeReferenceTailType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Reference Tail</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Reference Tail</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeReferenceTail(TypeReferenceTail object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeReference(TypeReference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Spec Type Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Spec Type Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSpecTypeElement(SpecTypeElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayDef(ArrayDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTTCN3Module(TTCN3Module object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Language Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Language Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLanguageSpec(LanguageSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Par Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Par Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleParDef(ModuleParDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModulePar(ModulePar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleParList(ModuleParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleParameter(ModuleParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Multityped Module Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Multityped Module Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMultitypedModuleParList(MultitypedModuleParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Definitions List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Definitions List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleDefinitionsList(ModuleDefinitionsList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Definition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Definition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleDefinition(ModuleDefinition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Friend Module Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Friend Module Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFriendModuleDef(FriendModuleDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Group Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Group Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGroupDef(GroupDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ext Function Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ext Function Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtFunctionDef(ExtFunctionDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ext Const Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ext Const Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtConstDef(ExtConstDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifier Object List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifier Object List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifierObjectList(IdentifierObjectList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamedObject(NamedObject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportDef(ImportDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All With Excepts</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All With Excepts</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllWithExcepts(AllWithExcepts object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Excepts Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Excepts Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptsDef(ExceptsDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptSpec(ExceptSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptElement(ExceptElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifier List Or All</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifier List Or All</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifierListOrAll(IdentifierListOrAll object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Group Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Group Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptGroupSpec(ExceptGroupSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Type Def Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Type Def Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptTypeDefSpec(ExceptTypeDefSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Template Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Template Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptTemplateSpec(ExceptTemplateSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Const Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Const Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptConstSpec(ExceptConstSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Testcase Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Testcase Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptTestcaseSpec(ExceptTestcaseSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Altstep Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Altstep Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptAltstepSpec(ExceptAltstepSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Function Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Function Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptFunctionSpec(ExceptFunctionSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Signature Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Signature Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptSignatureSpec(ExceptSignatureSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Except Module Par Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Except Module Par Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptModuleParSpec(ExceptModuleParSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportSpec(ImportSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportElement(ImportElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Group Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Group Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportGroupSpec(ImportGroupSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifier List Or All With Except</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifier List Or All With Except</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifierListOrAllWithExcept(IdentifierListOrAllWithExcept object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All With Except</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All With Except</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllWithExcept(AllWithExcept object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Type Def Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Type Def Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportTypeDefSpec(ImportTypeDefSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Template Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Template Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportTemplateSpec(ImportTemplateSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Const Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Const Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportConstSpec(ImportConstSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Altstep Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Altstep Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportAltstepSpec(ImportAltstepSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Testcase Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Testcase Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportTestcaseSpec(ImportTestcaseSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Function Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Function Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportFunctionSpec(ImportFunctionSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Signature Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Signature Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportSignatureSpec(ImportSignatureSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Module Par Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Module Par Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportModuleParSpec(ImportModuleParSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Group Ref List With Except</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Group Ref List With Except</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGroupRefListWithExcept(GroupRefListWithExcept object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Qualified Identifier With Except</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Qualified Identifier With Except</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseQualifiedIdentifierWithExcept(QualifiedIdentifierWithExcept object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Groups With Except</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Groups With Except</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllGroupsWithExcept(AllGroupsWithExcept object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Altstep Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Altstep Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltstepDef(AltstepDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Altstep Local Def List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Altstep Local Def List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltstepLocalDefList(AltstepLocalDefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Altstep Local Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Altstep Local Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltstepLocalDef(AltstepLocalDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Testcase Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Testcase Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTestcaseDef(TestcaseDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Or Value Formal Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Or Value Formal Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateOrValueFormalParList(TemplateOrValueFormalParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Or Value Formal Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Or Value Formal Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateOrValueFormalPar(TemplateOrValueFormalPar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Config Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Config Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConfigSpec(ConfigSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>System Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>System Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSystemSpec(SystemSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Signature Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Signature Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSignatureDef(SignatureDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Exception Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Exception Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExceptionSpec(ExceptionSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Signature Formal Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Signature Formal Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSignatureFormalParList(SignatureFormalParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Control Part</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Control Part</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleControlPart(ModuleControlPart object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Control Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Control Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleControlBody(ModuleControlBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Control Statement Or Def List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Control Statement Or Def List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseControlStatementOrDefList(ControlStatementOrDefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Control Statement Or Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Control Statement Or Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseControlStatementOrDef(ControlStatementOrDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Control Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Control Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseControlStatement(ControlStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>SUT Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>SUT Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSUTStatements(SUTStatements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Action Text</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Action Text</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseActionText(ActionText object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Behaviour Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Behaviour Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBehaviourStatements(BehaviourStatements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Activate Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Activate Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseActivateOp(ActivateOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Deactivate Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Deactivate Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeactivateStatement(DeactivateStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Label Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Label Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabelStatement(LabelStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Goto Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Goto Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGotoStatement(GotoStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Return Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Return Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReturnStatement(ReturnStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Interleaved Construct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Interleaved Construct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInterleavedConstruct(InterleavedConstruct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Interleaved Guard List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Interleaved Guard List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInterleavedGuardList(InterleavedGuardList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Interleaved Guard Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Interleaved Guard Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInterleavedGuardElement(InterleavedGuardElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Interleaved Guard</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Interleaved Guard</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInterleavedGuard(InterleavedGuard object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alt Construct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alt Construct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltConstruct(AltConstruct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alt Guard List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alt Guard List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltGuardList(AltGuardList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Else Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Else Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElseStatement(ElseStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Guard Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Guard Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGuardStatement(GuardStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Guard Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Guard Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGuardOp(GuardOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Done Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Done Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDoneStatement(DoneStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Killed Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Killed Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseKilledStatement(KilledStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Or Any</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Or Any</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentOrAny(ComponentOrAny object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Index Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Index Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIndexAssignment(IndexAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Index Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Index Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIndexSpec(IndexSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Get Reply Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Get Reply Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGetReplyStatement(GetReplyStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Check Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Check Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCheckStatement(CheckStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Check Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Check Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortCheckOp(PortCheckOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Check Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Check Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCheckParameter(CheckParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Redirect Present</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Redirect Present</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRedirectPresent(RedirectPresent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>From Clause Present</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>From Clause Present</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFromClausePresent(FromClausePresent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Check Port Ops Present</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Check Port Ops Present</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCheckPortOpsPresent(CheckPortOpsPresent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Get Reply Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Get Reply Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortGetReplyOp(PortGetReplyOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Redirect With Value And Param</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Redirect With Value And Param</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortRedirectWithValueAndParam(PortRedirectWithValueAndParam object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Redirect With Value And Param Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Redirect With Value And Param Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRedirectWithValueAndParamSpec(RedirectWithValueAndParamSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value Match Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value Match Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValueMatchSpec(ValueMatchSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Catch Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Catch Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCatchStatement(CatchStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Catch Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Catch Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortCatchOp(PortCatchOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Catch Op Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Catch Op Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCatchOpParameter(CatchOpParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Get Call Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Get Call Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGetCallStatement(GetCallStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Get Call Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Get Call Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortGetCallOp(PortGetCallOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Redirect With Param</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Redirect With Param</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortRedirectWithParam(PortRedirectWithParam object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Redirect With Param Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Redirect With Param Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRedirectWithParamSpec(RedirectWithParamSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Param Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Param Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParamSpec(ParamSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Param Assignment List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Param Assignment List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParamAssignmentList(ParamAssignmentList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Assignment List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Assignment List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssignmentList(AssignmentList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableAssignment(VariableAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableList(VariableList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Entry</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Entry</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableEntry(VariableEntry object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Trigger Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Trigger Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTriggerStatement(TriggerStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Trigger Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Trigger Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortTriggerOp(PortTriggerOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Receive Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Receive Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReceiveStatement(ReceiveStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Or Any</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Or Any</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortOrAny(PortOrAny object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Receive Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Receive Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortReceiveOp(PortReceiveOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>From Clause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>From Clause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFromClause(FromClause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Address Ref List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Address Ref List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddressRefList(AddressRefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Redirect</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Redirect</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortRedirect(PortRedirect object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sender Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sender Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSenderSpec(SenderSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValueSpec(ValueSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Value Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Value Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleValueSpec(SingleValueSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alt Guard Char</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alt Guard Char</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltGuardChar(AltGuardChar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Altstep Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Altstep Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAltstepInstance(AltstepInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionInstance(FunctionInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionRef(FunctionRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Actual Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Actual Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionActualParList(FunctionActualParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Actual Par Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Actual Par Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionActualParAssignment(FunctionActualParAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Ref Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Ref Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentRefAssignment(ComponentRefAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formal Port And Value Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formal Port And Value Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormalPortAndValuePar(FormalPortAndValuePar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Ref Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Ref Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortRefAssignment(PortRefAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Ref Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Ref Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerRefAssignment(TimerRefAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Actual Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Actual Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionActualPar(FunctionActualPar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentRef(ComponentRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Or Default Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Or Default Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentOrDefaultReference(ComponentOrDefaultReference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Testcase Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Testcase Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTestcaseInstance(TestcaseInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Testcase Actual Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Testcase Actual Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTestcaseActualParList(TestcaseActualParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerStatements(TimerStatements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timeout Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timeout Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimeoutStatement(TimeoutStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Start Timer Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Start Timer Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStartTimerStatement(StartTimerStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Stop Timer Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Stop Timer Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStopTimerStatement(StopTimerStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Ref Or Any</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Ref Or Any</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerRefOrAny(TimerRefOrAny object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Ref Or All</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Ref Or All</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerRefOrAll(TimerRefOrAll object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Basic Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Basic Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBasicStatements(BasicStatements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatementBlock(StatementBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Statement List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Statement List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionStatementList(FunctionStatementList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionStatement(FunctionStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Testcase Operation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Testcase Operation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTestcaseOperation(TestcaseOperation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Set Local Verdict</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Set Local Verdict</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSetLocalVerdict(SetLocalVerdict object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Configuration Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Configuration Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConfigurationStatements(ConfigurationStatements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Kill TC Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Kill TC Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseKillTCStatement(KillTCStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Stop TC Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Stop TC Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStopTCStatement(StopTCStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Reference Or Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Reference Or Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentReferenceOrLiteral(ComponentReferenceOrLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Start TC Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Start TC Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStartTCStatement(StartTCStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unmap Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unmap Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnmapStatement(UnmapStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Disconnect Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Disconnect Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDisconnectStatement(DisconnectStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Connections Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Connections Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllConnectionsSpec(AllConnectionsSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Ports Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Ports Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllPortsSpec(AllPortsSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Map Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Map Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMapStatement(MapStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Param Clause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Param Clause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParamClause(ParamClause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Connect Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Connect Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConnectStatement(ConnectStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Connection Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Connection Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleConnectionSpec(SingleConnectionSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortRef(PortRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Communication Statements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Communication Statements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCommunicationStatements(CommunicationStatements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Check State Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Check State Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCheckStateStatement(CheckStateStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Or All Any</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Or All Any</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortOrAllAny(PortOrAllAny object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Halt Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Halt Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHaltStatement(HaltStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Start Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Start Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStartStatement(StartStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Stop Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Stop Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStopStatement(StopStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Clear Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Clear Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseClearStatement(ClearStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Or All</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Or All</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortOrAll(PortOrAll object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Raise Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Raise Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRaiseStatement(RaiseStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Raise Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Raise Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortRaiseOp(PortRaiseOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reply Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reply Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReplyStatement(ReplyStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Reply Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Reply Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortReplyOp(PortReplyOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reply Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reply Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReplyValue(ReplyValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallStatement(CallStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Call Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Call Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortCallOp(PortCallOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Parameters</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Parameters</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallParameters(CallParameters object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Timer Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Timer Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallTimerValue(CallTimerValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Call Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Call Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortCallBody(PortCallBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Body Statement List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Body Statement List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallBodyStatementList(CallBodyStatementList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Body Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Body Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallBodyStatement(CallBodyStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Body Guard</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Body Guard</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallBodyGuard(CallBodyGuard object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Body Ops</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Body Ops</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallBodyOps(CallBodyOps object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Send Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Send Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSendStatement(SendStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Send Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Send Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortSendOp(PortSendOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>To Clause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>To Clause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseToClause(ToClause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Def List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Def List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionDefList(FunctionDefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Local Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Local Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionLocalDef(FunctionLocalDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Local Inst</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Local Inst</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionLocalInst(FunctionLocalInst object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerInstance(TimerInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Var Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Var Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVarInstance(VarInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Var List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Var List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVarList(VarList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Module Or Group</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Module Or Group</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModuleOrGroup(ModuleOrGroup object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Referenced Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Referenced Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferencedType(ReferencedType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeDef(TypeDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Def Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Def Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeDefBody(TypeDefBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Type Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Type Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubTypeDef(SubTypeDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Type Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Type Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubTypeDefNamed(SubTypeDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Structured Type Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Structured Type Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStructuredTypeDef(StructuredTypeDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordDef(RecordDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordDefNamed(RecordDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Of Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Of Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordOfDef(RecordOfDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Of Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Of Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordOfDefNamed(RecordOfDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Struct Def Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Struct Def Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStructDefBody(StructDefBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Struct Field Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Struct Field Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStructFieldDef(StructFieldDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Set Of Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Set Of Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSetOfDef(SetOfDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Set Of Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Set Of Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSetOfDefNamed(SetOfDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortDef(PortDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Def Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Def Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortDefBody(PortDefBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Def Attribs</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Def Attribs</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortDefAttribs(PortDefAttribs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mixed Attribs</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mixed Attribs</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMixedAttribs(MixedAttribs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mixed List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mixed List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMixedList(MixedList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Proc Or Type List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Proc Or Type List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProcOrTypeList(ProcOrTypeList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Proc Or Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Proc Or Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProcOrType(ProcOrType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Message Attribs</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Message Attribs</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMessageAttribs(MessageAttribs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Config Param Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Config Param Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConfigParamDef(ConfigParamDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Map Param Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Map Param Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMapParamDef(MapParamDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unmap Param Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unmap Param Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnmapParamDef(UnmapParamDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Address Decl</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Address Decl</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddressDecl(AddressDecl object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Procedure Attribs</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Procedure Attribs</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProcedureAttribs(ProcedureAttribs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentDef(ComponentDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Def List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Def List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentDefList(ComponentDefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortInstance(PortInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortElement(PortElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Element Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Element Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentElementDef(ComponentElementDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Procedure List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Procedure List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProcedureList(ProcedureList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Or Signature List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Or Signature List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllOrSignatureList(AllOrSignatureList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Signature List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Signature List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSignatureList(SignatureList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enum Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enum Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumDef(EnumDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enum Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enum Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumDefNamed(EnumDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Type Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Type Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedTypeDef(NestedTypeDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Record Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Record Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedRecordDef(NestedRecordDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Union Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Union Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedUnionDef(NestedUnionDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Set Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Set Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedSetDef(NestedSetDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Record Of Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Record Of Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedRecordOfDef(NestedRecordOfDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Set Of Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Set Of Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedSetOfDef(NestedSetOfDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Enum Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Enum Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedEnumDef(NestedEnumDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Message List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Message List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMessageList(MessageList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Or Type List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Or Type List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllOrTypeList(AllOrTypeList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeList(TypeList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionDef(UnionDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionDefNamed(UnionDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Def Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Def Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionDefBody(UnionDefBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enumeration List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enumeration List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumerationList(EnumerationList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enumeration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enumeration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnumeration(Enumeration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Field Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Field Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionFieldDef(UnionFieldDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Set Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Set Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSetDef(SetDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Set Def Named</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Set Def Named</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSetDefNamed(SetDefNamed object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Type Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Type Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubTypeSpec(SubTypeSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Allowed Values Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Allowed Values Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllowedValuesSpec(AllowedValuesSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Or Range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Or Range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateOrRange(TemplateOrRange object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Range Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Range Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRangeDef(RangeDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBound(Bound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>String Length</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>String Length</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStringLength(StringLength object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Char String Match</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Char String Match</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCharStringMatch(CharStringMatch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pattern Particle</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pattern Particle</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePatternParticle(PatternParticle object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateDef(TemplateDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Derived Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Derived Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDerivedDef(DerivedDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Base Template</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Base Template</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBaseTemplate(BaseTemplate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Temp Var List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Temp Var List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTempVarList(TempVarList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Var Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Var Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerVarInstance(TimerVarInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Var Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Var Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleVarInstance(SingleVarInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Temp Var Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Temp Var Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleTempVarInstance(SingleTempVarInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>In Line Template</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>In Line Template</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInLineTemplate(InLineTemplate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Derived Ref With Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Derived Ref With Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDerivedRefWithParList(DerivedRefWithParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateBody(TemplateBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Extra Matching Attributes</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Extra Matching Attributes</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtraMatchingAttributes(ExtraMatchingAttributes object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Spec List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Spec List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldSpecList(FieldSpecList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldSpec(FieldSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleSpec(SimpleSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Template Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Template Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleTemplateSpec(SimpleTemplateSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Template Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Template Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleTemplateExpression(SingleTemplateExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Ref With Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Ref With Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateRefWithParList(TemplateRefWithParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Actual Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Actual Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateActualParList(TemplateActualParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Matching Symbol</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Matching Symbol</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMatchingSymbol(MatchingSymbol object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subset Match</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subset Match</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubsetMatch(SubsetMatch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Superset Match</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Superset Match</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSupersetMatch(SupersetMatch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRange(Range object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Wildcard Length Match</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Wildcard Length Match</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWildcardLengthMatch(WildcardLengthMatch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Complement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Complement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComplement(Complement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>List Of Templates</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>List Of Templates</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseListOfTemplates(ListOfTemplates object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Ops</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Ops</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateOps(TemplateOps object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Match Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Match Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMatchOp(MatchOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Valueof Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Valueof Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValueofOp(ValueofOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Configuration Ops</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Configuration Ops</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConfigurationOps(ConfigurationOps object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Create Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Create Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCreateOp(CreateOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Running Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Running Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRunningOp(RunningOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Op Call</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Op Call</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOpCall(OpCall object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alive Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alive Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAliveOp(AliveOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template List Item</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template List Item</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateListItem(TemplateListItem object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Elements From</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Elements From</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllElementsFrom(AllElementsFrom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Signature</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Signature</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSignature(Signature object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Instance Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Instance Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateInstanceAssignment(TemplateInstanceAssignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Instance Actual Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Instance Actual Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateInstanceActualPar(TemplateInstanceActualPar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Template Restriction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Template Restriction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTemplateRestriction(TemplateRestriction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Restricted Template</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Restricted Template</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRestrictedTemplate(RestrictedTemplate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Log Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Log Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLogStatement(LogStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Log Item</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Log Item</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLogItem(LogItem object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpression(Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>With Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>With Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWithStatement(WithStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>With Attrib List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>With Attrib List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWithAttribList(WithAttribList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Multi With Attrib</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Multi With Attrib</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMultiWithAttrib(MultiWithAttrib object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single With Attrib</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single With Attrib</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleWithAttrib(SingleWithAttrib object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Attrib Qualifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Attrib Qualifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAttribQualifier(AttribQualifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifier List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifier List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifierList(IdentifierList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Qualified Identifier List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Qualified Identifier List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseQualifiedIdentifierList(QualifiedIdentifierList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Const Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Const Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleConstDef(SingleConstDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Compound Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Compound Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCompoundExpression(CompoundExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayExpression(ArrayExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Expression List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Expression List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldExpressionList(FieldExpressionList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Element Expression List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Element Expression List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayElementExpressionList(ArrayElementExpressionList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Expression Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Expression Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldExpressionSpec(FieldExpressionSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Not Used Or Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Not Used Or Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNotUsedOrExpression(NotUsedOrExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstantExpression(ConstantExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Compound Const Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Compound Const Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCompoundConstExpression(CompoundConstExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Const Expression List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Const Expression List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldConstExpressionList(FieldConstExpressionList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Const Expression Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Const Expression Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldConstExpressionSpec(FieldConstExpressionSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Const Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Const Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayConstExpression(ArrayConstExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Element Const Expression List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Element Const Expression List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayElementConstExpressionList(ArrayElementConstExpressionList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Const List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Const List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstList(ConstList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValue(Value object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Referenced Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Referenced Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferencedValue(ReferencedValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ref Value Head</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ref Value Head</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRefValueHead(RefValueHead object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ref Value Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ref Value Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRefValueElement(RefValueElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Head</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Head</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHead(Head object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ref Value Tail</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ref Value Tail</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRefValueTail(RefValueTail object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Spec Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Spec Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSpecElement(SpecElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Extended Field Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Extended Field Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtendedFieldReference(ExtendedFieldReference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ref Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ref Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRefValue(RefValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Predefined Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Predefined Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePredefinedValue(PredefinedValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanExpression(BooleanExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleExpression(SingleExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Def Or Field Ref List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Def Or Field Ref List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDefOrFieldRefList(DefOrFieldRefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Def Or Field Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Def Or Field Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDefOrFieldRef(DefOrFieldRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllRef(AllRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Group Ref List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Group Ref List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGroupRefList(GroupRefList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTTCN3Reference(TTCN3Reference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTTCN3ReferenceList(TTCN3ReferenceList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldReference(FieldReference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Or Bit Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Or Bit Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayOrBitRef(ArrayOrBitRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Or Bit Number</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Or Bit Number</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldOrBitNumber(FieldOrBitNumber object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Value Or Attrib</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Value Or Attrib</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayValueOrAttrib(ArrayValueOrAttrib object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Element Spec List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Element Spec List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayElementSpecList(ArrayElementSpecList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Element Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Element Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayElementSpec(ArrayElementSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Ops</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Ops</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerOps(TimerOps object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Running Timer Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Running Timer Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRunningTimerOp(RunningTimerOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Read Timer Op</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Read Timer Op</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReadTimerOp(ReadTimerOp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Permutation Match</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Permutation Match</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePermutationMatch(PermutationMatch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Loop Construct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Loop Construct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLoopConstruct(LoopConstruct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>For Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>For Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseForStatement(ForStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>While Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>While Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWhileStatement(WhileStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Do While Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Do While Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDoWhileStatement(DoWhileStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Conditional Construct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Conditional Construct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConditionalConstruct(ConditionalConstruct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Else If Clause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Else If Clause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElseIfClause(ElseIfClause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Else Clause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Else Clause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElseClause(ElseClause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Initial</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Initial</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInitial(Initial object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Case Construct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Case Construct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectCaseConstruct(SelectCaseConstruct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Case Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Case Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectCaseBody(SelectCaseBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Case</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Case</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectCase(SelectCase object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionDef(FunctionDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mtc Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mtc Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMtcSpec(MtcSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Formal Par List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Formal Par List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionFormalParList(FunctionFormalParList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Formal Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Formal Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionFormalPar(FunctionFormalPar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formal Value Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formal Value Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormalValuePar(FormalValuePar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formal Timer Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formal Timer Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormalTimerPar(FormalTimerPar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formal Port Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formal Port Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormalPortPar(FormalPortPar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formal Template Par</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formal Template Par</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormalTemplatePar(FormalTemplatePar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Runs On Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Runs On Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRunsOnSpec(RunsOnSpec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Return Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Return Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReturnType(ReturnType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Assignment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Assignment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssignment(Assignment object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableRef(VariableRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pre Def Function</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pre Def Function</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePreDefFunction(PreDefFunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2char</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2char</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2char(Fint2char object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2unichar</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2unichar</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2unichar(Fint2unichar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2bit</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2bit</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2bit(Fint2bit object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2enum</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2enum</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2enum(Fint2enum object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2hex</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2hex</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2hex(Fint2hex object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2oct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2oct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2oct(Fint2oct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2str</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2str</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2str(Fint2str object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fint2float</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fint2float</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFint2float(Fint2float object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ffloat2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ffloat2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFfloat2int(Ffloat2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fchar2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fchar2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFchar2int(Fchar2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fchar2oct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fchar2oct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFchar2oct(Fchar2oct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Funichar2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Funichar2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunichar2int(Funichar2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Funichar2oct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Funichar2oct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunichar2oct(Funichar2oct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fbit2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fbit2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFbit2int(Fbit2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fbit2hex</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fbit2hex</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFbit2hex(Fbit2hex object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fbit2oct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fbit2oct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFbit2oct(Fbit2oct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fbit2str</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fbit2str</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFbit2str(Fbit2str object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fhex2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fhex2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFhex2int(Fhex2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fhex2bit</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fhex2bit</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFhex2bit(Fhex2bit object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fhex2oct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fhex2oct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFhex2oct(Fhex2oct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fhex2str</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fhex2str</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFhex2str(Fhex2str object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foct2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foct2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFoct2int(Foct2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foct2bit</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foct2bit</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFoct2bit(Foct2bit object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foct2hex</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foct2hex</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFoct2hex(Foct2hex object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foct2str</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foct2str</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFoct2str(Foct2str object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foct2char</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foct2char</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFoct2char(Foct2char object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foct2unichar</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foct2unichar</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFoct2unichar(Foct2unichar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fstr2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fstr2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFstr2int(Fstr2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fstr2hex</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fstr2hex</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFstr2hex(Fstr2hex object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fstr2oct</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fstr2oct</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFstr2oct(Fstr2oct object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fstr2float</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fstr2float</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFstr2float(Fstr2float object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fenum2int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fenum2int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFenum2int(Fenum2int object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Flengthof</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Flengthof</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFlengthof(Flengthof object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fsizeof</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fsizeof</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFsizeof(Fsizeof object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fispresent</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fispresent</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFispresent(Fispresent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fischosen</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fischosen</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFischosen(Fischosen object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fisvalue</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fisvalue</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFisvalue(Fisvalue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fisbound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fisbound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFisbound(Fisbound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fregexp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fregexp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFregexp(Fregexp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fsubstr</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fsubstr</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFsubstr(Fsubstr object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Freplace</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Freplace</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFreplace(Freplace object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fencvalue</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fencvalue</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFencvalue(Fencvalue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fdecvalue</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fdecvalue</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFdecvalue(Fdecvalue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fencvalue Unichar</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fencvalue Unichar</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFencvalueUnichar(FencvalueUnichar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fdecvalue Unichar</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fdecvalue Unichar</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFdecvalueUnichar(FdecvalueUnichar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Frnd</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Frnd</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFrnd(Frnd object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ftestcasename</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ftestcasename</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFtestcasename(Ftestcasename object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Xor Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Xor Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseXorExpression(XorExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>And Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAndExpression(AndExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Equal Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Equal Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEqualExpression(EqualExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Rel Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Rel Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRelExpression(RelExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Shift Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Shift Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseShiftExpression(ShiftExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bit Or Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bit Or Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBitOrExpression(BitOrExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bit Xor Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bit Xor Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBitXorExpression(BitXorExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bit And Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bit And Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBitAndExpression(BitAndExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Add Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Add Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddExpression(AddExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mul Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mul Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMulExpression(MulExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //TTCN3Switch
