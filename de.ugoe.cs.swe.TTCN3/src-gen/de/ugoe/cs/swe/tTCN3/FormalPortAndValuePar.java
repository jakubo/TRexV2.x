/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Port And Value Par</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalPortAndValuePar()
 * @model
 * @generated
 */
public interface FormalPortAndValuePar extends EObject
{
} // FormalPortAndValuePar
