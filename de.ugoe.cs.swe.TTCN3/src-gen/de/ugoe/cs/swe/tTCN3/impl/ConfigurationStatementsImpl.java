/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ConfigurationStatements;
import de.ugoe.cs.swe.tTCN3.ConnectStatement;
import de.ugoe.cs.swe.tTCN3.DisconnectStatement;
import de.ugoe.cs.swe.tTCN3.DoneStatement;
import de.ugoe.cs.swe.tTCN3.KillTCStatement;
import de.ugoe.cs.swe.tTCN3.KilledStatement;
import de.ugoe.cs.swe.tTCN3.MapStatement;
import de.ugoe.cs.swe.tTCN3.StartTCStatement;
import de.ugoe.cs.swe.tTCN3.StopTCStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.UnmapStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration Statements</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getConnect <em>Connect</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getMap <em>Map</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getDisconnect <em>Disconnect</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getUnmap <em>Unmap</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getDone <em>Done</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getKilled <em>Killed</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getStartTc <em>Start Tc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getStopTc <em>Stop Tc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl#getKillTc <em>Kill Tc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigurationStatementsImpl extends MinimalEObjectImpl.Container implements ConfigurationStatements
{
  /**
   * The cached value of the '{@link #getConnect() <em>Connect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConnect()
   * @generated
   * @ordered
   */
  protected ConnectStatement connect;

  /**
   * The cached value of the '{@link #getMap() <em>Map</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMap()
   * @generated
   * @ordered
   */
  protected MapStatement map;

  /**
   * The cached value of the '{@link #getDisconnect() <em>Disconnect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisconnect()
   * @generated
   * @ordered
   */
  protected DisconnectStatement disconnect;

  /**
   * The cached value of the '{@link #getUnmap() <em>Unmap</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnmap()
   * @generated
   * @ordered
   */
  protected UnmapStatement unmap;

  /**
   * The cached value of the '{@link #getDone() <em>Done</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDone()
   * @generated
   * @ordered
   */
  protected DoneStatement done;

  /**
   * The cached value of the '{@link #getKilled() <em>Killed</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKilled()
   * @generated
   * @ordered
   */
  protected KilledStatement killed;

  /**
   * The cached value of the '{@link #getStartTc() <em>Start Tc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStartTc()
   * @generated
   * @ordered
   */
  protected StartTCStatement startTc;

  /**
   * The cached value of the '{@link #getStopTc() <em>Stop Tc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStopTc()
   * @generated
   * @ordered
   */
  protected StopTCStatement stopTc;

  /**
   * The cached value of the '{@link #getKillTc() <em>Kill Tc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKillTc()
   * @generated
   * @ordered
   */
  protected KillTCStatement killTc;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConfigurationStatementsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getConfigurationStatements();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConnectStatement getConnect()
  {
    return connect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConnect(ConnectStatement newConnect, NotificationChain msgs)
  {
    ConnectStatement oldConnect = connect;
    connect = newConnect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT, oldConnect, newConnect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConnect(ConnectStatement newConnect)
  {
    if (newConnect != connect)
    {
      NotificationChain msgs = null;
      if (connect != null)
        msgs = ((InternalEObject)connect).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT, null, msgs);
      if (newConnect != null)
        msgs = ((InternalEObject)newConnect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT, null, msgs);
      msgs = basicSetConnect(newConnect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT, newConnect, newConnect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MapStatement getMap()
  {
    return map;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMap(MapStatement newMap, NotificationChain msgs)
  {
    MapStatement oldMap = map;
    map = newMap;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__MAP, oldMap, newMap);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMap(MapStatement newMap)
  {
    if (newMap != map)
    {
      NotificationChain msgs = null;
      if (map != null)
        msgs = ((InternalEObject)map).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__MAP, null, msgs);
      if (newMap != null)
        msgs = ((InternalEObject)newMap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__MAP, null, msgs);
      msgs = basicSetMap(newMap, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__MAP, newMap, newMap));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DisconnectStatement getDisconnect()
  {
    return disconnect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDisconnect(DisconnectStatement newDisconnect, NotificationChain msgs)
  {
    DisconnectStatement oldDisconnect = disconnect;
    disconnect = newDisconnect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT, oldDisconnect, newDisconnect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDisconnect(DisconnectStatement newDisconnect)
  {
    if (newDisconnect != disconnect)
    {
      NotificationChain msgs = null;
      if (disconnect != null)
        msgs = ((InternalEObject)disconnect).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT, null, msgs);
      if (newDisconnect != null)
        msgs = ((InternalEObject)newDisconnect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT, null, msgs);
      msgs = basicSetDisconnect(newDisconnect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT, newDisconnect, newDisconnect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnmapStatement getUnmap()
  {
    return unmap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUnmap(UnmapStatement newUnmap, NotificationChain msgs)
  {
    UnmapStatement oldUnmap = unmap;
    unmap = newUnmap;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP, oldUnmap, newUnmap);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnmap(UnmapStatement newUnmap)
  {
    if (newUnmap != unmap)
    {
      NotificationChain msgs = null;
      if (unmap != null)
        msgs = ((InternalEObject)unmap).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP, null, msgs);
      if (newUnmap != null)
        msgs = ((InternalEObject)newUnmap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP, null, msgs);
      msgs = basicSetUnmap(newUnmap, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP, newUnmap, newUnmap));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DoneStatement getDone()
  {
    return done;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDone(DoneStatement newDone, NotificationChain msgs)
  {
    DoneStatement oldDone = done;
    done = newDone;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__DONE, oldDone, newDone);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDone(DoneStatement newDone)
  {
    if (newDone != done)
    {
      NotificationChain msgs = null;
      if (done != null)
        msgs = ((InternalEObject)done).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__DONE, null, msgs);
      if (newDone != null)
        msgs = ((InternalEObject)newDone).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__DONE, null, msgs);
      msgs = basicSetDone(newDone, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__DONE, newDone, newDone));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public KilledStatement getKilled()
  {
    return killed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetKilled(KilledStatement newKilled, NotificationChain msgs)
  {
    KilledStatement oldKilled = killed;
    killed = newKilled;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__KILLED, oldKilled, newKilled);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKilled(KilledStatement newKilled)
  {
    if (newKilled != killed)
    {
      NotificationChain msgs = null;
      if (killed != null)
        msgs = ((InternalEObject)killed).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__KILLED, null, msgs);
      if (newKilled != null)
        msgs = ((InternalEObject)newKilled).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__KILLED, null, msgs);
      msgs = basicSetKilled(newKilled, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__KILLED, newKilled, newKilled));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StartTCStatement getStartTc()
  {
    return startTc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStartTc(StartTCStatement newStartTc, NotificationChain msgs)
  {
    StartTCStatement oldStartTc = startTc;
    startTc = newStartTc;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__START_TC, oldStartTc, newStartTc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStartTc(StartTCStatement newStartTc)
  {
    if (newStartTc != startTc)
    {
      NotificationChain msgs = null;
      if (startTc != null)
        msgs = ((InternalEObject)startTc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__START_TC, null, msgs);
      if (newStartTc != null)
        msgs = ((InternalEObject)newStartTc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__START_TC, null, msgs);
      msgs = basicSetStartTc(newStartTc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__START_TC, newStartTc, newStartTc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StopTCStatement getStopTc()
  {
    return stopTc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStopTc(StopTCStatement newStopTc, NotificationChain msgs)
  {
    StopTCStatement oldStopTc = stopTc;
    stopTc = newStopTc;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC, oldStopTc, newStopTc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStopTc(StopTCStatement newStopTc)
  {
    if (newStopTc != stopTc)
    {
      NotificationChain msgs = null;
      if (stopTc != null)
        msgs = ((InternalEObject)stopTc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC, null, msgs);
      if (newStopTc != null)
        msgs = ((InternalEObject)newStopTc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC, null, msgs);
      msgs = basicSetStopTc(newStopTc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC, newStopTc, newStopTc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public KillTCStatement getKillTc()
  {
    return killTc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetKillTc(KillTCStatement newKillTc, NotificationChain msgs)
  {
    KillTCStatement oldKillTc = killTc;
    killTc = newKillTc;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC, oldKillTc, newKillTc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKillTc(KillTCStatement newKillTc)
  {
    if (newKillTc != killTc)
    {
      NotificationChain msgs = null;
      if (killTc != null)
        msgs = ((InternalEObject)killTc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC, null, msgs);
      if (newKillTc != null)
        msgs = ((InternalEObject)newKillTc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC, null, msgs);
      msgs = basicSetKillTc(newKillTc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC, newKillTc, newKillTc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT:
        return basicSetConnect(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__MAP:
        return basicSetMap(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT:
        return basicSetDisconnect(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP:
        return basicSetUnmap(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__DONE:
        return basicSetDone(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILLED:
        return basicSetKilled(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__START_TC:
        return basicSetStartTc(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC:
        return basicSetStopTc(null, msgs);
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC:
        return basicSetKillTc(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT:
        return getConnect();
      case TTCN3Package.CONFIGURATION_STATEMENTS__MAP:
        return getMap();
      case TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT:
        return getDisconnect();
      case TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP:
        return getUnmap();
      case TTCN3Package.CONFIGURATION_STATEMENTS__DONE:
        return getDone();
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILLED:
        return getKilled();
      case TTCN3Package.CONFIGURATION_STATEMENTS__START_TC:
        return getStartTc();
      case TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC:
        return getStopTc();
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC:
        return getKillTc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT:
        setConnect((ConnectStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__MAP:
        setMap((MapStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT:
        setDisconnect((DisconnectStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP:
        setUnmap((UnmapStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__DONE:
        setDone((DoneStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILLED:
        setKilled((KilledStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__START_TC:
        setStartTc((StartTCStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC:
        setStopTc((StopTCStatement)newValue);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC:
        setKillTc((KillTCStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT:
        setConnect((ConnectStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__MAP:
        setMap((MapStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT:
        setDisconnect((DisconnectStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP:
        setUnmap((UnmapStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__DONE:
        setDone((DoneStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILLED:
        setKilled((KilledStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__START_TC:
        setStartTc((StartTCStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC:
        setStopTc((StopTCStatement)null);
        return;
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC:
        setKillTc((KillTCStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_STATEMENTS__CONNECT:
        return connect != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__MAP:
        return map != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__DISCONNECT:
        return disconnect != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__UNMAP:
        return unmap != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__DONE:
        return done != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILLED:
        return killed != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__START_TC:
        return startTc != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__STOP_TC:
        return stopTc != null;
      case TTCN3Package.CONFIGURATION_STATEMENTS__KILL_TC:
        return killTc != null;
    }
    return super.eIsSet(featureID);
  }

} //ConfigurationStatementsImpl
