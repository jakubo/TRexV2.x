/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AddressDecl;
import de.ugoe.cs.swe.tTCN3.ConfigParamDef;
import de.ugoe.cs.swe.tTCN3.ProcedureAttribs;
import de.ugoe.cs.swe.tTCN3.ProcedureList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure Attribs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ProcedureAttribsImpl#getAddresses <em>Addresses</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ProcedureAttribsImpl#getProcs <em>Procs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ProcedureAttribsImpl#getConfigs <em>Configs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureAttribsImpl extends MinimalEObjectImpl.Container implements ProcedureAttribs
{
  /**
   * The cached value of the '{@link #getAddresses() <em>Addresses</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddresses()
   * @generated
   * @ordered
   */
  protected EList<AddressDecl> addresses;

  /**
   * The cached value of the '{@link #getProcs() <em>Procs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProcs()
   * @generated
   * @ordered
   */
  protected EList<ProcedureList> procs;

  /**
   * The cached value of the '{@link #getConfigs() <em>Configs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfigs()
   * @generated
   * @ordered
   */
  protected EList<ConfigParamDef> configs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProcedureAttribsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getProcedureAttribs();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AddressDecl> getAddresses()
  {
    if (addresses == null)
    {
      addresses = new EObjectContainmentEList<AddressDecl>(AddressDecl.class, this, TTCN3Package.PROCEDURE_ATTRIBS__ADDRESSES);
    }
    return addresses;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ProcedureList> getProcs()
  {
    if (procs == null)
    {
      procs = new EObjectContainmentEList<ProcedureList>(ProcedureList.class, this, TTCN3Package.PROCEDURE_ATTRIBS__PROCS);
    }
    return procs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ConfigParamDef> getConfigs()
  {
    if (configs == null)
    {
      configs = new EObjectContainmentEList<ConfigParamDef>(ConfigParamDef.class, this, TTCN3Package.PROCEDURE_ATTRIBS__CONFIGS);
    }
    return configs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_ATTRIBS__ADDRESSES:
        return ((InternalEList<?>)getAddresses()).basicRemove(otherEnd, msgs);
      case TTCN3Package.PROCEDURE_ATTRIBS__PROCS:
        return ((InternalEList<?>)getProcs()).basicRemove(otherEnd, msgs);
      case TTCN3Package.PROCEDURE_ATTRIBS__CONFIGS:
        return ((InternalEList<?>)getConfigs()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_ATTRIBS__ADDRESSES:
        return getAddresses();
      case TTCN3Package.PROCEDURE_ATTRIBS__PROCS:
        return getProcs();
      case TTCN3Package.PROCEDURE_ATTRIBS__CONFIGS:
        return getConfigs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_ATTRIBS__ADDRESSES:
        getAddresses().clear();
        getAddresses().addAll((Collection<? extends AddressDecl>)newValue);
        return;
      case TTCN3Package.PROCEDURE_ATTRIBS__PROCS:
        getProcs().clear();
        getProcs().addAll((Collection<? extends ProcedureList>)newValue);
        return;
      case TTCN3Package.PROCEDURE_ATTRIBS__CONFIGS:
        getConfigs().clear();
        getConfigs().addAll((Collection<? extends ConfigParamDef>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_ATTRIBS__ADDRESSES:
        getAddresses().clear();
        return;
      case TTCN3Package.PROCEDURE_ATTRIBS__PROCS:
        getProcs().clear();
        return;
      case TTCN3Package.PROCEDURE_ATTRIBS__CONFIGS:
        getConfigs().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_ATTRIBS__ADDRESSES:
        return addresses != null && !addresses.isEmpty();
      case TTCN3Package.PROCEDURE_ATTRIBS__PROCS:
        return procs != null && !procs.isEmpty();
      case TTCN3Package.PROCEDURE_ATTRIBS__CONFIGS:
        return configs != null && !configs.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ProcedureAttribsImpl
