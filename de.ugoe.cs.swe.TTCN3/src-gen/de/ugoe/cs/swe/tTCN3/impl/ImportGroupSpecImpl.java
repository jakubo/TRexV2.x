/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept;
import de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept;
import de.ugoe.cs.swe.tTCN3.ImportGroupSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import Group Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportGroupSpecImpl#getGroupRef <em>Group Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportGroupSpecImpl#getGroup <em>Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImportGroupSpecImpl extends MinimalEObjectImpl.Container implements ImportGroupSpec
{
  /**
   * The cached value of the '{@link #getGroupRef() <em>Group Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroupRef()
   * @generated
   * @ordered
   */
  protected GroupRefListWithExcept groupRef;

  /**
   * The cached value of the '{@link #getGroup() <em>Group</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroup()
   * @generated
   * @ordered
   */
  protected AllGroupsWithExcept group;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ImportGroupSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getImportGroupSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupRefListWithExcept getGroupRef()
  {
    return groupRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroupRef(GroupRefListWithExcept newGroupRef, NotificationChain msgs)
  {
    GroupRefListWithExcept oldGroupRef = groupRef;
    groupRef = newGroupRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF, oldGroupRef, newGroupRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGroupRef(GroupRefListWithExcept newGroupRef)
  {
    if (newGroupRef != groupRef)
    {
      NotificationChain msgs = null;
      if (groupRef != null)
        msgs = ((InternalEObject)groupRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF, null, msgs);
      if (newGroupRef != null)
        msgs = ((InternalEObject)newGroupRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF, null, msgs);
      msgs = basicSetGroupRef(newGroupRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF, newGroupRef, newGroupRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllGroupsWithExcept getGroup()
  {
    return group;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroup(AllGroupsWithExcept newGroup, NotificationChain msgs)
  {
    AllGroupsWithExcept oldGroup = group;
    group = newGroup;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_GROUP_SPEC__GROUP, oldGroup, newGroup);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGroup(AllGroupsWithExcept newGroup)
  {
    if (newGroup != group)
    {
      NotificationChain msgs = null;
      if (group != null)
        msgs = ((InternalEObject)group).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_GROUP_SPEC__GROUP, null, msgs);
      if (newGroup != null)
        msgs = ((InternalEObject)newGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_GROUP_SPEC__GROUP, null, msgs);
      msgs = basicSetGroup(newGroup, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_GROUP_SPEC__GROUP, newGroup, newGroup));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF:
        return basicSetGroupRef(null, msgs);
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP:
        return basicSetGroup(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF:
        return getGroupRef();
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP:
        return getGroup();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF:
        setGroupRef((GroupRefListWithExcept)newValue);
        return;
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP:
        setGroup((AllGroupsWithExcept)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF:
        setGroupRef((GroupRefListWithExcept)null);
        return;
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP:
        setGroup((AllGroupsWithExcept)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP_REF:
        return groupRef != null;
      case TTCN3Package.IMPORT_GROUP_SPEC__GROUP:
        return group != null;
    }
    return super.eIsSet(featureID);
  }

} //ImportGroupSpecImpl
