/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib;
import de.ugoe.cs.swe.tTCN3.ExtraMatchingAttributes;
import de.ugoe.cs.swe.tTCN3.FieldSpecList;
import de.ugoe.cs.swe.tTCN3.SimpleSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateBody;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Body</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateBodyImpl#getSimple <em>Simple</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateBodyImpl#getField <em>Field</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateBodyImpl#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateBodyImpl#getExtra <em>Extra</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateBodyImpl extends MinimalEObjectImpl.Container implements TemplateBody
{
  /**
   * The cached value of the '{@link #getSimple() <em>Simple</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSimple()
   * @generated
   * @ordered
   */
  protected SimpleSpec simple;

  /**
   * The cached value of the '{@link #getField() <em>Field</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField()
   * @generated
   * @ordered
   */
  protected FieldSpecList field;

  /**
   * The cached value of the '{@link #getArray() <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArray()
   * @generated
   * @ordered
   */
  protected ArrayValueOrAttrib array;

  /**
   * The cached value of the '{@link #getExtra() <em>Extra</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtra()
   * @generated
   * @ordered
   */
  protected ExtraMatchingAttributes extra;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateBodyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTemplateBody();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleSpec getSimple()
  {
    return simple;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSimple(SimpleSpec newSimple, NotificationChain msgs)
  {
    SimpleSpec oldSimple = simple;
    simple = newSimple;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__SIMPLE, oldSimple, newSimple);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSimple(SimpleSpec newSimple)
  {
    if (newSimple != simple)
    {
      NotificationChain msgs = null;
      if (simple != null)
        msgs = ((InternalEObject)simple).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__SIMPLE, null, msgs);
      if (newSimple != null)
        msgs = ((InternalEObject)newSimple).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__SIMPLE, null, msgs);
      msgs = basicSetSimple(newSimple, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__SIMPLE, newSimple, newSimple));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldSpecList getField()
  {
    return field;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetField(FieldSpecList newField, NotificationChain msgs)
  {
    FieldSpecList oldField = field;
    field = newField;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__FIELD, oldField, newField);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField(FieldSpecList newField)
  {
    if (newField != field)
    {
      NotificationChain msgs = null;
      if (field != null)
        msgs = ((InternalEObject)field).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__FIELD, null, msgs);
      if (newField != null)
        msgs = ((InternalEObject)newField).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__FIELD, null, msgs);
      msgs = basicSetField(newField, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__FIELD, newField, newField));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayValueOrAttrib getArray()
  {
    return array;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetArray(ArrayValueOrAttrib newArray, NotificationChain msgs)
  {
    ArrayValueOrAttrib oldArray = array;
    array = newArray;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__ARRAY, oldArray, newArray);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArray(ArrayValueOrAttrib newArray)
  {
    if (newArray != array)
    {
      NotificationChain msgs = null;
      if (array != null)
        msgs = ((InternalEObject)array).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__ARRAY, null, msgs);
      if (newArray != null)
        msgs = ((InternalEObject)newArray).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__ARRAY, null, msgs);
      msgs = basicSetArray(newArray, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__ARRAY, newArray, newArray));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtraMatchingAttributes getExtra()
  {
    return extra;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExtra(ExtraMatchingAttributes newExtra, NotificationChain msgs)
  {
    ExtraMatchingAttributes oldExtra = extra;
    extra = newExtra;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__EXTRA, oldExtra, newExtra);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExtra(ExtraMatchingAttributes newExtra)
  {
    if (newExtra != extra)
    {
      NotificationChain msgs = null;
      if (extra != null)
        msgs = ((InternalEObject)extra).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__EXTRA, null, msgs);
      if (newExtra != null)
        msgs = ((InternalEObject)newExtra).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_BODY__EXTRA, null, msgs);
      msgs = basicSetExtra(newExtra, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_BODY__EXTRA, newExtra, newExtra));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_BODY__SIMPLE:
        return basicSetSimple(null, msgs);
      case TTCN3Package.TEMPLATE_BODY__FIELD:
        return basicSetField(null, msgs);
      case TTCN3Package.TEMPLATE_BODY__ARRAY:
        return basicSetArray(null, msgs);
      case TTCN3Package.TEMPLATE_BODY__EXTRA:
        return basicSetExtra(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_BODY__SIMPLE:
        return getSimple();
      case TTCN3Package.TEMPLATE_BODY__FIELD:
        return getField();
      case TTCN3Package.TEMPLATE_BODY__ARRAY:
        return getArray();
      case TTCN3Package.TEMPLATE_BODY__EXTRA:
        return getExtra();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_BODY__SIMPLE:
        setSimple((SimpleSpec)newValue);
        return;
      case TTCN3Package.TEMPLATE_BODY__FIELD:
        setField((FieldSpecList)newValue);
        return;
      case TTCN3Package.TEMPLATE_BODY__ARRAY:
        setArray((ArrayValueOrAttrib)newValue);
        return;
      case TTCN3Package.TEMPLATE_BODY__EXTRA:
        setExtra((ExtraMatchingAttributes)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_BODY__SIMPLE:
        setSimple((SimpleSpec)null);
        return;
      case TTCN3Package.TEMPLATE_BODY__FIELD:
        setField((FieldSpecList)null);
        return;
      case TTCN3Package.TEMPLATE_BODY__ARRAY:
        setArray((ArrayValueOrAttrib)null);
        return;
      case TTCN3Package.TEMPLATE_BODY__EXTRA:
        setExtra((ExtraMatchingAttributes)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_BODY__SIMPLE:
        return simple != null;
      case TTCN3Package.TEMPLATE_BODY__FIELD:
        return field != null;
      case TTCN3Package.TEMPLATE_BODY__ARRAY:
        return array != null;
      case TTCN3Package.TEMPLATE_BODY__EXTRA:
        return extra != null;
    }
    return super.eIsSet(featureID);
  }

} //TemplateBodyImpl
