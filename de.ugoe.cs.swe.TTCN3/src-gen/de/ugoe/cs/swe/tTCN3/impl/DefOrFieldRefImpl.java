/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllRef;
import de.ugoe.cs.swe.tTCN3.DefOrFieldRef;
import de.ugoe.cs.swe.tTCN3.ExtendedFieldReference;
import de.ugoe.cs.swe.tTCN3.FieldReference;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TTCN3Reference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Def Or Field Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefImpl#getId <em>Id</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefImpl#getField <em>Field</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefImpl#getExtended <em>Extended</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefImpl#getAll <em>All</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefOrFieldRefImpl extends MinimalEObjectImpl.Container implements DefOrFieldRef
{
  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected TTCN3Reference id;

  /**
   * The cached value of the '{@link #getField() <em>Field</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField()
   * @generated
   * @ordered
   */
  protected FieldReference field;

  /**
   * The cached value of the '{@link #getExtended() <em>Extended</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtended()
   * @generated
   * @ordered
   */
  protected ExtendedFieldReference extended;

  /**
   * The cached value of the '{@link #getAll() <em>All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected AllRef all;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DefOrFieldRefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getDefOrFieldRef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Reference getId()
  {
    if (id != null && id.eIsProxy())
    {
      InternalEObject oldId = (InternalEObject)id;
      id = (TTCN3Reference)eResolveProxy(oldId);
      if (id != oldId)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.DEF_OR_FIELD_REF__ID, oldId, id));
      }
    }
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Reference basicGetId()
  {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(TTCN3Reference newId)
  {
    TTCN3Reference oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldReference getField()
  {
    return field;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetField(FieldReference newField, NotificationChain msgs)
  {
    FieldReference oldField = field;
    field = newField;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__FIELD, oldField, newField);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField(FieldReference newField)
  {
    if (newField != field)
    {
      NotificationChain msgs = null;
      if (field != null)
        msgs = ((InternalEObject)field).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DEF_OR_FIELD_REF__FIELD, null, msgs);
      if (newField != null)
        msgs = ((InternalEObject)newField).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DEF_OR_FIELD_REF__FIELD, null, msgs);
      msgs = basicSetField(newField, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__FIELD, newField, newField));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtendedFieldReference getExtended()
  {
    return extended;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExtended(ExtendedFieldReference newExtended, NotificationChain msgs)
  {
    ExtendedFieldReference oldExtended = extended;
    extended = newExtended;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__EXTENDED, oldExtended, newExtended);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExtended(ExtendedFieldReference newExtended)
  {
    if (newExtended != extended)
    {
      NotificationChain msgs = null;
      if (extended != null)
        msgs = ((InternalEObject)extended).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DEF_OR_FIELD_REF__EXTENDED, null, msgs);
      if (newExtended != null)
        msgs = ((InternalEObject)newExtended).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DEF_OR_FIELD_REF__EXTENDED, null, msgs);
      msgs = basicSetExtended(newExtended, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__EXTENDED, newExtended, newExtended));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllRef getAll()
  {
    return all;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAll(AllRef newAll, NotificationChain msgs)
  {
    AllRef oldAll = all;
    all = newAll;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__ALL, oldAll, newAll);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAll(AllRef newAll)
  {
    if (newAll != all)
    {
      NotificationChain msgs = null;
      if (all != null)
        msgs = ((InternalEObject)all).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DEF_OR_FIELD_REF__ALL, null, msgs);
      if (newAll != null)
        msgs = ((InternalEObject)newAll).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DEF_OR_FIELD_REF__ALL, null, msgs);
      msgs = basicSetAll(newAll, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DEF_OR_FIELD_REF__ALL, newAll, newAll));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.DEF_OR_FIELD_REF__FIELD:
        return basicSetField(null, msgs);
      case TTCN3Package.DEF_OR_FIELD_REF__EXTENDED:
        return basicSetExtended(null, msgs);
      case TTCN3Package.DEF_OR_FIELD_REF__ALL:
        return basicSetAll(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.DEF_OR_FIELD_REF__ID:
        if (resolve) return getId();
        return basicGetId();
      case TTCN3Package.DEF_OR_FIELD_REF__FIELD:
        return getField();
      case TTCN3Package.DEF_OR_FIELD_REF__EXTENDED:
        return getExtended();
      case TTCN3Package.DEF_OR_FIELD_REF__ALL:
        return getAll();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.DEF_OR_FIELD_REF__ID:
        setId((TTCN3Reference)newValue);
        return;
      case TTCN3Package.DEF_OR_FIELD_REF__FIELD:
        setField((FieldReference)newValue);
        return;
      case TTCN3Package.DEF_OR_FIELD_REF__EXTENDED:
        setExtended((ExtendedFieldReference)newValue);
        return;
      case TTCN3Package.DEF_OR_FIELD_REF__ALL:
        setAll((AllRef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.DEF_OR_FIELD_REF__ID:
        setId((TTCN3Reference)null);
        return;
      case TTCN3Package.DEF_OR_FIELD_REF__FIELD:
        setField((FieldReference)null);
        return;
      case TTCN3Package.DEF_OR_FIELD_REF__EXTENDED:
        setExtended((ExtendedFieldReference)null);
        return;
      case TTCN3Package.DEF_OR_FIELD_REF__ALL:
        setAll((AllRef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.DEF_OR_FIELD_REF__ID:
        return id != null;
      case TTCN3Package.DEF_OR_FIELD_REF__FIELD:
        return field != null;
      case TTCN3Package.DEF_OR_FIELD_REF__EXTENDED:
        return extended != null;
      case TTCN3Package.DEF_OR_FIELD_REF__ALL:
        return all != null;
    }
    return super.eIsSet(featureID);
  }

} //DefOrFieldRefImpl
