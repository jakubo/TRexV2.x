/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ControlStatement;
import de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList;
import de.ugoe.cs.swe.tTCN3.FunctionLocalDef;
import de.ugoe.cs.swe.tTCN3.FunctionLocalInst;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Statement Or Def List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl#getLocalDef <em>Local Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl#getLocalInst <em>Local Inst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl#getWithstm <em>Withstm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl#getControl <em>Control</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlStatementOrDefListImpl extends MinimalEObjectImpl.Container implements ControlStatementOrDefList
{
  /**
   * The cached value of the '{@link #getLocalDef() <em>Local Def</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocalDef()
   * @generated
   * @ordered
   */
  protected EList<FunctionLocalDef> localDef;

  /**
   * The cached value of the '{@link #getLocalInst() <em>Local Inst</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocalInst()
   * @generated
   * @ordered
   */
  protected EList<FunctionLocalInst> localInst;

  /**
   * The cached value of the '{@link #getWithstm() <em>Withstm</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWithstm()
   * @generated
   * @ordered
   */
  protected EList<WithStatement> withstm;

  /**
   * The cached value of the '{@link #getControl() <em>Control</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getControl()
   * @generated
   * @ordered
   */
  protected EList<ControlStatement> control;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected EList<String> sc;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ControlStatementOrDefListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getControlStatementOrDefList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionLocalDef> getLocalDef()
  {
    if (localDef == null)
    {
      localDef = new EObjectContainmentEList<FunctionLocalDef>(FunctionLocalDef.class, this, TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF);
    }
    return localDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionLocalInst> getLocalInst()
  {
    if (localInst == null)
    {
      localInst = new EObjectContainmentEList<FunctionLocalInst>(FunctionLocalInst.class, this, TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST);
    }
    return localInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<WithStatement> getWithstm()
  {
    if (withstm == null)
    {
      withstm = new EObjectContainmentEList<WithStatement>(WithStatement.class, this, TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM);
    }
    return withstm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ControlStatement> getControl()
  {
    if (control == null)
    {
      control = new EObjectContainmentEList<ControlStatement>(ControlStatement.class, this, TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__CONTROL);
    }
    return control;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSc()
  {
    if (sc == null)
    {
      sc = new EDataTypeEList<String>(String.class, this, TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__SC);
    }
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF:
        return ((InternalEList<?>)getLocalDef()).basicRemove(otherEnd, msgs);
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST:
        return ((InternalEList<?>)getLocalInst()).basicRemove(otherEnd, msgs);
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM:
        return ((InternalEList<?>)getWithstm()).basicRemove(otherEnd, msgs);
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__CONTROL:
        return ((InternalEList<?>)getControl()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF:
        return getLocalDef();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST:
        return getLocalInst();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM:
        return getWithstm();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__CONTROL:
        return getControl();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF:
        getLocalDef().clear();
        getLocalDef().addAll((Collection<? extends FunctionLocalDef>)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST:
        getLocalInst().clear();
        getLocalInst().addAll((Collection<? extends FunctionLocalInst>)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM:
        getWithstm().clear();
        getWithstm().addAll((Collection<? extends WithStatement>)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__CONTROL:
        getControl().clear();
        getControl().addAll((Collection<? extends ControlStatement>)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__SC:
        getSc().clear();
        getSc().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF:
        getLocalDef().clear();
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST:
        getLocalInst().clear();
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM:
        getWithstm().clear();
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__CONTROL:
        getControl().clear();
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__SC:
        getSc().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF:
        return localDef != null && !localDef.isEmpty();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST:
        return localInst != null && !localInst.isEmpty();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM:
        return withstm != null && !withstm.isEmpty();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__CONTROL:
        return control != null && !control.isEmpty();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST__SC:
        return sc != null && !sc.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //ControlStatementOrDefListImpl
