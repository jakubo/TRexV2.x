/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar;
import de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment;
import de.ugoe.cs.swe.tTCN3.TestcaseActualParList;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Testcase Actual Par List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseActualParListImpl#getTemplParam <em>Templ Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseActualParListImpl#getTemplAssign <em>Templ Assign</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestcaseActualParListImpl extends MinimalEObjectImpl.Container implements TestcaseActualParList
{
  /**
   * The cached value of the '{@link #getTemplParam() <em>Templ Param</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplParam()
   * @generated
   * @ordered
   */
  protected EList<TemplateInstanceActualPar> templParam;

  /**
   * The cached value of the '{@link #getTemplAssign() <em>Templ Assign</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplAssign()
   * @generated
   * @ordered
   */
  protected EList<TemplateInstanceAssignment> templAssign;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TestcaseActualParListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTestcaseActualParList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TemplateInstanceActualPar> getTemplParam()
  {
    if (templParam == null)
    {
      templParam = new EObjectContainmentEList<TemplateInstanceActualPar>(TemplateInstanceActualPar.class, this, TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM);
    }
    return templParam;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TemplateInstanceAssignment> getTemplAssign()
  {
    if (templAssign == null)
    {
      templAssign = new EObjectContainmentEList<TemplateInstanceAssignment>(TemplateInstanceAssignment.class, this, TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN);
    }
    return templAssign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM:
        return ((InternalEList<?>)getTemplParam()).basicRemove(otherEnd, msgs);
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN:
        return ((InternalEList<?>)getTemplAssign()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM:
        return getTemplParam();
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN:
        return getTemplAssign();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM:
        getTemplParam().clear();
        getTemplParam().addAll((Collection<? extends TemplateInstanceActualPar>)newValue);
        return;
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN:
        getTemplAssign().clear();
        getTemplAssign().addAll((Collection<? extends TemplateInstanceAssignment>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM:
        getTemplParam().clear();
        return;
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN:
        getTemplAssign().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM:
        return templParam != null && !templParam.isEmpty();
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN:
        return templAssign != null && !templAssign.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TestcaseActualParListImpl
