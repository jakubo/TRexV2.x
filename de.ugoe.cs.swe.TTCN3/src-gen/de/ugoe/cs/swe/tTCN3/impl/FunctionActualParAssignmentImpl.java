/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ComponentRefAssignment;
import de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment;
import de.ugoe.cs.swe.tTCN3.PortRefAssignment;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment;
import de.ugoe.cs.swe.tTCN3.TimerRefAssignment;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Actual Par Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParAssignmentImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParAssignmentImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParAssignmentImpl#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParAssignmentImpl#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionActualParAssignmentImpl extends MinimalEObjectImpl.Container implements FunctionActualParAssignment
{
  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected TemplateInstanceAssignment template;

  /**
   * The cached value of the '{@link #getComponent() <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComponent()
   * @generated
   * @ordered
   */
  protected ComponentRefAssignment component;

  /**
   * The cached value of the '{@link #getPort() <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort()
   * @generated
   * @ordered
   */
  protected PortRefAssignment port;

  /**
   * The cached value of the '{@link #getTimer() <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimer()
   * @generated
   * @ordered
   */
  protected TimerRefAssignment timer;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionActualParAssignmentImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionActualParAssignment();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateInstanceAssignment getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(TemplateInstanceAssignment newTemplate, NotificationChain msgs)
  {
    TemplateInstanceAssignment oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(TemplateInstanceAssignment newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentRefAssignment getComponent()
  {
    return component;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComponent(ComponentRefAssignment newComponent, NotificationChain msgs)
  {
    ComponentRefAssignment oldComponent = component;
    component = newComponent;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT, oldComponent, newComponent);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponent(ComponentRefAssignment newComponent)
  {
    if (newComponent != component)
    {
      NotificationChain msgs = null;
      if (component != null)
        msgs = ((InternalEObject)component).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT, null, msgs);
      if (newComponent != null)
        msgs = ((InternalEObject)newComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT, null, msgs);
      msgs = basicSetComponent(newComponent, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT, newComponent, newComponent));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRefAssignment getPort()
  {
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPort(PortRefAssignment newPort, NotificationChain msgs)
  {
    PortRefAssignment oldPort = port;
    port = newPort;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT, oldPort, newPort);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort(PortRefAssignment newPort)
  {
    if (newPort != port)
    {
      NotificationChain msgs = null;
      if (port != null)
        msgs = ((InternalEObject)port).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT, null, msgs);
      if (newPort != null)
        msgs = ((InternalEObject)newPort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT, null, msgs);
      msgs = basicSetPort(newPort, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT, newPort, newPort));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerRefAssignment getTimer()
  {
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimer(TimerRefAssignment newTimer, NotificationChain msgs)
  {
    TimerRefAssignment oldTimer = timer;
    timer = newTimer;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER, oldTimer, newTimer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimer(TimerRefAssignment newTimer)
  {
    if (newTimer != timer)
    {
      NotificationChain msgs = null;
      if (timer != null)
        msgs = ((InternalEObject)timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER, null, msgs);
      if (newTimer != null)
        msgs = ((InternalEObject)newTimer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER, null, msgs);
      msgs = basicSetTimer(newTimer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER, newTimer, newTimer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE:
        return basicSetTemplate(null, msgs);
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT:
        return basicSetComponent(null, msgs);
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT:
        return basicSetPort(null, msgs);
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER:
        return basicSetTimer(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE:
        return getTemplate();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT:
        return getComponent();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT:
        return getPort();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER:
        return getTimer();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE:
        setTemplate((TemplateInstanceAssignment)newValue);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT:
        setComponent((ComponentRefAssignment)newValue);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT:
        setPort((PortRefAssignment)newValue);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER:
        setTimer((TimerRefAssignment)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE:
        setTemplate((TemplateInstanceAssignment)null);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT:
        setComponent((ComponentRefAssignment)null);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT:
        setPort((PortRefAssignment)null);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER:
        setTimer((TimerRefAssignment)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE:
        return template != null;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT:
        return component != null;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT:
        return port != null;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER:
        return timer != null;
    }
    return super.eIsSet(featureID);
  }

} //FunctionActualParAssignmentImpl
