/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ConfigSpec;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList;
import de.ugoe.cs.swe.tTCN3.TestcaseDef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Testcase Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseDefImpl#getParList <em>Par List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseDefImpl#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseDefImpl#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestcaseDefImpl extends TTCN3ReferenceImpl implements TestcaseDef
{
  /**
   * The cached value of the '{@link #getParList() <em>Par List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParList()
   * @generated
   * @ordered
   */
  protected TemplateOrValueFormalParList parList;

  /**
   * The cached value of the '{@link #getSpec() <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpec()
   * @generated
   * @ordered
   */
  protected ConfigSpec spec;

  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected StatementBlock statement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TestcaseDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTestcaseDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateOrValueFormalParList getParList()
  {
    return parList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParList(TemplateOrValueFormalParList newParList, NotificationChain msgs)
  {
    TemplateOrValueFormalParList oldParList = parList;
    parList = newParList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_DEF__PAR_LIST, oldParList, newParList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParList(TemplateOrValueFormalParList newParList)
  {
    if (newParList != parList)
    {
      NotificationChain msgs = null;
      if (parList != null)
        msgs = ((InternalEObject)parList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_DEF__PAR_LIST, null, msgs);
      if (newParList != null)
        msgs = ((InternalEObject)newParList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_DEF__PAR_LIST, null, msgs);
      msgs = basicSetParList(newParList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_DEF__PAR_LIST, newParList, newParList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigSpec getSpec()
  {
    return spec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSpec(ConfigSpec newSpec, NotificationChain msgs)
  {
    ConfigSpec oldSpec = spec;
    spec = newSpec;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_DEF__SPEC, oldSpec, newSpec);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpec(ConfigSpec newSpec)
  {
    if (newSpec != spec)
    {
      NotificationChain msgs = null;
      if (spec != null)
        msgs = ((InternalEObject)spec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_DEF__SPEC, null, msgs);
      if (newSpec != null)
        msgs = ((InternalEObject)newSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_DEF__SPEC, null, msgs);
      msgs = basicSetSpec(newSpec, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_DEF__SPEC, newSpec, newSpec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(StatementBlock newStatement, NotificationChain msgs)
  {
    StatementBlock oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_DEF__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(StatementBlock newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_DEF__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_DEF__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_DEF__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_DEF__PAR_LIST:
        return basicSetParList(null, msgs);
      case TTCN3Package.TESTCASE_DEF__SPEC:
        return basicSetSpec(null, msgs);
      case TTCN3Package.TESTCASE_DEF__STATEMENT:
        return basicSetStatement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_DEF__PAR_LIST:
        return getParList();
      case TTCN3Package.TESTCASE_DEF__SPEC:
        return getSpec();
      case TTCN3Package.TESTCASE_DEF__STATEMENT:
        return getStatement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_DEF__PAR_LIST:
        setParList((TemplateOrValueFormalParList)newValue);
        return;
      case TTCN3Package.TESTCASE_DEF__SPEC:
        setSpec((ConfigSpec)newValue);
        return;
      case TTCN3Package.TESTCASE_DEF__STATEMENT:
        setStatement((StatementBlock)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_DEF__PAR_LIST:
        setParList((TemplateOrValueFormalParList)null);
        return;
      case TTCN3Package.TESTCASE_DEF__SPEC:
        setSpec((ConfigSpec)null);
        return;
      case TTCN3Package.TESTCASE_DEF__STATEMENT:
        setStatement((StatementBlock)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_DEF__PAR_LIST:
        return parList != null;
      case TTCN3Package.TESTCASE_DEF__SPEC:
        return spec != null;
      case TTCN3Package.TESTCASE_DEF__STATEMENT:
        return statement != null;
    }
    return super.eIsSet(featureID);
  }

} //TestcaseDefImpl
