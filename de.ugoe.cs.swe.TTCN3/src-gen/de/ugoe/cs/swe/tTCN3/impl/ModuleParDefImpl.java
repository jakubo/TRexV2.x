/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ModulePar;
import de.ugoe.cs.swe.tTCN3.ModuleParDef;
import de.ugoe.cs.swe.tTCN3.MultitypedModuleParList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Module Par Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleParDefImpl#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleParDefImpl#getMultitypeParam <em>Multitype Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModuleParDefImpl extends MinimalEObjectImpl.Container implements ModuleParDef
{
  /**
   * The cached value of the '{@link #getParam() <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParam()
   * @generated
   * @ordered
   */
  protected ModulePar param;

  /**
   * The cached value of the '{@link #getMultitypeParam() <em>Multitype Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMultitypeParam()
   * @generated
   * @ordered
   */
  protected MultitypedModuleParList multitypeParam;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModuleParDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getModuleParDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModulePar getParam()
  {
    return param;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParam(ModulePar newParam, NotificationChain msgs)
  {
    ModulePar oldParam = param;
    param = newParam;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_PAR_DEF__PARAM, oldParam, newParam);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParam(ModulePar newParam)
  {
    if (newParam != param)
    {
      NotificationChain msgs = null;
      if (param != null)
        msgs = ((InternalEObject)param).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_PAR_DEF__PARAM, null, msgs);
      if (newParam != null)
        msgs = ((InternalEObject)newParam).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_PAR_DEF__PARAM, null, msgs);
      msgs = basicSetParam(newParam, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_PAR_DEF__PARAM, newParam, newParam));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultitypedModuleParList getMultitypeParam()
  {
    return multitypeParam;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMultitypeParam(MultitypedModuleParList newMultitypeParam, NotificationChain msgs)
  {
    MultitypedModuleParList oldMultitypeParam = multitypeParam;
    multitypeParam = newMultitypeParam;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM, oldMultitypeParam, newMultitypeParam);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMultitypeParam(MultitypedModuleParList newMultitypeParam)
  {
    if (newMultitypeParam != multitypeParam)
    {
      NotificationChain msgs = null;
      if (multitypeParam != null)
        msgs = ((InternalEObject)multitypeParam).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM, null, msgs);
      if (newMultitypeParam != null)
        msgs = ((InternalEObject)newMultitypeParam).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM, null, msgs);
      msgs = basicSetMultitypeParam(newMultitypeParam, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM, newMultitypeParam, newMultitypeParam));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_PAR_DEF__PARAM:
        return basicSetParam(null, msgs);
      case TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM:
        return basicSetMultitypeParam(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_PAR_DEF__PARAM:
        return getParam();
      case TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM:
        return getMultitypeParam();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_PAR_DEF__PARAM:
        setParam((ModulePar)newValue);
        return;
      case TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM:
        setMultitypeParam((MultitypedModuleParList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_PAR_DEF__PARAM:
        setParam((ModulePar)null);
        return;
      case TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM:
        setMultitypeParam((MultitypedModuleParList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_PAR_DEF__PARAM:
        return param != null;
      case TTCN3Package.MODULE_PAR_DEF__MULTITYPE_PARAM:
        return multitypeParam != null;
    }
    return super.eIsSet(featureID);
  }

} //ModuleParDefImpl
