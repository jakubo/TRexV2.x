/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.BasicStatements;
import de.ugoe.cs.swe.tTCN3.BehaviourStatements;
import de.ugoe.cs.swe.tTCN3.ControlStatement;
import de.ugoe.cs.swe.tTCN3.SUTStatements;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TimerStatements;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementImpl#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementImpl#getBasic <em>Basic</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementImpl#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementImpl#getSut <em>Sut</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlStatementImpl extends MinimalEObjectImpl.Container implements ControlStatement
{
  /**
   * The cached value of the '{@link #getTimer() <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimer()
   * @generated
   * @ordered
   */
  protected TimerStatements timer;

  /**
   * The cached value of the '{@link #getBasic() <em>Basic</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBasic()
   * @generated
   * @ordered
   */
  protected BasicStatements basic;

  /**
   * The cached value of the '{@link #getBehavior() <em>Behavior</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBehavior()
   * @generated
   * @ordered
   */
  protected BehaviourStatements behavior;

  /**
   * The cached value of the '{@link #getSut() <em>Sut</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSut()
   * @generated
   * @ordered
   */
  protected SUTStatements sut;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ControlStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getControlStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerStatements getTimer()
  {
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimer(TimerStatements newTimer, NotificationChain msgs)
  {
    TimerStatements oldTimer = timer;
    timer = newTimer;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__TIMER, oldTimer, newTimer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimer(TimerStatements newTimer)
  {
    if (newTimer != timer)
    {
      NotificationChain msgs = null;
      if (timer != null)
        msgs = ((InternalEObject)timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__TIMER, null, msgs);
      if (newTimer != null)
        msgs = ((InternalEObject)newTimer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__TIMER, null, msgs);
      msgs = basicSetTimer(newTimer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__TIMER, newTimer, newTimer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicStatements getBasic()
  {
    return basic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBasic(BasicStatements newBasic, NotificationChain msgs)
  {
    BasicStatements oldBasic = basic;
    basic = newBasic;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__BASIC, oldBasic, newBasic);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBasic(BasicStatements newBasic)
  {
    if (newBasic != basic)
    {
      NotificationChain msgs = null;
      if (basic != null)
        msgs = ((InternalEObject)basic).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__BASIC, null, msgs);
      if (newBasic != null)
        msgs = ((InternalEObject)newBasic).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__BASIC, null, msgs);
      msgs = basicSetBasic(newBasic, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__BASIC, newBasic, newBasic));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BehaviourStatements getBehavior()
  {
    return behavior;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBehavior(BehaviourStatements newBehavior, NotificationChain msgs)
  {
    BehaviourStatements oldBehavior = behavior;
    behavior = newBehavior;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__BEHAVIOR, oldBehavior, newBehavior);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBehavior(BehaviourStatements newBehavior)
  {
    if (newBehavior != behavior)
    {
      NotificationChain msgs = null;
      if (behavior != null)
        msgs = ((InternalEObject)behavior).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__BEHAVIOR, null, msgs);
      if (newBehavior != null)
        msgs = ((InternalEObject)newBehavior).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__BEHAVIOR, null, msgs);
      msgs = basicSetBehavior(newBehavior, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__BEHAVIOR, newBehavior, newBehavior));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SUTStatements getSut()
  {
    return sut;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSut(SUTStatements newSut, NotificationChain msgs)
  {
    SUTStatements oldSut = sut;
    sut = newSut;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__SUT, oldSut, newSut);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSut(SUTStatements newSut)
  {
    if (newSut != sut)
    {
      NotificationChain msgs = null;
      if (sut != null)
        msgs = ((InternalEObject)sut).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__SUT, null, msgs);
      if (newSut != null)
        msgs = ((InternalEObject)newSut).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT__SUT, null, msgs);
      msgs = basicSetSut(newSut, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT__SUT, newSut, newSut));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT__TIMER:
        return basicSetTimer(null, msgs);
      case TTCN3Package.CONTROL_STATEMENT__BASIC:
        return basicSetBasic(null, msgs);
      case TTCN3Package.CONTROL_STATEMENT__BEHAVIOR:
        return basicSetBehavior(null, msgs);
      case TTCN3Package.CONTROL_STATEMENT__SUT:
        return basicSetSut(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT__TIMER:
        return getTimer();
      case TTCN3Package.CONTROL_STATEMENT__BASIC:
        return getBasic();
      case TTCN3Package.CONTROL_STATEMENT__BEHAVIOR:
        return getBehavior();
      case TTCN3Package.CONTROL_STATEMENT__SUT:
        return getSut();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT__TIMER:
        setTimer((TimerStatements)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT__BASIC:
        setBasic((BasicStatements)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT__BEHAVIOR:
        setBehavior((BehaviourStatements)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT__SUT:
        setSut((SUTStatements)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT__TIMER:
        setTimer((TimerStatements)null);
        return;
      case TTCN3Package.CONTROL_STATEMENT__BASIC:
        setBasic((BasicStatements)null);
        return;
      case TTCN3Package.CONTROL_STATEMENT__BEHAVIOR:
        setBehavior((BehaviourStatements)null);
        return;
      case TTCN3Package.CONTROL_STATEMENT__SUT:
        setSut((SUTStatements)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT__TIMER:
        return timer != null;
      case TTCN3Package.CONTROL_STATEMENT__BASIC:
        return basic != null;
      case TTCN3Package.CONTROL_STATEMENT__BEHAVIOR:
        return behavior != null;
      case TTCN3Package.CONTROL_STATEMENT__SUT:
        return sut != null;
    }
    return super.eIsSet(featureID);
  }

} //ControlStatementImpl
