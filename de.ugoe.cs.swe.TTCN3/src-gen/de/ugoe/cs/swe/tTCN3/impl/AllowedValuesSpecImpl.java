/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllowedValuesSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateOrRange;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Allowed Values Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AllowedValuesSpecImpl#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AllowedValuesSpecImpl extends SubTypeSpecImpl implements AllowedValuesSpec
{
  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected EList<TemplateOrRange> template;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AllowedValuesSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAllowedValuesSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TemplateOrRange> getTemplate()
  {
    if (template == null)
    {
      template = new EObjectContainmentEList<TemplateOrRange>(TemplateOrRange.class, this, TTCN3Package.ALLOWED_VALUES_SPEC__TEMPLATE);
    }
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALLOWED_VALUES_SPEC__TEMPLATE:
        return ((InternalEList<?>)getTemplate()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALLOWED_VALUES_SPEC__TEMPLATE:
        return getTemplate();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALLOWED_VALUES_SPEC__TEMPLATE:
        getTemplate().clear();
        getTemplate().addAll((Collection<? extends TemplateOrRange>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALLOWED_VALUES_SPEC__TEMPLATE:
        getTemplate().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALLOWED_VALUES_SPEC__TEMPLATE:
        return template != null && !template.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //AllowedValuesSpecImpl
