/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ComponentDefList;
import de.ugoe.cs.swe.tTCN3.ComponentElementDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Def List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefListImpl#getElement <em>Element</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefListImpl#getWst <em>Wst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefListImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentDefListImpl extends MinimalEObjectImpl.Container implements ComponentDefList
{
  /**
   * The cached value of the '{@link #getElement() <em>Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElement()
   * @generated
   * @ordered
   */
  protected ComponentElementDef element;

  /**
   * The cached value of the '{@link #getWst() <em>Wst</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWst()
   * @generated
   * @ordered
   */
  protected WithStatement wst;

  /**
   * The default value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected static final String SC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected String sc = SC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentDefListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getComponentDefList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentElementDef getElement()
  {
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElement(ComponentElementDef newElement, NotificationChain msgs)
  {
    ComponentElementDef oldElement = element;
    element = newElement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_DEF_LIST__ELEMENT, oldElement, newElement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElement(ComponentElementDef newElement)
  {
    if (newElement != element)
    {
      NotificationChain msgs = null;
      if (element != null)
        msgs = ((InternalEObject)element).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_DEF_LIST__ELEMENT, null, msgs);
      if (newElement != null)
        msgs = ((InternalEObject)newElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_DEF_LIST__ELEMENT, null, msgs);
      msgs = basicSetElement(newElement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_DEF_LIST__ELEMENT, newElement, newElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement getWst()
  {
    return wst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWst(WithStatement newWst, NotificationChain msgs)
  {
    WithStatement oldWst = wst;
    wst = newWst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_DEF_LIST__WST, oldWst, newWst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWst(WithStatement newWst)
  {
    if (newWst != wst)
    {
      NotificationChain msgs = null;
      if (wst != null)
        msgs = ((InternalEObject)wst).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_DEF_LIST__WST, null, msgs);
      if (newWst != null)
        msgs = ((InternalEObject)newWst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_DEF_LIST__WST, null, msgs);
      msgs = basicSetWst(newWst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_DEF_LIST__WST, newWst, newWst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSc()
  {
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSc(String newSc)
  {
    String oldSc = sc;
    sc = newSc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_DEF_LIST__SC, oldSc, sc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF_LIST__ELEMENT:
        return basicSetElement(null, msgs);
      case TTCN3Package.COMPONENT_DEF_LIST__WST:
        return basicSetWst(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF_LIST__ELEMENT:
        return getElement();
      case TTCN3Package.COMPONENT_DEF_LIST__WST:
        return getWst();
      case TTCN3Package.COMPONENT_DEF_LIST__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF_LIST__ELEMENT:
        setElement((ComponentElementDef)newValue);
        return;
      case TTCN3Package.COMPONENT_DEF_LIST__WST:
        setWst((WithStatement)newValue);
        return;
      case TTCN3Package.COMPONENT_DEF_LIST__SC:
        setSc((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF_LIST__ELEMENT:
        setElement((ComponentElementDef)null);
        return;
      case TTCN3Package.COMPONENT_DEF_LIST__WST:
        setWst((WithStatement)null);
        return;
      case TTCN3Package.COMPONENT_DEF_LIST__SC:
        setSc(SC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF_LIST__ELEMENT:
        return element != null;
      case TTCN3Package.COMPONENT_DEF_LIST__WST:
        return wst != null;
      case TTCN3Package.COMPONENT_DEF_LIST__SC:
        return SC_EDEFAULT == null ? sc != null : !SC_EDEFAULT.equals(sc);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //ComponentDefListImpl
