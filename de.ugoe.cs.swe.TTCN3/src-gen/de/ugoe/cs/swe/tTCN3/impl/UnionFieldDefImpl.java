/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayDef;
import de.ugoe.cs.swe.tTCN3.NestedTypeDef;
import de.ugoe.cs.swe.tTCN3.SubTypeSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.Type;
import de.ugoe.cs.swe.tTCN3.UnionFieldDef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Union Field Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.UnionFieldDefImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.UnionFieldDefImpl#getNestedType <em>Nested Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.UnionFieldDefImpl#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.UnionFieldDefImpl#getSubType <em>Sub Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnionFieldDefImpl extends FieldReferenceImpl implements UnionFieldDef
{
  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * The cached value of the '{@link #getNestedType() <em>Nested Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNestedType()
   * @generated
   * @ordered
   */
  protected NestedTypeDef nestedType;

  /**
   * The cached value of the '{@link #getArray() <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArray()
   * @generated
   * @ordered
   */
  protected ArrayDef array;

  /**
   * The cached value of the '{@link #getSubType() <em>Sub Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubType()
   * @generated
   * @ordered
   */
  protected SubTypeSpec subType;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UnionFieldDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getUnionFieldDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedTypeDef getNestedType()
  {
    return nestedType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNestedType(NestedTypeDef newNestedType, NotificationChain msgs)
  {
    NestedTypeDef oldNestedType = nestedType;
    nestedType = newNestedType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE, oldNestedType, newNestedType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNestedType(NestedTypeDef newNestedType)
  {
    if (newNestedType != nestedType)
    {
      NotificationChain msgs = null;
      if (nestedType != null)
        msgs = ((InternalEObject)nestedType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE, null, msgs);
      if (newNestedType != null)
        msgs = ((InternalEObject)newNestedType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE, null, msgs);
      msgs = basicSetNestedType(newNestedType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE, newNestedType, newNestedType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayDef getArray()
  {
    return array;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetArray(ArrayDef newArray, NotificationChain msgs)
  {
    ArrayDef oldArray = array;
    array = newArray;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__ARRAY, oldArray, newArray);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArray(ArrayDef newArray)
  {
    if (newArray != array)
    {
      NotificationChain msgs = null;
      if (array != null)
        msgs = ((InternalEObject)array).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__ARRAY, null, msgs);
      if (newArray != null)
        msgs = ((InternalEObject)newArray).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__ARRAY, null, msgs);
      msgs = basicSetArray(newArray, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__ARRAY, newArray, newArray));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTypeSpec getSubType()
  {
    return subType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSubType(SubTypeSpec newSubType, NotificationChain msgs)
  {
    SubTypeSpec oldSubType = subType;
    subType = newSubType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__SUB_TYPE, oldSubType, newSubType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubType(SubTypeSpec newSubType)
  {
    if (newSubType != subType)
    {
      NotificationChain msgs = null;
      if (subType != null)
        msgs = ((InternalEObject)subType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__SUB_TYPE, null, msgs);
      if (newSubType != null)
        msgs = ((InternalEObject)newSubType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.UNION_FIELD_DEF__SUB_TYPE, null, msgs);
      msgs = basicSetSubType(newSubType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.UNION_FIELD_DEF__SUB_TYPE, newSubType, newSubType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.UNION_FIELD_DEF__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE:
        return basicSetNestedType(null, msgs);
      case TTCN3Package.UNION_FIELD_DEF__ARRAY:
        return basicSetArray(null, msgs);
      case TTCN3Package.UNION_FIELD_DEF__SUB_TYPE:
        return basicSetSubType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.UNION_FIELD_DEF__TYPE:
        return getType();
      case TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE:
        return getNestedType();
      case TTCN3Package.UNION_FIELD_DEF__ARRAY:
        return getArray();
      case TTCN3Package.UNION_FIELD_DEF__SUB_TYPE:
        return getSubType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.UNION_FIELD_DEF__TYPE:
        setType((Type)newValue);
        return;
      case TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE:
        setNestedType((NestedTypeDef)newValue);
        return;
      case TTCN3Package.UNION_FIELD_DEF__ARRAY:
        setArray((ArrayDef)newValue);
        return;
      case TTCN3Package.UNION_FIELD_DEF__SUB_TYPE:
        setSubType((SubTypeSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.UNION_FIELD_DEF__TYPE:
        setType((Type)null);
        return;
      case TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE:
        setNestedType((NestedTypeDef)null);
        return;
      case TTCN3Package.UNION_FIELD_DEF__ARRAY:
        setArray((ArrayDef)null);
        return;
      case TTCN3Package.UNION_FIELD_DEF__SUB_TYPE:
        setSubType((SubTypeSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.UNION_FIELD_DEF__TYPE:
        return type != null;
      case TTCN3Package.UNION_FIELD_DEF__NESTED_TYPE:
        return nestedType != null;
      case TTCN3Package.UNION_FIELD_DEF__ARRAY:
        return array != null;
      case TTCN3Package.UNION_FIELD_DEF__SUB_TYPE:
        return subType != null;
    }
    return super.eIsSet(featureID);
  }

} //UnionFieldDefImpl
