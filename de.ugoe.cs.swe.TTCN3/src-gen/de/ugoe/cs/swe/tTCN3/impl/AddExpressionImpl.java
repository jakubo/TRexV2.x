/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AddExpression;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Add Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AddExpressionImpl extends SingleExpressionImpl implements AddExpression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AddExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAddExpression();
  }

} //AddExpressionImpl
