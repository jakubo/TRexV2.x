/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FunctionDef;
import de.ugoe.cs.swe.tTCN3.FunctionFormalParList;
import de.ugoe.cs.swe.tTCN3.MtcSpec;
import de.ugoe.cs.swe.tTCN3.ReturnType;
import de.ugoe.cs.swe.tTCN3.RunsOnSpec;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.SystemSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getDet <em>Det</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getParameterList <em>Parameter List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getRunsOn <em>Runs On</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getMtc <em>Mtc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getSystem <em>System</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionDefImpl extends FunctionRefImpl implements FunctionDef
{
  /**
   * The default value of the '{@link #getDet() <em>Det</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDet()
   * @generated
   * @ordered
   */
  protected static final String DET_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDet() <em>Det</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDet()
   * @generated
   * @ordered
   */
  protected String det = DET_EDEFAULT;

  /**
   * The cached value of the '{@link #getParameterList() <em>Parameter List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParameterList()
   * @generated
   * @ordered
   */
  protected FunctionFormalParList parameterList;

  /**
   * The cached value of the '{@link #getRunsOn() <em>Runs On</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRunsOn()
   * @generated
   * @ordered
   */
  protected RunsOnSpec runsOn;

  /**
   * The cached value of the '{@link #getMtc() <em>Mtc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMtc()
   * @generated
   * @ordered
   */
  protected MtcSpec mtc;

  /**
   * The cached value of the '{@link #getSystem() <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystem()
   * @generated
   * @ordered
   */
  protected SystemSpec system;

  /**
   * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturnType()
   * @generated
   * @ordered
   */
  protected ReturnType returnType;

  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected StatementBlock statement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDet()
  {
    return det;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDet(String newDet)
  {
    String oldDet = det;
    det = newDet;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__DET, oldDet, det));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionFormalParList getParameterList()
  {
    return parameterList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParameterList(FunctionFormalParList newParameterList, NotificationChain msgs)
  {
    FunctionFormalParList oldParameterList = parameterList;
    parameterList = newParameterList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__PARAMETER_LIST, oldParameterList, newParameterList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParameterList(FunctionFormalParList newParameterList)
  {
    if (newParameterList != parameterList)
    {
      NotificationChain msgs = null;
      if (parameterList != null)
        msgs = ((InternalEObject)parameterList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__PARAMETER_LIST, null, msgs);
      if (newParameterList != null)
        msgs = ((InternalEObject)newParameterList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__PARAMETER_LIST, null, msgs);
      msgs = basicSetParameterList(newParameterList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__PARAMETER_LIST, newParameterList, newParameterList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunsOnSpec getRunsOn()
  {
    return runsOn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRunsOn(RunsOnSpec newRunsOn, NotificationChain msgs)
  {
    RunsOnSpec oldRunsOn = runsOn;
    runsOn = newRunsOn;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__RUNS_ON, oldRunsOn, newRunsOn);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRunsOn(RunsOnSpec newRunsOn)
  {
    if (newRunsOn != runsOn)
    {
      NotificationChain msgs = null;
      if (runsOn != null)
        msgs = ((InternalEObject)runsOn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__RUNS_ON, null, msgs);
      if (newRunsOn != null)
        msgs = ((InternalEObject)newRunsOn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__RUNS_ON, null, msgs);
      msgs = basicSetRunsOn(newRunsOn, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__RUNS_ON, newRunsOn, newRunsOn));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MtcSpec getMtc()
  {
    return mtc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMtc(MtcSpec newMtc, NotificationChain msgs)
  {
    MtcSpec oldMtc = mtc;
    mtc = newMtc;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__MTC, oldMtc, newMtc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMtc(MtcSpec newMtc)
  {
    if (newMtc != mtc)
    {
      NotificationChain msgs = null;
      if (mtc != null)
        msgs = ((InternalEObject)mtc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__MTC, null, msgs);
      if (newMtc != null)
        msgs = ((InternalEObject)newMtc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__MTC, null, msgs);
      msgs = basicSetMtc(newMtc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__MTC, newMtc, newMtc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemSpec getSystem()
  {
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSystem(SystemSpec newSystem, NotificationChain msgs)
  {
    SystemSpec oldSystem = system;
    system = newSystem;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__SYSTEM, oldSystem, newSystem);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSystem(SystemSpec newSystem)
  {
    if (newSystem != system)
    {
      NotificationChain msgs = null;
      if (system != null)
        msgs = ((InternalEObject)system).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__SYSTEM, null, msgs);
      if (newSystem != null)
        msgs = ((InternalEObject)newSystem).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__SYSTEM, null, msgs);
      msgs = basicSetSystem(newSystem, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__SYSTEM, newSystem, newSystem));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnType getReturnType()
  {
    return returnType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReturnType(ReturnType newReturnType, NotificationChain msgs)
  {
    ReturnType oldReturnType = returnType;
    returnType = newReturnType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__RETURN_TYPE, oldReturnType, newReturnType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturnType(ReturnType newReturnType)
  {
    if (newReturnType != returnType)
    {
      NotificationChain msgs = null;
      if (returnType != null)
        msgs = ((InternalEObject)returnType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__RETURN_TYPE, null, msgs);
      if (newReturnType != null)
        msgs = ((InternalEObject)newReturnType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__RETURN_TYPE, null, msgs);
      msgs = basicSetReturnType(newReturnType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__RETURN_TYPE, newReturnType, newReturnType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(StatementBlock newStatement, NotificationChain msgs)
  {
    StatementBlock oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(StatementBlock newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF__PARAMETER_LIST:
        return basicSetParameterList(null, msgs);
      case TTCN3Package.FUNCTION_DEF__RUNS_ON:
        return basicSetRunsOn(null, msgs);
      case TTCN3Package.FUNCTION_DEF__MTC:
        return basicSetMtc(null, msgs);
      case TTCN3Package.FUNCTION_DEF__SYSTEM:
        return basicSetSystem(null, msgs);
      case TTCN3Package.FUNCTION_DEF__RETURN_TYPE:
        return basicSetReturnType(null, msgs);
      case TTCN3Package.FUNCTION_DEF__STATEMENT:
        return basicSetStatement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF__DET:
        return getDet();
      case TTCN3Package.FUNCTION_DEF__PARAMETER_LIST:
        return getParameterList();
      case TTCN3Package.FUNCTION_DEF__RUNS_ON:
        return getRunsOn();
      case TTCN3Package.FUNCTION_DEF__MTC:
        return getMtc();
      case TTCN3Package.FUNCTION_DEF__SYSTEM:
        return getSystem();
      case TTCN3Package.FUNCTION_DEF__RETURN_TYPE:
        return getReturnType();
      case TTCN3Package.FUNCTION_DEF__STATEMENT:
        return getStatement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF__DET:
        setDet((String)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF__PARAMETER_LIST:
        setParameterList((FunctionFormalParList)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF__RUNS_ON:
        setRunsOn((RunsOnSpec)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF__MTC:
        setMtc((MtcSpec)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF__SYSTEM:
        setSystem((SystemSpec)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF__RETURN_TYPE:
        setReturnType((ReturnType)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF__STATEMENT:
        setStatement((StatementBlock)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF__DET:
        setDet(DET_EDEFAULT);
        return;
      case TTCN3Package.FUNCTION_DEF__PARAMETER_LIST:
        setParameterList((FunctionFormalParList)null);
        return;
      case TTCN3Package.FUNCTION_DEF__RUNS_ON:
        setRunsOn((RunsOnSpec)null);
        return;
      case TTCN3Package.FUNCTION_DEF__MTC:
        setMtc((MtcSpec)null);
        return;
      case TTCN3Package.FUNCTION_DEF__SYSTEM:
        setSystem((SystemSpec)null);
        return;
      case TTCN3Package.FUNCTION_DEF__RETURN_TYPE:
        setReturnType((ReturnType)null);
        return;
      case TTCN3Package.FUNCTION_DEF__STATEMENT:
        setStatement((StatementBlock)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF__DET:
        return DET_EDEFAULT == null ? det != null : !DET_EDEFAULT.equals(det);
      case TTCN3Package.FUNCTION_DEF__PARAMETER_LIST:
        return parameterList != null;
      case TTCN3Package.FUNCTION_DEF__RUNS_ON:
        return runsOn != null;
      case TTCN3Package.FUNCTION_DEF__MTC:
        return mtc != null;
      case TTCN3Package.FUNCTION_DEF__SYSTEM:
        return system != null;
      case TTCN3Package.FUNCTION_DEF__RETURN_TYPE:
        return returnType != null;
      case TTCN3Package.FUNCTION_DEF__STATEMENT:
        return statement != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (det: ");
    result.append(det);
    result.append(')');
    return result.toString();
  }

} //FunctionDefImpl
