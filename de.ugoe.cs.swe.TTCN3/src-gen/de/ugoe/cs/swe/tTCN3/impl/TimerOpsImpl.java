/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ReadTimerOp;
import de.ugoe.cs.swe.tTCN3.RunningTimerOp;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TimerOps;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timer Ops</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TimerOpsImpl#getRead <em>Read</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TimerOpsImpl#getRun <em>Run</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimerOpsImpl extends MinimalEObjectImpl.Container implements TimerOps
{
  /**
   * The cached value of the '{@link #getRead() <em>Read</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRead()
   * @generated
   * @ordered
   */
  protected ReadTimerOp read;

  /**
   * The cached value of the '{@link #getRun() <em>Run</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRun()
   * @generated
   * @ordered
   */
  protected RunningTimerOp run;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TimerOpsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTimerOps();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReadTimerOp getRead()
  {
    return read;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRead(ReadTimerOp newRead, NotificationChain msgs)
  {
    ReadTimerOp oldRead = read;
    read = newRead;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TIMER_OPS__READ, oldRead, newRead);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRead(ReadTimerOp newRead)
  {
    if (newRead != read)
    {
      NotificationChain msgs = null;
      if (read != null)
        msgs = ((InternalEObject)read).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TIMER_OPS__READ, null, msgs);
      if (newRead != null)
        msgs = ((InternalEObject)newRead).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TIMER_OPS__READ, null, msgs);
      msgs = basicSetRead(newRead, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TIMER_OPS__READ, newRead, newRead));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunningTimerOp getRun()
  {
    return run;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRun(RunningTimerOp newRun, NotificationChain msgs)
  {
    RunningTimerOp oldRun = run;
    run = newRun;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TIMER_OPS__RUN, oldRun, newRun);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRun(RunningTimerOp newRun)
  {
    if (newRun != run)
    {
      NotificationChain msgs = null;
      if (run != null)
        msgs = ((InternalEObject)run).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TIMER_OPS__RUN, null, msgs);
      if (newRun != null)
        msgs = ((InternalEObject)newRun).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TIMER_OPS__RUN, null, msgs);
      msgs = basicSetRun(newRun, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TIMER_OPS__RUN, newRun, newRun));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_OPS__READ:
        return basicSetRead(null, msgs);
      case TTCN3Package.TIMER_OPS__RUN:
        return basicSetRun(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_OPS__READ:
        return getRead();
      case TTCN3Package.TIMER_OPS__RUN:
        return getRun();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_OPS__READ:
        setRead((ReadTimerOp)newValue);
        return;
      case TTCN3Package.TIMER_OPS__RUN:
        setRun((RunningTimerOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_OPS__READ:
        setRead((ReadTimerOp)null);
        return;
      case TTCN3Package.TIMER_OPS__RUN:
        setRun((RunningTimerOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_OPS__READ:
        return read != null;
      case TTCN3Package.TIMER_OPS__RUN:
        return run != null;
    }
    return super.eIsSet(featureID);
  }

} //TimerOpsImpl
