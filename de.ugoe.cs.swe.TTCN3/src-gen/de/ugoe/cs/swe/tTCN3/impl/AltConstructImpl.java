/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AltConstruct;
import de.ugoe.cs.swe.tTCN3.AltGuardList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alt Construct</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltConstructImpl#getAgList <em>Ag List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AltConstructImpl extends MinimalEObjectImpl.Container implements AltConstruct
{
  /**
   * The cached value of the '{@link #getAgList() <em>Ag List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAgList()
   * @generated
   * @ordered
   */
  protected AltGuardList agList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AltConstructImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAltConstruct();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltGuardList getAgList()
  {
    return agList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAgList(AltGuardList newAgList, NotificationChain msgs)
  {
    AltGuardList oldAgList = agList;
    agList = newAgList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALT_CONSTRUCT__AG_LIST, oldAgList, newAgList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAgList(AltGuardList newAgList)
  {
    if (newAgList != agList)
    {
      NotificationChain msgs = null;
      if (agList != null)
        msgs = ((InternalEObject)agList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALT_CONSTRUCT__AG_LIST, null, msgs);
      if (newAgList != null)
        msgs = ((InternalEObject)newAgList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALT_CONSTRUCT__AG_LIST, null, msgs);
      msgs = basicSetAgList(newAgList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALT_CONSTRUCT__AG_LIST, newAgList, newAgList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_CONSTRUCT__AG_LIST:
        return basicSetAgList(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_CONSTRUCT__AG_LIST:
        return getAgList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_CONSTRUCT__AG_LIST:
        setAgList((AltGuardList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_CONSTRUCT__AG_LIST:
        setAgList((AltGuardList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_CONSTRUCT__AG_LIST:
        return agList != null;
    }
    return super.eIsSet(featureID);
  }

} //AltConstructImpl
