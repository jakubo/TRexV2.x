/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference;
import de.ugoe.cs.swe.tTCN3.ComponentRef;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentRefImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentRefImpl#getSystem <em>System</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentRefImpl#getSelf <em>Self</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentRefImpl#getMtc <em>Mtc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentRefImpl extends MinimalEObjectImpl.Container implements ComponentRef
{
  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected ComponentOrDefaultReference ref;

  /**
   * The default value of the '{@link #getSystem() <em>System</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystem()
   * @generated
   * @ordered
   */
  protected static final String SYSTEM_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSystem() <em>System</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystem()
   * @generated
   * @ordered
   */
  protected String system = SYSTEM_EDEFAULT;

  /**
   * The default value of the '{@link #getSelf() <em>Self</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSelf()
   * @generated
   * @ordered
   */
  protected static final String SELF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSelf() <em>Self</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSelf()
   * @generated
   * @ordered
   */
  protected String self = SELF_EDEFAULT;

  /**
   * The default value of the '{@link #getMtc() <em>Mtc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMtc()
   * @generated
   * @ordered
   */
  protected static final String MTC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMtc() <em>Mtc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMtc()
   * @generated
   * @ordered
   */
  protected String mtc = MTC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentRefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getComponentRef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentOrDefaultReference getRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRef(ComponentOrDefaultReference newRef, NotificationChain msgs)
  {
    ComponentOrDefaultReference oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_REF__REF, oldRef, newRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRef(ComponentOrDefaultReference newRef)
  {
    if (newRef != ref)
    {
      NotificationChain msgs = null;
      if (ref != null)
        msgs = ((InternalEObject)ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_REF__REF, null, msgs);
      if (newRef != null)
        msgs = ((InternalEObject)newRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_REF__REF, null, msgs);
      msgs = basicSetRef(newRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_REF__REF, newRef, newRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSystem()
  {
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSystem(String newSystem)
  {
    String oldSystem = system;
    system = newSystem;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_REF__SYSTEM, oldSystem, system));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSelf()
  {
    return self;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSelf(String newSelf)
  {
    String oldSelf = self;
    self = newSelf;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_REF__SELF, oldSelf, self));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMtc()
  {
    return mtc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMtc(String newMtc)
  {
    String oldMtc = mtc;
    mtc = newMtc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_REF__MTC, oldMtc, mtc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_REF__REF:
        return basicSetRef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_REF__REF:
        return getRef();
      case TTCN3Package.COMPONENT_REF__SYSTEM:
        return getSystem();
      case TTCN3Package.COMPONENT_REF__SELF:
        return getSelf();
      case TTCN3Package.COMPONENT_REF__MTC:
        return getMtc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_REF__REF:
        setRef((ComponentOrDefaultReference)newValue);
        return;
      case TTCN3Package.COMPONENT_REF__SYSTEM:
        setSystem((String)newValue);
        return;
      case TTCN3Package.COMPONENT_REF__SELF:
        setSelf((String)newValue);
        return;
      case TTCN3Package.COMPONENT_REF__MTC:
        setMtc((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_REF__REF:
        setRef((ComponentOrDefaultReference)null);
        return;
      case TTCN3Package.COMPONENT_REF__SYSTEM:
        setSystem(SYSTEM_EDEFAULT);
        return;
      case TTCN3Package.COMPONENT_REF__SELF:
        setSelf(SELF_EDEFAULT);
        return;
      case TTCN3Package.COMPONENT_REF__MTC:
        setMtc(MTC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_REF__REF:
        return ref != null;
      case TTCN3Package.COMPONENT_REF__SYSTEM:
        return SYSTEM_EDEFAULT == null ? system != null : !SYSTEM_EDEFAULT.equals(system);
      case TTCN3Package.COMPONENT_REF__SELF:
        return SELF_EDEFAULT == null ? self != null : !SELF_EDEFAULT.equals(self);
      case TTCN3Package.COMPONENT_REF__MTC:
        return MTC_EDEFAULT == null ? mtc != null : !MTC_EDEFAULT.equals(mtc);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (system: ");
    result.append(system);
    result.append(", self: ");
    result.append(self);
    result.append(", mtc: ");
    result.append(mtc);
    result.append(')');
    return result.toString();
  }

} //ComponentRefImpl
