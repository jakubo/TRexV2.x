/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AltGuardList;
import de.ugoe.cs.swe.tTCN3.ElseStatement;
import de.ugoe.cs.swe.tTCN3.GuardStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alt Guard List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltGuardListImpl#getGuardList <em>Guard List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltGuardListImpl#getElseList <em>Else List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AltGuardListImpl extends MinimalEObjectImpl.Container implements AltGuardList
{
  /**
   * The cached value of the '{@link #getGuardList() <em>Guard List</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGuardList()
   * @generated
   * @ordered
   */
  protected EList<GuardStatement> guardList;

  /**
   * The cached value of the '{@link #getElseList() <em>Else List</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElseList()
   * @generated
   * @ordered
   */
  protected EList<ElseStatement> elseList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AltGuardListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAltGuardList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<GuardStatement> getGuardList()
  {
    if (guardList == null)
    {
      guardList = new EObjectContainmentEList<GuardStatement>(GuardStatement.class, this, TTCN3Package.ALT_GUARD_LIST__GUARD_LIST);
    }
    return guardList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ElseStatement> getElseList()
  {
    if (elseList == null)
    {
      elseList = new EObjectContainmentEList<ElseStatement>(ElseStatement.class, this, TTCN3Package.ALT_GUARD_LIST__ELSE_LIST);
    }
    return elseList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_GUARD_LIST__GUARD_LIST:
        return ((InternalEList<?>)getGuardList()).basicRemove(otherEnd, msgs);
      case TTCN3Package.ALT_GUARD_LIST__ELSE_LIST:
        return ((InternalEList<?>)getElseList()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_GUARD_LIST__GUARD_LIST:
        return getGuardList();
      case TTCN3Package.ALT_GUARD_LIST__ELSE_LIST:
        return getElseList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_GUARD_LIST__GUARD_LIST:
        getGuardList().clear();
        getGuardList().addAll((Collection<? extends GuardStatement>)newValue);
        return;
      case TTCN3Package.ALT_GUARD_LIST__ELSE_LIST:
        getElseList().clear();
        getElseList().addAll((Collection<? extends ElseStatement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_GUARD_LIST__GUARD_LIST:
        getGuardList().clear();
        return;
      case TTCN3Package.ALT_GUARD_LIST__ELSE_LIST:
        getElseList().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALT_GUARD_LIST__GUARD_LIST:
        return guardList != null && !guardList.isEmpty();
      case TTCN3Package.ALT_GUARD_LIST__ELSE_LIST:
        return elseList != null && !elseList.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //AltGuardListImpl
