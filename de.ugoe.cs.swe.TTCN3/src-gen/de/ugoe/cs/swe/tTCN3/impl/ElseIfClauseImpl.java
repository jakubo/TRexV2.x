/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.BooleanExpression;
import de.ugoe.cs.swe.tTCN3.ElseIfClause;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Else If Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ElseIfClauseImpl#getKeyElse <em>Key Else</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ElseIfClauseImpl#getKeyIf <em>Key If</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ElseIfClauseImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ElseIfClauseImpl#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElseIfClauseImpl extends MinimalEObjectImpl.Container implements ElseIfClause
{
  /**
   * The default value of the '{@link #getKeyElse() <em>Key Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyElse()
   * @generated
   * @ordered
   */
  protected static final String KEY_ELSE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getKeyElse() <em>Key Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyElse()
   * @generated
   * @ordered
   */
  protected String keyElse = KEY_ELSE_EDEFAULT;

  /**
   * The default value of the '{@link #getKeyIf() <em>Key If</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyIf()
   * @generated
   * @ordered
   */
  protected static final String KEY_IF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getKeyIf() <em>Key If</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyIf()
   * @generated
   * @ordered
   */
  protected String keyIf = KEY_IF_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression()
   * @generated
   * @ordered
   */
  protected BooleanExpression expression;

  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected StatementBlock statement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ElseIfClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getElseIfClause();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getKeyElse()
  {
    return keyElse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKeyElse(String newKeyElse)
  {
    String oldKeyElse = keyElse;
    keyElse = newKeyElse;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ELSE_IF_CLAUSE__KEY_ELSE, oldKeyElse, keyElse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getKeyIf()
  {
    return keyIf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKeyIf(String newKeyIf)
  {
    String oldKeyIf = keyIf;
    keyIf = newKeyIf;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ELSE_IF_CLAUSE__KEY_IF, oldKeyIf, keyIf));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanExpression getExpression()
  {
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpression(BooleanExpression newExpression, NotificationChain msgs)
  {
    BooleanExpression oldExpression = expression;
    expression = newExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION, oldExpression, newExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression(BooleanExpression newExpression)
  {
    if (newExpression != expression)
    {
      NotificationChain msgs = null;
      if (expression != null)
        msgs = ((InternalEObject)expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION, null, msgs);
      if (newExpression != null)
        msgs = ((InternalEObject)newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION, null, msgs);
      msgs = basicSetExpression(newExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION, newExpression, newExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(StatementBlock newStatement, NotificationChain msgs)
  {
    StatementBlock oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ELSE_IF_CLAUSE__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(StatementBlock newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ELSE_IF_CLAUSE__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ELSE_IF_CLAUSE__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ELSE_IF_CLAUSE__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION:
        return basicSetExpression(null, msgs);
      case TTCN3Package.ELSE_IF_CLAUSE__STATEMENT:
        return basicSetStatement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_ELSE:
        return getKeyElse();
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_IF:
        return getKeyIf();
      case TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION:
        return getExpression();
      case TTCN3Package.ELSE_IF_CLAUSE__STATEMENT:
        return getStatement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_ELSE:
        setKeyElse((String)newValue);
        return;
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_IF:
        setKeyIf((String)newValue);
        return;
      case TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION:
        setExpression((BooleanExpression)newValue);
        return;
      case TTCN3Package.ELSE_IF_CLAUSE__STATEMENT:
        setStatement((StatementBlock)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_ELSE:
        setKeyElse(KEY_ELSE_EDEFAULT);
        return;
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_IF:
        setKeyIf(KEY_IF_EDEFAULT);
        return;
      case TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION:
        setExpression((BooleanExpression)null);
        return;
      case TTCN3Package.ELSE_IF_CLAUSE__STATEMENT:
        setStatement((StatementBlock)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_ELSE:
        return KEY_ELSE_EDEFAULT == null ? keyElse != null : !KEY_ELSE_EDEFAULT.equals(keyElse);
      case TTCN3Package.ELSE_IF_CLAUSE__KEY_IF:
        return KEY_IF_EDEFAULT == null ? keyIf != null : !KEY_IF_EDEFAULT.equals(keyIf);
      case TTCN3Package.ELSE_IF_CLAUSE__EXPRESSION:
        return expression != null;
      case TTCN3Package.ELSE_IF_CLAUSE__STATEMENT:
        return statement != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (keyElse: ");
    result.append(keyElse);
    result.append(", keyIf: ");
    result.append(keyIf);
    result.append(')');
    return result.toString();
  }

} //ElseIfClauseImpl
