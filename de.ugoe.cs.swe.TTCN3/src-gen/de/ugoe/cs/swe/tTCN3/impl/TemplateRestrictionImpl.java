/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateRestriction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Restriction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateRestrictionImpl#getOmit <em>Omit</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateRestrictionImpl#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateRestrictionImpl#getPresent <em>Present</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateRestrictionImpl extends MinimalEObjectImpl.Container implements TemplateRestriction
{
  /**
   * The default value of the '{@link #getOmit() <em>Omit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOmit()
   * @generated
   * @ordered
   */
  protected static final String OMIT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOmit() <em>Omit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOmit()
   * @generated
   * @ordered
   */
  protected String omit = OMIT_EDEFAULT;

  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final String VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected String value = VALUE_EDEFAULT;

  /**
   * The default value of the '{@link #getPresent() <em>Present</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPresent()
   * @generated
   * @ordered
   */
  protected static final String PRESENT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPresent() <em>Present</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPresent()
   * @generated
   * @ordered
   */
  protected String present = PRESENT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateRestrictionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTemplateRestriction();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOmit()
  {
    return omit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOmit(String newOmit)
  {
    String oldOmit = omit;
    omit = newOmit;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_RESTRICTION__OMIT, oldOmit, omit));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(String newValue)
  {
    String oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_RESTRICTION__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPresent()
  {
    return present;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPresent(String newPresent)
  {
    String oldPresent = present;
    present = newPresent;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_RESTRICTION__PRESENT, oldPresent, present));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_RESTRICTION__OMIT:
        return getOmit();
      case TTCN3Package.TEMPLATE_RESTRICTION__VALUE:
        return getValue();
      case TTCN3Package.TEMPLATE_RESTRICTION__PRESENT:
        return getPresent();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_RESTRICTION__OMIT:
        setOmit((String)newValue);
        return;
      case TTCN3Package.TEMPLATE_RESTRICTION__VALUE:
        setValue((String)newValue);
        return;
      case TTCN3Package.TEMPLATE_RESTRICTION__PRESENT:
        setPresent((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_RESTRICTION__OMIT:
        setOmit(OMIT_EDEFAULT);
        return;
      case TTCN3Package.TEMPLATE_RESTRICTION__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case TTCN3Package.TEMPLATE_RESTRICTION__PRESENT:
        setPresent(PRESENT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_RESTRICTION__OMIT:
        return OMIT_EDEFAULT == null ? omit != null : !OMIT_EDEFAULT.equals(omit);
      case TTCN3Package.TEMPLATE_RESTRICTION__VALUE:
        return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
      case TTCN3Package.TEMPLATE_RESTRICTION__PRESENT:
        return PRESENT_EDEFAULT == null ? present != null : !PRESENT_EDEFAULT.equals(present);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (omit: ");
    result.append(omit);
    result.append(", value: ");
    result.append(value);
    result.append(", present: ");
    result.append(present);
    result.append(')');
    return result.toString();
  }

} //TemplateRestrictionImpl
