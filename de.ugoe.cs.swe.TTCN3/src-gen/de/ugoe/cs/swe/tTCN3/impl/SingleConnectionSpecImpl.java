/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.PortRef;
import de.ugoe.cs.swe.tTCN3.SingleConnectionSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Connection Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleConnectionSpecImpl#getPort1 <em>Port1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleConnectionSpecImpl#getPort2 <em>Port2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleConnectionSpecImpl extends MinimalEObjectImpl.Container implements SingleConnectionSpec
{
  /**
   * The cached value of the '{@link #getPort1() <em>Port1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort1()
   * @generated
   * @ordered
   */
  protected PortRef port1;

  /**
   * The cached value of the '{@link #getPort2() <em>Port2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort2()
   * @generated
   * @ordered
   */
  protected PortRef port2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SingleConnectionSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSingleConnectionSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRef getPort1()
  {
    return port1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPort1(PortRef newPort1, NotificationChain msgs)
  {
    PortRef oldPort1 = port1;
    port1 = newPort1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1, oldPort1, newPort1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort1(PortRef newPort1)
  {
    if (newPort1 != port1)
    {
      NotificationChain msgs = null;
      if (port1 != null)
        msgs = ((InternalEObject)port1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1, null, msgs);
      if (newPort1 != null)
        msgs = ((InternalEObject)newPort1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1, null, msgs);
      msgs = basicSetPort1(newPort1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1, newPort1, newPort1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRef getPort2()
  {
    return port2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPort2(PortRef newPort2, NotificationChain msgs)
  {
    PortRef oldPort2 = port2;
    port2 = newPort2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2, oldPort2, newPort2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort2(PortRef newPort2)
  {
    if (newPort2 != port2)
    {
      NotificationChain msgs = null;
      if (port2 != null)
        msgs = ((InternalEObject)port2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2, null, msgs);
      if (newPort2 != null)
        msgs = ((InternalEObject)newPort2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2, null, msgs);
      msgs = basicSetPort2(newPort2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2, newPort2, newPort2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1:
        return basicSetPort1(null, msgs);
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2:
        return basicSetPort2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1:
        return getPort1();
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2:
        return getPort2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1:
        setPort1((PortRef)newValue);
        return;
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2:
        setPort2((PortRef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1:
        setPort1((PortRef)null);
        return;
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2:
        setPort2((PortRef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT1:
        return port1 != null;
      case TTCN3Package.SINGLE_CONNECTION_SPEC__PORT2:
        return port2 != null;
    }
    return super.eIsSet(featureID);
  }

} //SingleConnectionSpecImpl
