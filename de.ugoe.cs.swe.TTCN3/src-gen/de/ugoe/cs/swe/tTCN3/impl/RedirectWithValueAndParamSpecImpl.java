/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.IndexSpec;
import de.ugoe.cs.swe.tTCN3.ParamSpec;
import de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec;
import de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec;
import de.ugoe.cs.swe.tTCN3.SenderSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.ValueSpec;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Redirect With Value And Param Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl#getIndex <em>Index</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RedirectWithValueAndParamSpecImpl extends PortRedirectWithValueAndParamImpl implements RedirectWithValueAndParamSpec
{
  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected ValueSpec value;

  /**
   * The cached value of the '{@link #getParam() <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParam()
   * @generated
   * @ordered
   */
  protected ParamSpec param;

  /**
   * The cached value of the '{@link #getSender() <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSender()
   * @generated
   * @ordered
   */
  protected SenderSpec sender;

  /**
   * The cached value of the '{@link #getIndex() <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIndex()
   * @generated
   * @ordered
   */
  protected IndexSpec index;

  /**
   * The cached value of the '{@link #getRedirect() <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRedirect()
   * @generated
   * @ordered
   */
  protected RedirectWithParamSpec redirect;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RedirectWithValueAndParamSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getRedirectWithValueAndParamSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueSpec getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetValue(ValueSpec newValue, NotificationChain msgs)
  {
    ValueSpec oldValue = value;
    value = newValue;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE, oldValue, newValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(ValueSpec newValue)
  {
    if (newValue != value)
    {
      NotificationChain msgs = null;
      if (value != null)
        msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE, null, msgs);
      if (newValue != null)
        msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE, null, msgs);
      msgs = basicSetValue(newValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE, newValue, newValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParamSpec getParam()
  {
    return param;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParam(ParamSpec newParam, NotificationChain msgs)
  {
    ParamSpec oldParam = param;
    param = newParam;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM, oldParam, newParam);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParam(ParamSpec newParam)
  {
    if (newParam != param)
    {
      NotificationChain msgs = null;
      if (param != null)
        msgs = ((InternalEObject)param).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM, null, msgs);
      if (newParam != null)
        msgs = ((InternalEObject)newParam).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM, null, msgs);
      msgs = basicSetParam(newParam, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM, newParam, newParam));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SenderSpec getSender()
  {
    return sender;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSender(SenderSpec newSender, NotificationChain msgs)
  {
    SenderSpec oldSender = sender;
    sender = newSender;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER, oldSender, newSender);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSender(SenderSpec newSender)
  {
    if (newSender != sender)
    {
      NotificationChain msgs = null;
      if (sender != null)
        msgs = ((InternalEObject)sender).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER, null, msgs);
      if (newSender != null)
        msgs = ((InternalEObject)newSender).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER, null, msgs);
      msgs = basicSetSender(newSender, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER, newSender, newSender));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IndexSpec getIndex()
  {
    return index;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIndex(IndexSpec newIndex, NotificationChain msgs)
  {
    IndexSpec oldIndex = index;
    index = newIndex;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX, oldIndex, newIndex);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIndex(IndexSpec newIndex)
  {
    if (newIndex != index)
    {
      NotificationChain msgs = null;
      if (index != null)
        msgs = ((InternalEObject)index).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX, null, msgs);
      if (newIndex != null)
        msgs = ((InternalEObject)newIndex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX, null, msgs);
      msgs = basicSetIndex(newIndex, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX, newIndex, newIndex));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RedirectWithParamSpec getRedirect()
  {
    return redirect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRedirect(RedirectWithParamSpec newRedirect, NotificationChain msgs)
  {
    RedirectWithParamSpec oldRedirect = redirect;
    redirect = newRedirect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT, oldRedirect, newRedirect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRedirect(RedirectWithParamSpec newRedirect)
  {
    if (newRedirect != redirect)
    {
      NotificationChain msgs = null;
      if (redirect != null)
        msgs = ((InternalEObject)redirect).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT, null, msgs);
      if (newRedirect != null)
        msgs = ((InternalEObject)newRedirect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT, null, msgs);
      msgs = basicSetRedirect(newRedirect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT, newRedirect, newRedirect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE:
        return basicSetValue(null, msgs);
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM:
        return basicSetParam(null, msgs);
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER:
        return basicSetSender(null, msgs);
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX:
        return basicSetIndex(null, msgs);
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT:
        return basicSetRedirect(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE:
        return getValue();
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM:
        return getParam();
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER:
        return getSender();
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX:
        return getIndex();
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT:
        return getRedirect();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE:
        setValue((ValueSpec)newValue);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM:
        setParam((ParamSpec)newValue);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER:
        setSender((SenderSpec)newValue);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX:
        setIndex((IndexSpec)newValue);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT:
        setRedirect((RedirectWithParamSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE:
        setValue((ValueSpec)null);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM:
        setParam((ParamSpec)null);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER:
        setSender((SenderSpec)null);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX:
        setIndex((IndexSpec)null);
        return;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT:
        setRedirect((RedirectWithParamSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE:
        return value != null;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM:
        return param != null;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER:
        return sender != null;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX:
        return index != null;
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT:
        return redirect != null;
    }
    return super.eIsSet(featureID);
  }

} //RedirectWithValueAndParamSpecImpl
