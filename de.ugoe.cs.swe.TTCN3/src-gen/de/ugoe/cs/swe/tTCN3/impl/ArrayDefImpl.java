/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayDef;
import de.ugoe.cs.swe.tTCN3.SingleExpression;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ArrayDefImpl#getExpr <em>Expr</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ArrayDefImpl#getRange <em>Range</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArrayDefImpl extends MinimalEObjectImpl.Container implements ArrayDef
{
  /**
   * The cached value of the '{@link #getExpr() <em>Expr</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpr()
   * @generated
   * @ordered
   */
  protected EList<SingleExpression> expr;

  /**
   * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRange()
   * @generated
   * @ordered
   */
  protected EList<SingleExpression> range;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ArrayDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getArrayDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SingleExpression> getExpr()
  {
    if (expr == null)
    {
      expr = new EObjectContainmentEList<SingleExpression>(SingleExpression.class, this, TTCN3Package.ARRAY_DEF__EXPR);
    }
    return expr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SingleExpression> getRange()
  {
    if (range == null)
    {
      range = new EObjectContainmentEList<SingleExpression>(SingleExpression.class, this, TTCN3Package.ARRAY_DEF__RANGE);
    }
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ARRAY_DEF__EXPR:
        return ((InternalEList<?>)getExpr()).basicRemove(otherEnd, msgs);
      case TTCN3Package.ARRAY_DEF__RANGE:
        return ((InternalEList<?>)getRange()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ARRAY_DEF__EXPR:
        return getExpr();
      case TTCN3Package.ARRAY_DEF__RANGE:
        return getRange();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ARRAY_DEF__EXPR:
        getExpr().clear();
        getExpr().addAll((Collection<? extends SingleExpression>)newValue);
        return;
      case TTCN3Package.ARRAY_DEF__RANGE:
        getRange().clear();
        getRange().addAll((Collection<? extends SingleExpression>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ARRAY_DEF__EXPR:
        getExpr().clear();
        return;
      case TTCN3Package.ARRAY_DEF__RANGE:
        getRange().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ARRAY_DEF__EXPR:
        return expr != null && !expr.isEmpty();
      case TTCN3Package.ARRAY_DEF__RANGE:
        return range != null && !range.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ArrayDefImpl
