/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllOrSignatureList;
import de.ugoe.cs.swe.tTCN3.ProcedureList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ProcedureListImpl#getAllOrSigList <em>All Or Sig List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureListImpl extends MinimalEObjectImpl.Container implements ProcedureList
{
  /**
   * The cached value of the '{@link #getAllOrSigList() <em>All Or Sig List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAllOrSigList()
   * @generated
   * @ordered
   */
  protected AllOrSignatureList allOrSigList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProcedureListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getProcedureList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllOrSignatureList getAllOrSigList()
  {
    return allOrSigList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAllOrSigList(AllOrSignatureList newAllOrSigList, NotificationChain msgs)
  {
    AllOrSignatureList oldAllOrSigList = allOrSigList;
    allOrSigList = newAllOrSigList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST, oldAllOrSigList, newAllOrSigList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAllOrSigList(AllOrSignatureList newAllOrSigList)
  {
    if (newAllOrSigList != allOrSigList)
    {
      NotificationChain msgs = null;
      if (allOrSigList != null)
        msgs = ((InternalEObject)allOrSigList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST, null, msgs);
      if (newAllOrSigList != null)
        msgs = ((InternalEObject)newAllOrSigList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST, null, msgs);
      msgs = basicSetAllOrSigList(newAllOrSigList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST, newAllOrSigList, newAllOrSigList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST:
        return basicSetAllOrSigList(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST:
        return getAllOrSigList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST:
        setAllOrSigList((AllOrSignatureList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST:
        setAllOrSigList((AllOrSignatureList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PROCEDURE_LIST__ALL_OR_SIG_LIST:
        return allOrSigList != null;
    }
    return super.eIsSet(featureID);
  }

} //ProcedureListImpl
