/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ComponentOrAny;
import de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.VariableRef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Or Any</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentOrAnyImpl#getCompOrDefault <em>Comp Or Default</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentOrAnyImpl#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentOrAnyImpl extends MinimalEObjectImpl.Container implements ComponentOrAny
{
  /**
   * The cached value of the '{@link #getCompOrDefault() <em>Comp Or Default</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCompOrDefault()
   * @generated
   * @ordered
   */
  protected ComponentOrDefaultReference compOrDefault;

  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected VariableRef ref;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentOrAnyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getComponentOrAny();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentOrDefaultReference getCompOrDefault()
  {
    return compOrDefault;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCompOrDefault(ComponentOrDefaultReference newCompOrDefault, NotificationChain msgs)
  {
    ComponentOrDefaultReference oldCompOrDefault = compOrDefault;
    compOrDefault = newCompOrDefault;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT, oldCompOrDefault, newCompOrDefault);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCompOrDefault(ComponentOrDefaultReference newCompOrDefault)
  {
    if (newCompOrDefault != compOrDefault)
    {
      NotificationChain msgs = null;
      if (compOrDefault != null)
        msgs = ((InternalEObject)compOrDefault).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT, null, msgs);
      if (newCompOrDefault != null)
        msgs = ((InternalEObject)newCompOrDefault).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT, null, msgs);
      msgs = basicSetCompOrDefault(newCompOrDefault, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT, newCompOrDefault, newCompOrDefault));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableRef getRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRef(VariableRef newRef, NotificationChain msgs)
  {
    VariableRef oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_OR_ANY__REF, oldRef, newRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRef(VariableRef newRef)
  {
    if (newRef != ref)
    {
      NotificationChain msgs = null;
      if (ref != null)
        msgs = ((InternalEObject)ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_OR_ANY__REF, null, msgs);
      if (newRef != null)
        msgs = ((InternalEObject)newRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMPONENT_OR_ANY__REF, null, msgs);
      msgs = basicSetRef(newRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMPONENT_OR_ANY__REF, newRef, newRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT:
        return basicSetCompOrDefault(null, msgs);
      case TTCN3Package.COMPONENT_OR_ANY__REF:
        return basicSetRef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT:
        return getCompOrDefault();
      case TTCN3Package.COMPONENT_OR_ANY__REF:
        return getRef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT:
        setCompOrDefault((ComponentOrDefaultReference)newValue);
        return;
      case TTCN3Package.COMPONENT_OR_ANY__REF:
        setRef((VariableRef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT:
        setCompOrDefault((ComponentOrDefaultReference)null);
        return;
      case TTCN3Package.COMPONENT_OR_ANY__REF:
        setRef((VariableRef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_OR_ANY__COMP_OR_DEFAULT:
        return compOrDefault != null;
      case TTCN3Package.COMPONENT_OR_ANY__REF:
        return ref != null;
    }
    return super.eIsSet(featureID);
  }

} //ComponentOrAnyImpl
