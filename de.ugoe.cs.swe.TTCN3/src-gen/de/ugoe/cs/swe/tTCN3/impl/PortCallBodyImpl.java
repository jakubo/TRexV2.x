/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.CallBodyStatementList;
import de.ugoe.cs.swe.tTCN3.PortCallBody;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Call Body</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortCallBodyImpl#getCallBody <em>Call Body</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortCallBodyImpl extends MinimalEObjectImpl.Container implements PortCallBody
{
  /**
   * The cached value of the '{@link #getCallBody() <em>Call Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCallBody()
   * @generated
   * @ordered
   */
  protected CallBodyStatementList callBody;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortCallBodyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortCallBody();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallBodyStatementList getCallBody()
  {
    return callBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCallBody(CallBodyStatementList newCallBody, NotificationChain msgs)
  {
    CallBodyStatementList oldCallBody = callBody;
    callBody = newCallBody;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_CALL_BODY__CALL_BODY, oldCallBody, newCallBody);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCallBody(CallBodyStatementList newCallBody)
  {
    if (newCallBody != callBody)
    {
      NotificationChain msgs = null;
      if (callBody != null)
        msgs = ((InternalEObject)callBody).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_CALL_BODY__CALL_BODY, null, msgs);
      if (newCallBody != null)
        msgs = ((InternalEObject)newCallBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_CALL_BODY__CALL_BODY, null, msgs);
      msgs = basicSetCallBody(newCallBody, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_CALL_BODY__CALL_BODY, newCallBody, newCallBody));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CALL_BODY__CALL_BODY:
        return basicSetCallBody(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CALL_BODY__CALL_BODY:
        return getCallBody();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CALL_BODY__CALL_BODY:
        setCallBody((CallBodyStatementList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CALL_BODY__CALL_BODY:
        setCallBody((CallBodyStatementList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CALL_BODY__CALL_BODY:
        return callBody != null;
    }
    return super.eIsSet(featureID);
  }

} //PortCallBodyImpl
