/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FunctionDefList;
import de.ugoe.cs.swe.tTCN3.FunctionLocalDef;
import de.ugoe.cs.swe.tTCN3.FunctionLocalInst;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Def List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefListImpl#getLocDef <em>Loc Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefListImpl#getLocInst <em>Loc Inst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefListImpl#getWs <em>Ws</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefListImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionDefListImpl extends MinimalEObjectImpl.Container implements FunctionDefList
{
  /**
   * The cached value of the '{@link #getLocDef() <em>Loc Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocDef()
   * @generated
   * @ordered
   */
  protected FunctionLocalDef locDef;

  /**
   * The cached value of the '{@link #getLocInst() <em>Loc Inst</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocInst()
   * @generated
   * @ordered
   */
  protected FunctionLocalInst locInst;

  /**
   * The cached value of the '{@link #getWs() <em>Ws</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWs()
   * @generated
   * @ordered
   */
  protected WithStatement ws;

  /**
   * The default value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected static final String SC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected String sc = SC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionDefListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionDefList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionLocalDef getLocDef()
  {
    return locDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLocDef(FunctionLocalDef newLocDef, NotificationChain msgs)
  {
    FunctionLocalDef oldLocDef = locDef;
    locDef = newLocDef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF, oldLocDef, newLocDef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocDef(FunctionLocalDef newLocDef)
  {
    if (newLocDef != locDef)
    {
      NotificationChain msgs = null;
      if (locDef != null)
        msgs = ((InternalEObject)locDef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF, null, msgs);
      if (newLocDef != null)
        msgs = ((InternalEObject)newLocDef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF, null, msgs);
      msgs = basicSetLocDef(newLocDef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF, newLocDef, newLocDef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionLocalInst getLocInst()
  {
    return locInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLocInst(FunctionLocalInst newLocInst, NotificationChain msgs)
  {
    FunctionLocalInst oldLocInst = locInst;
    locInst = newLocInst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__LOC_INST, oldLocInst, newLocInst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocInst(FunctionLocalInst newLocInst)
  {
    if (newLocInst != locInst)
    {
      NotificationChain msgs = null;
      if (locInst != null)
        msgs = ((InternalEObject)locInst).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF_LIST__LOC_INST, null, msgs);
      if (newLocInst != null)
        msgs = ((InternalEObject)newLocInst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF_LIST__LOC_INST, null, msgs);
      msgs = basicSetLocInst(newLocInst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__LOC_INST, newLocInst, newLocInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement getWs()
  {
    return ws;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWs(WithStatement newWs, NotificationChain msgs)
  {
    WithStatement oldWs = ws;
    ws = newWs;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__WS, oldWs, newWs);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWs(WithStatement newWs)
  {
    if (newWs != ws)
    {
      NotificationChain msgs = null;
      if (ws != null)
        msgs = ((InternalEObject)ws).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF_LIST__WS, null, msgs);
      if (newWs != null)
        msgs = ((InternalEObject)newWs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_DEF_LIST__WS, null, msgs);
      msgs = basicSetWs(newWs, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__WS, newWs, newWs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSc()
  {
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSc(String newSc)
  {
    String oldSc = sc;
    sc = newSc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_DEF_LIST__SC, oldSc, sc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF:
        return basicSetLocDef(null, msgs);
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_INST:
        return basicSetLocInst(null, msgs);
      case TTCN3Package.FUNCTION_DEF_LIST__WS:
        return basicSetWs(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF:
        return getLocDef();
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_INST:
        return getLocInst();
      case TTCN3Package.FUNCTION_DEF_LIST__WS:
        return getWs();
      case TTCN3Package.FUNCTION_DEF_LIST__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF:
        setLocDef((FunctionLocalDef)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_INST:
        setLocInst((FunctionLocalInst)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF_LIST__WS:
        setWs((WithStatement)newValue);
        return;
      case TTCN3Package.FUNCTION_DEF_LIST__SC:
        setSc((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF:
        setLocDef((FunctionLocalDef)null);
        return;
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_INST:
        setLocInst((FunctionLocalInst)null);
        return;
      case TTCN3Package.FUNCTION_DEF_LIST__WS:
        setWs((WithStatement)null);
        return;
      case TTCN3Package.FUNCTION_DEF_LIST__SC:
        setSc(SC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_DEF:
        return locDef != null;
      case TTCN3Package.FUNCTION_DEF_LIST__LOC_INST:
        return locInst != null;
      case TTCN3Package.FUNCTION_DEF_LIST__WS:
        return ws != null;
      case TTCN3Package.FUNCTION_DEF_LIST__SC:
        return SC_EDEFAULT == null ? sc != null : !SC_EDEFAULT.equals(sc);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //FunctionDefListImpl
