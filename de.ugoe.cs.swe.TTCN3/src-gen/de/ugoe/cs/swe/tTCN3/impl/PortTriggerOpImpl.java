/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FromClause;
import de.ugoe.cs.swe.tTCN3.InLineTemplate;
import de.ugoe.cs.swe.tTCN3.PortRedirect;
import de.ugoe.cs.swe.tTCN3.PortTriggerOp;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Trigger Op</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortTriggerOpImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortTriggerOpImpl#getFrom <em>From</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortTriggerOpImpl#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortTriggerOpImpl extends MinimalEObjectImpl.Container implements PortTriggerOp
{
  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected InLineTemplate template;

  /**
   * The cached value of the '{@link #getFrom() <em>From</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFrom()
   * @generated
   * @ordered
   */
  protected FromClause from;

  /**
   * The cached value of the '{@link #getRedirect() <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRedirect()
   * @generated
   * @ordered
   */
  protected PortRedirect redirect;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortTriggerOpImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortTriggerOp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InLineTemplate getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(InLineTemplate newTemplate, NotificationChain msgs)
  {
    InLineTemplate oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_TRIGGER_OP__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(InLineTemplate newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_TRIGGER_OP__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_TRIGGER_OP__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_TRIGGER_OP__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FromClause getFrom()
  {
    return from;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFrom(FromClause newFrom, NotificationChain msgs)
  {
    FromClause oldFrom = from;
    from = newFrom;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_TRIGGER_OP__FROM, oldFrom, newFrom);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFrom(FromClause newFrom)
  {
    if (newFrom != from)
    {
      NotificationChain msgs = null;
      if (from != null)
        msgs = ((InternalEObject)from).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_TRIGGER_OP__FROM, null, msgs);
      if (newFrom != null)
        msgs = ((InternalEObject)newFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_TRIGGER_OP__FROM, null, msgs);
      msgs = basicSetFrom(newFrom, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_TRIGGER_OP__FROM, newFrom, newFrom));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRedirect getRedirect()
  {
    return redirect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRedirect(PortRedirect newRedirect, NotificationChain msgs)
  {
    PortRedirect oldRedirect = redirect;
    redirect = newRedirect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_TRIGGER_OP__REDIRECT, oldRedirect, newRedirect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRedirect(PortRedirect newRedirect)
  {
    if (newRedirect != redirect)
    {
      NotificationChain msgs = null;
      if (redirect != null)
        msgs = ((InternalEObject)redirect).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_TRIGGER_OP__REDIRECT, null, msgs);
      if (newRedirect != null)
        msgs = ((InternalEObject)newRedirect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_TRIGGER_OP__REDIRECT, null, msgs);
      msgs = basicSetRedirect(newRedirect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_TRIGGER_OP__REDIRECT, newRedirect, newRedirect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_TRIGGER_OP__TEMPLATE:
        return basicSetTemplate(null, msgs);
      case TTCN3Package.PORT_TRIGGER_OP__FROM:
        return basicSetFrom(null, msgs);
      case TTCN3Package.PORT_TRIGGER_OP__REDIRECT:
        return basicSetRedirect(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_TRIGGER_OP__TEMPLATE:
        return getTemplate();
      case TTCN3Package.PORT_TRIGGER_OP__FROM:
        return getFrom();
      case TTCN3Package.PORT_TRIGGER_OP__REDIRECT:
        return getRedirect();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_TRIGGER_OP__TEMPLATE:
        setTemplate((InLineTemplate)newValue);
        return;
      case TTCN3Package.PORT_TRIGGER_OP__FROM:
        setFrom((FromClause)newValue);
        return;
      case TTCN3Package.PORT_TRIGGER_OP__REDIRECT:
        setRedirect((PortRedirect)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_TRIGGER_OP__TEMPLATE:
        setTemplate((InLineTemplate)null);
        return;
      case TTCN3Package.PORT_TRIGGER_OP__FROM:
        setFrom((FromClause)null);
        return;
      case TTCN3Package.PORT_TRIGGER_OP__REDIRECT:
        setRedirect((PortRedirect)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_TRIGGER_OP__TEMPLATE:
        return template != null;
      case TTCN3Package.PORT_TRIGGER_OP__FROM:
        return from != null;
      case TTCN3Package.PORT_TRIGGER_OP__REDIRECT:
        return redirect != null;
    }
    return super.eIsSet(featureID);
  }

} //PortTriggerOpImpl
