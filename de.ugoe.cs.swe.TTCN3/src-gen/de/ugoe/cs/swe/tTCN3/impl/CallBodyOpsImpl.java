/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.CallBodyOps;
import de.ugoe.cs.swe.tTCN3.CatchStatement;
import de.ugoe.cs.swe.tTCN3.GetReplyStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Body Ops</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CallBodyOpsImpl#getReply <em>Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CallBodyOpsImpl#getCatch <em>Catch</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallBodyOpsImpl extends MinimalEObjectImpl.Container implements CallBodyOps
{
  /**
   * The cached value of the '{@link #getReply() <em>Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReply()
   * @generated
   * @ordered
   */
  protected GetReplyStatement reply;

  /**
   * The cached value of the '{@link #getCatch() <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCatch()
   * @generated
   * @ordered
   */
  protected CatchStatement catch_;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CallBodyOpsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getCallBodyOps();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetReplyStatement getReply()
  {
    return reply;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReply(GetReplyStatement newReply, NotificationChain msgs)
  {
    GetReplyStatement oldReply = reply;
    reply = newReply;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CALL_BODY_OPS__REPLY, oldReply, newReply);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReply(GetReplyStatement newReply)
  {
    if (newReply != reply)
    {
      NotificationChain msgs = null;
      if (reply != null)
        msgs = ((InternalEObject)reply).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CALL_BODY_OPS__REPLY, null, msgs);
      if (newReply != null)
        msgs = ((InternalEObject)newReply).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CALL_BODY_OPS__REPLY, null, msgs);
      msgs = basicSetReply(newReply, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CALL_BODY_OPS__REPLY, newReply, newReply));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CatchStatement getCatch()
  {
    return catch_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCatch(CatchStatement newCatch, NotificationChain msgs)
  {
    CatchStatement oldCatch = catch_;
    catch_ = newCatch;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CALL_BODY_OPS__CATCH, oldCatch, newCatch);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCatch(CatchStatement newCatch)
  {
    if (newCatch != catch_)
    {
      NotificationChain msgs = null;
      if (catch_ != null)
        msgs = ((InternalEObject)catch_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CALL_BODY_OPS__CATCH, null, msgs);
      if (newCatch != null)
        msgs = ((InternalEObject)newCatch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CALL_BODY_OPS__CATCH, null, msgs);
      msgs = basicSetCatch(newCatch, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CALL_BODY_OPS__CATCH, newCatch, newCatch));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CALL_BODY_OPS__REPLY:
        return basicSetReply(null, msgs);
      case TTCN3Package.CALL_BODY_OPS__CATCH:
        return basicSetCatch(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CALL_BODY_OPS__REPLY:
        return getReply();
      case TTCN3Package.CALL_BODY_OPS__CATCH:
        return getCatch();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CALL_BODY_OPS__REPLY:
        setReply((GetReplyStatement)newValue);
        return;
      case TTCN3Package.CALL_BODY_OPS__CATCH:
        setCatch((CatchStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CALL_BODY_OPS__REPLY:
        setReply((GetReplyStatement)null);
        return;
      case TTCN3Package.CALL_BODY_OPS__CATCH:
        setCatch((CatchStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CALL_BODY_OPS__REPLY:
        return reply != null;
      case TTCN3Package.CALL_BODY_OPS__CATCH:
        return catch_ != null;
    }
    return super.eIsSet(featureID);
  }

} //CallBodyOpsImpl
