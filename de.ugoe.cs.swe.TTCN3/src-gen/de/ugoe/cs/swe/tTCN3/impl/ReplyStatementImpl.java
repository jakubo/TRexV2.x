/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayOrBitRef;
import de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar;
import de.ugoe.cs.swe.tTCN3.PortReplyOp;
import de.ugoe.cs.swe.tTCN3.ReplyStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reply Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ReplyStatementImpl#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ReplyStatementImpl#getArrayRefs <em>Array Refs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ReplyStatementImpl#getOp <em>Op</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReplyStatementImpl extends MinimalEObjectImpl.Container implements ReplyStatement
{
  /**
   * The cached value of the '{@link #getPort() <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort()
   * @generated
   * @ordered
   */
  protected FormalPortAndValuePar port;

  /**
   * The cached value of the '{@link #getArrayRefs() <em>Array Refs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArrayRefs()
   * @generated
   * @ordered
   */
  protected EList<ArrayOrBitRef> arrayRefs;

  /**
   * The cached value of the '{@link #getOp() <em>Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOp()
   * @generated
   * @ordered
   */
  protected PortReplyOp op;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ReplyStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getReplyStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalPortAndValuePar getPort()
  {
    if (port != null && port.eIsProxy())
    {
      InternalEObject oldPort = (InternalEObject)port;
      port = (FormalPortAndValuePar)eResolveProxy(oldPort);
      if (port != oldPort)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.REPLY_STATEMENT__PORT, oldPort, port));
      }
    }
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalPortAndValuePar basicGetPort()
  {
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort(FormalPortAndValuePar newPort)
  {
    FormalPortAndValuePar oldPort = port;
    port = newPort;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REPLY_STATEMENT__PORT, oldPort, port));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ArrayOrBitRef> getArrayRefs()
  {
    if (arrayRefs == null)
    {
      arrayRefs = new EObjectContainmentEList<ArrayOrBitRef>(ArrayOrBitRef.class, this, TTCN3Package.REPLY_STATEMENT__ARRAY_REFS);
    }
    return arrayRefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortReplyOp getOp()
  {
    return op;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOp(PortReplyOp newOp, NotificationChain msgs)
  {
    PortReplyOp oldOp = op;
    op = newOp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REPLY_STATEMENT__OP, oldOp, newOp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOp(PortReplyOp newOp)
  {
    if (newOp != op)
    {
      NotificationChain msgs = null;
      if (op != null)
        msgs = ((InternalEObject)op).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REPLY_STATEMENT__OP, null, msgs);
      if (newOp != null)
        msgs = ((InternalEObject)newOp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REPLY_STATEMENT__OP, null, msgs);
      msgs = basicSetOp(newOp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REPLY_STATEMENT__OP, newOp, newOp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.REPLY_STATEMENT__ARRAY_REFS:
        return ((InternalEList<?>)getArrayRefs()).basicRemove(otherEnd, msgs);
      case TTCN3Package.REPLY_STATEMENT__OP:
        return basicSetOp(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.REPLY_STATEMENT__PORT:
        if (resolve) return getPort();
        return basicGetPort();
      case TTCN3Package.REPLY_STATEMENT__ARRAY_REFS:
        return getArrayRefs();
      case TTCN3Package.REPLY_STATEMENT__OP:
        return getOp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.REPLY_STATEMENT__PORT:
        setPort((FormalPortAndValuePar)newValue);
        return;
      case TTCN3Package.REPLY_STATEMENT__ARRAY_REFS:
        getArrayRefs().clear();
        getArrayRefs().addAll((Collection<? extends ArrayOrBitRef>)newValue);
        return;
      case TTCN3Package.REPLY_STATEMENT__OP:
        setOp((PortReplyOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.REPLY_STATEMENT__PORT:
        setPort((FormalPortAndValuePar)null);
        return;
      case TTCN3Package.REPLY_STATEMENT__ARRAY_REFS:
        getArrayRefs().clear();
        return;
      case TTCN3Package.REPLY_STATEMENT__OP:
        setOp((PortReplyOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.REPLY_STATEMENT__PORT:
        return port != null;
      case TTCN3Package.REPLY_STATEMENT__ARRAY_REFS:
        return arrayRefs != null && !arrayRefs.isEmpty();
      case TTCN3Package.REPLY_STATEMENT__OP:
        return op != null;
    }
    return super.eIsSet(featureID);
  }

} //ReplyStatementImpl
