/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AltGuardList;
import de.ugoe.cs.swe.tTCN3.AltstepDef;
import de.ugoe.cs.swe.tTCN3.AltstepLocalDefList;
import de.ugoe.cs.swe.tTCN3.FunctionFormalParList;
import de.ugoe.cs.swe.tTCN3.MtcSpec;
import de.ugoe.cs.swe.tTCN3.RunsOnSpec;
import de.ugoe.cs.swe.tTCN3.SystemSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Altstep Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl#getParams <em>Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl#getMtc <em>Mtc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl#getSystem <em>System</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl#getLocal <em>Local</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl#getGuard <em>Guard</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AltstepDefImpl extends FunctionRefImpl implements AltstepDef
{
  /**
   * The cached value of the '{@link #getParams() <em>Params</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParams()
   * @generated
   * @ordered
   */
  protected FunctionFormalParList params;

  /**
   * The cached value of the '{@link #getSpec() <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpec()
   * @generated
   * @ordered
   */
  protected RunsOnSpec spec;

  /**
   * The cached value of the '{@link #getMtc() <em>Mtc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMtc()
   * @generated
   * @ordered
   */
  protected MtcSpec mtc;

  /**
   * The cached value of the '{@link #getSystem() <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystem()
   * @generated
   * @ordered
   */
  protected SystemSpec system;

  /**
   * The cached value of the '{@link #getLocal() <em>Local</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocal()
   * @generated
   * @ordered
   */
  protected AltstepLocalDefList local;

  /**
   * The cached value of the '{@link #getGuard() <em>Guard</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGuard()
   * @generated
   * @ordered
   */
  protected AltGuardList guard;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AltstepDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAltstepDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionFormalParList getParams()
  {
    return params;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParams(FunctionFormalParList newParams, NotificationChain msgs)
  {
    FunctionFormalParList oldParams = params;
    params = newParams;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__PARAMS, oldParams, newParams);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParams(FunctionFormalParList newParams)
  {
    if (newParams != params)
    {
      NotificationChain msgs = null;
      if (params != null)
        msgs = ((InternalEObject)params).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__PARAMS, null, msgs);
      if (newParams != null)
        msgs = ((InternalEObject)newParams).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__PARAMS, null, msgs);
      msgs = basicSetParams(newParams, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__PARAMS, newParams, newParams));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunsOnSpec getSpec()
  {
    return spec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSpec(RunsOnSpec newSpec, NotificationChain msgs)
  {
    RunsOnSpec oldSpec = spec;
    spec = newSpec;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__SPEC, oldSpec, newSpec);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpec(RunsOnSpec newSpec)
  {
    if (newSpec != spec)
    {
      NotificationChain msgs = null;
      if (spec != null)
        msgs = ((InternalEObject)spec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__SPEC, null, msgs);
      if (newSpec != null)
        msgs = ((InternalEObject)newSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__SPEC, null, msgs);
      msgs = basicSetSpec(newSpec, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__SPEC, newSpec, newSpec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MtcSpec getMtc()
  {
    return mtc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMtc(MtcSpec newMtc, NotificationChain msgs)
  {
    MtcSpec oldMtc = mtc;
    mtc = newMtc;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__MTC, oldMtc, newMtc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMtc(MtcSpec newMtc)
  {
    if (newMtc != mtc)
    {
      NotificationChain msgs = null;
      if (mtc != null)
        msgs = ((InternalEObject)mtc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__MTC, null, msgs);
      if (newMtc != null)
        msgs = ((InternalEObject)newMtc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__MTC, null, msgs);
      msgs = basicSetMtc(newMtc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__MTC, newMtc, newMtc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemSpec getSystem()
  {
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSystem(SystemSpec newSystem, NotificationChain msgs)
  {
    SystemSpec oldSystem = system;
    system = newSystem;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__SYSTEM, oldSystem, newSystem);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSystem(SystemSpec newSystem)
  {
    if (newSystem != system)
    {
      NotificationChain msgs = null;
      if (system != null)
        msgs = ((InternalEObject)system).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__SYSTEM, null, msgs);
      if (newSystem != null)
        msgs = ((InternalEObject)newSystem).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__SYSTEM, null, msgs);
      msgs = basicSetSystem(newSystem, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__SYSTEM, newSystem, newSystem));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepLocalDefList getLocal()
  {
    return local;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLocal(AltstepLocalDefList newLocal, NotificationChain msgs)
  {
    AltstepLocalDefList oldLocal = local;
    local = newLocal;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__LOCAL, oldLocal, newLocal);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocal(AltstepLocalDefList newLocal)
  {
    if (newLocal != local)
    {
      NotificationChain msgs = null;
      if (local != null)
        msgs = ((InternalEObject)local).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__LOCAL, null, msgs);
      if (newLocal != null)
        msgs = ((InternalEObject)newLocal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__LOCAL, null, msgs);
      msgs = basicSetLocal(newLocal, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__LOCAL, newLocal, newLocal));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltGuardList getGuard()
  {
    return guard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGuard(AltGuardList newGuard, NotificationChain msgs)
  {
    AltGuardList oldGuard = guard;
    guard = newGuard;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__GUARD, oldGuard, newGuard);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGuard(AltGuardList newGuard)
  {
    if (newGuard != guard)
    {
      NotificationChain msgs = null;
      if (guard != null)
        msgs = ((InternalEObject)guard).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__GUARD, null, msgs);
      if (newGuard != null)
        msgs = ((InternalEObject)newGuard).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALTSTEP_DEF__GUARD, null, msgs);
      msgs = basicSetGuard(newGuard, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALTSTEP_DEF__GUARD, newGuard, newGuard));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_DEF__PARAMS:
        return basicSetParams(null, msgs);
      case TTCN3Package.ALTSTEP_DEF__SPEC:
        return basicSetSpec(null, msgs);
      case TTCN3Package.ALTSTEP_DEF__MTC:
        return basicSetMtc(null, msgs);
      case TTCN3Package.ALTSTEP_DEF__SYSTEM:
        return basicSetSystem(null, msgs);
      case TTCN3Package.ALTSTEP_DEF__LOCAL:
        return basicSetLocal(null, msgs);
      case TTCN3Package.ALTSTEP_DEF__GUARD:
        return basicSetGuard(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_DEF__PARAMS:
        return getParams();
      case TTCN3Package.ALTSTEP_DEF__SPEC:
        return getSpec();
      case TTCN3Package.ALTSTEP_DEF__MTC:
        return getMtc();
      case TTCN3Package.ALTSTEP_DEF__SYSTEM:
        return getSystem();
      case TTCN3Package.ALTSTEP_DEF__LOCAL:
        return getLocal();
      case TTCN3Package.ALTSTEP_DEF__GUARD:
        return getGuard();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_DEF__PARAMS:
        setParams((FunctionFormalParList)newValue);
        return;
      case TTCN3Package.ALTSTEP_DEF__SPEC:
        setSpec((RunsOnSpec)newValue);
        return;
      case TTCN3Package.ALTSTEP_DEF__MTC:
        setMtc((MtcSpec)newValue);
        return;
      case TTCN3Package.ALTSTEP_DEF__SYSTEM:
        setSystem((SystemSpec)newValue);
        return;
      case TTCN3Package.ALTSTEP_DEF__LOCAL:
        setLocal((AltstepLocalDefList)newValue);
        return;
      case TTCN3Package.ALTSTEP_DEF__GUARD:
        setGuard((AltGuardList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_DEF__PARAMS:
        setParams((FunctionFormalParList)null);
        return;
      case TTCN3Package.ALTSTEP_DEF__SPEC:
        setSpec((RunsOnSpec)null);
        return;
      case TTCN3Package.ALTSTEP_DEF__MTC:
        setMtc((MtcSpec)null);
        return;
      case TTCN3Package.ALTSTEP_DEF__SYSTEM:
        setSystem((SystemSpec)null);
        return;
      case TTCN3Package.ALTSTEP_DEF__LOCAL:
        setLocal((AltstepLocalDefList)null);
        return;
      case TTCN3Package.ALTSTEP_DEF__GUARD:
        setGuard((AltGuardList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_DEF__PARAMS:
        return params != null;
      case TTCN3Package.ALTSTEP_DEF__SPEC:
        return spec != null;
      case TTCN3Package.ALTSTEP_DEF__MTC:
        return mtc != null;
      case TTCN3Package.ALTSTEP_DEF__SYSTEM:
        return system != null;
      case TTCN3Package.ALTSTEP_DEF__LOCAL:
        return local != null;
      case TTCN3Package.ALTSTEP_DEF__GUARD:
        return guard != null;
    }
    return super.eIsSet(featureID);
  }

} //AltstepDefImpl
