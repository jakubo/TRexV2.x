/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.PortRedirectWithParam;
import de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Redirect With Param</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortRedirectWithParamImpl#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortRedirectWithParamImpl extends MinimalEObjectImpl.Container implements PortRedirectWithParam
{
  /**
   * The cached value of the '{@link #getRedirect() <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRedirect()
   * @generated
   * @ordered
   */
  protected RedirectWithParamSpec redirect;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortRedirectWithParamImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortRedirectWithParam();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RedirectWithParamSpec getRedirect()
  {
    return redirect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRedirect(RedirectWithParamSpec newRedirect, NotificationChain msgs)
  {
    RedirectWithParamSpec oldRedirect = redirect;
    redirect = newRedirect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT, oldRedirect, newRedirect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRedirect(RedirectWithParamSpec newRedirect)
  {
    if (newRedirect != redirect)
    {
      NotificationChain msgs = null;
      if (redirect != null)
        msgs = ((InternalEObject)redirect).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT, null, msgs);
      if (newRedirect != null)
        msgs = ((InternalEObject)newRedirect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT, null, msgs);
      msgs = basicSetRedirect(newRedirect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT, newRedirect, newRedirect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT:
        return basicSetRedirect(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT:
        return getRedirect();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT:
        setRedirect((RedirectWithParamSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT:
        setRedirect((RedirectWithParamSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM__REDIRECT:
        return redirect != null;
    }
    return super.eIsSet(featureID);
  }

} //PortRedirectWithParamImpl
