/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ComponentDef;
import de.ugoe.cs.swe.tTCN3.EnumDef;
import de.ugoe.cs.swe.tTCN3.PortDef;
import de.ugoe.cs.swe.tTCN3.RecordDef;
import de.ugoe.cs.swe.tTCN3.RecordOfDef;
import de.ugoe.cs.swe.tTCN3.SetDef;
import de.ugoe.cs.swe.tTCN3.SetOfDef;
import de.ugoe.cs.swe.tTCN3.StructuredTypeDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.UnionDef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structured Type Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getRecord <em>Record</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getUnion <em>Union</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getSet <em>Set</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getRecordOf <em>Record Of</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getSetOf <em>Set Of</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getEnumDef <em>Enum Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StructuredTypeDefImpl extends MinimalEObjectImpl.Container implements StructuredTypeDef
{
  /**
   * The cached value of the '{@link #getRecord() <em>Record</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRecord()
   * @generated
   * @ordered
   */
  protected RecordDef record;

  /**
   * The cached value of the '{@link #getUnion() <em>Union</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnion()
   * @generated
   * @ordered
   */
  protected UnionDef union;

  /**
   * The cached value of the '{@link #getSet() <em>Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSet()
   * @generated
   * @ordered
   */
  protected SetDef set;

  /**
   * The cached value of the '{@link #getRecordOf() <em>Record Of</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRecordOf()
   * @generated
   * @ordered
   */
  protected RecordOfDef recordOf;

  /**
   * The cached value of the '{@link #getSetOf() <em>Set Of</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSetOf()
   * @generated
   * @ordered
   */
  protected SetOfDef setOf;

  /**
   * The cached value of the '{@link #getEnumDef() <em>Enum Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEnumDef()
   * @generated
   * @ordered
   */
  protected EnumDef enumDef;

  /**
   * The cached value of the '{@link #getPort() <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort()
   * @generated
   * @ordered
   */
  protected PortDef port;

  /**
   * The cached value of the '{@link #getComponent() <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComponent()
   * @generated
   * @ordered
   */
  protected ComponentDef component;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StructuredTypeDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getStructuredTypeDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordDef getRecord()
  {
    return record;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRecord(RecordDef newRecord, NotificationChain msgs)
  {
    RecordDef oldRecord = record;
    record = newRecord;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__RECORD, oldRecord, newRecord);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRecord(RecordDef newRecord)
  {
    if (newRecord != record)
    {
      NotificationChain msgs = null;
      if (record != null)
        msgs = ((InternalEObject)record).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__RECORD, null, msgs);
      if (newRecord != null)
        msgs = ((InternalEObject)newRecord).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__RECORD, null, msgs);
      msgs = basicSetRecord(newRecord, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__RECORD, newRecord, newRecord));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionDef getUnion()
  {
    return union;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUnion(UnionDef newUnion, NotificationChain msgs)
  {
    UnionDef oldUnion = union;
    union = newUnion;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__UNION, oldUnion, newUnion);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnion(UnionDef newUnion)
  {
    if (newUnion != union)
    {
      NotificationChain msgs = null;
      if (union != null)
        msgs = ((InternalEObject)union).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__UNION, null, msgs);
      if (newUnion != null)
        msgs = ((InternalEObject)newUnion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__UNION, null, msgs);
      msgs = basicSetUnion(newUnion, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__UNION, newUnion, newUnion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetDef getSet()
  {
    return set;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSet(SetDef newSet, NotificationChain msgs)
  {
    SetDef oldSet = set;
    set = newSet;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__SET, oldSet, newSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSet(SetDef newSet)
  {
    if (newSet != set)
    {
      NotificationChain msgs = null;
      if (set != null)
        msgs = ((InternalEObject)set).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__SET, null, msgs);
      if (newSet != null)
        msgs = ((InternalEObject)newSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__SET, null, msgs);
      msgs = basicSetSet(newSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__SET, newSet, newSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordOfDef getRecordOf()
  {
    return recordOf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRecordOf(RecordOfDef newRecordOf, NotificationChain msgs)
  {
    RecordOfDef oldRecordOf = recordOf;
    recordOf = newRecordOf;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF, oldRecordOf, newRecordOf);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRecordOf(RecordOfDef newRecordOf)
  {
    if (newRecordOf != recordOf)
    {
      NotificationChain msgs = null;
      if (recordOf != null)
        msgs = ((InternalEObject)recordOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF, null, msgs);
      if (newRecordOf != null)
        msgs = ((InternalEObject)newRecordOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF, null, msgs);
      msgs = basicSetRecordOf(newRecordOf, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF, newRecordOf, newRecordOf));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetOfDef getSetOf()
  {
    return setOf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSetOf(SetOfDef newSetOf, NotificationChain msgs)
  {
    SetOfDef oldSetOf = setOf;
    setOf = newSetOf;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF, oldSetOf, newSetOf);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSetOf(SetOfDef newSetOf)
  {
    if (newSetOf != setOf)
    {
      NotificationChain msgs = null;
      if (setOf != null)
        msgs = ((InternalEObject)setOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF, null, msgs);
      if (newSetOf != null)
        msgs = ((InternalEObject)newSetOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF, null, msgs);
      msgs = basicSetSetOf(newSetOf, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF, newSetOf, newSetOf));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumDef getEnumDef()
  {
    return enumDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEnumDef(EnumDef newEnumDef, NotificationChain msgs)
  {
    EnumDef oldEnumDef = enumDef;
    enumDef = newEnumDef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF, oldEnumDef, newEnumDef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEnumDef(EnumDef newEnumDef)
  {
    if (newEnumDef != enumDef)
    {
      NotificationChain msgs = null;
      if (enumDef != null)
        msgs = ((InternalEObject)enumDef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF, null, msgs);
      if (newEnumDef != null)
        msgs = ((InternalEObject)newEnumDef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF, null, msgs);
      msgs = basicSetEnumDef(newEnumDef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF, newEnumDef, newEnumDef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortDef getPort()
  {
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPort(PortDef newPort, NotificationChain msgs)
  {
    PortDef oldPort = port;
    port = newPort;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__PORT, oldPort, newPort);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort(PortDef newPort)
  {
    if (newPort != port)
    {
      NotificationChain msgs = null;
      if (port != null)
        msgs = ((InternalEObject)port).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__PORT, null, msgs);
      if (newPort != null)
        msgs = ((InternalEObject)newPort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__PORT, null, msgs);
      msgs = basicSetPort(newPort, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__PORT, newPort, newPort));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentDef getComponent()
  {
    return component;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComponent(ComponentDef newComponent, NotificationChain msgs)
  {
    ComponentDef oldComponent = component;
    component = newComponent;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT, oldComponent, newComponent);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponent(ComponentDef newComponent)
  {
    if (newComponent != component)
    {
      NotificationChain msgs = null;
      if (component != null)
        msgs = ((InternalEObject)component).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT, null, msgs);
      if (newComponent != null)
        msgs = ((InternalEObject)newComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT, null, msgs);
      msgs = basicSetComponent(newComponent, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT, newComponent, newComponent));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD:
        return basicSetRecord(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__UNION:
        return basicSetUnion(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET:
        return basicSetSet(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF:
        return basicSetRecordOf(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF:
        return basicSetSetOf(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF:
        return basicSetEnumDef(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__PORT:
        return basicSetPort(null, msgs);
      case TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT:
        return basicSetComponent(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD:
        return getRecord();
      case TTCN3Package.STRUCTURED_TYPE_DEF__UNION:
        return getUnion();
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET:
        return getSet();
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF:
        return getRecordOf();
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF:
        return getSetOf();
      case TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF:
        return getEnumDef();
      case TTCN3Package.STRUCTURED_TYPE_DEF__PORT:
        return getPort();
      case TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT:
        return getComponent();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD:
        setRecord((RecordDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__UNION:
        setUnion((UnionDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET:
        setSet((SetDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF:
        setRecordOf((RecordOfDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF:
        setSetOf((SetOfDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF:
        setEnumDef((EnumDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__PORT:
        setPort((PortDef)newValue);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT:
        setComponent((ComponentDef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD:
        setRecord((RecordDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__UNION:
        setUnion((UnionDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET:
        setSet((SetDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF:
        setRecordOf((RecordOfDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF:
        setSetOf((SetOfDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF:
        setEnumDef((EnumDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__PORT:
        setPort((PortDef)null);
        return;
      case TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT:
        setComponent((ComponentDef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD:
        return record != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__UNION:
        return union != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET:
        return set != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__RECORD_OF:
        return recordOf != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__SET_OF:
        return setOf != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__ENUM_DEF:
        return enumDef != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__PORT:
        return port != null;
      case TTCN3Package.STRUCTURED_TYPE_DEF__COMPONENT:
        return component != null;
    }
    return super.eIsSet(featureID);
  }

} //StructuredTypeDefImpl
