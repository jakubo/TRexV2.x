/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AddressRefList;
import de.ugoe.cs.swe.tTCN3.FromClause;
import de.ugoe.cs.swe.tTCN3.InLineTemplate;
import de.ugoe.cs.swe.tTCN3.IndexSpec;
import de.ugoe.cs.swe.tTCN3.SenderSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>From Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FromClauseImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FromClauseImpl#getIndex <em>Index</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FromClauseImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FromClauseImpl#getAddresses <em>Addresses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FromClauseImpl extends FromClausePresentImpl implements FromClause
{
  /**
   * The cached value of the '{@link #getSender() <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSender()
   * @generated
   * @ordered
   */
  protected SenderSpec sender;

  /**
   * The cached value of the '{@link #getIndex() <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIndex()
   * @generated
   * @ordered
   */
  protected IndexSpec index;

  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected InLineTemplate template;

  /**
   * The cached value of the '{@link #getAddresses() <em>Addresses</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddresses()
   * @generated
   * @ordered
   */
  protected AddressRefList addresses;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FromClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFromClause();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SenderSpec getSender()
  {
    return sender;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSender(SenderSpec newSender, NotificationChain msgs)
  {
    SenderSpec oldSender = sender;
    sender = newSender;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__SENDER, oldSender, newSender);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSender(SenderSpec newSender)
  {
    if (newSender != sender)
    {
      NotificationChain msgs = null;
      if (sender != null)
        msgs = ((InternalEObject)sender).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__SENDER, null, msgs);
      if (newSender != null)
        msgs = ((InternalEObject)newSender).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__SENDER, null, msgs);
      msgs = basicSetSender(newSender, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__SENDER, newSender, newSender));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IndexSpec getIndex()
  {
    return index;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIndex(IndexSpec newIndex, NotificationChain msgs)
  {
    IndexSpec oldIndex = index;
    index = newIndex;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__INDEX, oldIndex, newIndex);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIndex(IndexSpec newIndex)
  {
    if (newIndex != index)
    {
      NotificationChain msgs = null;
      if (index != null)
        msgs = ((InternalEObject)index).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__INDEX, null, msgs);
      if (newIndex != null)
        msgs = ((InternalEObject)newIndex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__INDEX, null, msgs);
      msgs = basicSetIndex(newIndex, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__INDEX, newIndex, newIndex));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InLineTemplate getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(InLineTemplate newTemplate, NotificationChain msgs)
  {
    InLineTemplate oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(InLineTemplate newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddressRefList getAddresses()
  {
    return addresses;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAddresses(AddressRefList newAddresses, NotificationChain msgs)
  {
    AddressRefList oldAddresses = addresses;
    addresses = newAddresses;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__ADDRESSES, oldAddresses, newAddresses);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAddresses(AddressRefList newAddresses)
  {
    if (newAddresses != addresses)
    {
      NotificationChain msgs = null;
      if (addresses != null)
        msgs = ((InternalEObject)addresses).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__ADDRESSES, null, msgs);
      if (newAddresses != null)
        msgs = ((InternalEObject)newAddresses).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FROM_CLAUSE__ADDRESSES, null, msgs);
      msgs = basicSetAddresses(newAddresses, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FROM_CLAUSE__ADDRESSES, newAddresses, newAddresses));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FROM_CLAUSE__SENDER:
        return basicSetSender(null, msgs);
      case TTCN3Package.FROM_CLAUSE__INDEX:
        return basicSetIndex(null, msgs);
      case TTCN3Package.FROM_CLAUSE__TEMPLATE:
        return basicSetTemplate(null, msgs);
      case TTCN3Package.FROM_CLAUSE__ADDRESSES:
        return basicSetAddresses(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FROM_CLAUSE__SENDER:
        return getSender();
      case TTCN3Package.FROM_CLAUSE__INDEX:
        return getIndex();
      case TTCN3Package.FROM_CLAUSE__TEMPLATE:
        return getTemplate();
      case TTCN3Package.FROM_CLAUSE__ADDRESSES:
        return getAddresses();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FROM_CLAUSE__SENDER:
        setSender((SenderSpec)newValue);
        return;
      case TTCN3Package.FROM_CLAUSE__INDEX:
        setIndex((IndexSpec)newValue);
        return;
      case TTCN3Package.FROM_CLAUSE__TEMPLATE:
        setTemplate((InLineTemplate)newValue);
        return;
      case TTCN3Package.FROM_CLAUSE__ADDRESSES:
        setAddresses((AddressRefList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FROM_CLAUSE__SENDER:
        setSender((SenderSpec)null);
        return;
      case TTCN3Package.FROM_CLAUSE__INDEX:
        setIndex((IndexSpec)null);
        return;
      case TTCN3Package.FROM_CLAUSE__TEMPLATE:
        setTemplate((InLineTemplate)null);
        return;
      case TTCN3Package.FROM_CLAUSE__ADDRESSES:
        setAddresses((AddressRefList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FROM_CLAUSE__SENDER:
        return sender != null;
      case TTCN3Package.FROM_CLAUSE__INDEX:
        return index != null;
      case TTCN3Package.FROM_CLAUSE__TEMPLATE:
        return template != null;
      case TTCN3Package.FROM_CLAUSE__ADDRESSES:
        return addresses != null;
    }
    return super.eIsSet(featureID);
  }

} //FromClauseImpl
