/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FunctionDefList;
import de.ugoe.cs.swe.tTCN3.FunctionStatementList;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statement Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StatementBlockImpl#getDef <em>Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.StatementBlockImpl#getStat <em>Stat</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StatementBlockImpl extends MinimalEObjectImpl.Container implements StatementBlock
{
  /**
   * The cached value of the '{@link #getDef() <em>Def</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDef()
   * @generated
   * @ordered
   */
  protected EList<FunctionDefList> def;

  /**
   * The cached value of the '{@link #getStat() <em>Stat</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStat()
   * @generated
   * @ordered
   */
  protected EList<FunctionStatementList> stat;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StatementBlockImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getStatementBlock();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionDefList> getDef()
  {
    if (def == null)
    {
      def = new EObjectContainmentEList<FunctionDefList>(FunctionDefList.class, this, TTCN3Package.STATEMENT_BLOCK__DEF);
    }
    return def;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionStatementList> getStat()
  {
    if (stat == null)
    {
      stat = new EObjectContainmentEList<FunctionStatementList>(FunctionStatementList.class, this, TTCN3Package.STATEMENT_BLOCK__STAT);
    }
    return stat;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.STATEMENT_BLOCK__DEF:
        return ((InternalEList<?>)getDef()).basicRemove(otherEnd, msgs);
      case TTCN3Package.STATEMENT_BLOCK__STAT:
        return ((InternalEList<?>)getStat()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.STATEMENT_BLOCK__DEF:
        return getDef();
      case TTCN3Package.STATEMENT_BLOCK__STAT:
        return getStat();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.STATEMENT_BLOCK__DEF:
        getDef().clear();
        getDef().addAll((Collection<? extends FunctionDefList>)newValue);
        return;
      case TTCN3Package.STATEMENT_BLOCK__STAT:
        getStat().clear();
        getStat().addAll((Collection<? extends FunctionStatementList>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.STATEMENT_BLOCK__DEF:
        getDef().clear();
        return;
      case TTCN3Package.STATEMENT_BLOCK__STAT:
        getStat().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.STATEMENT_BLOCK__DEF:
        return def != null && !def.isEmpty();
      case TTCN3Package.STATEMENT_BLOCK__STAT:
        return stat != null && !stat.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //StatementBlockImpl
