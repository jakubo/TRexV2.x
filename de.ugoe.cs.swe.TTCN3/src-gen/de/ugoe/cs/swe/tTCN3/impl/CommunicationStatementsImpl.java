/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.CallStatement;
import de.ugoe.cs.swe.tTCN3.CatchStatement;
import de.ugoe.cs.swe.tTCN3.CheckStateStatement;
import de.ugoe.cs.swe.tTCN3.CheckStatement;
import de.ugoe.cs.swe.tTCN3.ClearStatement;
import de.ugoe.cs.swe.tTCN3.CommunicationStatements;
import de.ugoe.cs.swe.tTCN3.GetCallStatement;
import de.ugoe.cs.swe.tTCN3.GetReplyStatement;
import de.ugoe.cs.swe.tTCN3.HaltStatement;
import de.ugoe.cs.swe.tTCN3.RaiseStatement;
import de.ugoe.cs.swe.tTCN3.ReceiveStatement;
import de.ugoe.cs.swe.tTCN3.ReplyStatement;
import de.ugoe.cs.swe.tTCN3.SendStatement;
import de.ugoe.cs.swe.tTCN3.StartStatement;
import de.ugoe.cs.swe.tTCN3.StopStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TriggerStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Statements</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getSend <em>Send</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getCall <em>Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getReply <em>Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getRaise <em>Raise</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getReceive <em>Receive</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getGetCall <em>Get Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getGetReply <em>Get Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getCatch <em>Catch</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getCheck <em>Check</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getClear <em>Clear</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getStart <em>Start</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getStop <em>Stop</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getHalt <em>Halt</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl#getCheckState <em>Check State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationStatementsImpl extends MinimalEObjectImpl.Container implements CommunicationStatements
{
  /**
   * The cached value of the '{@link #getSend() <em>Send</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSend()
   * @generated
   * @ordered
   */
  protected SendStatement send;

  /**
   * The cached value of the '{@link #getCall() <em>Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCall()
   * @generated
   * @ordered
   */
  protected CallStatement call;

  /**
   * The cached value of the '{@link #getReply() <em>Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReply()
   * @generated
   * @ordered
   */
  protected ReplyStatement reply;

  /**
   * The cached value of the '{@link #getRaise() <em>Raise</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRaise()
   * @generated
   * @ordered
   */
  protected RaiseStatement raise;

  /**
   * The cached value of the '{@link #getReceive() <em>Receive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReceive()
   * @generated
   * @ordered
   */
  protected ReceiveStatement receive;

  /**
   * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTrigger()
   * @generated
   * @ordered
   */
  protected TriggerStatement trigger;

  /**
   * The cached value of the '{@link #getGetCall() <em>Get Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetCall()
   * @generated
   * @ordered
   */
  protected GetCallStatement getCall;

  /**
   * The cached value of the '{@link #getGetReply() <em>Get Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetReply()
   * @generated
   * @ordered
   */
  protected GetReplyStatement getReply;

  /**
   * The cached value of the '{@link #getCatch() <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCatch()
   * @generated
   * @ordered
   */
  protected CatchStatement catch_;

  /**
   * The cached value of the '{@link #getCheck() <em>Check</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCheck()
   * @generated
   * @ordered
   */
  protected CheckStatement check;

  /**
   * The cached value of the '{@link #getClear() <em>Clear</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClear()
   * @generated
   * @ordered
   */
  protected ClearStatement clear;

  /**
   * The cached value of the '{@link #getStart() <em>Start</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStart()
   * @generated
   * @ordered
   */
  protected StartStatement start;

  /**
   * The cached value of the '{@link #getStop() <em>Stop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStop()
   * @generated
   * @ordered
   */
  protected StopStatement stop;

  /**
   * The cached value of the '{@link #getHalt() <em>Halt</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHalt()
   * @generated
   * @ordered
   */
  protected HaltStatement halt;

  /**
   * The cached value of the '{@link #getCheckState() <em>Check State</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCheckState()
   * @generated
   * @ordered
   */
  protected CheckStateStatement checkState;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CommunicationStatementsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getCommunicationStatements();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendStatement getSend()
  {
    return send;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSend(SendStatement newSend, NotificationChain msgs)
  {
    SendStatement oldSend = send;
    send = newSend;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__SEND, oldSend, newSend);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSend(SendStatement newSend)
  {
    if (newSend != send)
    {
      NotificationChain msgs = null;
      if (send != null)
        msgs = ((InternalEObject)send).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__SEND, null, msgs);
      if (newSend != null)
        msgs = ((InternalEObject)newSend).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__SEND, null, msgs);
      msgs = basicSetSend(newSend, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__SEND, newSend, newSend));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallStatement getCall()
  {
    return call;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCall(CallStatement newCall, NotificationChain msgs)
  {
    CallStatement oldCall = call;
    call = newCall;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CALL, oldCall, newCall);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCall(CallStatement newCall)
  {
    if (newCall != call)
    {
      NotificationChain msgs = null;
      if (call != null)
        msgs = ((InternalEObject)call).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CALL, null, msgs);
      if (newCall != null)
        msgs = ((InternalEObject)newCall).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CALL, null, msgs);
      msgs = basicSetCall(newCall, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CALL, newCall, newCall));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReplyStatement getReply()
  {
    return reply;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReply(ReplyStatement newReply, NotificationChain msgs)
  {
    ReplyStatement oldReply = reply;
    reply = newReply;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__REPLY, oldReply, newReply);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReply(ReplyStatement newReply)
  {
    if (newReply != reply)
    {
      NotificationChain msgs = null;
      if (reply != null)
        msgs = ((InternalEObject)reply).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__REPLY, null, msgs);
      if (newReply != null)
        msgs = ((InternalEObject)newReply).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__REPLY, null, msgs);
      msgs = basicSetReply(newReply, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__REPLY, newReply, newReply));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RaiseStatement getRaise()
  {
    return raise;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRaise(RaiseStatement newRaise, NotificationChain msgs)
  {
    RaiseStatement oldRaise = raise;
    raise = newRaise;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__RAISE, oldRaise, newRaise);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRaise(RaiseStatement newRaise)
  {
    if (newRaise != raise)
    {
      NotificationChain msgs = null;
      if (raise != null)
        msgs = ((InternalEObject)raise).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__RAISE, null, msgs);
      if (newRaise != null)
        msgs = ((InternalEObject)newRaise).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__RAISE, null, msgs);
      msgs = basicSetRaise(newRaise, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__RAISE, newRaise, newRaise));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReceiveStatement getReceive()
  {
    return receive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReceive(ReceiveStatement newReceive, NotificationChain msgs)
  {
    ReceiveStatement oldReceive = receive;
    receive = newReceive;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE, oldReceive, newReceive);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReceive(ReceiveStatement newReceive)
  {
    if (newReceive != receive)
    {
      NotificationChain msgs = null;
      if (receive != null)
        msgs = ((InternalEObject)receive).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE, null, msgs);
      if (newReceive != null)
        msgs = ((InternalEObject)newReceive).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE, null, msgs);
      msgs = basicSetReceive(newReceive, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE, newReceive, newReceive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TriggerStatement getTrigger()
  {
    return trigger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTrigger(TriggerStatement newTrigger, NotificationChain msgs)
  {
    TriggerStatement oldTrigger = trigger;
    trigger = newTrigger;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER, oldTrigger, newTrigger);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTrigger(TriggerStatement newTrigger)
  {
    if (newTrigger != trigger)
    {
      NotificationChain msgs = null;
      if (trigger != null)
        msgs = ((InternalEObject)trigger).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER, null, msgs);
      if (newTrigger != null)
        msgs = ((InternalEObject)newTrigger).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER, null, msgs);
      msgs = basicSetTrigger(newTrigger, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER, newTrigger, newTrigger));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetCallStatement getGetCall()
  {
    return getCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetCall(GetCallStatement newGetCall, NotificationChain msgs)
  {
    GetCallStatement oldGetCall = getCall;
    getCall = newGetCall;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL, oldGetCall, newGetCall);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetCall(GetCallStatement newGetCall)
  {
    if (newGetCall != getCall)
    {
      NotificationChain msgs = null;
      if (getCall != null)
        msgs = ((InternalEObject)getCall).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL, null, msgs);
      if (newGetCall != null)
        msgs = ((InternalEObject)newGetCall).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL, null, msgs);
      msgs = basicSetGetCall(newGetCall, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL, newGetCall, newGetCall));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetReplyStatement getGetReply()
  {
    return getReply;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetReply(GetReplyStatement newGetReply, NotificationChain msgs)
  {
    GetReplyStatement oldGetReply = getReply;
    getReply = newGetReply;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY, oldGetReply, newGetReply);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetReply(GetReplyStatement newGetReply)
  {
    if (newGetReply != getReply)
    {
      NotificationChain msgs = null;
      if (getReply != null)
        msgs = ((InternalEObject)getReply).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY, null, msgs);
      if (newGetReply != null)
        msgs = ((InternalEObject)newGetReply).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY, null, msgs);
      msgs = basicSetGetReply(newGetReply, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY, newGetReply, newGetReply));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CatchStatement getCatch()
  {
    return catch_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCatch(CatchStatement newCatch, NotificationChain msgs)
  {
    CatchStatement oldCatch = catch_;
    catch_ = newCatch;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CATCH, oldCatch, newCatch);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCatch(CatchStatement newCatch)
  {
    if (newCatch != catch_)
    {
      NotificationChain msgs = null;
      if (catch_ != null)
        msgs = ((InternalEObject)catch_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CATCH, null, msgs);
      if (newCatch != null)
        msgs = ((InternalEObject)newCatch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CATCH, null, msgs);
      msgs = basicSetCatch(newCatch, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CATCH, newCatch, newCatch));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckStatement getCheck()
  {
    return check;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCheck(CheckStatement newCheck, NotificationChain msgs)
  {
    CheckStatement oldCheck = check;
    check = newCheck;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CHECK, oldCheck, newCheck);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCheck(CheckStatement newCheck)
  {
    if (newCheck != check)
    {
      NotificationChain msgs = null;
      if (check != null)
        msgs = ((InternalEObject)check).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CHECK, null, msgs);
      if (newCheck != null)
        msgs = ((InternalEObject)newCheck).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CHECK, null, msgs);
      msgs = basicSetCheck(newCheck, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CHECK, newCheck, newCheck));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClearStatement getClear()
  {
    return clear;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetClear(ClearStatement newClear, NotificationChain msgs)
  {
    ClearStatement oldClear = clear;
    clear = newClear;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR, oldClear, newClear);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClear(ClearStatement newClear)
  {
    if (newClear != clear)
    {
      NotificationChain msgs = null;
      if (clear != null)
        msgs = ((InternalEObject)clear).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR, null, msgs);
      if (newClear != null)
        msgs = ((InternalEObject)newClear).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR, null, msgs);
      msgs = basicSetClear(newClear, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR, newClear, newClear));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StartStatement getStart()
  {
    return start;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStart(StartStatement newStart, NotificationChain msgs)
  {
    StartStatement oldStart = start;
    start = newStart;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__START, oldStart, newStart);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStart(StartStatement newStart)
  {
    if (newStart != start)
    {
      NotificationChain msgs = null;
      if (start != null)
        msgs = ((InternalEObject)start).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__START, null, msgs);
      if (newStart != null)
        msgs = ((InternalEObject)newStart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__START, null, msgs);
      msgs = basicSetStart(newStart, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__START, newStart, newStart));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StopStatement getStop()
  {
    return stop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStop(StopStatement newStop, NotificationChain msgs)
  {
    StopStatement oldStop = stop;
    stop = newStop;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__STOP, oldStop, newStop);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStop(StopStatement newStop)
  {
    if (newStop != stop)
    {
      NotificationChain msgs = null;
      if (stop != null)
        msgs = ((InternalEObject)stop).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__STOP, null, msgs);
      if (newStop != null)
        msgs = ((InternalEObject)newStop).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__STOP, null, msgs);
      msgs = basicSetStop(newStop, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__STOP, newStop, newStop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HaltStatement getHalt()
  {
    return halt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetHalt(HaltStatement newHalt, NotificationChain msgs)
  {
    HaltStatement oldHalt = halt;
    halt = newHalt;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__HALT, oldHalt, newHalt);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHalt(HaltStatement newHalt)
  {
    if (newHalt != halt)
    {
      NotificationChain msgs = null;
      if (halt != null)
        msgs = ((InternalEObject)halt).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__HALT, null, msgs);
      if (newHalt != null)
        msgs = ((InternalEObject)newHalt).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__HALT, null, msgs);
      msgs = basicSetHalt(newHalt, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__HALT, newHalt, newHalt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckStateStatement getCheckState()
  {
    return checkState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCheckState(CheckStateStatement newCheckState, NotificationChain msgs)
  {
    CheckStateStatement oldCheckState = checkState;
    checkState = newCheckState;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE, oldCheckState, newCheckState);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCheckState(CheckStateStatement newCheckState)
  {
    if (newCheckState != checkState)
    {
      NotificationChain msgs = null;
      if (checkState != null)
        msgs = ((InternalEObject)checkState).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE, null, msgs);
      if (newCheckState != null)
        msgs = ((InternalEObject)newCheckState).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE, null, msgs);
      msgs = basicSetCheckState(newCheckState, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE, newCheckState, newCheckState));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.COMMUNICATION_STATEMENTS__SEND:
        return basicSetSend(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__CALL:
        return basicSetCall(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__REPLY:
        return basicSetReply(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__RAISE:
        return basicSetRaise(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE:
        return basicSetReceive(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER:
        return basicSetTrigger(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL:
        return basicSetGetCall(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY:
        return basicSetGetReply(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__CATCH:
        return basicSetCatch(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK:
        return basicSetCheck(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR:
        return basicSetClear(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__START:
        return basicSetStart(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__STOP:
        return basicSetStop(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__HALT:
        return basicSetHalt(null, msgs);
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE:
        return basicSetCheckState(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.COMMUNICATION_STATEMENTS__SEND:
        return getSend();
      case TTCN3Package.COMMUNICATION_STATEMENTS__CALL:
        return getCall();
      case TTCN3Package.COMMUNICATION_STATEMENTS__REPLY:
        return getReply();
      case TTCN3Package.COMMUNICATION_STATEMENTS__RAISE:
        return getRaise();
      case TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE:
        return getReceive();
      case TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER:
        return getTrigger();
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL:
        return getGetCall();
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY:
        return getGetReply();
      case TTCN3Package.COMMUNICATION_STATEMENTS__CATCH:
        return getCatch();
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK:
        return getCheck();
      case TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR:
        return getClear();
      case TTCN3Package.COMMUNICATION_STATEMENTS__START:
        return getStart();
      case TTCN3Package.COMMUNICATION_STATEMENTS__STOP:
        return getStop();
      case TTCN3Package.COMMUNICATION_STATEMENTS__HALT:
        return getHalt();
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE:
        return getCheckState();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.COMMUNICATION_STATEMENTS__SEND:
        setSend((SendStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CALL:
        setCall((CallStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__REPLY:
        setReply((ReplyStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__RAISE:
        setRaise((RaiseStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE:
        setReceive((ReceiveStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER:
        setTrigger((TriggerStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL:
        setGetCall((GetCallStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY:
        setGetReply((GetReplyStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CATCH:
        setCatch((CatchStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK:
        setCheck((CheckStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR:
        setClear((ClearStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__START:
        setStart((StartStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__STOP:
        setStop((StopStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__HALT:
        setHalt((HaltStatement)newValue);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE:
        setCheckState((CheckStateStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMMUNICATION_STATEMENTS__SEND:
        setSend((SendStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CALL:
        setCall((CallStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__REPLY:
        setReply((ReplyStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__RAISE:
        setRaise((RaiseStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE:
        setReceive((ReceiveStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER:
        setTrigger((TriggerStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL:
        setGetCall((GetCallStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY:
        setGetReply((GetReplyStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CATCH:
        setCatch((CatchStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK:
        setCheck((CheckStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR:
        setClear((ClearStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__START:
        setStart((StartStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__STOP:
        setStop((StopStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__HALT:
        setHalt((HaltStatement)null);
        return;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE:
        setCheckState((CheckStateStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMMUNICATION_STATEMENTS__SEND:
        return send != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CALL:
        return call != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__REPLY:
        return reply != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__RAISE:
        return raise != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__RECEIVE:
        return receive != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__TRIGGER:
        return trigger != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_CALL:
        return getCall != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__GET_REPLY:
        return getReply != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CATCH:
        return catch_ != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK:
        return check != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CLEAR:
        return clear != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__START:
        return start != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__STOP:
        return stop != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__HALT:
        return halt != null;
      case TTCN3Package.COMMUNICATION_STATEMENTS__CHECK_STATE:
        return checkState != null;
    }
    return super.eIsSet(featureID);
  }

} //CommunicationStatementsImpl
