/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateActualParList;
import de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar;
import de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Actual Par List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateActualParListImpl#getActual <em>Actual</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateActualParListImpl#getAssign <em>Assign</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateActualParListImpl extends MinimalEObjectImpl.Container implements TemplateActualParList
{
  /**
   * The cached value of the '{@link #getActual() <em>Actual</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getActual()
   * @generated
   * @ordered
   */
  protected EList<TemplateInstanceActualPar> actual;

  /**
   * The cached value of the '{@link #getAssign() <em>Assign</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssign()
   * @generated
   * @ordered
   */
  protected EList<TemplateInstanceAssignment> assign;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateActualParListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTemplateActualParList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TemplateInstanceActualPar> getActual()
  {
    if (actual == null)
    {
      actual = new EObjectContainmentEList<TemplateInstanceActualPar>(TemplateInstanceActualPar.class, this, TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ACTUAL);
    }
    return actual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TemplateInstanceAssignment> getAssign()
  {
    if (assign == null)
    {
      assign = new EObjectContainmentEList<TemplateInstanceAssignment>(TemplateInstanceAssignment.class, this, TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ASSIGN);
    }
    return assign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ACTUAL:
        return ((InternalEList<?>)getActual()).basicRemove(otherEnd, msgs);
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ASSIGN:
        return ((InternalEList<?>)getAssign()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ACTUAL:
        return getActual();
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ASSIGN:
        return getAssign();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ACTUAL:
        getActual().clear();
        getActual().addAll((Collection<? extends TemplateInstanceActualPar>)newValue);
        return;
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ASSIGN:
        getAssign().clear();
        getAssign().addAll((Collection<? extends TemplateInstanceAssignment>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ACTUAL:
        getActual().clear();
        return;
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ASSIGN:
        getAssign().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ACTUAL:
        return actual != null && !actual.isEmpty();
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST__ASSIGN:
        return assign != null && !assign.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TemplateActualParListImpl
