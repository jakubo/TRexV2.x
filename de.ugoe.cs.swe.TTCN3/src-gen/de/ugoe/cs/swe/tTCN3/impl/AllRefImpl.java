/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllRef;
import de.ugoe.cs.swe.tTCN3.GroupRefList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>All Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AllRefImpl#getGroupList <em>Group List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AllRefImpl#getIdList <em>Id List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AllRefImpl extends MinimalEObjectImpl.Container implements AllRef
{
  /**
   * The cached value of the '{@link #getGroupList() <em>Group List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroupList()
   * @generated
   * @ordered
   */
  protected GroupRefList groupList;

  /**
   * The cached value of the '{@link #getIdList() <em>Id List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdList()
   * @generated
   * @ordered
   */
  protected TTCN3ReferenceList idList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AllRefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAllRef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupRefList getGroupList()
  {
    return groupList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroupList(GroupRefList newGroupList, NotificationChain msgs)
  {
    GroupRefList oldGroupList = groupList;
    groupList = newGroupList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_REF__GROUP_LIST, oldGroupList, newGroupList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGroupList(GroupRefList newGroupList)
  {
    if (newGroupList != groupList)
    {
      NotificationChain msgs = null;
      if (groupList != null)
        msgs = ((InternalEObject)groupList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALL_REF__GROUP_LIST, null, msgs);
      if (newGroupList != null)
        msgs = ((InternalEObject)newGroupList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALL_REF__GROUP_LIST, null, msgs);
      msgs = basicSetGroupList(newGroupList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_REF__GROUP_LIST, newGroupList, newGroupList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3ReferenceList getIdList()
  {
    return idList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIdList(TTCN3ReferenceList newIdList, NotificationChain msgs)
  {
    TTCN3ReferenceList oldIdList = idList;
    idList = newIdList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_REF__ID_LIST, oldIdList, newIdList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdList(TTCN3ReferenceList newIdList)
  {
    if (newIdList != idList)
    {
      NotificationChain msgs = null;
      if (idList != null)
        msgs = ((InternalEObject)idList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALL_REF__ID_LIST, null, msgs);
      if (newIdList != null)
        msgs = ((InternalEObject)newIdList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALL_REF__ID_LIST, null, msgs);
      msgs = basicSetIdList(newIdList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_REF__ID_LIST, newIdList, newIdList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_REF__GROUP_LIST:
        return basicSetGroupList(null, msgs);
      case TTCN3Package.ALL_REF__ID_LIST:
        return basicSetIdList(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_REF__GROUP_LIST:
        return getGroupList();
      case TTCN3Package.ALL_REF__ID_LIST:
        return getIdList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_REF__GROUP_LIST:
        setGroupList((GroupRefList)newValue);
        return;
      case TTCN3Package.ALL_REF__ID_LIST:
        setIdList((TTCN3ReferenceList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_REF__GROUP_LIST:
        setGroupList((GroupRefList)null);
        return;
      case TTCN3Package.ALL_REF__ID_LIST:
        setIdList((TTCN3ReferenceList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_REF__GROUP_LIST:
        return groupList != null;
      case TTCN3Package.ALL_REF__ID_LIST:
        return idList != null;
    }
    return super.eIsSet(featureID);
  }

} //AllRefImpl
