/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ModuleDefinition;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.Visibility;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Module Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl#getDef <em>Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl#getPublicGroup <em>Public Group</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl#getPrivateFriend <em>Private Friend</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModuleDefinitionImpl extends MinimalEObjectImpl.Container implements ModuleDefinition
{
  /**
   * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected static final Visibility VISIBILITY_EDEFAULT = Visibility.PUBLIC;

  /**
   * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected Visibility visibility = VISIBILITY_EDEFAULT;

  /**
   * The cached value of the '{@link #getDef() <em>Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDef()
   * @generated
   * @ordered
   */
  protected EObject def;

  /**
   * The default value of the '{@link #getPublicGroup() <em>Public Group</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPublicGroup()
   * @generated
   * @ordered
   */
  protected static final String PUBLIC_GROUP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPublicGroup() <em>Public Group</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPublicGroup()
   * @generated
   * @ordered
   */
  protected String publicGroup = PUBLIC_GROUP_EDEFAULT;

  /**
   * The default value of the '{@link #getPrivateFriend() <em>Private Friend</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrivateFriend()
   * @generated
   * @ordered
   */
  protected static final String PRIVATE_FRIEND_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPrivateFriend() <em>Private Friend</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrivateFriend()
   * @generated
   * @ordered
   */
  protected String privateFriend = PRIVATE_FRIEND_EDEFAULT;

  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected WithStatement statement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModuleDefinitionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getModuleDefinition();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Visibility getVisibility()
  {
    return visibility;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVisibility(Visibility newVisibility)
  {
    Visibility oldVisibility = visibility;
    visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__VISIBILITY, oldVisibility, visibility));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EObject getDef()
  {
    return def;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDef(EObject newDef, NotificationChain msgs)
  {
    EObject oldDef = def;
    def = newDef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__DEF, oldDef, newDef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDef(EObject newDef)
  {
    if (newDef != def)
    {
      NotificationChain msgs = null;
      if (def != null)
        msgs = ((InternalEObject)def).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_DEFINITION__DEF, null, msgs);
      if (newDef != null)
        msgs = ((InternalEObject)newDef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_DEFINITION__DEF, null, msgs);
      msgs = basicSetDef(newDef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__DEF, newDef, newDef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPublicGroup()
  {
    return publicGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPublicGroup(String newPublicGroup)
  {
    String oldPublicGroup = publicGroup;
    publicGroup = newPublicGroup;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__PUBLIC_GROUP, oldPublicGroup, publicGroup));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPrivateFriend()
  {
    return privateFriend;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrivateFriend(String newPrivateFriend)
  {
    String oldPrivateFriend = privateFriend;
    privateFriend = newPrivateFriend;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__PRIVATE_FRIEND, oldPrivateFriend, privateFriend));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(WithStatement newStatement, NotificationChain msgs)
  {
    WithStatement oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(WithStatement newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_DEFINITION__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_DEFINITION__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_DEFINITION__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_DEFINITION__DEF:
        return basicSetDef(null, msgs);
      case TTCN3Package.MODULE_DEFINITION__STATEMENT:
        return basicSetStatement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_DEFINITION__VISIBILITY:
        return getVisibility();
      case TTCN3Package.MODULE_DEFINITION__DEF:
        return getDef();
      case TTCN3Package.MODULE_DEFINITION__PUBLIC_GROUP:
        return getPublicGroup();
      case TTCN3Package.MODULE_DEFINITION__PRIVATE_FRIEND:
        return getPrivateFriend();
      case TTCN3Package.MODULE_DEFINITION__STATEMENT:
        return getStatement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_DEFINITION__VISIBILITY:
        setVisibility((Visibility)newValue);
        return;
      case TTCN3Package.MODULE_DEFINITION__DEF:
        setDef((EObject)newValue);
        return;
      case TTCN3Package.MODULE_DEFINITION__PUBLIC_GROUP:
        setPublicGroup((String)newValue);
        return;
      case TTCN3Package.MODULE_DEFINITION__PRIVATE_FRIEND:
        setPrivateFriend((String)newValue);
        return;
      case TTCN3Package.MODULE_DEFINITION__STATEMENT:
        setStatement((WithStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_DEFINITION__VISIBILITY:
        setVisibility(VISIBILITY_EDEFAULT);
        return;
      case TTCN3Package.MODULE_DEFINITION__DEF:
        setDef((EObject)null);
        return;
      case TTCN3Package.MODULE_DEFINITION__PUBLIC_GROUP:
        setPublicGroup(PUBLIC_GROUP_EDEFAULT);
        return;
      case TTCN3Package.MODULE_DEFINITION__PRIVATE_FRIEND:
        setPrivateFriend(PRIVATE_FRIEND_EDEFAULT);
        return;
      case TTCN3Package.MODULE_DEFINITION__STATEMENT:
        setStatement((WithStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_DEFINITION__VISIBILITY:
        return visibility != VISIBILITY_EDEFAULT;
      case TTCN3Package.MODULE_DEFINITION__DEF:
        return def != null;
      case TTCN3Package.MODULE_DEFINITION__PUBLIC_GROUP:
        return PUBLIC_GROUP_EDEFAULT == null ? publicGroup != null : !PUBLIC_GROUP_EDEFAULT.equals(publicGroup);
      case TTCN3Package.MODULE_DEFINITION__PRIVATE_FRIEND:
        return PRIVATE_FRIEND_EDEFAULT == null ? privateFriend != null : !PRIVATE_FRIEND_EDEFAULT.equals(privateFriend);
      case TTCN3Package.MODULE_DEFINITION__STATEMENT:
        return statement != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (visibility: ");
    result.append(visibility);
    result.append(", publicGroup: ");
    result.append(publicGroup);
    result.append(", privateFriend: ");
    result.append(privateFriend);
    result.append(')');
    return result.toString();
  }

} //ModuleDefinitionImpl
