/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.CatchOpParameter;
import de.ugoe.cs.swe.tTCN3.PortCatchOp;
import de.ugoe.cs.swe.tTCN3.PortRedirect;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Catch Op</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortCatchOpImpl#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortCatchOpImpl#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortCatchOpImpl extends CheckPortOpsPresentImpl implements PortCatchOp
{
  /**
   * The cached value of the '{@link #getParam() <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParam()
   * @generated
   * @ordered
   */
  protected CatchOpParameter param;

  /**
   * The cached value of the '{@link #getRedirect() <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRedirect()
   * @generated
   * @ordered
   */
  protected PortRedirect redirect;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortCatchOpImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortCatchOp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CatchOpParameter getParam()
  {
    return param;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParam(CatchOpParameter newParam, NotificationChain msgs)
  {
    CatchOpParameter oldParam = param;
    param = newParam;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_CATCH_OP__PARAM, oldParam, newParam);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParam(CatchOpParameter newParam)
  {
    if (newParam != param)
    {
      NotificationChain msgs = null;
      if (param != null)
        msgs = ((InternalEObject)param).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_CATCH_OP__PARAM, null, msgs);
      if (newParam != null)
        msgs = ((InternalEObject)newParam).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_CATCH_OP__PARAM, null, msgs);
      msgs = basicSetParam(newParam, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_CATCH_OP__PARAM, newParam, newParam));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRedirect getRedirect()
  {
    return redirect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRedirect(PortRedirect newRedirect, NotificationChain msgs)
  {
    PortRedirect oldRedirect = redirect;
    redirect = newRedirect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_CATCH_OP__REDIRECT, oldRedirect, newRedirect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRedirect(PortRedirect newRedirect)
  {
    if (newRedirect != redirect)
    {
      NotificationChain msgs = null;
      if (redirect != null)
        msgs = ((InternalEObject)redirect).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_CATCH_OP__REDIRECT, null, msgs);
      if (newRedirect != null)
        msgs = ((InternalEObject)newRedirect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_CATCH_OP__REDIRECT, null, msgs);
      msgs = basicSetRedirect(newRedirect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_CATCH_OP__REDIRECT, newRedirect, newRedirect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CATCH_OP__PARAM:
        return basicSetParam(null, msgs);
      case TTCN3Package.PORT_CATCH_OP__REDIRECT:
        return basicSetRedirect(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CATCH_OP__PARAM:
        return getParam();
      case TTCN3Package.PORT_CATCH_OP__REDIRECT:
        return getRedirect();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CATCH_OP__PARAM:
        setParam((CatchOpParameter)newValue);
        return;
      case TTCN3Package.PORT_CATCH_OP__REDIRECT:
        setRedirect((PortRedirect)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CATCH_OP__PARAM:
        setParam((CatchOpParameter)null);
        return;
      case TTCN3Package.PORT_CATCH_OP__REDIRECT:
        setRedirect((PortRedirect)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_CATCH_OP__PARAM:
        return param != null;
      case TTCN3Package.PORT_CATCH_OP__REDIRECT:
        return redirect != null;
    }
    return super.eIsSet(featureID);
  }

} //PortCatchOpImpl
