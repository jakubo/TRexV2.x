/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.Bound;
import de.ugoe.cs.swe.tTCN3.Range;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RangeImpl#getB1 <em>B1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RangeImpl#getB2 <em>B2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RangeImpl extends MinimalEObjectImpl.Container implements Range
{
  /**
   * The cached value of the '{@link #getB1() <em>B1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getB1()
   * @generated
   * @ordered
   */
  protected Bound b1;

  /**
   * The cached value of the '{@link #getB2() <em>B2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getB2()
   * @generated
   * @ordered
   */
  protected Bound b2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RangeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getRange();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Bound getB1()
  {
    return b1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetB1(Bound newB1, NotificationChain msgs)
  {
    Bound oldB1 = b1;
    b1 = newB1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.RANGE__B1, oldB1, newB1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setB1(Bound newB1)
  {
    if (newB1 != b1)
    {
      NotificationChain msgs = null;
      if (b1 != null)
        msgs = ((InternalEObject)b1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RANGE__B1, null, msgs);
      if (newB1 != null)
        msgs = ((InternalEObject)newB1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RANGE__B1, null, msgs);
      msgs = basicSetB1(newB1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.RANGE__B1, newB1, newB1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Bound getB2()
  {
    return b2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetB2(Bound newB2, NotificationChain msgs)
  {
    Bound oldB2 = b2;
    b2 = newB2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.RANGE__B2, oldB2, newB2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setB2(Bound newB2)
  {
    if (newB2 != b2)
    {
      NotificationChain msgs = null;
      if (b2 != null)
        msgs = ((InternalEObject)b2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RANGE__B2, null, msgs);
      if (newB2 != null)
        msgs = ((InternalEObject)newB2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RANGE__B2, null, msgs);
      msgs = basicSetB2(newB2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.RANGE__B2, newB2, newB2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.RANGE__B1:
        return basicSetB1(null, msgs);
      case TTCN3Package.RANGE__B2:
        return basicSetB2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.RANGE__B1:
        return getB1();
      case TTCN3Package.RANGE__B2:
        return getB2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.RANGE__B1:
        setB1((Bound)newValue);
        return;
      case TTCN3Package.RANGE__B2:
        setB2((Bound)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.RANGE__B1:
        setB1((Bound)null);
        return;
      case TTCN3Package.RANGE__B2:
        setB2((Bound)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.RANGE__B1:
        return b1 != null;
      case TTCN3Package.RANGE__B2:
        return b2 != null;
    }
    return super.eIsSet(featureID);
  }

} //RangeImpl
