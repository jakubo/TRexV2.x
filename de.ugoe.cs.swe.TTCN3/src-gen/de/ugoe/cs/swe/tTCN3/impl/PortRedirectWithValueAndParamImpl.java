/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.PortRedirectWithValueAndParam;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Redirect With Value And Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PortRedirectWithValueAndParamImpl extends MinimalEObjectImpl.Container implements PortRedirectWithValueAndParam
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortRedirectWithValueAndParamImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortRedirectWithValueAndParam();
  }

} //PortRedirectWithValueAndParamImpl
