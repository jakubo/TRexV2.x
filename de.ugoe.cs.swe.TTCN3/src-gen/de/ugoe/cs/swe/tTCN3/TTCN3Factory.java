/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package
 * @generated
 */
public interface TTCN3Factory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  TTCN3Factory eINSTANCE = de.ugoe.cs.swe.tTCN3.impl.TTCN3FactoryImpl.init();

  /**
   * Returns a new object of class '<em>File</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>File</em>'.
   * @generated
   */
  TTCN3File createTTCN3File();

  /**
   * Returns a new object of class '<em>Const Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Const Def</em>'.
   * @generated
   */
  ConstDef createConstDef();

  /**
   * Returns a new object of class '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type</em>'.
   * @generated
   */
  Type createType();

  /**
   * Returns a new object of class '<em>Type Reference Tail Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Reference Tail Type</em>'.
   * @generated
   */
  TypeReferenceTailType createTypeReferenceTailType();

  /**
   * Returns a new object of class '<em>Type Reference Tail</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Reference Tail</em>'.
   * @generated
   */
  TypeReferenceTail createTypeReferenceTail();

  /**
   * Returns a new object of class '<em>Type Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Reference</em>'.
   * @generated
   */
  TypeReference createTypeReference();

  /**
   * Returns a new object of class '<em>Spec Type Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Spec Type Element</em>'.
   * @generated
   */
  SpecTypeElement createSpecTypeElement();

  /**
   * Returns a new object of class '<em>Array Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Def</em>'.
   * @generated
   */
  ArrayDef createArrayDef();

  /**
   * Returns a new object of class '<em>Module</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module</em>'.
   * @generated
   */
  TTCN3Module createTTCN3Module();

  /**
   * Returns a new object of class '<em>Language Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Language Spec</em>'.
   * @generated
   */
  LanguageSpec createLanguageSpec();

  /**
   * Returns a new object of class '<em>Module Par Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Par Def</em>'.
   * @generated
   */
  ModuleParDef createModuleParDef();

  /**
   * Returns a new object of class '<em>Module Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Par</em>'.
   * @generated
   */
  ModulePar createModulePar();

  /**
   * Returns a new object of class '<em>Module Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Par List</em>'.
   * @generated
   */
  ModuleParList createModuleParList();

  /**
   * Returns a new object of class '<em>Module Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Parameter</em>'.
   * @generated
   */
  ModuleParameter createModuleParameter();

  /**
   * Returns a new object of class '<em>Multityped Module Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Multityped Module Par List</em>'.
   * @generated
   */
  MultitypedModuleParList createMultitypedModuleParList();

  /**
   * Returns a new object of class '<em>Module Definitions List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Definitions List</em>'.
   * @generated
   */
  ModuleDefinitionsList createModuleDefinitionsList();

  /**
   * Returns a new object of class '<em>Module Definition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Definition</em>'.
   * @generated
   */
  ModuleDefinition createModuleDefinition();

  /**
   * Returns a new object of class '<em>Friend Module Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Friend Module Def</em>'.
   * @generated
   */
  FriendModuleDef createFriendModuleDef();

  /**
   * Returns a new object of class '<em>Group Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Group Def</em>'.
   * @generated
   */
  GroupDef createGroupDef();

  /**
   * Returns a new object of class '<em>Ext Function Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ext Function Def</em>'.
   * @generated
   */
  ExtFunctionDef createExtFunctionDef();

  /**
   * Returns a new object of class '<em>Ext Const Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ext Const Def</em>'.
   * @generated
   */
  ExtConstDef createExtConstDef();

  /**
   * Returns a new object of class '<em>Identifier Object List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifier Object List</em>'.
   * @generated
   */
  IdentifierObjectList createIdentifierObjectList();

  /**
   * Returns a new object of class '<em>Named Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Named Object</em>'.
   * @generated
   */
  NamedObject createNamedObject();

  /**
   * Returns a new object of class '<em>Import Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Def</em>'.
   * @generated
   */
  ImportDef createImportDef();

  /**
   * Returns a new object of class '<em>All With Excepts</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All With Excepts</em>'.
   * @generated
   */
  AllWithExcepts createAllWithExcepts();

  /**
   * Returns a new object of class '<em>Excepts Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Excepts Def</em>'.
   * @generated
   */
  ExceptsDef createExceptsDef();

  /**
   * Returns a new object of class '<em>Except Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Spec</em>'.
   * @generated
   */
  ExceptSpec createExceptSpec();

  /**
   * Returns a new object of class '<em>Except Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Element</em>'.
   * @generated
   */
  ExceptElement createExceptElement();

  /**
   * Returns a new object of class '<em>Identifier List Or All</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifier List Or All</em>'.
   * @generated
   */
  IdentifierListOrAll createIdentifierListOrAll();

  /**
   * Returns a new object of class '<em>Except Group Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Group Spec</em>'.
   * @generated
   */
  ExceptGroupSpec createExceptGroupSpec();

  /**
   * Returns a new object of class '<em>Except Type Def Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Type Def Spec</em>'.
   * @generated
   */
  ExceptTypeDefSpec createExceptTypeDefSpec();

  /**
   * Returns a new object of class '<em>Except Template Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Template Spec</em>'.
   * @generated
   */
  ExceptTemplateSpec createExceptTemplateSpec();

  /**
   * Returns a new object of class '<em>Except Const Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Const Spec</em>'.
   * @generated
   */
  ExceptConstSpec createExceptConstSpec();

  /**
   * Returns a new object of class '<em>Except Testcase Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Testcase Spec</em>'.
   * @generated
   */
  ExceptTestcaseSpec createExceptTestcaseSpec();

  /**
   * Returns a new object of class '<em>Except Altstep Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Altstep Spec</em>'.
   * @generated
   */
  ExceptAltstepSpec createExceptAltstepSpec();

  /**
   * Returns a new object of class '<em>Except Function Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Function Spec</em>'.
   * @generated
   */
  ExceptFunctionSpec createExceptFunctionSpec();

  /**
   * Returns a new object of class '<em>Except Signature Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Signature Spec</em>'.
   * @generated
   */
  ExceptSignatureSpec createExceptSignatureSpec();

  /**
   * Returns a new object of class '<em>Except Module Par Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Except Module Par Spec</em>'.
   * @generated
   */
  ExceptModuleParSpec createExceptModuleParSpec();

  /**
   * Returns a new object of class '<em>Import Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Spec</em>'.
   * @generated
   */
  ImportSpec createImportSpec();

  /**
   * Returns a new object of class '<em>Import Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Element</em>'.
   * @generated
   */
  ImportElement createImportElement();

  /**
   * Returns a new object of class '<em>Import Group Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Group Spec</em>'.
   * @generated
   */
  ImportGroupSpec createImportGroupSpec();

  /**
   * Returns a new object of class '<em>Identifier List Or All With Except</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifier List Or All With Except</em>'.
   * @generated
   */
  IdentifierListOrAllWithExcept createIdentifierListOrAllWithExcept();

  /**
   * Returns a new object of class '<em>All With Except</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All With Except</em>'.
   * @generated
   */
  AllWithExcept createAllWithExcept();

  /**
   * Returns a new object of class '<em>Import Type Def Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Type Def Spec</em>'.
   * @generated
   */
  ImportTypeDefSpec createImportTypeDefSpec();

  /**
   * Returns a new object of class '<em>Import Template Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Template Spec</em>'.
   * @generated
   */
  ImportTemplateSpec createImportTemplateSpec();

  /**
   * Returns a new object of class '<em>Import Const Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Const Spec</em>'.
   * @generated
   */
  ImportConstSpec createImportConstSpec();

  /**
   * Returns a new object of class '<em>Import Altstep Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Altstep Spec</em>'.
   * @generated
   */
  ImportAltstepSpec createImportAltstepSpec();

  /**
   * Returns a new object of class '<em>Import Testcase Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Testcase Spec</em>'.
   * @generated
   */
  ImportTestcaseSpec createImportTestcaseSpec();

  /**
   * Returns a new object of class '<em>Import Function Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Function Spec</em>'.
   * @generated
   */
  ImportFunctionSpec createImportFunctionSpec();

  /**
   * Returns a new object of class '<em>Import Signature Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Signature Spec</em>'.
   * @generated
   */
  ImportSignatureSpec createImportSignatureSpec();

  /**
   * Returns a new object of class '<em>Import Module Par Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Module Par Spec</em>'.
   * @generated
   */
  ImportModuleParSpec createImportModuleParSpec();

  /**
   * Returns a new object of class '<em>Group Ref List With Except</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Group Ref List With Except</em>'.
   * @generated
   */
  GroupRefListWithExcept createGroupRefListWithExcept();

  /**
   * Returns a new object of class '<em>Qualified Identifier With Except</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Qualified Identifier With Except</em>'.
   * @generated
   */
  QualifiedIdentifierWithExcept createQualifiedIdentifierWithExcept();

  /**
   * Returns a new object of class '<em>All Groups With Except</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Groups With Except</em>'.
   * @generated
   */
  AllGroupsWithExcept createAllGroupsWithExcept();

  /**
   * Returns a new object of class '<em>Altstep Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Altstep Def</em>'.
   * @generated
   */
  AltstepDef createAltstepDef();

  /**
   * Returns a new object of class '<em>Altstep Local Def List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Altstep Local Def List</em>'.
   * @generated
   */
  AltstepLocalDefList createAltstepLocalDefList();

  /**
   * Returns a new object of class '<em>Altstep Local Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Altstep Local Def</em>'.
   * @generated
   */
  AltstepLocalDef createAltstepLocalDef();

  /**
   * Returns a new object of class '<em>Testcase Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Testcase Def</em>'.
   * @generated
   */
  TestcaseDef createTestcaseDef();

  /**
   * Returns a new object of class '<em>Template Or Value Formal Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Or Value Formal Par List</em>'.
   * @generated
   */
  TemplateOrValueFormalParList createTemplateOrValueFormalParList();

  /**
   * Returns a new object of class '<em>Template Or Value Formal Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Or Value Formal Par</em>'.
   * @generated
   */
  TemplateOrValueFormalPar createTemplateOrValueFormalPar();

  /**
   * Returns a new object of class '<em>Config Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Config Spec</em>'.
   * @generated
   */
  ConfigSpec createConfigSpec();

  /**
   * Returns a new object of class '<em>System Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>System Spec</em>'.
   * @generated
   */
  SystemSpec createSystemSpec();

  /**
   * Returns a new object of class '<em>Signature Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Signature Def</em>'.
   * @generated
   */
  SignatureDef createSignatureDef();

  /**
   * Returns a new object of class '<em>Exception Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Exception Spec</em>'.
   * @generated
   */
  ExceptionSpec createExceptionSpec();

  /**
   * Returns a new object of class '<em>Signature Formal Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Signature Formal Par List</em>'.
   * @generated
   */
  SignatureFormalParList createSignatureFormalParList();

  /**
   * Returns a new object of class '<em>Module Control Part</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Control Part</em>'.
   * @generated
   */
  ModuleControlPart createModuleControlPart();

  /**
   * Returns a new object of class '<em>Module Control Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Control Body</em>'.
   * @generated
   */
  ModuleControlBody createModuleControlBody();

  /**
   * Returns a new object of class '<em>Control Statement Or Def List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Control Statement Or Def List</em>'.
   * @generated
   */
  ControlStatementOrDefList createControlStatementOrDefList();

  /**
   * Returns a new object of class '<em>Control Statement Or Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Control Statement Or Def</em>'.
   * @generated
   */
  ControlStatementOrDef createControlStatementOrDef();

  /**
   * Returns a new object of class '<em>Control Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Control Statement</em>'.
   * @generated
   */
  ControlStatement createControlStatement();

  /**
   * Returns a new object of class '<em>SUT Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>SUT Statements</em>'.
   * @generated
   */
  SUTStatements createSUTStatements();

  /**
   * Returns a new object of class '<em>Action Text</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Action Text</em>'.
   * @generated
   */
  ActionText createActionText();

  /**
   * Returns a new object of class '<em>Behaviour Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Behaviour Statements</em>'.
   * @generated
   */
  BehaviourStatements createBehaviourStatements();

  /**
   * Returns a new object of class '<em>Activate Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Activate Op</em>'.
   * @generated
   */
  ActivateOp createActivateOp();

  /**
   * Returns a new object of class '<em>Deactivate Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Deactivate Statement</em>'.
   * @generated
   */
  DeactivateStatement createDeactivateStatement();

  /**
   * Returns a new object of class '<em>Label Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Label Statement</em>'.
   * @generated
   */
  LabelStatement createLabelStatement();

  /**
   * Returns a new object of class '<em>Goto Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Goto Statement</em>'.
   * @generated
   */
  GotoStatement createGotoStatement();

  /**
   * Returns a new object of class '<em>Return Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Return Statement</em>'.
   * @generated
   */
  ReturnStatement createReturnStatement();

  /**
   * Returns a new object of class '<em>Interleaved Construct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Interleaved Construct</em>'.
   * @generated
   */
  InterleavedConstruct createInterleavedConstruct();

  /**
   * Returns a new object of class '<em>Interleaved Guard List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Interleaved Guard List</em>'.
   * @generated
   */
  InterleavedGuardList createInterleavedGuardList();

  /**
   * Returns a new object of class '<em>Interleaved Guard Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Interleaved Guard Element</em>'.
   * @generated
   */
  InterleavedGuardElement createInterleavedGuardElement();

  /**
   * Returns a new object of class '<em>Interleaved Guard</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Interleaved Guard</em>'.
   * @generated
   */
  InterleavedGuard createInterleavedGuard();

  /**
   * Returns a new object of class '<em>Alt Construct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alt Construct</em>'.
   * @generated
   */
  AltConstruct createAltConstruct();

  /**
   * Returns a new object of class '<em>Alt Guard List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alt Guard List</em>'.
   * @generated
   */
  AltGuardList createAltGuardList();

  /**
   * Returns a new object of class '<em>Else Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Else Statement</em>'.
   * @generated
   */
  ElseStatement createElseStatement();

  /**
   * Returns a new object of class '<em>Guard Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Guard Statement</em>'.
   * @generated
   */
  GuardStatement createGuardStatement();

  /**
   * Returns a new object of class '<em>Guard Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Guard Op</em>'.
   * @generated
   */
  GuardOp createGuardOp();

  /**
   * Returns a new object of class '<em>Done Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Done Statement</em>'.
   * @generated
   */
  DoneStatement createDoneStatement();

  /**
   * Returns a new object of class '<em>Killed Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Killed Statement</em>'.
   * @generated
   */
  KilledStatement createKilledStatement();

  /**
   * Returns a new object of class '<em>Component Or Any</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Or Any</em>'.
   * @generated
   */
  ComponentOrAny createComponentOrAny();

  /**
   * Returns a new object of class '<em>Index Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Index Assignment</em>'.
   * @generated
   */
  IndexAssignment createIndexAssignment();

  /**
   * Returns a new object of class '<em>Index Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Index Spec</em>'.
   * @generated
   */
  IndexSpec createIndexSpec();

  /**
   * Returns a new object of class '<em>Get Reply Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Get Reply Statement</em>'.
   * @generated
   */
  GetReplyStatement createGetReplyStatement();

  /**
   * Returns a new object of class '<em>Check Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Check Statement</em>'.
   * @generated
   */
  CheckStatement createCheckStatement();

  /**
   * Returns a new object of class '<em>Port Check Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Check Op</em>'.
   * @generated
   */
  PortCheckOp createPortCheckOp();

  /**
   * Returns a new object of class '<em>Check Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Check Parameter</em>'.
   * @generated
   */
  CheckParameter createCheckParameter();

  /**
   * Returns a new object of class '<em>Redirect Present</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Redirect Present</em>'.
   * @generated
   */
  RedirectPresent createRedirectPresent();

  /**
   * Returns a new object of class '<em>From Clause Present</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>From Clause Present</em>'.
   * @generated
   */
  FromClausePresent createFromClausePresent();

  /**
   * Returns a new object of class '<em>Check Port Ops Present</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Check Port Ops Present</em>'.
   * @generated
   */
  CheckPortOpsPresent createCheckPortOpsPresent();

  /**
   * Returns a new object of class '<em>Port Get Reply Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Get Reply Op</em>'.
   * @generated
   */
  PortGetReplyOp createPortGetReplyOp();

  /**
   * Returns a new object of class '<em>Port Redirect With Value And Param</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Redirect With Value And Param</em>'.
   * @generated
   */
  PortRedirectWithValueAndParam createPortRedirectWithValueAndParam();

  /**
   * Returns a new object of class '<em>Redirect With Value And Param Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Redirect With Value And Param Spec</em>'.
   * @generated
   */
  RedirectWithValueAndParamSpec createRedirectWithValueAndParamSpec();

  /**
   * Returns a new object of class '<em>Value Match Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value Match Spec</em>'.
   * @generated
   */
  ValueMatchSpec createValueMatchSpec();

  /**
   * Returns a new object of class '<em>Catch Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Catch Statement</em>'.
   * @generated
   */
  CatchStatement createCatchStatement();

  /**
   * Returns a new object of class '<em>Port Catch Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Catch Op</em>'.
   * @generated
   */
  PortCatchOp createPortCatchOp();

  /**
   * Returns a new object of class '<em>Catch Op Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Catch Op Parameter</em>'.
   * @generated
   */
  CatchOpParameter createCatchOpParameter();

  /**
   * Returns a new object of class '<em>Get Call Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Get Call Statement</em>'.
   * @generated
   */
  GetCallStatement createGetCallStatement();

  /**
   * Returns a new object of class '<em>Port Get Call Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Get Call Op</em>'.
   * @generated
   */
  PortGetCallOp createPortGetCallOp();

  /**
   * Returns a new object of class '<em>Port Redirect With Param</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Redirect With Param</em>'.
   * @generated
   */
  PortRedirectWithParam createPortRedirectWithParam();

  /**
   * Returns a new object of class '<em>Redirect With Param Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Redirect With Param Spec</em>'.
   * @generated
   */
  RedirectWithParamSpec createRedirectWithParamSpec();

  /**
   * Returns a new object of class '<em>Param Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Param Spec</em>'.
   * @generated
   */
  ParamSpec createParamSpec();

  /**
   * Returns a new object of class '<em>Param Assignment List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Param Assignment List</em>'.
   * @generated
   */
  ParamAssignmentList createParamAssignmentList();

  /**
   * Returns a new object of class '<em>Assignment List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assignment List</em>'.
   * @generated
   */
  AssignmentList createAssignmentList();

  /**
   * Returns a new object of class '<em>Variable Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Assignment</em>'.
   * @generated
   */
  VariableAssignment createVariableAssignment();

  /**
   * Returns a new object of class '<em>Variable List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable List</em>'.
   * @generated
   */
  VariableList createVariableList();

  /**
   * Returns a new object of class '<em>Variable Entry</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Entry</em>'.
   * @generated
   */
  VariableEntry createVariableEntry();

  /**
   * Returns a new object of class '<em>Trigger Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Trigger Statement</em>'.
   * @generated
   */
  TriggerStatement createTriggerStatement();

  /**
   * Returns a new object of class '<em>Port Trigger Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Trigger Op</em>'.
   * @generated
   */
  PortTriggerOp createPortTriggerOp();

  /**
   * Returns a new object of class '<em>Receive Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Receive Statement</em>'.
   * @generated
   */
  ReceiveStatement createReceiveStatement();

  /**
   * Returns a new object of class '<em>Port Or Any</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Or Any</em>'.
   * @generated
   */
  PortOrAny createPortOrAny();

  /**
   * Returns a new object of class '<em>Port Receive Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Receive Op</em>'.
   * @generated
   */
  PortReceiveOp createPortReceiveOp();

  /**
   * Returns a new object of class '<em>From Clause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>From Clause</em>'.
   * @generated
   */
  FromClause createFromClause();

  /**
   * Returns a new object of class '<em>Address Ref List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Address Ref List</em>'.
   * @generated
   */
  AddressRefList createAddressRefList();

  /**
   * Returns a new object of class '<em>Port Redirect</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Redirect</em>'.
   * @generated
   */
  PortRedirect createPortRedirect();

  /**
   * Returns a new object of class '<em>Sender Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sender Spec</em>'.
   * @generated
   */
  SenderSpec createSenderSpec();

  /**
   * Returns a new object of class '<em>Value Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value Spec</em>'.
   * @generated
   */
  ValueSpec createValueSpec();

  /**
   * Returns a new object of class '<em>Single Value Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Value Spec</em>'.
   * @generated
   */
  SingleValueSpec createSingleValueSpec();

  /**
   * Returns a new object of class '<em>Alt Guard Char</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alt Guard Char</em>'.
   * @generated
   */
  AltGuardChar createAltGuardChar();

  /**
   * Returns a new object of class '<em>Altstep Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Altstep Instance</em>'.
   * @generated
   */
  AltstepInstance createAltstepInstance();

  /**
   * Returns a new object of class '<em>Function Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Instance</em>'.
   * @generated
   */
  FunctionInstance createFunctionInstance();

  /**
   * Returns a new object of class '<em>Function Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Ref</em>'.
   * @generated
   */
  FunctionRef createFunctionRef();

  /**
   * Returns a new object of class '<em>Function Actual Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Actual Par List</em>'.
   * @generated
   */
  FunctionActualParList createFunctionActualParList();

  /**
   * Returns a new object of class '<em>Function Actual Par Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Actual Par Assignment</em>'.
   * @generated
   */
  FunctionActualParAssignment createFunctionActualParAssignment();

  /**
   * Returns a new object of class '<em>Component Ref Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Ref Assignment</em>'.
   * @generated
   */
  ComponentRefAssignment createComponentRefAssignment();

  /**
   * Returns a new object of class '<em>Formal Port And Value Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formal Port And Value Par</em>'.
   * @generated
   */
  FormalPortAndValuePar createFormalPortAndValuePar();

  /**
   * Returns a new object of class '<em>Port Ref Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Ref Assignment</em>'.
   * @generated
   */
  PortRefAssignment createPortRefAssignment();

  /**
   * Returns a new object of class '<em>Timer Ref Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Ref Assignment</em>'.
   * @generated
   */
  TimerRefAssignment createTimerRefAssignment();

  /**
   * Returns a new object of class '<em>Function Actual Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Actual Par</em>'.
   * @generated
   */
  FunctionActualPar createFunctionActualPar();

  /**
   * Returns a new object of class '<em>Component Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Ref</em>'.
   * @generated
   */
  ComponentRef createComponentRef();

  /**
   * Returns a new object of class '<em>Component Or Default Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Or Default Reference</em>'.
   * @generated
   */
  ComponentOrDefaultReference createComponentOrDefaultReference();

  /**
   * Returns a new object of class '<em>Testcase Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Testcase Instance</em>'.
   * @generated
   */
  TestcaseInstance createTestcaseInstance();

  /**
   * Returns a new object of class '<em>Testcase Actual Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Testcase Actual Par List</em>'.
   * @generated
   */
  TestcaseActualParList createTestcaseActualParList();

  /**
   * Returns a new object of class '<em>Timer Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Statements</em>'.
   * @generated
   */
  TimerStatements createTimerStatements();

  /**
   * Returns a new object of class '<em>Timeout Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timeout Statement</em>'.
   * @generated
   */
  TimeoutStatement createTimeoutStatement();

  /**
   * Returns a new object of class '<em>Start Timer Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Start Timer Statement</em>'.
   * @generated
   */
  StartTimerStatement createStartTimerStatement();

  /**
   * Returns a new object of class '<em>Stop Timer Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Stop Timer Statement</em>'.
   * @generated
   */
  StopTimerStatement createStopTimerStatement();

  /**
   * Returns a new object of class '<em>Timer Ref Or Any</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Ref Or Any</em>'.
   * @generated
   */
  TimerRefOrAny createTimerRefOrAny();

  /**
   * Returns a new object of class '<em>Timer Ref Or All</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Ref Or All</em>'.
   * @generated
   */
  TimerRefOrAll createTimerRefOrAll();

  /**
   * Returns a new object of class '<em>Basic Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Basic Statements</em>'.
   * @generated
   */
  BasicStatements createBasicStatements();

  /**
   * Returns a new object of class '<em>Statement Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Block</em>'.
   * @generated
   */
  StatementBlock createStatementBlock();

  /**
   * Returns a new object of class '<em>Function Statement List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Statement List</em>'.
   * @generated
   */
  FunctionStatementList createFunctionStatementList();

  /**
   * Returns a new object of class '<em>Function Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Statement</em>'.
   * @generated
   */
  FunctionStatement createFunctionStatement();

  /**
   * Returns a new object of class '<em>Testcase Operation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Testcase Operation</em>'.
   * @generated
   */
  TestcaseOperation createTestcaseOperation();

  /**
   * Returns a new object of class '<em>Set Local Verdict</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Set Local Verdict</em>'.
   * @generated
   */
  SetLocalVerdict createSetLocalVerdict();

  /**
   * Returns a new object of class '<em>Configuration Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Configuration Statements</em>'.
   * @generated
   */
  ConfigurationStatements createConfigurationStatements();

  /**
   * Returns a new object of class '<em>Kill TC Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Kill TC Statement</em>'.
   * @generated
   */
  KillTCStatement createKillTCStatement();

  /**
   * Returns a new object of class '<em>Stop TC Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Stop TC Statement</em>'.
   * @generated
   */
  StopTCStatement createStopTCStatement();

  /**
   * Returns a new object of class '<em>Component Reference Or Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Reference Or Literal</em>'.
   * @generated
   */
  ComponentReferenceOrLiteral createComponentReferenceOrLiteral();

  /**
   * Returns a new object of class '<em>Start TC Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Start TC Statement</em>'.
   * @generated
   */
  StartTCStatement createStartTCStatement();

  /**
   * Returns a new object of class '<em>Unmap Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unmap Statement</em>'.
   * @generated
   */
  UnmapStatement createUnmapStatement();

  /**
   * Returns a new object of class '<em>Disconnect Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Disconnect Statement</em>'.
   * @generated
   */
  DisconnectStatement createDisconnectStatement();

  /**
   * Returns a new object of class '<em>All Connections Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Connections Spec</em>'.
   * @generated
   */
  AllConnectionsSpec createAllConnectionsSpec();

  /**
   * Returns a new object of class '<em>All Ports Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Ports Spec</em>'.
   * @generated
   */
  AllPortsSpec createAllPortsSpec();

  /**
   * Returns a new object of class '<em>Map Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Map Statement</em>'.
   * @generated
   */
  MapStatement createMapStatement();

  /**
   * Returns a new object of class '<em>Param Clause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Param Clause</em>'.
   * @generated
   */
  ParamClause createParamClause();

  /**
   * Returns a new object of class '<em>Connect Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Connect Statement</em>'.
   * @generated
   */
  ConnectStatement createConnectStatement();

  /**
   * Returns a new object of class '<em>Single Connection Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Connection Spec</em>'.
   * @generated
   */
  SingleConnectionSpec createSingleConnectionSpec();

  /**
   * Returns a new object of class '<em>Port Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Ref</em>'.
   * @generated
   */
  PortRef createPortRef();

  /**
   * Returns a new object of class '<em>Communication Statements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Communication Statements</em>'.
   * @generated
   */
  CommunicationStatements createCommunicationStatements();

  /**
   * Returns a new object of class '<em>Check State Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Check State Statement</em>'.
   * @generated
   */
  CheckStateStatement createCheckStateStatement();

  /**
   * Returns a new object of class '<em>Port Or All Any</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Or All Any</em>'.
   * @generated
   */
  PortOrAllAny createPortOrAllAny();

  /**
   * Returns a new object of class '<em>Halt Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Halt Statement</em>'.
   * @generated
   */
  HaltStatement createHaltStatement();

  /**
   * Returns a new object of class '<em>Start Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Start Statement</em>'.
   * @generated
   */
  StartStatement createStartStatement();

  /**
   * Returns a new object of class '<em>Stop Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Stop Statement</em>'.
   * @generated
   */
  StopStatement createStopStatement();

  /**
   * Returns a new object of class '<em>Clear Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Clear Statement</em>'.
   * @generated
   */
  ClearStatement createClearStatement();

  /**
   * Returns a new object of class '<em>Port Or All</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Or All</em>'.
   * @generated
   */
  PortOrAll createPortOrAll();

  /**
   * Returns a new object of class '<em>Raise Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Raise Statement</em>'.
   * @generated
   */
  RaiseStatement createRaiseStatement();

  /**
   * Returns a new object of class '<em>Port Raise Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Raise Op</em>'.
   * @generated
   */
  PortRaiseOp createPortRaiseOp();

  /**
   * Returns a new object of class '<em>Reply Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reply Statement</em>'.
   * @generated
   */
  ReplyStatement createReplyStatement();

  /**
   * Returns a new object of class '<em>Port Reply Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Reply Op</em>'.
   * @generated
   */
  PortReplyOp createPortReplyOp();

  /**
   * Returns a new object of class '<em>Reply Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reply Value</em>'.
   * @generated
   */
  ReplyValue createReplyValue();

  /**
   * Returns a new object of class '<em>Call Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Statement</em>'.
   * @generated
   */
  CallStatement createCallStatement();

  /**
   * Returns a new object of class '<em>Port Call Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Call Op</em>'.
   * @generated
   */
  PortCallOp createPortCallOp();

  /**
   * Returns a new object of class '<em>Call Parameters</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Parameters</em>'.
   * @generated
   */
  CallParameters createCallParameters();

  /**
   * Returns a new object of class '<em>Call Timer Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Timer Value</em>'.
   * @generated
   */
  CallTimerValue createCallTimerValue();

  /**
   * Returns a new object of class '<em>Port Call Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Call Body</em>'.
   * @generated
   */
  PortCallBody createPortCallBody();

  /**
   * Returns a new object of class '<em>Call Body Statement List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Body Statement List</em>'.
   * @generated
   */
  CallBodyStatementList createCallBodyStatementList();

  /**
   * Returns a new object of class '<em>Call Body Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Body Statement</em>'.
   * @generated
   */
  CallBodyStatement createCallBodyStatement();

  /**
   * Returns a new object of class '<em>Call Body Guard</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Body Guard</em>'.
   * @generated
   */
  CallBodyGuard createCallBodyGuard();

  /**
   * Returns a new object of class '<em>Call Body Ops</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Body Ops</em>'.
   * @generated
   */
  CallBodyOps createCallBodyOps();

  /**
   * Returns a new object of class '<em>Send Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Send Statement</em>'.
   * @generated
   */
  SendStatement createSendStatement();

  /**
   * Returns a new object of class '<em>Port Send Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Send Op</em>'.
   * @generated
   */
  PortSendOp createPortSendOp();

  /**
   * Returns a new object of class '<em>To Clause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>To Clause</em>'.
   * @generated
   */
  ToClause createToClause();

  /**
   * Returns a new object of class '<em>Function Def List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Def List</em>'.
   * @generated
   */
  FunctionDefList createFunctionDefList();

  /**
   * Returns a new object of class '<em>Function Local Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Local Def</em>'.
   * @generated
   */
  FunctionLocalDef createFunctionLocalDef();

  /**
   * Returns a new object of class '<em>Function Local Inst</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Local Inst</em>'.
   * @generated
   */
  FunctionLocalInst createFunctionLocalInst();

  /**
   * Returns a new object of class '<em>Timer Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Instance</em>'.
   * @generated
   */
  TimerInstance createTimerInstance();

  /**
   * Returns a new object of class '<em>Var Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Var Instance</em>'.
   * @generated
   */
  VarInstance createVarInstance();

  /**
   * Returns a new object of class '<em>Var List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Var List</em>'.
   * @generated
   */
  VarList createVarList();

  /**
   * Returns a new object of class '<em>Module Or Group</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Module Or Group</em>'.
   * @generated
   */
  ModuleOrGroup createModuleOrGroup();

  /**
   * Returns a new object of class '<em>Referenced Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Referenced Type</em>'.
   * @generated
   */
  ReferencedType createReferencedType();

  /**
   * Returns a new object of class '<em>Type Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Def</em>'.
   * @generated
   */
  TypeDef createTypeDef();

  /**
   * Returns a new object of class '<em>Type Def Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Def Body</em>'.
   * @generated
   */
  TypeDefBody createTypeDefBody();

  /**
   * Returns a new object of class '<em>Sub Type Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Type Def</em>'.
   * @generated
   */
  SubTypeDef createSubTypeDef();

  /**
   * Returns a new object of class '<em>Sub Type Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Type Def Named</em>'.
   * @generated
   */
  SubTypeDefNamed createSubTypeDefNamed();

  /**
   * Returns a new object of class '<em>Structured Type Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Structured Type Def</em>'.
   * @generated
   */
  StructuredTypeDef createStructuredTypeDef();

  /**
   * Returns a new object of class '<em>Record Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Def</em>'.
   * @generated
   */
  RecordDef createRecordDef();

  /**
   * Returns a new object of class '<em>Record Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Def Named</em>'.
   * @generated
   */
  RecordDefNamed createRecordDefNamed();

  /**
   * Returns a new object of class '<em>Record Of Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Of Def</em>'.
   * @generated
   */
  RecordOfDef createRecordOfDef();

  /**
   * Returns a new object of class '<em>Record Of Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Of Def Named</em>'.
   * @generated
   */
  RecordOfDefNamed createRecordOfDefNamed();

  /**
   * Returns a new object of class '<em>Struct Def Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Struct Def Body</em>'.
   * @generated
   */
  StructDefBody createStructDefBody();

  /**
   * Returns a new object of class '<em>Struct Field Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Struct Field Def</em>'.
   * @generated
   */
  StructFieldDef createStructFieldDef();

  /**
   * Returns a new object of class '<em>Set Of Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Set Of Def</em>'.
   * @generated
   */
  SetOfDef createSetOfDef();

  /**
   * Returns a new object of class '<em>Set Of Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Set Of Def Named</em>'.
   * @generated
   */
  SetOfDefNamed createSetOfDefNamed();

  /**
   * Returns a new object of class '<em>Port Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Def</em>'.
   * @generated
   */
  PortDef createPortDef();

  /**
   * Returns a new object of class '<em>Port Def Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Def Body</em>'.
   * @generated
   */
  PortDefBody createPortDefBody();

  /**
   * Returns a new object of class '<em>Port Def Attribs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Def Attribs</em>'.
   * @generated
   */
  PortDefAttribs createPortDefAttribs();

  /**
   * Returns a new object of class '<em>Mixed Attribs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mixed Attribs</em>'.
   * @generated
   */
  MixedAttribs createMixedAttribs();

  /**
   * Returns a new object of class '<em>Mixed List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mixed List</em>'.
   * @generated
   */
  MixedList createMixedList();

  /**
   * Returns a new object of class '<em>Proc Or Type List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Proc Or Type List</em>'.
   * @generated
   */
  ProcOrTypeList createProcOrTypeList();

  /**
   * Returns a new object of class '<em>Proc Or Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Proc Or Type</em>'.
   * @generated
   */
  ProcOrType createProcOrType();

  /**
   * Returns a new object of class '<em>Message Attribs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Message Attribs</em>'.
   * @generated
   */
  MessageAttribs createMessageAttribs();

  /**
   * Returns a new object of class '<em>Config Param Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Config Param Def</em>'.
   * @generated
   */
  ConfigParamDef createConfigParamDef();

  /**
   * Returns a new object of class '<em>Map Param Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Map Param Def</em>'.
   * @generated
   */
  MapParamDef createMapParamDef();

  /**
   * Returns a new object of class '<em>Unmap Param Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unmap Param Def</em>'.
   * @generated
   */
  UnmapParamDef createUnmapParamDef();

  /**
   * Returns a new object of class '<em>Address Decl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Address Decl</em>'.
   * @generated
   */
  AddressDecl createAddressDecl();

  /**
   * Returns a new object of class '<em>Procedure Attribs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Procedure Attribs</em>'.
   * @generated
   */
  ProcedureAttribs createProcedureAttribs();

  /**
   * Returns a new object of class '<em>Component Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Def</em>'.
   * @generated
   */
  ComponentDef createComponentDef();

  /**
   * Returns a new object of class '<em>Component Def List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Def List</em>'.
   * @generated
   */
  ComponentDefList createComponentDefList();

  /**
   * Returns a new object of class '<em>Port Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Instance</em>'.
   * @generated
   */
  PortInstance createPortInstance();

  /**
   * Returns a new object of class '<em>Port Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Element</em>'.
   * @generated
   */
  PortElement createPortElement();

  /**
   * Returns a new object of class '<em>Component Element Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Element Def</em>'.
   * @generated
   */
  ComponentElementDef createComponentElementDef();

  /**
   * Returns a new object of class '<em>Procedure List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Procedure List</em>'.
   * @generated
   */
  ProcedureList createProcedureList();

  /**
   * Returns a new object of class '<em>All Or Signature List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Or Signature List</em>'.
   * @generated
   */
  AllOrSignatureList createAllOrSignatureList();

  /**
   * Returns a new object of class '<em>Signature List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Signature List</em>'.
   * @generated
   */
  SignatureList createSignatureList();

  /**
   * Returns a new object of class '<em>Enum Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enum Def</em>'.
   * @generated
   */
  EnumDef createEnumDef();

  /**
   * Returns a new object of class '<em>Enum Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enum Def Named</em>'.
   * @generated
   */
  EnumDefNamed createEnumDefNamed();

  /**
   * Returns a new object of class '<em>Nested Type Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Type Def</em>'.
   * @generated
   */
  NestedTypeDef createNestedTypeDef();

  /**
   * Returns a new object of class '<em>Nested Record Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Record Def</em>'.
   * @generated
   */
  NestedRecordDef createNestedRecordDef();

  /**
   * Returns a new object of class '<em>Nested Union Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Union Def</em>'.
   * @generated
   */
  NestedUnionDef createNestedUnionDef();

  /**
   * Returns a new object of class '<em>Nested Set Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Set Def</em>'.
   * @generated
   */
  NestedSetDef createNestedSetDef();

  /**
   * Returns a new object of class '<em>Nested Record Of Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Record Of Def</em>'.
   * @generated
   */
  NestedRecordOfDef createNestedRecordOfDef();

  /**
   * Returns a new object of class '<em>Nested Set Of Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Set Of Def</em>'.
   * @generated
   */
  NestedSetOfDef createNestedSetOfDef();

  /**
   * Returns a new object of class '<em>Nested Enum Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Enum Def</em>'.
   * @generated
   */
  NestedEnumDef createNestedEnumDef();

  /**
   * Returns a new object of class '<em>Message List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Message List</em>'.
   * @generated
   */
  MessageList createMessageList();

  /**
   * Returns a new object of class '<em>All Or Type List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Or Type List</em>'.
   * @generated
   */
  AllOrTypeList createAllOrTypeList();

  /**
   * Returns a new object of class '<em>Type List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type List</em>'.
   * @generated
   */
  TypeList createTypeList();

  /**
   * Returns a new object of class '<em>Union Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Def</em>'.
   * @generated
   */
  UnionDef createUnionDef();

  /**
   * Returns a new object of class '<em>Union Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Def Named</em>'.
   * @generated
   */
  UnionDefNamed createUnionDefNamed();

  /**
   * Returns a new object of class '<em>Union Def Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Def Body</em>'.
   * @generated
   */
  UnionDefBody createUnionDefBody();

  /**
   * Returns a new object of class '<em>Enumeration List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enumeration List</em>'.
   * @generated
   */
  EnumerationList createEnumerationList();

  /**
   * Returns a new object of class '<em>Enumeration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enumeration</em>'.
   * @generated
   */
  Enumeration createEnumeration();

  /**
   * Returns a new object of class '<em>Union Field Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Field Def</em>'.
   * @generated
   */
  UnionFieldDef createUnionFieldDef();

  /**
   * Returns a new object of class '<em>Set Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Set Def</em>'.
   * @generated
   */
  SetDef createSetDef();

  /**
   * Returns a new object of class '<em>Set Def Named</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Set Def Named</em>'.
   * @generated
   */
  SetDefNamed createSetDefNamed();

  /**
   * Returns a new object of class '<em>Sub Type Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Type Spec</em>'.
   * @generated
   */
  SubTypeSpec createSubTypeSpec();

  /**
   * Returns a new object of class '<em>Allowed Values Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Allowed Values Spec</em>'.
   * @generated
   */
  AllowedValuesSpec createAllowedValuesSpec();

  /**
   * Returns a new object of class '<em>Template Or Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Or Range</em>'.
   * @generated
   */
  TemplateOrRange createTemplateOrRange();

  /**
   * Returns a new object of class '<em>Range Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Range Def</em>'.
   * @generated
   */
  RangeDef createRangeDef();

  /**
   * Returns a new object of class '<em>Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bound</em>'.
   * @generated
   */
  Bound createBound();

  /**
   * Returns a new object of class '<em>String Length</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>String Length</em>'.
   * @generated
   */
  StringLength createStringLength();

  /**
   * Returns a new object of class '<em>Char String Match</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Char String Match</em>'.
   * @generated
   */
  CharStringMatch createCharStringMatch();

  /**
   * Returns a new object of class '<em>Pattern Particle</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pattern Particle</em>'.
   * @generated
   */
  PatternParticle createPatternParticle();

  /**
   * Returns a new object of class '<em>Template Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Def</em>'.
   * @generated
   */
  TemplateDef createTemplateDef();

  /**
   * Returns a new object of class '<em>Derived Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Derived Def</em>'.
   * @generated
   */
  DerivedDef createDerivedDef();

  /**
   * Returns a new object of class '<em>Base Template</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Template</em>'.
   * @generated
   */
  BaseTemplate createBaseTemplate();

  /**
   * Returns a new object of class '<em>Temp Var List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Temp Var List</em>'.
   * @generated
   */
  TempVarList createTempVarList();

  /**
   * Returns a new object of class '<em>Timer Var Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Var Instance</em>'.
   * @generated
   */
  TimerVarInstance createTimerVarInstance();

  /**
   * Returns a new object of class '<em>Single Var Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Var Instance</em>'.
   * @generated
   */
  SingleVarInstance createSingleVarInstance();

  /**
   * Returns a new object of class '<em>Single Temp Var Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Temp Var Instance</em>'.
   * @generated
   */
  SingleTempVarInstance createSingleTempVarInstance();

  /**
   * Returns a new object of class '<em>In Line Template</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>In Line Template</em>'.
   * @generated
   */
  InLineTemplate createInLineTemplate();

  /**
   * Returns a new object of class '<em>Derived Ref With Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Derived Ref With Par List</em>'.
   * @generated
   */
  DerivedRefWithParList createDerivedRefWithParList();

  /**
   * Returns a new object of class '<em>Template Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Body</em>'.
   * @generated
   */
  TemplateBody createTemplateBody();

  /**
   * Returns a new object of class '<em>Extra Matching Attributes</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Extra Matching Attributes</em>'.
   * @generated
   */
  ExtraMatchingAttributes createExtraMatchingAttributes();

  /**
   * Returns a new object of class '<em>Field Spec List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Spec List</em>'.
   * @generated
   */
  FieldSpecList createFieldSpecList();

  /**
   * Returns a new object of class '<em>Field Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Spec</em>'.
   * @generated
   */
  FieldSpec createFieldSpec();

  /**
   * Returns a new object of class '<em>Simple Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Spec</em>'.
   * @generated
   */
  SimpleSpec createSimpleSpec();

  /**
   * Returns a new object of class '<em>Simple Template Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Template Spec</em>'.
   * @generated
   */
  SimpleTemplateSpec createSimpleTemplateSpec();

  /**
   * Returns a new object of class '<em>Single Template Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Template Expression</em>'.
   * @generated
   */
  SingleTemplateExpression createSingleTemplateExpression();

  /**
   * Returns a new object of class '<em>Template Ref With Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Ref With Par List</em>'.
   * @generated
   */
  TemplateRefWithParList createTemplateRefWithParList();

  /**
   * Returns a new object of class '<em>Template Actual Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Actual Par List</em>'.
   * @generated
   */
  TemplateActualParList createTemplateActualParList();

  /**
   * Returns a new object of class '<em>Matching Symbol</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Matching Symbol</em>'.
   * @generated
   */
  MatchingSymbol createMatchingSymbol();

  /**
   * Returns a new object of class '<em>Subset Match</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subset Match</em>'.
   * @generated
   */
  SubsetMatch createSubsetMatch();

  /**
   * Returns a new object of class '<em>Superset Match</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Superset Match</em>'.
   * @generated
   */
  SupersetMatch createSupersetMatch();

  /**
   * Returns a new object of class '<em>Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Range</em>'.
   * @generated
   */
  Range createRange();

  /**
   * Returns a new object of class '<em>Wildcard Length Match</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Wildcard Length Match</em>'.
   * @generated
   */
  WildcardLengthMatch createWildcardLengthMatch();

  /**
   * Returns a new object of class '<em>Complement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Complement</em>'.
   * @generated
   */
  Complement createComplement();

  /**
   * Returns a new object of class '<em>List Of Templates</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>List Of Templates</em>'.
   * @generated
   */
  ListOfTemplates createListOfTemplates();

  /**
   * Returns a new object of class '<em>Template Ops</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Ops</em>'.
   * @generated
   */
  TemplateOps createTemplateOps();

  /**
   * Returns a new object of class '<em>Match Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Match Op</em>'.
   * @generated
   */
  MatchOp createMatchOp();

  /**
   * Returns a new object of class '<em>Valueof Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Valueof Op</em>'.
   * @generated
   */
  ValueofOp createValueofOp();

  /**
   * Returns a new object of class '<em>Configuration Ops</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Configuration Ops</em>'.
   * @generated
   */
  ConfigurationOps createConfigurationOps();

  /**
   * Returns a new object of class '<em>Create Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Create Op</em>'.
   * @generated
   */
  CreateOp createCreateOp();

  /**
   * Returns a new object of class '<em>Running Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Running Op</em>'.
   * @generated
   */
  RunningOp createRunningOp();

  /**
   * Returns a new object of class '<em>Op Call</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Op Call</em>'.
   * @generated
   */
  OpCall createOpCall();

  /**
   * Returns a new object of class '<em>Alive Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alive Op</em>'.
   * @generated
   */
  AliveOp createAliveOp();

  /**
   * Returns a new object of class '<em>Template List Item</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template List Item</em>'.
   * @generated
   */
  TemplateListItem createTemplateListItem();

  /**
   * Returns a new object of class '<em>All Elements From</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Elements From</em>'.
   * @generated
   */
  AllElementsFrom createAllElementsFrom();

  /**
   * Returns a new object of class '<em>Signature</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Signature</em>'.
   * @generated
   */
  Signature createSignature();

  /**
   * Returns a new object of class '<em>Template Instance Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Instance Assignment</em>'.
   * @generated
   */
  TemplateInstanceAssignment createTemplateInstanceAssignment();

  /**
   * Returns a new object of class '<em>Template Instance Actual Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Instance Actual Par</em>'.
   * @generated
   */
  TemplateInstanceActualPar createTemplateInstanceActualPar();

  /**
   * Returns a new object of class '<em>Template Restriction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Template Restriction</em>'.
   * @generated
   */
  TemplateRestriction createTemplateRestriction();

  /**
   * Returns a new object of class '<em>Restricted Template</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Restricted Template</em>'.
   * @generated
   */
  RestrictedTemplate createRestrictedTemplate();

  /**
   * Returns a new object of class '<em>Log Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Log Statement</em>'.
   * @generated
   */
  LogStatement createLogStatement();

  /**
   * Returns a new object of class '<em>Log Item</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Log Item</em>'.
   * @generated
   */
  LogItem createLogItem();

  /**
   * Returns a new object of class '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression</em>'.
   * @generated
   */
  Expression createExpression();

  /**
   * Returns a new object of class '<em>With Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>With Statement</em>'.
   * @generated
   */
  WithStatement createWithStatement();

  /**
   * Returns a new object of class '<em>With Attrib List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>With Attrib List</em>'.
   * @generated
   */
  WithAttribList createWithAttribList();

  /**
   * Returns a new object of class '<em>Multi With Attrib</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Multi With Attrib</em>'.
   * @generated
   */
  MultiWithAttrib createMultiWithAttrib();

  /**
   * Returns a new object of class '<em>Single With Attrib</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single With Attrib</em>'.
   * @generated
   */
  SingleWithAttrib createSingleWithAttrib();

  /**
   * Returns a new object of class '<em>Attrib Qualifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Attrib Qualifier</em>'.
   * @generated
   */
  AttribQualifier createAttribQualifier();

  /**
   * Returns a new object of class '<em>Identifier List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifier List</em>'.
   * @generated
   */
  IdentifierList createIdentifierList();

  /**
   * Returns a new object of class '<em>Qualified Identifier List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Qualified Identifier List</em>'.
   * @generated
   */
  QualifiedIdentifierList createQualifiedIdentifierList();

  /**
   * Returns a new object of class '<em>Single Const Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Const Def</em>'.
   * @generated
   */
  SingleConstDef createSingleConstDef();

  /**
   * Returns a new object of class '<em>Compound Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Compound Expression</em>'.
   * @generated
   */
  CompoundExpression createCompoundExpression();

  /**
   * Returns a new object of class '<em>Array Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Expression</em>'.
   * @generated
   */
  ArrayExpression createArrayExpression();

  /**
   * Returns a new object of class '<em>Field Expression List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Expression List</em>'.
   * @generated
   */
  FieldExpressionList createFieldExpressionList();

  /**
   * Returns a new object of class '<em>Array Element Expression List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Element Expression List</em>'.
   * @generated
   */
  ArrayElementExpressionList createArrayElementExpressionList();

  /**
   * Returns a new object of class '<em>Field Expression Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Expression Spec</em>'.
   * @generated
   */
  FieldExpressionSpec createFieldExpressionSpec();

  /**
   * Returns a new object of class '<em>Not Used Or Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Not Used Or Expression</em>'.
   * @generated
   */
  NotUsedOrExpression createNotUsedOrExpression();

  /**
   * Returns a new object of class '<em>Constant Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant Expression</em>'.
   * @generated
   */
  ConstantExpression createConstantExpression();

  /**
   * Returns a new object of class '<em>Compound Const Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Compound Const Expression</em>'.
   * @generated
   */
  CompoundConstExpression createCompoundConstExpression();

  /**
   * Returns a new object of class '<em>Field Const Expression List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Const Expression List</em>'.
   * @generated
   */
  FieldConstExpressionList createFieldConstExpressionList();

  /**
   * Returns a new object of class '<em>Field Const Expression Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Const Expression Spec</em>'.
   * @generated
   */
  FieldConstExpressionSpec createFieldConstExpressionSpec();

  /**
   * Returns a new object of class '<em>Array Const Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Const Expression</em>'.
   * @generated
   */
  ArrayConstExpression createArrayConstExpression();

  /**
   * Returns a new object of class '<em>Array Element Const Expression List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Element Const Expression List</em>'.
   * @generated
   */
  ArrayElementConstExpressionList createArrayElementConstExpressionList();

  /**
   * Returns a new object of class '<em>Const List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Const List</em>'.
   * @generated
   */
  ConstList createConstList();

  /**
   * Returns a new object of class '<em>Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value</em>'.
   * @generated
   */
  Value createValue();

  /**
   * Returns a new object of class '<em>Referenced Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Referenced Value</em>'.
   * @generated
   */
  ReferencedValue createReferencedValue();

  /**
   * Returns a new object of class '<em>Ref Value Head</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ref Value Head</em>'.
   * @generated
   */
  RefValueHead createRefValueHead();

  /**
   * Returns a new object of class '<em>Ref Value Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ref Value Element</em>'.
   * @generated
   */
  RefValueElement createRefValueElement();

  /**
   * Returns a new object of class '<em>Head</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Head</em>'.
   * @generated
   */
  Head createHead();

  /**
   * Returns a new object of class '<em>Ref Value Tail</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ref Value Tail</em>'.
   * @generated
   */
  RefValueTail createRefValueTail();

  /**
   * Returns a new object of class '<em>Spec Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Spec Element</em>'.
   * @generated
   */
  SpecElement createSpecElement();

  /**
   * Returns a new object of class '<em>Extended Field Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Extended Field Reference</em>'.
   * @generated
   */
  ExtendedFieldReference createExtendedFieldReference();

  /**
   * Returns a new object of class '<em>Ref Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ref Value</em>'.
   * @generated
   */
  RefValue createRefValue();

  /**
   * Returns a new object of class '<em>Predefined Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Predefined Value</em>'.
   * @generated
   */
  PredefinedValue createPredefinedValue();

  /**
   * Returns a new object of class '<em>Boolean Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Expression</em>'.
   * @generated
   */
  BooleanExpression createBooleanExpression();

  /**
   * Returns a new object of class '<em>Single Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Expression</em>'.
   * @generated
   */
  SingleExpression createSingleExpression();

  /**
   * Returns a new object of class '<em>Def Or Field Ref List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Def Or Field Ref List</em>'.
   * @generated
   */
  DefOrFieldRefList createDefOrFieldRefList();

  /**
   * Returns a new object of class '<em>Def Or Field Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Def Or Field Ref</em>'.
   * @generated
   */
  DefOrFieldRef createDefOrFieldRef();

  /**
   * Returns a new object of class '<em>All Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Ref</em>'.
   * @generated
   */
  AllRef createAllRef();

  /**
   * Returns a new object of class '<em>Group Ref List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Group Ref List</em>'.
   * @generated
   */
  GroupRefList createGroupRefList();

  /**
   * Returns a new object of class '<em>Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference</em>'.
   * @generated
   */
  TTCN3Reference createTTCN3Reference();

  /**
   * Returns a new object of class '<em>Reference List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference List</em>'.
   * @generated
   */
  TTCN3ReferenceList createTTCN3ReferenceList();

  /**
   * Returns a new object of class '<em>Field Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Reference</em>'.
   * @generated
   */
  FieldReference createFieldReference();

  /**
   * Returns a new object of class '<em>Array Or Bit Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Or Bit Ref</em>'.
   * @generated
   */
  ArrayOrBitRef createArrayOrBitRef();

  /**
   * Returns a new object of class '<em>Field Or Bit Number</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Or Bit Number</em>'.
   * @generated
   */
  FieldOrBitNumber createFieldOrBitNumber();

  /**
   * Returns a new object of class '<em>Array Value Or Attrib</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Value Or Attrib</em>'.
   * @generated
   */
  ArrayValueOrAttrib createArrayValueOrAttrib();

  /**
   * Returns a new object of class '<em>Array Element Spec List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Element Spec List</em>'.
   * @generated
   */
  ArrayElementSpecList createArrayElementSpecList();

  /**
   * Returns a new object of class '<em>Array Element Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Element Spec</em>'.
   * @generated
   */
  ArrayElementSpec createArrayElementSpec();

  /**
   * Returns a new object of class '<em>Timer Ops</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Ops</em>'.
   * @generated
   */
  TimerOps createTimerOps();

  /**
   * Returns a new object of class '<em>Running Timer Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Running Timer Op</em>'.
   * @generated
   */
  RunningTimerOp createRunningTimerOp();

  /**
   * Returns a new object of class '<em>Read Timer Op</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Read Timer Op</em>'.
   * @generated
   */
  ReadTimerOp createReadTimerOp();

  /**
   * Returns a new object of class '<em>Permutation Match</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Permutation Match</em>'.
   * @generated
   */
  PermutationMatch createPermutationMatch();

  /**
   * Returns a new object of class '<em>Loop Construct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Loop Construct</em>'.
   * @generated
   */
  LoopConstruct createLoopConstruct();

  /**
   * Returns a new object of class '<em>For Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>For Statement</em>'.
   * @generated
   */
  ForStatement createForStatement();

  /**
   * Returns a new object of class '<em>While Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>While Statement</em>'.
   * @generated
   */
  WhileStatement createWhileStatement();

  /**
   * Returns a new object of class '<em>Do While Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Do While Statement</em>'.
   * @generated
   */
  DoWhileStatement createDoWhileStatement();

  /**
   * Returns a new object of class '<em>Conditional Construct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Conditional Construct</em>'.
   * @generated
   */
  ConditionalConstruct createConditionalConstruct();

  /**
   * Returns a new object of class '<em>Else If Clause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Else If Clause</em>'.
   * @generated
   */
  ElseIfClause createElseIfClause();

  /**
   * Returns a new object of class '<em>Else Clause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Else Clause</em>'.
   * @generated
   */
  ElseClause createElseClause();

  /**
   * Returns a new object of class '<em>Initial</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Initial</em>'.
   * @generated
   */
  Initial createInitial();

  /**
   * Returns a new object of class '<em>Select Case Construct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Case Construct</em>'.
   * @generated
   */
  SelectCaseConstruct createSelectCaseConstruct();

  /**
   * Returns a new object of class '<em>Select Case Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Case Body</em>'.
   * @generated
   */
  SelectCaseBody createSelectCaseBody();

  /**
   * Returns a new object of class '<em>Select Case</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Case</em>'.
   * @generated
   */
  SelectCase createSelectCase();

  /**
   * Returns a new object of class '<em>Function Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Def</em>'.
   * @generated
   */
  FunctionDef createFunctionDef();

  /**
   * Returns a new object of class '<em>Mtc Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mtc Spec</em>'.
   * @generated
   */
  MtcSpec createMtcSpec();

  /**
   * Returns a new object of class '<em>Function Formal Par List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Formal Par List</em>'.
   * @generated
   */
  FunctionFormalParList createFunctionFormalParList();

  /**
   * Returns a new object of class '<em>Function Formal Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Formal Par</em>'.
   * @generated
   */
  FunctionFormalPar createFunctionFormalPar();

  /**
   * Returns a new object of class '<em>Formal Value Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formal Value Par</em>'.
   * @generated
   */
  FormalValuePar createFormalValuePar();

  /**
   * Returns a new object of class '<em>Formal Timer Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formal Timer Par</em>'.
   * @generated
   */
  FormalTimerPar createFormalTimerPar();

  /**
   * Returns a new object of class '<em>Formal Port Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formal Port Par</em>'.
   * @generated
   */
  FormalPortPar createFormalPortPar();

  /**
   * Returns a new object of class '<em>Formal Template Par</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formal Template Par</em>'.
   * @generated
   */
  FormalTemplatePar createFormalTemplatePar();

  /**
   * Returns a new object of class '<em>Runs On Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Runs On Spec</em>'.
   * @generated
   */
  RunsOnSpec createRunsOnSpec();

  /**
   * Returns a new object of class '<em>Return Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Return Type</em>'.
   * @generated
   */
  ReturnType createReturnType();

  /**
   * Returns a new object of class '<em>Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assignment</em>'.
   * @generated
   */
  Assignment createAssignment();

  /**
   * Returns a new object of class '<em>Variable Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Ref</em>'.
   * @generated
   */
  VariableRef createVariableRef();

  /**
   * Returns a new object of class '<em>Pre Def Function</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pre Def Function</em>'.
   * @generated
   */
  PreDefFunction createPreDefFunction();

  /**
   * Returns a new object of class '<em>Fint2char</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2char</em>'.
   * @generated
   */
  Fint2char createFint2char();

  /**
   * Returns a new object of class '<em>Fint2unichar</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2unichar</em>'.
   * @generated
   */
  Fint2unichar createFint2unichar();

  /**
   * Returns a new object of class '<em>Fint2bit</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2bit</em>'.
   * @generated
   */
  Fint2bit createFint2bit();

  /**
   * Returns a new object of class '<em>Fint2enum</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2enum</em>'.
   * @generated
   */
  Fint2enum createFint2enum();

  /**
   * Returns a new object of class '<em>Fint2hex</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2hex</em>'.
   * @generated
   */
  Fint2hex createFint2hex();

  /**
   * Returns a new object of class '<em>Fint2oct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2oct</em>'.
   * @generated
   */
  Fint2oct createFint2oct();

  /**
   * Returns a new object of class '<em>Fint2str</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2str</em>'.
   * @generated
   */
  Fint2str createFint2str();

  /**
   * Returns a new object of class '<em>Fint2float</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fint2float</em>'.
   * @generated
   */
  Fint2float createFint2float();

  /**
   * Returns a new object of class '<em>Ffloat2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ffloat2int</em>'.
   * @generated
   */
  Ffloat2int createFfloat2int();

  /**
   * Returns a new object of class '<em>Fchar2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fchar2int</em>'.
   * @generated
   */
  Fchar2int createFchar2int();

  /**
   * Returns a new object of class '<em>Fchar2oct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fchar2oct</em>'.
   * @generated
   */
  Fchar2oct createFchar2oct();

  /**
   * Returns a new object of class '<em>Funichar2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Funichar2int</em>'.
   * @generated
   */
  Funichar2int createFunichar2int();

  /**
   * Returns a new object of class '<em>Funichar2oct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Funichar2oct</em>'.
   * @generated
   */
  Funichar2oct createFunichar2oct();

  /**
   * Returns a new object of class '<em>Fbit2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fbit2int</em>'.
   * @generated
   */
  Fbit2int createFbit2int();

  /**
   * Returns a new object of class '<em>Fbit2hex</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fbit2hex</em>'.
   * @generated
   */
  Fbit2hex createFbit2hex();

  /**
   * Returns a new object of class '<em>Fbit2oct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fbit2oct</em>'.
   * @generated
   */
  Fbit2oct createFbit2oct();

  /**
   * Returns a new object of class '<em>Fbit2str</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fbit2str</em>'.
   * @generated
   */
  Fbit2str createFbit2str();

  /**
   * Returns a new object of class '<em>Fhex2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fhex2int</em>'.
   * @generated
   */
  Fhex2int createFhex2int();

  /**
   * Returns a new object of class '<em>Fhex2bit</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fhex2bit</em>'.
   * @generated
   */
  Fhex2bit createFhex2bit();

  /**
   * Returns a new object of class '<em>Fhex2oct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fhex2oct</em>'.
   * @generated
   */
  Fhex2oct createFhex2oct();

  /**
   * Returns a new object of class '<em>Fhex2str</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fhex2str</em>'.
   * @generated
   */
  Fhex2str createFhex2str();

  /**
   * Returns a new object of class '<em>Foct2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foct2int</em>'.
   * @generated
   */
  Foct2int createFoct2int();

  /**
   * Returns a new object of class '<em>Foct2bit</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foct2bit</em>'.
   * @generated
   */
  Foct2bit createFoct2bit();

  /**
   * Returns a new object of class '<em>Foct2hex</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foct2hex</em>'.
   * @generated
   */
  Foct2hex createFoct2hex();

  /**
   * Returns a new object of class '<em>Foct2str</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foct2str</em>'.
   * @generated
   */
  Foct2str createFoct2str();

  /**
   * Returns a new object of class '<em>Foct2char</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foct2char</em>'.
   * @generated
   */
  Foct2char createFoct2char();

  /**
   * Returns a new object of class '<em>Foct2unichar</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foct2unichar</em>'.
   * @generated
   */
  Foct2unichar createFoct2unichar();

  /**
   * Returns a new object of class '<em>Fstr2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fstr2int</em>'.
   * @generated
   */
  Fstr2int createFstr2int();

  /**
   * Returns a new object of class '<em>Fstr2hex</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fstr2hex</em>'.
   * @generated
   */
  Fstr2hex createFstr2hex();

  /**
   * Returns a new object of class '<em>Fstr2oct</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fstr2oct</em>'.
   * @generated
   */
  Fstr2oct createFstr2oct();

  /**
   * Returns a new object of class '<em>Fstr2float</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fstr2float</em>'.
   * @generated
   */
  Fstr2float createFstr2float();

  /**
   * Returns a new object of class '<em>Fenum2int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fenum2int</em>'.
   * @generated
   */
  Fenum2int createFenum2int();

  /**
   * Returns a new object of class '<em>Flengthof</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Flengthof</em>'.
   * @generated
   */
  Flengthof createFlengthof();

  /**
   * Returns a new object of class '<em>Fsizeof</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fsizeof</em>'.
   * @generated
   */
  Fsizeof createFsizeof();

  /**
   * Returns a new object of class '<em>Fispresent</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fispresent</em>'.
   * @generated
   */
  Fispresent createFispresent();

  /**
   * Returns a new object of class '<em>Fischosen</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fischosen</em>'.
   * @generated
   */
  Fischosen createFischosen();

  /**
   * Returns a new object of class '<em>Fisvalue</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fisvalue</em>'.
   * @generated
   */
  Fisvalue createFisvalue();

  /**
   * Returns a new object of class '<em>Fisbound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fisbound</em>'.
   * @generated
   */
  Fisbound createFisbound();

  /**
   * Returns a new object of class '<em>Fregexp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fregexp</em>'.
   * @generated
   */
  Fregexp createFregexp();

  /**
   * Returns a new object of class '<em>Fsubstr</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fsubstr</em>'.
   * @generated
   */
  Fsubstr createFsubstr();

  /**
   * Returns a new object of class '<em>Freplace</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Freplace</em>'.
   * @generated
   */
  Freplace createFreplace();

  /**
   * Returns a new object of class '<em>Fencvalue</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fencvalue</em>'.
   * @generated
   */
  Fencvalue createFencvalue();

  /**
   * Returns a new object of class '<em>Fdecvalue</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fdecvalue</em>'.
   * @generated
   */
  Fdecvalue createFdecvalue();

  /**
   * Returns a new object of class '<em>Fencvalue Unichar</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fencvalue Unichar</em>'.
   * @generated
   */
  FencvalueUnichar createFencvalueUnichar();

  /**
   * Returns a new object of class '<em>Fdecvalue Unichar</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fdecvalue Unichar</em>'.
   * @generated
   */
  FdecvalueUnichar createFdecvalueUnichar();

  /**
   * Returns a new object of class '<em>Frnd</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Frnd</em>'.
   * @generated
   */
  Frnd createFrnd();

  /**
   * Returns a new object of class '<em>Ftestcasename</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ftestcasename</em>'.
   * @generated
   */
  Ftestcasename createFtestcasename();

  /**
   * Returns a new object of class '<em>Xor Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Xor Expression</em>'.
   * @generated
   */
  XorExpression createXorExpression();

  /**
   * Returns a new object of class '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>And Expression</em>'.
   * @generated
   */
  AndExpression createAndExpression();

  /**
   * Returns a new object of class '<em>Equal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equal Expression</em>'.
   * @generated
   */
  EqualExpression createEqualExpression();

  /**
   * Returns a new object of class '<em>Rel Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Rel Expression</em>'.
   * @generated
   */
  RelExpression createRelExpression();

  /**
   * Returns a new object of class '<em>Shift Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Shift Expression</em>'.
   * @generated
   */
  ShiftExpression createShiftExpression();

  /**
   * Returns a new object of class '<em>Bit Or Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bit Or Expression</em>'.
   * @generated
   */
  BitOrExpression createBitOrExpression();

  /**
   * Returns a new object of class '<em>Bit Xor Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bit Xor Expression</em>'.
   * @generated
   */
  BitXorExpression createBitXorExpression();

  /**
   * Returns a new object of class '<em>Bit And Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bit And Expression</em>'.
   * @generated
   */
  BitAndExpression createBitAndExpression();

  /**
   * Returns a new object of class '<em>Add Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add Expression</em>'.
   * @generated
   */
  AddExpression createAddExpression();

  /**
   * Returns a new object of class '<em>Mul Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mul Expression</em>'.
   * @generated
   */
  MulExpression createMulExpression();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  TTCN3Package getTTCN3Package();

} //TTCN3Factory
