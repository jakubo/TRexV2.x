/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifier List Or All With Except</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getIdList <em>Id List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getAll <em>All</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getIdentifierListOrAllWithExcept()
 * @model
 * @generated
 */
public interface IdentifierListOrAllWithExcept extends EObject
{
  /**
   * Returns the value of the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id List</em>' containment reference.
   * @see #setIdList(IdentifierList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getIdentifierListOrAllWithExcept_IdList()
   * @model containment="true"
   * @generated
   */
  IdentifierList getIdList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getIdList <em>Id List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id List</em>' containment reference.
   * @see #getIdList()
   * @generated
   */
  void setIdList(IdentifierList value);

  /**
   * Returns the value of the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' containment reference.
   * @see #setAll(AllWithExcept)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getIdentifierListOrAllWithExcept_All()
   * @model containment="true"
   * @generated
   */
  AllWithExcept getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getAll <em>All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' containment reference.
   * @see #getAll()
   * @generated
   */
  void setAll(AllWithExcept value);

} // IdentifierListOrAllWithExcept
