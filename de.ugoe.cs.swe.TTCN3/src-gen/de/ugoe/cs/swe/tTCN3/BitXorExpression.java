/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bit Xor Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBitXorExpression()
 * @model
 * @generated
 */
public interface BitXorExpression extends SingleExpression
{
} // BitXorExpression
