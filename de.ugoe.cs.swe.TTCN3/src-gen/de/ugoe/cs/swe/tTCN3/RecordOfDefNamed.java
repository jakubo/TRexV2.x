/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Of Def Named</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRecordOfDefNamed()
 * @model
 * @generated
 */
public interface RecordOfDefNamed extends ReferencedType, RecordOfDef
{
} // RecordOfDefNamed
