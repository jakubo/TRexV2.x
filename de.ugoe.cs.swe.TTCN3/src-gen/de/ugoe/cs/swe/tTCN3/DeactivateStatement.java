/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deactivate Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.DeactivateStatement#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDeactivateStatement()
 * @model
 * @generated
 */
public interface DeactivateStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component</em>' containment reference.
   * @see #setComponent(ComponentOrDefaultReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDeactivateStatement_Component()
   * @model containment="true"
   * @generated
   */
  ComponentOrDefaultReference getComponent();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.DeactivateStatement#getComponent <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' containment reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ComponentOrDefaultReference value);

} // DeactivateStatement
