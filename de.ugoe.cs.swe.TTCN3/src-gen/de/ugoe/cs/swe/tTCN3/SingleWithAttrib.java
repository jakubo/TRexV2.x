/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single With Attrib</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleWithAttrib#getAttrib <em>Attrib</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleWithAttrib()
 * @model
 * @generated
 */
public interface SingleWithAttrib extends EObject
{
  /**
   * Returns the value of the '<em><b>Attrib</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Attrib</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Attrib</em>' containment reference.
   * @see #setAttrib(AttribQualifier)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleWithAttrib_Attrib()
   * @model containment="true"
   * @generated
   */
  AttribQualifier getAttrib();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleWithAttrib#getAttrib <em>Attrib</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Attrib</em>' containment reference.
   * @see #getAttrib()
   * @generated
   */
  void setAttrib(AttribQualifier value);

} // SingleWithAttrib
