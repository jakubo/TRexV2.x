/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getSystem <em>System</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getSelf <em>Self</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getMtc <em>Mtc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentRef()
 * @model
 * @generated
 */
public interface ComponentRef extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(ComponentOrDefaultReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentRef_Ref()
   * @model containment="true"
   * @generated
   */
  ComponentOrDefaultReference getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(ComponentOrDefaultReference value);

  /**
   * Returns the value of the '<em><b>System</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System</em>' attribute.
   * @see #setSystem(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentRef_System()
   * @model
   * @generated
   */
  String getSystem();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getSystem <em>System</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System</em>' attribute.
   * @see #getSystem()
   * @generated
   */
  void setSystem(String value);

  /**
   * Returns the value of the '<em><b>Self</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Self</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Self</em>' attribute.
   * @see #setSelf(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentRef_Self()
   * @model
   * @generated
   */
  String getSelf();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getSelf <em>Self</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Self</em>' attribute.
   * @see #getSelf()
   * @generated
   */
  void setSelf(String value);

  /**
   * Returns the value of the '<em><b>Mtc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mtc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mtc</em>' attribute.
   * @see #setMtc(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentRef_Mtc()
   * @model
   * @generated
   */
  String getMtc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getMtc <em>Mtc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mtc</em>' attribute.
   * @see #getMtc()
   * @generated
   */
  void setMtc(String value);

} // ComponentRef
