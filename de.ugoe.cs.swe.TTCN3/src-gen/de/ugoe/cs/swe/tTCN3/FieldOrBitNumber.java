/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Or Bit Number</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldOrBitNumber()
 * @model
 * @generated
 */
public interface FieldOrBitNumber extends ArrayOrBitRef
{
} // FieldOrBitNumber
