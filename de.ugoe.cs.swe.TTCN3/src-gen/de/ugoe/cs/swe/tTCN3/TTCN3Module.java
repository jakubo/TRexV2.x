/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getDefs <em>Defs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getControls <em>Controls</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getWithstm <em>Withstm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3Module()
 * @model
 * @generated
 */
public interface TTCN3Module extends ModuleOrGroup
{
  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(LanguageSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3Module_Spec()
   * @model containment="true"
   * @generated
   */
  LanguageSpec getSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(LanguageSpec value);

  /**
   * Returns the value of the '<em><b>Defs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defs</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Defs</em>' containment reference.
   * @see #setDefs(ModuleDefinitionsList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3Module_Defs()
   * @model containment="true"
   * @generated
   */
  ModuleDefinitionsList getDefs();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getDefs <em>Defs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Defs</em>' containment reference.
   * @see #getDefs()
   * @generated
   */
  void setDefs(ModuleDefinitionsList value);

  /**
   * Returns the value of the '<em><b>Controls</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Controls</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Controls</em>' containment reference.
   * @see #setControls(ModuleControlPart)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3Module_Controls()
   * @model containment="true"
   * @generated
   */
  ModuleControlPart getControls();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getControls <em>Controls</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Controls</em>' containment reference.
   * @see #getControls()
   * @generated
   */
  void setControls(ModuleControlPart value);

  /**
   * Returns the value of the '<em><b>Withstm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Withstm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Withstm</em>' containment reference.
   * @see #setWithstm(WithStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3Module_Withstm()
   * @model containment="true"
   * @generated
   */
  WithStatement getWithstm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getWithstm <em>Withstm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Withstm</em>' containment reference.
   * @see #getWithstm()
   * @generated
   */
  void setWithstm(WithStatement value);

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute.
   * @see #setSc(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3Module_Sc()
   * @model
   * @generated
   */
  String getSc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getSc <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sc</em>' attribute.
   * @see #getSc()
   * @generated
   */
  void setSc(String value);

} // TTCN3Module
