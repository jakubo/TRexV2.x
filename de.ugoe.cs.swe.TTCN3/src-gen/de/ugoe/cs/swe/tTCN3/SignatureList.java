/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signature List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SignatureList#getSigs <em>Sigs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSignatureList()
 * @model
 * @generated
 */
public interface SignatureList extends EObject
{
  /**
   * Returns the value of the '<em><b>Sigs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.Signature}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sigs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sigs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSignatureList_Sigs()
   * @model containment="true"
   * @generated
   */
  EList<Signature> getSigs();

} // SignatureList
