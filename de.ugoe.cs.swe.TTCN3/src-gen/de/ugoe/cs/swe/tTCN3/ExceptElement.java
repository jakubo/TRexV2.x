/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Except Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getGroup <em>Group</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getConst <em>Const</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getAltstep <em>Altstep</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getSignature <em>Signature</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getModulePar <em>Module Par</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement()
 * @model
 * @generated
 */
public interface ExceptElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Group</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' containment reference.
   * @see #setGroup(ExceptGroupSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Group()
   * @model containment="true"
   * @generated
   */
  ExceptGroupSpec getGroup();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getGroup <em>Group</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Group</em>' containment reference.
   * @see #getGroup()
   * @generated
   */
  void setGroup(ExceptGroupSpec value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(ExceptTypeDefSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Type()
   * @model containment="true"
   * @generated
   */
  ExceptTypeDefSpec getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(ExceptTypeDefSpec value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(ExceptTemplateSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Template()
   * @model containment="true"
   * @generated
   */
  ExceptTemplateSpec getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(ExceptTemplateSpec value);

  /**
   * Returns the value of the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const</em>' containment reference.
   * @see #setConst(ExceptConstSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Const()
   * @model containment="true"
   * @generated
   */
  ExceptConstSpec getConst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getConst <em>Const</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const</em>' containment reference.
   * @see #getConst()
   * @generated
   */
  void setConst(ExceptConstSpec value);

  /**
   * Returns the value of the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Testcase</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Testcase</em>' containment reference.
   * @see #setTestcase(ExceptTestcaseSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Testcase()
   * @model containment="true"
   * @generated
   */
  ExceptTestcaseSpec getTestcase();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getTestcase <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Testcase</em>' containment reference.
   * @see #getTestcase()
   * @generated
   */
  void setTestcase(ExceptTestcaseSpec value);

  /**
   * Returns the value of the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Altstep</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Altstep</em>' containment reference.
   * @see #setAltstep(ExceptAltstepSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Altstep()
   * @model containment="true"
   * @generated
   */
  ExceptAltstepSpec getAltstep();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getAltstep <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Altstep</em>' containment reference.
   * @see #getAltstep()
   * @generated
   */
  void setAltstep(ExceptAltstepSpec value);

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference.
   * @see #setFunction(ExceptFunctionSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Function()
   * @model containment="true"
   * @generated
   */
  ExceptFunctionSpec getFunction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getFunction <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' containment reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(ExceptFunctionSpec value);

  /**
   * Returns the value of the '<em><b>Signature</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Signature</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Signature</em>' containment reference.
   * @see #setSignature(ExceptSignatureSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_Signature()
   * @model containment="true"
   * @generated
   */
  ExceptSignatureSpec getSignature();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getSignature <em>Signature</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Signature</em>' containment reference.
   * @see #getSignature()
   * @generated
   */
  void setSignature(ExceptSignatureSpec value);

  /**
   * Returns the value of the '<em><b>Module Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Module Par</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Module Par</em>' containment reference.
   * @see #setModulePar(ExceptModuleParSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptElement_ModulePar()
   * @model containment="true"
   * @generated
   */
  ExceptModuleParSpec getModulePar();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getModulePar <em>Module Par</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Module Par</em>' containment reference.
   * @see #getModulePar()
   * @generated
   */
  void setModulePar(ExceptModuleParSpec value);

} // ExceptElement
