/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Const Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ArrayConstExpression#getList <em>List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayConstExpression()
 * @model
 * @generated
 */
public interface ArrayConstExpression extends CompoundConstExpression
{
  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference.
   * @see #setList(ArrayElementConstExpressionList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayConstExpression_List()
   * @model containment="true"
   * @generated
   */
  ArrayElementConstExpressionList getList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ArrayConstExpression#getList <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' containment reference.
   * @see #getList()
   * @generated
   */
  void setList(ArrayElementConstExpressionList value);

} // ArrayConstExpression
