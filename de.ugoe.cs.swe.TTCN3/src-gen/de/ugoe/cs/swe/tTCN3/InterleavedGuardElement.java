/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interleaved Guard Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getGuard <em>Guard</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuardElement()
 * @model
 * @generated
 */
public interface InterleavedGuardElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard</em>' containment reference.
   * @see #setGuard(InterleavedGuard)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuardElement_Guard()
   * @model containment="true"
   * @generated
   */
  InterleavedGuard getGuard();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getGuard <em>Guard</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Guard</em>' containment reference.
   * @see #getGuard()
   * @generated
   */
  void setGuard(InterleavedGuard value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuardElement_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

} // InterleavedGuardElement
