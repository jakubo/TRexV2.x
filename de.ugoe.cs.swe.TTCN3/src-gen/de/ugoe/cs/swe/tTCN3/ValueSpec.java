/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ValueSpec#getVariable <em>Variable</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ValueSpec#getSpecs <em>Specs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValueSpec()
 * @model
 * @generated
 */
public interface ValueSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' containment reference.
   * @see #setVariable(VariableRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValueSpec_Variable()
   * @model containment="true"
   * @generated
   */
  VariableRef getVariable();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ValueSpec#getVariable <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' containment reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(VariableRef value);

  /**
   * Returns the value of the '<em><b>Specs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.SingleValueSpec}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValueSpec_Specs()
   * @model containment="true"
   * @generated
   */
  EList<SingleValueSpec> getSpecs();

} // ValueSpec
