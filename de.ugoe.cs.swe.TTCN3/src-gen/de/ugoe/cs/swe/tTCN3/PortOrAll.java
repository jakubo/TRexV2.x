/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Or All</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAll#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAll#getArrayRefs <em>Array Refs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAll()
 * @model
 * @generated
 */
public interface PortOrAll extends EObject
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' reference.
   * @see #setPort(FormalPortAndValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAll_Port()
   * @model
   * @generated
   */
  FormalPortAndValuePar getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAll#getPort <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' reference.
   * @see #getPort()
   * @generated
   */
  void setPort(FormalPortAndValuePar value);

  /**
   * Returns the value of the '<em><b>Array Refs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ArrayOrBitRef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array Refs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array Refs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAll_ArrayRefs()
   * @model containment="true"
   * @generated
   */
  EList<ArrayOrBitRef> getArrayRefs();

} // PortOrAll
