/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BaseTemplate#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BaseTemplate#getParList <em>Par List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBaseTemplate()
 * @model
 * @generated
 */
public interface BaseTemplate extends FunctionRef
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBaseTemplate_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BaseTemplate#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Par List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Par List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Par List</em>' containment reference.
   * @see #setParList(TemplateOrValueFormalParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBaseTemplate_ParList()
   * @model containment="true"
   * @generated
   */
  TemplateOrValueFormalParList getParList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BaseTemplate#getParList <em>Par List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Par List</em>' containment reference.
   * @see #getParList()
   * @generated
   */
  void setParList(TemplateOrValueFormalParList value);

} // BaseTemplate
