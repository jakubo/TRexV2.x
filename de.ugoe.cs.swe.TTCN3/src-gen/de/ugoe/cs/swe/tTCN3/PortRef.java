/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRef#getComponent <em>Component</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRef#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRef#getArray <em>Array</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRef()
 * @model
 * @generated
 */
public interface PortRef extends AllConnectionsSpec
{
  /**
   * Returns the value of the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component</em>' containment reference.
   * @see #setComponent(ComponentRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRef_Component()
   * @model containment="true"
   * @generated
   */
  ComponentRef getComponent();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRef#getComponent <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' containment reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ComponentRef value);

  /**
   * Returns the value of the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' reference.
   * @see #setPort(FormalPortAndValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRef_Port()
   * @model
   * @generated
   */
  FormalPortAndValuePar getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRef#getPort <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' reference.
   * @see #getPort()
   * @generated
   */
  void setPort(FormalPortAndValuePar value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayOrBitRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRef_Array()
   * @model containment="true"
   * @generated
   */
  ArrayOrBitRef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRef#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayOrBitRef value);

} // PortRef
