/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Redirect With Value And Param Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getIndex <em>Index</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithValueAndParamSpec()
 * @model
 * @generated
 */
public interface RedirectWithValueAndParamSpec extends PortRedirectWithValueAndParam
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(ValueSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithValueAndParamSpec_Value()
   * @model containment="true"
   * @generated
   */
  ValueSpec getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(ValueSpec value);

  /**
   * Returns the value of the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param</em>' containment reference.
   * @see #setParam(ParamSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithValueAndParamSpec_Param()
   * @model containment="true"
   * @generated
   */
  ParamSpec getParam();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getParam <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param</em>' containment reference.
   * @see #getParam()
   * @generated
   */
  void setParam(ParamSpec value);

  /**
   * Returns the value of the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sender</em>' containment reference.
   * @see #setSender(SenderSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithValueAndParamSpec_Sender()
   * @model containment="true"
   * @generated
   */
  SenderSpec getSender();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getSender <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sender</em>' containment reference.
   * @see #getSender()
   * @generated
   */
  void setSender(SenderSpec value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(IndexSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithValueAndParamSpec_Index()
   * @model containment="true"
   * @generated
   */
  IndexSpec getIndex();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(IndexSpec value);

  /**
   * Returns the value of the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Redirect</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Redirect</em>' containment reference.
   * @see #setRedirect(RedirectWithParamSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithValueAndParamSpec_Redirect()
   * @model containment="true"
   * @generated
   */
  RedirectWithParamSpec getRedirect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getRedirect <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Redirect</em>' containment reference.
   * @see #getRedirect()
   * @generated
   */
  void setRedirect(RedirectWithParamSpec value);

} // RedirectWithValueAndParamSpec
