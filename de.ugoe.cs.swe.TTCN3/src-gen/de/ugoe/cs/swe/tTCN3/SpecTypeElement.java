/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Spec Type Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SpecTypeElement#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSpecTypeElement()
 * @model
 * @generated
 */
public interface SpecTypeElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tail</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tail</em>' containment reference.
   * @see #setTail(TypeReferenceTail)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSpecTypeElement_Tail()
   * @model containment="true"
   * @generated
   */
  TypeReferenceTail getTail();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SpecTypeElement#getTail <em>Tail</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tail</em>' containment reference.
   * @see #getTail()
   * @generated
   */
  void setTail(TypeReferenceTail value);

} // SpecTypeElement
