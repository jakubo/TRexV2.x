/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Par Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleParDef#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleParDef#getMultitypeParam <em>Multitype Param</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleParDef()
 * @model
 * @generated
 */
public interface ModuleParDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param</em>' containment reference.
   * @see #setParam(ModulePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleParDef_Param()
   * @model containment="true"
   * @generated
   */
  ModulePar getParam();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleParDef#getParam <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param</em>' containment reference.
   * @see #getParam()
   * @generated
   */
  void setParam(ModulePar value);

  /**
   * Returns the value of the '<em><b>Multitype Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Multitype Param</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Multitype Param</em>' containment reference.
   * @see #setMultitypeParam(MultitypedModuleParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleParDef_MultitypeParam()
   * @model containment="true"
   * @generated
   */
  MultitypedModuleParList getMultitypeParam();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleParDef#getMultitypeParam <em>Multitype Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Multitype Param</em>' containment reference.
   * @see #getMultitypeParam()
   * @generated
   */
  void setMultitypeParam(MultitypedModuleParList value);

} // ModuleParDef
