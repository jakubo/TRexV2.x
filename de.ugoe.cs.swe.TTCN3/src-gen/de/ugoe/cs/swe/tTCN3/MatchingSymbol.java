/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Matching Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getComplement <em>Complement</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getQwlm <em>Qwlm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSwlm <em>Swlm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getRange <em>Range</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getString <em>String</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSubset <em>Subset</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSuperset <em>Superset</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getTemplates <em>Templates</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol()
 * @model
 * @generated
 */
public interface MatchingSymbol extends EObject
{
  /**
   * Returns the value of the '<em><b>Complement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Complement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Complement</em>' containment reference.
   * @see #setComplement(Complement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Complement()
   * @model containment="true"
   * @generated
   */
  Complement getComplement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getComplement <em>Complement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Complement</em>' containment reference.
   * @see #getComplement()
   * @generated
   */
  void setComplement(Complement value);

  /**
   * Returns the value of the '<em><b>Qwlm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Qwlm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Qwlm</em>' containment reference.
   * @see #setQwlm(WildcardLengthMatch)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Qwlm()
   * @model containment="true"
   * @generated
   */
  WildcardLengthMatch getQwlm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getQwlm <em>Qwlm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Qwlm</em>' containment reference.
   * @see #getQwlm()
   * @generated
   */
  void setQwlm(WildcardLengthMatch value);

  /**
   * Returns the value of the '<em><b>Swlm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Swlm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Swlm</em>' containment reference.
   * @see #setSwlm(WildcardLengthMatch)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Swlm()
   * @model containment="true"
   * @generated
   */
  WildcardLengthMatch getSwlm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSwlm <em>Swlm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Swlm</em>' containment reference.
   * @see #getSwlm()
   * @generated
   */
  void setSwlm(WildcardLengthMatch value);

  /**
   * Returns the value of the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Range</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range</em>' containment reference.
   * @see #setRange(Range)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Range()
   * @model containment="true"
   * @generated
   */
  Range getRange();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getRange <em>Range</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Range</em>' containment reference.
   * @see #getRange()
   * @generated
   */
  void setRange(Range value);

  /**
   * Returns the value of the '<em><b>String</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>String</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>String</em>' containment reference.
   * @see #setString(CharStringMatch)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_String()
   * @model containment="true"
   * @generated
   */
  CharStringMatch getString();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getString <em>String</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>String</em>' containment reference.
   * @see #getString()
   * @generated
   */
  void setString(CharStringMatch value);

  /**
   * Returns the value of the '<em><b>Subset</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subset</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subset</em>' containment reference.
   * @see #setSubset(SubsetMatch)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Subset()
   * @model containment="true"
   * @generated
   */
  SubsetMatch getSubset();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSubset <em>Subset</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subset</em>' containment reference.
   * @see #getSubset()
   * @generated
   */
  void setSubset(SubsetMatch value);

  /**
   * Returns the value of the '<em><b>Superset</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Superset</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Superset</em>' containment reference.
   * @see #setSuperset(SupersetMatch)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Superset()
   * @model containment="true"
   * @generated
   */
  SupersetMatch getSuperset();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSuperset <em>Superset</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Superset</em>' containment reference.
   * @see #getSuperset()
   * @generated
   */
  void setSuperset(SupersetMatch value);

  /**
   * Returns the value of the '<em><b>Templates</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Templates</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Templates</em>' containment reference.
   * @see #setTemplates(ListOfTemplates)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMatchingSymbol_Templates()
   * @model containment="true"
   * @generated
   */
  ListOfTemplates getTemplates();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getTemplates <em>Templates</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Templates</em>' containment reference.
   * @see #getTemplates()
   * @generated
   */
  void setTemplates(ListOfTemplates value);

} // MatchingSymbol
