/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Of Def Named</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetOfDefNamed()
 * @model
 * @generated
 */
public interface SetOfDefNamed extends ReferencedType, SetOfDef
{
} // SetOfDefNamed
