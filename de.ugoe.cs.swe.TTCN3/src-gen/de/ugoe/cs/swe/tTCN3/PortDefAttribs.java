/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Def Attribs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMessage <em>Message</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getProcedure <em>Procedure</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMixed <em>Mixed</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortDefAttribs()
 * @model
 * @generated
 */
public interface PortDefAttribs extends EObject
{
  /**
   * Returns the value of the '<em><b>Message</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Message</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Message</em>' containment reference.
   * @see #setMessage(MessageAttribs)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortDefAttribs_Message()
   * @model containment="true"
   * @generated
   */
  MessageAttribs getMessage();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMessage <em>Message</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Message</em>' containment reference.
   * @see #getMessage()
   * @generated
   */
  void setMessage(MessageAttribs value);

  /**
   * Returns the value of the '<em><b>Procedure</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Procedure</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Procedure</em>' containment reference.
   * @see #setProcedure(ProcedureAttribs)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortDefAttribs_Procedure()
   * @model containment="true"
   * @generated
   */
  ProcedureAttribs getProcedure();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getProcedure <em>Procedure</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Procedure</em>' containment reference.
   * @see #getProcedure()
   * @generated
   */
  void setProcedure(ProcedureAttribs value);

  /**
   * Returns the value of the '<em><b>Mixed</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mixed</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mixed</em>' containment reference.
   * @see #setMixed(MixedAttribs)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortDefAttribs_Mixed()
   * @model containment="true"
   * @generated
   */
  MixedAttribs getMixed();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMixed <em>Mixed</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mixed</em>' containment reference.
   * @see #getMixed()
   * @generated
   */
  void setMixed(MixedAttribs value);

} // PortDefAttribs
