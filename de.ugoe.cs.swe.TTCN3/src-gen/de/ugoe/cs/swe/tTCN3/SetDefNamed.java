/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Def Named</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetDefNamed()
 * @model
 * @generated
 */
public interface SetDefNamed extends ReferencedType, SetDef
{
} // SetDefNamed
