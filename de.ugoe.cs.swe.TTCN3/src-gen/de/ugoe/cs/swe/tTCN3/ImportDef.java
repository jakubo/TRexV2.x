/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportDef#getName <em>Name</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportDef#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportDef#getAll <em>All</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportDef#getImportSpec <em>Import Spec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportDef()
 * @model
 * @generated
 */
public interface ImportDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportDef_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(LanguageSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportDef_Spec()
   * @model containment="true"
   * @generated
   */
  LanguageSpec getSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(LanguageSpec value);

  /**
   * Returns the value of the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' containment reference.
   * @see #setAll(AllWithExcepts)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportDef_All()
   * @model containment="true"
   * @generated
   */
  AllWithExcepts getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getAll <em>All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' containment reference.
   * @see #getAll()
   * @generated
   */
  void setAll(AllWithExcepts value);

  /**
   * Returns the value of the '<em><b>Import Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Import Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Import Spec</em>' containment reference.
   * @see #setImportSpec(ImportSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportDef_ImportSpec()
   * @model containment="true"
   * @generated
   */
  ImportSpec getImportSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getImportSpec <em>Import Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Import Spec</em>' containment reference.
   * @see #getImportSpec()
   * @generated
   */
  void setImportSpec(ImportSpec value);

} // ImportDef
