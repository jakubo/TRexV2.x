/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Group Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroupRef <em>Group Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroup <em>Group</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportGroupSpec()
 * @model
 * @generated
 */
public interface ImportGroupSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Group Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group Ref</em>' containment reference.
   * @see #setGroupRef(GroupRefListWithExcept)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportGroupSpec_GroupRef()
   * @model containment="true"
   * @generated
   */
  GroupRefListWithExcept getGroupRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroupRef <em>Group Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Group Ref</em>' containment reference.
   * @see #getGroupRef()
   * @generated
   */
  void setGroupRef(GroupRefListWithExcept value);

  /**
   * Returns the value of the '<em><b>Group</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' containment reference.
   * @see #setGroup(AllGroupsWithExcept)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportGroupSpec_Group()
   * @model containment="true"
   * @generated
   */
  AllGroupsWithExcept getGroup();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroup <em>Group</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Group</em>' containment reference.
   * @see #getGroup()
   * @generated
   */
  void setGroup(AllGroupsWithExcept value);

} // ImportGroupSpec
