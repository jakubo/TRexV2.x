/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ftestcasename</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFtestcasename()
 * @model
 * @generated
 */
public interface Ftestcasename extends PreDefFunction
{
} // Ftestcasename
