/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Identifier List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList#getQids <em>Qids</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getQualifiedIdentifierList()
 * @model
 * @generated
 */
public interface QualifiedIdentifierList extends EObject
{
  /**
   * Returns the value of the '<em><b>Qids</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Qids</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Qids</em>' attribute list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getQualifiedIdentifierList_Qids()
   * @model unique="false"
   * @generated
   */
  EList<String> getQids();

} // QualifiedIdentifierList
