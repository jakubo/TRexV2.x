/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receive Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement#getAny <em>Any</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement#getReceive <em>Receive</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getReceiveStatement()
 * @model
 * @generated
 */
public interface ReceiveStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Any</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any</em>' containment reference.
   * @see #setAny(PortOrAny)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getReceiveStatement_Any()
   * @model containment="true"
   * @generated
   */
  PortOrAny getAny();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement#getAny <em>Any</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Any</em>' containment reference.
   * @see #getAny()
   * @generated
   */
  void setAny(PortOrAny value);

  /**
   * Returns the value of the '<em><b>Receive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Receive</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Receive</em>' containment reference.
   * @see #setReceive(PortReceiveOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getReceiveStatement_Receive()
   * @model containment="true"
   * @generated
   */
  PortReceiveOp getReceive();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement#getReceive <em>Receive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Receive</em>' containment reference.
   * @see #getReceive()
   * @generated
   */
  void setReceive(PortReceiveOp value);

} // ReceiveStatement
