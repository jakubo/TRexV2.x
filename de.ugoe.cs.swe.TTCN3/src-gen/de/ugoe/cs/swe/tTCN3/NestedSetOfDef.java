/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nested Set Of Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getLength <em>Length</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getNested <em>Nested</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getNestedSetOfDef()
 * @model
 * @generated
 */
public interface NestedSetOfDef extends NestedTypeDef
{
  /**
   * Returns the value of the '<em><b>Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Length</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Length</em>' containment reference.
   * @see #setLength(StringLength)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getNestedSetOfDef_Length()
   * @model containment="true"
   * @generated
   */
  StringLength getLength();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getLength <em>Length</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Length</em>' containment reference.
   * @see #getLength()
   * @generated
   */
  void setLength(StringLength value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getNestedSetOfDef_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nested</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nested</em>' containment reference.
   * @see #setNested(NestedTypeDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getNestedSetOfDef_Nested()
   * @model containment="true"
   * @generated
   */
  NestedTypeDef getNested();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getNested <em>Nested</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nested</em>' containment reference.
   * @see #getNested()
   * @generated
   */
  void setNested(NestedTypeDef value);

} // NestedSetOfDef
