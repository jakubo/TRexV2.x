/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Def Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortDefBody#getAttribs <em>Attribs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortDefBody()
 * @model
 * @generated
 */
public interface PortDefBody extends EObject
{
  /**
   * Returns the value of the '<em><b>Attribs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Attribs</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Attribs</em>' containment reference.
   * @see #setAttribs(PortDefAttribs)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortDefBody_Attribs()
   * @model containment="true"
   * @generated
   */
  PortDefAttribs getAttribs();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortDefBody#getAttribs <em>Attribs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Attribs</em>' containment reference.
   * @see #getAttribs()
   * @generated
   */
  void setAttribs(PortDefAttribs value);

} // PortDefBody
