/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From Clause Present</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFromClausePresent()
 * @model
 * @generated
 */
public interface FromClausePresent extends CheckParameter
{
} // FromClausePresent
