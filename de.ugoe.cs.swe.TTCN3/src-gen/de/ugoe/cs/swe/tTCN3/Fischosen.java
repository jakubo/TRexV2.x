/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fischosen</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Fischosen#getT1 <em>T1</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFischosen()
 * @model
 * @generated
 */
public interface Fischosen extends PreDefFunction
{
  /**
   * Returns the value of the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>T1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>T1</em>' containment reference.
   * @see #setT1(TemplateOrRange)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFischosen_T1()
   * @model containment="true"
   * @generated
   */
  TemplateOrRange getT1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Fischosen#getT1 <em>T1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>T1</em>' containment reference.
   * @see #getT1()
   * @generated
   */
  void setT1(TemplateOrRange value);

} // Fischosen
