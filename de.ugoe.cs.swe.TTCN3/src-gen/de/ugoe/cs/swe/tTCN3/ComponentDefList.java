/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Def List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getElement <em>Element</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getWst <em>Wst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDefList()
 * @model
 * @generated
 */
public interface ComponentDefList extends EObject
{
  /**
   * Returns the value of the '<em><b>Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Element</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Element</em>' containment reference.
   * @see #setElement(ComponentElementDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDefList_Element()
   * @model containment="true"
   * @generated
   */
  ComponentElementDef getElement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getElement <em>Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Element</em>' containment reference.
   * @see #getElement()
   * @generated
   */
  void setElement(ComponentElementDef value);

  /**
   * Returns the value of the '<em><b>Wst</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Wst</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Wst</em>' containment reference.
   * @see #setWst(WithStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDefList_Wst()
   * @model containment="true"
   * @generated
   */
  WithStatement getWst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getWst <em>Wst</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Wst</em>' containment reference.
   * @see #getWst()
   * @generated
   */
  void setWst(WithStatement value);

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute.
   * @see #setSc(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDefList_Sc()
   * @model
   * @generated
   */
  String getSc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getSc <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sc</em>' attribute.
   * @see #getSc()
   * @generated
   */
  void setSc(String value);

} // ComponentDefList
