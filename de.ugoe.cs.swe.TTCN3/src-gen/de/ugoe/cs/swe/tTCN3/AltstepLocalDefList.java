/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Altstep Local Def List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getDefs <em>Defs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getWithstats <em>Withstats</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDefList()
 * @model
 * @generated
 */
public interface AltstepLocalDefList extends EObject
{
  /**
   * Returns the value of the '<em><b>Defs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Defs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDefList_Defs()
   * @model containment="true"
   * @generated
   */
  EList<AltstepLocalDef> getDefs();

  /**
   * Returns the value of the '<em><b>Withstats</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.WithStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Withstats</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Withstats</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDefList_Withstats()
   * @model containment="true"
   * @generated
   */
  EList<WithStatement> getWithstats();

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepLocalDefList_Sc()
   * @model unique="false"
   * @generated
   */
  EList<String> getSc();

} // AltstepLocalDefList
