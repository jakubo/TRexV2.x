/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>All Or Signature List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getAll <em>All</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getSignatureList <em>Signature List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllOrSignatureList()
 * @model
 * @generated
 */
public interface AllOrSignatureList extends EObject
{
  /**
   * Returns the value of the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' attribute.
   * @see #setAll(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllOrSignatureList_All()
   * @model
   * @generated
   */
  String getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getAll <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' attribute.
   * @see #getAll()
   * @generated
   */
  void setAll(String value);

  /**
   * Returns the value of the '<em><b>Signature List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Signature List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Signature List</em>' containment reference.
   * @see #setSignatureList(SignatureList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllOrSignatureList_SignatureList()
   * @model containment="true"
   * @generated
   */
  SignatureList getSignatureList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getSignatureList <em>Signature List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Signature List</em>' containment reference.
   * @see #getSignatureList()
   * @generated
   */
  void setSignatureList(SignatureList value);

} // AllOrSignatureList
