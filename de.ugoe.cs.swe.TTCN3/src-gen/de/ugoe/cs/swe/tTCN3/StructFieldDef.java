/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct Field Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getNestedType <em>Nested Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getOptional <em>Optional</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructFieldDef()
 * @model
 * @generated
 */
public interface StructFieldDef extends FieldReference
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructFieldDef_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Nested Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nested Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nested Type</em>' containment reference.
   * @see #setNestedType(NestedTypeDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructFieldDef_NestedType()
   * @model containment="true"
   * @generated
   */
  NestedTypeDef getNestedType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getNestedType <em>Nested Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nested Type</em>' containment reference.
   * @see #getNestedType()
   * @generated
   */
  void setNestedType(NestedTypeDef value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructFieldDef_Array()
   * @model containment="true"
   * @generated
   */
  ArrayDef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayDef value);

  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(SubTypeSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructFieldDef_Spec()
   * @model containment="true"
   * @generated
   */
  SubTypeSpec getSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(SubTypeSpec value);

  /**
   * Returns the value of the '<em><b>Optional</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Optional</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Optional</em>' attribute.
   * @see #setOptional(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructFieldDef_Optional()
   * @model
   * @generated
   */
  String getOptional();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getOptional <em>Optional</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Optional</em>' attribute.
   * @see #getOptional()
   * @generated
   */
  void setOptional(String value);

} // StructFieldDef
