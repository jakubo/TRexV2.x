/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Restriction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getOmit <em>Omit</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getPresent <em>Present</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateRestriction()
 * @model
 * @generated
 */
public interface TemplateRestriction extends EObject
{
  /**
   * Returns the value of the '<em><b>Omit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Omit</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Omit</em>' attribute.
   * @see #setOmit(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateRestriction_Omit()
   * @model
   * @generated
   */
  String getOmit();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getOmit <em>Omit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Omit</em>' attribute.
   * @see #getOmit()
   * @generated
   */
  void setOmit(String value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateRestriction_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>Present</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Present</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Present</em>' attribute.
   * @see #setPresent(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateRestriction_Present()
   * @model
   * @generated
   */
  String getPresent();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getPresent <em>Present</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Present</em>' attribute.
   * @see #getPresent()
   * @generated
   */
  void setPresent(String value);

} // TemplateRestriction
