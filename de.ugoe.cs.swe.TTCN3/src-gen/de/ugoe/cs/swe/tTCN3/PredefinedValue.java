/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Predefined Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getBstring <em>Bstring</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getBoolean <em>Boolean</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getInteger <em>Integer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getHstring <em>Hstring</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getOstring <em>Ostring</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getVerdictType <em>Verdict Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getFloat <em>Float</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getAddress <em>Address</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getOmit <em>Omit</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getCharString <em>Char String</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getMacro <em>Macro</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue()
 * @model
 * @generated
 */
public interface PredefinedValue extends EObject
{
  /**
   * Returns the value of the '<em><b>Bstring</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bstring</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bstring</em>' attribute.
   * @see #setBstring(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Bstring()
   * @model
   * @generated
   */
  String getBstring();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getBstring <em>Bstring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bstring</em>' attribute.
   * @see #getBstring()
   * @generated
   */
  void setBstring(String value);

  /**
   * Returns the value of the '<em><b>Boolean</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Boolean</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Boolean</em>' attribute.
   * @see #setBoolean(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Boolean()
   * @model
   * @generated
   */
  String getBoolean();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getBoolean <em>Boolean</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Boolean</em>' attribute.
   * @see #getBoolean()
   * @generated
   */
  void setBoolean(String value);

  /**
   * Returns the value of the '<em><b>Integer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Integer</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Integer</em>' attribute.
   * @see #setInteger(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Integer()
   * @model
   * @generated
   */
  String getInteger();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getInteger <em>Integer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Integer</em>' attribute.
   * @see #getInteger()
   * @generated
   */
  void setInteger(String value);

  /**
   * Returns the value of the '<em><b>Hstring</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Hstring</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Hstring</em>' attribute.
   * @see #setHstring(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Hstring()
   * @model
   * @generated
   */
  String getHstring();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getHstring <em>Hstring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Hstring</em>' attribute.
   * @see #getHstring()
   * @generated
   */
  void setHstring(String value);

  /**
   * Returns the value of the '<em><b>Ostring</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ostring</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ostring</em>' attribute.
   * @see #setOstring(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Ostring()
   * @model
   * @generated
   */
  String getOstring();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getOstring <em>Ostring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ostring</em>' attribute.
   * @see #getOstring()
   * @generated
   */
  void setOstring(String value);

  /**
   * Returns the value of the '<em><b>Verdict Type</b></em>' attribute.
   * The literals are from the enumeration {@link de.ugoe.cs.swe.tTCN3.VerdictTypeValue}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Verdict Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Verdict Type</em>' attribute.
   * @see de.ugoe.cs.swe.tTCN3.VerdictTypeValue
   * @see #setVerdictType(VerdictTypeValue)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_VerdictType()
   * @model
   * @generated
   */
  VerdictTypeValue getVerdictType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getVerdictType <em>Verdict Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Verdict Type</em>' attribute.
   * @see de.ugoe.cs.swe.tTCN3.VerdictTypeValue
   * @see #getVerdictType()
   * @generated
   */
  void setVerdictType(VerdictTypeValue value);

  /**
   * Returns the value of the '<em><b>Float</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Float</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Float</em>' attribute.
   * @see #setFloat(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Float()
   * @model
   * @generated
   */
  String getFloat();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getFloat <em>Float</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Float</em>' attribute.
   * @see #getFloat()
   * @generated
   */
  void setFloat(String value);

  /**
   * Returns the value of the '<em><b>Address</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Address</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Address</em>' attribute.
   * @see #setAddress(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Address()
   * @model
   * @generated
   */
  String getAddress();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getAddress <em>Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Address</em>' attribute.
   * @see #getAddress()
   * @generated
   */
  void setAddress(String value);

  /**
   * Returns the value of the '<em><b>Omit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Omit</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Omit</em>' attribute.
   * @see #setOmit(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Omit()
   * @model
   * @generated
   */
  String getOmit();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getOmit <em>Omit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Omit</em>' attribute.
   * @see #getOmit()
   * @generated
   */
  void setOmit(String value);

  /**
   * Returns the value of the '<em><b>Char String</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Char String</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Char String</em>' attribute.
   * @see #setCharString(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_CharString()
   * @model
   * @generated
   */
  String getCharString();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getCharString <em>Char String</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Char String</em>' attribute.
   * @see #getCharString()
   * @generated
   */
  void setCharString(String value);

  /**
   * Returns the value of the '<em><b>Macro</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Macro</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Macro</em>' attribute.
   * @see #setMacro(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPredefinedValue_Macro()
   * @model
   * @generated
   */
  String getMacro();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getMacro <em>Macro</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Macro</em>' attribute.
   * @see #getMacro()
   * @generated
   */
  void setMacro(String value);

} // PredefinedValue
