/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bit Or Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBitOrExpression()
 * @model
 * @generated
 */
public interface BitOrExpression extends SingleExpression
{
} // BitOrExpression
