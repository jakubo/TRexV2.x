/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stop Timer Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StopTimerStatement#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStopTimerStatement()
 * @model
 * @generated
 */
public interface StopTimerStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(TimerRefOrAll)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStopTimerStatement_Ref()
   * @model containment="true"
   * @generated
   */
  TimerRefOrAll getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StopTimerStatement#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(TimerRefOrAll value);

} // StopTimerStatement
