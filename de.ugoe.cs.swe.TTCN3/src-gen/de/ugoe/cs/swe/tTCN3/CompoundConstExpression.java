/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound Const Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCompoundConstExpression()
 * @model
 * @generated
 */
public interface CompoundConstExpression extends ConstantExpression
{
} // CompoundConstExpression
