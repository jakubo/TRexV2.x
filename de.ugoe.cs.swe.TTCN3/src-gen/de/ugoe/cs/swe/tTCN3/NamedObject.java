/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getNamedObject()
 * @model
 * @generated
 */
public interface NamedObject extends RefValue
{
} // NamedObject
