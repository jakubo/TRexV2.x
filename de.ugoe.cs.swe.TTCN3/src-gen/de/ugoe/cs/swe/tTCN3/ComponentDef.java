/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentDef#getExtends <em>Extends</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentDef#getDefs <em>Defs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDef()
 * @model
 * @generated
 */
public interface ComponentDef extends ReferencedType, TimerVarInstance
{
  /**
   * Returns the value of the '<em><b>Extends</b></em>' reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ComponentDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extends</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extends</em>' reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDef_Extends()
   * @model
   * @generated
   */
  EList<ComponentDef> getExtends();

  /**
   * Returns the value of the '<em><b>Defs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ComponentDefList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Defs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentDef_Defs()
   * @model containment="true"
   * @generated
   */
  EList<ComponentDefList> getDefs();

} // ComponentDef
