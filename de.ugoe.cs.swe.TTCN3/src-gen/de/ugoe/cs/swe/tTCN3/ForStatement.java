/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ForStatement#getInit <em>Init</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ForStatement#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ForStatement#getAssign <em>Assign</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ForStatement#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getForStatement()
 * @model
 * @generated
 */
public interface ForStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Init</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Init</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Init</em>' containment reference.
   * @see #setInit(Initial)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getForStatement_Init()
   * @model containment="true"
   * @generated
   */
  Initial getInit();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getInit <em>Init</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Init</em>' containment reference.
   * @see #getInit()
   * @generated
   */
  void setInit(Initial value);

  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(BooleanExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getForStatement_Expression()
   * @model containment="true"
   * @generated
   */
  BooleanExpression getExpression();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(BooleanExpression value);

  /**
   * Returns the value of the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assign</em>' containment reference.
   * @see #setAssign(Assignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getForStatement_Assign()
   * @model containment="true"
   * @generated
   */
  Assignment getAssign();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getAssign <em>Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Assign</em>' containment reference.
   * @see #getAssign()
   * @generated
   */
  void setAssign(Assignment value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getForStatement_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

} // ForStatement
