/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Reference Tail</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getArray <em>Array</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeReferenceTail()
 * @model
 * @generated
 */
public interface TypeReferenceTail extends SpecTypeElement
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' reference.
   * @see #setType(TypeReferenceTailType)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeReferenceTail_Type()
   * @model
   * @generated
   */
  TypeReferenceTailType getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getType <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' reference.
   * @see #getType()
   * @generated
   */
  void setType(TypeReferenceTailType value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayOrBitRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeReferenceTail_Array()
   * @model containment="true"
   * @generated
   */
  ArrayOrBitRef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayOrBitRef value);

} // TypeReferenceTail
