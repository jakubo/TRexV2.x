/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Of Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getSetLength <em>Set Length</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getNested <em>Nested</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getSetSpec <em>Set Spec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetOfDef()
 * @model
 * @generated
 */
public interface SetOfDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Set Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Set Length</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Set Length</em>' containment reference.
   * @see #setSetLength(StringLength)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetOfDef_SetLength()
   * @model containment="true"
   * @generated
   */
  StringLength getSetLength();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getSetLength <em>Set Length</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Set Length</em>' containment reference.
   * @see #getSetLength()
   * @generated
   */
  void setSetLength(StringLength value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetOfDef_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nested</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nested</em>' containment reference.
   * @see #setNested(NestedTypeDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetOfDef_Nested()
   * @model containment="true"
   * @generated
   */
  NestedTypeDef getNested();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getNested <em>Nested</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nested</em>' containment reference.
   * @see #getNested()
   * @generated
   */
  void setNested(NestedTypeDef value);

  /**
   * Returns the value of the '<em><b>Set Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Set Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Set Spec</em>' containment reference.
   * @see #setSetSpec(SubTypeSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetOfDef_SetSpec()
   * @model containment="true"
   * @generated
   */
  SubTypeSpec getSetSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getSetSpec <em>Set Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Set Spec</em>' containment reference.
   * @see #getSetSpec()
   * @generated
   */
  void setSetSpec(SubTypeSpec value);

} // SetOfDef
