/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>All Or Type List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList#getAll <em>All</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList#getTypeList <em>Type List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllOrTypeList()
 * @model
 * @generated
 */
public interface AllOrTypeList extends EObject
{
  /**
   * Returns the value of the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' attribute.
   * @see #setAll(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllOrTypeList_All()
   * @model
   * @generated
   */
  String getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList#getAll <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' attribute.
   * @see #getAll()
   * @generated
   */
  void setAll(String value);

  /**
   * Returns the value of the '<em><b>Type List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type List</em>' containment reference.
   * @see #setTypeList(TypeList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllOrTypeList_TypeList()
   * @model containment="true"
   * @generated
   */
  TypeList getTypeList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList#getTypeList <em>Type List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type List</em>' containment reference.
   * @see #getTypeList()
   * @generated
   */
  void setTypeList(TypeList value);

} // AllOrTypeList
