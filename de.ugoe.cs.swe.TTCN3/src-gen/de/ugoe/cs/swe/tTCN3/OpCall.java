/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getVerdict <em>Verdict</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getExtendedFunction <em>Extended Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getPreFunction <em>Pre Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getActivate <em>Activate</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getTemplateOps <em>Template Ops</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getExtendedTemplate <em>Extended Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.OpCall#getField <em>Field</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall()
 * @model
 * @generated
 */
public interface OpCall extends SingleExpression
{
  /**
   * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Configuration</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Configuration</em>' containment reference.
   * @see #setConfiguration(ConfigurationOps)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Configuration()
   * @model containment="true"
   * @generated
   */
  ConfigurationOps getConfiguration();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getConfiguration <em>Configuration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Configuration</em>' containment reference.
   * @see #getConfiguration()
   * @generated
   */
  void setConfiguration(ConfigurationOps value);

  /**
   * Returns the value of the '<em><b>Verdict</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Verdict</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Verdict</em>' attribute.
   * @see #setVerdict(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Verdict()
   * @model
   * @generated
   */
  String getVerdict();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getVerdict <em>Verdict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Verdict</em>' attribute.
   * @see #getVerdict()
   * @generated
   */
  void setVerdict(String value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(TimerOps)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Timer()
   * @model containment="true"
   * @generated
   */
  TimerOps getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerOps value);

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference.
   * @see #setFunction(FunctionInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Function()
   * @model containment="true"
   * @generated
   */
  FunctionInstance getFunction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getFunction <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' containment reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(FunctionInstance value);

  /**
   * Returns the value of the '<em><b>Extended Function</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extended Function</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extended Function</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_ExtendedFunction()
   * @model containment="true"
   * @generated
   */
  EList<ExtendedFieldReference> getExtendedFunction();

  /**
   * Returns the value of the '<em><b>Pre Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pre Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pre Function</em>' containment reference.
   * @see #setPreFunction(PreDefFunction)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_PreFunction()
   * @model containment="true"
   * @generated
   */
  PreDefFunction getPreFunction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getPreFunction <em>Pre Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pre Function</em>' containment reference.
   * @see #getPreFunction()
   * @generated
   */
  void setPreFunction(PreDefFunction value);

  /**
   * Returns the value of the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Testcase</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Testcase</em>' containment reference.
   * @see #setTestcase(TestcaseInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Testcase()
   * @model containment="true"
   * @generated
   */
  TestcaseInstance getTestcase();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getTestcase <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Testcase</em>' containment reference.
   * @see #getTestcase()
   * @generated
   */
  void setTestcase(TestcaseInstance value);

  /**
   * Returns the value of the '<em><b>Activate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Activate</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Activate</em>' containment reference.
   * @see #setActivate(ActivateOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Activate()
   * @model containment="true"
   * @generated
   */
  ActivateOp getActivate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getActivate <em>Activate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Activate</em>' containment reference.
   * @see #getActivate()
   * @generated
   */
  void setActivate(ActivateOp value);

  /**
   * Returns the value of the '<em><b>Template Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template Ops</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template Ops</em>' containment reference.
   * @see #setTemplateOps(TemplateOps)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_TemplateOps()
   * @model containment="true"
   * @generated
   */
  TemplateOps getTemplateOps();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.OpCall#getTemplateOps <em>Template Ops</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template Ops</em>' containment reference.
   * @see #getTemplateOps()
   * @generated
   */
  void setTemplateOps(TemplateOps value);

  /**
   * Returns the value of the '<em><b>Extended Template</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extended Template</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extended Template</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_ExtendedTemplate()
   * @model containment="true"
   * @generated
   */
  EList<ExtendedFieldReference> getExtendedTemplate();

  /**
   * Returns the value of the '<em><b>Field</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getOpCall_Field()
   * @model containment="true"
   * @generated
   */
  EList<ExtendedFieldReference> getField();

} // OpCall
