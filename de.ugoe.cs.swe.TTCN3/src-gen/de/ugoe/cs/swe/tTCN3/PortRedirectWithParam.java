/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Redirect With Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithParam#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirectWithParam()
 * @model
 * @generated
 */
public interface PortRedirectWithParam extends EObject
{
  /**
   * Returns the value of the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Redirect</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Redirect</em>' containment reference.
   * @see #setRedirect(RedirectWithParamSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirectWithParam_Redirect()
   * @model containment="true"
   * @generated
   */
  RedirectWithParamSpec getRedirect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithParam#getRedirect <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Redirect</em>' containment reference.
   * @see #getRedirect()
   * @generated
   */
  void setRedirect(RedirectWithParamSpec value);

} // PortRedirectWithParam
