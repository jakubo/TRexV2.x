/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getDef <em>Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPublicGroup <em>Public Group</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPrivateFriend <em>Private Friend</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinition()
 * @model
 * @generated
 */
public interface ModuleDefinition extends EObject
{
  /**
   * Returns the value of the '<em><b>Visibility</b></em>' attribute.
   * The literals are from the enumeration {@link de.ugoe.cs.swe.tTCN3.Visibility}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Visibility</em>' attribute.
   * @see de.ugoe.cs.swe.tTCN3.Visibility
   * @see #setVisibility(Visibility)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinition_Visibility()
   * @model
   * @generated
   */
  Visibility getVisibility();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getVisibility <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Visibility</em>' attribute.
   * @see de.ugoe.cs.swe.tTCN3.Visibility
   * @see #getVisibility()
   * @generated
   */
  void setVisibility(Visibility value);

  /**
   * Returns the value of the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Def</em>' containment reference.
   * @see #setDef(EObject)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinition_Def()
   * @model containment="true"
   * @generated
   */
  EObject getDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getDef <em>Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Def</em>' containment reference.
   * @see #getDef()
   * @generated
   */
  void setDef(EObject value);

  /**
   * Returns the value of the '<em><b>Public Group</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Public Group</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Public Group</em>' attribute.
   * @see #setPublicGroup(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinition_PublicGroup()
   * @model
   * @generated
   */
  String getPublicGroup();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPublicGroup <em>Public Group</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Public Group</em>' attribute.
   * @see #getPublicGroup()
   * @generated
   */
  void setPublicGroup(String value);

  /**
   * Returns the value of the '<em><b>Private Friend</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Private Friend</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Private Friend</em>' attribute.
   * @see #setPrivateFriend(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinition_PrivateFriend()
   * @model
   * @generated
   */
  String getPrivateFriend();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPrivateFriend <em>Private Friend</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Private Friend</em>' attribute.
   * @see #getPrivateFriend()
   * @generated
   */
  void setPrivateFriend(String value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(WithStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinition_Statement()
   * @model containment="true"
   * @generated
   */
  WithStatement getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(WithStatement value);

} // ModuleDefinition
