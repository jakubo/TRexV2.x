/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Length</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StringLength#getExpr <em>Expr</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StringLength#getBound <em>Bound</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStringLength()
 * @model
 * @generated
 */
public interface StringLength extends ExtraMatchingAttributes
{
  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference.
   * @see #setExpr(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStringLength_Expr()
   * @model containment="true"
   * @generated
   */
  SingleExpression getExpr();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StringLength#getExpr <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr</em>' containment reference.
   * @see #getExpr()
   * @generated
   */
  void setExpr(SingleExpression value);

  /**
   * Returns the value of the '<em><b>Bound</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bound</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bound</em>' containment reference.
   * @see #setBound(Bound)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStringLength_Bound()
   * @model containment="true"
   * @generated
   */
  Bound getBound();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StringLength#getBound <em>Bound</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bound</em>' containment reference.
   * @see #getBound()
   * @generated
   */
  void setBound(Bound value);

} // StringLength
