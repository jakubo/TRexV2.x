/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Check Port Ops Present</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCheckPortOpsPresent()
 * @model
 * @generated
 */
public interface CheckPortOpsPresent extends CheckParameter
{
  /**
   * Returns the value of the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>From</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>From</em>' containment reference.
   * @see #setFrom(FromClause)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCheckPortOpsPresent_From()
   * @model containment="true"
   * @generated
   */
  FromClause getFrom();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent#getFrom <em>From</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>From</em>' containment reference.
   * @see #getFrom()
   * @generated
   */
  void setFrom(FromClause value);

} // CheckPortOpsPresent
