/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Freplace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Freplace#getE1 <em>E1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Freplace#getE2 <em>E2</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Freplace#getE3 <em>E3</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Freplace#getE4 <em>E4</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFreplace()
 * @model
 * @generated
 */
public interface Freplace extends PreDefFunction
{
  /**
   * Returns the value of the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E1</em>' containment reference.
   * @see #setE1(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFreplace_E1()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE1 <em>E1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E1</em>' containment reference.
   * @see #getE1()
   * @generated
   */
  void setE1(SingleExpression value);

  /**
   * Returns the value of the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E2</em>' containment reference.
   * @see #setE2(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFreplace_E2()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE2();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE2 <em>E2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E2</em>' containment reference.
   * @see #getE2()
   * @generated
   */
  void setE2(SingleExpression value);

  /**
   * Returns the value of the '<em><b>E3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E3</em>' containment reference.
   * @see #setE3(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFreplace_E3()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE3();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE3 <em>E3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E3</em>' containment reference.
   * @see #getE3()
   * @generated
   */
  void setE3(SingleExpression value);

  /**
   * Returns the value of the '<em><b>E4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E4</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E4</em>' containment reference.
   * @see #setE4(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFreplace_E4()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE4();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE4 <em>E4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E4</em>' containment reference.
   * @see #getE4()
   * @generated
   */
  void setE4(SingleExpression value);

} // Freplace
