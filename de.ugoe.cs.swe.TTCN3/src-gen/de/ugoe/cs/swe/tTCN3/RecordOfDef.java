/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Of Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getLenght <em>Lenght</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getNested <em>Nested</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getSpec <em>Spec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRecordOfDef()
 * @model
 * @generated
 */
public interface RecordOfDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Lenght</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lenght</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lenght</em>' containment reference.
   * @see #setLenght(StringLength)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRecordOfDef_Lenght()
   * @model containment="true"
   * @generated
   */
  StringLength getLenght();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getLenght <em>Lenght</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lenght</em>' containment reference.
   * @see #getLenght()
   * @generated
   */
  void setLenght(StringLength value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRecordOfDef_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nested</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nested</em>' containment reference.
   * @see #setNested(NestedTypeDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRecordOfDef_Nested()
   * @model containment="true"
   * @generated
   */
  NestedTypeDef getNested();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getNested <em>Nested</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nested</em>' containment reference.
   * @see #getNested()
   * @generated
   */
  void setNested(NestedTypeDef value);

  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(SubTypeSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRecordOfDef_Spec()
   * @model containment="true"
   * @generated
   */
  SubTypeSpec getSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(SubTypeSpec value);

} // RecordOfDef
