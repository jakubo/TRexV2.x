/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Ref List With Except</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept#getQualifier <em>Qualifier</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGroupRefListWithExcept()
 * @model
 * @generated
 */
public interface GroupRefListWithExcept extends EObject
{
  /**
   * Returns the value of the '<em><b>Qualifier</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Qualifier</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Qualifier</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGroupRefListWithExcept_Qualifier()
   * @model containment="true"
   * @generated
   */
  EList<QualifiedIdentifierWithExcept> getQualifier();

} // GroupRefListWithExcept
