/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>With Attrib List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.WithAttribList#getMulti <em>Multi</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getWithAttribList()
 * @model
 * @generated
 */
public interface WithAttribList extends EObject
{
  /**
   * Returns the value of the '<em><b>Multi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Multi</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Multi</em>' containment reference.
   * @see #setMulti(MultiWithAttrib)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getWithAttribList_Multi()
   * @model containment="true"
   * @generated
   */
  MultiWithAttrib getMulti();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.WithAttribList#getMulti <em>Multi</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Multi</em>' containment reference.
   * @see #getMulti()
   * @generated
   */
  void setMulti(MultiWithAttrib value);

} // WithAttribList
