/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>With Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.WithStatement#getAttrib <em>Attrib</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getWithStatement()
 * @model
 * @generated
 */
public interface WithStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Attrib</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Attrib</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Attrib</em>' containment reference.
   * @see #setAttrib(WithAttribList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getWithStatement_Attrib()
   * @model containment="true"
   * @generated
   */
  WithAttribList getAttrib();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.WithStatement#getAttrib <em>Attrib</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Attrib</em>' containment reference.
   * @see #getAttrib()
   * @generated
   */
  void setAttrib(WithAttribList value);

} // WithStatement
