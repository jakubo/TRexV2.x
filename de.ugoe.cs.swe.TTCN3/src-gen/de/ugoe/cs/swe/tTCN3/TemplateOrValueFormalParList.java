/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Or Value Formal Par List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList#getParams <em>Params</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateOrValueFormalParList()
 * @model
 * @generated
 */
public interface TemplateOrValueFormalParList extends EObject
{
  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Params</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateOrValueFormalParList_Params()
   * @model containment="true"
   * @generated
   */
  EList<TemplateOrValueFormalPar> getParams();

} // TemplateOrValueFormalParList
