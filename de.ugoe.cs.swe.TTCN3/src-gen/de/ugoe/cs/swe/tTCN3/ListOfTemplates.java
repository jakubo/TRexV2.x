/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List Of Templates</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ListOfTemplates#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getListOfTemplates()
 * @model
 * @generated
 */
public interface ListOfTemplates extends SubsetMatch, SupersetMatch, Complement
{
  /**
   * Returns the value of the '<em><b>Items</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateListItem}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Items</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getListOfTemplates_Items()
   * @model containment="true"
   * @generated
   */
  EList<TemplateListItem> getItems();

} // ListOfTemplates
