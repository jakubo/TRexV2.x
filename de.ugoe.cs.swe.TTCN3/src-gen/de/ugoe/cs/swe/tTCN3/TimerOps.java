/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Ops</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerOps#getRead <em>Read</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerOps#getRun <em>Run</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerOps()
 * @model
 * @generated
 */
public interface TimerOps extends EObject
{
  /**
   * Returns the value of the '<em><b>Read</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Read</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Read</em>' containment reference.
   * @see #setRead(ReadTimerOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerOps_Read()
   * @model containment="true"
   * @generated
   */
  ReadTimerOp getRead();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerOps#getRead <em>Read</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Read</em>' containment reference.
   * @see #getRead()
   * @generated
   */
  void setRead(ReadTimerOp value);

  /**
   * Returns the value of the '<em><b>Run</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Run</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Run</em>' containment reference.
   * @see #setRun(RunningTimerOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerOps_Run()
   * @model containment="true"
   * @generated
   */
  RunningTimerOp getRun();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerOps#getRun <em>Run</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Run</em>' containment reference.
   * @see #getRun()
   * @generated
   */
  void setRun(RunningTimerOp value);

} // TimerOps
