/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldReference()
 * @model
 * @generated
 */
public interface FieldReference extends TypeReferenceTailType, RefValueElement, RefValue
{
} // FieldReference
