/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Construct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getStatement <em>Statement</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElseifs <em>Elseifs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConditionalConstruct()
 * @model
 * @generated
 */
public interface ConditionalConstruct extends EObject
{
  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(BooleanExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConditionalConstruct_Expression()
   * @model containment="true"
   * @generated
   */
  BooleanExpression getExpression();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(BooleanExpression value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConditionalConstruct_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

  /**
   * Returns the value of the '<em><b>Elseifs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ElseIfClause}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elseifs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elseifs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConditionalConstruct_Elseifs()
   * @model containment="true"
   * @generated
   */
  EList<ElseIfClause> getElseifs();

  /**
   * Returns the value of the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else</em>' containment reference.
   * @see #setElse(ElseClause)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConditionalConstruct_Else()
   * @model containment="true"
   * @generated
   */
  ElseClause getElse();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElse <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else</em>' containment reference.
   * @see #getElse()
   * @generated
   */
  void setElse(ElseClause value);

} // ConditionalConstruct
