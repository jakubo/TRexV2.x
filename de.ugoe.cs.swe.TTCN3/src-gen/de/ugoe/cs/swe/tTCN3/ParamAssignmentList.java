/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Param Assignment List</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getParamAssignmentList()
 * @model
 * @generated
 */
public interface ParamAssignmentList extends ParamSpec
{
} // ParamAssignmentList
