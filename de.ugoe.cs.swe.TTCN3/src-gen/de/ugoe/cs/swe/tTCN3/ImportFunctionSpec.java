/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Function Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportFunctionSpec#getIdOrAll <em>Id Or All</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportFunctionSpec()
 * @model
 * @generated
 */
public interface ImportFunctionSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id Or All</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id Or All</em>' containment reference.
   * @see #setIdOrAll(IdentifierListOrAllWithExcept)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportFunctionSpec_IdOrAll()
   * @model containment="true"
   * @generated
   */
  IdentifierListOrAllWithExcept getIdOrAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportFunctionSpec#getIdOrAll <em>Id Or All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id Or All</em>' containment reference.
   * @see #getIdOrAll()
   * @generated
   */
  void setIdOrAll(IdentifierListOrAllWithExcept value);

} // ImportFunctionSpec
