/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Type Def Named</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSubTypeDefNamed()
 * @model
 * @generated
 */
public interface SubTypeDefNamed extends ReferencedType, SubTypeDef, FieldReference
{
} // SubTypeDefNamed
