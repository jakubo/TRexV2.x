/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Redirect Present</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectPresent#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectPresent#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectPresent()
 * @model
 * @generated
 */
public interface RedirectPresent extends CheckParameter
{
  /**
   * Returns the value of the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sender</em>' containment reference.
   * @see #setSender(SenderSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectPresent_Sender()
   * @model containment="true"
   * @generated
   */
  SenderSpec getSender();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectPresent#getSender <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sender</em>' containment reference.
   * @see #getSender()
   * @generated
   */
  void setSender(SenderSpec value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(IndexSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectPresent_Index()
   * @model containment="true"
   * @generated
   */
  IndexSpec getIndex();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectPresent#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(IndexSpec value);

} // RedirectPresent
