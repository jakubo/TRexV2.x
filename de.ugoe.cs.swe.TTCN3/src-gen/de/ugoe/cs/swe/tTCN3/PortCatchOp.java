/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Catch Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortCatchOp#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortCatchOp#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCatchOp()
 * @model
 * @generated
 */
public interface PortCatchOp extends CheckPortOpsPresent
{
  /**
   * Returns the value of the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param</em>' containment reference.
   * @see #setParam(CatchOpParameter)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCatchOp_Param()
   * @model containment="true"
   * @generated
   */
  CatchOpParameter getParam();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortCatchOp#getParam <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param</em>' containment reference.
   * @see #getParam()
   * @generated
   */
  void setParam(CatchOpParameter value);

  /**
   * Returns the value of the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Redirect</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Redirect</em>' containment reference.
   * @see #setRedirect(PortRedirect)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCatchOp_Redirect()
   * @model containment="true"
   * @generated
   */
  PortRedirect getRedirect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortCatchOp#getRedirect <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Redirect</em>' containment reference.
   * @see #getRedirect()
   * @generated
   */
  void setRedirect(PortRedirect value);

} // PortCatchOp
