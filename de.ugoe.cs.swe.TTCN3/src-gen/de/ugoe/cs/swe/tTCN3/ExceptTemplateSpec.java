/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Except Template Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec#getIdOrAll <em>Id Or All</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptTemplateSpec()
 * @model
 * @generated
 */
public interface ExceptTemplateSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id Or All</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id Or All</em>' containment reference.
   * @see #setIdOrAll(IdentifierListOrAll)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptTemplateSpec_IdOrAll()
   * @model containment="true"
   * @generated
   */
  IdentifierListOrAll getIdOrAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec#getIdOrAll <em>Id Or All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id Or All</em>' containment reference.
   * @see #getIdOrAll()
   * @generated
   */
  void setIdOrAll(IdentifierListOrAll value);

} // ExceptTemplateSpec
