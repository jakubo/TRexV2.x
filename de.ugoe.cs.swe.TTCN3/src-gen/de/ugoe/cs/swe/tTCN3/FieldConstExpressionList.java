/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Const Expression List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionList#getSpecs <em>Specs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionList()
 * @model
 * @generated
 */
public interface FieldConstExpressionList extends CompoundConstExpression
{
  /**
   * Returns the value of the '<em><b>Specs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionList_Specs()
   * @model containment="true"
   * @generated
   */
  EList<FieldConstExpressionSpec> getSpecs();

} // FieldConstExpressionList
