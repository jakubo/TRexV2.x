/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Identifier With Except</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getId <em>Id</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getDef <em>Def</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getQualifiedIdentifierWithExcept()
 * @model
 * @generated
 */
public interface QualifiedIdentifierWithExcept extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getQualifiedIdentifierWithExcept_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Def</em>' containment reference.
   * @see #setDef(ExceptsDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getQualifiedIdentifierWithExcept_Def()
   * @model containment="true"
   * @generated
   */
  ExceptsDef getDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getDef <em>Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Def</em>' containment reference.
   * @see #getDef()
   * @generated
   */
  void setDef(ExceptsDef value);

} // QualifiedIdentifierWithExcept
