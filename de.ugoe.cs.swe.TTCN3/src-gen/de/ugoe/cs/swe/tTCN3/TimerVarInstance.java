/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Var Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerVarInstance()
 * @model
 * @generated
 */
public interface TimerVarInstance extends RefValue
{
} // TimerVarInstance
