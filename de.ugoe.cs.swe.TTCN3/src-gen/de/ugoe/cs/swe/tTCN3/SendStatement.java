/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SendStatement#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SendStatement#getArryRefs <em>Arry Refs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SendStatement#getSend <em>Send</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSendStatement()
 * @model
 * @generated
 */
public interface SendStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' reference.
   * @see #setPort(FormalPortAndValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSendStatement_Port()
   * @model
   * @generated
   */
  FormalPortAndValuePar getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SendStatement#getPort <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' reference.
   * @see #getPort()
   * @generated
   */
  void setPort(FormalPortAndValuePar value);

  /**
   * Returns the value of the '<em><b>Arry Refs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ArrayOrBitRef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arry Refs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arry Refs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSendStatement_ArryRefs()
   * @model containment="true"
   * @generated
   */
  EList<ArrayOrBitRef> getArryRefs();

  /**
   * Returns the value of the '<em><b>Send</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Send</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Send</em>' containment reference.
   * @see #setSend(PortSendOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSendStatement_Send()
   * @model containment="true"
   * @generated
   */
  PortSendOp getSend();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SendStatement#getSend <em>Send</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Send</em>' containment reference.
   * @see #getSend()
   * @generated
   */
  void setSend(PortSendOp value);

} // SendStatement
