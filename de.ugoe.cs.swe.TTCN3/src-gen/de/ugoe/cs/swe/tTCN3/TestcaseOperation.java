/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Testcase Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseOperation#getTxt <em>Txt</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseOperation#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseOperation()
 * @model
 * @generated
 */
public interface TestcaseOperation extends EObject
{
  /**
   * Returns the value of the '<em><b>Txt</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Txt</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Txt</em>' attribute list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseOperation_Txt()
   * @model unique="false"
   * @generated
   */
  EList<String> getTxt();

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.InLineTemplate}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseOperation_Template()
   * @model containment="true"
   * @generated
   */
  EList<InLineTemplate> getTemplate();

} // TestcaseOperation
