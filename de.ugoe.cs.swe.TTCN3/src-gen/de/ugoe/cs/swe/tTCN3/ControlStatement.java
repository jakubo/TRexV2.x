/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getBasic <em>Basic</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getSut <em>Sut</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatement()
 * @model
 * @generated
 */
public interface ControlStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(TimerStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatement_Timer()
   * @model containment="true"
   * @generated
   */
  TimerStatements getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerStatements value);

  /**
   * Returns the value of the '<em><b>Basic</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Basic</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Basic</em>' containment reference.
   * @see #setBasic(BasicStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatement_Basic()
   * @model containment="true"
   * @generated
   */
  BasicStatements getBasic();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getBasic <em>Basic</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Basic</em>' containment reference.
   * @see #getBasic()
   * @generated
   */
  void setBasic(BasicStatements value);

  /**
   * Returns the value of the '<em><b>Behavior</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Behavior</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Behavior</em>' containment reference.
   * @see #setBehavior(BehaviourStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatement_Behavior()
   * @model containment="true"
   * @generated
   */
  BehaviourStatements getBehavior();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getBehavior <em>Behavior</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Behavior</em>' containment reference.
   * @see #getBehavior()
   * @generated
   */
  void setBehavior(BehaviourStatements value);

  /**
   * Returns the value of the '<em><b>Sut</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sut</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sut</em>' containment reference.
   * @see #setSut(SUTStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatement_Sut()
   * @model containment="true"
   * @generated
   */
  SUTStatements getSut();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getSut <em>Sut</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sut</em>' containment reference.
   * @see #getSut()
   * @generated
   */
  void setSut(SUTStatements value);

} // ControlStatement
