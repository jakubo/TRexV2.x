/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Factory
 * @model kind="package"
 * @generated
 */
public interface TTCN3Package extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "tTCN3";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.ugoe.de/cs/swe/TTCN3";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "tTCN3";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  TTCN3Package eINSTANCE = de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl.init();

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3FileImpl <em>File</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3FileImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTTCN3File()
   * @generated
   */
  int TTCN3_FILE = 0;

  /**
   * The feature id for the '<em><b>Modules</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_FILE__MODULES = 0;

  /**
   * The number of structural features of the '<em>File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_FILE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConstDefImpl <em>Const Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConstDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConstDef()
   * @generated
   */
  int CONST_DEF = 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_DEF__TYPE = 0;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_DEF__DEFS = 1;

  /**
   * The number of structural features of the '<em>Const Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getType()
   * @generated
   */
  int TYPE = 2;

  /**
   * The feature id for the '<em><b>Pre</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__PRE = 0;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__REF = 1;

  /**
   * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__EXTENSIONS = 2;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeReferenceTailTypeImpl <em>Type Reference Tail Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeReferenceTailTypeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTypeReferenceTailType()
   * @generated
   */
  int TYPE_REFERENCE_TAIL_TYPE = 3;

  /**
   * The number of structural features of the '<em>Type Reference Tail Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_TAIL_TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SpecTypeElementImpl <em>Spec Type Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SpecTypeElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSpecTypeElement()
   * @generated
   */
  int SPEC_TYPE_ELEMENT = 6;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPEC_TYPE_ELEMENT__TAIL = 0;

  /**
   * The number of structural features of the '<em>Spec Type Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPEC_TYPE_ELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeReferenceTailImpl <em>Type Reference Tail</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeReferenceTailImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTypeReferenceTail()
   * @generated
   */
  int TYPE_REFERENCE_TAIL = 4;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_TAIL__TAIL = SPEC_TYPE_ELEMENT__TAIL;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_TAIL__TYPE = SPEC_TYPE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_TAIL__ARRAY = SPEC_TYPE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Type Reference Tail</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_TAIL_FEATURE_COUNT = SPEC_TYPE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeReferenceImpl <em>Type Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeReferenceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTypeReference()
   * @generated
   */
  int TYPE_REFERENCE = 5;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE__TAIL = SPEC_TYPE_ELEMENT__TAIL;

  /**
   * The feature id for the '<em><b>Head</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE__HEAD = SPEC_TYPE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_FEATURE_COUNT = SPEC_TYPE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayDefImpl <em>Array Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayDef()
   * @generated
   */
  int ARRAY_DEF = 7;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DEF__EXPR = 0;

  /**
   * The feature id for the '<em><b>Range</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DEF__RANGE = 1;

  /**
   * The number of structural features of the '<em>Array Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReferencedTypeImpl <em>Referenced Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReferencedTypeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReferencedType()
   * @generated
   */
  int REFERENCED_TYPE = 200;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_TYPE__NAME = TYPE_REFERENCE_TAIL_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Referenced Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_TYPE_FEATURE_COUNT = TYPE_REFERENCE_TAIL_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleOrGroupImpl <em>Module Or Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleOrGroupImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleOrGroup()
   * @generated
   */
  int MODULE_OR_GROUP = 199;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_OR_GROUP__NAME = REFERENCED_TYPE__NAME;

  /**
   * The number of structural features of the '<em>Module Or Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_OR_GROUP_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl <em>Module</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTTCN3Module()
   * @generated
   */
  int TTCN3_MODULE = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE__NAME = MODULE_OR_GROUP__NAME;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE__SPEC = MODULE_OR_GROUP_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE__DEFS = MODULE_OR_GROUP_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Controls</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE__CONTROLS = MODULE_OR_GROUP_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Withstm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE__WITHSTM = MODULE_OR_GROUP_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE__SC = MODULE_OR_GROUP_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Module</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_MODULE_FEATURE_COUNT = MODULE_OR_GROUP_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.LanguageSpecImpl <em>Language Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.LanguageSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getLanguageSpec()
   * @generated
   */
  int LANGUAGE_SPEC = 9;

  /**
   * The feature id for the '<em><b>Txt</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANGUAGE_SPEC__TXT = 0;

  /**
   * The number of structural features of the '<em>Language Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANGUAGE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleParDefImpl <em>Module Par Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleParDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleParDef()
   * @generated
   */
  int MODULE_PAR_DEF = 10;

  /**
   * The feature id for the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR_DEF__PARAM = 0;

  /**
   * The feature id for the '<em><b>Multitype Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR_DEF__MULTITYPE_PARAM = 1;

  /**
   * The number of structural features of the '<em>Module Par Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleParImpl <em>Module Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModulePar()
   * @generated
   */
  int MODULE_PAR = 11;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR__TYPE = 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR__LIST = 1;

  /**
   * The number of structural features of the '<em>Module Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleParListImpl <em>Module Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleParList()
   * @generated
   */
  int MODULE_PAR_LIST = 12;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR_LIST__PARAMS = 0;

  /**
   * The number of structural features of the '<em>Module Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RefValueHeadImpl <em>Ref Value Head</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RefValueHeadImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRefValueHead()
   * @generated
   */
  int REF_VALUE_HEAD = 329;

  /**
   * The number of structural features of the '<em>Ref Value Head</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_HEAD_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RefValueImpl <em>Ref Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RefValueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRefValue()
   * @generated
   */
  int REF_VALUE = 335;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE__NAME = REF_VALUE_HEAD_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ref Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_FEATURE_COUNT = REF_VALUE_HEAD_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleParameterImpl <em>Module Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleParameterImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleParameter()
   * @generated
   */
  int MODULE_PARAMETER = 13;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PARAMETER__NAME = REF_VALUE__NAME;

  /**
   * The feature id for the '<em><b>Const Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PARAMETER__CONST_EXPR = REF_VALUE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Module Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_PARAMETER_FEATURE_COUNT = REF_VALUE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MultitypedModuleParListImpl <em>Multityped Module Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MultitypedModuleParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMultitypedModuleParList()
   * @generated
   */
  int MULTITYPED_MODULE_PAR_LIST = 14;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTITYPED_MODULE_PAR_LIST__PARAMS = 0;

  /**
   * The number of structural features of the '<em>Multityped Module Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTITYPED_MODULE_PAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionsListImpl <em>Module Definitions List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionsListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleDefinitionsList()
   * @generated
   */
  int MODULE_DEFINITIONS_LIST = 15;

  /**
   * The feature id for the '<em><b>Definitions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITIONS_LIST__DEFINITIONS = 0;

  /**
   * The number of structural features of the '<em>Module Definitions List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITIONS_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl <em>Module Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleDefinitionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleDefinition()
   * @generated
   */
  int MODULE_DEFINITION = 16;

  /**
   * The feature id for the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITION__VISIBILITY = 0;

  /**
   * The feature id for the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITION__DEF = 1;

  /**
   * The feature id for the '<em><b>Public Group</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITION__PUBLIC_GROUP = 2;

  /**
   * The feature id for the '<em><b>Private Friend</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITION__PRIVATE_FRIEND = 3;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITION__STATEMENT = 4;

  /**
   * The number of structural features of the '<em>Module Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_DEFINITION_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FriendModuleDefImpl <em>Friend Module Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FriendModuleDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFriendModuleDef()
   * @generated
   */
  int FRIEND_MODULE_DEF = 17;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FRIEND_MODULE_DEF__ID = 0;

  /**
   * The number of structural features of the '<em>Friend Module Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FRIEND_MODULE_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GroupDefImpl <em>Group Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GroupDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGroupDef()
   * @generated
   */
  int GROUP_DEF = 18;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_DEF__NAME = MODULE_OR_GROUP__NAME;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_DEF__LIST = MODULE_OR_GROUP_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Group Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_DEF_FEATURE_COUNT = MODULE_OR_GROUP_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionRefImpl <em>Function Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionRef()
   * @generated
   */
  int FUNCTION_REF = 130;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_REF__NAME = REF_VALUE__NAME;

  /**
   * The number of structural features of the '<em>Function Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_REF_FEATURE_COUNT = REF_VALUE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExtFunctionDefImpl <em>Ext Function Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExtFunctionDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExtFunctionDef()
   * @generated
   */
  int EXT_FUNCTION_DEF = 19;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_FUNCTION_DEF__NAME = FUNCTION_REF__NAME;

  /**
   * The feature id for the '<em><b>Det</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_FUNCTION_DEF__DET = FUNCTION_REF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_FUNCTION_DEF__LIST = FUNCTION_REF_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_FUNCTION_DEF__RETURN = FUNCTION_REF_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Ext Function Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_FUNCTION_DEF_FEATURE_COUNT = FUNCTION_REF_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExtConstDefImpl <em>Ext Const Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExtConstDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExtConstDef()
   * @generated
   */
  int EXT_CONST_DEF = 20;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_CONST_DEF__TYPE = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_CONST_DEF__ID = 1;

  /**
   * The number of structural features of the '<em>Ext Const Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXT_CONST_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.IdentifierObjectListImpl <em>Identifier Object List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.IdentifierObjectListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getIdentifierObjectList()
   * @generated
   */
  int IDENTIFIER_OBJECT_LIST = 21;

  /**
   * The feature id for the '<em><b>Ids</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_OBJECT_LIST__IDS = 0;

  /**
   * The number of structural features of the '<em>Identifier Object List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_OBJECT_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NamedObjectImpl <em>Named Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NamedObjectImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNamedObject()
   * @generated
   */
  int NAMED_OBJECT = 22;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_OBJECT__NAME = REF_VALUE__NAME;

  /**
   * The number of structural features of the '<em>Named Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_OBJECT_FEATURE_COUNT = REF_VALUE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportDefImpl <em>Import Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportDef()
   * @generated
   */
  int IMPORT_DEF = 23;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DEF__NAME = 0;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DEF__SPEC = 1;

  /**
   * The feature id for the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DEF__ALL = 2;

  /**
   * The feature id for the '<em><b>Import Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DEF__IMPORT_SPEC = 3;

  /**
   * The number of structural features of the '<em>Import Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DEF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllWithExceptsImpl <em>All With Excepts</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllWithExceptsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllWithExcepts()
   * @generated
   */
  int ALL_WITH_EXCEPTS = 24;

  /**
   * The feature id for the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_WITH_EXCEPTS__DEF = 0;

  /**
   * The number of structural features of the '<em>All With Excepts</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_WITH_EXCEPTS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptsDefImpl <em>Excepts Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptsDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptsDef()
   * @generated
   */
  int EXCEPTS_DEF = 25;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPTS_DEF__SPEC = 0;

  /**
   * The number of structural features of the '<em>Excepts Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPTS_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptSpecImpl <em>Except Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptSpec()
   * @generated
   */
  int EXCEPT_SPEC = 26;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_SPEC__ELEMENT = 0;

  /**
   * The number of structural features of the '<em>Except Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl <em>Except Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptElement()
   * @generated
   */
  int EXCEPT_ELEMENT = 27;

  /**
   * The feature id for the '<em><b>Group</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__GROUP = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__TYPE = 1;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__TEMPLATE = 2;

  /**
   * The feature id for the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__CONST = 3;

  /**
   * The feature id for the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__TESTCASE = 4;

  /**
   * The feature id for the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__ALTSTEP = 5;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__FUNCTION = 6;

  /**
   * The feature id for the '<em><b>Signature</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__SIGNATURE = 7;

  /**
   * The feature id for the '<em><b>Module Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT__MODULE_PAR = 8;

  /**
   * The number of structural features of the '<em>Except Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ELEMENT_FEATURE_COUNT = 9;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.IdentifierListOrAllImpl <em>Identifier List Or All</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.IdentifierListOrAllImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getIdentifierListOrAll()
   * @generated
   */
  int IDENTIFIER_LIST_OR_ALL = 28;

  /**
   * The feature id for the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_OR_ALL__ID_LIST = 0;

  /**
   * The feature id for the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_OR_ALL__ALL = 1;

  /**
   * The number of structural features of the '<em>Identifier List Or All</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_OR_ALL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptGroupSpecImpl <em>Except Group Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptGroupSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptGroupSpec()
   * @generated
   */
  int EXCEPT_GROUP_SPEC = 29;

  /**
   * The feature id for the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_GROUP_SPEC__ID_LIST = 0;

  /**
   * The feature id for the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_GROUP_SPEC__ALL = 1;

  /**
   * The number of structural features of the '<em>Except Group Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_GROUP_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptTypeDefSpecImpl <em>Except Type Def Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptTypeDefSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptTypeDefSpec()
   * @generated
   */
  int EXCEPT_TYPE_DEF_SPEC = 30;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_TYPE_DEF_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Type Def Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_TYPE_DEF_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptTemplateSpecImpl <em>Except Template Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptTemplateSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptTemplateSpec()
   * @generated
   */
  int EXCEPT_TEMPLATE_SPEC = 31;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_TEMPLATE_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Template Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_TEMPLATE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptConstSpecImpl <em>Except Const Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptConstSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptConstSpec()
   * @generated
   */
  int EXCEPT_CONST_SPEC = 32;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_CONST_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Const Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_CONST_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptTestcaseSpecImpl <em>Except Testcase Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptTestcaseSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptTestcaseSpec()
   * @generated
   */
  int EXCEPT_TESTCASE_SPEC = 33;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_TESTCASE_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Testcase Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_TESTCASE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptAltstepSpecImpl <em>Except Altstep Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptAltstepSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptAltstepSpec()
   * @generated
   */
  int EXCEPT_ALTSTEP_SPEC = 34;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ALTSTEP_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Altstep Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_ALTSTEP_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptFunctionSpecImpl <em>Except Function Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptFunctionSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptFunctionSpec()
   * @generated
   */
  int EXCEPT_FUNCTION_SPEC = 35;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_FUNCTION_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Function Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_FUNCTION_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptSignatureSpecImpl <em>Except Signature Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptSignatureSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptSignatureSpec()
   * @generated
   */
  int EXCEPT_SIGNATURE_SPEC = 36;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_SIGNATURE_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Signature Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_SIGNATURE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptModuleParSpecImpl <em>Except Module Par Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptModuleParSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptModuleParSpec()
   * @generated
   */
  int EXCEPT_MODULE_PAR_SPEC = 37;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_MODULE_PAR_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Except Module Par Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPT_MODULE_PAR_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportSpecImpl <em>Import Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportSpec()
   * @generated
   */
  int IMPORT_SPEC = 38;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_SPEC__ELEMENT = 0;

  /**
   * The number of structural features of the '<em>Import Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl <em>Import Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportElement()
   * @generated
   */
  int IMPORT_ELEMENT = 39;

  /**
   * The feature id for the '<em><b>Group</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__GROUP = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__TYPE = 1;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__TEMPLATE = 2;

  /**
   * The feature id for the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__CONST = 3;

  /**
   * The feature id for the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__TESTCASE = 4;

  /**
   * The feature id for the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__ALTSTEP = 5;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__FUNCTION = 6;

  /**
   * The feature id for the '<em><b>Signature</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__SIGNATURE = 7;

  /**
   * The feature id for the '<em><b>Module Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__MODULE_PAR = 8;

  /**
   * The feature id for the '<em><b>Import Spec</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT__IMPORT_SPEC = 9;

  /**
   * The number of structural features of the '<em>Import Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ELEMENT_FEATURE_COUNT = 10;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportGroupSpecImpl <em>Import Group Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportGroupSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportGroupSpec()
   * @generated
   */
  int IMPORT_GROUP_SPEC = 40;

  /**
   * The feature id for the '<em><b>Group Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_GROUP_SPEC__GROUP_REF = 0;

  /**
   * The feature id for the '<em><b>Group</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_GROUP_SPEC__GROUP = 1;

  /**
   * The number of structural features of the '<em>Import Group Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_GROUP_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.IdentifierListOrAllWithExceptImpl <em>Identifier List Or All With Except</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.IdentifierListOrAllWithExceptImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getIdentifierListOrAllWithExcept()
   * @generated
   */
  int IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT = 41;

  /**
   * The feature id for the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST = 0;

  /**
   * The feature id for the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL = 1;

  /**
   * The number of structural features of the '<em>Identifier List Or All With Except</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllWithExceptImpl <em>All With Except</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllWithExceptImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllWithExcept()
   * @generated
   */
  int ALL_WITH_EXCEPT = 42;

  /**
   * The feature id for the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_WITH_EXCEPT__ID_LIST = 0;

  /**
   * The number of structural features of the '<em>All With Except</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_WITH_EXCEPT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportTypeDefSpecImpl <em>Import Type Def Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportTypeDefSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportTypeDefSpec()
   * @generated
   */
  int IMPORT_TYPE_DEF_SPEC = 43;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_TYPE_DEF_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Type Def Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_TYPE_DEF_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportTemplateSpecImpl <em>Import Template Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportTemplateSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportTemplateSpec()
   * @generated
   */
  int IMPORT_TEMPLATE_SPEC = 44;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_TEMPLATE_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Template Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_TEMPLATE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportConstSpecImpl <em>Import Const Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportConstSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportConstSpec()
   * @generated
   */
  int IMPORT_CONST_SPEC = 45;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_CONST_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Const Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_CONST_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportAltstepSpecImpl <em>Import Altstep Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportAltstepSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportAltstepSpec()
   * @generated
   */
  int IMPORT_ALTSTEP_SPEC = 46;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ALTSTEP_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Altstep Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_ALTSTEP_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportTestcaseSpecImpl <em>Import Testcase Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportTestcaseSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportTestcaseSpec()
   * @generated
   */
  int IMPORT_TESTCASE_SPEC = 47;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_TESTCASE_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Testcase Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_TESTCASE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportFunctionSpecImpl <em>Import Function Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportFunctionSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportFunctionSpec()
   * @generated
   */
  int IMPORT_FUNCTION_SPEC = 48;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_FUNCTION_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Function Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_FUNCTION_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportSignatureSpecImpl <em>Import Signature Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportSignatureSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportSignatureSpec()
   * @generated
   */
  int IMPORT_SIGNATURE_SPEC = 49;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_SIGNATURE_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Signature Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_SIGNATURE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ImportModuleParSpecImpl <em>Import Module Par Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ImportModuleParSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getImportModuleParSpec()
   * @generated
   */
  int IMPORT_MODULE_PAR_SPEC = 50;

  /**
   * The feature id for the '<em><b>Id Or All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_MODULE_PAR_SPEC__ID_OR_ALL = 0;

  /**
   * The number of structural features of the '<em>Import Module Par Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_MODULE_PAR_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GroupRefListWithExceptImpl <em>Group Ref List With Except</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GroupRefListWithExceptImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGroupRefListWithExcept()
   * @generated
   */
  int GROUP_REF_LIST_WITH_EXCEPT = 51;

  /**
   * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_REF_LIST_WITH_EXCEPT__QUALIFIER = 0;

  /**
   * The number of structural features of the '<em>Group Ref List With Except</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_REF_LIST_WITH_EXCEPT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.QualifiedIdentifierWithExceptImpl <em>Qualified Identifier With Except</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.QualifiedIdentifierWithExceptImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getQualifiedIdentifierWithExcept()
   * @generated
   */
  int QUALIFIED_IDENTIFIER_WITH_EXCEPT = 52;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_IDENTIFIER_WITH_EXCEPT__ID = 0;

  /**
   * The feature id for the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_IDENTIFIER_WITH_EXCEPT__DEF = 1;

  /**
   * The number of structural features of the '<em>Qualified Identifier With Except</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_IDENTIFIER_WITH_EXCEPT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllGroupsWithExceptImpl <em>All Groups With Except</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllGroupsWithExceptImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllGroupsWithExcept()
   * @generated
   */
  int ALL_GROUPS_WITH_EXCEPT = 53;

  /**
   * The feature id for the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_GROUPS_WITH_EXCEPT__ID_LIST = 0;

  /**
   * The number of structural features of the '<em>All Groups With Except</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_GROUPS_WITH_EXCEPT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl <em>Altstep Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltstepDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltstepDef()
   * @generated
   */
  int ALTSTEP_DEF = 54;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__NAME = FUNCTION_REF__NAME;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__PARAMS = FUNCTION_REF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__SPEC = FUNCTION_REF_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Mtc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__MTC = FUNCTION_REF_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__SYSTEM = FUNCTION_REF_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Local</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__LOCAL = FUNCTION_REF_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF__GUARD = FUNCTION_REF_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>Altstep Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_DEF_FEATURE_COUNT = FUNCTION_REF_FEATURE_COUNT + 6;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefListImpl <em>Altstep Local Def List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltstepLocalDefList()
   * @generated
   */
  int ALTSTEP_LOCAL_DEF_LIST = 55;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF_LIST__DEFS = 0;

  /**
   * The feature id for the '<em><b>Withstats</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF_LIST__WITHSTATS = 1;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF_LIST__SC = 2;

  /**
   * The number of structural features of the '<em>Altstep Local Def List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF_LIST_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefImpl <em>Altstep Local Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltstepLocalDef()
   * @generated
   */
  int ALTSTEP_LOCAL_DEF = 56;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF__VARIABLE = 0;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF__TIMER = 1;

  /**
   * The feature id for the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF__CONST = 2;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF__TEMPLATE = 3;

  /**
   * The number of structural features of the '<em>Altstep Local Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_LOCAL_DEF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ReferenceImpl <em>Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3ReferenceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTTCN3Reference()
   * @generated
   */
  int TTCN3_REFERENCE = 343;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_REFERENCE__NAME = 0;

  /**
   * The number of structural features of the '<em>Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_REFERENCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseDefImpl <em>Testcase Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TestcaseDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTestcaseDef()
   * @generated
   */
  int TESTCASE_DEF = 57;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_DEF__NAME = TTCN3_REFERENCE__NAME;

  /**
   * The feature id for the '<em><b>Par List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_DEF__PAR_LIST = TTCN3_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_DEF__SPEC = TTCN3_REFERENCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_DEF__STATEMENT = TTCN3_REFERENCE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Testcase Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_DEF_FEATURE_COUNT = TTCN3_REFERENCE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateOrValueFormalParListImpl <em>Template Or Value Formal Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateOrValueFormalParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateOrValueFormalParList()
   * @generated
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR_LIST = 58;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR_LIST__PARAMS = 0;

  /**
   * The number of structural features of the '<em>Template Or Value Formal Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateOrValueFormalParImpl <em>Template Or Value Formal Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateOrValueFormalParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateOrValueFormalPar()
   * @generated
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR = 59;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR__VALUE = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR__TEMPLATE = 1;

  /**
   * The number of structural features of the '<em>Template Or Value Formal Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_VALUE_FORMAL_PAR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConfigSpecImpl <em>Config Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConfigSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConfigSpec()
   * @generated
   */
  int CONFIG_SPEC = 60;

  /**
   * The feature id for the '<em><b>Runs On</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIG_SPEC__RUNS_ON = 0;

  /**
   * The feature id for the '<em><b>System Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIG_SPEC__SYSTEM_SPEC = 1;

  /**
   * The number of structural features of the '<em>Config Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIG_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SystemSpecImpl <em>System Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SystemSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSystemSpec()
   * @generated
   */
  int SYSTEM_SPEC = 61;

  /**
   * The feature id for the '<em><b>Component</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYSTEM_SPEC__COMPONENT = 0;

  /**
   * The number of structural features of the '<em>System Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYSTEM_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SignatureDefImpl <em>Signature Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SignatureDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSignatureDef()
   * @generated
   */
  int SIGNATURE_DEF = 62;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_DEF__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Param List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_DEF__PARAM_LIST = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_DEF__RETURN = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Sprec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_DEF__SPREC = REFERENCED_TYPE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Signature Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_DEF_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExceptionSpecImpl <em>Exception Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExceptionSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExceptionSpec()
   * @generated
   */
  int EXCEPTION_SPEC = 63;

  /**
   * The number of structural features of the '<em>Exception Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPTION_SPEC_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SignatureFormalParListImpl <em>Signature Formal Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SignatureFormalParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSignatureFormalParList()
   * @generated
   */
  int SIGNATURE_FORMAL_PAR_LIST = 64;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_FORMAL_PAR_LIST__PARAMS = 0;

  /**
   * The number of structural features of the '<em>Signature Formal Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_FORMAL_PAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleControlPartImpl <em>Module Control Part</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleControlPartImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleControlPart()
   * @generated
   */
  int MODULE_CONTROL_PART = 65;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_CONTROL_PART__BODY = 0;

  /**
   * The feature id for the '<em><b>Ws</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_CONTROL_PART__WS = 1;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_CONTROL_PART__SC = 2;

  /**
   * The number of structural features of the '<em>Module Control Part</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_CONTROL_PART_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ModuleControlBodyImpl <em>Module Control Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ModuleControlBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getModuleControlBody()
   * @generated
   */
  int MODULE_CONTROL_BODY = 66;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_CONTROL_BODY__LIST = 0;

  /**
   * The number of structural features of the '<em>Module Control Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_CONTROL_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl <em>Control Statement Or Def List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getControlStatementOrDefList()
   * @generated
   */
  int CONTROL_STATEMENT_OR_DEF_LIST = 67;

  /**
   * The feature id for the '<em><b>Local Def</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_DEF = 0;

  /**
   * The feature id for the '<em><b>Local Inst</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_LIST__LOCAL_INST = 1;

  /**
   * The feature id for the '<em><b>Withstm</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_LIST__WITHSTM = 2;

  /**
   * The feature id for the '<em><b>Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_LIST__CONTROL = 3;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_LIST__SC = 4;

  /**
   * The number of structural features of the '<em>Control Statement Or Def List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_LIST_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefImpl <em>Control Statement Or Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getControlStatementOrDef()
   * @generated
   */
  int CONTROL_STATEMENT_OR_DEF = 68;

  /**
   * The feature id for the '<em><b>Local Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF__LOCAL_DEF = 0;

  /**
   * The feature id for the '<em><b>Local Inst</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF__LOCAL_INST = 1;

  /**
   * The feature id for the '<em><b>Withstm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF__WITHSTM = 2;

  /**
   * The feature id for the '<em><b>Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF__CONTROL = 3;

  /**
   * The number of structural features of the '<em>Control Statement Or Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_OR_DEF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementImpl <em>Control Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ControlStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getControlStatement()
   * @generated
   */
  int CONTROL_STATEMENT = 69;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT__TIMER = 0;

  /**
   * The feature id for the '<em><b>Basic</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT__BASIC = 1;

  /**
   * The feature id for the '<em><b>Behavior</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT__BEHAVIOR = 2;

  /**
   * The feature id for the '<em><b>Sut</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT__SUT = 3;

  /**
   * The number of structural features of the '<em>Control Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROL_STATEMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SUTStatementsImpl <em>SUT Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SUTStatementsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSUTStatements()
   * @generated
   */
  int SUT_STATEMENTS = 70;

  /**
   * The feature id for the '<em><b>Txt</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUT_STATEMENTS__TXT = 0;

  /**
   * The number of structural features of the '<em>SUT Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUT_STATEMENTS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ActionTextImpl <em>Action Text</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ActionTextImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getActionText()
   * @generated
   */
  int ACTION_TEXT = 71;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_TEXT__EXPR = 0;

  /**
   * The number of structural features of the '<em>Action Text</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_TEXT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl <em>Behaviour Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBehaviourStatements()
   * @generated
   */
  int BEHAVIOUR_STATEMENTS = 72;

  /**
   * The feature id for the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__TESTCASE = 0;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__FUNCTION = 1;

  /**
   * The feature id for the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__RETURN = 2;

  /**
   * The feature id for the '<em><b>Alt</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__ALT = 3;

  /**
   * The feature id for the '<em><b>Interleaved</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__INTERLEAVED = 4;

  /**
   * The feature id for the '<em><b>Label</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__LABEL = 5;

  /**
   * The feature id for the '<em><b>Goto</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__GOTO = 6;

  /**
   * The feature id for the '<em><b>Deactivate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__DEACTIVATE = 7;

  /**
   * The feature id for the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__ALTSTEP = 8;

  /**
   * The feature id for the '<em><b>Activate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS__ACTIVATE = 9;

  /**
   * The number of structural features of the '<em>Behaviour Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOUR_STATEMENTS_FEATURE_COUNT = 10;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ActivateOpImpl <em>Activate Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ActivateOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getActivateOp()
   * @generated
   */
  int ACTIVATE_OP = 73;

  /**
   * The feature id for the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVATE_OP__ALTSTEP = 0;

  /**
   * The number of structural features of the '<em>Activate Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVATE_OP_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DeactivateStatementImpl <em>Deactivate Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DeactivateStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDeactivateStatement()
   * @generated
   */
  int DEACTIVATE_STATEMENT = 74;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEACTIVATE_STATEMENT__COMPONENT = 0;

  /**
   * The number of structural features of the '<em>Deactivate Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEACTIVATE_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.LabelStatementImpl <em>Label Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.LabelStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getLabelStatement()
   * @generated
   */
  int LABEL_STATEMENT = 75;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_STATEMENT__NAME = TTCN3_REFERENCE__NAME;

  /**
   * The number of structural features of the '<em>Label Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_STATEMENT_FEATURE_COUNT = TTCN3_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GotoStatementImpl <em>Goto Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GotoStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGotoStatement()
   * @generated
   */
  int GOTO_STATEMENT = 76;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GOTO_STATEMENT__NAME = TTCN3_REFERENCE__NAME;

  /**
   * The number of structural features of the '<em>Goto Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GOTO_STATEMENT_FEATURE_COUNT = TTCN3_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReturnStatementImpl <em>Return Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReturnStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReturnStatement()
   * @generated
   */
  int RETURN_STATEMENT = 77;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_STATEMENT__EXPRESSION = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_STATEMENT__TEMPLATE = 1;

  /**
   * The number of structural features of the '<em>Return Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.InterleavedConstructImpl <em>Interleaved Construct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.InterleavedConstructImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getInterleavedConstruct()
   * @generated
   */
  int INTERLEAVED_CONSTRUCT = 78;

  /**
   * The number of structural features of the '<em>Interleaved Construct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_CONSTRUCT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardListImpl <em>Interleaved Guard List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getInterleavedGuardList()
   * @generated
   */
  int INTERLEAVED_GUARD_LIST = 79;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD_LIST__ELEMENTS = INTERLEAVED_CONSTRUCT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Interleaved Guard List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD_LIST_FEATURE_COUNT = INTERLEAVED_CONSTRUCT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardElementImpl <em>Interleaved Guard Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getInterleavedGuardElement()
   * @generated
   */
  int INTERLEAVED_GUARD_ELEMENT = 80;

  /**
   * The feature id for the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD_ELEMENT__GUARD = 0;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD_ELEMENT__STATEMENT = 1;

  /**
   * The number of structural features of the '<em>Interleaved Guard Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardImpl <em>Interleaved Guard</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getInterleavedGuard()
   * @generated
   */
  int INTERLEAVED_GUARD = 81;

  /**
   * The feature id for the '<em><b>Guard Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD__GUARD_OP = 0;

  /**
   * The number of structural features of the '<em>Interleaved Guard</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVED_GUARD_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltConstructImpl <em>Alt Construct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltConstructImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltConstruct()
   * @generated
   */
  int ALT_CONSTRUCT = 82;

  /**
   * The feature id for the '<em><b>Ag List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_CONSTRUCT__AG_LIST = 0;

  /**
   * The number of structural features of the '<em>Alt Construct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_CONSTRUCT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltGuardListImpl <em>Alt Guard List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltGuardListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltGuardList()
   * @generated
   */
  int ALT_GUARD_LIST = 83;

  /**
   * The feature id for the '<em><b>Guard List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_GUARD_LIST__GUARD_LIST = 0;

  /**
   * The feature id for the '<em><b>Else List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_GUARD_LIST__ELSE_LIST = 1;

  /**
   * The number of structural features of the '<em>Alt Guard List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_GUARD_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ElseStatementImpl <em>Else Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ElseStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getElseStatement()
   * @generated
   */
  int ELSE_STATEMENT = 84;

  /**
   * The feature id for the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_STATEMENT__BLOCK = 0;

  /**
   * The number of structural features of the '<em>Else Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GuardStatementImpl <em>Guard Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GuardStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGuardStatement()
   * @generated
   */
  int GUARD_STATEMENT = 85;

  /**
   * The feature id for the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_STATEMENT__GUARD = 0;

  /**
   * The feature id for the '<em><b>Step</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_STATEMENT__STEP = 1;

  /**
   * The feature id for the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_STATEMENT__BLOCK = 2;

  /**
   * The feature id for the '<em><b>Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_STATEMENT__OP = 3;

  /**
   * The number of structural features of the '<em>Guard Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_STATEMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl <em>Guard Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGuardOp()
   * @generated
   */
  int GUARD_OP = 86;

  /**
   * The feature id for the '<em><b>Timeout</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__TIMEOUT = 0;

  /**
   * The feature id for the '<em><b>Receive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__RECEIVE = 1;

  /**
   * The feature id for the '<em><b>Trigger</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__TRIGGER = 2;

  /**
   * The feature id for the '<em><b>Get Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__GET_CALL = 3;

  /**
   * The feature id for the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__CATCH = 4;

  /**
   * The feature id for the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__CHECK = 5;

  /**
   * The feature id for the '<em><b>Get Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__GET_REPLY = 6;

  /**
   * The feature id for the '<em><b>Done</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__DONE = 7;

  /**
   * The feature id for the '<em><b>Killed</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP__KILLED = 8;

  /**
   * The number of structural features of the '<em>Guard Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_OP_FEATURE_COUNT = 9;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DoneStatementImpl <em>Done Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DoneStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDoneStatement()
   * @generated
   */
  int DONE_STATEMENT = 87;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DONE_STATEMENT__COMPONENT = 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DONE_STATEMENT__INDEX = 1;

  /**
   * The number of structural features of the '<em>Done Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DONE_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.KilledStatementImpl <em>Killed Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.KilledStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getKilledStatement()
   * @generated
   */
  int KILLED_STATEMENT = 88;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int KILLED_STATEMENT__COMPONENT = 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int KILLED_STATEMENT__INDEX = 1;

  /**
   * The number of structural features of the '<em>Killed Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int KILLED_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentOrAnyImpl <em>Component Or Any</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentOrAnyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentOrAny()
   * @generated
   */
  int COMPONENT_OR_ANY = 89;

  /**
   * The feature id for the '<em><b>Comp Or Default</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_OR_ANY__COMP_OR_DEFAULT = 0;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_OR_ANY__REF = 1;

  /**
   * The number of structural features of the '<em>Component Or Any</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_OR_ANY_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.IndexAssignmentImpl <em>Index Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.IndexAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getIndexAssignment()
   * @generated
   */
  int INDEX_ASSIGNMENT = 90;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INDEX_ASSIGNMENT__INDEX = 0;

  /**
   * The number of structural features of the '<em>Index Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INDEX_ASSIGNMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.IndexSpecImpl <em>Index Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.IndexSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getIndexSpec()
   * @generated
   */
  int INDEX_SPEC = 91;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INDEX_SPEC__REF = 0;

  /**
   * The number of structural features of the '<em>Index Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INDEX_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GetReplyStatementImpl <em>Get Reply Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GetReplyStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGetReplyStatement()
   * @generated
   */
  int GET_REPLY_STATEMENT = 92;

  /**
   * The number of structural features of the '<em>Get Reply Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_REPLY_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CheckStatementImpl <em>Check Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CheckStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCheckStatement()
   * @generated
   */
  int CHECK_STATEMENT = 93;

  /**
   * The number of structural features of the '<em>Check Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortCheckOpImpl <em>Port Check Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortCheckOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortCheckOp()
   * @generated
   */
  int PORT_CHECK_OP = 94;

  /**
   * The feature id for the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CHECK_OP__CHECK = 0;

  /**
   * The number of structural features of the '<em>Port Check Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CHECK_OP_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CheckParameterImpl <em>Check Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CheckParameterImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCheckParameter()
   * @generated
   */
  int CHECK_PARAMETER = 95;

  /**
   * The number of structural features of the '<em>Check Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_PARAMETER_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RedirectPresentImpl <em>Redirect Present</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RedirectPresentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRedirectPresent()
   * @generated
   */
  int REDIRECT_PRESENT = 96;

  /**
   * The feature id for the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_PRESENT__SENDER = CHECK_PARAMETER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_PRESENT__INDEX = CHECK_PARAMETER_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Redirect Present</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_PRESENT_FEATURE_COUNT = CHECK_PARAMETER_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FromClausePresentImpl <em>From Clause Present</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FromClausePresentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFromClausePresent()
   * @generated
   */
  int FROM_CLAUSE_PRESENT = 97;

  /**
   * The number of structural features of the '<em>From Clause Present</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROM_CLAUSE_PRESENT_FEATURE_COUNT = CHECK_PARAMETER_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CheckPortOpsPresentImpl <em>Check Port Ops Present</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CheckPortOpsPresentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCheckPortOpsPresent()
   * @generated
   */
  int CHECK_PORT_OPS_PRESENT = 98;

  /**
   * The feature id for the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_PORT_OPS_PRESENT__FROM = CHECK_PARAMETER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Check Port Ops Present</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_PORT_OPS_PRESENT_FEATURE_COUNT = CHECK_PARAMETER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortGetReplyOpImpl <em>Port Get Reply Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortGetReplyOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortGetReplyOp()
   * @generated
   */
  int PORT_GET_REPLY_OP = 99;

  /**
   * The feature id for the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_REPLY_OP__FROM = CHECK_PORT_OPS_PRESENT__FROM;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_REPLY_OP__TEMPLATE = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_REPLY_OP__VALUE = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_REPLY_OP__REDIRECT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Port Get Reply Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_REPLY_OP_FEATURE_COUNT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortRedirectWithValueAndParamImpl <em>Port Redirect With Value And Param</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortRedirectWithValueAndParamImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortRedirectWithValueAndParam()
   * @generated
   */
  int PORT_REDIRECT_WITH_VALUE_AND_PARAM = 100;

  /**
   * The number of structural features of the '<em>Port Redirect With Value And Param</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl <em>Redirect With Value And Param Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RedirectWithValueAndParamSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRedirectWithValueAndParamSpec()
   * @generated
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC = 101;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC__VALUE = PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC__PARAM = PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC__SENDER = PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC__INDEX = PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC__REDIRECT = PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Redirect With Value And Param Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_VALUE_AND_PARAM_SPEC_FEATURE_COUNT = PORT_REDIRECT_WITH_VALUE_AND_PARAM_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ValueMatchSpecImpl <em>Value Match Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ValueMatchSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getValueMatchSpec()
   * @generated
   */
  int VALUE_MATCH_SPEC = 102;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_MATCH_SPEC__TEMPLATE = 0;

  /**
   * The number of structural features of the '<em>Value Match Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_MATCH_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CatchStatementImpl <em>Catch Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CatchStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCatchStatement()
   * @generated
   */
  int CATCH_STATEMENT = 103;

  /**
   * The number of structural features of the '<em>Catch Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATCH_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortCatchOpImpl <em>Port Catch Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortCatchOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortCatchOp()
   * @generated
   */
  int PORT_CATCH_OP = 104;

  /**
   * The feature id for the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CATCH_OP__FROM = CHECK_PORT_OPS_PRESENT__FROM;

  /**
   * The feature id for the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CATCH_OP__PARAM = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CATCH_OP__REDIRECT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Port Catch Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CATCH_OP_FEATURE_COUNT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CatchOpParameterImpl <em>Catch Op Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CatchOpParameterImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCatchOpParameter()
   * @generated
   */
  int CATCH_OP_PARAMETER = 105;

  /**
   * The feature id for the '<em><b>Signature</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATCH_OP_PARAMETER__SIGNATURE = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATCH_OP_PARAMETER__TEMPLATE = 1;

  /**
   * The number of structural features of the '<em>Catch Op Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATCH_OP_PARAMETER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GetCallStatementImpl <em>Get Call Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GetCallStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGetCallStatement()
   * @generated
   */
  int GET_CALL_STATEMENT = 106;

  /**
   * The number of structural features of the '<em>Get Call Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_CALL_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortGetCallOpImpl <em>Port Get Call Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortGetCallOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortGetCallOp()
   * @generated
   */
  int PORT_GET_CALL_OP = 107;

  /**
   * The feature id for the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_CALL_OP__FROM = CHECK_PORT_OPS_PRESENT__FROM;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_CALL_OP__TEMPLATE = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_CALL_OP__REDIRECT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Port Get Call Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_GET_CALL_OP_FEATURE_COUNT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortRedirectWithParamImpl <em>Port Redirect With Param</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortRedirectWithParamImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortRedirectWithParam()
   * @generated
   */
  int PORT_REDIRECT_WITH_PARAM = 108;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT_WITH_PARAM__REDIRECT = 0;

  /**
   * The number of structural features of the '<em>Port Redirect With Param</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT_WITH_PARAM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithParamSpecImpl <em>Redirect With Param Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RedirectWithParamSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRedirectWithParamSpec()
   * @generated
   */
  int REDIRECT_WITH_PARAM_SPEC = 109;

  /**
   * The feature id for the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_PARAM_SPEC__PARAM = 0;

  /**
   * The feature id for the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_PARAM_SPEC__SENDER = 1;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_PARAM_SPEC__INDEX = 2;

  /**
   * The number of structural features of the '<em>Redirect With Param Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REDIRECT_WITH_PARAM_SPEC_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ParamSpecImpl <em>Param Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ParamSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getParamSpec()
   * @generated
   */
  int PARAM_SPEC = 110;

  /**
   * The number of structural features of the '<em>Param Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_SPEC_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ParamAssignmentListImpl <em>Param Assignment List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ParamAssignmentListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getParamAssignmentList()
   * @generated
   */
  int PARAM_ASSIGNMENT_LIST = 111;

  /**
   * The number of structural features of the '<em>Param Assignment List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_ASSIGNMENT_LIST_FEATURE_COUNT = PARAM_SPEC_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AssignmentListImpl <em>Assignment List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AssignmentListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAssignmentList()
   * @generated
   */
  int ASSIGNMENT_LIST = 112;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_LIST__ASSIGN = PARAM_ASSIGNMENT_LIST_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Assignment List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_LIST_FEATURE_COUNT = PARAM_ASSIGNMENT_LIST_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.VariableAssignmentImpl <em>Variable Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.VariableAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVariableAssignment()
   * @generated
   */
  int VARIABLE_ASSIGNMENT = 113;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_ASSIGNMENT__REF = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_ASSIGNMENT__NAME = 1;

  /**
   * The number of structural features of the '<em>Variable Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_ASSIGNMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.VariableListImpl <em>Variable List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.VariableListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVariableList()
   * @generated
   */
  int VARIABLE_LIST = 114;

  /**
   * The feature id for the '<em><b>Entries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_LIST__ENTRIES = PARAM_ASSIGNMENT_LIST_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_LIST_FEATURE_COUNT = PARAM_ASSIGNMENT_LIST_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.VariableEntryImpl <em>Variable Entry</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.VariableEntryImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVariableEntry()
   * @generated
   */
  int VARIABLE_ENTRY = 115;

  /**
   * The number of structural features of the '<em>Variable Entry</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_ENTRY_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TriggerStatementImpl <em>Trigger Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TriggerStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTriggerStatement()
   * @generated
   */
  int TRIGGER_STATEMENT = 116;

  /**
   * The number of structural features of the '<em>Trigger Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRIGGER_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortTriggerOpImpl <em>Port Trigger Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortTriggerOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortTriggerOp()
   * @generated
   */
  int PORT_TRIGGER_OP = 117;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_TRIGGER_OP__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_TRIGGER_OP__FROM = 1;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_TRIGGER_OP__REDIRECT = 2;

  /**
   * The number of structural features of the '<em>Port Trigger Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_TRIGGER_OP_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReceiveStatementImpl <em>Receive Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReceiveStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReceiveStatement()
   * @generated
   */
  int RECEIVE_STATEMENT = 118;

  /**
   * The feature id for the '<em><b>Any</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT__ANY = 0;

  /**
   * The feature id for the '<em><b>Receive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT__RECEIVE = 1;

  /**
   * The number of structural features of the '<em>Receive Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortOrAnyImpl <em>Port Or Any</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortOrAnyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortOrAny()
   * @generated
   */
  int PORT_OR_ANY = 119;

  /**
   * The feature id for the '<em><b>Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__REPLY = GET_REPLY_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__CHECK = GET_REPLY_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__CATCH = GET_REPLY_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__CALL = GET_REPLY_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Trigger</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__TRIGGER = GET_REPLY_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__REF = GET_REPLY_STATEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__ARRAY = GET_REPLY_STATEMENT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY__VARIABLE = GET_REPLY_STATEMENT_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>Port Or Any</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ANY_FEATURE_COUNT = GET_REPLY_STATEMENT_FEATURE_COUNT + 8;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortReceiveOpImpl <em>Port Receive Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortReceiveOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortReceiveOp()
   * @generated
   */
  int PORT_RECEIVE_OP = 120;

  /**
   * The feature id for the '<em><b>From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RECEIVE_OP__FROM = CHECK_PORT_OPS_PRESENT__FROM;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RECEIVE_OP__TEMPLATE = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RECEIVE_OP__REDIRECT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Port Receive Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RECEIVE_OP_FEATURE_COUNT = CHECK_PORT_OPS_PRESENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FromClauseImpl <em>From Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FromClauseImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFromClause()
   * @generated
   */
  int FROM_CLAUSE = 121;

  /**
   * The feature id for the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROM_CLAUSE__SENDER = FROM_CLAUSE_PRESENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROM_CLAUSE__INDEX = FROM_CLAUSE_PRESENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROM_CLAUSE__TEMPLATE = FROM_CLAUSE_PRESENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Addresses</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROM_CLAUSE__ADDRESSES = FROM_CLAUSE_PRESENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>From Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROM_CLAUSE_FEATURE_COUNT = FROM_CLAUSE_PRESENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AddressRefListImpl <em>Address Ref List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AddressRefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAddressRefList()
   * @generated
   */
  int ADDRESS_REF_LIST = 122;

  /**
   * The feature id for the '<em><b>Templates</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDRESS_REF_LIST__TEMPLATES = 0;

  /**
   * The number of structural features of the '<em>Address Ref List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDRESS_REF_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortRedirectImpl <em>Port Redirect</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortRedirectImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortRedirect()
   * @generated
   */
  int PORT_REDIRECT = 123;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT__VALUE = 0;

  /**
   * The feature id for the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT__SENDER = 1;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT__INDEX = 2;

  /**
   * The number of structural features of the '<em>Port Redirect</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REDIRECT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SenderSpecImpl <em>Sender Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SenderSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSenderSpec()
   * @generated
   */
  int SENDER_SPEC = 124;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SENDER_SPEC__VARIABLE = 0;

  /**
   * The number of structural features of the '<em>Sender Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SENDER_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ValueSpecImpl <em>Value Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ValueSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getValueSpec()
   * @generated
   */
  int VALUE_SPEC = 125;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_SPEC__VARIABLE = 0;

  /**
   * The feature id for the '<em><b>Specs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_SPEC__SPECS = 1;

  /**
   * The number of structural features of the '<em>Value Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleValueSpecImpl <em>Single Value Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleValueSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleValueSpec()
   * @generated
   */
  int SINGLE_VALUE_SPEC = 126;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VALUE_SPEC__VARIABLE = 0;

  /**
   * The feature id for the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VALUE_SPEC__FIELD = 1;

  /**
   * The feature id for the '<em><b>Ext</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VALUE_SPEC__EXT = 2;

  /**
   * The number of structural features of the '<em>Single Value Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VALUE_SPEC_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltGuardCharImpl <em>Alt Guard Char</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltGuardCharImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltGuardChar()
   * @generated
   */
  int ALT_GUARD_CHAR = 127;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_GUARD_CHAR__EXPR = 0;

  /**
   * The number of structural features of the '<em>Alt Guard Char</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALT_GUARD_CHAR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AltstepInstanceImpl <em>Altstep Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AltstepInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAltstepInstance()
   * @generated
   */
  int ALTSTEP_INSTANCE = 128;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_INSTANCE__REF = 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_INSTANCE__LIST = 1;

  /**
   * The number of structural features of the '<em>Altstep Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTSTEP_INSTANCE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionInstanceImpl <em>Function Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionInstance()
   * @generated
   */
  int FUNCTION_INSTANCE = 129;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INSTANCE__REF = 0;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INSTANCE__PARAMS = 1;

  /**
   * The number of structural features of the '<em>Function Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INSTANCE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParListImpl <em>Function Actual Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionActualParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionActualParList()
   * @generated
   */
  int FUNCTION_ACTUAL_PAR_LIST = 131;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_LIST__PARAMS = 0;

  /**
   * The feature id for the '<em><b>Asssign</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_LIST__ASSSIGN = 1;

  /**
   * The number of structural features of the '<em>Function Actual Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParAssignmentImpl <em>Function Actual Par Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionActualParAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionActualParAssignment()
   * @generated
   */
  int FUNCTION_ACTUAL_PAR_ASSIGNMENT = 132;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_ASSIGNMENT__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_ASSIGNMENT__COMPONENT = 1;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_ASSIGNMENT__PORT = 2;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_ASSIGNMENT__TIMER = 3;

  /**
   * The number of structural features of the '<em>Function Actual Par Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_ASSIGNMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentRefAssignmentImpl <em>Component Ref Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentRefAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentRefAssignment()
   * @generated
   */
  int COMPONENT_REF_ASSIGNMENT = 133;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF_ASSIGNMENT__REF = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF_ASSIGNMENT__VALUE = 1;

  /**
   * The number of structural features of the '<em>Component Ref Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF_ASSIGNMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FormalPortAndValueParImpl <em>Formal Port And Value Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FormalPortAndValueParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFormalPortAndValuePar()
   * @generated
   */
  int FORMAL_PORT_AND_VALUE_PAR = 134;

  /**
   * The number of structural features of the '<em>Formal Port And Value Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortRefAssignmentImpl <em>Port Ref Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortRefAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortRefAssignment()
   * @generated
   */
  int PORT_REF_ASSIGNMENT = 135;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF_ASSIGNMENT__PORT = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF_ASSIGNMENT__VALUE = 1;

  /**
   * The number of structural features of the '<em>Port Ref Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF_ASSIGNMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerRefAssignmentImpl <em>Timer Ref Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerRefAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerRefAssignment()
   * @generated
   */
  int TIMER_REF_ASSIGNMENT = 136;

  /**
   * The feature id for the '<em><b>Timer</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_ASSIGNMENT__TIMER = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_ASSIGNMENT__VALUE = 1;

  /**
   * The number of structural features of the '<em>Timer Ref Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_ASSIGNMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParImpl <em>Function Actual Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionActualParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionActualPar()
   * @generated
   */
  int FUNCTION_ACTUAL_PAR = 137;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR__COMPONENT = 1;

  /**
   * The feature id for the '<em><b>Timer</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR__TIMER = 2;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR__PORT = 3;

  /**
   * The number of structural features of the '<em>Function Actual Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_ACTUAL_PAR_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentRefImpl <em>Component Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentRef()
   * @generated
   */
  int COMPONENT_REF = 138;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF__REF = 0;

  /**
   * The feature id for the '<em><b>System</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF__SYSTEM = 1;

  /**
   * The feature id for the '<em><b>Self</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF__SELF = 2;

  /**
   * The feature id for the '<em><b>Mtc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF__MTC = 3;

  /**
   * The number of structural features of the '<em>Component Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentOrDefaultReferenceImpl <em>Component Or Default Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentOrDefaultReferenceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentOrDefaultReference()
   * @generated
   */
  int COMPONENT_OR_DEFAULT_REFERENCE = 139;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_OR_DEFAULT_REFERENCE__VARIABLE = 0;

  /**
   * The number of structural features of the '<em>Component Or Default Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_OR_DEFAULT_REFERENCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseInstanceImpl <em>Testcase Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TestcaseInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTestcaseInstance()
   * @generated
   */
  int TESTCASE_INSTANCE = 140;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_INSTANCE__REF = 0;

  /**
   * The feature id for the '<em><b>Tc Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_INSTANCE__TC_PARAMS = 1;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_INSTANCE__EXPR = 2;

  /**
   * The feature id for the '<em><b>Sexpr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_INSTANCE__SEXPR = 3;

  /**
   * The number of structural features of the '<em>Testcase Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_INSTANCE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseActualParListImpl <em>Testcase Actual Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TestcaseActualParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTestcaseActualParList()
   * @generated
   */
  int TESTCASE_ACTUAL_PAR_LIST = 141;

  /**
   * The feature id for the '<em><b>Templ Param</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_ACTUAL_PAR_LIST__TEMPL_PARAM = 0;

  /**
   * The feature id for the '<em><b>Templ Assign</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_ACTUAL_PAR_LIST__TEMPL_ASSIGN = 1;

  /**
   * The number of structural features of the '<em>Testcase Actual Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_ACTUAL_PAR_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerStatementsImpl <em>Timer Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerStatementsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerStatements()
   * @generated
   */
  int TIMER_STATEMENTS = 142;

  /**
   * The feature id for the '<em><b>Start</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_STATEMENTS__START = 0;

  /**
   * The feature id for the '<em><b>Stop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_STATEMENTS__STOP = 1;

  /**
   * The feature id for the '<em><b>Timeout</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_STATEMENTS__TIMEOUT = 2;

  /**
   * The number of structural features of the '<em>Timer Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_STATEMENTS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimeoutStatementImpl <em>Timeout Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimeoutStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimeoutStatement()
   * @generated
   */
  int TIMEOUT_STATEMENT = 143;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMEOUT_STATEMENT__REF = 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMEOUT_STATEMENT__INDEX = 1;

  /**
   * The number of structural features of the '<em>Timeout Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMEOUT_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StartTimerStatementImpl <em>Start Timer Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StartTimerStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStartTimerStatement()
   * @generated
   */
  int START_TIMER_STATEMENT = 144;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TIMER_STATEMENT__REF = 0;

  /**
   * The feature id for the '<em><b>Arry Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TIMER_STATEMENT__ARRY_REFS = 1;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TIMER_STATEMENT__EXPR = 2;

  /**
   * The number of structural features of the '<em>Start Timer Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TIMER_STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StopTimerStatementImpl <em>Stop Timer Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StopTimerStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStopTimerStatement()
   * @generated
   */
  int STOP_TIMER_STATEMENT = 145;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STOP_TIMER_STATEMENT__REF = 0;

  /**
   * The number of structural features of the '<em>Stop Timer Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STOP_TIMER_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerRefOrAnyImpl <em>Timer Ref Or Any</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerRefOrAnyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerRefOrAny()
   * @generated
   */
  int TIMER_REF_OR_ANY = 146;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ANY__REF = 0;

  /**
   * The feature id for the '<em><b>Timer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ANY__TIMER = 1;

  /**
   * The feature id for the '<em><b>From</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ANY__FROM = 2;

  /**
   * The number of structural features of the '<em>Timer Ref Or Any</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ANY_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerRefOrAllImpl <em>Timer Ref Or All</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerRefOrAllImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerRefOrAll()
   * @generated
   */
  int TIMER_REF_OR_ALL = 147;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ALL__REF = 0;

  /**
   * The feature id for the '<em><b>Timer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ALL__TIMER = 1;

  /**
   * The number of structural features of the '<em>Timer Ref Or All</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_REF_OR_ALL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl <em>Basic Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBasicStatements()
   * @generated
   */
  int BASIC_STATEMENTS = 148;

  /**
   * The feature id for the '<em><b>Log</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS__LOG = 0;

  /**
   * The feature id for the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS__BLOCK = 1;

  /**
   * The feature id for the '<em><b>Loop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS__LOOP = 2;

  /**
   * The feature id for the '<em><b>Conditional</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS__CONDITIONAL = 3;

  /**
   * The feature id for the '<em><b>Select</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS__SELECT = 4;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS__ASSIGN = 5;

  /**
   * The number of structural features of the '<em>Basic Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_STATEMENTS_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StatementBlockImpl <em>Statement Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StatementBlockImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStatementBlock()
   * @generated
   */
  int STATEMENT_BLOCK = 149;

  /**
   * The feature id for the '<em><b>Def</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_BLOCK__DEF = 0;

  /**
   * The feature id for the '<em><b>Stat</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_BLOCK__STAT = 1;

  /**
   * The number of structural features of the '<em>Statement Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_BLOCK_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementListImpl <em>Function Statement List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionStatementListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionStatementList()
   * @generated
   */
  int FUNCTION_STATEMENT_LIST = 150;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT_LIST__STATEMENTS = 0;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT_LIST__SC = 1;

  /**
   * The number of structural features of the '<em>Function Statement List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl <em>Function Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionStatement()
   * @generated
   */
  int FUNCTION_STATEMENT = 151;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__TIMER = 0;

  /**
   * The feature id for the '<em><b>Basic</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__BASIC = 1;

  /**
   * The feature id for the '<em><b>Behavior</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__BEHAVIOR = 2;

  /**
   * The feature id for the '<em><b>Verdict</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__VERDICT = 3;

  /**
   * The feature id for the '<em><b>Communication</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__COMMUNICATION = 4;

  /**
   * The feature id for the '<em><b>Configuration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__CONFIGURATION = 5;

  /**
   * The feature id for the '<em><b>Sut</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__SUT = 6;

  /**
   * The feature id for the '<em><b>Test</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT__TEST = 7;

  /**
   * The number of structural features of the '<em>Function Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_STATEMENT_FEATURE_COUNT = 8;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseOperationImpl <em>Testcase Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TestcaseOperationImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTestcaseOperation()
   * @generated
   */
  int TESTCASE_OPERATION = 152;

  /**
   * The feature id for the '<em><b>Txt</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_OPERATION__TXT = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_OPERATION__TEMPLATE = 1;

  /**
   * The number of structural features of the '<em>Testcase Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TESTCASE_OPERATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SetLocalVerdictImpl <em>Set Local Verdict</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SetLocalVerdictImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSetLocalVerdict()
   * @generated
   */
  int SET_LOCAL_VERDICT = 153;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_LOCAL_VERDICT__EXPRESSION = 0;

  /**
   * The feature id for the '<em><b>Log</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_LOCAL_VERDICT__LOG = 1;

  /**
   * The number of structural features of the '<em>Set Local Verdict</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_LOCAL_VERDICT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl <em>Configuration Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConfigurationStatementsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConfigurationStatements()
   * @generated
   */
  int CONFIGURATION_STATEMENTS = 154;

  /**
   * The feature id for the '<em><b>Connect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__CONNECT = 0;

  /**
   * The feature id for the '<em><b>Map</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__MAP = 1;

  /**
   * The feature id for the '<em><b>Disconnect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__DISCONNECT = 2;

  /**
   * The feature id for the '<em><b>Unmap</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__UNMAP = 3;

  /**
   * The feature id for the '<em><b>Done</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__DONE = 4;

  /**
   * The feature id for the '<em><b>Killed</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__KILLED = 5;

  /**
   * The feature id for the '<em><b>Start Tc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__START_TC = 6;

  /**
   * The feature id for the '<em><b>Stop Tc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__STOP_TC = 7;

  /**
   * The feature id for the '<em><b>Kill Tc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS__KILL_TC = 8;

  /**
   * The number of structural features of the '<em>Configuration Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_STATEMENTS_FEATURE_COUNT = 9;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.KillTCStatementImpl <em>Kill TC Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.KillTCStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getKillTCStatement()
   * @generated
   */
  int KILL_TC_STATEMENT = 155;

  /**
   * The number of structural features of the '<em>Kill TC Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int KILL_TC_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StopTCStatementImpl <em>Stop TC Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StopTCStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStopTCStatement()
   * @generated
   */
  int STOP_TC_STATEMENT = 156;

  /**
   * The number of structural features of the '<em>Stop TC Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STOP_TC_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentReferenceOrLiteralImpl <em>Component Reference Or Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentReferenceOrLiteralImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentReferenceOrLiteral()
   * @generated
   */
  int COMPONENT_REFERENCE_OR_LITERAL = 157;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REFERENCE_OR_LITERAL__REF = KILL_TC_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Component Reference Or Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REFERENCE_OR_LITERAL_FEATURE_COUNT = KILL_TC_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StartTCStatementImpl <em>Start TC Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StartTCStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStartTCStatement()
   * @generated
   */
  int START_TC_STATEMENT = 158;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TC_STATEMENT__REF = 0;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TC_STATEMENT__FUNCTION = 1;

  /**
   * The number of structural features of the '<em>Start TC Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_TC_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.UnmapStatementImpl <em>Unmap Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.UnmapStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getUnmapStatement()
   * @generated
   */
  int UNMAP_STATEMENT = 159;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAP_STATEMENT__SPEC = 0;

  /**
   * The feature id for the '<em><b>Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAP_STATEMENT__CLAUSE = 1;

  /**
   * The number of structural features of the '<em>Unmap Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAP_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DisconnectStatementImpl <em>Disconnect Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DisconnectStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDisconnectStatement()
   * @generated
   */
  int DISCONNECT_STATEMENT = 160;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISCONNECT_STATEMENT__SPEC = 0;

  /**
   * The number of structural features of the '<em>Disconnect Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISCONNECT_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllConnectionsSpecImpl <em>All Connections Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllConnectionsSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllConnectionsSpec()
   * @generated
   */
  int ALL_CONNECTIONS_SPEC = 161;

  /**
   * The number of structural features of the '<em>All Connections Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_CONNECTIONS_SPEC_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllPortsSpecImpl <em>All Ports Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllPortsSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllPortsSpec()
   * @generated
   */
  int ALL_PORTS_SPEC = 162;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_PORTS_SPEC__COMPONENT = 0;

  /**
   * The number of structural features of the '<em>All Ports Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_PORTS_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MapStatementImpl <em>Map Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MapStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMapStatement()
   * @generated
   */
  int MAP_STATEMENT = 163;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAP_STATEMENT__SPEC = 0;

  /**
   * The feature id for the '<em><b>Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAP_STATEMENT__CLAUSE = 1;

  /**
   * The number of structural features of the '<em>Map Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAP_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ParamClauseImpl <em>Param Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ParamClauseImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getParamClause()
   * @generated
   */
  int PARAM_CLAUSE = 164;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_CLAUSE__LIST = 0;

  /**
   * The number of structural features of the '<em>Param Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_CLAUSE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConnectStatementImpl <em>Connect Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConnectStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConnectStatement()
   * @generated
   */
  int CONNECT_STATEMENT = 165;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONNECT_STATEMENT__SPEC = 0;

  /**
   * The number of structural features of the '<em>Connect Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONNECT_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleConnectionSpecImpl <em>Single Connection Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleConnectionSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleConnectionSpec()
   * @generated
   */
  int SINGLE_CONNECTION_SPEC = 166;

  /**
   * The feature id for the '<em><b>Port1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONNECTION_SPEC__PORT1 = 0;

  /**
   * The feature id for the '<em><b>Port2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONNECTION_SPEC__PORT2 = 1;

  /**
   * The number of structural features of the '<em>Single Connection Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONNECTION_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortRefImpl <em>Port Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortRef()
   * @generated
   */
  int PORT_REF = 167;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF__COMPONENT = ALL_CONNECTIONS_SPEC_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF__PORT = ALL_CONNECTIONS_SPEC_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF__ARRAY = ALL_CONNECTIONS_SPEC_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Port Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REF_FEATURE_COUNT = ALL_CONNECTIONS_SPEC_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl <em>Communication Statements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CommunicationStatementsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCommunicationStatements()
   * @generated
   */
  int COMMUNICATION_STATEMENTS = 168;

  /**
   * The feature id for the '<em><b>Send</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__SEND = 0;

  /**
   * The feature id for the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__CALL = 1;

  /**
   * The feature id for the '<em><b>Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__REPLY = 2;

  /**
   * The feature id for the '<em><b>Raise</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__RAISE = 3;

  /**
   * The feature id for the '<em><b>Receive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__RECEIVE = 4;

  /**
   * The feature id for the '<em><b>Trigger</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__TRIGGER = 5;

  /**
   * The feature id for the '<em><b>Get Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__GET_CALL = 6;

  /**
   * The feature id for the '<em><b>Get Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__GET_REPLY = 7;

  /**
   * The feature id for the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__CATCH = 8;

  /**
   * The feature id for the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__CHECK = 9;

  /**
   * The feature id for the '<em><b>Clear</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__CLEAR = 10;

  /**
   * The feature id for the '<em><b>Start</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__START = 11;

  /**
   * The feature id for the '<em><b>Stop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__STOP = 12;

  /**
   * The feature id for the '<em><b>Halt</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__HALT = 13;

  /**
   * The feature id for the '<em><b>Check State</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS__CHECK_STATE = 14;

  /**
   * The number of structural features of the '<em>Communication Statements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_STATEMENTS_FEATURE_COUNT = 15;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CheckStateStatementImpl <em>Check State Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CheckStateStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCheckStateStatement()
   * @generated
   */
  int CHECK_STATE_STATEMENT = 169;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_STATE_STATEMENT__PORT = 0;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_STATE_STATEMENT__EXPR = 1;

  /**
   * The number of structural features of the '<em>Check State Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_STATE_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortOrAllAnyImpl <em>Port Or All Any</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortOrAllAnyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortOrAllAny()
   * @generated
   */
  int PORT_OR_ALL_ANY = 170;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ALL_ANY__PORT = 0;

  /**
   * The number of structural features of the '<em>Port Or All Any</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ALL_ANY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.HaltStatementImpl <em>Halt Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.HaltStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getHaltStatement()
   * @generated
   */
  int HALT_STATEMENT = 171;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HALT_STATEMENT__PORT = 0;

  /**
   * The number of structural features of the '<em>Halt Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HALT_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StartStatementImpl <em>Start Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StartStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStartStatement()
   * @generated
   */
  int START_STATEMENT = 172;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_STATEMENT__PORT = 0;

  /**
   * The number of structural features of the '<em>Start Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StopStatementImpl <em>Stop Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StopStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStopStatement()
   * @generated
   */
  int STOP_STATEMENT = 173;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STOP_STATEMENT__PORT = 0;

  /**
   * The number of structural features of the '<em>Stop Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STOP_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ClearStatementImpl <em>Clear Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ClearStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getClearStatement()
   * @generated
   */
  int CLEAR_STATEMENT = 174;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLEAR_STATEMENT__PORT = 0;

  /**
   * The number of structural features of the '<em>Clear Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLEAR_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortOrAllImpl <em>Port Or All</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortOrAllImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortOrAll()
   * @generated
   */
  int PORT_OR_ALL = 175;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ALL__PORT = 0;

  /**
   * The feature id for the '<em><b>Array Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ALL__ARRAY_REFS = 1;

  /**
   * The number of structural features of the '<em>Port Or All</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_OR_ALL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RaiseStatementImpl <em>Raise Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RaiseStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRaiseStatement()
   * @generated
   */
  int RAISE_STATEMENT = 176;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_STATEMENT__PORT = 0;

  /**
   * The feature id for the '<em><b>Array Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_STATEMENT__ARRAY_REFS = 1;

  /**
   * The feature id for the '<em><b>Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_STATEMENT__OP = 2;

  /**
   * The number of structural features of the '<em>Raise Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortRaiseOpImpl <em>Port Raise Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortRaiseOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortRaiseOp()
   * @generated
   */
  int PORT_RAISE_OP = 177;

  /**
   * The feature id for the '<em><b>Signature</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RAISE_OP__SIGNATURE = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RAISE_OP__TEMPLATE = 1;

  /**
   * The feature id for the '<em><b>To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RAISE_OP__TO = 2;

  /**
   * The number of structural features of the '<em>Port Raise Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_RAISE_OP_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReplyStatementImpl <em>Reply Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReplyStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReplyStatement()
   * @generated
   */
  int REPLY_STATEMENT = 178;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_STATEMENT__PORT = 0;

  /**
   * The feature id for the '<em><b>Array Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_STATEMENT__ARRAY_REFS = 1;

  /**
   * The feature id for the '<em><b>Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_STATEMENT__OP = 2;

  /**
   * The number of structural features of the '<em>Reply Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortReplyOpImpl <em>Port Reply Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortReplyOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortReplyOp()
   * @generated
   */
  int PORT_REPLY_OP = 179;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REPLY_OP__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REPLY_OP__VALUE = 1;

  /**
   * The feature id for the '<em><b>To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REPLY_OP__TO = 2;

  /**
   * The number of structural features of the '<em>Port Reply Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_REPLY_OP_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReplyValueImpl <em>Reply Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReplyValueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReplyValue()
   * @generated
   */
  int REPLY_VALUE = 180;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_VALUE__EXPR = 0;

  /**
   * The number of structural features of the '<em>Reply Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_VALUE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallStatementImpl <em>Call Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallStatement()
   * @generated
   */
  int CALL_STATEMENT = 181;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATEMENT__PORT = 0;

  /**
   * The feature id for the '<em><b>Array Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATEMENT__ARRAY_REFS = 1;

  /**
   * The feature id for the '<em><b>Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATEMENT__OP = 2;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATEMENT__BODY = 3;

  /**
   * The number of structural features of the '<em>Call Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATEMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortCallOpImpl <em>Port Call Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortCallOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortCallOp()
   * @generated
   */
  int PORT_CALL_OP = 182;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CALL_OP__PARAMS = 0;

  /**
   * The feature id for the '<em><b>To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CALL_OP__TO = 1;

  /**
   * The number of structural features of the '<em>Port Call Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CALL_OP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallParametersImpl <em>Call Parameters</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallParametersImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallParameters()
   * @generated
   */
  int CALL_PARAMETERS = 183;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_PARAMETERS__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_PARAMETERS__VALUE = 1;

  /**
   * The number of structural features of the '<em>Call Parameters</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_PARAMETERS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallTimerValueImpl <em>Call Timer Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallTimerValueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallTimerValue()
   * @generated
   */
  int CALL_TIMER_VALUE = 184;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_TIMER_VALUE__EXPR = 0;

  /**
   * The number of structural features of the '<em>Call Timer Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_TIMER_VALUE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortCallBodyImpl <em>Port Call Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortCallBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortCallBody()
   * @generated
   */
  int PORT_CALL_BODY = 185;

  /**
   * The feature id for the '<em><b>Call Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CALL_BODY__CALL_BODY = 0;

  /**
   * The number of structural features of the '<em>Port Call Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_CALL_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallBodyStatementListImpl <em>Call Body Statement List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallBodyStatementListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallBodyStatementList()
   * @generated
   */
  int CALL_BODY_STATEMENT_LIST = 186;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_STATEMENT_LIST__BODY = 0;

  /**
   * The number of structural features of the '<em>Call Body Statement List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_STATEMENT_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallBodyStatementImpl <em>Call Body Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallBodyStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallBodyStatement()
   * @generated
   */
  int CALL_BODY_STATEMENT = 187;

  /**
   * The feature id for the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_STATEMENT__CALL = 0;

  /**
   * The feature id for the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_STATEMENT__BLOCK = 1;

  /**
   * The number of structural features of the '<em>Call Body Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallBodyGuardImpl <em>Call Body Guard</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallBodyGuardImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallBodyGuard()
   * @generated
   */
  int CALL_BODY_GUARD = 188;

  /**
   * The feature id for the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_GUARD__GUARD = 0;

  /**
   * The feature id for the '<em><b>Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_GUARD__OPS = 1;

  /**
   * The number of structural features of the '<em>Call Body Guard</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_GUARD_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CallBodyOpsImpl <em>Call Body Ops</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CallBodyOpsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCallBodyOps()
   * @generated
   */
  int CALL_BODY_OPS = 189;

  /**
   * The feature id for the '<em><b>Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_OPS__REPLY = 0;

  /**
   * The feature id for the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_OPS__CATCH = 1;

  /**
   * The number of structural features of the '<em>Call Body Ops</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_BODY_OPS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SendStatementImpl <em>Send Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SendStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSendStatement()
   * @generated
   */
  int SEND_STATEMENT = 190;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT__PORT = 0;

  /**
   * The feature id for the '<em><b>Arry Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT__ARRY_REFS = 1;

  /**
   * The feature id for the '<em><b>Send</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT__SEND = 2;

  /**
   * The number of structural features of the '<em>Send Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortSendOpImpl <em>Port Send Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortSendOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortSendOp()
   * @generated
   */
  int PORT_SEND_OP = 191;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_SEND_OP__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_SEND_OP__TO = 1;

  /**
   * The number of structural features of the '<em>Port Send Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_SEND_OP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ToClauseImpl <em>To Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ToClauseImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getToClause()
   * @generated
   */
  int TO_CLAUSE = 192;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TO_CLAUSE__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TO_CLAUSE__REF = 1;

  /**
   * The number of structural features of the '<em>To Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TO_CLAUSE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefListImpl <em>Function Def List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionDefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionDefList()
   * @generated
   */
  int FUNCTION_DEF_LIST = 193;

  /**
   * The feature id for the '<em><b>Loc Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF_LIST__LOC_DEF = 0;

  /**
   * The feature id for the '<em><b>Loc Inst</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF_LIST__LOC_INST = 1;

  /**
   * The feature id for the '<em><b>Ws</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF_LIST__WS = 2;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF_LIST__SC = 3;

  /**
   * The number of structural features of the '<em>Function Def List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF_LIST_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionLocalDefImpl <em>Function Local Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionLocalDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionLocalDef()
   * @generated
   */
  int FUNCTION_LOCAL_DEF = 194;

  /**
   * The feature id for the '<em><b>Const Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_LOCAL_DEF__CONST_DEF = 0;

  /**
   * The feature id for the '<em><b>Template Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_LOCAL_DEF__TEMPLATE_DEF = 1;

  /**
   * The number of structural features of the '<em>Function Local Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_LOCAL_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionLocalInstImpl <em>Function Local Inst</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionLocalInstImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionLocalInst()
   * @generated
   */
  int FUNCTION_LOCAL_INST = 195;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_LOCAL_INST__VARIABLE = 0;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_LOCAL_INST__TIMER = 1;

  /**
   * The number of structural features of the '<em>Function Local Inst</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_LOCAL_INST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerInstanceImpl <em>Timer Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerInstance()
   * @generated
   */
  int TIMER_INSTANCE = 196;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_INSTANCE__LIST = 0;

  /**
   * The number of structural features of the '<em>Timer Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_INSTANCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl <em>Var Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVarInstance()
   * @generated
   */
  int VAR_INSTANCE = 197;

  /**
   * The feature id for the '<em><b>List Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__LIST_MOD = 0;

  /**
   * The feature id for the '<em><b>List Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__LIST_TYPE = 1;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__LIST = 2;

  /**
   * The feature id for the '<em><b>Res Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__RES_TEMPLATE = 3;

  /**
   * The feature id for the '<em><b>Temp Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__TEMP_MOD = 4;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__TYPE = 5;

  /**
   * The feature id for the '<em><b>Temp List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE__TEMP_LIST = 6;

  /**
   * The number of structural features of the '<em>Var Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_INSTANCE_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.VarListImpl <em>Var List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.VarListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVarList()
   * @generated
   */
  int VAR_LIST = 198;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_LIST__VARIABLES = 0;

  /**
   * The number of structural features of the '<em>Var List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeDefImpl <em>Type Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTypeDef()
   * @generated
   */
  int TYPE_DEF = 201;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF__BODY = 0;

  /**
   * The number of structural features of the '<em>Type Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeDefBodyImpl <em>Type Def Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeDefBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTypeDefBody()
   * @generated
   */
  int TYPE_DEF_BODY = 202;

  /**
   * The feature id for the '<em><b>Structured</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF_BODY__STRUCTURED = 0;

  /**
   * The feature id for the '<em><b>Sub</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF_BODY__SUB = 1;

  /**
   * The number of structural features of the '<em>Type Def Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF_BODY_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SubTypeDefImpl <em>Sub Type Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SubTypeDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSubTypeDef()
   * @generated
   */
  int SUB_TYPE_DEF = 203;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF__TYPE = 0;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF__ARRAY = 1;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF__SPEC = 2;

  /**
   * The number of structural features of the '<em>Sub Type Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SubTypeDefNamedImpl <em>Sub Type Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SubTypeDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSubTypeDefNamed()
   * @generated
   */
  int SUB_TYPE_DEF_NAMED = 204;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF_NAMED__TYPE = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF_NAMED__ARRAY = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF_NAMED__SPEC = REFERENCED_TYPE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Sub Type Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl <em>Structured Type Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StructuredTypeDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStructuredTypeDef()
   * @generated
   */
  int STRUCTURED_TYPE_DEF = 205;

  /**
   * The feature id for the '<em><b>Record</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__RECORD = 0;

  /**
   * The feature id for the '<em><b>Union</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__UNION = 1;

  /**
   * The feature id for the '<em><b>Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__SET = 2;

  /**
   * The feature id for the '<em><b>Record Of</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__RECORD_OF = 3;

  /**
   * The feature id for the '<em><b>Set Of</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__SET_OF = 4;

  /**
   * The feature id for the '<em><b>Enum Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__ENUM_DEF = 5;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__PORT = 6;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF__COMPONENT = 7;

  /**
   * The number of structural features of the '<em>Structured Type Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TYPE_DEF_FEATURE_COUNT = 8;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RecordDefImpl <em>Record Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RecordDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRecordDef()
   * @generated
   */
  int RECORD_DEF = 206;

  /**
   * The number of structural features of the '<em>Record Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_DEF_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RecordDefNamedImpl <em>Record Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RecordDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRecordDefNamed()
   * @generated
   */
  int RECORD_DEF_NAMED = 207;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_DEF_NAMED__BODY = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Record Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RecordOfDefImpl <em>Record Of Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RecordOfDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRecordOfDef()
   * @generated
   */
  int RECORD_OF_DEF = 208;

  /**
   * The feature id for the '<em><b>Lenght</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF__LENGHT = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF__TYPE = 1;

  /**
   * The feature id for the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF__NESTED = 2;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF__SPEC = 3;

  /**
   * The number of structural features of the '<em>Record Of Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RecordOfDefNamedImpl <em>Record Of Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RecordOfDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRecordOfDefNamed()
   * @generated
   */
  int RECORD_OF_DEF_NAMED = 209;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Lenght</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_NAMED__LENGHT = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_NAMED__TYPE = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_NAMED__NESTED = REFERENCED_TYPE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_NAMED__SPEC = REFERENCED_TYPE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Record Of Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_OF_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StructDefBodyImpl <em>Struct Def Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StructDefBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStructDefBody()
   * @generated
   */
  int STRUCT_DEF_BODY = 210;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_DEF_BODY__DEFS = RECORD_DEF_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Struct Def Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_DEF_BODY_FEATURE_COUNT = RECORD_DEF_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldReferenceImpl <em>Field Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldReferenceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldReference()
   * @generated
   */
  int FIELD_REFERENCE = 345;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_REFERENCE__NAME = TYPE_REFERENCE_TAIL_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Field Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_REFERENCE_FEATURE_COUNT = TYPE_REFERENCE_TAIL_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StructFieldDefImpl <em>Struct Field Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StructFieldDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStructFieldDef()
   * @generated
   */
  int STRUCT_FIELD_DEF = 211;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF__NAME = FIELD_REFERENCE__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF__TYPE = FIELD_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Nested Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF__NESTED_TYPE = FIELD_REFERENCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF__ARRAY = FIELD_REFERENCE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF__SPEC = FIELD_REFERENCE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Optional</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF__OPTIONAL = FIELD_REFERENCE_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Struct Field Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCT_FIELD_DEF_FEATURE_COUNT = FIELD_REFERENCE_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SetOfDefImpl <em>Set Of Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SetOfDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSetOfDef()
   * @generated
   */
  int SET_OF_DEF = 212;

  /**
   * The feature id for the '<em><b>Set Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF__SET_LENGTH = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF__TYPE = 1;

  /**
   * The feature id for the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF__NESTED = 2;

  /**
   * The feature id for the '<em><b>Set Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF__SET_SPEC = 3;

  /**
   * The number of structural features of the '<em>Set Of Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SetOfDefNamedImpl <em>Set Of Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SetOfDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSetOfDefNamed()
   * @generated
   */
  int SET_OF_DEF_NAMED = 213;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Set Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_NAMED__SET_LENGTH = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_NAMED__TYPE = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_NAMED__NESTED = REFERENCED_TYPE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Set Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_NAMED__SET_SPEC = REFERENCED_TYPE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Set Of Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_OF_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortDefImpl <em>Port Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortDef()
   * @generated
   */
  int PORT_DEF = 214;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF__BODY = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Port Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortDefBodyImpl <em>Port Def Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortDefBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortDefBody()
   * @generated
   */
  int PORT_DEF_BODY = 215;

  /**
   * The feature id for the '<em><b>Attribs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_BODY__ATTRIBS = 0;

  /**
   * The number of structural features of the '<em>Port Def Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortDefAttribsImpl <em>Port Def Attribs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortDefAttribsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortDefAttribs()
   * @generated
   */
  int PORT_DEF_ATTRIBS = 216;

  /**
   * The feature id for the '<em><b>Message</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_ATTRIBS__MESSAGE = 0;

  /**
   * The feature id for the '<em><b>Procedure</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_ATTRIBS__PROCEDURE = 1;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_ATTRIBS__MIXED = 2;

  /**
   * The number of structural features of the '<em>Port Def Attribs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DEF_ATTRIBS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MixedAttribsImpl <em>Mixed Attribs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MixedAttribsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMixedAttribs()
   * @generated
   */
  int MIXED_ATTRIBS = 217;

  /**
   * The feature id for the '<em><b>Address Decls</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIXED_ATTRIBS__ADDRESS_DECLS = 0;

  /**
   * The feature id for the '<em><b>Mixed Lists</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIXED_ATTRIBS__MIXED_LISTS = 1;

  /**
   * The feature id for the '<em><b>Config Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIXED_ATTRIBS__CONFIG_DEFS = 2;

  /**
   * The number of structural features of the '<em>Mixed Attribs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIXED_ATTRIBS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MixedListImpl <em>Mixed List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MixedListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMixedList()
   * @generated
   */
  int MIXED_LIST = 218;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIXED_LIST__LIST = 0;

  /**
   * The number of structural features of the '<em>Mixed List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIXED_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ProcOrTypeListImpl <em>Proc Or Type List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ProcOrTypeListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getProcOrTypeList()
   * @generated
   */
  int PROC_OR_TYPE_LIST = 219;

  /**
   * The feature id for the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_OR_TYPE_LIST__ALL = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_OR_TYPE_LIST__TYPE = 1;

  /**
   * The number of structural features of the '<em>Proc Or Type List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_OR_TYPE_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ProcOrTypeImpl <em>Proc Or Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ProcOrTypeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getProcOrType()
   * @generated
   */
  int PROC_OR_TYPE = 220;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_OR_TYPE__TYPE = 0;

  /**
   * The number of structural features of the '<em>Proc Or Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_OR_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MessageAttribsImpl <em>Message Attribs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MessageAttribsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMessageAttribs()
   * @generated
   */
  int MESSAGE_ATTRIBS = 221;

  /**
   * The feature id for the '<em><b>Adresses</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_ATTRIBS__ADRESSES = 0;

  /**
   * The feature id for the '<em><b>Messages</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_ATTRIBS__MESSAGES = 1;

  /**
   * The feature id for the '<em><b>Configs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_ATTRIBS__CONFIGS = 2;

  /**
   * The number of structural features of the '<em>Message Attribs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_ATTRIBS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConfigParamDefImpl <em>Config Param Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConfigParamDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConfigParamDef()
   * @generated
   */
  int CONFIG_PARAM_DEF = 222;

  /**
   * The feature id for the '<em><b>Map</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIG_PARAM_DEF__MAP = 0;

  /**
   * The feature id for the '<em><b>Unmap</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIG_PARAM_DEF__UNMAP = 1;

  /**
   * The number of structural features of the '<em>Config Param Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIG_PARAM_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MapParamDefImpl <em>Map Param Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MapParamDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMapParamDef()
   * @generated
   */
  int MAP_PARAM_DEF = 223;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAP_PARAM_DEF__VALUES = 0;

  /**
   * The number of structural features of the '<em>Map Param Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAP_PARAM_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.UnmapParamDefImpl <em>Unmap Param Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.UnmapParamDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getUnmapParamDef()
   * @generated
   */
  int UNMAP_PARAM_DEF = 224;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAP_PARAM_DEF__VALUES = 0;

  /**
   * The number of structural features of the '<em>Unmap Param Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNMAP_PARAM_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AddressDeclImpl <em>Address Decl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AddressDeclImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAddressDecl()
   * @generated
   */
  int ADDRESS_DECL = 225;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDRESS_DECL__TYPE = 0;

  /**
   * The number of structural features of the '<em>Address Decl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDRESS_DECL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ProcedureAttribsImpl <em>Procedure Attribs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ProcedureAttribsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getProcedureAttribs()
   * @generated
   */
  int PROCEDURE_ATTRIBS = 226;

  /**
   * The feature id for the '<em><b>Addresses</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCEDURE_ATTRIBS__ADDRESSES = 0;

  /**
   * The feature id for the '<em><b>Procs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCEDURE_ATTRIBS__PROCS = 1;

  /**
   * The feature id for the '<em><b>Configs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCEDURE_ATTRIBS__CONFIGS = 2;

  /**
   * The number of structural features of the '<em>Procedure Attribs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCEDURE_ATTRIBS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefImpl <em>Component Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentDef()
   * @generated
   */
  int COMPONENT_DEF = 227;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Extends</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF__EXTENDS = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF__DEFS = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Component Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefListImpl <em>Component Def List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentDefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentDefList()
   * @generated
   */
  int COMPONENT_DEF_LIST = 228;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF_LIST__ELEMENT = 0;

  /**
   * The feature id for the '<em><b>Wst</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF_LIST__WST = 1;

  /**
   * The feature id for the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF_LIST__SC = 2;

  /**
   * The number of structural features of the '<em>Component Def List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEF_LIST_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortInstanceImpl <em>Port Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortInstance()
   * @generated
   */
  int PORT_INSTANCE = 229;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_INSTANCE__REF = 0;

  /**
   * The feature id for the '<em><b>Instances</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_INSTANCE__INSTANCES = 1;

  /**
   * The number of structural features of the '<em>Port Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_INSTANCE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PortElementImpl <em>Port Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PortElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPortElement()
   * @generated
   */
  int PORT_ELEMENT = 230;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_ELEMENT__NAME = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_ELEMENT__ARRAY = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Port Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_ELEMENT_FEATURE_COUNT = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComponentElementDefImpl <em>Component Element Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComponentElementDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComponentElementDef()
   * @generated
   */
  int COMPONENT_ELEMENT_DEF = 231;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_ELEMENT_DEF__PORT = 0;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_ELEMENT_DEF__VARIABLE = 1;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_ELEMENT_DEF__TIMER = 2;

  /**
   * The feature id for the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_ELEMENT_DEF__CONST = 3;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_ELEMENT_DEF__TEMPLATE = 4;

  /**
   * The number of structural features of the '<em>Component Element Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_ELEMENT_DEF_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ProcedureListImpl <em>Procedure List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ProcedureListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getProcedureList()
   * @generated
   */
  int PROCEDURE_LIST = 232;

  /**
   * The feature id for the '<em><b>All Or Sig List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCEDURE_LIST__ALL_OR_SIG_LIST = 0;

  /**
   * The number of structural features of the '<em>Procedure List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCEDURE_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllOrSignatureListImpl <em>All Or Signature List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllOrSignatureListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllOrSignatureList()
   * @generated
   */
  int ALL_OR_SIGNATURE_LIST = 233;

  /**
   * The feature id for the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_OR_SIGNATURE_LIST__ALL = 0;

  /**
   * The feature id for the '<em><b>Signature List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST = 1;

  /**
   * The number of structural features of the '<em>All Or Signature List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_OR_SIGNATURE_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SignatureListImpl <em>Signature List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SignatureListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSignatureList()
   * @generated
   */
  int SIGNATURE_LIST = 234;

  /**
   * The feature id for the '<em><b>Sigs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_LIST__SIGS = 0;

  /**
   * The number of structural features of the '<em>Signature List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.EnumDefImpl <em>Enum Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.EnumDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getEnumDef()
   * @generated
   */
  int ENUM_DEF = 235;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DEF__LIST = 0;

  /**
   * The number of structural features of the '<em>Enum Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.EnumDefNamedImpl <em>Enum Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.EnumDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getEnumDefNamed()
   * @generated
   */
  int ENUM_DEF_NAMED = 236;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DEF_NAMED__LIST = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Enum Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedTypeDefImpl <em>Nested Type Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedTypeDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedTypeDef()
   * @generated
   */
  int NESTED_TYPE_DEF = 237;

  /**
   * The number of structural features of the '<em>Nested Type Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_TYPE_DEF_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedRecordDefImpl <em>Nested Record Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedRecordDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedRecordDef()
   * @generated
   */
  int NESTED_RECORD_DEF = 238;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_RECORD_DEF__DEFS = NESTED_TYPE_DEF_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Nested Record Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_RECORD_DEF_FEATURE_COUNT = NESTED_TYPE_DEF_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedUnionDefImpl <em>Nested Union Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedUnionDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedUnionDef()
   * @generated
   */
  int NESTED_UNION_DEF = 239;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_UNION_DEF__DEFS = NESTED_TYPE_DEF_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Nested Union Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_UNION_DEF_FEATURE_COUNT = NESTED_TYPE_DEF_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedSetDefImpl <em>Nested Set Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedSetDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedSetDef()
   * @generated
   */
  int NESTED_SET_DEF = 240;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_SET_DEF__DEFS = NESTED_TYPE_DEF_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Nested Set Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_SET_DEF_FEATURE_COUNT = NESTED_TYPE_DEF_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedRecordOfDefImpl <em>Nested Record Of Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedRecordOfDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedRecordOfDef()
   * @generated
   */
  int NESTED_RECORD_OF_DEF = 241;

  /**
   * The feature id for the '<em><b>Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_RECORD_OF_DEF__LENGTH = NESTED_TYPE_DEF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_RECORD_OF_DEF__TYPE = NESTED_TYPE_DEF_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_RECORD_OF_DEF__NESTED = NESTED_TYPE_DEF_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Nested Record Of Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_RECORD_OF_DEF_FEATURE_COUNT = NESTED_TYPE_DEF_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedSetOfDefImpl <em>Nested Set Of Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedSetOfDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedSetOfDef()
   * @generated
   */
  int NESTED_SET_OF_DEF = 242;

  /**
   * The feature id for the '<em><b>Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_SET_OF_DEF__LENGTH = NESTED_TYPE_DEF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_SET_OF_DEF__TYPE = NESTED_TYPE_DEF_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Nested</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_SET_OF_DEF__NESTED = NESTED_TYPE_DEF_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Nested Set Of Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_SET_OF_DEF_FEATURE_COUNT = NESTED_TYPE_DEF_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NestedEnumDefImpl <em>Nested Enum Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NestedEnumDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNestedEnumDef()
   * @generated
   */
  int NESTED_ENUM_DEF = 243;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_ENUM_DEF__LIST = NESTED_TYPE_DEF_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Nested Enum Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_ENUM_DEF_FEATURE_COUNT = NESTED_TYPE_DEF_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MessageListImpl <em>Message List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MessageListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMessageList()
   * @generated
   */
  int MESSAGE_LIST = 244;

  /**
   * The feature id for the '<em><b>All Or Type List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_LIST__ALL_OR_TYPE_LIST = 0;

  /**
   * The number of structural features of the '<em>Message List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllOrTypeListImpl <em>All Or Type List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllOrTypeListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllOrTypeList()
   * @generated
   */
  int ALL_OR_TYPE_LIST = 245;

  /**
   * The feature id for the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_OR_TYPE_LIST__ALL = 0;

  /**
   * The feature id for the '<em><b>Type List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_OR_TYPE_LIST__TYPE_LIST = 1;

  /**
   * The number of structural features of the '<em>All Or Type List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_OR_TYPE_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TypeListImpl <em>Type List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TypeListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTypeList()
   * @generated
   */
  int TYPE_LIST = 246;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_LIST__TYPES = EXCEPTION_SPEC_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_LIST_FEATURE_COUNT = EXCEPTION_SPEC_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.UnionDefImpl <em>Union Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.UnionDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getUnionDef()
   * @generated
   */
  int UNION_DEF = 247;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF__BODY = 0;

  /**
   * The number of structural features of the '<em>Union Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.UnionDefNamedImpl <em>Union Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.UnionDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getUnionDefNamed()
   * @generated
   */
  int UNION_DEF_NAMED = 248;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF_NAMED__BODY = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Union Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.UnionDefBodyImpl <em>Union Def Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.UnionDefBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getUnionDefBody()
   * @generated
   */
  int UNION_DEF_BODY = 249;

  /**
   * The feature id for the '<em><b>Defs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF_BODY__DEFS = 0;

  /**
   * The number of structural features of the '<em>Union Def Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_DEF_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.EnumerationListImpl <em>Enumeration List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.EnumerationListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getEnumerationList()
   * @generated
   */
  int ENUMERATION_LIST = 250;

  /**
   * The feature id for the '<em><b>Enums</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATION_LIST__ENUMS = 0;

  /**
   * The number of structural features of the '<em>Enumeration List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATION_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.EnumerationImpl <em>Enumeration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.EnumerationImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getEnumeration()
   * @generated
   */
  int ENUMERATION = 251;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATION__NAME = FIELD_REFERENCE__NAME;

  /**
   * The number of structural features of the '<em>Enumeration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUMERATION_FEATURE_COUNT = FIELD_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.UnionFieldDefImpl <em>Union Field Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.UnionFieldDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getUnionFieldDef()
   * @generated
   */
  int UNION_FIELD_DEF = 252;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_FIELD_DEF__NAME = FIELD_REFERENCE__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_FIELD_DEF__TYPE = FIELD_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Nested Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_FIELD_DEF__NESTED_TYPE = FIELD_REFERENCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_FIELD_DEF__ARRAY = FIELD_REFERENCE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Sub Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_FIELD_DEF__SUB_TYPE = FIELD_REFERENCE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Union Field Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_FIELD_DEF_FEATURE_COUNT = FIELD_REFERENCE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SetDefImpl <em>Set Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SetDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSetDef()
   * @generated
   */
  int SET_DEF = 253;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_DEF__BODY = 0;

  /**
   * The number of structural features of the '<em>Set Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SetDefNamedImpl <em>Set Def Named</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SetDefNamedImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSetDefNamed()
   * @generated
   */
  int SET_DEF_NAMED = 254;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_DEF_NAMED__NAME = REFERENCED_TYPE__NAME;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_DEF_NAMED__BODY = REFERENCED_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Set Def Named</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_DEF_NAMED_FEATURE_COUNT = REFERENCED_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SubTypeSpecImpl <em>Sub Type Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SubTypeSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSubTypeSpec()
   * @generated
   */
  int SUB_TYPE_SPEC = 255;

  /**
   * The feature id for the '<em><b>Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_SPEC__LENGTH = 0;

  /**
   * The number of structural features of the '<em>Sub Type Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllowedValuesSpecImpl <em>Allowed Values Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllowedValuesSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllowedValuesSpec()
   * @generated
   */
  int ALLOWED_VALUES_SPEC = 256;

  /**
   * The feature id for the '<em><b>Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALLOWED_VALUES_SPEC__LENGTH = SUB_TYPE_SPEC__LENGTH;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALLOWED_VALUES_SPEC__TEMPLATE = SUB_TYPE_SPEC_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Allowed Values Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALLOWED_VALUES_SPEC_FEATURE_COUNT = SUB_TYPE_SPEC_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateOrRangeImpl <em>Template Or Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateOrRangeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateOrRange()
   * @generated
   */
  int TEMPLATE_OR_RANGE = 257;

  /**
   * The feature id for the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_RANGE__RANGE = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_RANGE__TEMPLATE = 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_RANGE__TYPE = 2;

  /**
   * The number of structural features of the '<em>Template Or Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OR_RANGE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RangeDefImpl <em>Range Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RangeDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRangeDef()
   * @generated
   */
  int RANGE_DEF = 258;

  /**
   * The feature id for the '<em><b>B1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_DEF__B1 = 0;

  /**
   * The feature id for the '<em><b>B2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_DEF__B2 = 1;

  /**
   * The number of structural features of the '<em>Range Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_DEF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BoundImpl <em>Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BoundImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBound()
   * @generated
   */
  int BOUND = 259;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOUND__EXPR = 0;

  /**
   * The number of structural features of the '<em>Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOUND_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExtraMatchingAttributesImpl <em>Extra Matching Attributes</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExtraMatchingAttributesImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExtraMatchingAttributes()
   * @generated
   */
  int EXTRA_MATCHING_ATTRIBUTES = 273;

  /**
   * The number of structural features of the '<em>Extra Matching Attributes</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTRA_MATCHING_ATTRIBUTES_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.StringLengthImpl <em>String Length</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.StringLengthImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getStringLength()
   * @generated
   */
  int STRING_LENGTH = 260;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_LENGTH__EXPR = EXTRA_MATCHING_ATTRIBUTES_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Bound</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_LENGTH__BOUND = EXTRA_MATCHING_ATTRIBUTES_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>String Length</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_LENGTH_FEATURE_COUNT = EXTRA_MATCHING_ATTRIBUTES_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CharStringMatchImpl <em>Char String Match</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CharStringMatchImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCharStringMatch()
   * @generated
   */
  int CHAR_STRING_MATCH = 261;

  /**
   * The feature id for the '<em><b>Length</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHAR_STRING_MATCH__LENGTH = ALLOWED_VALUES_SPEC__LENGTH;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHAR_STRING_MATCH__TEMPLATE = ALLOWED_VALUES_SPEC__TEMPLATE;

  /**
   * The feature id for the '<em><b>Pattern</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHAR_STRING_MATCH__PATTERN = ALLOWED_VALUES_SPEC_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Char String Match</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHAR_STRING_MATCH_FEATURE_COUNT = ALLOWED_VALUES_SPEC_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PatternParticleImpl <em>Pattern Particle</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PatternParticleImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPatternParticle()
   * @generated
   */
  int PATTERN_PARTICLE = 262;

  /**
   * The number of structural features of the '<em>Pattern Particle</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_PARTICLE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl <em>Template Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateDef()
   * @generated
   */
  int TEMPLATE_DEF = 263;

  /**
   * The feature id for the '<em><b>Restriction</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_DEF__RESTRICTION = 0;

  /**
   * The feature id for the '<em><b>Fuzzy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_DEF__FUZZY = 1;

  /**
   * The feature id for the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_DEF__BASE = 2;

  /**
   * The feature id for the '<em><b>Derived</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_DEF__DERIVED = 3;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_DEF__BODY = 4;

  /**
   * The number of structural features of the '<em>Template Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_DEF_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DerivedDefImpl <em>Derived Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DerivedDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDerivedDef()
   * @generated
   */
  int DERIVED_DEF = 264;

  /**
   * The feature id for the '<em><b>Template</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DERIVED_DEF__TEMPLATE = 0;

  /**
   * The number of structural features of the '<em>Derived Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DERIVED_DEF_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BaseTemplateImpl <em>Base Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BaseTemplateImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBaseTemplate()
   * @generated
   */
  int BASE_TEMPLATE = 265;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TEMPLATE__NAME = FUNCTION_REF__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TEMPLATE__TYPE = FUNCTION_REF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Par List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TEMPLATE__PAR_LIST = FUNCTION_REF_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Base Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TEMPLATE_FEATURE_COUNT = FUNCTION_REF_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TempVarListImpl <em>Temp Var List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TempVarListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTempVarList()
   * @generated
   */
  int TEMP_VAR_LIST = 266;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMP_VAR_LIST__VARIABLES = 0;

  /**
   * The number of structural features of the '<em>Temp Var List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMP_VAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerVarInstanceImpl <em>Timer Var Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerVarInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerVarInstance()
   * @generated
   */
  int TIMER_VAR_INSTANCE = 267;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_VAR_INSTANCE__NAME = REF_VALUE__NAME;

  /**
   * The number of structural features of the '<em>Timer Var Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_VAR_INSTANCE_FEATURE_COUNT = REF_VALUE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleVarInstanceImpl <em>Single Var Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleVarInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleVarInstance()
   * @generated
   */
  int SINGLE_VAR_INSTANCE = 268;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VAR_INSTANCE__NAME = TIMER_VAR_INSTANCE__NAME;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VAR_INSTANCE__ARRAY = TIMER_VAR_INSTANCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ac</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VAR_INSTANCE__AC = TIMER_VAR_INSTANCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VAR_INSTANCE__EXPR = TIMER_VAR_INSTANCE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Single Var Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_VAR_INSTANCE_FEATURE_COUNT = TIMER_VAR_INSTANCE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleTempVarInstanceImpl <em>Single Temp Var Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleTempVarInstanceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleTempVarInstance()
   * @generated
   */
  int SINGLE_TEMP_VAR_INSTANCE = 269;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMP_VAR_INSTANCE__NAME = REF_VALUE__NAME;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMP_VAR_INSTANCE__ARRAY = REF_VALUE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ac</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMP_VAR_INSTANCE__AC = REF_VALUE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMP_VAR_INSTANCE__TEMPLATE = REF_VALUE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Single Temp Var Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMP_VAR_INSTANCE_FEATURE_COUNT = REF_VALUE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateInstanceActualParImpl <em>Template Instance Actual Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateInstanceActualParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateInstanceActualPar()
   * @generated
   */
  int TEMPLATE_INSTANCE_ACTUAL_PAR = 300;

  /**
   * The number of structural features of the '<em>Template Instance Actual Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_INSTANCE_ACTUAL_PAR_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.InLineTemplateImpl <em>In Line Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.InLineTemplateImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getInLineTemplate()
   * @generated
   */
  int IN_LINE_TEMPLATE = 270;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN_LINE_TEMPLATE__TYPE = TEMPLATE_INSTANCE_ACTUAL_PAR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Derived</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN_LINE_TEMPLATE__DERIVED = TEMPLATE_INSTANCE_ACTUAL_PAR_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN_LINE_TEMPLATE__TEMPLATE = TEMPLATE_INSTANCE_ACTUAL_PAR_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>In Line Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN_LINE_TEMPLATE_FEATURE_COUNT = TEMPLATE_INSTANCE_ACTUAL_PAR_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DerivedRefWithParListImpl <em>Derived Ref With Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DerivedRefWithParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDerivedRefWithParList()
   * @generated
   */
  int DERIVED_REF_WITH_PAR_LIST = 271;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DERIVED_REF_WITH_PAR_LIST__TEMPLATE = 0;

  /**
   * The number of structural features of the '<em>Derived Ref With Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DERIVED_REF_WITH_PAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateBodyImpl <em>Template Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateBody()
   * @generated
   */
  int TEMPLATE_BODY = 272;

  /**
   * The feature id for the '<em><b>Simple</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_BODY__SIMPLE = 0;

  /**
   * The feature id for the '<em><b>Field</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_BODY__FIELD = 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_BODY__ARRAY = 2;

  /**
   * The feature id for the '<em><b>Extra</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_BODY__EXTRA = 3;

  /**
   * The number of structural features of the '<em>Template Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_BODY_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldSpecListImpl <em>Field Spec List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldSpecListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldSpecList()
   * @generated
   */
  int FIELD_SPEC_LIST = 274;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SPEC_LIST__SPEC = 0;

  /**
   * The number of structural features of the '<em>Field Spec List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SPEC_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldSpecImpl <em>Field Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldSpec()
   * @generated
   */
  int FIELD_SPEC = 275;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SPEC__REF = 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SPEC__BODY = 1;

  /**
   * The number of structural features of the '<em>Field Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SimpleSpecImpl <em>Simple Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SimpleSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSimpleSpec()
   * @generated
   */
  int SIMPLE_SPEC = 276;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_SPEC__EXPR = 0;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_SPEC__SPEC = 1;

  /**
   * The number of structural features of the '<em>Simple Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SimpleTemplateSpecImpl <em>Simple Template Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SimpleTemplateSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSimpleTemplateSpec()
   * @generated
   */
  int SIMPLE_TEMPLATE_SPEC = 277;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_TEMPLATE_SPEC__EXPR = 0;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_TEMPLATE_SPEC__SPEC = 1;

  /**
   * The number of structural features of the '<em>Simple Template Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_TEMPLATE_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleTemplateExpressionImpl <em>Single Template Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleTemplateExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleTemplateExpression()
   * @generated
   */
  int SINGLE_TEMPLATE_EXPRESSION = 278;

  /**
   * The feature id for the '<em><b>Symbol</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMPLATE_EXPRESSION__SYMBOL = 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMPLATE_EXPRESSION__LIST = 1;

  /**
   * The feature id for the '<em><b>Extended</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMPLATE_EXPRESSION__EXTENDED = 2;

  /**
   * The number of structural features of the '<em>Single Template Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_TEMPLATE_EXPRESSION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateRefWithParListImpl <em>Template Ref With Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateRefWithParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateRefWithParList()
   * @generated
   */
  int TEMPLATE_REF_WITH_PAR_LIST = 279;

  /**
   * The feature id for the '<em><b>Template</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REF_WITH_PAR_LIST__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REF_WITH_PAR_LIST__LIST = 1;

  /**
   * The number of structural features of the '<em>Template Ref With Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REF_WITH_PAR_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateActualParListImpl <em>Template Actual Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateActualParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateActualParList()
   * @generated
   */
  int TEMPLATE_ACTUAL_PAR_LIST = 280;

  /**
   * The feature id for the '<em><b>Actual</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_ACTUAL_PAR_LIST__ACTUAL = 0;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_ACTUAL_PAR_LIST__ASSIGN = 1;

  /**
   * The number of structural features of the '<em>Template Actual Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_ACTUAL_PAR_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl <em>Matching Symbol</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMatchingSymbol()
   * @generated
   */
  int MATCHING_SYMBOL = 281;

  /**
   * The feature id for the '<em><b>Complement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__COMPLEMENT = 0;

  /**
   * The feature id for the '<em><b>Qwlm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__QWLM = 1;

  /**
   * The feature id for the '<em><b>Swlm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__SWLM = 2;

  /**
   * The feature id for the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__RANGE = 3;

  /**
   * The feature id for the '<em><b>String</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__STRING = 4;

  /**
   * The feature id for the '<em><b>Subset</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__SUBSET = 5;

  /**
   * The feature id for the '<em><b>Superset</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__SUPERSET = 6;

  /**
   * The feature id for the '<em><b>Templates</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL__TEMPLATES = 7;

  /**
   * The number of structural features of the '<em>Matching Symbol</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCHING_SYMBOL_FEATURE_COUNT = 8;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SubsetMatchImpl <em>Subset Match</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SubsetMatchImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSubsetMatch()
   * @generated
   */
  int SUBSET_MATCH = 282;

  /**
   * The number of structural features of the '<em>Subset Match</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSET_MATCH_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SupersetMatchImpl <em>Superset Match</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SupersetMatchImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSupersetMatch()
   * @generated
   */
  int SUPERSET_MATCH = 283;

  /**
   * The number of structural features of the '<em>Superset Match</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUPERSET_MATCH_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RangeImpl <em>Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RangeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRange()
   * @generated
   */
  int RANGE = 284;

  /**
   * The feature id for the '<em><b>B1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE__B1 = 0;

  /**
   * The feature id for the '<em><b>B2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE__B2 = 1;

  /**
   * The number of structural features of the '<em>Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.WildcardLengthMatchImpl <em>Wildcard Length Match</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.WildcardLengthMatchImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getWildcardLengthMatch()
   * @generated
   */
  int WILDCARD_LENGTH_MATCH = 285;

  /**
   * The number of structural features of the '<em>Wildcard Length Match</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILDCARD_LENGTH_MATCH_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ComplementImpl <em>Complement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ComplementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getComplement()
   * @generated
   */
  int COMPLEMENT = 286;

  /**
   * The number of structural features of the '<em>Complement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ListOfTemplatesImpl <em>List Of Templates</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ListOfTemplatesImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getListOfTemplates()
   * @generated
   */
  int LIST_OF_TEMPLATES = 287;

  /**
   * The feature id for the '<em><b>Items</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_OF_TEMPLATES__ITEMS = SUBSET_MATCH_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>List Of Templates</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_OF_TEMPLATES_FEATURE_COUNT = SUBSET_MATCH_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateOpsImpl <em>Template Ops</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateOpsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateOps()
   * @generated
   */
  int TEMPLATE_OPS = 288;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OPS__TEMPLATE = 0;

  /**
   * The number of structural features of the '<em>Template Ops</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_OPS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MatchOpImpl <em>Match Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MatchOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMatchOp()
   * @generated
   */
  int MATCH_OP = 289;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCH_OP__TEMPLATE = TEMPLATE_OPS__TEMPLATE;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCH_OP__EXPR = TEMPLATE_OPS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Match Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATCH_OP_FEATURE_COUNT = TEMPLATE_OPS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ValueofOpImpl <em>Valueof Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ValueofOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getValueofOp()
   * @generated
   */
  int VALUEOF_OP = 290;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUEOF_OP__TEMPLATE = TEMPLATE_OPS__TEMPLATE;

  /**
   * The number of structural features of the '<em>Valueof Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUEOF_OP_FEATURE_COUNT = TEMPLATE_OPS_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationOpsImpl <em>Configuration Ops</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConfigurationOpsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConfigurationOps()
   * @generated
   */
  int CONFIGURATION_OPS = 291;

  /**
   * The feature id for the '<em><b>Create</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_OPS__CREATE = 0;

  /**
   * The feature id for the '<em><b>Alive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_OPS__ALIVE = 1;

  /**
   * The feature id for the '<em><b>Running</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_OPS__RUNNING = 2;

  /**
   * The number of structural features of the '<em>Configuration Ops</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONFIGURATION_OPS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CreateOpImpl <em>Create Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CreateOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCreateOp()
   * @generated
   */
  int CREATE_OP = 292;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CREATE_OP__TYPE = 0;

  /**
   * The feature id for the '<em><b>Expr1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CREATE_OP__EXPR1 = 1;

  /**
   * The feature id for the '<em><b>Expr2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CREATE_OP__EXPR2 = 2;

  /**
   * The number of structural features of the '<em>Create Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CREATE_OP_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RunningOpImpl <em>Running Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RunningOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRunningOp()
   * @generated
   */
  int RUNNING_OP = 293;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNNING_OP__COMPONENT = 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNNING_OP__INDEX = 1;

  /**
   * The number of structural features of the '<em>Running Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNNING_OP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleExpressionImpl <em>Single Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleExpression()
   * @generated
   */
  int SINGLE_EXPRESSION = 338;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_EXPRESSION__LEFT = WILDCARD_LENGTH_MATCH_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_EXPRESSION__RIGHT = WILDCARD_LENGTH_MATCH_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Single Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_EXPRESSION_FEATURE_COUNT = WILDCARD_LENGTH_MATCH_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl <em>Op Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.OpCallImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getOpCall()
   * @generated
   */
  int OP_CALL = 294;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Configuration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__CONFIGURATION = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Verdict</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__VERDICT = SINGLE_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__TIMER = SINGLE_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__FUNCTION = SINGLE_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Extended Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__EXTENDED_FUNCTION = SINGLE_EXPRESSION_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Pre Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__PRE_FUNCTION = SINGLE_EXPRESSION_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__TESTCASE = SINGLE_EXPRESSION_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Activate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__ACTIVATE = SINGLE_EXPRESSION_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Template Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__TEMPLATE_OPS = SINGLE_EXPRESSION_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Extended Template</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__EXTENDED_TEMPLATE = SINGLE_EXPRESSION_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Field</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL__FIELD = SINGLE_EXPRESSION_FEATURE_COUNT + 10;

  /**
   * The number of structural features of the '<em>Op Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_CALL_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 11;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AliveOpImpl <em>Alive Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AliveOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAliveOp()
   * @generated
   */
  int ALIVE_OP = 295;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIVE_OP__COMPONENT = 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIVE_OP__INDEX = 1;

  /**
   * The number of structural features of the '<em>Alive Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALIVE_OP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateListItemImpl <em>Template List Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateListItemImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateListItem()
   * @generated
   */
  int TEMPLATE_LIST_ITEM = 296;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_LIST_ITEM__BODY = 0;

  /**
   * The feature id for the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_LIST_ITEM__ALL = 1;

  /**
   * The number of structural features of the '<em>Template List Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_LIST_ITEM_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllElementsFromImpl <em>All Elements From</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllElementsFromImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllElementsFrom()
   * @generated
   */
  int ALL_ELEMENTS_FROM = 297;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_ELEMENTS_FROM__BODY = 0;

  /**
   * The number of structural features of the '<em>All Elements From</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_ELEMENTS_FROM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SignatureImpl <em>Signature</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SignatureImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSignature()
   * @generated
   */
  int SIGNATURE = 298;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE__REF = 0;

  /**
   * The number of structural features of the '<em>Signature</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNATURE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateInstanceAssignmentImpl <em>Template Instance Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateInstanceAssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateInstanceAssignment()
   * @generated
   */
  int TEMPLATE_INSTANCE_ASSIGNMENT = 299;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_INSTANCE_ASSIGNMENT__NAME = 0;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_INSTANCE_ASSIGNMENT__TEMPLATE = 1;

  /**
   * The number of structural features of the '<em>Template Instance Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_INSTANCE_ASSIGNMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TemplateRestrictionImpl <em>Template Restriction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TemplateRestrictionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTemplateRestriction()
   * @generated
   */
  int TEMPLATE_RESTRICTION = 301;

  /**
   * The feature id for the '<em><b>Omit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_RESTRICTION__OMIT = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_RESTRICTION__VALUE = 1;

  /**
   * The feature id for the '<em><b>Present</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_RESTRICTION__PRESENT = 2;

  /**
   * The number of structural features of the '<em>Template Restriction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_RESTRICTION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReturnTypeImpl <em>Return Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReturnTypeImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReturnType()
   * @generated
   */
  int RETURN_TYPE = 375;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_TYPE__TYPE = 0;

  /**
   * The number of structural features of the '<em>Return Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RestrictedTemplateImpl <em>Restricted Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RestrictedTemplateImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRestrictedTemplate()
   * @generated
   */
  int RESTRICTED_TEMPLATE = 302;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESTRICTED_TEMPLATE__TYPE = RETURN_TYPE__TYPE;

  /**
   * The feature id for the '<em><b>Restriction</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESTRICTED_TEMPLATE__RESTRICTION = RETURN_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Restricted Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESTRICTED_TEMPLATE_FEATURE_COUNT = RETURN_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.LogStatementImpl <em>Log Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.LogStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getLogStatement()
   * @generated
   */
  int LOG_STATEMENT = 303;

  /**
   * The feature id for the '<em><b>Item</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOG_STATEMENT__ITEM = 0;

  /**
   * The number of structural features of the '<em>Log Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOG_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.LogItemImpl <em>Log Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.LogItemImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getLogItem()
   * @generated
   */
  int LOG_ITEM = 304;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOG_ITEM__TEMPLATE = 0;

  /**
   * The number of structural features of the '<em>Log Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOG_ITEM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.NotUsedOrExpressionImpl <em>Not Used Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.NotUsedOrExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getNotUsedOrExpression()
   * @generated
   */
  int NOT_USED_OR_EXPRESSION = 319;

  /**
   * The number of structural features of the '<em>Not Used Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_USED_OR_EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 305;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = NOT_USED_OR_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.WithStatementImpl <em>With Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.WithStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getWithStatement()
   * @generated
   */
  int WITH_STATEMENT = 306;

  /**
   * The feature id for the '<em><b>Attrib</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITH_STATEMENT__ATTRIB = 0;

  /**
   * The number of structural features of the '<em>With Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITH_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.WithAttribListImpl <em>With Attrib List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.WithAttribListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getWithAttribList()
   * @generated
   */
  int WITH_ATTRIB_LIST = 307;

  /**
   * The feature id for the '<em><b>Multi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITH_ATTRIB_LIST__MULTI = 0;

  /**
   * The number of structural features of the '<em>With Attrib List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITH_ATTRIB_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MultiWithAttribImpl <em>Multi With Attrib</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MultiWithAttribImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMultiWithAttrib()
   * @generated
   */
  int MULTI_WITH_ATTRIB = 308;

  /**
   * The feature id for the '<em><b>Single</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_WITH_ATTRIB__SINGLE = 0;

  /**
   * The number of structural features of the '<em>Multi With Attrib</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_WITH_ATTRIB_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleWithAttribImpl <em>Single With Attrib</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleWithAttribImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleWithAttrib()
   * @generated
   */
  int SINGLE_WITH_ATTRIB = 309;

  /**
   * The feature id for the '<em><b>Attrib</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_WITH_ATTRIB__ATTRIB = 0;

  /**
   * The number of structural features of the '<em>Single With Attrib</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_WITH_ATTRIB_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AttribQualifierImpl <em>Attrib Qualifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AttribQualifierImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAttribQualifier()
   * @generated
   */
  int ATTRIB_QUALIFIER = 310;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ATTRIB_QUALIFIER__LIST = 0;

  /**
   * The number of structural features of the '<em>Attrib Qualifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ATTRIB_QUALIFIER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.IdentifierListImpl <em>Identifier List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.IdentifierListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getIdentifierList()
   * @generated
   */
  int IDENTIFIER_LIST = 311;

  /**
   * The feature id for the '<em><b>Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST__IDS = 0;

  /**
   * The number of structural features of the '<em>Identifier List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.QualifiedIdentifierListImpl <em>Qualified Identifier List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.QualifiedIdentifierListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getQualifiedIdentifierList()
   * @generated
   */
  int QUALIFIED_IDENTIFIER_LIST = 312;

  /**
   * The feature id for the '<em><b>Qids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_IDENTIFIER_LIST__QIDS = 0;

  /**
   * The number of structural features of the '<em>Qualified Identifier List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_IDENTIFIER_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SingleConstDefImpl <em>Single Const Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SingleConstDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSingleConstDef()
   * @generated
   */
  int SINGLE_CONST_DEF = 313;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONST_DEF__NAME = FIELD_REFERENCE__NAME;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONST_DEF__ARRAY = FIELD_REFERENCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Assign</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONST_DEF__ASSIGN = FIELD_REFERENCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONST_DEF__EXPR = FIELD_REFERENCE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Single Const Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_CONST_DEF_FEATURE_COUNT = FIELD_REFERENCE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CompoundExpressionImpl <em>Compound Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CompoundExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCompoundExpression()
   * @generated
   */
  int COMPOUND_EXPRESSION = 314;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOUND_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOUND_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Compound Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOUND_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayExpressionImpl <em>Array Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayExpression()
   * @generated
   */
  int ARRAY_EXPRESSION = 315;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_EXPRESSION__LEFT = COMPOUND_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_EXPRESSION__RIGHT = COMPOUND_EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_EXPRESSION__LIST = COMPOUND_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Array Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_EXPRESSION_FEATURE_COUNT = COMPOUND_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldExpressionListImpl <em>Field Expression List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldExpressionListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldExpressionList()
   * @generated
   */
  int FIELD_EXPRESSION_LIST = 316;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_LIST__LEFT = COMPOUND_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_LIST__RIGHT = COMPOUND_EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Specs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_LIST__SPECS = COMPOUND_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Field Expression List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_LIST_FEATURE_COUNT = COMPOUND_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayElementExpressionListImpl <em>Array Element Expression List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayElementExpressionListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayElementExpressionList()
   * @generated
   */
  int ARRAY_ELEMENT_EXPRESSION_LIST = 317;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_EXPRESSION_LIST__EXPR = 0;

  /**
   * The number of structural features of the '<em>Array Element Expression List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_EXPRESSION_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldExpressionSpecImpl <em>Field Expression Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldExpressionSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldExpressionSpec()
   * @generated
   */
  int FIELD_EXPRESSION_SPEC = 318;

  /**
   * The feature id for the '<em><b>Field Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_SPEC__FIELD_REF = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_SPEC__TYPE = 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_SPEC__ARRAY = 2;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_SPEC__EXPR = 3;

  /**
   * The number of structural features of the '<em>Field Expression Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_SPEC_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConstantExpressionImpl <em>Constant Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConstantExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConstantExpression()
   * @generated
   */
  int CONSTANT_EXPRESSION = 320;

  /**
   * The number of structural features of the '<em>Constant Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.CompoundConstExpressionImpl <em>Compound Const Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.CompoundConstExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getCompoundConstExpression()
   * @generated
   */
  int COMPOUND_CONST_EXPRESSION = 321;

  /**
   * The number of structural features of the '<em>Compound Const Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOUND_CONST_EXPRESSION_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldConstExpressionListImpl <em>Field Const Expression List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldConstExpressionListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldConstExpressionList()
   * @generated
   */
  int FIELD_CONST_EXPRESSION_LIST = 322;

  /**
   * The feature id for the '<em><b>Specs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_LIST__SPECS = COMPOUND_CONST_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Field Const Expression List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_LIST_FEATURE_COUNT = COMPOUND_CONST_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldConstExpressionSpecImpl <em>Field Const Expression Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldConstExpressionSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldConstExpressionSpec()
   * @generated
   */
  int FIELD_CONST_EXPRESSION_SPEC = 323;

  /**
   * The feature id for the '<em><b>Field Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_SPEC__FIELD_REF = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_SPEC__TYPE = 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_SPEC__ARRAY = 2;

  /**
   * The feature id for the '<em><b>Const Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_SPEC__CONST_EXPR = 3;

  /**
   * The number of structural features of the '<em>Field Const Expression Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CONST_EXPRESSION_SPEC_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayConstExpressionImpl <em>Array Const Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayConstExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayConstExpression()
   * @generated
   */
  int ARRAY_CONST_EXPRESSION = 324;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_CONST_EXPRESSION__LIST = COMPOUND_CONST_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Array Const Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_CONST_EXPRESSION_FEATURE_COUNT = COMPOUND_CONST_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayElementConstExpressionListImpl <em>Array Element Const Expression List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayElementConstExpressionListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayElementConstExpressionList()
   * @generated
   */
  int ARRAY_ELEMENT_CONST_EXPRESSION_LIST = 325;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_CONST_EXPRESSION_LIST__EXPR = 0;

  /**
   * The number of structural features of the '<em>Array Element Const Expression List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_CONST_EXPRESSION_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConstListImpl <em>Const List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConstListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConstList()
   * @generated
   */
  int CONST_LIST = 326;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_LIST__LIST = 0;

  /**
   * The number of structural features of the '<em>Const List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONST_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ValueImpl <em>Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ValueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getValue()
   * @generated
   */
  int VALUE = 327;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Predef</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__PREDEF = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__REF = SINGLE_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReferencedValueImpl <em>Referenced Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReferencedValueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReferencedValue()
   * @generated
   */
  int REFERENCED_VALUE = 328;

  /**
   * The feature id for the '<em><b>Head</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_VALUE__HEAD = PATTERN_PARTICLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_VALUE__FIELDS = PATTERN_PARTICLE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Referenced Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_VALUE_FEATURE_COUNT = PATTERN_PARTICLE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RefValueElementImpl <em>Ref Value Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RefValueElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRefValueElement()
   * @generated
   */
  int REF_VALUE_ELEMENT = 330;

  /**
   * The number of structural features of the '<em>Ref Value Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SpecElementImpl <em>Spec Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SpecElementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSpecElement()
   * @generated
   */
  int SPEC_ELEMENT = 333;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPEC_ELEMENT__TAIL = 0;

  /**
   * The number of structural features of the '<em>Spec Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPEC_ELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.HeadImpl <em>Head</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.HeadImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getHead()
   * @generated
   */
  int HEAD = 331;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HEAD__TAIL = SPEC_ELEMENT__TAIL;

  /**
   * The feature id for the '<em><b>Target</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HEAD__TARGET = SPEC_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Head</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HEAD_FEATURE_COUNT = SPEC_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RefValueTailImpl <em>Ref Value Tail</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RefValueTailImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRefValueTail()
   * @generated
   */
  int REF_VALUE_TAIL = 332;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_TAIL__TAIL = SPEC_ELEMENT__TAIL;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_TAIL__VALUE = SPEC_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_TAIL__ARRAY = SPEC_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Ref Value Tail</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REF_VALUE_TAIL_FEATURE_COUNT = SPEC_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ExtendedFieldReferenceImpl <em>Extended Field Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ExtendedFieldReferenceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getExtendedFieldReference()
   * @generated
   */
  int EXTENDED_FIELD_REFERENCE = 334;

  /**
   * The feature id for the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_FIELD_REFERENCE__FIELD = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_FIELD_REFERENCE__TYPE = 1;

  /**
   * The feature id for the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_FIELD_REFERENCE__ARRAY = 2;

  /**
   * The number of structural features of the '<em>Extended Field Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_FIELD_REFERENCE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl <em>Predefined Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPredefinedValue()
   * @generated
   */
  int PREDEFINED_VALUE = 336;

  /**
   * The feature id for the '<em><b>Bstring</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__BSTRING = 0;

  /**
   * The feature id for the '<em><b>Boolean</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__BOOLEAN = 1;

  /**
   * The feature id for the '<em><b>Integer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__INTEGER = 2;

  /**
   * The feature id for the '<em><b>Hstring</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__HSTRING = 3;

  /**
   * The feature id for the '<em><b>Ostring</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__OSTRING = 4;

  /**
   * The feature id for the '<em><b>Verdict Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__VERDICT_TYPE = 5;

  /**
   * The feature id for the '<em><b>Float</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__FLOAT = 6;

  /**
   * The feature id for the '<em><b>Address</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__ADDRESS = 7;

  /**
   * The feature id for the '<em><b>Omit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__OMIT = 8;

  /**
   * The feature id for the '<em><b>Char String</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__CHAR_STRING = 9;

  /**
   * The feature id for the '<em><b>Macro</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE__MACRO = 10;

  /**
   * The number of structural features of the '<em>Predefined Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREDEFINED_VALUE_FEATURE_COUNT = 11;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BooleanExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBooleanExpression()
   * @generated
   */
  int BOOLEAN_EXPRESSION = 337;

  /**
   * The number of structural features of the '<em>Boolean Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefListImpl <em>Def Or Field Ref List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDefOrFieldRefList()
   * @generated
   */
  int DEF_OR_FIELD_REF_LIST = 339;

  /**
   * The feature id for the '<em><b>Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF_LIST__REFS = 0;

  /**
   * The number of structural features of the '<em>Def Or Field Ref List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefImpl <em>Def Or Field Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DefOrFieldRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDefOrFieldRef()
   * @generated
   */
  int DEF_OR_FIELD_REF = 340;

  /**
   * The feature id for the '<em><b>Id</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF__ID = 0;

  /**
   * The feature id for the '<em><b>Field</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF__FIELD = 1;

  /**
   * The feature id for the '<em><b>Extended</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF__EXTENDED = 2;

  /**
   * The feature id for the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF__ALL = 3;

  /**
   * The number of structural features of the '<em>Def Or Field Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEF_OR_FIELD_REF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AllRefImpl <em>All Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AllRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAllRef()
   * @generated
   */
  int ALL_REF = 341;

  /**
   * The feature id for the '<em><b>Group List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_REF__GROUP_LIST = 0;

  /**
   * The feature id for the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_REF__ID_LIST = 1;

  /**
   * The number of structural features of the '<em>All Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_REF_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.GroupRefListImpl <em>Group Ref List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.GroupRefListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getGroupRefList()
   * @generated
   */
  int GROUP_REF_LIST = 342;

  /**
   * The feature id for the '<em><b>Groups</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_REF_LIST__GROUPS = 0;

  /**
   * The number of structural features of the '<em>Group Ref List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_REF_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ReferenceListImpl <em>Reference List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3ReferenceListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTTCN3ReferenceList()
   * @generated
   */
  int TTCN3_REFERENCE_LIST = 344;

  /**
   * The feature id for the '<em><b>Refs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_REFERENCE_LIST__REFS = 0;

  /**
   * The number of structural features of the '<em>Reference List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTCN3_REFERENCE_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayOrBitRefImpl <em>Array Or Bit Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayOrBitRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayOrBitRef()
   * @generated
   */
  int ARRAY_OR_BIT_REF = 346;

  /**
   * The number of structural features of the '<em>Array Or Bit Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_OR_BIT_REF_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FieldOrBitNumberImpl <em>Field Or Bit Number</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FieldOrBitNumberImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFieldOrBitNumber()
   * @generated
   */
  int FIELD_OR_BIT_NUMBER = 347;

  /**
   * The number of structural features of the '<em>Field Or Bit Number</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_OR_BIT_NUMBER_FEATURE_COUNT = ARRAY_OR_BIT_REF_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayValueOrAttribImpl <em>Array Value Or Attrib</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayValueOrAttribImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayValueOrAttrib()
   * @generated
   */
  int ARRAY_VALUE_OR_ATTRIB = 348;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VALUE_OR_ATTRIB__LIST = 0;

  /**
   * The number of structural features of the '<em>Array Value Or Attrib</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VALUE_OR_ATTRIB_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayElementSpecListImpl <em>Array Element Spec List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayElementSpecListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayElementSpecList()
   * @generated
   */
  int ARRAY_ELEMENT_SPEC_LIST = 349;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_SPEC_LIST__SPEC = 0;

  /**
   * The number of structural features of the '<em>Array Element Spec List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_SPEC_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ArrayElementSpecImpl <em>Array Element Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ArrayElementSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getArrayElementSpec()
   * @generated
   */
  int ARRAY_ELEMENT_SPEC = 350;

  /**
   * The feature id for the '<em><b>Match</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_SPEC__MATCH = 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_SPEC__BODY = 1;

  /**
   * The number of structural features of the '<em>Array Element Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ELEMENT_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.TimerOpsImpl <em>Timer Ops</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.TimerOpsImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getTimerOps()
   * @generated
   */
  int TIMER_OPS = 351;

  /**
   * The feature id for the '<em><b>Read</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_OPS__READ = 0;

  /**
   * The feature id for the '<em><b>Run</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_OPS__RUN = 1;

  /**
   * The number of structural features of the '<em>Timer Ops</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_OPS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RunningTimerOpImpl <em>Running Timer Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RunningTimerOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRunningTimerOp()
   * @generated
   */
  int RUNNING_TIMER_OP = 352;

  /**
   * The feature id for the '<em><b>Timer Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNNING_TIMER_OP__TIMER_REF = 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNNING_TIMER_OP__INDEX = 1;

  /**
   * The number of structural features of the '<em>Running Timer Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNNING_TIMER_OP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ReadTimerOpImpl <em>Read Timer Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ReadTimerOpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getReadTimerOp()
   * @generated
   */
  int READ_TIMER_OP = 353;

  /**
   * The feature id for the '<em><b>Timer</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int READ_TIMER_OP__TIMER = 0;

  /**
   * The number of structural features of the '<em>Read Timer Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int READ_TIMER_OP_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PermutationMatchImpl <em>Permutation Match</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PermutationMatchImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPermutationMatch()
   * @generated
   */
  int PERMUTATION_MATCH = 354;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERMUTATION_MATCH__LIST = 0;

  /**
   * The number of structural features of the '<em>Permutation Match</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERMUTATION_MATCH_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.LoopConstructImpl <em>Loop Construct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.LoopConstructImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getLoopConstruct()
   * @generated
   */
  int LOOP_CONSTRUCT = 355;

  /**
   * The feature id for the '<em><b>For Stm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOOP_CONSTRUCT__FOR_STM = 0;

  /**
   * The feature id for the '<em><b>While Stm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOOP_CONSTRUCT__WHILE_STM = 1;

  /**
   * The feature id for the '<em><b>Do Stm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOOP_CONSTRUCT__DO_STM = 2;

  /**
   * The number of structural features of the '<em>Loop Construct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOOP_CONSTRUCT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ForStatementImpl <em>For Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ForStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getForStatement()
   * @generated
   */
  int FOR_STATEMENT = 356;

  /**
   * The feature id for the '<em><b>Init</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__INIT = 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__EXPRESSION = 1;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__ASSIGN = 2;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__STATEMENT = 3;

  /**
   * The number of structural features of the '<em>For Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.WhileStatementImpl <em>While Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.WhileStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getWhileStatement()
   * @generated
   */
  int WHILE_STATEMENT = 357;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT__EXPRESSION = 0;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT__STATEMENT = 1;

  /**
   * The number of structural features of the '<em>While Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.DoWhileStatementImpl <em>Do While Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.DoWhileStatementImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getDoWhileStatement()
   * @generated
   */
  int DO_WHILE_STATEMENT = 358;

  /**
   * The feature id for the '<em><b>Key Do</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DO_WHILE_STATEMENT__KEY_DO = 0;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DO_WHILE_STATEMENT__STATEMENT = 1;

  /**
   * The feature id for the '<em><b>Key While</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DO_WHILE_STATEMENT__KEY_WHILE = 2;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DO_WHILE_STATEMENT__EXPRESSION = 3;

  /**
   * The number of structural features of the '<em>Do While Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DO_WHILE_STATEMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ConditionalConstructImpl <em>Conditional Construct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ConditionalConstructImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getConditionalConstruct()
   * @generated
   */
  int CONDITIONAL_CONSTRUCT = 359;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_CONSTRUCT__EXPRESSION = 0;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_CONSTRUCT__STATEMENT = 1;

  /**
   * The feature id for the '<em><b>Elseifs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_CONSTRUCT__ELSEIFS = 2;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_CONSTRUCT__ELSE = 3;

  /**
   * The number of structural features of the '<em>Conditional Construct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_CONSTRUCT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ElseIfClauseImpl <em>Else If Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ElseIfClauseImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getElseIfClause()
   * @generated
   */
  int ELSE_IF_CLAUSE = 360;

  /**
   * The feature id for the '<em><b>Key Else</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_CLAUSE__KEY_ELSE = 0;

  /**
   * The feature id for the '<em><b>Key If</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_CLAUSE__KEY_IF = 1;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_CLAUSE__EXPRESSION = 2;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_CLAUSE__STATEMENT = 3;

  /**
   * The number of structural features of the '<em>Else If Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_CLAUSE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ElseClauseImpl <em>Else Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ElseClauseImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getElseClause()
   * @generated
   */
  int ELSE_CLAUSE = 361;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_CLAUSE__STATEMENT = 0;

  /**
   * The number of structural features of the '<em>Else Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_CLAUSE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.InitialImpl <em>Initial</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.InitialImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getInitial()
   * @generated
   */
  int INITIAL = 362;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INITIAL__VARIABLE = 0;

  /**
   * The feature id for the '<em><b>Assignment</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INITIAL__ASSIGNMENT = 1;

  /**
   * The number of structural features of the '<em>Initial</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INITIAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SelectCaseConstructImpl <em>Select Case Construct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SelectCaseConstructImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSelectCaseConstruct()
   * @generated
   */
  int SELECT_CASE_CONSTRUCT = 363;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE_CONSTRUCT__EXPRESSION = 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE_CONSTRUCT__BODY = 1;

  /**
   * The number of structural features of the '<em>Select Case Construct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE_CONSTRUCT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SelectCaseBodyImpl <em>Select Case Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SelectCaseBodyImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSelectCaseBody()
   * @generated
   */
  int SELECT_CASE_BODY = 364;

  /**
   * The feature id for the '<em><b>Cases</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE_BODY__CASES = 0;

  /**
   * The number of structural features of the '<em>Select Case Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.SelectCaseImpl <em>Select Case</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.SelectCaseImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getSelectCase()
   * @generated
   */
  int SELECT_CASE = 365;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE__TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Else</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE__ELSE = 1;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE__STATEMENT = 2;

  /**
   * The number of structural features of the '<em>Select Case</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_CASE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl <em>Function Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionDefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionDef()
   * @generated
   */
  int FUNCTION_DEF = 366;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__NAME = FUNCTION_REF__NAME;

  /**
   * The feature id for the '<em><b>Det</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__DET = FUNCTION_REF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Parameter List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__PARAMETER_LIST = FUNCTION_REF_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Runs On</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__RUNS_ON = FUNCTION_REF_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Mtc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__MTC = FUNCTION_REF_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__SYSTEM = FUNCTION_REF_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Return Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__RETURN_TYPE = FUNCTION_REF_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF__STATEMENT = FUNCTION_REF_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>Function Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_DEF_FEATURE_COUNT = FUNCTION_REF_FEATURE_COUNT + 7;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MtcSpecImpl <em>Mtc Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MtcSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMtcSpec()
   * @generated
   */
  int MTC_SPEC = 367;

  /**
   * The feature id for the '<em><b>Component</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MTC_SPEC__COMPONENT = 0;

  /**
   * The number of structural features of the '<em>Mtc Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MTC_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionFormalParListImpl <em>Function Formal Par List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionFormalParListImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionFormalParList()
   * @generated
   */
  int FUNCTION_FORMAL_PAR_LIST = 368;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR_LIST__PARAMS = 0;

  /**
   * The number of structural features of the '<em>Function Formal Par List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FunctionFormalParImpl <em>Function Formal Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FunctionFormalParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunctionFormalPar()
   * @generated
   */
  int FUNCTION_FORMAL_PAR = 369;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR__VALUE = 0;

  /**
   * The feature id for the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR__TIMER = 1;

  /**
   * The feature id for the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR__TEMPLATE = 2;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR__PORT = 3;

  /**
   * The number of structural features of the '<em>Function Formal Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FORMAL_PAR_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FormalValueParImpl <em>Formal Value Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FormalValueParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFormalValuePar()
   * @generated
   */
  int FORMAL_VALUE_PAR = 370;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_VALUE_PAR__NAME = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>In Out</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_VALUE_PAR__IN_OUT = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_VALUE_PAR__MOD = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_VALUE_PAR__TYPE = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_VALUE_PAR__EXPRESSION = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Formal Value Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_VALUE_PAR_FEATURE_COUNT = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FormalTimerParImpl <em>Formal Timer Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FormalTimerParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFormalTimerPar()
   * @generated
   */
  int FORMAL_TIMER_PAR = 371;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TIMER_PAR__NAME = TIMER_VAR_INSTANCE__NAME;

  /**
   * The number of structural features of the '<em>Formal Timer Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TIMER_PAR_FEATURE_COUNT = TIMER_VAR_INSTANCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FormalPortParImpl <em>Formal Port Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FormalPortParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFormalPortPar()
   * @generated
   */
  int FORMAL_PORT_PAR = 372;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_PORT_PAR__NAME = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_PORT_PAR__PORT = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Formal Port Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_PORT_PAR_FEATURE_COUNT = FORMAL_PORT_AND_VALUE_PAR_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl <em>Formal Template Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFormalTemplatePar()
   * @generated
   */
  int FORMAL_TEMPLATE_PAR = 373;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR__NAME = REF_VALUE__NAME;

  /**
   * The feature id for the '<em><b>In Out</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR__IN_OUT = REF_VALUE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Restriction</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR__RESTRICTION = REF_VALUE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR__MOD = REF_VALUE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR__TYPE = REF_VALUE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Templ</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR__TEMPL = REF_VALUE_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Formal Template Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMAL_TEMPLATE_PAR_FEATURE_COUNT = REF_VALUE_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RunsOnSpecImpl <em>Runs On Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RunsOnSpecImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRunsOnSpec()
   * @generated
   */
  int RUNS_ON_SPEC = 374;

  /**
   * The feature id for the '<em><b>Component</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNS_ON_SPEC__COMPONENT = 0;

  /**
   * The number of structural features of the '<em>Runs On Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUNS_ON_SPEC_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AssignmentImpl <em>Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AssignmentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAssignment()
   * @generated
   */
  int ASSIGNMENT = 376;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT__REF = 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT__EXPRESSION = 1;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT__BODY = 2;

  /**
   * The feature id for the '<em><b>Extra</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT__EXTRA = 3;

  /**
   * The number of structural features of the '<em>Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.VariableRefImpl <em>Variable Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.VariableRefImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVariableRef()
   * @generated
   */
  int VARIABLE_REF = 377;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_REF__REF = VARIABLE_ENTRY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_REF_FEATURE_COUNT = VARIABLE_ENTRY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.PreDefFunctionImpl <em>Pre Def Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.PreDefFunctionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getPreDefFunction()
   * @generated
   */
  int PRE_DEF_FUNCTION = 378;

  /**
   * The number of structural features of the '<em>Pre Def Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRE_DEF_FUNCTION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2charImpl <em>Fint2char</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2charImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2char()
   * @generated
   */
  int FINT2CHAR = 379;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2CHAR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fint2char</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2CHAR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2unicharImpl <em>Fint2unichar</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2unicharImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2unichar()
   * @generated
   */
  int FINT2UNICHAR = 380;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2UNICHAR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fint2unichar</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2UNICHAR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2bitImpl <em>Fint2bit</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2bitImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2bit()
   * @generated
   */
  int FINT2BIT = 381;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2BIT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2BIT__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Fint2bit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2BIT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2enumImpl <em>Fint2enum</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2enumImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2enum()
   * @generated
   */
  int FINT2ENUM = 382;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2ENUM__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2ENUM__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Fint2enum</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2ENUM_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2hexImpl <em>Fint2hex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2hexImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2hex()
   * @generated
   */
  int FINT2HEX = 383;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2HEX__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2HEX__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Fint2hex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2HEX_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2octImpl <em>Fint2oct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2octImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2oct()
   * @generated
   */
  int FINT2OCT = 384;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2OCT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2OCT__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Fint2oct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2OCT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2strImpl <em>Fint2str</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2strImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2str()
   * @generated
   */
  int FINT2STR = 385;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2STR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fint2str</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2STR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fint2floatImpl <em>Fint2float</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fint2floatImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFint2float()
   * @generated
   */
  int FINT2FLOAT = 386;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2FLOAT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fint2float</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FINT2FLOAT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Ffloat2intImpl <em>Ffloat2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Ffloat2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFfloat2int()
   * @generated
   */
  int FFLOAT2INT = 387;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FFLOAT2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ffloat2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FFLOAT2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fchar2intImpl <em>Fchar2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fchar2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFchar2int()
   * @generated
   */
  int FCHAR2INT = 388;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FCHAR2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fchar2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FCHAR2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fchar2octImpl <em>Fchar2oct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fchar2octImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFchar2oct()
   * @generated
   */
  int FCHAR2OCT = 389;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FCHAR2OCT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fchar2oct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FCHAR2OCT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Funichar2intImpl <em>Funichar2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Funichar2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunichar2int()
   * @generated
   */
  int FUNICHAR2INT = 390;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNICHAR2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Funichar2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNICHAR2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Funichar2octImpl <em>Funichar2oct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Funichar2octImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFunichar2oct()
   * @generated
   */
  int FUNICHAR2OCT = 391;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNICHAR2OCT__EXPR = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Funichar2oct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNICHAR2OCT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fbit2intImpl <em>Fbit2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fbit2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFbit2int()
   * @generated
   */
  int FBIT2INT = 392;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fbit2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fbit2hexImpl <em>Fbit2hex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fbit2hexImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFbit2hex()
   * @generated
   */
  int FBIT2HEX = 393;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2HEX__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fbit2hex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2HEX_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fbit2octImpl <em>Fbit2oct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fbit2octImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFbit2oct()
   * @generated
   */
  int FBIT2OCT = 394;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2OCT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fbit2oct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2OCT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fbit2strImpl <em>Fbit2str</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fbit2strImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFbit2str()
   * @generated
   */
  int FBIT2STR = 395;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2STR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fbit2str</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBIT2STR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fhex2intImpl <em>Fhex2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fhex2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFhex2int()
   * @generated
   */
  int FHEX2INT = 396;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fhex2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fhex2bitImpl <em>Fhex2bit</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fhex2bitImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFhex2bit()
   * @generated
   */
  int FHEX2BIT = 397;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2BIT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fhex2bit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2BIT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fhex2octImpl <em>Fhex2oct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fhex2octImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFhex2oct()
   * @generated
   */
  int FHEX2OCT = 398;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2OCT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fhex2oct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2OCT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fhex2strImpl <em>Fhex2str</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fhex2strImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFhex2str()
   * @generated
   */
  int FHEX2STR = 399;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2STR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fhex2str</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FHEX2STR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Foct2intImpl <em>Foct2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Foct2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFoct2int()
   * @generated
   */
  int FOCT2INT = 400;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Foct2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Foct2bitImpl <em>Foct2bit</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Foct2bitImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFoct2bit()
   * @generated
   */
  int FOCT2BIT = 401;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2BIT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Foct2bit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2BIT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Foct2hexImpl <em>Foct2hex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Foct2hexImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFoct2hex()
   * @generated
   */
  int FOCT2HEX = 402;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2HEX__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Foct2hex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2HEX_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Foct2strImpl <em>Foct2str</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Foct2strImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFoct2str()
   * @generated
   */
  int FOCT2STR = 403;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2STR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Foct2str</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2STR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Foct2charImpl <em>Foct2char</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Foct2charImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFoct2char()
   * @generated
   */
  int FOCT2CHAR = 404;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2CHAR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Foct2char</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2CHAR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Foct2unicharImpl <em>Foct2unichar</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Foct2unicharImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFoct2unichar()
   * @generated
   */
  int FOCT2UNICHAR = 405;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2UNICHAR__EXPR = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Foct2unichar</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOCT2UNICHAR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fstr2intImpl <em>Fstr2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fstr2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFstr2int()
   * @generated
   */
  int FSTR2INT = 406;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fstr2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fstr2hexImpl <em>Fstr2hex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fstr2hexImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFstr2hex()
   * @generated
   */
  int FSTR2HEX = 407;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2HEX__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fstr2hex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2HEX_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fstr2octImpl <em>Fstr2oct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fstr2octImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFstr2oct()
   * @generated
   */
  int FSTR2OCT = 408;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2OCT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fstr2oct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2OCT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fstr2floatImpl <em>Fstr2float</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fstr2floatImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFstr2float()
   * @generated
   */
  int FSTR2FLOAT = 409;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2FLOAT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fstr2float</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSTR2FLOAT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.Fenum2intImpl <em>Fenum2int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.Fenum2intImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFenum2int()
   * @generated
   */
  int FENUM2INT = 410;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENUM2INT__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fenum2int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENUM2INT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FlengthofImpl <em>Flengthof</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FlengthofImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFlengthof()
   * @generated
   */
  int FLENGTHOF = 411;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLENGTHOF__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Flengthof</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLENGTHOF_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FsizeofImpl <em>Fsizeof</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FsizeofImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFsizeof()
   * @generated
   */
  int FSIZEOF = 412;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSIZEOF__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fsizeof</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSIZEOF_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FispresentImpl <em>Fispresent</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FispresentImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFispresent()
   * @generated
   */
  int FISPRESENT = 413;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISPRESENT__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fispresent</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISPRESENT_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FischosenImpl <em>Fischosen</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FischosenImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFischosen()
   * @generated
   */
  int FISCHOSEN = 414;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISCHOSEN__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fischosen</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISCHOSEN_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FisvalueImpl <em>Fisvalue</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FisvalueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFisvalue()
   * @generated
   */
  int FISVALUE = 415;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISVALUE__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fisvalue</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISVALUE_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FisboundImpl <em>Fisbound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FisboundImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFisbound()
   * @generated
   */
  int FISBOUND = 416;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISBOUND__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fisbound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FISBOUND_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FregexpImpl <em>Fregexp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FregexpImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFregexp()
   * @generated
   */
  int FREGEXP = 417;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREGEXP__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREGEXP__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREGEXP__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Fregexp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREGEXP_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FsubstrImpl <em>Fsubstr</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FsubstrImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFsubstr()
   * @generated
   */
  int FSUBSTR = 418;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSUBSTR__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSUBSTR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSUBSTR__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Fsubstr</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FSUBSTR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FreplaceImpl <em>Freplace</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FreplaceImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFreplace()
   * @generated
   */
  int FREPLACE = 419;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREPLACE__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREPLACE__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>E3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREPLACE__E3 = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>E4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREPLACE__E4 = PRE_DEF_FUNCTION_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Freplace</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FREPLACE_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FencvalueImpl <em>Fencvalue</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FencvalueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFencvalue()
   * @generated
   */
  int FENCVALUE = 420;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENCVALUE__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Fencvalue</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENCVALUE_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FdecvalueImpl <em>Fdecvalue</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FdecvalueImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFdecvalue()
   * @generated
   */
  int FDECVALUE = 421;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Fdecvalue</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FencvalueUnicharImpl <em>Fencvalue Unichar</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FencvalueUnicharImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFencvalueUnichar()
   * @generated
   */
  int FENCVALUE_UNICHAR = 422;

  /**
   * The feature id for the '<em><b>T1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENCVALUE_UNICHAR__T1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENCVALUE_UNICHAR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Fencvalue Unichar</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FENCVALUE_UNICHAR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FdecvalueUnicharImpl <em>Fdecvalue Unichar</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FdecvalueUnicharImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFdecvalueUnichar()
   * @generated
   */
  int FDECVALUE_UNICHAR = 423;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE_UNICHAR__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>E2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE_UNICHAR__E2 = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>E3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE_UNICHAR__E3 = PRE_DEF_FUNCTION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Fdecvalue Unichar</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FDECVALUE_UNICHAR_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FrndImpl <em>Frnd</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FrndImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFrnd()
   * @generated
   */
  int FRND = 424;

  /**
   * The feature id for the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FRND__E1 = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Frnd</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FRND_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.FtestcasenameImpl <em>Ftestcasename</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.FtestcasenameImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getFtestcasename()
   * @generated
   */
  int FTESTCASENAME = 425;

  /**
   * The number of structural features of the '<em>Ftestcasename</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FTESTCASENAME_FEATURE_COUNT = PRE_DEF_FUNCTION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.XorExpressionImpl <em>Xor Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.XorExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getXorExpression()
   * @generated
   */
  int XOR_EXPRESSION = 426;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Xor Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AndExpressionImpl <em>And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AndExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAndExpression()
   * @generated
   */
  int AND_EXPRESSION = 427;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.EqualExpressionImpl <em>Equal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.EqualExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getEqualExpression()
   * @generated
   */
  int EQUAL_EXPRESSION = 428;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUAL_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUAL_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Equal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUAL_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.RelExpressionImpl <em>Rel Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.RelExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getRelExpression()
   * @generated
   */
  int REL_EXPRESSION = 429;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Rel Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.ShiftExpressionImpl <em>Shift Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.ShiftExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getShiftExpression()
   * @generated
   */
  int SHIFT_EXPRESSION = 430;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Shift Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SHIFT_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BitOrExpressionImpl <em>Bit Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BitOrExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBitOrExpression()
   * @generated
   */
  int BIT_OR_EXPRESSION = 431;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_OR_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_OR_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Bit Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_OR_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BitXorExpressionImpl <em>Bit Xor Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BitXorExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBitXorExpression()
   * @generated
   */
  int BIT_XOR_EXPRESSION = 432;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_XOR_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_XOR_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Bit Xor Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_XOR_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.BitAndExpressionImpl <em>Bit And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.BitAndExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getBitAndExpression()
   * @generated
   */
  int BIT_AND_EXPRESSION = 433;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_AND_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_AND_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Bit And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_AND_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.AddExpressionImpl <em>Add Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.AddExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getAddExpression()
   * @generated
   */
  int ADD_EXPRESSION = 434;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Add Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.impl.MulExpressionImpl <em>Mul Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.impl.MulExpressionImpl
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getMulExpression()
   * @generated
   */
  int MUL_EXPRESSION = 435;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION__LEFT = SINGLE_EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION__RIGHT = SINGLE_EXPRESSION__RIGHT;

  /**
   * The number of structural features of the '<em>Mul Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION_FEATURE_COUNT = SINGLE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.Visibility <em>Visibility</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.Visibility
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVisibility()
   * @generated
   */
  int VISIBILITY = 436;

  /**
   * The meta object id for the '{@link de.ugoe.cs.swe.tTCN3.VerdictTypeValue <em>Verdict Type Value</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.ugoe.cs.swe.tTCN3.VerdictTypeValue
   * @see de.ugoe.cs.swe.tTCN3.impl.TTCN3PackageImpl#getVerdictTypeValue()
   * @generated
   */
  int VERDICT_TYPE_VALUE = 437;


  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TTCN3File <em>File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>File</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3File
   * @generated
   */
  EClass getTTCN3File();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TTCN3File#getModules <em>Modules</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modules</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3File#getModules()
   * @see #getTTCN3File()
   * @generated
   */
  EReference getTTCN3File_Modules();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConstDef <em>Const Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Const Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConstDef
   * @generated
   */
  EClass getConstDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConstDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConstDef#getType()
   * @see #getConstDef()
   * @generated
   */
  EReference getConstDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConstDef#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConstDef#getDefs()
   * @see #getConstDef()
   * @generated
   */
  EReference getConstDef_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.Type#getPre <em>Pre</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Pre</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Type#getPre()
   * @see #getType()
   * @generated
   */
  EAttribute getType_Pre();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Type#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Type#getRef()
   * @see #getType()
   * @generated
   */
  EReference getType_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.Type#getExtensions <em>Extensions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Extensions</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Type#getExtensions()
   * @see #getType()
   * @generated
   */
  EReference getType_Extensions();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTailType <em>Type Reference Tail Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Reference Tail Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeReferenceTailType
   * @generated
   */
  EClass getTypeReferenceTailType();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail <em>Type Reference Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Reference Tail</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeReferenceTail
   * @generated
   */
  EClass getTypeReferenceTail();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getType()
   * @see #getTypeReferenceTail()
   * @generated
   */
  EReference getTypeReferenceTail_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeReferenceTail#getArray()
   * @see #getTypeReferenceTail()
   * @generated
   */
  EReference getTypeReferenceTail_Array();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TypeReference <em>Type Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Reference</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeReference
   * @generated
   */
  EClass getTypeReference();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TypeReference#getHead <em>Head</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Head</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeReference#getHead()
   * @see #getTypeReference()
   * @generated
   */
  EReference getTypeReference_Head();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SpecTypeElement <em>Spec Type Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Spec Type Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SpecTypeElement
   * @generated
   */
  EClass getSpecTypeElement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SpecTypeElement#getTail <em>Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tail</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SpecTypeElement#getTail()
   * @see #getSpecTypeElement()
   * @generated
   */
  EReference getSpecTypeElement_Tail();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayDef <em>Array Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayDef
   * @generated
   */
  EClass getArrayDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ArrayDef#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayDef#getExpr()
   * @see #getArrayDef()
   * @generated
   */
  EReference getArrayDef_Expr();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ArrayDef#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Range</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayDef#getRange()
   * @see #getArrayDef()
   * @generated
   */
  EReference getArrayDef_Range();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module <em>Module</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module
   * @generated
   */
  EClass getTTCN3Module();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module#getSpec()
   * @see #getTTCN3Module()
   * @generated
   */
  EReference getTTCN3Module_Spec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module#getDefs()
   * @see #getTTCN3Module()
   * @generated
   */
  EReference getTTCN3Module_Defs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getControls <em>Controls</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Controls</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module#getControls()
   * @see #getTTCN3Module()
   * @generated
   */
  EReference getTTCN3Module_Controls();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getWithstm <em>Withstm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Withstm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module#getWithstm()
   * @see #getTTCN3Module()
   * @generated
   */
  EReference getTTCN3Module_Withstm();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TTCN3Module#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Module#getSc()
   * @see #getTTCN3Module()
   * @generated
   */
  EAttribute getTTCN3Module_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.LanguageSpec <em>Language Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Language Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LanguageSpec
   * @generated
   */
  EClass getLanguageSpec();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.LanguageSpec#getTxt <em>Txt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Txt</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LanguageSpec#getTxt()
   * @see #getLanguageSpec()
   * @generated
   */
  EAttribute getLanguageSpec_Txt();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleParDef <em>Module Par Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Par Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParDef
   * @generated
   */
  EClass getModuleParDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleParDef#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParDef#getParam()
   * @see #getModuleParDef()
   * @generated
   */
  EReference getModuleParDef_Param();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleParDef#getMultitypeParam <em>Multitype Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Multitype Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParDef#getMultitypeParam()
   * @see #getModuleParDef()
   * @generated
   */
  EReference getModuleParDef_MultitypeParam();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModulePar <em>Module Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModulePar
   * @generated
   */
  EClass getModulePar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModulePar#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModulePar#getType()
   * @see #getModulePar()
   * @generated
   */
  EReference getModulePar_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModulePar#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModulePar#getList()
   * @see #getModulePar()
   * @generated
   */
  EReference getModulePar_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleParList <em>Module Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParList
   * @generated
   */
  EClass getModuleParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ModuleParList#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParList#getParams()
   * @see #getModuleParList()
   * @generated
   */
  EReference getModuleParList_Params();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleParameter <em>Module Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Parameter</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParameter
   * @generated
   */
  EClass getModuleParameter();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleParameter#getConstExpr <em>Const Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleParameter#getConstExpr()
   * @see #getModuleParameter()
   * @generated
   */
  EReference getModuleParameter_ConstExpr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MultitypedModuleParList <em>Multityped Module Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multityped Module Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MultitypedModuleParList
   * @generated
   */
  EClass getMultitypedModuleParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MultitypedModuleParList#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MultitypedModuleParList#getParams()
   * @see #getMultitypedModuleParList()
   * @generated
   */
  EReference getMultitypedModuleParList_Params();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList <em>Module Definitions List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Definitions List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList
   * @generated
   */
  EClass getModuleDefinitionsList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList#getDefinitions <em>Definitions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Definitions</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList#getDefinitions()
   * @see #getModuleDefinitionsList()
   * @generated
   */
  EReference getModuleDefinitionsList_Definitions();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition <em>Module Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Definition</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition
   * @generated
   */
  EClass getModuleDefinition();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getVisibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Visibility</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition#getVisibility()
   * @see #getModuleDefinition()
   * @generated
   */
  EAttribute getModuleDefinition_Visibility();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getDef <em>Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition#getDef()
   * @see #getModuleDefinition()
   * @generated
   */
  EReference getModuleDefinition_Def();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPublicGroup <em>Public Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Public Group</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPublicGroup()
   * @see #getModuleDefinition()
   * @generated
   */
  EAttribute getModuleDefinition_PublicGroup();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPrivateFriend <em>Private Friend</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Private Friend</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition#getPrivateFriend()
   * @see #getModuleDefinition()
   * @generated
   */
  EAttribute getModuleDefinition_PrivateFriend();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleDefinition#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleDefinition#getStatement()
   * @see #getModuleDefinition()
   * @generated
   */
  EReference getModuleDefinition_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FriendModuleDef <em>Friend Module Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Friend Module Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FriendModuleDef
   * @generated
   */
  EClass getFriendModuleDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FriendModuleDef#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FriendModuleDef#getId()
   * @see #getFriendModuleDef()
   * @generated
   */
  EReference getFriendModuleDef_Id();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GroupDef <em>Group Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Group Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GroupDef
   * @generated
   */
  EClass getGroupDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GroupDef#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GroupDef#getList()
   * @see #getGroupDef()
   * @generated
   */
  EReference getGroupDef_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef <em>Ext Function Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ext Function Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtFunctionDef
   * @generated
   */
  EClass getExtFunctionDef();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getDet <em>Det</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Det</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getDet()
   * @see #getExtFunctionDef()
   * @generated
   */
  EAttribute getExtFunctionDef_Det();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getList()
   * @see #getExtFunctionDef()
   * @generated
   */
  EReference getExtFunctionDef_List();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getReturn <em>Return</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getReturn()
   * @see #getExtFunctionDef()
   * @generated
   */
  EReference getExtFunctionDef_Return();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExtConstDef <em>Ext Const Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ext Const Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtConstDef
   * @generated
   */
  EClass getExtConstDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExtConstDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtConstDef#getType()
   * @see #getExtConstDef()
   * @generated
   */
  EReference getExtConstDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExtConstDef#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtConstDef#getId()
   * @see #getExtConstDef()
   * @generated
   */
  EReference getExtConstDef_Id();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.IdentifierObjectList <em>Identifier Object List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier Object List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierObjectList
   * @generated
   */
  EClass getIdentifierObjectList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.IdentifierObjectList#getIds <em>Ids</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ids</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierObjectList#getIds()
   * @see #getIdentifierObjectList()
   * @generated
   */
  EReference getIdentifierObjectList_Ids();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NamedObject <em>Named Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Named Object</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NamedObject
   * @generated
   */
  EClass getNamedObject();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportDef <em>Import Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportDef
   * @generated
   */
  EClass getImportDef();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportDef#getName()
   * @see #getImportDef()
   * @generated
   */
  EAttribute getImportDef_Name();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportDef#getSpec()
   * @see #getImportDef()
   * @generated
   */
  EReference getImportDef_Spec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportDef#getAll()
   * @see #getImportDef()
   * @generated
   */
  EReference getImportDef_All();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportDef#getImportSpec <em>Import Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Import Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportDef#getImportSpec()
   * @see #getImportDef()
   * @generated
   */
  EReference getImportDef_ImportSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllWithExcepts <em>All With Excepts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All With Excepts</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllWithExcepts
   * @generated
   */
  EClass getAllWithExcepts();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllWithExcepts#getDef <em>Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllWithExcepts#getDef()
   * @see #getAllWithExcepts()
   * @generated
   */
  EReference getAllWithExcepts_Def();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptsDef <em>Excepts Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Excepts Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptsDef
   * @generated
   */
  EClass getExceptsDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptsDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptsDef#getSpec()
   * @see #getExceptsDef()
   * @generated
   */
  EReference getExceptsDef_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptSpec <em>Except Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptSpec
   * @generated
   */
  EClass getExceptSpec();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ExceptSpec#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptSpec#getElement()
   * @see #getExceptSpec()
   * @generated
   */
  EReference getExceptSpec_Element();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptElement <em>Except Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement
   * @generated
   */
  EClass getExceptElement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Group</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getGroup()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Group();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getType()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getTemplate()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getConst()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Const();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getTestcase <em>Testcase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Testcase</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getTestcase()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Testcase();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getAltstep <em>Altstep</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Altstep</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getAltstep()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Altstep();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getFunction()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Function();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getSignature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Signature</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getSignature()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_Signature();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptElement#getModulePar <em>Module Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Module Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptElement#getModulePar()
   * @see #getExceptElement()
   * @generated
   */
  EReference getExceptElement_ModulePar();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAll <em>Identifier List Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier List Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAll
   * @generated
   */
  EClass getIdentifierListOrAll();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAll#getIdList <em>Id List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAll#getIdList()
   * @see #getIdentifierListOrAll()
   * @generated
   */
  EReference getIdentifierListOrAll_IdList();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAll#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAll#getAll()
   * @see #getIdentifierListOrAll()
   * @generated
   */
  EAttribute getIdentifierListOrAll_All();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec <em>Except Group Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Group Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptGroupSpec
   * @generated
   */
  EClass getExceptGroupSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getIdList <em>Id List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getIdList()
   * @see #getExceptGroupSpec()
   * @generated
   */
  EReference getExceptGroupSpec_IdList();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getAll()
   * @see #getExceptGroupSpec()
   * @generated
   */
  EAttribute getExceptGroupSpec_All();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec <em>Except Type Def Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Type Def Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec
   * @generated
   */
  EClass getExceptTypeDefSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec#getIdOrAll()
   * @see #getExceptTypeDefSpec()
   * @generated
   */
  EReference getExceptTypeDefSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec <em>Except Template Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Template Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec
   * @generated
   */
  EClass getExceptTemplateSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec#getIdOrAll()
   * @see #getExceptTemplateSpec()
   * @generated
   */
  EReference getExceptTemplateSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptConstSpec <em>Except Const Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Const Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptConstSpec
   * @generated
   */
  EClass getExceptConstSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptConstSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptConstSpec#getIdOrAll()
   * @see #getExceptConstSpec()
   * @generated
   */
  EReference getExceptConstSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec <em>Except Testcase Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Testcase Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec
   * @generated
   */
  EClass getExceptTestcaseSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec#getIdOrAll()
   * @see #getExceptTestcaseSpec()
   * @generated
   */
  EReference getExceptTestcaseSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec <em>Except Altstep Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Altstep Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec
   * @generated
   */
  EClass getExceptAltstepSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec#getIdOrAll()
   * @see #getExceptAltstepSpec()
   * @generated
   */
  EReference getExceptAltstepSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec <em>Except Function Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Function Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec
   * @generated
   */
  EClass getExceptFunctionSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec#getIdOrAll()
   * @see #getExceptFunctionSpec()
   * @generated
   */
  EReference getExceptFunctionSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec <em>Except Signature Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Signature Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec
   * @generated
   */
  EClass getExceptSignatureSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec#getIdOrAll()
   * @see #getExceptSignatureSpec()
   * @generated
   */
  EReference getExceptSignatureSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec <em>Except Module Par Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Except Module Par Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec
   * @generated
   */
  EClass getExceptModuleParSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec#getIdOrAll()
   * @see #getExceptModuleParSpec()
   * @generated
   */
  EReference getExceptModuleParSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportSpec <em>Import Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportSpec
   * @generated
   */
  EClass getImportSpec();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ImportSpec#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportSpec#getElement()
   * @see #getImportSpec()
   * @generated
   */
  EReference getImportSpec_Element();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportElement <em>Import Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement
   * @generated
   */
  EClass getImportElement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Group</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getGroup()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Group();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getType()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getTemplate()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getConst()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Const();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getTestcase <em>Testcase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Testcase</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getTestcase()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Testcase();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getAltstep <em>Altstep</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Altstep</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getAltstep()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Altstep();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getFunction()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Function();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getSignature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Signature</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getSignature()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_Signature();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getModulePar <em>Module Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Module Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getModulePar()
   * @see #getImportElement()
   * @generated
   */
  EReference getImportElement_ModulePar();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getImportSpec <em>Import Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Import Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportElement#getImportSpec()
   * @see #getImportElement()
   * @generated
   */
  EAttribute getImportElement_ImportSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec <em>Import Group Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Group Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportGroupSpec
   * @generated
   */
  EClass getImportGroupSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroupRef <em>Group Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Group Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroupRef()
   * @see #getImportGroupSpec()
   * @generated
   */
  EReference getImportGroupSpec_GroupRef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Group</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportGroupSpec#getGroup()
   * @see #getImportGroupSpec()
   * @generated
   */
  EReference getImportGroupSpec_Group();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept <em>Identifier List Or All With Except</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier List Or All With Except</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept
   * @generated
   */
  EClass getIdentifierListOrAllWithExcept();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getIdList <em>Id List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getIdList()
   * @see #getIdentifierListOrAllWithExcept()
   * @generated
   */
  EReference getIdentifierListOrAllWithExcept_IdList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept#getAll()
   * @see #getIdentifierListOrAllWithExcept()
   * @generated
   */
  EReference getIdentifierListOrAllWithExcept_All();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllWithExcept <em>All With Except</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All With Except</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllWithExcept
   * @generated
   */
  EClass getAllWithExcept();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllWithExcept#getIdList <em>Id List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllWithExcept#getIdList()
   * @see #getAllWithExcept()
   * @generated
   */
  EReference getAllWithExcept_IdList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec <em>Import Type Def Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Type Def Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec
   * @generated
   */
  EClass getImportTypeDefSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec#getIdOrAll()
   * @see #getImportTypeDefSpec()
   * @generated
   */
  EReference getImportTypeDefSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportTemplateSpec <em>Import Template Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Template Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportTemplateSpec
   * @generated
   */
  EClass getImportTemplateSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportTemplateSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportTemplateSpec#getIdOrAll()
   * @see #getImportTemplateSpec()
   * @generated
   */
  EReference getImportTemplateSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportConstSpec <em>Import Const Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Const Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportConstSpec
   * @generated
   */
  EClass getImportConstSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportConstSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportConstSpec#getIdOrAll()
   * @see #getImportConstSpec()
   * @generated
   */
  EReference getImportConstSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportAltstepSpec <em>Import Altstep Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Altstep Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportAltstepSpec
   * @generated
   */
  EClass getImportAltstepSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportAltstepSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportAltstepSpec#getIdOrAll()
   * @see #getImportAltstepSpec()
   * @generated
   */
  EReference getImportAltstepSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec <em>Import Testcase Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Testcase Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec
   * @generated
   */
  EClass getImportTestcaseSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec#getIdOrAll()
   * @see #getImportTestcaseSpec()
   * @generated
   */
  EReference getImportTestcaseSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportFunctionSpec <em>Import Function Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Function Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportFunctionSpec
   * @generated
   */
  EClass getImportFunctionSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportFunctionSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportFunctionSpec#getIdOrAll()
   * @see #getImportFunctionSpec()
   * @generated
   */
  EReference getImportFunctionSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportSignatureSpec <em>Import Signature Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Signature Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportSignatureSpec
   * @generated
   */
  EClass getImportSignatureSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportSignatureSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportSignatureSpec#getIdOrAll()
   * @see #getImportSignatureSpec()
   * @generated
   */
  EReference getImportSignatureSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ImportModuleParSpec <em>Import Module Par Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Module Par Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportModuleParSpec
   * @generated
   */
  EClass getImportModuleParSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ImportModuleParSpec#getIdOrAll <em>Id Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ImportModuleParSpec#getIdOrAll()
   * @see #getImportModuleParSpec()
   * @generated
   */
  EReference getImportModuleParSpec_IdOrAll();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept <em>Group Ref List With Except</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Group Ref List With Except</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept
   * @generated
   */
  EClass getGroupRefListWithExcept();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept#getQualifier <em>Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Qualifier</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GroupRefListWithExcept#getQualifier()
   * @see #getGroupRefListWithExcept()
   * @generated
   */
  EReference getGroupRefListWithExcept_Qualifier();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept <em>Qualified Identifier With Except</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Qualified Identifier With Except</em>'.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept
   * @generated
   */
  EClass getQualifiedIdentifierWithExcept();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getId()
   * @see #getQualifiedIdentifierWithExcept()
   * @generated
   */
  EAttribute getQualifiedIdentifierWithExcept_Id();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getDef <em>Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierWithExcept#getDef()
   * @see #getQualifiedIdentifierWithExcept()
   * @generated
   */
  EReference getQualifiedIdentifierWithExcept_Def();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept <em>All Groups With Except</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Groups With Except</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept
   * @generated
   */
  EClass getAllGroupsWithExcept();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept#getIdList <em>Id List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept#getIdList()
   * @see #getAllGroupsWithExcept()
   * @generated
   */
  EReference getAllGroupsWithExcept_IdList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltstepDef <em>Altstep Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Altstep Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef
   * @generated
   */
  EClass getAltstepDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef#getParams()
   * @see #getAltstepDef()
   * @generated
   */
  EReference getAltstepDef_Params();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef#getSpec()
   * @see #getAltstepDef()
   * @generated
   */
  EReference getAltstepDef_Spec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getMtc <em>Mtc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Mtc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef#getMtc()
   * @see #getAltstepDef()
   * @generated
   */
  EReference getAltstepDef_Mtc();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getSystem <em>System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>System</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef#getSystem()
   * @see #getAltstepDef()
   * @generated
   */
  EReference getAltstepDef_System();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getLocal <em>Local</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Local</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef#getLocal()
   * @see #getAltstepDef()
   * @generated
   */
  EReference getAltstepDef_Local();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getGuard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepDef#getGuard()
   * @see #getAltstepDef()
   * @generated
   */
  EReference getAltstepDef_Guard();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList <em>Altstep Local Def List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Altstep Local Def List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDefList
   * @generated
   */
  EClass getAltstepLocalDefList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getDefs()
   * @see #getAltstepLocalDefList()
   * @generated
   */
  EReference getAltstepLocalDefList_Defs();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getWithstats <em>Withstats</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Withstats</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getWithstats()
   * @see #getAltstepLocalDefList()
   * @generated
   */
  EReference getAltstepLocalDefList_Withstats();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDefList#getSc()
   * @see #getAltstepLocalDefList()
   * @generated
   */
  EAttribute getAltstepLocalDefList_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef <em>Altstep Local Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Altstep Local Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDef
   * @generated
   */
  EClass getAltstepLocalDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getVariable()
   * @see #getAltstepLocalDef()
   * @generated
   */
  EReference getAltstepLocalDef_Variable();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTimer()
   * @see #getAltstepLocalDef()
   * @generated
   */
  EReference getAltstepLocalDef_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getConst()
   * @see #getAltstepLocalDef()
   * @generated
   */
  EReference getAltstepLocalDef_Const();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepLocalDef#getTemplate()
   * @see #getAltstepLocalDef()
   * @generated
   */
  EReference getAltstepLocalDef_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef <em>Testcase Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Testcase Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseDef
   * @generated
   */
  EClass getTestcaseDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getParList <em>Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseDef#getParList()
   * @see #getTestcaseDef()
   * @generated
   */
  EReference getTestcaseDef_ParList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseDef#getSpec()
   * @see #getTestcaseDef()
   * @generated
   */
  EReference getTestcaseDef_Spec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseDef#getStatement()
   * @see #getTestcaseDef()
   * @generated
   */
  EReference getTestcaseDef_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList <em>Template Or Value Formal Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Or Value Formal Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList
   * @generated
   */
  EClass getTemplateOrValueFormalParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList#getParams()
   * @see #getTemplateOrValueFormalParList()
   * @generated
   */
  EReference getTemplateOrValueFormalParList_Params();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar <em>Template Or Value Formal Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Or Value Formal Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar
   * @generated
   */
  EClass getTemplateOrValueFormalPar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getValue()
   * @see #getTemplateOrValueFormalPar()
   * @generated
   */
  EReference getTemplateOrValueFormalPar_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getTemplate()
   * @see #getTemplateOrValueFormalPar()
   * @generated
   */
  EReference getTemplateOrValueFormalPar_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConfigSpec <em>Config Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Config Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigSpec
   * @generated
   */
  EClass getConfigSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigSpec#getRunsOn <em>Runs On</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Runs On</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigSpec#getRunsOn()
   * @see #getConfigSpec()
   * @generated
   */
  EReference getConfigSpec_RunsOn();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigSpec#getSystemSpec <em>System Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>System Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigSpec#getSystemSpec()
   * @see #getConfigSpec()
   * @generated
   */
  EReference getConfigSpec_SystemSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SystemSpec <em>System Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>System Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SystemSpec
   * @generated
   */
  EClass getSystemSpec();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.SystemSpec#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SystemSpec#getComponent()
   * @see #getSystemSpec()
   * @generated
   */
  EReference getSystemSpec_Component();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SignatureDef <em>Signature Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signature Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureDef
   * @generated
   */
  EClass getSignatureDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getParamList <em>Param List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Param List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureDef#getParamList()
   * @see #getSignatureDef()
   * @generated
   */
  EReference getSignatureDef_ParamList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getReturn <em>Return</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureDef#getReturn()
   * @see #getSignatureDef()
   * @generated
   */
  EReference getSignatureDef_Return();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getSprec <em>Sprec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sprec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureDef#getSprec()
   * @see #getSignatureDef()
   * @generated
   */
  EReference getSignatureDef_Sprec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExceptionSpec <em>Exception Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exception Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExceptionSpec
   * @generated
   */
  EClass getExceptionSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SignatureFormalParList <em>Signature Formal Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signature Formal Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureFormalParList
   * @generated
   */
  EClass getSignatureFormalParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SignatureFormalParList#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureFormalParList#getParams()
   * @see #getSignatureFormalParList()
   * @generated
   */
  EReference getSignatureFormalParList_Params();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart <em>Module Control Part</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Control Part</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlPart
   * @generated
   */
  EClass getModuleControlPart();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlPart#getBody()
   * @see #getModuleControlPart()
   * @generated
   */
  EReference getModuleControlPart_Body();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getWs <em>Ws</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ws</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlPart#getWs()
   * @see #getModuleControlPart()
   * @generated
   */
  EReference getModuleControlPart_Ws();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlPart#getSc()
   * @see #getModuleControlPart()
   * @generated
   */
  EAttribute getModuleControlPart_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleControlBody <em>Module Control Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Control Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlBody
   * @generated
   */
  EClass getModuleControlBody();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ModuleControlBody#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleControlBody#getList()
   * @see #getModuleControlBody()
   * @generated
   */
  EReference getModuleControlBody_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList <em>Control Statement Or Def List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Control Statement Or Def List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList
   * @generated
   */
  EClass getControlStatementOrDefList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getLocalDef <em>Local Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Local Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getLocalDef()
   * @see #getControlStatementOrDefList()
   * @generated
   */
  EReference getControlStatementOrDefList_LocalDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getLocalInst <em>Local Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Local Inst</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getLocalInst()
   * @see #getControlStatementOrDefList()
   * @generated
   */
  EReference getControlStatementOrDefList_LocalInst();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getWithstm <em>Withstm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Withstm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getWithstm()
   * @see #getControlStatementOrDefList()
   * @generated
   */
  EReference getControlStatementOrDefList_Withstm();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getControl <em>Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Control</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getControl()
   * @see #getControlStatementOrDefList()
   * @generated
   */
  EReference getControlStatementOrDefList_Control();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList#getSc()
   * @see #getControlStatementOrDefList()
   * @generated
   */
  EAttribute getControlStatementOrDefList_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef <em>Control Statement Or Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Control Statement Or Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDef
   * @generated
   */
  EClass getControlStatementOrDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalDef <em>Local Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Local Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalDef()
   * @see #getControlStatementOrDef()
   * @generated
   */
  EReference getControlStatementOrDef_LocalDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalInst <em>Local Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Local Inst</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalInst()
   * @see #getControlStatementOrDef()
   * @generated
   */
  EReference getControlStatementOrDef_LocalInst();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getWithstm <em>Withstm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Withstm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getWithstm()
   * @see #getControlStatementOrDef()
   * @generated
   */
  EReference getControlStatementOrDef_Withstm();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getControl <em>Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Control</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getControl()
   * @see #getControlStatementOrDef()
   * @generated
   */
  EReference getControlStatementOrDef_Control();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ControlStatement <em>Control Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Control Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatement
   * @generated
   */
  EClass getControlStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatement#getTimer()
   * @see #getControlStatement()
   * @generated
   */
  EReference getControlStatement_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getBasic <em>Basic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Basic</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatement#getBasic()
   * @see #getControlStatement()
   * @generated
   */
  EReference getControlStatement_Basic();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getBehavior <em>Behavior</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Behavior</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatement#getBehavior()
   * @see #getControlStatement()
   * @generated
   */
  EReference getControlStatement_Behavior();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ControlStatement#getSut <em>Sut</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sut</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ControlStatement#getSut()
   * @see #getControlStatement()
   * @generated
   */
  EReference getControlStatement_Sut();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SUTStatements <em>SUT Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SUT Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SUTStatements
   * @generated
   */
  EClass getSUTStatements();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SUTStatements#getTxt <em>Txt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Txt</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SUTStatements#getTxt()
   * @see #getSUTStatements()
   * @generated
   */
  EReference getSUTStatements_Txt();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ActionText <em>Action Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action Text</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ActionText
   * @generated
   */
  EClass getActionText();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ActionText#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ActionText#getExpr()
   * @see #getActionText()
   * @generated
   */
  EReference getActionText_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements <em>Behaviour Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Behaviour Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements
   * @generated
   */
  EClass getBehaviourStatements();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getTestcase <em>Testcase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Testcase</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getTestcase()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Testcase();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getFunction()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Function();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getReturn <em>Return</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getReturn()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Return();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAlt <em>Alt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Alt</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAlt()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Alt();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getInterleaved <em>Interleaved</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Interleaved</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getInterleaved()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Interleaved();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Label</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getLabel()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Label();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getGoto <em>Goto</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Goto</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getGoto()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Goto();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getDeactivate <em>Deactivate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Deactivate</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getDeactivate()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Deactivate();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAltstep <em>Altstep</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Altstep</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAltstep()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Altstep();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getActivate <em>Activate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Activate</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BehaviourStatements#getActivate()
   * @see #getBehaviourStatements()
   * @generated
   */
  EReference getBehaviourStatements_Activate();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ActivateOp <em>Activate Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Activate Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ActivateOp
   * @generated
   */
  EClass getActivateOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ActivateOp#getAltstep <em>Altstep</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Altstep</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ActivateOp#getAltstep()
   * @see #getActivateOp()
   * @generated
   */
  EReference getActivateOp_Altstep();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DeactivateStatement <em>Deactivate Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Deactivate Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DeactivateStatement
   * @generated
   */
  EClass getDeactivateStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DeactivateStatement#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DeactivateStatement#getComponent()
   * @see #getDeactivateStatement()
   * @generated
   */
  EReference getDeactivateStatement_Component();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.LabelStatement <em>Label Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Label Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LabelStatement
   * @generated
   */
  EClass getLabelStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GotoStatement <em>Goto Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Goto Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GotoStatement
   * @generated
   */
  EClass getGotoStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReturnStatement <em>Return Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Return Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReturnStatement
   * @generated
   */
  EClass getReturnStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReturnStatement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReturnStatement#getExpression()
   * @see #getReturnStatement()
   * @generated
   */
  EReference getReturnStatement_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReturnStatement#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReturnStatement#getTemplate()
   * @see #getReturnStatement()
   * @generated
   */
  EReference getReturnStatement_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.InterleavedConstruct <em>Interleaved Construct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interleaved Construct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedConstruct
   * @generated
   */
  EClass getInterleavedConstruct();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardList <em>Interleaved Guard List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interleaved Guard List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardList
   * @generated
   */
  EClass getInterleavedGuardList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardList#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardList#getElements()
   * @see #getInterleavedGuardList()
   * @generated
   */
  EReference getInterleavedGuardList_Elements();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement <em>Interleaved Guard Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interleaved Guard Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardElement
   * @generated
   */
  EClass getInterleavedGuardElement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getGuard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getGuard()
   * @see #getInterleavedGuardElement()
   * @generated
   */
  EReference getInterleavedGuardElement_Guard();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuardElement#getStatement()
   * @see #getInterleavedGuardElement()
   * @generated
   */
  EReference getInterleavedGuardElement_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuard <em>Interleaved Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interleaved Guard</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuard
   * @generated
   */
  EClass getInterleavedGuard();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuard#getGuardOp <em>Guard Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InterleavedGuard#getGuardOp()
   * @see #getInterleavedGuard()
   * @generated
   */
  EReference getInterleavedGuard_GuardOp();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltConstruct <em>Alt Construct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alt Construct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltConstruct
   * @generated
   */
  EClass getAltConstruct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltConstruct#getAgList <em>Ag List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ag List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltConstruct#getAgList()
   * @see #getAltConstruct()
   * @generated
   */
  EReference getAltConstruct_AgList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltGuardList <em>Alt Guard List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alt Guard List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardList
   * @generated
   */
  EClass getAltGuardList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AltGuardList#getGuardList <em>Guard List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Guard List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardList#getGuardList()
   * @see #getAltGuardList()
   * @generated
   */
  EReference getAltGuardList_GuardList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AltGuardList#getElseList <em>Else List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Else List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardList#getElseList()
   * @see #getAltGuardList()
   * @generated
   */
  EReference getAltGuardList_ElseList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ElseStatement <em>Else Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Else Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseStatement
   * @generated
   */
  EClass getElseStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ElseStatement#getBlock <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Block</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseStatement#getBlock()
   * @see #getElseStatement()
   * @generated
   */
  EReference getElseStatement_Block();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GuardStatement <em>Guard Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardStatement
   * @generated
   */
  EClass getGuardStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getGuard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardStatement#getGuard()
   * @see #getGuardStatement()
   * @generated
   */
  EReference getGuardStatement_Guard();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getStep <em>Step</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Step</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardStatement#getStep()
   * @see #getGuardStatement()
   * @generated
   */
  EReference getGuardStatement_Step();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getBlock <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Block</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardStatement#getBlock()
   * @see #getGuardStatement()
   * @generated
   */
  EReference getGuardStatement_Block();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardStatement#getOp()
   * @see #getGuardStatement()
   * @generated
   */
  EReference getGuardStatement_Op();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GuardOp <em>Guard Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp
   * @generated
   */
  EClass getGuardOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getTimeout <em>Timeout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timeout</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getTimeout()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Timeout();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getReceive <em>Receive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Receive</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getReceive()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Receive();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getTrigger <em>Trigger</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Trigger</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getTrigger()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Trigger();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getGetCall <em>Get Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Call</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getGetCall()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_GetCall();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getCatch <em>Catch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Catch</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getCatch()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Catch();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getCheck <em>Check</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Check</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getCheck()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Check();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getGetReply <em>Get Reply</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Reply</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getGetReply()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_GetReply();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getDone <em>Done</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Done</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getDone()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Done();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getKilled <em>Killed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Killed</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GuardOp#getKilled()
   * @see #getGuardOp()
   * @generated
   */
  EReference getGuardOp_Killed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DoneStatement <em>Done Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Done Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoneStatement
   * @generated
   */
  EClass getDoneStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DoneStatement#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoneStatement#getComponent()
   * @see #getDoneStatement()
   * @generated
   */
  EReference getDoneStatement_Component();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DoneStatement#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoneStatement#getIndex()
   * @see #getDoneStatement()
   * @generated
   */
  EReference getDoneStatement_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.KilledStatement <em>Killed Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Killed Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.KilledStatement
   * @generated
   */
  EClass getKilledStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.KilledStatement#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.KilledStatement#getComponent()
   * @see #getKilledStatement()
   * @generated
   */
  EReference getKilledStatement_Component();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.KilledStatement#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.KilledStatement#getIndex()
   * @see #getKilledStatement()
   * @generated
   */
  EReference getKilledStatement_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny <em>Component Or Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Or Any</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrAny
   * @generated
   */
  EClass getComponentOrAny();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny#getCompOrDefault <em>Comp Or Default</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Comp Or Default</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrAny#getCompOrDefault()
   * @see #getComponentOrAny()
   * @generated
   */
  EReference getComponentOrAny_CompOrDefault();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrAny#getRef()
   * @see #getComponentOrAny()
   * @generated
   */
  EReference getComponentOrAny_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.IndexAssignment <em>Index Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Index Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IndexAssignment
   * @generated
   */
  EClass getIndexAssignment();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.IndexAssignment#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IndexAssignment#getIndex()
   * @see #getIndexAssignment()
   * @generated
   */
  EReference getIndexAssignment_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.IndexSpec <em>Index Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Index Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IndexSpec
   * @generated
   */
  EClass getIndexSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.IndexSpec#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IndexSpec#getRef()
   * @see #getIndexSpec()
   * @generated
   */
  EReference getIndexSpec_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GetReplyStatement <em>Get Reply Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Get Reply Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GetReplyStatement
   * @generated
   */
  EClass getGetReplyStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CheckStatement <em>Check Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckStatement
   * @generated
   */
  EClass getCheckStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortCheckOp <em>Port Check Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Check Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCheckOp
   * @generated
   */
  EClass getPortCheckOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortCheckOp#getCheck <em>Check</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Check</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCheckOp#getCheck()
   * @see #getPortCheckOp()
   * @generated
   */
  EReference getPortCheckOp_Check();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CheckParameter <em>Check Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check Parameter</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckParameter
   * @generated
   */
  EClass getCheckParameter();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RedirectPresent <em>Redirect Present</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Redirect Present</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectPresent
   * @generated
   */
  EClass getRedirectPresent();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectPresent#getSender <em>Sender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sender</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectPresent#getSender()
   * @see #getRedirectPresent()
   * @generated
   */
  EReference getRedirectPresent_Sender();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectPresent#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectPresent#getIndex()
   * @see #getRedirectPresent()
   * @generated
   */
  EReference getRedirectPresent_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FromClausePresent <em>From Clause Present</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>From Clause Present</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FromClausePresent
   * @generated
   */
  EClass getFromClausePresent();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent <em>Check Port Ops Present</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check Port Ops Present</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent
   * @generated
   */
  EClass getCheckPortOpsPresent();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent#getFrom <em>From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>From</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckPortOpsPresent#getFrom()
   * @see #getCheckPortOpsPresent()
   * @generated
   */
  EReference getCheckPortOpsPresent_From();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp <em>Port Get Reply Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Get Reply Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetReplyOp
   * @generated
   */
  EClass getPortGetReplyOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getTemplate()
   * @see #getPortGetReplyOp()
   * @generated
   */
  EReference getPortGetReplyOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getValue()
   * @see #getPortGetReplyOp()
   * @generated
   */
  EReference getPortGetReplyOp_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getRedirect()
   * @see #getPortGetReplyOp()
   * @generated
   */
  EReference getPortGetReplyOp_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithValueAndParam <em>Port Redirect With Value And Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Redirect With Value And Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirectWithValueAndParam
   * @generated
   */
  EClass getPortRedirectWithValueAndParam();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec <em>Redirect With Value And Param Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Redirect With Value And Param Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec
   * @generated
   */
  EClass getRedirectWithValueAndParamSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getValue()
   * @see #getRedirectWithValueAndParamSpec()
   * @generated
   */
  EReference getRedirectWithValueAndParamSpec_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getParam()
   * @see #getRedirectWithValueAndParamSpec()
   * @generated
   */
  EReference getRedirectWithValueAndParamSpec_Param();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getSender <em>Sender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sender</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getSender()
   * @see #getRedirectWithValueAndParamSpec()
   * @generated
   */
  EReference getRedirectWithValueAndParamSpec_Sender();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getIndex()
   * @see #getRedirectWithValueAndParamSpec()
   * @generated
   */
  EReference getRedirectWithValueAndParamSpec_Index();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithValueAndParamSpec#getRedirect()
   * @see #getRedirectWithValueAndParamSpec()
   * @generated
   */
  EReference getRedirectWithValueAndParamSpec_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ValueMatchSpec <em>Value Match Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value Match Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ValueMatchSpec
   * @generated
   */
  EClass getValueMatchSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ValueMatchSpec#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ValueMatchSpec#getTemplate()
   * @see #getValueMatchSpec()
   * @generated
   */
  EReference getValueMatchSpec_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CatchStatement <em>Catch Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Catch Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CatchStatement
   * @generated
   */
  EClass getCatchStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortCatchOp <em>Port Catch Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Catch Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCatchOp
   * @generated
   */
  EClass getPortCatchOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortCatchOp#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCatchOp#getParam()
   * @see #getPortCatchOp()
   * @generated
   */
  EReference getPortCatchOp_Param();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortCatchOp#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCatchOp#getRedirect()
   * @see #getPortCatchOp()
   * @generated
   */
  EReference getPortCatchOp_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CatchOpParameter <em>Catch Op Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Catch Op Parameter</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CatchOpParameter
   * @generated
   */
  EClass getCatchOpParameter();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CatchOpParameter#getSignature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Signature</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CatchOpParameter#getSignature()
   * @see #getCatchOpParameter()
   * @generated
   */
  EReference getCatchOpParameter_Signature();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CatchOpParameter#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CatchOpParameter#getTemplate()
   * @see #getCatchOpParameter()
   * @generated
   */
  EReference getCatchOpParameter_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GetCallStatement <em>Get Call Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Get Call Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GetCallStatement
   * @generated
   */
  EClass getGetCallStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortGetCallOp <em>Port Get Call Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Get Call Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetCallOp
   * @generated
   */
  EClass getPortGetCallOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortGetCallOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetCallOp#getTemplate()
   * @see #getPortGetCallOp()
   * @generated
   */
  EReference getPortGetCallOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortGetCallOp#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortGetCallOp#getRedirect()
   * @see #getPortGetCallOp()
   * @generated
   */
  EReference getPortGetCallOp_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithParam <em>Port Redirect With Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Redirect With Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirectWithParam
   * @generated
   */
  EClass getPortRedirectWithParam();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRedirectWithParam#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirectWithParam#getRedirect()
   * @see #getPortRedirectWithParam()
   * @generated
   */
  EReference getPortRedirectWithParam_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec <em>Redirect With Param Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Redirect With Param Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec
   * @generated
   */
  EClass getRedirectWithParamSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getParam()
   * @see #getRedirectWithParamSpec()
   * @generated
   */
  EReference getRedirectWithParamSpec_Param();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getSender <em>Sender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sender</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getSender()
   * @see #getRedirectWithParamSpec()
   * @generated
   */
  EReference getRedirectWithParamSpec_Sender();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getIndex()
   * @see #getRedirectWithParamSpec()
   * @generated
   */
  EReference getRedirectWithParamSpec_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ParamSpec <em>Param Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Param Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ParamSpec
   * @generated
   */
  EClass getParamSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ParamAssignmentList <em>Param Assignment List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Param Assignment List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ParamAssignmentList
   * @generated
   */
  EClass getParamAssignmentList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AssignmentList <em>Assignment List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assignment List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AssignmentList
   * @generated
   */
  EClass getAssignmentList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AssignmentList#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Assign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AssignmentList#getAssign()
   * @see #getAssignmentList()
   * @generated
   */
  EReference getAssignmentList_Assign();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.VariableAssignment <em>Variable Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableAssignment
   * @generated
   */
  EClass getVariableAssignment();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VariableAssignment#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableAssignment#getRef()
   * @see #getVariableAssignment()
   * @generated
   */
  EReference getVariableAssignment_Ref();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.VariableAssignment#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableAssignment#getName()
   * @see #getVariableAssignment()
   * @generated
   */
  EAttribute getVariableAssignment_Name();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.VariableList <em>Variable List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableList
   * @generated
   */
  EClass getVariableList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.VariableList#getEntries <em>Entries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Entries</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableList#getEntries()
   * @see #getVariableList()
   * @generated
   */
  EReference getVariableList_Entries();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.VariableEntry <em>Variable Entry</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Entry</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableEntry
   * @generated
   */
  EClass getVariableEntry();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TriggerStatement <em>Trigger Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Trigger Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TriggerStatement
   * @generated
   */
  EClass getTriggerStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortTriggerOp <em>Port Trigger Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Trigger Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortTriggerOp
   * @generated
   */
  EClass getPortTriggerOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortTriggerOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortTriggerOp#getTemplate()
   * @see #getPortTriggerOp()
   * @generated
   */
  EReference getPortTriggerOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortTriggerOp#getFrom <em>From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>From</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortTriggerOp#getFrom()
   * @see #getPortTriggerOp()
   * @generated
   */
  EReference getPortTriggerOp_From();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortTriggerOp#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortTriggerOp#getRedirect()
   * @see #getPortTriggerOp()
   * @generated
   */
  EReference getPortTriggerOp_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement <em>Receive Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Receive Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReceiveStatement
   * @generated
   */
  EClass getReceiveStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement#getAny <em>Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Any</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReceiveStatement#getAny()
   * @see #getReceiveStatement()
   * @generated
   */
  EReference getReceiveStatement_Any();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReceiveStatement#getReceive <em>Receive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Receive</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReceiveStatement#getReceive()
   * @see #getReceiveStatement()
   * @generated
   */
  EReference getReceiveStatement_Receive();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortOrAny <em>Port Or Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Or Any</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny
   * @generated
   */
  EClass getPortOrAny();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getReply <em>Reply</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Reply</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getReply()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Reply();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCheck <em>Check</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Check</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getCheck()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Check();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCatch <em>Catch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Catch</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getCatch()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Catch();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCall <em>Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Call</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getCall()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Call();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getTrigger <em>Trigger</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Trigger</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getTrigger()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Trigger();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getRef()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getArray()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAny#getVariable()
   * @see #getPortOrAny()
   * @generated
   */
  EReference getPortOrAny_Variable();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortReceiveOp <em>Port Receive Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Receive Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReceiveOp
   * @generated
   */
  EClass getPortReceiveOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortReceiveOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReceiveOp#getTemplate()
   * @see #getPortReceiveOp()
   * @generated
   */
  EReference getPortReceiveOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortReceiveOp#getRedirect <em>Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReceiveOp#getRedirect()
   * @see #getPortReceiveOp()
   * @generated
   */
  EReference getPortReceiveOp_Redirect();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FromClause <em>From Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>From Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FromClause
   * @generated
   */
  EClass getFromClause();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FromClause#getSender <em>Sender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sender</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FromClause#getSender()
   * @see #getFromClause()
   * @generated
   */
  EReference getFromClause_Sender();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FromClause#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FromClause#getIndex()
   * @see #getFromClause()
   * @generated
   */
  EReference getFromClause_Index();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FromClause#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FromClause#getTemplate()
   * @see #getFromClause()
   * @generated
   */
  EReference getFromClause_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FromClause#getAddresses <em>Addresses</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Addresses</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FromClause#getAddresses()
   * @see #getFromClause()
   * @generated
   */
  EReference getFromClause_Addresses();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AddressRefList <em>Address Ref List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Address Ref List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AddressRefList
   * @generated
   */
  EClass getAddressRefList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AddressRefList#getTemplates <em>Templates</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Templates</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AddressRefList#getTemplates()
   * @see #getAddressRefList()
   * @generated
   */
  EReference getAddressRefList_Templates();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortRedirect <em>Port Redirect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Redirect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirect
   * @generated
   */
  EClass getPortRedirect();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirect#getValue()
   * @see #getPortRedirect()
   * @generated
   */
  EReference getPortRedirect_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getSender <em>Sender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sender</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirect#getSender()
   * @see #getPortRedirect()
   * @generated
   */
  EReference getPortRedirect_Sender();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRedirect#getIndex()
   * @see #getPortRedirect()
   * @generated
   */
  EReference getPortRedirect_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SenderSpec <em>Sender Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sender Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SenderSpec
   * @generated
   */
  EClass getSenderSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SenderSpec#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SenderSpec#getVariable()
   * @see #getSenderSpec()
   * @generated
   */
  EReference getSenderSpec_Variable();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ValueSpec <em>Value Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ValueSpec
   * @generated
   */
  EClass getValueSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ValueSpec#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ValueSpec#getVariable()
   * @see #getValueSpec()
   * @generated
   */
  EReference getValueSpec_Variable();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ValueSpec#getSpecs <em>Specs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Specs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ValueSpec#getSpecs()
   * @see #getValueSpec()
   * @generated
   */
  EReference getValueSpec_Specs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec <em>Single Value Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Value Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleValueSpec
   * @generated
   */
  EClass getSingleValueSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleValueSpec#getVariable()
   * @see #getSingleValueSpec()
   * @generated
   */
  EReference getSingleValueSpec_Variable();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleValueSpec#getField()
   * @see #getSingleValueSpec()
   * @generated
   */
  EReference getSingleValueSpec_Field();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getExt <em>Ext</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ext</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleValueSpec#getExt()
   * @see #getSingleValueSpec()
   * @generated
   */
  EReference getSingleValueSpec_Ext();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltGuardChar <em>Alt Guard Char</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alt Guard Char</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardChar
   * @generated
   */
  EClass getAltGuardChar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltGuardChar#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltGuardChar#getExpr()
   * @see #getAltGuardChar()
   * @generated
   */
  EReference getAltGuardChar_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AltstepInstance <em>Altstep Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Altstep Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepInstance
   * @generated
   */
  EClass getAltstepInstance();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.AltstepInstance#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepInstance#getRef()
   * @see #getAltstepInstance()
   * @generated
   */
  EReference getAltstepInstance_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AltstepInstance#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AltstepInstance#getList()
   * @see #getAltstepInstance()
   * @generated
   */
  EReference getAltstepInstance_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionInstance <em>Function Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionInstance
   * @generated
   */
  EClass getFunctionInstance();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.FunctionInstance#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionInstance#getRef()
   * @see #getFunctionInstance()
   * @generated
   */
  EReference getFunctionInstance_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionInstance#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionInstance#getParams()
   * @see #getFunctionInstance()
   * @generated
   */
  EReference getFunctionInstance_Params();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionRef <em>Function Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionRef
   * @generated
   */
  EClass getFunctionRef();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParList <em>Function Actual Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Actual Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParList
   * @generated
   */
  EClass getFunctionActualParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParList#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParList#getParams()
   * @see #getFunctionActualParList()
   * @generated
   */
  EReference getFunctionActualParList_Params();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParList#getAsssign <em>Asssign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Asssign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParList#getAsssign()
   * @see #getFunctionActualParList()
   * @generated
   */
  EReference getFunctionActualParList_Asssign();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment <em>Function Actual Par Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Actual Par Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment
   * @generated
   */
  EClass getFunctionActualParAssignment();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTemplate()
   * @see #getFunctionActualParAssignment()
   * @generated
   */
  EReference getFunctionActualParAssignment_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getComponent()
   * @see #getFunctionActualParAssignment()
   * @generated
   */
  EReference getFunctionActualParAssignment_Component();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getPort()
   * @see #getFunctionActualParAssignment()
   * @generated
   */
  EReference getFunctionActualParAssignment_Port();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTimer()
   * @see #getFunctionActualParAssignment()
   * @generated
   */
  EReference getFunctionActualParAssignment_Timer();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentRefAssignment <em>Component Ref Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Ref Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRefAssignment
   * @generated
   */
  EClass getComponentRefAssignment();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.ComponentRefAssignment#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRefAssignment#getRef()
   * @see #getComponentRefAssignment()
   * @generated
   */
  EReference getComponentRefAssignment_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentRefAssignment#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRefAssignment#getValue()
   * @see #getComponentRefAssignment()
   * @generated
   */
  EReference getComponentRefAssignment_Value();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar <em>Formal Port And Value Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formal Port And Value Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar
   * @generated
   */
  EClass getFormalPortAndValuePar();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment <em>Port Ref Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Ref Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRefAssignment
   * @generated
   */
  EClass getPortRefAssignment();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRefAssignment#getPort()
   * @see #getPortRefAssignment()
   * @generated
   */
  EReference getPortRefAssignment_Port();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRefAssignment#getValue()
   * @see #getPortRefAssignment()
   * @generated
   */
  EReference getPortRefAssignment_Value();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment <em>Timer Ref Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Ref Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefAssignment
   * @generated
   */
  EClass getTimerRefAssignment();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getTimer()
   * @see #getTimerRefAssignment()
   * @generated
   */
  EReference getTimerRefAssignment_Timer();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getValue()
   * @see #getTimerRefAssignment()
   * @generated
   */
  EReference getTimerRefAssignment_Value();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar <em>Function Actual Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Actual Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualPar
   * @generated
   */
  EClass getFunctionActualPar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTemplate()
   * @see #getFunctionActualPar()
   * @generated
   */
  EReference getFunctionActualPar_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualPar#getComponent()
   * @see #getFunctionActualPar()
   * @generated
   */
  EReference getFunctionActualPar_Component();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTimer()
   * @see #getFunctionActualPar()
   * @generated
   */
  EReference getFunctionActualPar_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionActualPar#getPort()
   * @see #getFunctionActualPar()
   * @generated
   */
  EReference getFunctionActualPar_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentRef <em>Component Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRef
   * @generated
   */
  EClass getComponentRef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRef#getRef()
   * @see #getComponentRef()
   * @generated
   */
  EReference getComponentRef_Ref();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getSystem <em>System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>System</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRef#getSystem()
   * @see #getComponentRef()
   * @generated
   */
  EAttribute getComponentRef_System();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getSelf <em>Self</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Self</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRef#getSelf()
   * @see #getComponentRef()
   * @generated
   */
  EAttribute getComponentRef_Self();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ComponentRef#getMtc <em>Mtc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Mtc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentRef#getMtc()
   * @see #getComponentRef()
   * @generated
   */
  EAttribute getComponentRef_Mtc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference <em>Component Or Default Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Or Default Reference</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference
   * @generated
   */
  EClass getComponentOrDefaultReference();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentOrDefaultReference#getVariable()
   * @see #getComponentOrDefaultReference()
   * @generated
   */
  EReference getComponentOrDefaultReference_Variable();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance <em>Testcase Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Testcase Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseInstance
   * @generated
   */
  EClass getTestcaseInstance();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseInstance#getRef()
   * @see #getTestcaseInstance()
   * @generated
   */
  EReference getTestcaseInstance_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getTcParams <em>Tc Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tc Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseInstance#getTcParams()
   * @see #getTestcaseInstance()
   * @generated
   */
  EReference getTestcaseInstance_TcParams();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseInstance#getExpr()
   * @see #getTestcaseInstance()
   * @generated
   */
  EReference getTestcaseInstance_Expr();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getSexpr <em>Sexpr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sexpr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseInstance#getSexpr()
   * @see #getTestcaseInstance()
   * @generated
   */
  EReference getTestcaseInstance_Sexpr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TestcaseActualParList <em>Testcase Actual Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Testcase Actual Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseActualParList
   * @generated
   */
  EClass getTestcaseActualParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TestcaseActualParList#getTemplParam <em>Templ Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Templ Param</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseActualParList#getTemplParam()
   * @see #getTestcaseActualParList()
   * @generated
   */
  EReference getTestcaseActualParList_TemplParam();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TestcaseActualParList#getTemplAssign <em>Templ Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Templ Assign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseActualParList#getTemplAssign()
   * @see #getTestcaseActualParList()
   * @generated
   */
  EReference getTestcaseActualParList_TemplAssign();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerStatements <em>Timer Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerStatements
   * @generated
   */
  EClass getTimerStatements();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getStart <em>Start</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Start</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerStatements#getStart()
   * @see #getTimerStatements()
   * @generated
   */
  EReference getTimerStatements_Start();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getStop <em>Stop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Stop</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerStatements#getStop()
   * @see #getTimerStatements()
   * @generated
   */
  EReference getTimerStatements_Stop();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getTimeout <em>Timeout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timeout</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerStatements#getTimeout()
   * @see #getTimerStatements()
   * @generated
   */
  EReference getTimerStatements_Timeout();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimeoutStatement <em>Timeout Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timeout Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimeoutStatement
   * @generated
   */
  EClass getTimeoutStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimeoutStatement#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimeoutStatement#getRef()
   * @see #getTimeoutStatement()
   * @generated
   */
  EReference getTimeoutStatement_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimeoutStatement#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimeoutStatement#getIndex()
   * @see #getTimeoutStatement()
   * @generated
   */
  EReference getTimeoutStatement_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement <em>Start Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Start Timer Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTimerStatement
   * @generated
   */
  EClass getStartTimerStatement();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTimerStatement#getRef()
   * @see #getStartTimerStatement()
   * @generated
   */
  EReference getStartTimerStatement_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getArryRefs <em>Arry Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Arry Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTimerStatement#getArryRefs()
   * @see #getStartTimerStatement()
   * @generated
   */
  EReference getStartTimerStatement_ArryRefs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTimerStatement#getExpr()
   * @see #getStartTimerStatement()
   * @generated
   */
  EReference getStartTimerStatement_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StopTimerStatement <em>Stop Timer Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Stop Timer Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StopTimerStatement
   * @generated
   */
  EClass getStopTimerStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StopTimerStatement#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StopTimerStatement#getRef()
   * @see #getStopTimerStatement()
   * @generated
   */
  EReference getStopTimerStatement_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny <em>Timer Ref Or Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Ref Or Any</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAny
   * @generated
   */
  EClass getTimerRefOrAny();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getRef()
   * @see #getTimerRefOrAny()
   * @generated
   */
  EReference getTimerRefOrAny_Ref();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getTimer()
   * @see #getTimerRefOrAny()
   * @generated
   */
  EAttribute getTimerRefOrAny_Timer();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getFrom <em>From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>From</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAny#getFrom()
   * @see #getTimerRefOrAny()
   * @generated
   */
  EAttribute getTimerRefOrAny_From();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll <em>Timer Ref Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Ref Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAll
   * @generated
   */
  EClass getTimerRefOrAll();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getRef()
   * @see #getTimerRefOrAll()
   * @generated
   */
  EReference getTimerRefOrAll_Ref();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getTimer()
   * @see #getTimerRefOrAll()
   * @generated
   */
  EAttribute getTimerRefOrAll_Timer();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BasicStatements <em>Basic Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Basic Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements
   * @generated
   */
  EClass getBasicStatements();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getLog <em>Log</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Log</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements#getLog()
   * @see #getBasicStatements()
   * @generated
   */
  EReference getBasicStatements_Log();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getBlock <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Block</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements#getBlock()
   * @see #getBasicStatements()
   * @generated
   */
  EReference getBasicStatements_Block();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getLoop <em>Loop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Loop</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements#getLoop()
   * @see #getBasicStatements()
   * @generated
   */
  EReference getBasicStatements_Loop();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getConditional <em>Conditional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conditional</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements#getConditional()
   * @see #getBasicStatements()
   * @generated
   */
  EReference getBasicStatements_Conditional();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getSelect <em>Select</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Select</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements#getSelect()
   * @see #getBasicStatements()
   * @generated
   */
  EReference getBasicStatements_Select();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BasicStatements#getAssign()
   * @see #getBasicStatements()
   * @generated
   */
  EReference getBasicStatements_Assign();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StatementBlock <em>Statement Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Block</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StatementBlock
   * @generated
   */
  EClass getStatementBlock();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.StatementBlock#getDef <em>Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StatementBlock#getDef()
   * @see #getStatementBlock()
   * @generated
   */
  EReference getStatementBlock_Def();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.StatementBlock#getStat <em>Stat</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Stat</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StatementBlock#getStat()
   * @see #getStatementBlock()
   * @generated
   */
  EReference getStatementBlock_Stat();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionStatementList <em>Function Statement List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Statement List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatementList
   * @generated
   */
  EClass getFunctionStatementList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FunctionStatementList#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatementList#getStatements()
   * @see #getFunctionStatementList()
   * @generated
   */
  EReference getFunctionStatementList_Statements();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.FunctionStatementList#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatementList#getSc()
   * @see #getFunctionStatementList()
   * @generated
   */
  EAttribute getFunctionStatementList_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement <em>Function Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement
   * @generated
   */
  EClass getFunctionStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getTimer()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getBasic <em>Basic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Basic</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getBasic()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Basic();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getBehavior <em>Behavior</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Behavior</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getBehavior()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Behavior();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getVerdict <em>Verdict</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Verdict</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getVerdict()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Verdict();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getCommunication <em>Communication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Communication</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getCommunication()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Communication();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getConfiguration <em>Configuration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Configuration</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getConfiguration()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Configuration();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getSut <em>Sut</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sut</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getSut()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Sut();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getTest <em>Test</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Test</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionStatement#getTest()
   * @see #getFunctionStatement()
   * @generated
   */
  EReference getFunctionStatement_Test();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TestcaseOperation <em>Testcase Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Testcase Operation</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseOperation
   * @generated
   */
  EClass getTestcaseOperation();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.TestcaseOperation#getTxt <em>Txt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Txt</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseOperation#getTxt()
   * @see #getTestcaseOperation()
   * @generated
   */
  EAttribute getTestcaseOperation_Txt();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TestcaseOperation#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TestcaseOperation#getTemplate()
   * @see #getTestcaseOperation()
   * @generated
   */
  EReference getTestcaseOperation_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict <em>Set Local Verdict</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Set Local Verdict</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetLocalVerdict
   * @generated
   */
  EClass getSetLocalVerdict();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getExpression()
   * @see #getSetLocalVerdict()
   * @generated
   */
  EReference getSetLocalVerdict_Expression();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getLog <em>Log</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Log</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getLog()
   * @see #getSetLocalVerdict()
   * @generated
   */
  EReference getSetLocalVerdict_Log();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements <em>Configuration Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Configuration Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements
   * @generated
   */
  EClass getConfigurationStatements();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getConnect <em>Connect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Connect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getConnect()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_Connect();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getMap <em>Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Map</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getMap()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_Map();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDisconnect <em>Disconnect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Disconnect</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDisconnect()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_Disconnect();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getUnmap <em>Unmap</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Unmap</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getUnmap()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_Unmap();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDone <em>Done</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Done</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDone()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_Done();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKilled <em>Killed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Killed</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKilled()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_Killed();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStartTc <em>Start Tc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Start Tc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStartTc()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_StartTc();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStopTc <em>Stop Tc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Stop Tc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStopTc()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_StopTc();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKillTc <em>Kill Tc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Kill Tc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKillTc()
   * @see #getConfigurationStatements()
   * @generated
   */
  EReference getConfigurationStatements_KillTc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.KillTCStatement <em>Kill TC Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Kill TC Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.KillTCStatement
   * @generated
   */
  EClass getKillTCStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StopTCStatement <em>Stop TC Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Stop TC Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StopTCStatement
   * @generated
   */
  EClass getStopTCStatement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral <em>Component Reference Or Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Reference Or Literal</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral
   * @generated
   */
  EClass getComponentReferenceOrLiteral();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral#getRef()
   * @see #getComponentReferenceOrLiteral()
   * @generated
   */
  EReference getComponentReferenceOrLiteral_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StartTCStatement <em>Start TC Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Start TC Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTCStatement
   * @generated
   */
  EClass getStartTCStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StartTCStatement#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTCStatement#getRef()
   * @see #getStartTCStatement()
   * @generated
   */
  EReference getStartTCStatement_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StartTCStatement#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartTCStatement#getFunction()
   * @see #getStartTCStatement()
   * @generated
   */
  EReference getStartTCStatement_Function();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.UnmapStatement <em>Unmap Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unmap Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnmapStatement
   * @generated
   */
  EClass getUnmapStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnmapStatement#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnmapStatement#getSpec()
   * @see #getUnmapStatement()
   * @generated
   */
  EReference getUnmapStatement_Spec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnmapStatement#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnmapStatement#getClause()
   * @see #getUnmapStatement()
   * @generated
   */
  EReference getUnmapStatement_Clause();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DisconnectStatement <em>Disconnect Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Disconnect Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DisconnectStatement
   * @generated
   */
  EClass getDisconnectStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DisconnectStatement#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DisconnectStatement#getSpec()
   * @see #getDisconnectStatement()
   * @generated
   */
  EReference getDisconnectStatement_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllConnectionsSpec <em>All Connections Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Connections Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllConnectionsSpec
   * @generated
   */
  EClass getAllConnectionsSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllPortsSpec <em>All Ports Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Ports Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllPortsSpec
   * @generated
   */
  EClass getAllPortsSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllPortsSpec#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllPortsSpec#getComponent()
   * @see #getAllPortsSpec()
   * @generated
   */
  EReference getAllPortsSpec_Component();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MapStatement <em>Map Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Map Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MapStatement
   * @generated
   */
  EClass getMapStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MapStatement#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MapStatement#getSpec()
   * @see #getMapStatement()
   * @generated
   */
  EReference getMapStatement_Spec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MapStatement#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MapStatement#getClause()
   * @see #getMapStatement()
   * @generated
   */
  EReference getMapStatement_Clause();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ParamClause <em>Param Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Param Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ParamClause
   * @generated
   */
  EClass getParamClause();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ParamClause#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ParamClause#getList()
   * @see #getParamClause()
   * @generated
   */
  EReference getParamClause_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConnectStatement <em>Connect Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Connect Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConnectStatement
   * @generated
   */
  EClass getConnectStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConnectStatement#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConnectStatement#getSpec()
   * @see #getConnectStatement()
   * @generated
   */
  EReference getConnectStatement_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec <em>Single Connection Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Connection Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConnectionSpec
   * @generated
   */
  EClass getSingleConnectionSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort1 <em>Port1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort1()
   * @see #getSingleConnectionSpec()
   * @generated
   */
  EReference getSingleConnectionSpec_Port1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort2 <em>Port2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort2()
   * @see #getSingleConnectionSpec()
   * @generated
   */
  EReference getSingleConnectionSpec_Port2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortRef <em>Port Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRef
   * @generated
   */
  EClass getPortRef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRef#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRef#getComponent()
   * @see #getPortRef()
   * @generated
   */
  EReference getPortRef_Component();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.PortRef#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRef#getPort()
   * @see #getPortRef()
   * @generated
   */
  EReference getPortRef_Port();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRef#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRef#getArray()
   * @see #getPortRef()
   * @generated
   */
  EReference getPortRef_Array();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements <em>Communication Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Communication Statements</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements
   * @generated
   */
  EClass getCommunicationStatements();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getSend <em>Send</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Send</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getSend()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Send();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCall <em>Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Call</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCall()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Call();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReply <em>Reply</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Reply</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReply()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Reply();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getRaise <em>Raise</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Raise</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getRaise()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Raise();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReceive <em>Receive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Receive</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReceive()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Receive();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getTrigger <em>Trigger</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Trigger</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getTrigger()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Trigger();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetCall <em>Get Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Call</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetCall()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_GetCall();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetReply <em>Get Reply</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Reply</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetReply()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_GetReply();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCatch <em>Catch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Catch</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCatch()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Catch();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheck <em>Check</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Check</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheck()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Check();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getClear <em>Clear</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Clear</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getClear()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Clear();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStart <em>Start</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Start</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStart()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Start();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStop <em>Stop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Stop</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStop()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Stop();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getHalt <em>Halt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Halt</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getHalt()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_Halt();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheckState <em>Check State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Check State</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheckState()
   * @see #getCommunicationStatements()
   * @generated
   */
  EReference getCommunicationStatements_CheckState();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CheckStateStatement <em>Check State Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check State Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckStateStatement
   * @generated
   */
  EClass getCheckStateStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CheckStateStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckStateStatement#getPort()
   * @see #getCheckStateStatement()
   * @generated
   */
  EReference getCheckStateStatement_Port();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CheckStateStatement#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CheckStateStatement#getExpr()
   * @see #getCheckStateStatement()
   * @generated
   */
  EReference getCheckStateStatement_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortOrAllAny <em>Port Or All Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Or All Any</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAllAny
   * @generated
   */
  EClass getPortOrAllAny();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAllAny#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAllAny#getPort()
   * @see #getPortOrAllAny()
   * @generated
   */
  EReference getPortOrAllAny_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.HaltStatement <em>Halt Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Halt Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.HaltStatement
   * @generated
   */
  EClass getHaltStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.HaltStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.HaltStatement#getPort()
   * @see #getHaltStatement()
   * @generated
   */
  EReference getHaltStatement_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StartStatement <em>Start Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Start Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartStatement
   * @generated
   */
  EClass getStartStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StartStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StartStatement#getPort()
   * @see #getStartStatement()
   * @generated
   */
  EReference getStartStatement_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StopStatement <em>Stop Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Stop Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StopStatement
   * @generated
   */
  EClass getStopStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StopStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StopStatement#getPort()
   * @see #getStopStatement()
   * @generated
   */
  EReference getStopStatement_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ClearStatement <em>Clear Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Clear Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ClearStatement
   * @generated
   */
  EClass getClearStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ClearStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ClearStatement#getPort()
   * @see #getClearStatement()
   * @generated
   */
  EReference getClearStatement_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortOrAll <em>Port Or All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Or All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAll
   * @generated
   */
  EClass getPortOrAll();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.PortOrAll#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAll#getPort()
   * @see #getPortOrAll()
   * @generated
   */
  EReference getPortOrAll_Port();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.PortOrAll#getArrayRefs <em>Array Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Array Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortOrAll#getArrayRefs()
   * @see #getPortOrAll()
   * @generated
   */
  EReference getPortOrAll_ArrayRefs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement <em>Raise Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Raise Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RaiseStatement
   * @generated
   */
  EClass getRaiseStatement();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RaiseStatement#getPort()
   * @see #getRaiseStatement()
   * @generated
   */
  EReference getRaiseStatement_Port();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getArrayRefs <em>Array Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Array Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RaiseStatement#getArrayRefs()
   * @see #getRaiseStatement()
   * @generated
   */
  EReference getRaiseStatement_ArrayRefs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RaiseStatement#getOp()
   * @see #getRaiseStatement()
   * @generated
   */
  EReference getRaiseStatement_Op();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortRaiseOp <em>Port Raise Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Raise Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRaiseOp
   * @generated
   */
  EClass getPortRaiseOp();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.PortRaiseOp#getSignature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Signature</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRaiseOp#getSignature()
   * @see #getPortRaiseOp()
   * @generated
   */
  EReference getPortRaiseOp_Signature();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRaiseOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRaiseOp#getTemplate()
   * @see #getPortRaiseOp()
   * @generated
   */
  EReference getPortRaiseOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortRaiseOp#getTo <em>To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>To</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortRaiseOp#getTo()
   * @see #getPortRaiseOp()
   * @generated
   */
  EReference getPortRaiseOp_To();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReplyStatement <em>Reply Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reply Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReplyStatement
   * @generated
   */
  EClass getReplyStatement();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.ReplyStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReplyStatement#getPort()
   * @see #getReplyStatement()
   * @generated
   */
  EReference getReplyStatement_Port();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ReplyStatement#getArrayRefs <em>Array Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Array Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReplyStatement#getArrayRefs()
   * @see #getReplyStatement()
   * @generated
   */
  EReference getReplyStatement_ArrayRefs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReplyStatement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReplyStatement#getOp()
   * @see #getReplyStatement()
   * @generated
   */
  EReference getReplyStatement_Op();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortReplyOp <em>Port Reply Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Reply Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReplyOp
   * @generated
   */
  EClass getPortReplyOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortReplyOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReplyOp#getTemplate()
   * @see #getPortReplyOp()
   * @generated
   */
  EReference getPortReplyOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortReplyOp#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReplyOp#getValue()
   * @see #getPortReplyOp()
   * @generated
   */
  EReference getPortReplyOp_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortReplyOp#getTo <em>To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>To</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortReplyOp#getTo()
   * @see #getPortReplyOp()
   * @generated
   */
  EReference getPortReplyOp_To();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReplyValue <em>Reply Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reply Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReplyValue
   * @generated
   */
  EClass getReplyValue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReplyValue#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReplyValue#getExpr()
   * @see #getReplyValue()
   * @generated
   */
  EReference getReplyValue_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallStatement <em>Call Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallStatement
   * @generated
   */
  EClass getCallStatement();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.CallStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallStatement#getPort()
   * @see #getCallStatement()
   * @generated
   */
  EReference getCallStatement_Port();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.CallStatement#getArrayRefs <em>Array Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Array Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallStatement#getArrayRefs()
   * @see #getCallStatement()
   * @generated
   */
  EReference getCallStatement_ArrayRefs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallStatement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallStatement#getOp()
   * @see #getCallStatement()
   * @generated
   */
  EReference getCallStatement_Op();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallStatement#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallStatement#getBody()
   * @see #getCallStatement()
   * @generated
   */
  EReference getCallStatement_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortCallOp <em>Port Call Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Call Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCallOp
   * @generated
   */
  EClass getPortCallOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortCallOp#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCallOp#getParams()
   * @see #getPortCallOp()
   * @generated
   */
  EReference getPortCallOp_Params();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortCallOp#getTo <em>To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>To</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCallOp#getTo()
   * @see #getPortCallOp()
   * @generated
   */
  EReference getPortCallOp_To();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallParameters <em>Call Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Parameters</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallParameters
   * @generated
   */
  EClass getCallParameters();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallParameters#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallParameters#getTemplate()
   * @see #getCallParameters()
   * @generated
   */
  EReference getCallParameters_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallParameters#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallParameters#getValue()
   * @see #getCallParameters()
   * @generated
   */
  EReference getCallParameters_Value();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallTimerValue <em>Call Timer Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Timer Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallTimerValue
   * @generated
   */
  EClass getCallTimerValue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallTimerValue#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallTimerValue#getExpr()
   * @see #getCallTimerValue()
   * @generated
   */
  EReference getCallTimerValue_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortCallBody <em>Port Call Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Call Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCallBody
   * @generated
   */
  EClass getPortCallBody();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortCallBody#getCallBody <em>Call Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Call Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortCallBody#getCallBody()
   * @see #getPortCallBody()
   * @generated
   */
  EReference getPortCallBody_CallBody();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatementList <em>Call Body Statement List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Body Statement List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatementList
   * @generated
   */
  EClass getCallBodyStatementList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatementList#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatementList#getBody()
   * @see #getCallBodyStatementList()
   * @generated
   */
  EReference getCallBodyStatementList_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatement <em>Call Body Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Body Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatement
   * @generated
   */
  EClass getCallBodyStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatement#getCall <em>Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Call</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatement#getCall()
   * @see #getCallBodyStatement()
   * @generated
   */
  EReference getCallBodyStatement_Call();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallBodyStatement#getBlock <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Block</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyStatement#getBlock()
   * @see #getCallBodyStatement()
   * @generated
   */
  EReference getCallBodyStatement_Block();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard <em>Call Body Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Body Guard</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyGuard
   * @generated
   */
  EClass getCallBodyGuard();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard#getGuard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyGuard#getGuard()
   * @see #getCallBodyGuard()
   * @generated
   */
  EReference getCallBodyGuard_Guard();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard#getOps <em>Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ops</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyGuard#getOps()
   * @see #getCallBodyGuard()
   * @generated
   */
  EReference getCallBodyGuard_Ops();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CallBodyOps <em>Call Body Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Body Ops</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyOps
   * @generated
   */
  EClass getCallBodyOps();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallBodyOps#getReply <em>Reply</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Reply</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyOps#getReply()
   * @see #getCallBodyOps()
   * @generated
   */
  EReference getCallBodyOps_Reply();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CallBodyOps#getCatch <em>Catch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Catch</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CallBodyOps#getCatch()
   * @see #getCallBodyOps()
   * @generated
   */
  EReference getCallBodyOps_Catch();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SendStatement <em>Send Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Send Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SendStatement
   * @generated
   */
  EClass getSendStatement();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.SendStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SendStatement#getPort()
   * @see #getSendStatement()
   * @generated
   */
  EReference getSendStatement_Port();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SendStatement#getArryRefs <em>Arry Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Arry Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SendStatement#getArryRefs()
   * @see #getSendStatement()
   * @generated
   */
  EReference getSendStatement_ArryRefs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SendStatement#getSend <em>Send</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Send</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SendStatement#getSend()
   * @see #getSendStatement()
   * @generated
   */
  EReference getSendStatement_Send();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortSendOp <em>Port Send Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Send Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortSendOp
   * @generated
   */
  EClass getPortSendOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortSendOp#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortSendOp#getTemplate()
   * @see #getPortSendOp()
   * @generated
   */
  EReference getPortSendOp_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortSendOp#getTo <em>To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>To</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortSendOp#getTo()
   * @see #getPortSendOp()
   * @generated
   */
  EReference getPortSendOp_To();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ToClause <em>To Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>To Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ToClause
   * @generated
   */
  EClass getToClause();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ToClause#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ToClause#getTemplate()
   * @see #getToClause()
   * @generated
   */
  EReference getToClause_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ToClause#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ToClause#getRef()
   * @see #getToClause()
   * @generated
   */
  EReference getToClause_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList <em>Function Def List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Def List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDefList
   * @generated
   */
  EClass getFunctionDefList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocDef <em>Loc Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Loc Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocDef()
   * @see #getFunctionDefList()
   * @generated
   */
  EReference getFunctionDefList_LocDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocInst <em>Loc Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Loc Inst</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocInst()
   * @see #getFunctionDefList()
   * @generated
   */
  EReference getFunctionDefList_LocInst();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getWs <em>Ws</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ws</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDefList#getWs()
   * @see #getFunctionDefList()
   * @generated
   */
  EReference getFunctionDefList_Ws();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDefList#getSc()
   * @see #getFunctionDefList()
   * @generated
   */
  EAttribute getFunctionDefList_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef <em>Function Local Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Local Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalDef
   * @generated
   */
  EClass getFunctionLocalDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getConstDef <em>Const Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getConstDef()
   * @see #getFunctionLocalDef()
   * @generated
   */
  EReference getFunctionLocalDef_ConstDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getTemplateDef <em>Template Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getTemplateDef()
   * @see #getFunctionLocalDef()
   * @generated
   */
  EReference getFunctionLocalDef_TemplateDef();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalInst <em>Function Local Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Local Inst</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalInst
   * @generated
   */
  EClass getFunctionLocalInst();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalInst#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalInst#getVariable()
   * @see #getFunctionLocalInst()
   * @generated
   */
  EReference getFunctionLocalInst_Variable();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalInst#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionLocalInst#getTimer()
   * @see #getFunctionLocalInst()
   * @generated
   */
  EReference getFunctionLocalInst_Timer();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerInstance <em>Timer Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerInstance
   * @generated
   */
  EClass getTimerInstance();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimerInstance#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerInstance#getList()
   * @see #getTimerInstance()
   * @generated
   */
  EReference getTimerInstance_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.VarInstance <em>Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance
   * @generated
   */
  EClass getVarInstance();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getListMod <em>List Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>List Mod</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getListMod()
   * @see #getVarInstance()
   * @generated
   */
  EAttribute getVarInstance_ListMod();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getListType <em>List Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getListType()
   * @see #getVarInstance()
   * @generated
   */
  EReference getVarInstance_ListType();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getList()
   * @see #getVarInstance()
   * @generated
   */
  EReference getVarInstance_List();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getResTemplate <em>Res Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Res Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getResTemplate()
   * @see #getVarInstance()
   * @generated
   */
  EReference getVarInstance_ResTemplate();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getTempMod <em>Temp Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Temp Mod</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getTempMod()
   * @see #getVarInstance()
   * @generated
   */
  EAttribute getVarInstance_TempMod();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getType()
   * @see #getVarInstance()
   * @generated
   */
  EReference getVarInstance_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getTempList <em>Temp List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Temp List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarInstance#getTempList()
   * @see #getVarInstance()
   * @generated
   */
  EReference getVarInstance_TempList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.VarList <em>Var List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarList
   * @generated
   */
  EClass getVarList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.VarList#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VarList#getVariables()
   * @see #getVarList()
   * @generated
   */
  EReference getVarList_Variables();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ModuleOrGroup <em>Module Or Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module Or Group</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ModuleOrGroup
   * @generated
   */
  EClass getModuleOrGroup();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReferencedType <em>Referenced Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Referenced Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReferencedType
   * @generated
   */
  EClass getReferencedType();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TypeDef <em>Type Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeDef
   * @generated
   */
  EClass getTypeDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TypeDef#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeDef#getBody()
   * @see #getTypeDef()
   * @generated
   */
  EReference getTypeDef_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TypeDefBody <em>Type Def Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Def Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeDefBody
   * @generated
   */
  EClass getTypeDefBody();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TypeDefBody#getStructured <em>Structured</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Structured</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeDefBody#getStructured()
   * @see #getTypeDefBody()
   * @generated
   */
  EReference getTypeDefBody_Structured();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TypeDefBody#getSub <em>Sub</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sub</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeDefBody#getSub()
   * @see #getTypeDefBody()
   * @generated
   */
  EReference getTypeDefBody_Sub();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SubTypeDef <em>Sub Type Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Type Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDef
   * @generated
   */
  EClass getSubTypeDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SubTypeDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDef#getType()
   * @see #getSubTypeDef()
   * @generated
   */
  EReference getSubTypeDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SubTypeDef#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDef#getArray()
   * @see #getSubTypeDef()
   * @generated
   */
  EReference getSubTypeDef_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SubTypeDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDef#getSpec()
   * @see #getSubTypeDef()
   * @generated
   */
  EReference getSubTypeDef_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SubTypeDefNamed <em>Sub Type Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Type Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
   * @generated
   */
  EClass getSubTypeDefNamed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef <em>Structured Type Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Structured Type Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef
   * @generated
   */
  EClass getStructuredTypeDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecord <em>Record</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Record</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecord()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_Record();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getUnion <em>Union</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Union</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getUnion()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_Union();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSet <em>Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Set</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSet()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_Set();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecordOf <em>Record Of</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Record Of</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecordOf()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_RecordOf();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSetOf <em>Set Of</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Set Of</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSetOf()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_SetOf();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getEnumDef <em>Enum Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Enum Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getEnumDef()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_EnumDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getPort()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_Port();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getComponent()
   * @see #getStructuredTypeDef()
   * @generated
   */
  EReference getStructuredTypeDef_Component();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RecordDef <em>Record Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordDef
   * @generated
   */
  EClass getRecordDef();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RecordDefNamed <em>Record Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordDefNamed
   * @generated
   */
  EClass getRecordDefNamed();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RecordDefNamed#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordDefNamed#getBody()
   * @see #getRecordDefNamed()
   * @generated
   */
  EReference getRecordDefNamed_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef <em>Record Of Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Of Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDef
   * @generated
   */
  EClass getRecordOfDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getLenght <em>Lenght</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Lenght</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDef#getLenght()
   * @see #getRecordOfDef()
   * @generated
   */
  EReference getRecordOfDef_Lenght();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDef#getType()
   * @see #getRecordOfDef()
   * @generated
   */
  EReference getRecordOfDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getNested <em>Nested</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDef#getNested()
   * @see #getRecordOfDef()
   * @generated
   */
  EReference getRecordOfDef_Nested();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RecordOfDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDef#getSpec()
   * @see #getRecordOfDef()
   * @generated
   */
  EReference getRecordOfDef_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RecordOfDefNamed <em>Record Of Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Of Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
   * @generated
   */
  EClass getRecordOfDefNamed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StructDefBody <em>Struct Def Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Struct Def Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructDefBody
   * @generated
   */
  EClass getStructDefBody();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.StructDefBody#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructDefBody#getDefs()
   * @see #getStructDefBody()
   * @generated
   */
  EReference getStructDefBody_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef <em>Struct Field Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Struct Field Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef
   * @generated
   */
  EClass getStructFieldDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef#getType()
   * @see #getStructFieldDef()
   * @generated
   */
  EReference getStructFieldDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getNestedType <em>Nested Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef#getNestedType()
   * @see #getStructFieldDef()
   * @generated
   */
  EReference getStructFieldDef_NestedType();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef#getArray()
   * @see #getStructFieldDef()
   * @generated
   */
  EReference getStructFieldDef_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef#getSpec()
   * @see #getStructFieldDef()
   * @generated
   */
  EReference getStructFieldDef_Spec();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.StructFieldDef#getOptional <em>Optional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Optional</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StructFieldDef#getOptional()
   * @see #getStructFieldDef()
   * @generated
   */
  EAttribute getStructFieldDef_Optional();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SetOfDef <em>Set Of Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Set Of Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDef
   * @generated
   */
  EClass getSetOfDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getSetLength <em>Set Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Set Length</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDef#getSetLength()
   * @see #getSetOfDef()
   * @generated
   */
  EReference getSetOfDef_SetLength();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDef#getType()
   * @see #getSetOfDef()
   * @generated
   */
  EReference getSetOfDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getNested <em>Nested</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDef#getNested()
   * @see #getSetOfDef()
   * @generated
   */
  EReference getSetOfDef_Nested();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SetOfDef#getSetSpec <em>Set Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Set Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDef#getSetSpec()
   * @see #getSetOfDef()
   * @generated
   */
  EReference getSetOfDef_SetSpec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SetOfDefNamed <em>Set Of Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Set Of Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetOfDefNamed
   * @generated
   */
  EClass getSetOfDefNamed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortDef <em>Port Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDef
   * @generated
   */
  EClass getPortDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortDef#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDef#getBody()
   * @see #getPortDef()
   * @generated
   */
  EReference getPortDef_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortDefBody <em>Port Def Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Def Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDefBody
   * @generated
   */
  EClass getPortDefBody();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortDefBody#getAttribs <em>Attribs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Attribs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDefBody#getAttribs()
   * @see #getPortDefBody()
   * @generated
   */
  EReference getPortDefBody_Attribs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs <em>Port Def Attribs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Def Attribs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDefAttribs
   * @generated
   */
  EClass getPortDefAttribs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMessage <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Message</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMessage()
   * @see #getPortDefAttribs()
   * @generated
   */
  EReference getPortDefAttribs_Message();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getProcedure <em>Procedure</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Procedure</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDefAttribs#getProcedure()
   * @see #getPortDefAttribs()
   * @generated
   */
  EReference getPortDefAttribs_Procedure();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Mixed</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortDefAttribs#getMixed()
   * @see #getPortDefAttribs()
   * @generated
   */
  EReference getPortDefAttribs_Mixed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MixedAttribs <em>Mixed Attribs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mixed Attribs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MixedAttribs
   * @generated
   */
  EClass getMixedAttribs();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MixedAttribs#getAddressDecls <em>Address Decls</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Address Decls</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MixedAttribs#getAddressDecls()
   * @see #getMixedAttribs()
   * @generated
   */
  EReference getMixedAttribs_AddressDecls();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MixedAttribs#getMixedLists <em>Mixed Lists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Mixed Lists</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MixedAttribs#getMixedLists()
   * @see #getMixedAttribs()
   * @generated
   */
  EReference getMixedAttribs_MixedLists();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MixedAttribs#getConfigDefs <em>Config Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Config Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MixedAttribs#getConfigDefs()
   * @see #getMixedAttribs()
   * @generated
   */
  EReference getMixedAttribs_ConfigDefs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MixedList <em>Mixed List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mixed List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MixedList
   * @generated
   */
  EClass getMixedList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MixedList#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MixedList#getList()
   * @see #getMixedList()
   * @generated
   */
  EReference getMixedList_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList <em>Proc Or Type List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Proc Or Type List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrTypeList
   * @generated
   */
  EClass getProcOrTypeList();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getAll()
   * @see #getProcOrTypeList()
   * @generated
   */
  EAttribute getProcOrTypeList_All();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getType()
   * @see #getProcOrTypeList()
   * @generated
   */
  EReference getProcOrTypeList_Type();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ProcOrType <em>Proc Or Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Proc Or Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrType
   * @generated
   */
  EClass getProcOrType();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ProcOrType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcOrType#getType()
   * @see #getProcOrType()
   * @generated
   */
  EReference getProcOrType_Type();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MessageAttribs <em>Message Attribs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Message Attribs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MessageAttribs
   * @generated
   */
  EClass getMessageAttribs();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MessageAttribs#getAdresses <em>Adresses</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Adresses</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MessageAttribs#getAdresses()
   * @see #getMessageAttribs()
   * @generated
   */
  EReference getMessageAttribs_Adresses();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MessageAttribs#getMessages <em>Messages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Messages</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MessageAttribs#getMessages()
   * @see #getMessageAttribs()
   * @generated
   */
  EReference getMessageAttribs_Messages();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MessageAttribs#getConfigs <em>Configs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Configs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MessageAttribs#getConfigs()
   * @see #getMessageAttribs()
   * @generated
   */
  EReference getMessageAttribs_Configs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef <em>Config Param Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Config Param Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigParamDef
   * @generated
   */
  EClass getConfigParamDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef#getMap <em>Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Map</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigParamDef#getMap()
   * @see #getConfigParamDef()
   * @generated
   */
  EReference getConfigParamDef_Map();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef#getUnmap <em>Unmap</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Unmap</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigParamDef#getUnmap()
   * @see #getConfigParamDef()
   * @generated
   */
  EReference getConfigParamDef_Unmap();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MapParamDef <em>Map Param Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Map Param Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MapParamDef
   * @generated
   */
  EClass getMapParamDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MapParamDef#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MapParamDef#getValues()
   * @see #getMapParamDef()
   * @generated
   */
  EReference getMapParamDef_Values();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.UnmapParamDef <em>Unmap Param Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unmap Param Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnmapParamDef
   * @generated
   */
  EClass getUnmapParamDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.UnmapParamDef#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnmapParamDef#getValues()
   * @see #getUnmapParamDef()
   * @generated
   */
  EReference getUnmapParamDef_Values();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AddressDecl <em>Address Decl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Address Decl</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AddressDecl
   * @generated
   */
  EClass getAddressDecl();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AddressDecl#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AddressDecl#getType()
   * @see #getAddressDecl()
   * @generated
   */
  EReference getAddressDecl_Type();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs <em>Procedure Attribs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Procedure Attribs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureAttribs
   * @generated
   */
  EClass getProcedureAttribs();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getAddresses <em>Addresses</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Addresses</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getAddresses()
   * @see #getProcedureAttribs()
   * @generated
   */
  EReference getProcedureAttribs_Addresses();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getProcs <em>Procs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Procs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getProcs()
   * @see #getProcedureAttribs()
   * @generated
   */
  EReference getProcedureAttribs_Procs();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getConfigs <em>Configs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Configs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getConfigs()
   * @see #getProcedureAttribs()
   * @generated
   */
  EReference getProcedureAttribs_Configs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentDef <em>Component Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDef
   * @generated
   */
  EClass getComponentDef();

  /**
   * Returns the meta object for the reference list '{@link de.ugoe.cs.swe.tTCN3.ComponentDef#getExtends <em>Extends</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Extends</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDef#getExtends()
   * @see #getComponentDef()
   * @generated
   */
  EReference getComponentDef_Extends();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ComponentDef#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDef#getDefs()
   * @see #getComponentDef()
   * @generated
   */
  EReference getComponentDef_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList <em>Component Def List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Def List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDefList
   * @generated
   */
  EClass getComponentDefList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDefList#getElement()
   * @see #getComponentDefList()
   * @generated
   */
  EReference getComponentDefList_Element();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getWst <em>Wst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Wst</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDefList#getWst()
   * @see #getComponentDefList()
   * @generated
   */
  EReference getComponentDefList_Wst();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ComponentDefList#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentDefList#getSc()
   * @see #getComponentDefList()
   * @generated
   */
  EAttribute getComponentDefList_Sc();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortInstance <em>Port Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortInstance
   * @generated
   */
  EClass getPortInstance();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.PortInstance#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortInstance#getRef()
   * @see #getPortInstance()
   * @generated
   */
  EReference getPortInstance_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.PortInstance#getInstances <em>Instances</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Instances</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortInstance#getInstances()
   * @see #getPortInstance()
   * @generated
   */
  EReference getPortInstance_Instances();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PortElement <em>Port Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortElement
   * @generated
   */
  EClass getPortElement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PortElement#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PortElement#getArray()
   * @see #getPortElement()
   * @generated
   */
  EReference getPortElement_Array();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef <em>Component Element Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Element Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef
   * @generated
   */
  EClass getComponentElementDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef#getPort()
   * @see #getComponentElementDef()
   * @generated
   */
  EReference getComponentElementDef_Port();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef#getVariable()
   * @see #getComponentElementDef()
   * @generated
   */
  EReference getComponentElementDef_Variable();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTimer()
   * @see #getComponentElementDef()
   * @generated
   */
  EReference getComponentElementDef_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef#getConst()
   * @see #getComponentElementDef()
   * @generated
   */
  EReference getComponentElementDef_Const();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTemplate()
   * @see #getComponentElementDef()
   * @generated
   */
  EReference getComponentElementDef_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ProcedureList <em>Procedure List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Procedure List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureList
   * @generated
   */
  EClass getProcedureList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ProcedureList#getAllOrSigList <em>All Or Sig List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>All Or Sig List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ProcedureList#getAllOrSigList()
   * @see #getProcedureList()
   * @generated
   */
  EReference getProcedureList_AllOrSigList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList <em>All Or Signature List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Or Signature List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllOrSignatureList
   * @generated
   */
  EClass getAllOrSignatureList();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getAll()
   * @see #getAllOrSignatureList()
   * @generated
   */
  EAttribute getAllOrSignatureList_All();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getSignatureList <em>Signature List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Signature List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllOrSignatureList#getSignatureList()
   * @see #getAllOrSignatureList()
   * @generated
   */
  EReference getAllOrSignatureList_SignatureList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SignatureList <em>Signature List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signature List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureList
   * @generated
   */
  EClass getSignatureList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SignatureList#getSigs <em>Sigs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sigs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SignatureList#getSigs()
   * @see #getSignatureList()
   * @generated
   */
  EReference getSignatureList_Sigs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.EnumDef <em>Enum Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.EnumDef
   * @generated
   */
  EClass getEnumDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.EnumDef#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.EnumDef#getList()
   * @see #getEnumDef()
   * @generated
   */
  EReference getEnumDef_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.EnumDefNamed <em>Enum Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.EnumDefNamed
   * @generated
   */
  EClass getEnumDefNamed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedTypeDef <em>Nested Type Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Type Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedTypeDef
   * @generated
   */
  EClass getNestedTypeDef();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedRecordDef <em>Nested Record Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Record Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordDef
   * @generated
   */
  EClass getNestedRecordDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.NestedRecordDef#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordDef#getDefs()
   * @see #getNestedRecordDef()
   * @generated
   */
  EReference getNestedRecordDef_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedUnionDef <em>Nested Union Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Union Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedUnionDef
   * @generated
   */
  EClass getNestedUnionDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.NestedUnionDef#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedUnionDef#getDefs()
   * @see #getNestedUnionDef()
   * @generated
   */
  EReference getNestedUnionDef_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedSetDef <em>Nested Set Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Set Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetDef
   * @generated
   */
  EClass getNestedSetDef();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.NestedSetDef#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetDef#getDefs()
   * @see #getNestedSetDef()
   * @generated
   */
  EReference getNestedSetDef_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedRecordOfDef <em>Nested Record Of Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Record Of Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordOfDef
   * @generated
   */
  EClass getNestedRecordOfDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedRecordOfDef#getLength <em>Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Length</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordOfDef#getLength()
   * @see #getNestedRecordOfDef()
   * @generated
   */
  EReference getNestedRecordOfDef_Length();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedRecordOfDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordOfDef#getType()
   * @see #getNestedRecordOfDef()
   * @generated
   */
  EReference getNestedRecordOfDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedRecordOfDef#getNested <em>Nested</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedRecordOfDef#getNested()
   * @see #getNestedRecordOfDef()
   * @generated
   */
  EReference getNestedRecordOfDef_Nested();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef <em>Nested Set Of Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Set Of Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetOfDef
   * @generated
   */
  EClass getNestedSetOfDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getLength <em>Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Length</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getLength()
   * @see #getNestedSetOfDef()
   * @generated
   */
  EReference getNestedSetOfDef_Length();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getType()
   * @see #getNestedSetOfDef()
   * @generated
   */
  EReference getNestedSetOfDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getNested <em>Nested</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedSetOfDef#getNested()
   * @see #getNestedSetOfDef()
   * @generated
   */
  EReference getNestedSetOfDef_Nested();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NestedEnumDef <em>Nested Enum Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Enum Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedEnumDef
   * @generated
   */
  EClass getNestedEnumDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.NestedEnumDef#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NestedEnumDef#getList()
   * @see #getNestedEnumDef()
   * @generated
   */
  EReference getNestedEnumDef_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MessageList <em>Message List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Message List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MessageList
   * @generated
   */
  EClass getMessageList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MessageList#getAllOrTypeList <em>All Or Type List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>All Or Type List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MessageList#getAllOrTypeList()
   * @see #getMessageList()
   * @generated
   */
  EReference getMessageList_AllOrTypeList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList <em>All Or Type List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Or Type List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllOrTypeList
   * @generated
   */
  EClass getAllOrTypeList();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllOrTypeList#getAll()
   * @see #getAllOrTypeList()
   * @generated
   */
  EAttribute getAllOrTypeList_All();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllOrTypeList#getTypeList <em>Type List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllOrTypeList#getTypeList()
   * @see #getAllOrTypeList()
   * @generated
   */
  EReference getAllOrTypeList_TypeList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TypeList <em>Type List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeList
   * @generated
   */
  EClass getTypeList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TypeList#getTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Types</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TypeList#getTypes()
   * @see #getTypeList()
   * @generated
   */
  EReference getTypeList_Types();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.UnionDef <em>Union Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionDef
   * @generated
   */
  EClass getUnionDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnionDef#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionDef#getBody()
   * @see #getUnionDef()
   * @generated
   */
  EReference getUnionDef_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.UnionDefNamed <em>Union Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionDefNamed
   * @generated
   */
  EClass getUnionDefNamed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.UnionDefBody <em>Union Def Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Def Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionDefBody
   * @generated
   */
  EClass getUnionDefBody();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.UnionDefBody#getDefs <em>Defs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Defs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionDefBody#getDefs()
   * @see #getUnionDefBody()
   * @generated
   */
  EReference getUnionDefBody_Defs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.EnumerationList <em>Enumeration List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enumeration List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.EnumerationList
   * @generated
   */
  EClass getEnumerationList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.EnumerationList#getEnums <em>Enums</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Enums</em>'.
   * @see de.ugoe.cs.swe.tTCN3.EnumerationList#getEnums()
   * @see #getEnumerationList()
   * @generated
   */
  EReference getEnumerationList_Enums();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Enumeration <em>Enumeration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enumeration</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Enumeration
   * @generated
   */
  EClass getEnumeration();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.UnionFieldDef <em>Union Field Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Field Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionFieldDef
   * @generated
   */
  EClass getUnionFieldDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnionFieldDef#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionFieldDef#getType()
   * @see #getUnionFieldDef()
   * @generated
   */
  EReference getUnionFieldDef_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnionFieldDef#getNestedType <em>Nested Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionFieldDef#getNestedType()
   * @see #getUnionFieldDef()
   * @generated
   */
  EReference getUnionFieldDef_NestedType();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnionFieldDef#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionFieldDef#getArray()
   * @see #getUnionFieldDef()
   * @generated
   */
  EReference getUnionFieldDef_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.UnionFieldDef#getSubType <em>Sub Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sub Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.UnionFieldDef#getSubType()
   * @see #getUnionFieldDef()
   * @generated
   */
  EReference getUnionFieldDef_SubType();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SetDef <em>Set Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Set Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetDef
   * @generated
   */
  EClass getSetDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SetDef#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetDef#getBody()
   * @see #getSetDef()
   * @generated
   */
  EReference getSetDef_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SetDefNamed <em>Set Def Named</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Set Def Named</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SetDefNamed
   * @generated
   */
  EClass getSetDefNamed();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SubTypeSpec <em>Sub Type Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Type Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeSpec
   * @generated
   */
  EClass getSubTypeSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SubTypeSpec#getLength <em>Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Length</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubTypeSpec#getLength()
   * @see #getSubTypeSpec()
   * @generated
   */
  EReference getSubTypeSpec_Length();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllowedValuesSpec <em>Allowed Values Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Allowed Values Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllowedValuesSpec
   * @generated
   */
  EClass getAllowedValuesSpec();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.AllowedValuesSpec#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllowedValuesSpec#getTemplate()
   * @see #getAllowedValuesSpec()
   * @generated
   */
  EReference getAllowedValuesSpec_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateOrRange <em>Template Or Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Or Range</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrRange
   * @generated
   */
  EClass getTemplateOrRange();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateOrRange#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Range</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrRange#getRange()
   * @see #getTemplateOrRange()
   * @generated
   */
  EReference getTemplateOrRange_Range();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateOrRange#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrRange#getTemplate()
   * @see #getTemplateOrRange()
   * @generated
   */
  EReference getTemplateOrRange_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateOrRange#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOrRange#getType()
   * @see #getTemplateOrRange()
   * @generated
   */
  EReference getTemplateOrRange_Type();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RangeDef <em>Range Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Range Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RangeDef
   * @generated
   */
  EClass getRangeDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RangeDef#getB1 <em>B1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>B1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RangeDef#getB1()
   * @see #getRangeDef()
   * @generated
   */
  EReference getRangeDef_B1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RangeDef#getB2 <em>B2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>B2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RangeDef#getB2()
   * @see #getRangeDef()
   * @generated
   */
  EReference getRangeDef_B2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Bound <em>Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bound</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Bound
   * @generated
   */
  EClass getBound();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Bound#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Bound#getExpr()
   * @see #getBound()
   * @generated
   */
  EReference getBound_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.StringLength <em>String Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Length</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StringLength
   * @generated
   */
  EClass getStringLength();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StringLength#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StringLength#getExpr()
   * @see #getStringLength()
   * @generated
   */
  EReference getStringLength_Expr();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.StringLength#getBound <em>Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bound</em>'.
   * @see de.ugoe.cs.swe.tTCN3.StringLength#getBound()
   * @see #getStringLength()
   * @generated
   */
  EReference getStringLength_Bound();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CharStringMatch <em>Char String Match</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Char String Match</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CharStringMatch
   * @generated
   */
  EClass getCharStringMatch();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.CharStringMatch#getPattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Pattern</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CharStringMatch#getPattern()
   * @see #getCharStringMatch()
   * @generated
   */
  EReference getCharStringMatch_Pattern();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PatternParticle <em>Pattern Particle</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern Particle</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PatternParticle
   * @generated
   */
  EClass getPatternParticle();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateDef <em>Template Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef
   * @generated
   */
  EClass getTemplateDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getRestriction <em>Restriction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Restriction</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef#getRestriction()
   * @see #getTemplateDef()
   * @generated
   */
  EReference getTemplateDef_Restriction();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getFuzzy <em>Fuzzy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fuzzy</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef#getFuzzy()
   * @see #getTemplateDef()
   * @generated
   */
  EAttribute getTemplateDef_Fuzzy();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getBase <em>Base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Base</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef#getBase()
   * @see #getTemplateDef()
   * @generated
   */
  EReference getTemplateDef_Base();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getDerived <em>Derived</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Derived</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef#getDerived()
   * @see #getTemplateDef()
   * @generated
   */
  EReference getTemplateDef_Derived();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateDef#getBody()
   * @see #getTemplateDef()
   * @generated
   */
  EReference getTemplateDef_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DerivedDef <em>Derived Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Derived Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DerivedDef
   * @generated
   */
  EClass getDerivedDef();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.DerivedDef#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DerivedDef#getTemplate()
   * @see #getDerivedDef()
   * @generated
   */
  EReference getDerivedDef_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BaseTemplate <em>Base Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BaseTemplate
   * @generated
   */
  EClass getBaseTemplate();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BaseTemplate#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BaseTemplate#getType()
   * @see #getBaseTemplate()
   * @generated
   */
  EReference getBaseTemplate_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.BaseTemplate#getParList <em>Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BaseTemplate#getParList()
   * @see #getBaseTemplate()
   * @generated
   */
  EReference getBaseTemplate_ParList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TempVarList <em>Temp Var List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Temp Var List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TempVarList
   * @generated
   */
  EClass getTempVarList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TempVarList#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TempVarList#getVariables()
   * @see #getTempVarList()
   * @generated
   */
  EReference getTempVarList_Variables();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerVarInstance <em>Timer Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Var Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerVarInstance
   * @generated
   */
  EClass getTimerVarInstance();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleVarInstance <em>Single Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Var Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleVarInstance
   * @generated
   */
  EClass getSingleVarInstance();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleVarInstance#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleVarInstance#getArray()
   * @see #getSingleVarInstance()
   * @generated
   */
  EReference getSingleVarInstance_Array();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.SingleVarInstance#getAc <em>Ac</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ac</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleVarInstance#getAc()
   * @see #getSingleVarInstance()
   * @generated
   */
  EAttribute getSingleVarInstance_Ac();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleVarInstance#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleVarInstance#getExpr()
   * @see #getSingleVarInstance()
   * @generated
   */
  EReference getSingleVarInstance_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance <em>Single Temp Var Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Temp Var Instance</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
   * @generated
   */
  EClass getSingleTempVarInstance();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getArray()
   * @see #getSingleTempVarInstance()
   * @generated
   */
  EReference getSingleTempVarInstance_Array();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getAc <em>Ac</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ac</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getAc()
   * @see #getSingleTempVarInstance()
   * @generated
   */
  EAttribute getSingleTempVarInstance_Ac();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getTemplate()
   * @see #getSingleTempVarInstance()
   * @generated
   */
  EReference getSingleTempVarInstance_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate <em>In Line Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>In Line Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InLineTemplate
   * @generated
   */
  EClass getInLineTemplate();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InLineTemplate#getType()
   * @see #getInLineTemplate()
   * @generated
   */
  EReference getInLineTemplate_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getDerived <em>Derived</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Derived</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InLineTemplate#getDerived()
   * @see #getInLineTemplate()
   * @generated
   */
  EReference getInLineTemplate_Derived();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.InLineTemplate#getTemplate()
   * @see #getInLineTemplate()
   * @generated
   */
  EReference getInLineTemplate_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DerivedRefWithParList <em>Derived Ref With Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Derived Ref With Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DerivedRefWithParList
   * @generated
   */
  EClass getDerivedRefWithParList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DerivedRefWithParList#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DerivedRefWithParList#getTemplate()
   * @see #getDerivedRefWithParList()
   * @generated
   */
  EReference getDerivedRefWithParList_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateBody <em>Template Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateBody
   * @generated
   */
  EClass getTemplateBody();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getSimple <em>Simple</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Simple</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateBody#getSimple()
   * @see #getTemplateBody()
   * @generated
   */
  EReference getTemplateBody_Simple();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Field</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateBody#getField()
   * @see #getTemplateBody()
   * @generated
   */
  EReference getTemplateBody_Field();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateBody#getArray()
   * @see #getTemplateBody()
   * @generated
   */
  EReference getTemplateBody_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateBody#getExtra <em>Extra</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Extra</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateBody#getExtra()
   * @see #getTemplateBody()
   * @generated
   */
  EReference getTemplateBody_Extra();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExtraMatchingAttributes <em>Extra Matching Attributes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extra Matching Attributes</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtraMatchingAttributes
   * @generated
   */
  EClass getExtraMatchingAttributes();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldSpecList <em>Field Spec List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Spec List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpecList
   * @generated
   */
  EClass getFieldSpecList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FieldSpecList#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpecList#getSpec()
   * @see #getFieldSpecList()
   * @generated
   */
  EReference getFieldSpecList_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldSpec <em>Field Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpec
   * @generated
   */
  EClass getFieldSpec();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.FieldSpec#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpec#getRef()
   * @see #getFieldSpec()
   * @generated
   */
  EReference getFieldSpec_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FieldSpec#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldSpec#getBody()
   * @see #getFieldSpec()
   * @generated
   */
  EReference getFieldSpec_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SimpleSpec <em>Simple Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SimpleSpec
   * @generated
   */
  EClass getSimpleSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SimpleSpec#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SimpleSpec#getExpr()
   * @see #getSimpleSpec()
   * @generated
   */
  EReference getSimpleSpec_Expr();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SimpleSpec#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SimpleSpec#getSpec()
   * @see #getSimpleSpec()
   * @generated
   */
  EReference getSimpleSpec_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec <em>Simple Template Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Template Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec
   * @generated
   */
  EClass getSimpleTemplateSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec#getExpr()
   * @see #getSimpleTemplateSpec()
   * @generated
   */
  EReference getSimpleTemplateSpec_Expr();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SimpleTemplateSpec#getSpec()
   * @see #getSimpleTemplateSpec()
   * @generated
   */
  EReference getSimpleTemplateSpec_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression <em>Single Template Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Template Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTemplateExpression
   * @generated
   */
  EClass getSingleTemplateExpression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getSymbol <em>Symbol</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Symbol</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getSymbol()
   * @see #getSingleTemplateExpression()
   * @generated
   */
  EReference getSingleTemplateExpression_Symbol();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getList()
   * @see #getSingleTemplateExpression()
   * @generated
   */
  EReference getSingleTemplateExpression_List();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getExtended <em>Extended</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Extended</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getExtended()
   * @see #getSingleTemplateExpression()
   * @generated
   */
  EReference getSingleTemplateExpression_Extended();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateRefWithParList <em>Template Ref With Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Ref With Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRefWithParList
   * @generated
   */
  EClass getTemplateRefWithParList();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TemplateRefWithParList#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRefWithParList#getTemplate()
   * @see #getTemplateRefWithParList()
   * @generated
   */
  EReference getTemplateRefWithParList_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateRefWithParList#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRefWithParList#getList()
   * @see #getTemplateRefWithParList()
   * @generated
   */
  EReference getTemplateRefWithParList_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateActualParList <em>Template Actual Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Actual Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateActualParList
   * @generated
   */
  EClass getTemplateActualParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TemplateActualParList#getActual <em>Actual</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Actual</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateActualParList#getActual()
   * @see #getTemplateActualParList()
   * @generated
   */
  EReference getTemplateActualParList_Actual();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.TemplateActualParList#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Assign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateActualParList#getAssign()
   * @see #getTemplateActualParList()
   * @generated
   */
  EReference getTemplateActualParList_Assign();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol <em>Matching Symbol</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Matching Symbol</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol
   * @generated
   */
  EClass getMatchingSymbol();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getComplement <em>Complement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Complement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getComplement()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Complement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getQwlm <em>Qwlm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Qwlm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getQwlm()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Qwlm();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSwlm <em>Swlm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Swlm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSwlm()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Swlm();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Range</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getRange()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Range();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getString <em>String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>String</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getString()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_String();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSubset <em>Subset</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subset</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSubset()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Subset();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSuperset <em>Superset</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Superset</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getSuperset()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Superset();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchingSymbol#getTemplates <em>Templates</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Templates</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchingSymbol#getTemplates()
   * @see #getMatchingSymbol()
   * @generated
   */
  EReference getMatchingSymbol_Templates();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SubsetMatch <em>Subset Match</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subset Match</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SubsetMatch
   * @generated
   */
  EClass getSubsetMatch();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SupersetMatch <em>Superset Match</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Superset Match</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SupersetMatch
   * @generated
   */
  EClass getSupersetMatch();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Range <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Range</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Range
   * @generated
   */
  EClass getRange();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Range#getB1 <em>B1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>B1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Range#getB1()
   * @see #getRange()
   * @generated
   */
  EReference getRange_B1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Range#getB2 <em>B2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>B2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Range#getB2()
   * @see #getRange()
   * @generated
   */
  EReference getRange_B2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.WildcardLengthMatch <em>Wildcard Length Match</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Wildcard Length Match</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WildcardLengthMatch
   * @generated
   */
  EClass getWildcardLengthMatch();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Complement <em>Complement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Complement
   * @generated
   */
  EClass getComplement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ListOfTemplates <em>List Of Templates</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>List Of Templates</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ListOfTemplates
   * @generated
   */
  EClass getListOfTemplates();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ListOfTemplates#getItems <em>Items</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Items</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ListOfTemplates#getItems()
   * @see #getListOfTemplates()
   * @generated
   */
  EReference getListOfTemplates_Items();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateOps <em>Template Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Ops</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOps
   * @generated
   */
  EClass getTemplateOps();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateOps#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateOps#getTemplate()
   * @see #getTemplateOps()
   * @generated
   */
  EReference getTemplateOps_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MatchOp <em>Match Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Match Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchOp
   * @generated
   */
  EClass getMatchOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.MatchOp#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MatchOp#getExpr()
   * @see #getMatchOp()
   * @generated
   */
  EReference getMatchOp_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ValueofOp <em>Valueof Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Valueof Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ValueofOp
   * @generated
   */
  EClass getValueofOp();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps <em>Configuration Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Configuration Ops</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationOps
   * @generated
   */
  EClass getConfigurationOps();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getCreate <em>Create</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Create</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationOps#getCreate()
   * @see #getConfigurationOps()
   * @generated
   */
  EReference getConfigurationOps_Create();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getAlive <em>Alive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Alive</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationOps#getAlive()
   * @see #getConfigurationOps()
   * @generated
   */
  EReference getConfigurationOps_Alive();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getRunning <em>Running</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Running</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConfigurationOps#getRunning()
   * @see #getConfigurationOps()
   * @generated
   */
  EReference getConfigurationOps_Running();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CreateOp <em>Create Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Create Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CreateOp
   * @generated
   */
  EClass getCreateOp();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.CreateOp#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CreateOp#getType()
   * @see #getCreateOp()
   * @generated
   */
  EReference getCreateOp_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CreateOp#getExpr1 <em>Expr1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CreateOp#getExpr1()
   * @see #getCreateOp()
   * @generated
   */
  EReference getCreateOp_Expr1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.CreateOp#getExpr2 <em>Expr2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CreateOp#getExpr2()
   * @see #getCreateOp()
   * @generated
   */
  EReference getCreateOp_Expr2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RunningOp <em>Running Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Running Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunningOp
   * @generated
   */
  EClass getRunningOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RunningOp#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunningOp#getComponent()
   * @see #getRunningOp()
   * @generated
   */
  EReference getRunningOp_Component();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RunningOp#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunningOp#getIndex()
   * @see #getRunningOp()
   * @generated
   */
  EReference getRunningOp_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.OpCall <em>Op Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Op Call</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall
   * @generated
   */
  EClass getOpCall();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getConfiguration <em>Configuration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Configuration</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getConfiguration()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_Configuration();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.OpCall#getVerdict <em>Verdict</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Verdict</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getVerdict()
   * @see #getOpCall()
   * @generated
   */
  EAttribute getOpCall_Verdict();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getTimer()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getFunction()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_Function();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.OpCall#getExtendedFunction <em>Extended Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Extended Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getExtendedFunction()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_ExtendedFunction();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getPreFunction <em>Pre Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pre Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getPreFunction()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_PreFunction();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getTestcase <em>Testcase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Testcase</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getTestcase()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_Testcase();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getActivate <em>Activate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Activate</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getActivate()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_Activate();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.OpCall#getTemplateOps <em>Template Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template Ops</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getTemplateOps()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_TemplateOps();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.OpCall#getExtendedTemplate <em>Extended Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Extended Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getExtendedTemplate()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_ExtendedTemplate();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.OpCall#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Field</em>'.
   * @see de.ugoe.cs.swe.tTCN3.OpCall#getField()
   * @see #getOpCall()
   * @generated
   */
  EReference getOpCall_Field();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AliveOp <em>Alive Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alive Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AliveOp
   * @generated
   */
  EClass getAliveOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AliveOp#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AliveOp#getComponent()
   * @see #getAliveOp()
   * @generated
   */
  EReference getAliveOp_Component();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AliveOp#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AliveOp#getIndex()
   * @see #getAliveOp()
   * @generated
   */
  EReference getAliveOp_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateListItem <em>Template List Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template List Item</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateListItem
   * @generated
   */
  EClass getTemplateListItem();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateListItem#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateListItem#getBody()
   * @see #getTemplateListItem()
   * @generated
   */
  EReference getTemplateListItem_Body();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateListItem#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateListItem#getAll()
   * @see #getTemplateListItem()
   * @generated
   */
  EReference getTemplateListItem_All();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllElementsFrom <em>All Elements From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Elements From</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllElementsFrom
   * @generated
   */
  EClass getAllElementsFrom();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllElementsFrom#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllElementsFrom#getBody()
   * @see #getAllElementsFrom()
   * @generated
   */
  EReference getAllElementsFrom_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Signature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signature</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Signature
   * @generated
   */
  EClass getSignature();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.Signature#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Signature#getRef()
   * @see #getSignature()
   * @generated
   */
  EReference getSignature_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment <em>Template Instance Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Instance Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment
   * @generated
   */
  EClass getTemplateInstanceAssignment();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment#getName()
   * @see #getTemplateInstanceAssignment()
   * @generated
   */
  EReference getTemplateInstanceAssignment_Name();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment#getTemplate()
   * @see #getTemplateInstanceAssignment()
   * @generated
   */
  EReference getTemplateInstanceAssignment_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar <em>Template Instance Actual Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Instance Actual Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar
   * @generated
   */
  EClass getTemplateInstanceActualPar();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction <em>Template Restriction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Restriction</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRestriction
   * @generated
   */
  EClass getTemplateRestriction();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getOmit <em>Omit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Omit</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRestriction#getOmit()
   * @see #getTemplateRestriction()
   * @generated
   */
  EAttribute getTemplateRestriction_Omit();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRestriction#getValue()
   * @see #getTemplateRestriction()
   * @generated
   */
  EAttribute getTemplateRestriction_Value();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TemplateRestriction#getPresent <em>Present</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Present</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TemplateRestriction#getPresent()
   * @see #getTemplateRestriction()
   * @generated
   */
  EAttribute getTemplateRestriction_Present();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RestrictedTemplate <em>Restricted Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Restricted Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RestrictedTemplate
   * @generated
   */
  EClass getRestrictedTemplate();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RestrictedTemplate#getRestriction <em>Restriction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Restriction</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RestrictedTemplate#getRestriction()
   * @see #getRestrictedTemplate()
   * @generated
   */
  EReference getRestrictedTemplate_Restriction();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.LogStatement <em>Log Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Log Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LogStatement
   * @generated
   */
  EClass getLogStatement();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.LogStatement#getItem <em>Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Item</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LogStatement#getItem()
   * @see #getLogStatement()
   * @generated
   */
  EReference getLogStatement_Item();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.LogItem <em>Log Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Log Item</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LogItem
   * @generated
   */
  EClass getLogItem();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.LogItem#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LogItem#getTemplate()
   * @see #getLogItem()
   * @generated
   */
  EReference getLogItem_Template();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.WithStatement <em>With Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>With Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WithStatement
   * @generated
   */
  EClass getWithStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.WithStatement#getAttrib <em>Attrib</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Attrib</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WithStatement#getAttrib()
   * @see #getWithStatement()
   * @generated
   */
  EReference getWithStatement_Attrib();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.WithAttribList <em>With Attrib List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>With Attrib List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WithAttribList
   * @generated
   */
  EClass getWithAttribList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.WithAttribList#getMulti <em>Multi</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Multi</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WithAttribList#getMulti()
   * @see #getWithAttribList()
   * @generated
   */
  EReference getWithAttribList_Multi();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MultiWithAttrib <em>Multi With Attrib</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multi With Attrib</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MultiWithAttrib
   * @generated
   */
  EClass getMultiWithAttrib();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.MultiWithAttrib#getSingle <em>Single</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Single</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MultiWithAttrib#getSingle()
   * @see #getMultiWithAttrib()
   * @generated
   */
  EReference getMultiWithAttrib_Single();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleWithAttrib <em>Single With Attrib</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single With Attrib</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleWithAttrib
   * @generated
   */
  EClass getSingleWithAttrib();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleWithAttrib#getAttrib <em>Attrib</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Attrib</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleWithAttrib#getAttrib()
   * @see #getSingleWithAttrib()
   * @generated
   */
  EReference getSingleWithAttrib_Attrib();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AttribQualifier <em>Attrib Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Attrib Qualifier</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AttribQualifier
   * @generated
   */
  EClass getAttribQualifier();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AttribQualifier#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AttribQualifier#getList()
   * @see #getAttribQualifier()
   * @generated
   */
  EReference getAttribQualifier_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.IdentifierList <em>Identifier List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierList
   * @generated
   */
  EClass getIdentifierList();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.IdentifierList#getIds <em>Ids</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Ids</em>'.
   * @see de.ugoe.cs.swe.tTCN3.IdentifierList#getIds()
   * @see #getIdentifierList()
   * @generated
   */
  EAttribute getIdentifierList_Ids();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList <em>Qualified Identifier List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Qualified Identifier List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList
   * @generated
   */
  EClass getQualifiedIdentifierList();

  /**
   * Returns the meta object for the attribute list '{@link de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList#getQids <em>Qids</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Qids</em>'.
   * @see de.ugoe.cs.swe.tTCN3.QualifiedIdentifierList#getQids()
   * @see #getQualifiedIdentifierList()
   * @generated
   */
  EAttribute getQualifiedIdentifierList_Qids();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleConstDef <em>Single Const Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Const Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConstDef
   * @generated
   */
  EClass getSingleConstDef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleConstDef#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConstDef#getArray()
   * @see #getSingleConstDef()
   * @generated
   */
  EReference getSingleConstDef_Array();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.SingleConstDef#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Assign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConstDef#getAssign()
   * @see #getSingleConstDef()
   * @generated
   */
  EAttribute getSingleConstDef_Assign();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleConstDef#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleConstDef#getExpr()
   * @see #getSingleConstDef()
   * @generated
   */
  EReference getSingleConstDef_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CompoundExpression <em>Compound Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Compound Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CompoundExpression
   * @generated
   */
  EClass getCompoundExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayExpression <em>Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayExpression
   * @generated
   */
  EClass getArrayExpression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ArrayExpression#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayExpression#getList()
   * @see #getArrayExpression()
   * @generated
   */
  EReference getArrayExpression_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionList <em>Field Expression List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Expression List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionList
   * @generated
   */
  EClass getFieldExpressionList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionList#getSpecs <em>Specs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Specs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionList#getSpecs()
   * @see #getFieldExpressionList()
   * @generated
   */
  EReference getFieldExpressionList_Specs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementExpressionList <em>Array Element Expression List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Element Expression List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementExpressionList
   * @generated
   */
  EClass getArrayElementExpressionList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ArrayElementExpressionList#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementExpressionList#getExpr()
   * @see #getArrayElementExpressionList()
   * @generated
   */
  EReference getArrayElementExpressionList_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec <em>Field Expression Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Expression Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionSpec
   * @generated
   */
  EClass getFieldExpressionSpec();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getFieldRef <em>Field Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getFieldRef()
   * @see #getFieldExpressionSpec()
   * @generated
   */
  EReference getFieldExpressionSpec_FieldRef();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getType()
   * @see #getFieldExpressionSpec()
   * @generated
   */
  EAttribute getFieldExpressionSpec_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getArray()
   * @see #getFieldExpressionSpec()
   * @generated
   */
  EReference getFieldExpressionSpec_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getExpr()
   * @see #getFieldExpressionSpec()
   * @generated
   */
  EReference getFieldExpressionSpec_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.NotUsedOrExpression <em>Not Used Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not Used Or Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.NotUsedOrExpression
   * @generated
   */
  EClass getNotUsedOrExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConstantExpression <em>Constant Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConstantExpression
   * @generated
   */
  EClass getConstantExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.CompoundConstExpression <em>Compound Const Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Compound Const Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.CompoundConstExpression
   * @generated
   */
  EClass getCompoundConstExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionList <em>Field Const Expression List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Const Expression List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionList
   * @generated
   */
  EClass getFieldConstExpressionList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionList#getSpecs <em>Specs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Specs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionList#getSpecs()
   * @see #getFieldConstExpressionList()
   * @generated
   */
  EReference getFieldConstExpressionList_Specs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec <em>Field Const Expression Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Const Expression Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec
   * @generated
   */
  EClass getFieldConstExpressionSpec();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getFieldRef <em>Field Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getFieldRef()
   * @see #getFieldConstExpressionSpec()
   * @generated
   */
  EReference getFieldConstExpressionSpec_FieldRef();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getType()
   * @see #getFieldConstExpressionSpec()
   * @generated
   */
  EAttribute getFieldConstExpressionSpec_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getArray()
   * @see #getFieldConstExpressionSpec()
   * @generated
   */
  EReference getFieldConstExpressionSpec_Array();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getConstExpr <em>Const Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getConstExpr()
   * @see #getFieldConstExpressionSpec()
   * @generated
   */
  EReference getFieldConstExpressionSpec_ConstExpr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayConstExpression <em>Array Const Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Const Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayConstExpression
   * @generated
   */
  EClass getArrayConstExpression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ArrayConstExpression#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayConstExpression#getList()
   * @see #getArrayConstExpression()
   * @generated
   */
  EReference getArrayConstExpression_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementConstExpressionList <em>Array Element Const Expression List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Element Const Expression List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementConstExpressionList
   * @generated
   */
  EClass getArrayElementConstExpressionList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ArrayElementConstExpressionList#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementConstExpressionList#getExpr()
   * @see #getArrayElementConstExpressionList()
   * @generated
   */
  EReference getArrayElementConstExpressionList_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConstList <em>Const List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Const List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConstList
   * @generated
   */
  EClass getConstList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ConstList#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConstList#getList()
   * @see #getConstList()
   * @generated
   */
  EReference getConstList_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Value <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Value
   * @generated
   */
  EClass getValue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Value#getPredef <em>Predef</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Predef</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Value#getPredef()
   * @see #getValue()
   * @generated
   */
  EReference getValue_Predef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Value#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Value#getRef()
   * @see #getValue()
   * @generated
   */
  EReference getValue_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReferencedValue <em>Referenced Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Referenced Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReferencedValue
   * @generated
   */
  EClass getReferencedValue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReferencedValue#getHead <em>Head</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Head</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReferencedValue#getHead()
   * @see #getReferencedValue()
   * @generated
   */
  EReference getReferencedValue_Head();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ReferencedValue#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReferencedValue#getFields()
   * @see #getReferencedValue()
   * @generated
   */
  EReference getReferencedValue_Fields();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RefValueHead <em>Ref Value Head</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ref Value Head</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RefValueHead
   * @generated
   */
  EClass getRefValueHead();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RefValueElement <em>Ref Value Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ref Value Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RefValueElement
   * @generated
   */
  EClass getRefValueElement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Head <em>Head</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Head</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Head
   * @generated
   */
  EClass getHead();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.Head#getTarget <em>Target</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Target</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Head#getTarget()
   * @see #getHead()
   * @generated
   */
  EReference getHead_Target();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RefValueTail <em>Ref Value Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ref Value Tail</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RefValueTail
   * @generated
   */
  EClass getRefValueTail();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.RefValueTail#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RefValueTail#getValue()
   * @see #getRefValueTail()
   * @generated
   */
  EReference getRefValueTail_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RefValueTail#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RefValueTail#getArray()
   * @see #getRefValueTail()
   * @generated
   */
  EReference getRefValueTail_Array();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SpecElement <em>Spec Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Spec Element</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SpecElement
   * @generated
   */
  EClass getSpecElement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SpecElement#getTail <em>Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tail</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SpecElement#getTail()
   * @see #getSpecElement()
   * @generated
   */
  EReference getSpecElement_Tail();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference <em>Extended Field Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extended Field Reference</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtendedFieldReference
   * @generated
   */
  EClass getExtendedFieldReference();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtendedFieldReference#getField()
   * @see #getExtendedFieldReference()
   * @generated
   */
  EReference getExtendedFieldReference_Field();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtendedFieldReference#getType()
   * @see #getExtendedFieldReference()
   * @generated
   */
  EAttribute getExtendedFieldReference_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference#getArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ExtendedFieldReference#getArray()
   * @see #getExtendedFieldReference()
   * @generated
   */
  EReference getExtendedFieldReference_Array();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RefValue <em>Ref Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ref Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RefValue
   * @generated
   */
  EClass getRefValue();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue <em>Predefined Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Predefined Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue
   * @generated
   */
  EClass getPredefinedValue();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getBstring <em>Bstring</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bstring</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getBstring()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Bstring();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getBoolean <em>Boolean</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Boolean</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getBoolean()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Boolean();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getInteger <em>Integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Integer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getInteger()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Integer();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getHstring <em>Hstring</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Hstring</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getHstring()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Hstring();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getOstring <em>Ostring</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ostring</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getOstring()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Ostring();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getVerdictType <em>Verdict Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Verdict Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getVerdictType()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_VerdictType();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getFloat <em>Float</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Float</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getFloat()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Float();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getAddress <em>Address</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Address</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getAddress()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Address();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getOmit <em>Omit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Omit</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getOmit()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Omit();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getCharString <em>Char String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Char String</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getCharString()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_CharString();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.PredefinedValue#getMacro <em>Macro</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Macro</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PredefinedValue#getMacro()
   * @see #getPredefinedValue()
   * @generated
   */
  EAttribute getPredefinedValue_Macro();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BooleanExpression <em>Boolean Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BooleanExpression
   * @generated
   */
  EClass getBooleanExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SingleExpression <em>Single Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleExpression
   * @generated
   */
  EClass getSingleExpression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleExpression#getLeft()
   * @see #getSingleExpression()
   * @generated
   */
  EReference getSingleExpression_Left();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SingleExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SingleExpression#getRight()
   * @see #getSingleExpression()
   * @generated
   */
  EReference getSingleExpression_Right();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRefList <em>Def Or Field Ref List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Def Or Field Ref List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRefList
   * @generated
   */
  EClass getDefOrFieldRefList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRefList#getRefs <em>Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRefList#getRefs()
   * @see #getDefOrFieldRefList()
   * @generated
   */
  EReference getDefOrFieldRefList_Refs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef <em>Def Or Field Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Def Or Field Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRef
   * @generated
   */
  EClass getDefOrFieldRef();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Id</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getId()
   * @see #getDefOrFieldRef()
   * @generated
   */
  EReference getDefOrFieldRef_Id();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Field</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getField()
   * @see #getDefOrFieldRef()
   * @generated
   */
  EReference getDefOrFieldRef_Field();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getExtended <em>Extended</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Extended</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getExtended()
   * @see #getDefOrFieldRef()
   * @generated
   */
  EReference getDefOrFieldRef_Extended();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getAll <em>All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>All</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getAll()
   * @see #getDefOrFieldRef()
   * @generated
   */
  EReference getDefOrFieldRef_All();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AllRef <em>All Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllRef
   * @generated
   */
  EClass getAllRef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllRef#getGroupList <em>Group List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Group List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllRef#getGroupList()
   * @see #getAllRef()
   * @generated
   */
  EReference getAllRef_GroupList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.AllRef#getIdList <em>Id List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AllRef#getIdList()
   * @see #getAllRef()
   * @generated
   */
  EReference getAllRef_IdList();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.GroupRefList <em>Group Ref List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Group Ref List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GroupRefList
   * @generated
   */
  EClass getGroupRefList();

  /**
   * Returns the meta object for the reference list '{@link de.ugoe.cs.swe.tTCN3.GroupRefList#getGroups <em>Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Groups</em>'.
   * @see de.ugoe.cs.swe.tTCN3.GroupRefList#getGroups()
   * @see #getGroupRefList()
   * @generated
   */
  EReference getGroupRefList_Groups();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TTCN3Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Reference
   * @generated
   */
  EClass getTTCN3Reference();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.TTCN3Reference#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Reference#getName()
   * @see #getTTCN3Reference()
   * @generated
   */
  EAttribute getTTCN3Reference_Name();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList <em>Reference List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList
   * @generated
   */
  EClass getTTCN3ReferenceList();

  /**
   * Returns the meta object for the reference list '{@link de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList#getRefs <em>Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Refs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList#getRefs()
   * @see #getTTCN3ReferenceList()
   * @generated
   */
  EReference getTTCN3ReferenceList_Refs();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldReference <em>Field Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Reference</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldReference
   * @generated
   */
  EClass getFieldReference();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayOrBitRef <em>Array Or Bit Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Or Bit Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayOrBitRef
   * @generated
   */
  EClass getArrayOrBitRef();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FieldOrBitNumber <em>Field Or Bit Number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Or Bit Number</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FieldOrBitNumber
   * @generated
   */
  EClass getFieldOrBitNumber();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib <em>Array Value Or Attrib</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Value Or Attrib</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib
   * @generated
   */
  EClass getArrayValueOrAttrib();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayValueOrAttrib#getList()
   * @see #getArrayValueOrAttrib()
   * @generated
   */
  EReference getArrayValueOrAttrib_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpecList <em>Array Element Spec List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Element Spec List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpecList
   * @generated
   */
  EClass getArrayElementSpecList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpecList#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpecList#getSpec()
   * @see #getArrayElementSpecList()
   * @generated
   */
  EReference getArrayElementSpecList_Spec();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec <em>Array Element Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Element Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpec
   * @generated
   */
  EClass getArrayElementSpec();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getMatch <em>Match</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Match</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getMatch()
   * @see #getArrayElementSpec()
   * @generated
   */
  EReference getArrayElementSpec_Match();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getBody()
   * @see #getArrayElementSpec()
   * @generated
   */
  EReference getArrayElementSpec_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.TimerOps <em>Timer Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Ops</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerOps
   * @generated
   */
  EClass getTimerOps();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimerOps#getRead <em>Read</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Read</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerOps#getRead()
   * @see #getTimerOps()
   * @generated
   */
  EReference getTimerOps_Read();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.TimerOps#getRun <em>Run</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Run</em>'.
   * @see de.ugoe.cs.swe.tTCN3.TimerOps#getRun()
   * @see #getTimerOps()
   * @generated
   */
  EReference getTimerOps_Run();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp <em>Running Timer Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Running Timer Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunningTimerOp
   * @generated
   */
  EClass getRunningTimerOp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp#getTimerRef <em>Timer Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunningTimerOp#getTimerRef()
   * @see #getRunningTimerOp()
   * @generated
   */
  EReference getRunningTimerOp_TimerRef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunningTimerOp#getIndex()
   * @see #getRunningTimerOp()
   * @generated
   */
  EReference getRunningTimerOp_Index();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReadTimerOp <em>Read Timer Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Read Timer Op</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReadTimerOp
   * @generated
   */
  EClass getReadTimerOp();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.ReadTimerOp#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReadTimerOp#getTimer()
   * @see #getReadTimerOp()
   * @generated
   */
  EReference getReadTimerOp_Timer();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PermutationMatch <em>Permutation Match</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Permutation Match</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PermutationMatch
   * @generated
   */
  EClass getPermutationMatch();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.PermutationMatch#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PermutationMatch#getList()
   * @see #getPermutationMatch()
   * @generated
   */
  EReference getPermutationMatch_List();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct <em>Loop Construct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Loop Construct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LoopConstruct
   * @generated
   */
  EClass getLoopConstruct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getForStm <em>For Stm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>For Stm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LoopConstruct#getForStm()
   * @see #getLoopConstruct()
   * @generated
   */
  EReference getLoopConstruct_ForStm();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getWhileStm <em>While Stm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>While Stm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LoopConstruct#getWhileStm()
   * @see #getLoopConstruct()
   * @generated
   */
  EReference getLoopConstruct_WhileStm();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.LoopConstruct#getDoStm <em>Do Stm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Do Stm</em>'.
   * @see de.ugoe.cs.swe.tTCN3.LoopConstruct#getDoStm()
   * @see #getLoopConstruct()
   * @generated
   */
  EReference getLoopConstruct_DoStm();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ForStatement <em>For Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ForStatement
   * @generated
   */
  EClass getForStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getInit <em>Init</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Init</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ForStatement#getInit()
   * @see #getForStatement()
   * @generated
   */
  EReference getForStatement_Init();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ForStatement#getExpression()
   * @see #getForStatement()
   * @generated
   */
  EReference getForStatement_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assign</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ForStatement#getAssign()
   * @see #getForStatement()
   * @generated
   */
  EReference getForStatement_Assign();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ForStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ForStatement#getStatement()
   * @see #getForStatement()
   * @generated
   */
  EReference getForStatement_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.WhileStatement <em>While Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>While Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WhileStatement
   * @generated
   */
  EClass getWhileStatement();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.WhileStatement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WhileStatement#getExpression()
   * @see #getWhileStatement()
   * @generated
   */
  EReference getWhileStatement_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.WhileStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.WhileStatement#getStatement()
   * @see #getWhileStatement()
   * @generated
   */
  EReference getWhileStatement_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.DoWhileStatement <em>Do While Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Do While Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoWhileStatement
   * @generated
   */
  EClass getDoWhileStatement();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.DoWhileStatement#getKeyDo <em>Key Do</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Key Do</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoWhileStatement#getKeyDo()
   * @see #getDoWhileStatement()
   * @generated
   */
  EAttribute getDoWhileStatement_KeyDo();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DoWhileStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoWhileStatement#getStatement()
   * @see #getDoWhileStatement()
   * @generated
   */
  EReference getDoWhileStatement_Statement();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.DoWhileStatement#getKeyWhile <em>Key While</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Key While</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoWhileStatement#getKeyWhile()
   * @see #getDoWhileStatement()
   * @generated
   */
  EAttribute getDoWhileStatement_KeyWhile();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.DoWhileStatement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.DoWhileStatement#getExpression()
   * @see #getDoWhileStatement()
   * @generated
   */
  EReference getDoWhileStatement_Expression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct <em>Conditional Construct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conditional Construct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConditionalConstruct
   * @generated
   */
  EClass getConditionalConstruct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getExpression()
   * @see #getConditionalConstruct()
   * @generated
   */
  EReference getConditionalConstruct_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getStatement()
   * @see #getConditionalConstruct()
   * @generated
   */
  EReference getConditionalConstruct_Statement();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElseifs <em>Elseifs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elseifs</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElseifs()
   * @see #getConditionalConstruct()
   * @generated
   */
  EReference getConditionalConstruct_Elseifs();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ConditionalConstruct#getElse()
   * @see #getConditionalConstruct()
   * @generated
   */
  EReference getConditionalConstruct_Else();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause <em>Else If Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Else If Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseIfClause
   * @generated
   */
  EClass getElseIfClause();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyElse <em>Key Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Key Else</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyElse()
   * @see #getElseIfClause()
   * @generated
   */
  EAttribute getElseIfClause_KeyElse();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyIf <em>Key If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Key If</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyIf()
   * @see #getElseIfClause()
   * @generated
   */
  EAttribute getElseIfClause_KeyIf();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseIfClause#getExpression()
   * @see #getElseIfClause()
   * @generated
   */
  EReference getElseIfClause_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseIfClause#getStatement()
   * @see #getElseIfClause()
   * @generated
   */
  EReference getElseIfClause_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ElseClause <em>Else Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Else Clause</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseClause
   * @generated
   */
  EClass getElseClause();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ElseClause#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ElseClause#getStatement()
   * @see #getElseClause()
   * @generated
   */
  EReference getElseClause_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Initial <em>Initial</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Initial</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Initial
   * @generated
   */
  EClass getInitial();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Initial#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Initial#getVariable()
   * @see #getInitial()
   * @generated
   */
  EReference getInitial_Variable();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Initial#getAssignment <em>Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Initial#getAssignment()
   * @see #getInitial()
   * @generated
   */
  EReference getInitial_Assignment();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct <em>Select Case Construct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Case Construct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseConstruct
   * @generated
   */
  EClass getSelectCaseConstruct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getExpression()
   * @see #getSelectCaseConstruct()
   * @generated
   */
  EReference getSelectCaseConstruct_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getBody()
   * @see #getSelectCaseConstruct()
   * @generated
   */
  EReference getSelectCaseConstruct_Body();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SelectCaseBody <em>Select Case Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Case Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseBody
   * @generated
   */
  EClass getSelectCaseBody();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SelectCaseBody#getCases <em>Cases</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Cases</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCaseBody#getCases()
   * @see #getSelectCaseBody()
   * @generated
   */
  EReference getSelectCaseBody_Cases();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.SelectCase <em>Select Case</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Case</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCase
   * @generated
   */
  EClass getSelectCase();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.SelectCase#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCase#getTemplate()
   * @see #getSelectCase()
   * @generated
   */
  EReference getSelectCase_Template();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.SelectCase#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Else</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCase#getElse()
   * @see #getSelectCase()
   * @generated
   */
  EAttribute getSelectCase_Else();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.SelectCase#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.SelectCase#getStatement()
   * @see #getSelectCase()
   * @generated
   */
  EReference getSelectCase_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionDef <em>Function Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Def</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef
   * @generated
   */
  EClass getFunctionDef();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getDet <em>Det</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Det</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getDet()
   * @see #getFunctionDef()
   * @generated
   */
  EAttribute getFunctionDef_Det();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getParameterList <em>Parameter List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parameter List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getParameterList()
   * @see #getFunctionDef()
   * @generated
   */
  EReference getFunctionDef_ParameterList();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getRunsOn <em>Runs On</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Runs On</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getRunsOn()
   * @see #getFunctionDef()
   * @generated
   */
  EReference getFunctionDef_RunsOn();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getMtc <em>Mtc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Mtc</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getMtc()
   * @see #getFunctionDef()
   * @generated
   */
  EReference getFunctionDef_Mtc();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getSystem <em>System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>System</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getSystem()
   * @see #getFunctionDef()
   * @generated
   */
  EReference getFunctionDef_System();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getReturnType <em>Return Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getReturnType()
   * @see #getFunctionDef()
   * @generated
   */
  EReference getFunctionDef_ReturnType();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionDef#getStatement()
   * @see #getFunctionDef()
   * @generated
   */
  EReference getFunctionDef_Statement();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MtcSpec <em>Mtc Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mtc Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MtcSpec
   * @generated
   */
  EClass getMtcSpec();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.MtcSpec#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MtcSpec#getComponent()
   * @see #getMtcSpec()
   * @generated
   */
  EReference getMtcSpec_Component();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalParList <em>Function Formal Par List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Formal Par List</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalParList
   * @generated
   */
  EClass getFunctionFormalParList();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalParList#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalParList#getParams()
   * @see #getFunctionFormalParList()
   * @generated
   */
  EReference getFunctionFormalParList_Params();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar <em>Function Formal Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Formal Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalPar
   * @generated
   */
  EClass getFunctionFormalPar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getValue()
   * @see #getFunctionFormalPar()
   * @generated
   */
  EReference getFunctionFormalPar_Value();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTimer()
   * @see #getFunctionFormalPar()
   * @generated
   */
  EReference getFunctionFormalPar_Timer();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTemplate <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Template</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTemplate()
   * @see #getFunctionFormalPar()
   * @generated
   */
  EReference getFunctionFormalPar_Template();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getPort()
   * @see #getFunctionFormalPar()
   * @generated
   */
  EReference getFunctionFormalPar_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar <em>Formal Value Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formal Value Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalValuePar
   * @generated
   */
  EClass getFormalValuePar();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getInOut <em>In Out</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>In Out</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalValuePar#getInOut()
   * @see #getFormalValuePar()
   * @generated
   */
  EAttribute getFormalValuePar_InOut();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getMod <em>Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Mod</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalValuePar#getMod()
   * @see #getFormalValuePar()
   * @generated
   */
  EAttribute getFormalValuePar_Mod();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalValuePar#getType()
   * @see #getFormalValuePar()
   * @generated
   */
  EReference getFormalValuePar_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalValuePar#getExpression()
   * @see #getFormalValuePar()
   * @generated
   */
  EReference getFormalValuePar_Expression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FormalTimerPar <em>Formal Timer Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formal Timer Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTimerPar
   * @generated
   */
  EClass getFormalTimerPar();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FormalPortPar <em>Formal Port Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formal Port Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalPortPar
   * @generated
   */
  EClass getFormalPortPar();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.FormalPortPar#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Port</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalPortPar#getPort()
   * @see #getFormalPortPar()
   * @generated
   */
  EReference getFormalPortPar_Port();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar <em>Formal Template Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formal Template Par</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar
   * @generated
   */
  EClass getFormalTemplatePar();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getInOut <em>In Out</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>In Out</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getInOut()
   * @see #getFormalTemplatePar()
   * @generated
   */
  EAttribute getFormalTemplatePar_InOut();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getRestriction <em>Restriction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Restriction</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getRestriction()
   * @see #getFormalTemplatePar()
   * @generated
   */
  EReference getFormalTemplatePar_Restriction();

  /**
   * Returns the meta object for the attribute '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getMod <em>Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Mod</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getMod()
   * @see #getFormalTemplatePar()
   * @generated
   */
  EAttribute getFormalTemplatePar_Mod();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getType()
   * @see #getFormalTemplatePar()
   * @generated
   */
  EReference getFormalTemplatePar_Type();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getTempl <em>Templ</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Templ</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getTempl()
   * @see #getFormalTemplatePar()
   * @generated
   */
  EReference getFormalTemplatePar_Templ();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RunsOnSpec <em>Runs On Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Runs On Spec</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunsOnSpec
   * @generated
   */
  EClass getRunsOnSpec();

  /**
   * Returns the meta object for the reference '{@link de.ugoe.cs.swe.tTCN3.RunsOnSpec#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Component</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RunsOnSpec#getComponent()
   * @see #getRunsOnSpec()
   * @generated
   */
  EReference getRunsOnSpec_Component();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ReturnType <em>Return Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Return Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReturnType
   * @generated
   */
  EClass getReturnType();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.ReturnType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ReturnType#getType()
   * @see #getReturnType()
   * @generated
   */
  EReference getReturnType_Type();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Assignment <em>Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assignment</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Assignment
   * @generated
   */
  EClass getAssignment();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Assignment#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Assignment#getRef()
   * @see #getAssignment()
   * @generated
   */
  EReference getAssignment_Ref();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Assignment#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Assignment#getExpression()
   * @see #getAssignment()
   * @generated
   */
  EReference getAssignment_Expression();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Assignment#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Assignment#getBody()
   * @see #getAssignment()
   * @generated
   */
  EReference getAssignment_Body();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Assignment#getExtra <em>Extra</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Extra</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Assignment#getExtra()
   * @see #getAssignment()
   * @generated
   */
  EReference getAssignment_Extra();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.VariableRef <em>Variable Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableRef
   * @generated
   */
  EClass getVariableRef();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.VariableRef#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VariableRef#getRef()
   * @see #getVariableRef()
   * @generated
   */
  EReference getVariableRef_Ref();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.PreDefFunction <em>Pre Def Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pre Def Function</em>'.
   * @see de.ugoe.cs.swe.tTCN3.PreDefFunction
   * @generated
   */
  EClass getPreDefFunction();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2char <em>Fint2char</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2char</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2char
   * @generated
   */
  EClass getFint2char();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2char#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2char#getE1()
   * @see #getFint2char()
   * @generated
   */
  EReference getFint2char_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2unichar <em>Fint2unichar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2unichar</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2unichar
   * @generated
   */
  EClass getFint2unichar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2unichar#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2unichar#getE1()
   * @see #getFint2unichar()
   * @generated
   */
  EReference getFint2unichar_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2bit <em>Fint2bit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2bit</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2bit
   * @generated
   */
  EClass getFint2bit();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2bit#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2bit#getE1()
   * @see #getFint2bit()
   * @generated
   */
  EReference getFint2bit_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2bit#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2bit#getE2()
   * @see #getFint2bit()
   * @generated
   */
  EReference getFint2bit_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2enum <em>Fint2enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2enum</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2enum
   * @generated
   */
  EClass getFint2enum();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2enum#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2enum#getE1()
   * @see #getFint2enum()
   * @generated
   */
  EReference getFint2enum_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2enum#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2enum#getE2()
   * @see #getFint2enum()
   * @generated
   */
  EReference getFint2enum_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2hex <em>Fint2hex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2hex</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2hex
   * @generated
   */
  EClass getFint2hex();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2hex#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2hex#getE1()
   * @see #getFint2hex()
   * @generated
   */
  EReference getFint2hex_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2hex#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2hex#getE2()
   * @see #getFint2hex()
   * @generated
   */
  EReference getFint2hex_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2oct <em>Fint2oct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2oct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2oct
   * @generated
   */
  EClass getFint2oct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2oct#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2oct#getE1()
   * @see #getFint2oct()
   * @generated
   */
  EReference getFint2oct_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2oct#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2oct#getE2()
   * @see #getFint2oct()
   * @generated
   */
  EReference getFint2oct_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2str <em>Fint2str</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2str</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2str
   * @generated
   */
  EClass getFint2str();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2str#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2str#getE1()
   * @see #getFint2str()
   * @generated
   */
  EReference getFint2str_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fint2float <em>Fint2float</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fint2float</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2float
   * @generated
   */
  EClass getFint2float();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fint2float#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fint2float#getE1()
   * @see #getFint2float()
   * @generated
   */
  EReference getFint2float_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Ffloat2int <em>Ffloat2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ffloat2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Ffloat2int
   * @generated
   */
  EClass getFfloat2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Ffloat2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Ffloat2int#getE1()
   * @see #getFfloat2int()
   * @generated
   */
  EReference getFfloat2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fchar2int <em>Fchar2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fchar2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fchar2int
   * @generated
   */
  EClass getFchar2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fchar2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fchar2int#getE1()
   * @see #getFchar2int()
   * @generated
   */
  EReference getFchar2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fchar2oct <em>Fchar2oct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fchar2oct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fchar2oct
   * @generated
   */
  EClass getFchar2oct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fchar2oct#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fchar2oct#getE1()
   * @see #getFchar2oct()
   * @generated
   */
  EReference getFchar2oct_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Funichar2int <em>Funichar2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Funichar2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Funichar2int
   * @generated
   */
  EClass getFunichar2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Funichar2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Funichar2int#getE1()
   * @see #getFunichar2int()
   * @generated
   */
  EReference getFunichar2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Funichar2oct <em>Funichar2oct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Funichar2oct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Funichar2oct
   * @generated
   */
  EClass getFunichar2oct();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.Funichar2oct#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Funichar2oct#getExpr()
   * @see #getFunichar2oct()
   * @generated
   */
  EReference getFunichar2oct_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fbit2int <em>Fbit2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fbit2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2int
   * @generated
   */
  EClass getFbit2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fbit2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2int#getE1()
   * @see #getFbit2int()
   * @generated
   */
  EReference getFbit2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fbit2hex <em>Fbit2hex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fbit2hex</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2hex
   * @generated
   */
  EClass getFbit2hex();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fbit2hex#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2hex#getE1()
   * @see #getFbit2hex()
   * @generated
   */
  EReference getFbit2hex_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fbit2oct <em>Fbit2oct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fbit2oct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2oct
   * @generated
   */
  EClass getFbit2oct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fbit2oct#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2oct#getE1()
   * @see #getFbit2oct()
   * @generated
   */
  EReference getFbit2oct_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fbit2str <em>Fbit2str</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fbit2str</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2str
   * @generated
   */
  EClass getFbit2str();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fbit2str#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fbit2str#getE1()
   * @see #getFbit2str()
   * @generated
   */
  EReference getFbit2str_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fhex2int <em>Fhex2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fhex2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2int
   * @generated
   */
  EClass getFhex2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fhex2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2int#getE1()
   * @see #getFhex2int()
   * @generated
   */
  EReference getFhex2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fhex2bit <em>Fhex2bit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fhex2bit</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2bit
   * @generated
   */
  EClass getFhex2bit();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fhex2bit#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2bit#getE1()
   * @see #getFhex2bit()
   * @generated
   */
  EReference getFhex2bit_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fhex2oct <em>Fhex2oct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fhex2oct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2oct
   * @generated
   */
  EClass getFhex2oct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fhex2oct#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2oct#getE1()
   * @see #getFhex2oct()
   * @generated
   */
  EReference getFhex2oct_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fhex2str <em>Fhex2str</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fhex2str</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2str
   * @generated
   */
  EClass getFhex2str();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fhex2str#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fhex2str#getE1()
   * @see #getFhex2str()
   * @generated
   */
  EReference getFhex2str_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Foct2int <em>Foct2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foct2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2int
   * @generated
   */
  EClass getFoct2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Foct2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2int#getE1()
   * @see #getFoct2int()
   * @generated
   */
  EReference getFoct2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Foct2bit <em>Foct2bit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foct2bit</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2bit
   * @generated
   */
  EClass getFoct2bit();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Foct2bit#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2bit#getE1()
   * @see #getFoct2bit()
   * @generated
   */
  EReference getFoct2bit_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Foct2hex <em>Foct2hex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foct2hex</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2hex
   * @generated
   */
  EClass getFoct2hex();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Foct2hex#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2hex#getE1()
   * @see #getFoct2hex()
   * @generated
   */
  EReference getFoct2hex_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Foct2str <em>Foct2str</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foct2str</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2str
   * @generated
   */
  EClass getFoct2str();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Foct2str#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2str#getE1()
   * @see #getFoct2str()
   * @generated
   */
  EReference getFoct2str_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Foct2char <em>Foct2char</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foct2char</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2char
   * @generated
   */
  EClass getFoct2char();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Foct2char#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2char#getE1()
   * @see #getFoct2char()
   * @generated
   */
  EReference getFoct2char_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Foct2unichar <em>Foct2unichar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foct2unichar</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2unichar
   * @generated
   */
  EClass getFoct2unichar();

  /**
   * Returns the meta object for the containment reference list '{@link de.ugoe.cs.swe.tTCN3.Foct2unichar#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Foct2unichar#getExpr()
   * @see #getFoct2unichar()
   * @generated
   */
  EReference getFoct2unichar_Expr();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fstr2int <em>Fstr2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fstr2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2int
   * @generated
   */
  EClass getFstr2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fstr2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2int#getE1()
   * @see #getFstr2int()
   * @generated
   */
  EReference getFstr2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fstr2hex <em>Fstr2hex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fstr2hex</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2hex
   * @generated
   */
  EClass getFstr2hex();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fstr2hex#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2hex#getE1()
   * @see #getFstr2hex()
   * @generated
   */
  EReference getFstr2hex_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fstr2oct <em>Fstr2oct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fstr2oct</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2oct
   * @generated
   */
  EClass getFstr2oct();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fstr2oct#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2oct#getE1()
   * @see #getFstr2oct()
   * @generated
   */
  EReference getFstr2oct_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fstr2float <em>Fstr2float</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fstr2float</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2float
   * @generated
   */
  EClass getFstr2float();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fstr2float#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fstr2float#getE1()
   * @see #getFstr2float()
   * @generated
   */
  EReference getFstr2float_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fenum2int <em>Fenum2int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fenum2int</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fenum2int
   * @generated
   */
  EClass getFenum2int();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fenum2int#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fenum2int#getE1()
   * @see #getFenum2int()
   * @generated
   */
  EReference getFenum2int_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Flengthof <em>Flengthof</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Flengthof</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Flengthof
   * @generated
   */
  EClass getFlengthof();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Flengthof#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Flengthof#getT1()
   * @see #getFlengthof()
   * @generated
   */
  EReference getFlengthof_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fsizeof <em>Fsizeof</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fsizeof</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fsizeof
   * @generated
   */
  EClass getFsizeof();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fsizeof#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fsizeof#getT1()
   * @see #getFsizeof()
   * @generated
   */
  EReference getFsizeof_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fispresent <em>Fispresent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fispresent</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fispresent
   * @generated
   */
  EClass getFispresent();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fispresent#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fispresent#getT1()
   * @see #getFispresent()
   * @generated
   */
  EReference getFispresent_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fischosen <em>Fischosen</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fischosen</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fischosen
   * @generated
   */
  EClass getFischosen();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fischosen#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fischosen#getT1()
   * @see #getFischosen()
   * @generated
   */
  EReference getFischosen_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fisvalue <em>Fisvalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fisvalue</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fisvalue
   * @generated
   */
  EClass getFisvalue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fisvalue#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fisvalue#getT1()
   * @see #getFisvalue()
   * @generated
   */
  EReference getFisvalue_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fisbound <em>Fisbound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fisbound</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fisbound
   * @generated
   */
  EClass getFisbound();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fisbound#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fisbound#getT1()
   * @see #getFisbound()
   * @generated
   */
  EReference getFisbound_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fregexp <em>Fregexp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fregexp</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fregexp
   * @generated
   */
  EClass getFregexp();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fregexp#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fregexp#getT1()
   * @see #getFregexp()
   * @generated
   */
  EReference getFregexp_T1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fregexp#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fregexp#getE1()
   * @see #getFregexp()
   * @generated
   */
  EReference getFregexp_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fregexp#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fregexp#getE2()
   * @see #getFregexp()
   * @generated
   */
  EReference getFregexp_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fsubstr <em>Fsubstr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fsubstr</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fsubstr
   * @generated
   */
  EClass getFsubstr();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fsubstr#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fsubstr#getT1()
   * @see #getFsubstr()
   * @generated
   */
  EReference getFsubstr_T1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fsubstr#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fsubstr#getE1()
   * @see #getFsubstr()
   * @generated
   */
  EReference getFsubstr_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fsubstr#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fsubstr#getE2()
   * @see #getFsubstr()
   * @generated
   */
  EReference getFsubstr_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Freplace <em>Freplace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Freplace</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Freplace
   * @generated
   */
  EClass getFreplace();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Freplace#getE1()
   * @see #getFreplace()
   * @generated
   */
  EReference getFreplace_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Freplace#getE2()
   * @see #getFreplace()
   * @generated
   */
  EReference getFreplace_E2();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE3 <em>E3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E3</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Freplace#getE3()
   * @see #getFreplace()
   * @generated
   */
  EReference getFreplace_E3();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Freplace#getE4 <em>E4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E4</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Freplace#getE4()
   * @see #getFreplace()
   * @generated
   */
  EReference getFreplace_E4();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fencvalue <em>Fencvalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fencvalue</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fencvalue
   * @generated
   */
  EClass getFencvalue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fencvalue#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fencvalue#getT1()
   * @see #getFencvalue()
   * @generated
   */
  EReference getFencvalue_T1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Fdecvalue <em>Fdecvalue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fdecvalue</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fdecvalue
   * @generated
   */
  EClass getFdecvalue();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fdecvalue#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fdecvalue#getE1()
   * @see #getFdecvalue()
   * @generated
   */
  EReference getFdecvalue_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Fdecvalue#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Fdecvalue#getE2()
   * @see #getFdecvalue()
   * @generated
   */
  EReference getFdecvalue_E2();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FencvalueUnichar <em>Fencvalue Unichar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fencvalue Unichar</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FencvalueUnichar
   * @generated
   */
  EClass getFencvalueUnichar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FencvalueUnichar#getT1 <em>T1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>T1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FencvalueUnichar#getT1()
   * @see #getFencvalueUnichar()
   * @generated
   */
  EReference getFencvalueUnichar_T1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FencvalueUnichar#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FencvalueUnichar#getE1()
   * @see #getFencvalueUnichar()
   * @generated
   */
  EReference getFencvalueUnichar_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar <em>Fdecvalue Unichar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fdecvalue Unichar</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FdecvalueUnichar
   * @generated
   */
  EClass getFdecvalueUnichar();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE1()
   * @see #getFdecvalueUnichar()
   * @generated
   */
  EReference getFdecvalueUnichar_E1();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE2 <em>E2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E2</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE2()
   * @see #getFdecvalueUnichar()
   * @generated
   */
  EReference getFdecvalueUnichar_E2();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE3 <em>E3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E3</em>'.
   * @see de.ugoe.cs.swe.tTCN3.FdecvalueUnichar#getE3()
   * @see #getFdecvalueUnichar()
   * @generated
   */
  EReference getFdecvalueUnichar_E3();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Frnd <em>Frnd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Frnd</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Frnd
   * @generated
   */
  EClass getFrnd();

  /**
   * Returns the meta object for the containment reference '{@link de.ugoe.cs.swe.tTCN3.Frnd#getE1 <em>E1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E1</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Frnd#getE1()
   * @see #getFrnd()
   * @generated
   */
  EReference getFrnd_E1();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.Ftestcasename <em>Ftestcasename</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ftestcasename</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Ftestcasename
   * @generated
   */
  EClass getFtestcasename();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.XorExpression <em>Xor Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Xor Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.XorExpression
   * @generated
   */
  EClass getXorExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AndExpression
   * @generated
   */
  EClass getAndExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.EqualExpression <em>Equal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equal Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.EqualExpression
   * @generated
   */
  EClass getEqualExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.RelExpression <em>Rel Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Rel Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.RelExpression
   * @generated
   */
  EClass getRelExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.ShiftExpression <em>Shift Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Shift Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.ShiftExpression
   * @generated
   */
  EClass getShiftExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BitOrExpression <em>Bit Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bit Or Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BitOrExpression
   * @generated
   */
  EClass getBitOrExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BitXorExpression <em>Bit Xor Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bit Xor Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BitXorExpression
   * @generated
   */
  EClass getBitXorExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.BitAndExpression <em>Bit And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bit And Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.BitAndExpression
   * @generated
   */
  EClass getBitAndExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.AddExpression <em>Add Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.AddExpression
   * @generated
   */
  EClass getAddExpression();

  /**
   * Returns the meta object for class '{@link de.ugoe.cs.swe.tTCN3.MulExpression <em>Mul Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mul Expression</em>'.
   * @see de.ugoe.cs.swe.tTCN3.MulExpression
   * @generated
   */
  EClass getMulExpression();

  /**
   * Returns the meta object for enum '{@link de.ugoe.cs.swe.tTCN3.Visibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Visibility</em>'.
   * @see de.ugoe.cs.swe.tTCN3.Visibility
   * @generated
   */
  EEnum getVisibility();

  /**
   * Returns the meta object for enum '{@link de.ugoe.cs.swe.tTCN3.VerdictTypeValue <em>Verdict Type Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Verdict Type Value</em>'.
   * @see de.ugoe.cs.swe.tTCN3.VerdictTypeValue
   * @generated
   */
  EEnum getVerdictTypeValue();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  TTCN3Factory getTTCN3Factory();

} //TTCN3Package
