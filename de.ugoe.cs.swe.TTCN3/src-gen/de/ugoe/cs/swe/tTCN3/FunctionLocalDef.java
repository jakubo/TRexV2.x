/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Local Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getConstDef <em>Const Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getTemplateDef <em>Template Def</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionLocalDef()
 * @model
 * @generated
 */
public interface FunctionLocalDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Const Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const Def</em>' containment reference.
   * @see #setConstDef(ConstDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionLocalDef_ConstDef()
   * @model containment="true"
   * @generated
   */
  ConstDef getConstDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getConstDef <em>Const Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const Def</em>' containment reference.
   * @see #getConstDef()
   * @generated
   */
  void setConstDef(ConstDef value);

  /**
   * Returns the value of the '<em><b>Template Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template Def</em>' containment reference.
   * @see #setTemplateDef(TemplateDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionLocalDef_TemplateDef()
   * @model containment="true"
   * @generated
   */
  TemplateDef getTemplateDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionLocalDef#getTemplateDef <em>Template Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template Def</em>' containment reference.
   * @see #getTemplateDef()
   * @generated
   */
  void setTemplateDef(TemplateDef value);

} // FunctionLocalDef
