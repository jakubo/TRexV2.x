/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alt Guard List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltGuardList#getGuardList <em>Guard List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltGuardList#getElseList <em>Else List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltGuardList()
 * @model
 * @generated
 */
public interface AltGuardList extends EObject
{
  /**
   * Returns the value of the '<em><b>Guard List</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.GuardStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard List</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard List</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltGuardList_GuardList()
   * @model containment="true"
   * @generated
   */
  EList<GuardStatement> getGuardList();

  /**
   * Returns the value of the '<em><b>Else List</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ElseStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else List</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else List</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltGuardList_ElseList()
   * @model containment="true"
   * @generated
   */
  EList<ElseStatement> getElseList();

} // AltGuardList
