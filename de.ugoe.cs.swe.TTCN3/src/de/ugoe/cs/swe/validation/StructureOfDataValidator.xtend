package de.ugoe.cs.swe.validation

import com.google.common.base.Strings

import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile
import de.ugoe.cs.swe.common.ConfigTools
import de.ugoe.cs.swe.common.MiscTools
import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass
import de.ugoe.cs.swe.tTCN3.AllOrSignatureList
import de.ugoe.cs.swe.tTCN3.AllOrTypeList
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.EnumDefNamed
import de.ugoe.cs.swe.tTCN3.GroupDef
import de.ugoe.cs.swe.tTCN3.PortDef
import de.ugoe.cs.swe.tTCN3.ProcOrTypeList
import de.ugoe.cs.swe.tTCN3.RecordDefNamed
import de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
import de.ugoe.cs.swe.tTCN3.ReferencedType
import de.ugoe.cs.swe.tTCN3.SetDefNamed
import de.ugoe.cs.swe.tTCN3.SetOfDefNamed
import de.ugoe.cs.swe.tTCN3.Signature
import de.ugoe.cs.swe.tTCN3.SignatureDef
import de.ugoe.cs.swe.tTCN3.StructuredTypeDef
import de.ugoe.cs.swe.tTCN3.SubTypeDef
import de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TTCN3Package
import de.ugoe.cs.swe.tTCN3.Type
import de.ugoe.cs.swe.tTCN3.TypeDef
import de.ugoe.cs.swe.tTCN3.UnionDefNamed

import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.xtext.EcoreUtil2.*
import org.eclipse.emf.ecore.EObject

class StructureOfDataValidator extends AbstractDeclarativeValidator {
	val ConfigTools configTools = ConfigTools.getInstance;
	var QualityCheckProfile activeProfile = configTools.selectedProfile as QualityCheckProfile

	@Check
	def checkTypeDefOrderInGroup(GroupDef group) {
		var StructuredTypeDef struct = null;
		var SubTypeDef sub = null;
		var previous = ""
		var current = ""

		if (!activeProfile.checkTypeDefOrderInGroup || group.list == null)
			return;

		for (d : group.list.definitions) {
			if (d.def instanceof TypeDef) {
				struct = (d.def as TypeDef).body.structured
				sub = (d.def as TypeDef).body.sub
				if (struct != null) {
					if (struct.record != null && struct.record instanceof RecordDefNamed) {
						current = (struct.record as RecordDefNamed).name
					} else if (struct.union != null && struct.union instanceof UnionDefNamed) {
						current = (struct.union as UnionDefNamed).name
					} else if (struct.set != null && struct.set instanceof SetDefNamed) {
						current = (struct.set as SetDefNamed).name
					} else if (struct.recordOf != null && struct.recordOf instanceof RecordOfDefNamed) {
						current = (struct.recordOf as RecordOfDefNamed).name
					} else if (struct.setOf != null && struct.setOf instanceof SetOfDefNamed) {
						current = (struct.setOf as SetOfDefNamed).name
					} else if (struct.enumDef != null && struct.enumDef instanceof EnumDefNamed) {
						current = (struct.enumDef as EnumDefNamed).name
					} else if (struct.port != null) {
						current = (struct.port as PortDef).name
					} else if (struct.component != null) {
						current = (struct.component as ComponentDef).name
					}
				} else if (sub != null) {
					if (sub instanceof SubTypeDefNamed) {
						current = sub.name
					}
				}
			}

			if (!Strings.isNullOrEmpty(current)) {
				compareAlphabeticOrder(group, previous, current)
				previous = current
				current = ""
			}
		}
	}

	// TODO: more information are available (concrete types). use them?
	private def compareAlphabeticOrder(GroupDef group, String previous, String current) {
		if (Strings.isNullOrEmpty(current))
			return;

		val INode node = NodeModelUtils.getNode(group)
		if (current.compareToIgnoreCase(previous) <= 0) {
			TTCN3StatisticsProvider.getInstance.incrementCountStructure
			val message = "Type definitions <\"" + previous + "\",\"" + current + "\"> within group \"" + group.name +
				"\" are not alphabetically ordered!"

			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.STRUCTURE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"4.1, " + "checkTypeDefOrderInGroup"
			);
		}
	}

	@Check
	def checkPortMessageGrouping(PortDef port) {
		if (!activeProfile.checkPortMessageGrouping)
			return;

		val INode node = NodeModelUtils.getNode(port)
		val parentGroup = port.findDesiredParent(GroupDef)
		var message = "";

		if (parentGroup == null) {
			TTCN3StatisticsProvider.getInstance.incrementCountStructure
			message = "Port type definition for \"" + port.name +
				"\" is found outside a group definition! Related messages can therefore never be in the same group as the port definition!"
			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.STRUCTURE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"4.2, " + MiscTools.getMethodName()
			);
		} else {
			val types = port.body.attribs.getAllContentsOfType(Type)
			val signatures = port.body.attribs.getAllContentsOfType(Signature)
			for (t : types) {
				val ref = t.ref.referencedType
				if (ref != null) {
					switch ref {
						SignatureDef: portReferencedTypeCHeck(port, ref, "Message type", t)
						RecordDefNamed: portReferencedTypeCHeck(port, ref, "Message type", t)
						SetDefNamed: portReferencedTypeCHeck(port, ref, "Message type", t)
						SubTypeDefNamed: portReferencedTypeCHeck(port, ref, "Message type", t)
					}
				}
			}
			for (s : signatures) {
				portReferencedTypeCHeck(port, s.ref, "Signature", s)
			}
		}
	}

	private def portReferencedTypeCHeck(PortDef port, ReferencedType type, String typeName, EObject reference) {
		val INode nodePort = NodeModelUtils.getNode(port)
		val INode nodeType = NodeModelUtils.getNode(type)
		val INode nodeRef = NodeModelUtils.getNode(reference)
		val TTCN3Module parentModulePort = port.findDesiredParent(TTCN3Module)
		val TTCN3Module parentModuleType = type.findDesiredParent(TTCN3Module)
		val GroupDef parentGroupPort = port.findDesiredParent(GroupDef)
		val GroupDef parentGroupType = type.findDesiredParent(GroupDef)

		if (parentGroupType == null) {
			TTCN3StatisticsProvider.getInstance.incrementCountStructure
			val message = "" + typeName + " definition for \"" + type.name + "\" <" + nodeType.startLine + "," +
				parentModuleType.name + ",- " + "> " + "related to port type definition for \"" + port.name + "\" <" +
				nodePort.startLine + "," + parentModulePort.name + "," + parentGroupPort.name + "> " +
				"is found outside a group definition! It can therefore never be in the same group as the port type definition!"

			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.STRUCTURE.toString,
				nodeRef.startLine.toString,
				nodeRef.endLine.toString,
				"4.2, " + "checkPortMessageGrouping"
			);
			return;
		}

		if (parentModulePort != parentModuleType) {
			TTCN3StatisticsProvider.getInstance.incrementCountStructure
			val message = "" + typeName + " definition for \"" + type.name + "\" <" + nodeType.startLine + "," +
				parentModuleType.name + "," + parentGroupType.name + "> " + "related to port type definition for \"" +
				port.name + "\" <" + nodePort.startLine + "," + parentModulePort.name + "," + parentGroupPort.name +
				"> " +
				"is not within the same module as the port type definition! It can therefore never be in the same group as the port definition!"
			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.STRUCTURE.toString,
				nodeRef.startLine.toString,
				nodeRef.endLine.toString,
				"4.2, " + "checkPortMessageGrouping"
			);
			return;
		}

		if (parentGroupPort != parentGroupType) {

			// check nested subgroups
			if (parentGroupPort.getAllContentsOfType(GroupDef).contains(parentGroupType)) {
				TTCN3StatisticsProvider.getInstance.incrementCountStructure
				val message = "" + typeName + " definition for \"" + type.name + "\" <" + nodeType.startLine + "," +
					parentModuleType.name + "," + parentGroupType.name + "> " + "related to port type definition for \"" +
					port.name + "\" <" + nodePort.startLine + "," + parentModulePort.name + "," + parentGroupPort.name +
					"> " + "is within a nested subgroup within the same group as the port type definition!"
				info(
					message,
					TTCN3Package.eINSTANCE.TTCN3Reference_Name,
					MessageClass.STRUCTURE.toString,
					nodeRef.startLine.toString,
					nodeRef.endLine.toString,
					"4.2, " + "checkPortMessageGrouping",
					LogLevel.INFORMATION.toString
				);
				return;
			} else {
				TTCN3StatisticsProvider.getInstance.incrementCountStructure
				val message = "" + typeName + " definition for \"" + type.name + "\" <" + nodeType.startLine + "," +
					parentModuleType.name + "," + parentGroupType.name + "> " + "related to port type definition for \"" +
					port.name + "\" <" + nodePort.startLine + "," + parentModulePort.name + "," + parentGroupPort.name +
					"> " + "is not within the same group as the port type definition!"
				warning(
					message,
					TTCN3Package.eINSTANCE.TTCN3Reference_Name,
					MessageClass.STRUCTURE.toString,
					nodeRef.startLine.toString,
					nodeRef.endLine.toString,
					"4.2, " + "checkPortMessageGrouping"
				);
				return;
			}
		}
	}

	@Check
	def checkNoAllKeywordInPortDefinitions(PortDef port) {
		if (!activeProfile.checkNoAllKeywordInPortDefinitions)
			return;

		val procOrTypeList = port.body.attribs.getAllContentsOfType(ProcOrTypeList)
		val allOrSignatureList = port.body.attribs.getAllContentsOfType(AllOrSignatureList)
		val allOrTypeList = port.body.attribs.getAllContentsOfType(AllOrTypeList)
		for (a : procOrTypeList) {
			if (a.all != null) {
				TTCN3StatisticsProvider.getInstance.incrementCountStructure
				val INode node = NodeModelUtils.getNode(a)
				val message = "\"all\" keyword is used in the definition of port \"" + port.name + "\"!"
				warning(
					message,
					a,
					null,
					MessageClass.STRUCTURE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"4.2.1, " + MiscTools.getMethodName()
				);
			}
		}
		for (a : allOrSignatureList) {
			if (a.all != null) {
				TTCN3StatisticsProvider.getInstance.incrementCountStructure
				val INode node = NodeModelUtils.getNode(a)
				val message = "\"all\" keyword is used in the definition of port \"" + port.name + "\"!"
				warning(
					message,
					a,
					null,
					MessageClass.STRUCTURE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"4.2.1, " + MiscTools.getMethodName()
				);
			}
		}
		for (a : allOrTypeList) {
			if (a.all != null) {
				TTCN3StatisticsProvider.getInstance.incrementCountStructure
				val INode node = NodeModelUtils.getNode(a)
				val message = "\"all\" keyword is used in the definition of port \"" + port.name + "\"!"
				warning(
					message,
                    a,
                    null,
					MessageClass.STRUCTURE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"4.2.1, " + MiscTools.getMethodName()
				);
			}
		}

	}

	override register(EValidatorRegistrar registrar) {
		//not needed for classes used as ComposedCheck
	}
}
