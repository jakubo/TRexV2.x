package de.ugoe.cs.swe.validation;

import java.util.LinkedHashMap;

import org.eclipse.emf.ecore.EObject;

public class DataFlowHelper {
	
	public enum STATUS {
		UNDECLARED, DECLARED, INITIALISED, UNCERTAIN 
	}

	public enum USE {
	    DEFINE, REFERENCE 
	}
	
	private DataFlowHelper parent;
	private LinkedHashMap<EObject,STATUS> states = new LinkedHashMap<>();
	
	public DataFlowHelper() {
		
	}

	public DataFlowHelper(DataFlowHelper parent) {
		this.parent = parent;
		//DONE: change to recursive lookup in parent, 
		//      store dfh for different branches and then propagate only combined to direct parent
		//      which should be recursively propagated
		//      - alternatively clone parent and differentiate 
		//        between parent and children if performance is poor
		//      - this way parents will not be updated incorrectly outside
		//        the scope of the block
		//this.states.putAll(parent.getStates());
	}

	public DataFlowHelper getParent() {
		return parent;
	}

	public void setParent(DataFlowHelper parent) {
		this.parent = parent;
	}

	public LinkedHashMap<EObject,STATUS> getStates() {
		return states;
	}

//	public void setStates(LinkedHashMap<SingleVarInstance,STATUS> states) {
//		this.states = states;
//	}
	
	public STATUS getState(EObject v) {
		if (this.states.containsKey(v)) {
			return this.states.get(v);
		} else {
			if (this.parent != null) {
				return this.parent.getState(v);
			} else {
				return STATUS.UNDECLARED;
			}
		} 
	}

	public void setState(EObject v, STATUS s) {
		this.states.put(v, s);
	}

}
