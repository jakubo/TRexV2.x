package de.ugoe.cs.swe.common;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.swe.tTCN3.TTCN3Module;

public class CommonHelper {
	public static TTCN3Module getModule(Resource resource) {
		final TreeIterator<EObject> contentIterator = resource.getAllContents();
		while (contentIterator.hasNext()) {
			final EObject next = contentIterator.next();
			if (next instanceof TTCN3Module) {
				return (TTCN3Module) next;
			}
		}
		return null;
	}	
}
