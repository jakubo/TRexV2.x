package de.ugoe.cs.swe.common;

import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.xtend.typesystem.emf.EcoreUtil2;

import com.google.common.collect.Lists;

import de.ugoe.cs.swe.tTCN3.Enumeration;
public class TTCN3ReferenceHelper {

	/**
	 * Own implementation of org.eclipse.xtext.EcoreUtil2::findCrossReferences
	 * that returns true after the first reference occurred, otherwise false.
	 */
	public static boolean isReferenced(EObject rootElement, Set<? extends EObject> targets) {
		Queue<EObject> elements = Lists.newLinkedList();
		elements.add(rootElement);

		while (!elements.isEmpty()) {
			if (findChildElements(targets, elements))
				return true;
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	private static boolean findChildElements(final Set<? extends EObject> targets, Queue<EObject> elements) {
		EObject rootElement = elements.poll();

		for (EReference ref : rootElement.eClass().getEAllReferences()) {
			if (rootElement.eIsSet(ref)) {
				if (ref.isContainment()) {
					Object content = rootElement.eGet(ref, false);
					if (ref.isMany()) {
						InternalEList<EObject> contentList = (InternalEList<EObject>) content;
						for (int i = 0; i < contentList.size(); ++i) {
							EObject childElement = contentList.basicGet(i);
							if (!childElement.eIsProxy())
								elements.add(childElement);
						}
					} else {
						EObject childElement = (EObject) content;
						if (!childElement.eIsProxy())
							elements.add(childElement);
					}
				} else if (!ref.isContainer()) {
					if (checkNoContainerElements(rootElement, targets, ref))
						return true;
				}
			}
		}
		return false;
	}

	@SuppressWarnings({ "unchecked" })
	private static boolean checkNoContainerElements(EObject rootElement, final Set<? extends EObject> targets, EReference ref) {
		Object value = rootElement.eGet(ref, true);
		if (ref.isMany()) {
			InternalEList<EObject> values = (InternalEList<EObject>) value;
			for (int i = 0; i < values.size(); ++i) {
				EObject refElement = values.get(i);
				if (targets.contains(refElement)) {
					return true;
				}
			}
		} else {
			EObject refElement = (EObject) value;
			if (targets.contains(refElement)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	//use for debugging only, should be generally slower 
	public static boolean isReferenced3(EObject rootElement, Set<? extends EObject> targets) {
		Map<EObject, Collection<Setting>> results = EcoreUtil.UsageCrossReferencer.findAll(targets, rootElement);
		for (EObject o : results.keySet()) {
			System.out.println(o + " -> "); 
			for (Setting s : results.get(o)) {
				System.out.println("    "+s.getEStructuralFeature());
			}
			
		}
		return results.size()>0;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean isReferenced2(EObject rootElement, Set<? extends EObject> targets) {
		boolean found = false;
		
		for(EReference ref: rootElement.eClass().getEAllReferences()) {
			
			if (found)
				return true;
			
			if(rootElement.eIsSet(ref)) {
				if(ref.isContainment()) {
					Object content = rootElement.eGet(ref, true);
					if(ref.isMany()) {
						InternalEList<EObject> contentList = (InternalEList<EObject>) content;
						for(int i=0; i<contentList.size(); ++i) {
							EObject childElement = contentList.basicGet(i);
							if(!childElement.eIsProxy())
								found |= isReferenced2(childElement, targets);
						}
					} else {
						EObject childElement = (EObject) content;
						if(!childElement.eIsProxy())
							found |= isReferenced2(childElement, targets);
					}
				} else if (!ref.isContainer()) {
					Object value = rootElement.eGet(ref, true);
					if(ref.isMany()) {
						InternalEList<EObject> values = (InternalEList<EObject>) value;
						for(int i=0; i< values.size(); ++i) {
							EObject refElement = values.get(i);
							if(targets.contains(refElement)) {
								return true;
							}
						}
					} else {
						EObject refElement = (EObject) value;
						if(targets.contains(refElement)) {
							if (refElement instanceof Enumeration) {
								return false;
							}
							return true;
						}
					}
				}
			}
		}
		return found;
	}		
}
