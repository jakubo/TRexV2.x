package de.ugoe.cs.swe.common

import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.AltstepLocalDef
import de.ugoe.cs.swe.tTCN3.AltstepLocalDefList
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.ComponentDefList
import de.ugoe.cs.swe.tTCN3.ConstDef
import de.ugoe.cs.swe.tTCN3.ConstList
import de.ugoe.cs.swe.tTCN3.EnumDef
import de.ugoe.cs.swe.tTCN3.EnumDefNamed
import de.ugoe.cs.swe.tTCN3.Enumeration
import de.ugoe.cs.swe.tTCN3.EnumerationList
import de.ugoe.cs.swe.tTCN3.FormalTemplatePar
import de.ugoe.cs.swe.tTCN3.FormalValuePar
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionDefList
import de.ugoe.cs.swe.tTCN3.FunctionFormalPar
import de.ugoe.cs.swe.tTCN3.FunctionFormalParList
import de.ugoe.cs.swe.tTCN3.GroupDef
import de.ugoe.cs.swe.tTCN3.Initial
import de.ugoe.cs.swe.tTCN3.ModuleControlPart
import de.ugoe.cs.swe.tTCN3.ModuleDefinition
import de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList
import de.ugoe.cs.swe.tTCN3.ModulePar
import de.ugoe.cs.swe.tTCN3.ModuleParDef
import de.ugoe.cs.swe.tTCN3.ModuleParameter
import de.ugoe.cs.swe.tTCN3.NestedRecordOfDef
import de.ugoe.cs.swe.tTCN3.NestedSetOfDef
import de.ugoe.cs.swe.tTCN3.NestedTypeDef
import de.ugoe.cs.swe.tTCN3.PortElement
import de.ugoe.cs.swe.tTCN3.RecordDefNamed
import de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
import de.ugoe.cs.swe.tTCN3.RefValue
import de.ugoe.cs.swe.tTCN3.RefValueElement
import de.ugoe.cs.swe.tTCN3.RefValueTail
import de.ugoe.cs.swe.tTCN3.ReferencedType
import de.ugoe.cs.swe.tTCN3.ReferencedValue
import de.ugoe.cs.swe.tTCN3.SetDefNamed
import de.ugoe.cs.swe.tTCN3.SetOfDefNamed
import de.ugoe.cs.swe.tTCN3.SignatureDef
import de.ugoe.cs.swe.tTCN3.SingleConstDef
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
import de.ugoe.cs.swe.tTCN3.SingleVarInstance
import de.ugoe.cs.swe.tTCN3.StatementBlock
import de.ugoe.cs.swe.tTCN3.StructuredTypeDef
import de.ugoe.cs.swe.tTCN3.SubTypeDef
import de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TemplateDef
import de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar
import de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList
import de.ugoe.cs.swe.tTCN3.TestcaseDef
import de.ugoe.cs.swe.tTCN3.TypeDef
import de.ugoe.cs.swe.tTCN3.TypeReference
import de.ugoe.cs.swe.tTCN3.TypeReferenceTail
import de.ugoe.cs.swe.tTCN3.UnionDefNamed
import de.ugoe.cs.swe.tTCN3.VarInstance
import de.ugoe.cs.swe.tTCN3.VarList
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.scoping.IScope

import static org.eclipse.xtext.scoping.Scopes.*
import de.ugoe.cs.swe.tTCN3.ExtConstDef
import de.ugoe.cs.swe.tTCN3.IdentifierObjectList

class TTCN3ScopeHelper {

	def static <T extends EObject> T findDesiredParent(EObject o, Class<T> t) {
		if (o.eContainer == null)
			null
		else if (t.isAssignableFrom(o.eContainer.class))
			o.eContainer as T
		else
			o.eContainer.findDesiredParent(t)
	}

	def static void scopeModuleParDef(ModuleParDef moduleParDef, ArrayList<EObject> list) {
		if (moduleParDef.param != null) {
			for (ModuleParameter p : moduleParDef.param.list.params) {
				list.add(p)
			}
		} else if (moduleParDef.multitypeParam != null) {
			for (ModulePar mp : moduleParDef.multitypeParam.params) {
				for (ModuleParameter p : mp.list.params) {
					list.add(p)
				}
			}
		}
	}

	def static void scopeModuleTypes(ModuleDefinitionsList moduleDefs, ArrayList<EObject> res, boolean onlyPorts) {
		var StructuredTypeDef struct = null;
		var SubTypeDef sub = null;

		// treat signature as type: makes things easy, but should be changed maybe
		if (!onlyPorts) {
			for (ModuleDefinition d : moduleDefs.definitions.filter[it.def instanceof SignatureDef]) {
				res.add(d.def)
			}
		}

		for (ModuleDefinition d : moduleDefs.definitions.filter[it.def instanceof TypeDef]) {
			struct = (d.def as TypeDef).body.structured
			sub = (d.def as TypeDef).body.sub

			if (onlyPorts) {
				if (struct != null && struct.port != null) {
					res.add(struct.port)
				}
			} else {

				if (struct != null) {
					if (struct.record != null && struct.record instanceof RecordDefNamed) {
						res.add(struct.record)
					} else if (struct.union != null && struct.union instanceof UnionDefNamed) {
						res.add(struct.union)
					} else if (struct.set != null && struct.set instanceof SetDefNamed) {
						res.add(struct.set)
					} else if (struct.recordOf != null && struct.recordOf instanceof RecordOfDefNamed) {
						res.add(struct.recordOf)
					} else if (struct.setOf != null && struct.setOf instanceof SetOfDefNamed) {
						res.add(struct.setOf)
					} else if (struct.enumDef != null && struct.enumDef instanceof EnumDefNamed) {
						res.add(struct.enumDef)
					} else if (struct.port != null) {
						res.add(struct.port)
					} else if (struct.component != null) {
						res.add(struct.component)
					}
				} else if (sub != null) {
					if (sub instanceof SubTypeDefNamed) {
						res.add(sub)
					}
				}

			}
		}
	}

	def static void scopeModuleVariable(ModuleDefinitionsList moduleDef, ArrayList<EObject> list) {

		var defs = moduleDef.definitions.filter[it.def instanceof ConstDef]
		for (ModuleDefinition v : defs) {
			val constDefList = (v.def as ConstDef).defs as ConstList
			for (EObject d : constDefList.list) {
				list.add(d)
			}
		}

        defs = moduleDef.definitions.filter[it.def instanceof ExtConstDef]
        for (ModuleDefinition v : defs) {
            val constDefList = (v.def as ExtConstDef).id as IdentifierObjectList
            for (EObject d : constDefList.ids) {
                list.add(d)
            }
        }

		defs = moduleDef.definitions.filter[it.def instanceof TypeDef]
		for (ModuleDefinition v : defs) {
			val body = (v.def as TypeDef).body
			if (body.structured != null && body.structured.enumDef != null &&
				body.structured.enumDef instanceof EnumDefNamed) {
				(body.structured.enumDef as EnumDefNamed).list.scopeEnumElements(list)
			}
			if (body.structured != null && body.structured.enumDef != null &&
				body.structured.enumDef instanceof EnumDef) {
				(body.structured.enumDef as EnumDef).list.scopeEnumElements(list)
			}
		}

		defs = moduleDef.definitions.filter[it.def instanceof ModuleParDef]
		for (ModuleDefinition v : defs) {
			if ((v.def as ModuleParDef).param != null) {
				for (ModuleParameter p : (v.def as ModuleParDef).param.list.params) {
					list.add(p)
				}
			} else if ((v as ModuleParDef).multitypeParam != null) {
				for (ModulePar p : (v.def as ModuleParDef).multitypeParam.params) {
					for (ModuleParameter pa : p.list.params) {
						list.add(pa)
					}
				}
			}
		}
	}

	def static void scopeEnumElements(EnumerationList eList, ArrayList<EObject> list) {
		for (Enumeration e : eList.enums) {
			list.add(e);
		}
	}

	def static void scopeStatementBlock(StatementBlock block, ArrayList<EObject> list) {
		for (FunctionDefList l : block.def.filter[it != null]) {
			if (l.locDef != null && l.locDef.constDef != null) {
				val constDefList = l.locDef.constDef.defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					list.add(d);
				}
			} else if (l.locDef != null && l.locDef.templateDef != null) {
				list.add(l.locDef.templateDef.base)
			}
			if (l.locInst != null && l.locInst.variable != null) {
				val varList = l.locInst.variable.list
				val tempList = l.locInst.variable.tempList
				if (varList != null) {
					list.addAll(varList.variables);
				}
				if (tempList != null) {
					list.addAll(tempList.variables);
				}
			} else if (l.locInst != null && l.locInst.timer != null) {
				val varList = l.locInst.timer.list
				if (varList != null) {
					list.addAll(varList.variables);
				}
			}
		}
	}

	def static void scopeInitial(Initial init, ArrayList<EObject> list) {
		if (init.variable != null) {
			val varList = init.variable.list as VarList
			for (SingleVarInstance d : varList.variables) {
				list.add(d);
			}
		}
	}

	def static void scopeFunctionParameterValue(FunctionFormalParList parList, ArrayList<EObject> list) {
		for (FunctionFormalPar p : parList.params) {
			if (p.template != null) {
				list.add(p.template)
			} else if (p.value != null) {
				list.add(p.value)
			} else if (p.port != null) {
				list.add(p.port)
			} else if (p.timer != null) {
				list.add(p.timer)
			}
		}
	}

	def static void scopeModuleControlPart(ModuleControlPart control, ArrayList<EObject> list) {
		for (o : control.body.list.localDef) {
			if (o.constDef != null) {
				val constDefList = o.constDef.defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					list.add(d);
				}
			}
		}
		for (o : control.body.list.localInst) {
			if (o.variable != null) {
				val varList = o.variable.list
				val tempList = o.variable.tempList
				if (varList != null) {
					list.addAll(varList.variables);
				}
				if (tempList != null) {
					list.addAll(tempList.variables);
				}
			}
		}
	}

	def static void scopeTemplateParameter(TemplateDef template, ArrayList<EObject> list) {
		if (template.base.parList != null) {
			for (TemplateOrValueFormalPar p : template.base.parList.params) {
				if (p.value != null) {
					list.add(p.value)
				} else if (p.template != null) {
					list.add(p.template)
				}
			}
		}
	}

	def static void scopeTemplateOrValueFormalParList(TemplateOrValueFormalParList paramsList,
		ArrayList<EObject> list) {
		var RefValue value;

		for (TemplateOrValueFormalPar p : paramsList.params) {
			if (p.value != null) {
				value = p.value as RefValue
			} else if (p.template != null) {
				value = p.template as RefValue;
			}

			if (value != null)
				list.add(value);
		}
	}

	def static void scopeAltstepLocalDefList(AltstepLocalDefList localList, ArrayList<EObject> list) {
		for (AltstepLocalDef a : localList.defs) {
			if (a.const != null) {
				val constDefList = a.const.defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					list.add(d);
				}
			}
			if (a.variable != null) {
				val varList = a.variable.list
				val tempList = a.variable.tempList
				if (varList != null) {
					list.addAll(varList.variables);
				}
				if (tempList != null) {
					list.addAll(tempList.variables);
				}		
			}
		}
	}

	def static void scopeAltstepExtendedDefList(AltstepDef altstep, ArrayList<EObject> list) {
		if (altstep.spec != null) {
			componentScopeValueRefs(altstep.spec.component, list, true, true, true)
		}
	}

	def static dispatch IScope scopeTimerSpec(FunctionDef function) {
		val list = newArrayList
		if (function.runsOn != null) {
			componentScopeValueRefs(function.runsOn.component, list, true, false, false)
		}
		if (function.mtc != null) {
			componentScopeValueRefs(function.mtc.component, list, true, false, false)
		}
		if (function.system != null) {
			componentScopeValueRefs(function.system.component, list, true, false, false)
		}
		scopeFor(list)
	}

	def static dispatch IScope scopeTimerSpec(AltstepDef altstep) {
		val list = newArrayList
		if (altstep.spec != null) {
			componentScopeValueRefs(altstep.spec.component, list, true, false, false)
		}
		scopeFor(list)
	}

	def static dispatch IScope scopeTimerSpec(TestcaseDef testcase) {
		val list = newArrayList
		componentScopeValueRefs(testcase.spec.runsOn.component, list, true, false, false)
		if (testcase.spec.systemSpec != null) {
			componentScopeValueRefs(testcase.spec.systemSpec.component, list, true, false, false)
		}
		scopeFor(list)
	}

	def static Iterable<RefValueElement> functionLocalParameter(FunctionDef function) {
		val list = newArrayList
		if (function.parameterList == null)
			return list;

		for (p : function.parameterList.params) {
			if (p.timer != null) {
				list.add(p.timer)
			} else if (p.value != null) {
				list.add(p.value)
			} else if (p.template != null) {
				list.add(p.template)
			} else if (p.port != null) {
				list.add(p.port)
			}
		}
		list
	}

    def static Iterable<RefValueElement> testcaseLocalParameter(TestcaseDef testcase) {
        val list = newArrayList
        if (testcase.parList == null)
            return list;

        for (p : testcase.parList.params) {
            if (p.value != null) {
                list.add(p.value)
            } else if (p.template != null) {
                list.add(p.template)
            }
        }
        list
    }


	def static Iterable<RefValueElement> altstepLocalParameter(AltstepDef altstep) {
		val list = newArrayList
		if (altstep.params == null)
			return list;

		for (p : altstep.params.params) {
			if (p.timer != null) {
				list.add(p.timer)
			} else if (p.value != null) {
				list.add(p.value)
			} else if (p.template != null) {
				list.add(p.template)
			} else if (p.port != null) {
				list.add(p.port)
			}
		}
		list
	}


	def static void componentScopeValueRefs(ComponentDef component, ArrayList<EObject> list, boolean searchTimer,
		boolean searchVar, boolean searchTemplate) {

		for (ComponentDefList l : component.defs.filter[it.element.variable != null]) {
			if (l.element.variable.list != null) {
				for (SingleVarInstance v : l.element.variable.list.variables) {
					list.add(v)
				}
			} else if (l.element.variable.tempList != null) {
				for (SingleTempVarInstance v : l.element.variable.tempList.variables) {
					list.add(v)
				}
			}
		}

		if (searchVar) {
			for (ComponentDefList l : component.defs.filter[it.element.const != null]) {
				for (SingleConstDef v : l.element.const.defs.list) {
					list.add(v)
				}
			}
			for (ComponentDefList l : component.defs.filter[it.element.port != null]) {
				for (PortElement v : l.element.port.instances) {
					list.add(v)
				}
			}
		}
		if (searchTimer) {
			for (ComponentDefList l : component.defs.filter[it.element.timer != null]) {
				for (SingleVarInstance v : l.element.timer.list.variables) {
					list.add(v)
				}
			}
		}
		if (searchTemplate) {
			for (ComponentDefList l : component.defs.filter[it.element.template != null]) {
				list.add(l.element.template.base)
			}			
		}		
		for (ComponentDef e : component.extends) {
			componentScopeValueRefs(e, list, searchTimer, searchVar, searchTemplate)
		}
	}
	
	def static Iterable<EObject> scopeReferencedFields(ReferencedType type) {
		val List<EObject> list = newArrayList
		val ReferencedType originalType = findOriginalType(type)

		if (originalType == null)
			return list;

		if (originalType instanceof RecordDefNamed) {
			list.addAll(originalType.body.defs)
		} else if (originalType instanceof SetDefNamed) {
			list.addAll(originalType.body.defs)
		} else if (originalType instanceof UnionDefNamed) {
			list.addAll(originalType.body.defs)
		} else if (originalType instanceof ComponentDef) {
			val ArrayList<EObject> compList = newArrayList
			originalType.componentScopeValueRefs(compList, true, true, true)
			list.addAll(compList.map[it as RefValue])
		} else if (originalType instanceof RecordOfDefNamed) {
			// should be impossible, ... or not?
		} else if (originalType instanceof SetOfDefNamed) {
			// should be impossible, ... or not?
		}
		return list		
	}	
	
	def static Iterable<EObject> scopeReferencedFields(TypeReference type) {
		scopeReferencedFields(getReferencedType(type))
	}

	def static Iterable<RefValueElement> templateFieldScope(TypeReference ref) {
		val ArrayList<RefValueElement> list = newArrayList

		if (ref == null)
			return list

		val type = findOriginalType(getReferencedType(ref))

		if (type != null && typeof(RecordDefNamed).isAssignableFrom(type.class)) {
			list.addAll((type as RecordDefNamed).body.defs)
		}
		if (type != null && typeof(UnionDefNamed).isAssignableFrom(type.class)) {
			list.addAll((type as UnionDefNamed).body.defs)
		}
		if (type != null && typeof(SetDefNamed).isAssignableFrom(type.class)) {
			list.addAll((type as SetDefNamed).body.defs)
		}
		if (type != null && typeof(SignatureDef).isAssignableFrom(type.class)) {
			list.addAll((type as SignatureDef).paramList.params)
		}
		list
	}

	// TODO: consider nested types
	public def static ReferencedType findOriginalType(ReferencedType type) {
		if (type != null && typeof(SubTypeDefNamed).isAssignableFrom(type.class)) {
			findOriginalType(getReferencedType((type as SubTypeDefNamed).type.ref))
		} else if (type != null && typeof(RecordOfDefNamed).isAssignableFrom(type.class)) {
			findOriginalType(getReferencedType((type as RecordOfDefNamed).type.ref))
		} else if (type != null && typeof(SetOfDefNamed).isAssignableFrom(type.class)) {
			findOriginalType(getReferencedType((type as SetOfDefNamed).type.ref))
		} else {
			return type;
		}
	}

	def static TypeReference findUsedType(NestedTypeDef it) {

		// TODO; consider missing nested types
		switch it {
			NestedRecordOfDef: {
				if (it.type != null) {
					return it.type.ref;
				} else if (it.nested != null) {
					it.nested.findUsedType
				}
			}
			NestedSetOfDef: {
				if (it.type != null) {
					return it.type.ref;
				} else if (it.nested != null) {
					it.nested.findUsedType
				}
			}
		}
		return null;
	}

	def static void componentScopePorts(ComponentDef component, ArrayList<EObject> list) {
		for (ComponentDefList l : component.defs.filter[it.element.port != null]) {
			for (PortElement e : l.element.port.instances) {
				list.add(e)
			}
		}
		for (ComponentDef e : component.extends) {
			componentScopePorts(e, list)
		}
	}

	def static void scopeAllGroups(EObject object, ArrayList<EObject> list) {
		val module = object.findDesiredParent(typeof(TTCN3Module)) as TTCN3Module
		if (module.defs != null) {
			module.defs.scopeGroups(list, true)
		}
	}

	def static void scopeGroups(ModuleDefinitionsList md, ArrayList<EObject> list, boolean hierarchy) {
		for (ModuleDefinition g : md.definitions.filter[it.def instanceof GroupDef]) {
			val GroupDef group = g.def as GroupDef
			list.add(group)
			if (hierarchy && group.list != null) {
				group.list.scopeGroups(list, hierarchy)
			}
		}
	}

	def static ReferencedType getReferencedType(TypeReference reference) {
		if (reference == null)
			return null;

		var TypeReferenceTail tail = reference.tail
		var ReferencedType type = reference.head

		if (type == null) {
			return type;
		}

		while (tail != null) {
			val t = tail.type
			if (t instanceof ReferencedType) {
				type = t
				tail = tail.tail
			} else {
				tail = null
			}
		}

		return type;
	}

	def static RefValue getReferencedValue(ReferencedValue value) {
		if (value == null)
			return null

		val head = value.head

		if (head.tail == null && head.target instanceof RefValue)
			return head.target as RefValue
		else if (head.tail == null)
			return null

		var RefValueTail ref = head.tail

		while (ref.tail != null) {
			ref = ref.tail
		}

		if (ref.value instanceof RefValue)
			return ref.value as RefValue
		else
			return null
	}

	def static TypeReference findVariableType(RefValue variable) {
		var TypeReference ref = null

		if (variable == null)
			return ref

		// TODO: implement missing ref values
		if (variable instanceof SingleVarInstance) {
			val instance = variable.findDesiredParent(VarInstance)
			if (instance != null && instance.type != null && instance.type.ref != null) {
				ref = instance.type.ref
			} else if (instance.listType != null && instance.listType.ref != null) {
				ref = instance.listType.ref
			}
		} else if (variable instanceof FormalValuePar) {
			ref = variable.type.ref
		} else if (variable instanceof FormalTemplatePar) {
			ref = variable.type.ref
		}

		return ref
	}
}
