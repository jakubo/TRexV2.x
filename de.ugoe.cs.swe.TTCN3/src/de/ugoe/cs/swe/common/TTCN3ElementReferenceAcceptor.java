package de.ugoe.cs.swe.common;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.EcoreUtil2.ElementReferenceAcceptor;

public class TTCN3ElementReferenceAcceptor implements ElementReferenceAcceptor {
	private boolean foundReference = false;
	
	@Override
	public void accept(EObject referrer, EObject referenced, EReference reference, int index) {
		foundReference = true;
	}
	
	public boolean foundReference() {
		return foundReference;
	}
	
}
