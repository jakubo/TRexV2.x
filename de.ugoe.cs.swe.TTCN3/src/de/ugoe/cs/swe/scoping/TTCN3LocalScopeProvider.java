package de.ugoe.cs.swe.scoping;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.ISelectable;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractGlobalScopeDelegatingScopeProvider;
import org.eclipse.xtext.scoping.impl.MultimapBasedSelectable;
import org.eclipse.xtext.scoping.impl.SelectableBasedScope;
import org.eclipse.xtext.util.IResourceScopeCache;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;

import de.ugoe.cs.swe.tTCN3.ImportDef;

public class TTCN3LocalScopeProvider extends AbstractGlobalScopeDelegatingScopeProvider {

	@Inject
	private IResourceScopeCache cache = IResourceScopeCache.NullImpl.INSTANCE;

	@Inject
	private IQualifiedNameProvider qualifiedNameProvider;

	@Override
	public IScope getScope(EObject context, EReference reference) {
		ISelectable resourceContent = getAllDescriptions(context.eResource());
		IScope globalScope = getGlobalScope(context.eResource(), reference);
		return createScope(globalScope, resourceContent, reference.getEReferenceType(), isIgnoreCase(reference));
	}

	protected IScope createScope(IScope parent, ISelectable resourceContent, EClass type, boolean ignoreCase) {
		return SelectableBasedScope.createScope(parent, resourceContent, type, ignoreCase);
	}

	protected ISelectable getAllDescriptions(final Resource resource) {
		return cache.get("internalGetAllDescriptionsLocal", resource, new Provider<ISelectable>() {
			public ISelectable get() {
				return internalGetAllDescriptions(resource);
			}
		});
	}

	protected ISelectable internalGetAllDescriptions(final Resource resource) {
		Iterable<EObject> allContents = new Iterable<EObject>() {
			public Iterator<EObject> iterator() {
				return EcoreUtil.getAllContents(resource, false);
			}
		};
		Iterable<IEObjectDescription> allDescriptions = Scopes.scopedElementsFor(allContents, qualifiedNameProvider);
		allDescriptions = alternativeDescriptions(allDescriptions);
		return new MultimapBasedSelectable(allDescriptions);
	}

	private List<IEObjectDescription> alternativeDescriptions(Iterable<IEObjectDescription> allDescriptions) {
		List<IEObjectDescription> list = Lists.newArrayList();

		for (IEObjectDescription d : allDescriptions) {
			EObject eObject = d.getEObjectOrProxy();

			if (!(eObject instanceof ImportDef)) {
				list.add(EObjectDescription.create(d.getQualifiedName().getLastSegment(), eObject));
			}

		}

		list.addAll(Lists.newArrayList(allDescriptions));

		return list;
	}

}
